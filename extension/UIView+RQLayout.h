//
//  UIView+RQLayout.h
//  RQcatorgy
//
//  Created by Get-CC on 15/8/7.
//  Copyright (c) 2015年 GET-CC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (RQLayout)


@property (nonatomic,assign)CGFloat iwidth;

@property (nonatomic,assign)CGFloat iheight;

@property (nonatomic,assign)CGFloat ix;

@property (nonatomic,assign)CGFloat iy;

@property (nonatomic,assign)CGFloat icenterX;

@property (nonatomic,assign)CGFloat icenterY;

@property (nonatomic,assign)CGFloat itop;

@property (nonatomic,assign)CGFloat ibottom;

@property (nonatomic,assign)CGFloat ileft;

@property (nonatomic,assign)CGFloat iright;



-(UIView *)itoLeftOf:(UIView *)view;

-(UIView *)itoRightOf:(UIView *)view;

-(UIView *)ibelow:(UIView *)view;

-(UIView *)iabove:(UIView *)view;


@end
