//
//  UIKitExtension.swift
//  CommonTool
//
//  Created by 程巍巍 on 3/14/15.
//  Copyright (c) 2015 Littocats. All rights reserved.
//

import ObjectiveC
import Foundation
import UIKit

extension String {
    func stringByAppendingPathComponent(_ path: String) -> String {
        return (self as NSString).appendingPathComponent(path)
    }
    var pathExtension: String? {
        return NSString(string: self).pathExtension
    }
    var lastPathComponent: String? {
        return NSString(string: self).lastPathComponent
    }
    
    func heightWithConstrainedWidth(_ width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        
        let boundingBox = self.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        
        return boundingBox.height
    }
    
    func widthWithConstrainedHeight(_ height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: CGFloat.greatestFiniteMagnitude, height: height)
        
        let boundingBox = self.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        
        return boundingBox.width
    }
    
    var parseJSONString: AnyObject? {

        let data = self.data(using: String.Encoding.utf8, allowLossyConversion: false)
        
        if let jsonData = data {
            // Will return an object or nil if JSON decoding fails
            do {
                let json = try JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.mutableContainers)
                return json as AnyObject?
            } catch let error as NSError {
                print(error)
                return nil
            }
        } else {
            // Lossless conversion of the string was not possible
            return nil
        }
    }
    
    var intValue:Int{
        
        if self.characters.count > 0
        {
            return NSString(string: self).integerValue
        }
        
        return 0
    }
}

/****************************************************** UIControl ***************************************************************/
extension UIControl {
    typealias UIControlBlockHandler = @convention(block) (_ sender: UIControl/* UIControl or subClass*/, _ event: UIControlEvents) ->Void
    /**
    *  为 events 添加 block 事件
    *  @discussion if block is nil , events handler will be removed if exist
    */
    func handle(events: UIControlEvents, withBlock block: UIControlBlockHandler?) ->Self {
        let nameArr = BlockHandleSharedTarget.names(events: events)
        let table = blockTable
        for name in nameArr{
            table.removeObject(forKey: name as AnyObject?)
            self.removeTarget(BlockHandleSharedTarget.self, action: Selector(name+":"), for: BlockHandle.eventsTable[name]!)
            if block != nil {
                table.setObject(unsafeBitCast(block, to: AnyObject.self), forKey: name as AnyObject?)
                self.addTarget(BlockHandleSharedTarget.self, action: Selector(name+":"), for: BlockHandle.eventsTable[name]!)
            }
        }
        return self
    }
    
    fileprivate var blockTable: NSMapTable<AnyObject, AnyObject>{
        get{
            var table = objc_getAssociatedObject(self as Any, &BlockHandle.BlockTableKey) as? NSMapTable<AnyObject, AnyObject>
            if table == nil {
                table = NSMapTable.strongToStrongObjects()
                objc_setAssociatedObject(self, &BlockHandle.BlockTableKey, table, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
            }
            return table!
        }
        
    }
    
    fileprivate struct BlockHandle {
        static var onceToken: Int = 0
        static var BlockTableKey = "BlockTableKey"
        static let eventsTable = [
            "TouchDown"         :UIControlEvents.touchDown,
            "TouchDownRepeat"   :UIControlEvents.touchDownRepeat,
            "TouchDragInside"   :UIControlEvents.touchDragInside,
            "TouchDragOutside"  :UIControlEvents.touchDragOutside,
            "TouchDragEnter"    :UIControlEvents.touchDragEnter,
            "TouchDragExit"     :UIControlEvents.touchDragExit,
            "TouchUpInside"     :UIControlEvents.touchUpInside,
            "TouchUpOutside"    :UIControlEvents.touchUpOutside,
            "TouchCancel"       :UIControlEvents.touchCancel,
            "ValueChanged"      :UIControlEvents.valueChanged,
            "EditingDidBegin"   :UIControlEvents.editingDidBegin,
            "EditingChanged"    :UIControlEvents.editingChanged,
            "EditingDidEnd"     :UIControlEvents.editingDidEnd,
            "EditingDidEndOnExit":UIControlEvents.editingDidEndOnExit
        ]
        static var handler: BlockHandleSharedTarget!
    }
    @objc fileprivate final class  BlockHandleSharedTarget: NSObject {
        class func names(events: UIControlEvents) ->[String]{
            var nameArr = [String]()
            for item in BlockHandle.eventsTable {
                if [events , item.1] == item.1 {
                    nameArr.append(item.0)
                }
            }
            return nameArr
        }
        class func handle(event: String, sender: UIControl)->Void{
            let blockObject: AnyObject? = sender.blockTable.object(forKey: event as AnyObject?)
            if blockObject == nil {return}
            let block = unsafeBitCast(blockObject, to: UIControlBlockHandler.self)
            block(sender, BlockHandle.eventsTable[event]!)
        }
        
        @objc class func TouchDown              (_ sender: UIControl)->Void{handle(event: "TouchDown", sender: sender)}
        @objc class func TouchDownRepeat        (_ sender: UIControl)->Void{handle(event: "TouchDownRepeat", sender: sender)}
        @objc class func TouchDragInside        (_ sender: UIControl)->Void{handle(event: "TouchDragInside", sender: sender)}
        @objc class func TouchDragOutside       (_ sender: UIControl)->Void{handle(event: "TouchDragOutside", sender: sender)}
        @objc class func TouchDragEnter         (_ sender: UIControl)->Void{handle(event: "TouchDragEnter", sender: sender)}
        @objc class func TouchDragExit          (_ sender: UIControl)->Void{handle(event: "TouchDragExit", sender: sender)}
        @objc class func TouchUpInside          (_ sender: UIControl)->Void{handle(event: "TouchUpInside", sender: sender)}
        @objc class func TouchUpOutside         (_ sender: UIControl)->Void{handle(event: "TouchUpOutside", sender: sender)}
        @objc class func TouchCancel            (_ sender: UIControl)->Void{handle(event: "TouchCancel", sender: sender)}
        @objc class func ValueChanged           (_ sender: UIControl)->Void{handle(event: "ValueChanged", sender: sender)}
        @objc class func EditingDidBegin        (_ sender: UIControl)->Void{handle(event: "EditingDidBegin", sender: sender)}
        @objc class func EditingChanged         (_ sender: UIControl)->Void{handle(event: "EditingChanged", sender: sender)}
        @objc class func EditingDidEnd          (_ sender: UIControl)->Void{handle(event: "EditingDidEnd", sender: sender)}
        @objc class func EditingDidEndOnExit    (_ sender: UIControl)->Void{handle(event: "EditingDidEndOnExit", sender: sender)}
    }
}


/****************************************************** UIImage ***************************************************************/

extension UIImage {
    class func image(color: UIColor)-> UIImage {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        let context = UIGraphicsGetCurrentContext()
        context?.setFillColor(color.cgColor)
        context?.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    class func gratientImage(_ colors: [UIColor],size: CGSize, from: CGPoint, to: CGPoint)-> UIImage {
        let scale = UIScreen.main.scale
        let rect = CGRect(x: 0, y: 0, width: size.width * scale, height: size.height * scale)
        let glayer = CAGradientLayer()
        glayer.bounds = rect
        glayer.colors = { (colors: [UIColor])->[CGColor] in
            var cgColors = [CGColor]()
            for color in colors {
                cgColors.append(color.cgColor)
            }
            return cgColors
            }(colors)
        glayer.startPoint = from
        glayer.endPoint = to
        
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context?.clear(rect)
        glayer.render(in: context!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return UIImage(cgImage: image!.cgImage!, scale: scale, orientation: UIImageOrientation.up)
    }
    
    func resize(_ size: CGSize) ->UIImage {
        let imageRef = self.cgImage
        let colorSpace = imageRef?.colorSpace
        let bitsPerComponent = imageRef?.bitsPerComponent
        let bitsPerRow = imageRef?.bytesPerRow
        let bitmapInfo = imageRef?.bitmapInfo
        let bitmap = CGContext(data: nil, width: Int(size.width), height: Int(size.height), bitsPerComponent: bitsPerComponent!, bytesPerRow: bitsPerRow!, space: colorSpace!, bitmapInfo: (bitmapInfo?.rawValue)!)
        bitmap?.draw(imageRef!, in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let newImageRef = bitmap?.makeImage()
        let newImage = UIImage(cgImage: newImageRef!)
        return newImage
    }
    
    
}

extension UILabel {
    func addAttribute(font: CGFloat? = nil, textAlignment: NSTextAlignment? = nil, textColor: String? = nil, text: String? = nil) {
        if font != nil
        {
            self.font = UIFont.systemFont(ofSize: font!)
        }
        if textAlignment != nil
        {
            self.textAlignment = textAlignment!
        }
        if textColor != nil
        {
            self.textColor = UIColor(rgba: textColor!)
        }
        if text != nil
        {
            self.text = text
        }
    }
}


/************************************************* NSAttributedString ********************************************************/


let CMTextFontFamilyAttributeName = "CMTextFontFamilyAttributeName"
let CMTextFontSizeAttributeName = "CMTextFontSizeAttributeName"

let CMTextAlignmentAttributeName = "NSTextAlignmentAttributeName"
let CMTextFirstLineHeadIndentAttributeName = "CMTextFirstLineHeadIndentAttributeName"
let CMTextHeadIndentAttributeName = "CMTextHeadIndentAttributeName"
let CMTextTailIndentAttributeName = "CMTextTailIndentAttributeName"
let CMTextLineSpaceAttributeName = "CMTextLineSpaceAttributeName"

private func FloatValue(_ str: String)->Float {
    let float = (str as NSString).floatValue
    return float
}



extension UIButton {
    func addAttribute(_ title: String?, titleColor: String?, font: CGFloat?, image: String?, backImage: String?, backColor: String?, tag: Int?) {
        self.setTitle(title, for: UIControlState())
        self.setTitleColor(UIColor(rgba: titleColor!), for: UIControlState())
        self.titleLabel?.font = UIFont.systemFont(ofSize: font!)
        if image != nil {
            self.setImage(UIImage(named: image!), for: UIControlState())
        }
        if backImage != nil {
            self.setBackgroundImage(UIImage(named: backImage!), for: UIControlState())
        }
        if backColor != nil {
            self.backgroundColor = UIColor(rgba: backColor!)
        }
        self.backgroundColor = UIColor.clear
        if tag != nil {
            self.tag = tag!
        }
    }
}

extension UITextField {
    func addAttribute(_ placeholder: String?, title: String?, titleColor: String?, font: CGFloat?, textAlignment: NSTextAlignment) {
        self.placeholder = placeholder
        self.text = title
        self.textColor = UIColor(rgba: titleColor!)
        self.font = UIFont.systemFont(ofSize: font!)
        self.textAlignment = textAlignment
        
    }
}

extension UIFont {
    func sizeOfString (_ string: String, constrainedToWidth width: Double) -> CGSize {
        return NSString(string: string).boundingRect(with: CGSize(width: width, height: DBL_MAX),
            options: NSStringDrawingOptions.usesLineFragmentOrigin,
            attributes: [NSFontAttributeName: self],
            context: nil).size
    }
}




