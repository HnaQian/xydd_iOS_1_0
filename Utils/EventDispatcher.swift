  //
//  EventDispatcher.swift
//  Limi
//
//  Created by 程巍巍 on 5/11/15.
//  Copyright (c) 2015 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit
import CoreData

protocol EventDispatcherCommand
{
    func excute(url: URL)->Void
}

final class EventDispatcher
{
    static let VCHandlerMap: [String: BaseViewController.Type] = [
        "goods_general": GoodsDetailViewController.self,
        "goods_item": GoodsDetailViewController.self,
        "good_list": GoodsListViewController.self,
        "goods_list": GoodsListViewController.self,
        "goods_list_by_tag": GoodsListByTagViewController.self,
        "fees": RechargeViewController.self,
        
        // 主题
        "article_list": FeatureViewController.self,
        "article_item": TacticDetailViewController.self,
        "package_item": WebViewController.self,
        "package_guess": WebViewController.self,
        "birthday": BirthdayTopicViewController.self,
        "wedding": WeddingTopicViewController.self,
        "star": TopicStarViewController.self,
        "custom": TopicCustomViewController.self,
        "abroad": TopicAbroadViewController.self,
        "designer": TopicDesignerViewController.self,
        "master_recommend_list": RecommendListViewController.self,
        
        // 个人中心
        "login": LoginViewController.self,
        "gift_list": OrderListViewControllerV1.self,
        "coupon": CouponListViewController.self,
        "fav_list": FavListViewController.self,
        "deliver_address": AddressListViewController.self,
        "deliver_address_manage": DeliveryAddressManageViewController.self,
        "deliver_address_edit": DeliveryAddressEditViewController.self,
        "feed_back": FeedBackViewController.self,
        "setting": SettingViewController.self,
        "me": UserCenterViewController.self,
        "account_info": AccountInfoViewController.self,
        "about_us": AboutViewController.self,
        "message_center": MessageList.self,
        "order_item": OrderDetailViewControllerV1.self,
        "index":HomeViewControllerV1.self,
        "gift":GiftChooseViewController.self,
        "search":AllSearchViewController.self,
        "checkin":DailySignViewController.self,
        "user_point":ConvertNotesViewController.self,
        "point_goods":PointMallViewController.self,
        //送礼提醒
        "gift_remind_list":RemindViewController.self,//送礼提醒列表
        "relation_add": AddGiftReminderViewController.self,//添加送礼提醒
        "relation_upload": AddFromBookViewController.self,//从通讯录导入
        "relation_wish": HeartWishViewController.self,//场景送祝福
        "holiday_wish": HeartWishViewController.self,//节日送祝福
        "coupon_item": CouponDetailViewController.self,//从通讯录导入
        "service_audio": RecordWishViewController.self,//录音服务
        "basket_gift": ShoppingBasketViewController.self,//礼物篮
        "top":LeaderBoardViewController.self,
        
        //问答社区
        "sns":CommonQuestionViewController.self,
        "snspush":MyQuestionViewController.self
    ]
    
    
    class func dispatch(_ url: String, onNavigationController navc: UINavigationController? = nil) {

        if url.characters.count == 0{
            return
        }
        
        //去除连接中的空格
        let murl = NSString(string: url).replacingOccurrences(of: " ", with: "")
        
        let url = URL(string: murl)
        
        if url == nil{
            return
        }
        
        dispatch(URL: url!, onNavigationController: navc)
    }
    
    
    class func dispatch(_ url: String, params: [String: AnyObject]?, onNavigationController navc: UINavigationController? = nil) {

        var fileURL = Foundation.URL(string: url)
        print(fileURL)
        if params != nil {
            let query = CMDQueryStringSerialization.queryString(with: params)
            let filePath = "\(url)?\(query!)"
            fileURL = Foundation.URL(string: filePath)
        }
        
        dispatch(URL: fileURL!, onNavigationController: navc)
    }
    
    
    
    class func dispatch(URL: Foundation.URL, onNavigationController navc: UINavigationController? = nil) {
  
        /**
         兼容新服务器 href 标准
         href属性的native的判断规则：
         
         host 是 wechat.giftyou.me
         包含 app=1 参数
         通过 path的最后一个component获取本地操作
         对照表如下
         */
        let eventURL = preParserURL(URL)
        
        let scheme = eventURL.scheme?.lowercased()
        
        let host = eventURL.host
        
        if host == nil {
            return
        }
//        assert(host != nil, "EventDispatcher dispatch error: host not found .")
        
        var handlerVC: UIViewController?
        
        if host == "artilce_item"{
            Statistics.count(Statistics.Tractic.article_detail_click, andAttributes: ["href":URL.absoluteString])
        }
        
        if scheme == "http" || scheme == "https"
        {
            var NEW_URL = URL
            
            if Utils.containsString("need_uid=1", inString: URL.absoluteString){
                let uid : Int = UserDefaults.standard.integer(forKey: "UserUid")
                let newUrl =  String(format: "%@&uid=%d", URL.absoluteString,uid)
                NEW_URL = Foundation.URL(string: newUrl)!
            }
            
            
            if Utils.containsString("need_token=1", inString: URL.absoluteString)
            {
                if let token : String = UserDefaults.standard.string(forKey: "UserToken"){
                    let newUrl =  String(format: "%@&token=%@", URL.absoluteString,token)
                    NEW_URL = Foundation.URL(string: newUrl)!
                }
            }
            
            handlerVC = WebViewController(URL: NEW_URL)
        }else if scheme == "local" || scheme == "xydd"
        {
            if host == "master_recommend_list"
            {
                handlerVC = RecommendListViewController(nibName: "RecommendListViewController", bundle: nil)
            }
            else if host == "about_us"{
                handlerVC = AboutViewController(nibName: "AboutViewController", bundle: nil)
            }
            else if host == "order_item"{
                if let param : NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: URL.query) as NSDictionary?,
                    let id = param["id"],
                    let idStr = id as? String
                {
                        handlerVC = OrderDetailViewControllerV1.newDetailVC(idStr.intValue)
                }
            }
            else if host == "deliver_address"{
                handlerVC = AddressListViewController(nibName: "AddressListViewController", bundle: nil)
                
            }
            else if host == "me"{
                handlerVC = UserCenterViewController()
            }
            else if host == "relation_wish" || host == "holiday_wish"
            {
                if let param : NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: URL.query) as NSDictionary?,
                    let id = param.object(forKey: "id"){
                        handlerVC = HeartWishViewController.newHeartWishViewController(JSON(["Id" :id] as AnyObject),presentType:host == "relation_wish" ? PresentType.personal : PresentType.holiday)
                }
            }
            else if host == "article_item"{
                let tacticDetailVc = TacticDetailViewController()
                if Utils.containsString("id=", inString: URL.absoluteString)
                {
                   let tempArray = (URL.absoluteString as String).components(separatedBy: "id=")
                    if tempArray.count > 1 {
                        if String(tempArray[1]).contains("&") {
                            let tempArray2 = String(tempArray[1])?.components(separatedBy: "&")
                            tacticDetailVc.article_id = Int((tempArray2?[0])!)
                        }else {
                            tacticDetailVc.article_id = Int(tempArray[1])
                        }
                    }
                }
                handlerVC = tacticDetailVc
            }
            else if host == "service_audio"{
                let record = RecordWishViewController()
                if let param : NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: URL.query) as NSDictionary?{
                    if let packageId: AnyObject = param.object(forKey: "package_id") as AnyObject?{
                        if let id = packageId as? String {
                            record.packageId = id
                        }

                    }
                }
                handlerVC = record
            }
            else if host == "package_item"{
                let webVC = WebViewController(URL: eventURL)
                webVC.type = Constant.WebViewType.packageItemType.rawValue
                handlerVC = webVC
            }else if host == "sns" || host == "snspush" {
                let communityVc = CommunityViewController()
                outIndex = (host == "sns") ? 0 : 1
                communityVc.isInsideTurning = false
                handlerVC = communityVc
            }
            else
            {
                handlerVC = VCHandlerMap[host!]?.init()
            }
            
            if host != "sns" && host != "snspush" {
                (handlerVC as? BaseViewController)?.eventURL = URL
            }
            
        }
        
        //        assert(handlerVC != nil, "EventDispatcher dispatch error: Cann't init vc for url \(URL)")
        if handlerVC == nil{return}
        // 获取 NavigationController
        
        var navigationController: UINavigationController? = navc
        
        if navigationController == nil
        {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            if appDelegate.currentViewController != nil{
                navigationController = appDelegate.currentViewController?.navigationController
            }
            else{
                navigationController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController
            }
            
            
        }
        
        //如果能够登录可以进入四个主页面
        if handlerVC!.isKind(of: HomeViewControllerV1.self)
        {
            let userInfo: Dictionary<String,Int>! = [
                "index": 0,
                ]
            NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.show_rootController), object: nil, userInfo: userInfo)
            navigationController!.popToRootViewController(animated: true)
            
        }
        else if handlerVC!.isKind(of: ServiceListViewController.self)
        {
            let tabVC = UIApplication.shared.keyWindow?.rootViewController as! MainViewController
            tabVC.selectedIndex = 1
            if navigationController != nil//返回root
            {
                navigationController!.popToRootViewController(animated: true)
            }
            
        }
        else if handlerVC!.isKind(of: GiftChooseViewController.self)
        {
            let tabVC = UIApplication.shared.keyWindow?.rootViewController as! MainViewController
            tabVC.selectedIndex = 2
            if navigationController != nil//返回root
            {
                navigationController!.popToRootViewController(animated: true)
            }
            
        }
        else if handlerVC!.isKind(of: UserCenterViewController.self)
        {
            let tabVC = UIApplication.shared.keyWindow?.rootViewController as! MainViewController
            tabVC.selectedIndex = 3
            if navigationController != nil//返回root
            {
                navigationController!.popToRootViewController(animated: true)
            }
            
        }
        else
        {
            handlerVC?.hidesBottomBarWhenPushed = true
            
            navigationController?.pushViewController(handlerVC!, animated: true)
        }
    }
    
    class func isNeedAppProcess(_ url: URL)-> Bool {
        if let param : NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: url.query) as NSDictionary?{
            if let app: AnyObject = param.object(forKey: "app") as AnyObject?{
                if "\(app)" == "1" {
                    return true
                }
            }
        }
        return false
    }
    
    class func isPureNumberWithGoodsId(_ url:URL) ->Bool {
        if let params:NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: url.query) as NSDictionary? {
            if let id = params.object(forKey: "id") as? String {
                if (id as NSString).intValue != 0{
                    //全部是数字
                    return true
                }else {
                    return false
                }
            }
        }
        return true
    }
    
    class func preParserURL(_ url: URL)-> URL {
        if let param : NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: url.query) as NSDictionary?{
            if let app: AnyObject = param.object(forKey: "app") as AnyObject?{
                if "\(app)" == "1" {
                    if let lastCom = url.path.lastPathComponent {
                        if let _ = VCHandlerMap[lastCom] {
                            return URL(string: "local://\(lastCom)/\(url.path)?\(url.query!)")!
                        }
                        else if url.query!.contains("&app=1"){
                            let query = url.query?.replacingOccurrences(of: "&app=1", with: "")
                            print(url.scheme! + "://" + url.host! + url.path + "?" + query!)
                            return URL(string: url.scheme! + "://" + url.host! + url.path + "?" + query!)!
                            
                        }
                        else if url.query!.contains("app=1"){
                            let query = url.query?.replacingOccurrences(of: "app=1", with: "")
                            print(url.scheme! + "://" + url.host! + url.path + "?" + query!)
                            return URL(string: url.scheme! + "://" + url.host! + url.path + "?" + query!)!
                        }
                    }
                }
                
            }
        }
        return url
    }
    
    class func preParserURLWithNotInMap(_ url: URL)-> URL {
        if let param : NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: url.query) as NSDictionary?{
            if let app: AnyObject = param.object(forKey: "app") as AnyObject?{
                if "\(app)" == "1" {
                    if let lastCom = url.path.lastPathComponent {
                        return URL(string: "local://\(lastCom)/\(url.path)?\(url.query!)")!
                    }
                }
            }
        }
        
        return url
    }
    

}
