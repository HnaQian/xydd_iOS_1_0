//
//  Crypto.swift
//  Crypto
//
//  Created by 程巍巍 on 3/19/15.
//  Copyright (c) 2015 Littocats. All rights reserved.
//  
//  需在 Bridg_Header.h 中 #import <CommonCrypto/CommonCrypto.h>

import Foundation

struct Crypto {
    static func MD5(data: Data) -> String{
        let c_data = (data as NSData).bytes
        var md: [UInt8] = [UInt8](repeating: 0, count: Int(CC_MD5_DIGEST_LENGTH))
        CC_MD5(c_data, CC_LONG(data.count), UnsafeMutablePointer<UInt8>(mutating: md))
        
        var ret: String = ""
        for index in 0 ..< Int(CC_MD5_DIGEST_LENGTH) {
            ret += String(format: "%.2X", md[index])
        }
        return ret
    }
    static func MD5(file: String) ->String?{
        let inStream = InputStream(fileAtPath: file)
        if inStream == nil {return nil}
        
        inStream?.open()
        if inStream!.streamStatus != Stream.Status.open {return nil}
        
        var hashObject: CC_MD5_CTX = CC_MD5state_st(A: 0, B: 0, C: 0, D: 0, Nl: 0, Nh: 0, data: (CC_LONG(0),CC_LONG(0),CC_LONG(0),CC_LONG(0),CC_LONG(0),CC_LONG(0),CC_LONG(0),CC_LONG(0),CC_LONG(0),CC_LONG(0),CC_LONG(0),CC_LONG(0),CC_LONG(0),CC_LONG(0),CC_LONG(0),CC_LONG(0)), num: 0)
        CC_MD5_Init(&hashObject)
        
        var hasMoreData = true
        let buffer = UnsafeMutablePointer<UInt8>.allocate(capacity: 4096)
        var readBytesCount = 0
        while hasMoreData {
            readBytesCount = inStream!.read(buffer, maxLength: 4096)
            if readBytesCount == -1 {break}
            if readBytesCount == 0 {hasMoreData = false; continue}
            CC_MD5_Update(&hashObject, buffer, CC_LONG(readBytesCount))
        }
        buffer.deallocate(capacity: 4096)
        
        var md: [UInt8] = [UInt8](repeating: 0, count: Int(CC_MD5_DIGEST_LENGTH))
        CC_MD5_Final(UnsafeMutablePointer<UInt8>(mutating: md), &hashObject)
        
        var ret: String = ""
        for index in 0 ..< Int(CC_MD5_DIGEST_LENGTH) {
            ret += String(format: "%.2X", md[index])
        }
        return ret
    }
    
    static func Base64Encode(data: Data) -> String{
        let inStream = InputStream(data: data)
        inStream.open()
        let buffer = UnsafeMutablePointer<UInt8>.allocate(capacity: 3)
        var bValue: Int = 0
        let ret: NSMutableString = NSMutableString()
        while inStream.read(buffer, maxLength: 3) == 3{
            bValue = (Int(buffer[0]) << 16) + (Int(buffer[1]) << 8) + Int(buffer[2])
            ret.append(base64_table[bValue >> 18])
            ret.append(base64_table[bValue >> 12 & 0b111111])
            ret.append(base64_table[bValue >> 6 & 0b111111])
            ret.append(base64_table[bValue & 0b111111])
        }
        let remainBitLen = data.count%3
        bValue = 0
        memset(buffer + remainBitLen, 0, 3 - remainBitLen)
        bValue = (Int(buffer[0]) << 16) | (Int(buffer[1]) << 8) | Int(buffer[2])
        for index in 0 ... remainBitLen {
            ret.append(base64_table[bValue >> (18 - index * 6) & 0b111111])
        }
        if remainBitLen != 0 { ret.append(remainBitLen == 1 ? "==" : "=")}
        inStream.close()
        buffer.deallocate(capacity: 3)
        return ret as String
    }
    static func Base64Decode(data: String) -> Data{
        let inStream = InputStream(data: (data as NSString).data(using: String.Encoding.utf8.rawValue)!)
        inStream.open()
        let buffer = UnsafeMutablePointer<UInt8>.allocate(capacity: 4)
        var bValue: Int = 0
        let value: UnsafeMutablePointer<UInt8> = UnsafeMutablePointer<UInt8>.allocate(capacity: 3)
        let ret: NSMutableData = NSMutableData()
        while inStream.read(buffer, maxLength: 4) == 4{
            bValue = de_base64_table[Int(buffer[0])] << 18 | de_base64_table[Int(buffer[1])] << 12 | de_base64_table[Int(buffer[2])] << 6 | de_base64_table[Int(buffer[3])]
            value[0] = UInt8(bValue >> 16)
            value[1] = UInt8(bValue >> 8 & 0xff)
            value[2] = UInt8(bValue & 0xff)
            ret.append(value, length: 3)
        }
        
        // 处理最后三位
        var additionalBitLen = 0
        for index in 0 ... 3 {
            if buffer[index] == 61 {    // "="
                buffer[index] = 0
                additionalBitLen += 1
            }
        }
        if additionalBitLen != 0 {
            bValue = de_base64_table[Int(buffer[0])] << 18 | de_base64_table[Int(buffer[1])] << 12 | de_base64_table[Int(buffer[2])] << 6 | de_base64_table[Int(buffer[3])]
            value[0] = UInt8(bValue >> 16)
            value[1] = UInt8(bValue >> 8 & 0xff)
            value[2] = UInt8(bValue & 0xff)
            
            ret.replaceBytes(in: NSMakeRange(ret.length - 3, 3), withBytes: value, length: 3 - additionalBitLen)
        }
        inStream.close()
        value.deallocate(capacity: 3)
        buffer.deallocate(capacity: 4)
        return ret as Data
    }
    
    // 对称加密
    let aes128 = 1
    enum SymmetricCryptType{
        case AES128
        case aes192
        case aes256
        case des
        case des3
        
        var keySize: Int{
//            return (self.rawValue & 0xFFFF0000) >> 16
            return self.rawValue >> 16
        }
        var algorithm: CCAlgorithm{
            return CCAlgorithm(self.rawValue & 0xFFFF)
        }
        fileprivate var rawValue: Int{
            switch self {
            case .AES128: return kCCKeySizeAES128 << 16 | Int(kCCAlgorithmAES)
            case .aes192: return kCCKeySizeAES192 << 16 | Int(kCCAlgorithmAES)
            case .aes256: return kCCKeySizeAES256 << 16 | Int(kCCAlgorithmAES)
            case .des: return kCCKeySizeDES << 16 | Int(kCCAlgorithmDES)
            case .des3: return kCCKeySize3DES << 16 | Int(kCCAlgorithm3DES)
            }
        }
        
        fileprivate static let aes128: Int = kCCKeySizeAES128 << 16 | Int(kCCAlgorithmAES)
    }
    static func SymmetricEncrypt(_ data: Data, withPassword password: String, type: SymmetricCryptType) ->Data {
        _ = type.keySize
        return SymmetricCrypt(data: data, keyStr: password, keySize: type.keySize, algorithm: type.algorithm, operation: CCOperation(kCCEncrypt))
    }
    static func SymmetricDecrypt(_ data: Data, withPassword password: String, type: SymmetricCryptType) ->Data {
        return SymmetricCrypt(data: data, keyStr: password, keySize: type.keySize, algorithm: type.algorithm, operation: CCOperation(kCCDecrypt))
    }    


}

extension Crypto {
    fileprivate static func SymmetricCrypt(data: Data, keyStr: String, keySize: Int, algorithm: CCAlgorithm, operation: CCOperation) ->Data{
        let keyLength = ((keyStr as NSString).length/keySize+1)*keySize+1
        var key = [CChar](repeating: 0, count: keyLength)
        
        let _ = keyStr.getCString(&key, maxLength: keyLength, encoding: String.Encoding.utf8)
        
        let dataLength = data.count
        let bufferSize = dataLength + kCCBlockSizeAES128
        let buffer = UnsafeMutablePointer<UInt8>.allocate(capacity: Int(bufferSize))
        
        var retLen = 0
        
        let status: CCCryptorStatus = CCCrypt(
            operation, algorithm,
            CCOptions(kCCOptionPKCS7Padding | kCCOptionECBMode),
            UnsafeRawPointer(key), keySize,
            nil,
            (data as NSData).bytes, dataLength,
            buffer, bufferSize,
            &retLen
        )
        
        var result: Data!
        if Int(status) == kCCSuccess{
            result = Data(bytesNoCopy: buffer, count: retLen, deallocator: .free)
        }
        
        return result
    }
    
}

extension Crypto {
    fileprivate static let base64_table = [
        "A", "B", "C", "D", "E", "F", "G", "H",
        "I", "J", "K", "L", "M", "N", "O", "P",
        "Q", "R", "S", "T", "U", "V", "W", "X",
        "Y", "Z", "a", "b", "c", "d", "e", "f",
        "g", "h", "i", "j", "k", "l", "m", "n",
        "o", "p", "q", "r", "s", "t", "u", "v",
        "w", "x", "y", "z", "0", "1", "2", "3",
        "4", "5", "6", "7", "8", "9", "+", "/"
    ]
    fileprivate static let de_base64_table = [
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x3E,0x00,0x00,0x00,0x3F,
        0x34,0x35,0x36,0x37,0x38,0x39,0x3A,0x3B,0x3C,0x3D,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,
        0x0F,0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x00,0x00,0x00,0x00,0x00,
        0x00,0x1A,0x1B,0x1C,0x1D,0x1E,0x1F,0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,
        0x29,0x2A,0x2B,0x2C,0x2D,0x2E,0x2F,0x30,0x31,0x32,0x33,0x00,0x00,0x00,0x00,0x00
    ]
}


