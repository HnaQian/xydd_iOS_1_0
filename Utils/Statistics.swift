//
//  Statistics.swift
//  Limi
//
//  Created by Richie on 16/5/20.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//统计

import Foundation
import UIKit

///统计
struct Statistics {
    
    /**
     统计，attributes可不传   
     注：attributes 的key不可以为id/su/td，这些是UM专用key，建议使用二个字符以上的key
     */
    
    static func count(_ eventId:String,andAttributes attributes:[String:String]! = nil,file:String = #file,line:Int = #line)
    {
//                let str = eventId + "\n" + file + "\n" + "\(line)"
//                UIAlertView(title: "统计成功", message: str, delegate: nil, cancelButtonTitle: "确定").show()
        
        if attributes == nil{
            MobClick.event(eventId)
        }else{
            print(attributes)
            MobClick.event(eventId, attributes: attributes)
        }
    }

    
    //    func setValuesForKeys(setValues values:AnyObject?...,forKeys keys:[String]){
    //
    //    }
    //
    //
    //    func setValueForKey(value:AnyObject?,key:String?){
    //
    //    }
    
    
    ///首页统计
    struct Home {
        
        /*
         home_banner_goods_click	首页-Banner-商品
         home_banner_article_click	首页-Banner-文章
         home_banner_giftbox_click  首页-Banner-礼盒
         home_banner_remind_click	首页-Banner-提醒
         home_icon1_click	首页-ICON1
         home_icon2_click	首页-ICON2
         home_icon3_click	首页-ICON3
         home_icon4_click	首页-ICON4
         home_sales_click	首页-争分夺礼
         home_area_left_click	首页-礼盒
         home_area_right1_click	首页-礼物排行榜
         home_area_right2_click	首页-生日礼
         home_ad_add_click	首页-新增广告位
         home_article_tab1_click	首页-攻略分类Tab1
         home_article_tab2_click	首页-攻略分类Tab2
         home_article_tab3_click	首页-攻略分类Tab3
         home_article_click	首页-攻略
         home_search_click	首页-搜索
         
         
         1.3.8+
        
         home_sales_click	 首页-争分夺礼	 	 	 细分每个商品id的点击（是否仅统计首页点击露出商品）
         home_area_click	 首页-礼盒（男送女）
         
         home_ad_add_click	 首页-新增广告位	 	 	 这部分就是攻略上方的新增广告位
         home_article_click	 首页-攻略	 	 	 细分投放文章id的点击
         home_article_box_click	 首页-攻略礼盒	 	 	 细分投放礼盒链接
         home_article_master_click	 首页-攻略达人	 	 	 细分投放文章id的点击
         home_notice_ad_click     首页-轮播广告
         home_notice_holiday_click  首页-送礼提醒
         
         home_tab_click     首页-tab
         home_sign_click	首页-签到
         */
        
        
        static let home_area_click  = "home_area_click"
        static let home_icon1_click = "home_icon1_click"
        static let home_icon2_click = "home_icon2_click"
        static let home_icon3_click = "home_icon3_click"
        static let home_icon4_click = "home_icon4_click"
        static let home_sales_click = "home_sales_click"
        
        static let home_ad_add_click    = "home_ad_add_click"
        static let home_article_click   = "home_article_click"
        static let home_search_click    = "home_search_click"
        static let home_notice_ad_click = "home_notice_ad_click"
        static let home_area_left_click = "home_area_left_click"
        
        static let home_area_right1_click = "home_area_right1_click"
        static let home_area_right2_click = "home_area_right2_click"
        static let home_banner_goods_click = "home_banner_goods_click"
        static let home_article_tab1_click = "home_article_tab1_click"
        static let home_article_tab2_click = "home_article_tab2_click"
        static let home_article_tab3_click = "home_article_tab3_click"
        static let home_banner_remind_click = "home_banner_remind_click"
        static let home_notice_holiday_click = "home_notice_holiday_click"
        static let home_banner_article_click = "home_banner_article_click"
        static let home_window_click = "home_window_click"
        
        static let home_article_box_click       = "home_article_box_click"
        static let home_article_master_click    = "home_article_master_click"
        static let home_banner_giftbox_click    = "home_banner_giftbox_click"
        
        static let home_tab_click = "home_tab_click"
        static let home_sign_click = "home_sign_click"
    }
    
    
    ///Dock栏
    struct Dcok {
        
        /*
         dock_click  DOCK栏图标点击次数
         */
        
        static let dock_click = "dock_click"
    }
    
    
    ///选礼统计
    struct GiftChoice {
        /*
         selectgift_click	选礼分组
         selectgift_more_click	选礼分组－查看更多
         selectgift_grouptab1_click	选礼分组-礼物tab
         demo_test	选礼分组-攻略tab
         selectgift_choice_click	选礼分组-筛选
         selectgift_giftlist_click	选礼分组-礼物
         selectgift_articlelist_click	选礼分组-攻略
         
         1.3.60 +
         selectgift_who_click	选礼-送给谁
         selectgift_label_click	选礼-个性标签
         selectgift_sex_click	选礼-性别
         selectgift_old_click	选礼-年龄
         selectgift_constellation_click	选礼-星座
         selectgift_occasion_click	选礼-场合
         selectgift_finish_click	选礼-选好了
         selectgift_giftlist_click	选礼-商品列表
         selectgift_resulttab1_click	选礼-对象标签
         selectgift_resulttab2_click	选礼-个性标签
         selectgift_resulttab3_click	选礼-场合标签
         selectgift_resulttab4_click	选礼-预算标签
         */
        
        static let demo_test   = "demo_test"
        
        static let selectgift_click             = "selectgift_click"
        static let selectgift_more_click        = "selectgift_more_click"
        static let selectgift_choice_click      = "selectgift_choice_click"
        static let selectgift_grouptab1_click   = "selectgift_grouptab1_click"
        static let selectgift_articlelist_click = "selectgift_articlelist_click"
        
        static let selectgift_who_click         = "selectgift_who_click"
        static let selectgift_label_click       = "selectgift_label_click"
        static let selectgift_sex_click         = "selectgift_sex_click"
        static let selectgift_old_click         = "selectgift_old_click"
        static let selectgift_constellation_click   = "selectgift_constellation_click"
        static let selectgift_occasion_click    = "selectgift_occasion_click"
        static let selectgift_finish_click      = "selectgift_finish_click"
        static let selectgift_giftlist_click    = "selectgift_giftlist_click"
        static let selectgift_resulttab1_click  = "selectgift_resulttab1_click"
        static let selectgift_resulttab2_click  = "selectgift_resulttab2_click"
        static let selectgift_resulttab3_click  = "selectgift_resulttab3_click"
        static let selectgift_resulttab4_click  = "selectgift_resulttab4_click"
    }
    
    //服务
    struct Service {
        /*
         service_remind_click	服务-送礼提醒
         service_online_click	服务-在线礼物顾问
         service_package_click	服务-礼物包装
         service_online_who_click	服务-在线礼物顾问-送给谁
         service_online_occasion_click	服务-在线礼物顾问-场合
         service_online_price_click	服务-在线礼物顾问-预算
         service_online_select_click	服务-在线礼物顾问-选好了
         service_package_select_click	服务-礼物包装-选择
         */
        static let service_remind_click = "service_remind_click"
        static let service_online_click = "service_online_click"
        static let service_package_click = "service_package_click"
        static let service_online_who_click = "service_online_who_click"
        static let service_online_occasion_click = "service_online_occasion_click"
        static let service_online_price_click = "service_online_price_click"
        static let service_online_select_click = "service_online_select_click"
        static let service_package_select_click = "service_package_select_click"
    }
    
    ///我的统计
    struct Mine {
        
        /*
         my_birthwiki_click	我-我的生日百科
         my_order_click	我-我的订单
         my_coupon_click	我-我的红包
         my_like_click	我-我喜欢的
         my_address_click	我-收货地址
         my_comment_click	我-点个赞
         my_suggest_click	我-吐个槽
         my_contact_click	我-联系客服
         my_about_click	我-关于心意点点
         my_messagedetails_click	我-我的消息
         my_id_click	我-账户管理
         
         1.3.60 +
         my_shareapp_click	我-分享心意点点
         
         my_contact_online_click	我-在线客服
         my_contact_tel_click	我-电话客服
         
         my_sign_click	我-点击登陆
         my_settings_click	我-设置
         my_settings_id_click	我-设置-账户管理
         my_settings_address_click	我-设置-收货地址
         my_settings_comment_click	我-设置-给个好评
         my_settings_suggest_click	我-设置-直接向CEO吐槽
         my_settings_about_click	我-设置-关于心意点点
         */
        
        static let my_id_click          = "my_id_click"
        static let my_like_click        = "my_like_click"
        static let my_about_click       = "my_about_click"
        static let my_order_click       = "my_order_click"
        static let my_coupon_click      = "my_coupon_click"
        static let my_address_click     = "my_address_click"
        static let my_comment_click     = "my_comment_click"
        static let my_suggest_click     = "my_suggest_click"
        static let my_contact_click     = "my_contact_click"
        static let my_birthwiki_click   = "my_birthwiki_click"
        
        static let my_messagedetails_click = "my_messagedetails_click"
        
        static let my_shareapp_click    = "my_shareapp_click"
        
        static let my_contact_online_click = "my_contact_online_click"
        static let my_contact_tel_click = "my_contact_tel_click"
        static let my_sign_click = "my_sign_click"
        static let my_settings_click = "my_settings_click"
        static let my_settings_id_click = "my_settings_id_click"
        static let my_settings_address_click = "my_settings_address_click"
        static let my_settings_comment_click = "my_settings_comment_click"
        static let my_settings_suggest_click = "my_settings_suggest_click"
        static let my_settings_about_click = "my_settings_about_click"
    }
    
    
    
    ///攻略统计
    struct  Tractic {
        /*
         article_detail_goods_click	文章详情-相关商品
         article_detail_click	文章详情（所有入口进入）
         article_detail_like_click	文章详情-点赞
         article_detail_share_click	文章详情-分享
         article_detail_recommend_click	文章详情-相关推荐
         */
        static let article_detail_goods_click   = "article_detail_goods_click"
        static let article_detail_click         = "article_detail_click"
        static let article_detail_like_click    = "article_detail_like_click"
        static let article_detail_share_click   = "article_detail_share_click"
        static let article_detail_recommend_click = "article_detail_recommend_click"
    }
    
    
    ///商品统计
    struct Goods {
        /*
         goods_outside_detail_click
         goods_inside_detail_click
         goods_detail_click	商品详情（所有入口进入）
         goods_detail_cart_click	商品详情-加入礼物篮
         goods_detail_sent_click	商品详情-立即送
         goods_detail_like_click	商品详情-我喜欢
         goods_detail_share_click	商品详情-分享
         goods_detail_outside_click	商品详情-外链购买
         goods_detail_basket_click	商品详情-进入礼物篮
         
         1.3.60 +
         goods_detail_service_click	商品详情-联系客服
         goods_detail_argument_click	商品详情-礼品参数
         goods_detail_recommend_click	商品详情-相关推荐
         goods_detail_description_click 商品详情-服务说明
         */
        
        static let goods_outside_detail_click       = "goods_outside_detail_click"
        static let goods_inside_detail_click       = "goods_inside_detail_click"
        
        static let goods_detail_click       = "goods_detail_click"
        static let goods_detail_cart_click  = "goods_detail_cart_click"
        static let goods_detail_sent_click  = "goods_detail_sent_click"
        static let goods_detail_like_click  = "goods_detail_like_click"
        static let goods_detail_share_click = "goods_detail_share_click"
        
        static let goods_detail_outside_click   = "goods_detail_outside_click"
        static let goods_detail_basket_click    = "goods_detail_basket_click"
        
        static let goods_detail_service_click = "goods_detail_service_click"
        static let goods_detail_argument_click = "goods_detail_argument_click"
        static let goods_detail_recommend_click = "goods_detail_recommend_click"
        static let goods_detail_description_click = "goods_detail_description_click"
    }
    
    
    ///送礼提醒统计
    struct GiftRemind {
        /*
         remind_tab_click	提醒-4个Tab点击
         remind_holiday_click	提醒-节日详情
         remind_father_click	提醒-爸爸生日添加
         remind_monther_click	提醒-妈妈生日添加
         remind_new_click	提醒-点击加号
         remind_add_click	提醒-新增提醒
         remind_add_finish_click	提醒-新增提醒－保存
         remind_import_click	提醒-导入提醒
         remind_import_finish_click	提醒-导入提醒-导入
         remind_wish_click	提醒-送祝福
         remind_card_click	提醒-送贺卡
         remind_coupon_click	提醒-发红包
         remindshare	提醒-分享节日图
         remind_article_click	提醒-节日送礼攻略
         remind_flowerbutton_click	提醒-订购鲜花
         remind_giftbutton_click	提醒-礼物精选
         remind_giftlist_click	提醒-礼物列表
         
         1.3.60 +
         remind_sharepicture_click 提醒-分享节日图
         */
        
        static let remind_tab_click     = "remind_tab_click"
        static let remind_new_click     = "remind_new_click"
        static let remind_add_click     = "remind_add_click"
        static let remind_wish_click    = "remind_wish_click"
        static let remind_card_click    = "remind_card_click"
        static let remind_coupon_click  = "remind_coupon_click"
        static let remind_import_click  = "remind_import_click"
        static let remind_father_click  = "remind_father_click"
        static let remind_holiday_click = "remind_holiday_click"
        static let remind_monther_click = "remind_monther_click"
        
        static let remind_article_click         = "remind_article_click"
        static let remind_giftlist_click        = "remind_giftlist_click"
        static let remind_add_finish_click      = "remind_add_finish_click"
        static let remind_giftbutton_click      = "remind_giftbutton_click"
        static let remind_flowerbutton_click    = "remind_flowerbutton_click"
        static let remindshare    = "remindshare"
        static let remind_import_finish_click   = "remind_import_finish_click"
        
        
        static let remind_sharepicture_click = "remind_sharepicture_click"
        
    }
    
    
    ///搜索和礼物篮
    struct  SearchAndGiftBasket {
        /*
         search_key_click	搜索-关键词
         basket_selectgift_click	礼物篮为空-挑礼物按钮
         basket_like_click	礼物篮为空-我喜欢的按钮
         basket_recommend_gift_click	礼物篮-推荐礼物
         basket_account_click	礼物篮-去结算按钮
         basket_delete_gift_click	礼物篮－左滑删除礼物
         basket_move_to_favor_click	礼物篮－左滑移至喜欢
         basket_move_all_to_favor_click	编辑礼物篮－批量喜欢
         basket_delete_all_click	编辑礼物篮－批量删除
         */
        static let search_key_click         = "search_key_click"
        static let basket_like_click        = "basket_like_click"
        static let basket_account_click     = "basket_account_click"
        static let basket_selectgift_click  = "basket_selectgift_click"
        static let basket_delete_all_click  = "basket_delete_all_click"
        static let basket_delete_gift_click = "basket_delete_gift_click"
        
        static let basket_move_to_favor_click       = "basket_move_to_favor_click"
        static let basket_recommend_gift_click      = "basket_recommend_gift_click"
        static let basket_move_all_to_favor_click   = "basket_move_all_to_favor_click"
    }
    
    
    ///订单
    struct Order {
        /*
         order_service_click	填写订单-点击选择心意服务
         order_service_box_click	订单-心意服务－选择礼盒
         order_service_card_click	订单-心意服务－选择贺卡
         order_service_record_click	订单-心意服务－点击录制语音
         order_service_finish_click	订单－心意服务－点击确定保存
         order_submit_click	订单-提交订单
         */
        
        static let order_submit_click       = "order_submit_click"
        static let order_service_click      = "order_service_click"
        static let order_service_box_click  = "order_service_box_click"
        static let order_service_card_click = "order_service_card_click"
        
        static let order_service_record_click   = "order_service_record_click"
        static let order_service_finish_click   = "order_service_finish_click"
    }
    
    
    ///支付
    struct Pay{
        /*
         pay_payment_click	支付－选择支付方式
         pay_recommendgift_click	支付完成－推荐礼物
         pay_goon_click	支付完成－继续逛逛按钮
         pay_takereceipt_click	支付完成－索要发票按钮
         pay_takereceipt_finish_click	索要发票－完成按钮
         
         */
        
        static let pay_goon_click       = "pay_goon_click"
        static let pay_payment_click    = "pay_payment_click"
        
        static let pay_takereceipt_click    = "pay_takereceipt_click"
        static let pay_recommendgift_click  = "pay_recommendgift_click"
        
        static let pay_takereceipt_finish_click = "pay_takereceipt_finish_click"
    }
    
    struct Answers {
        /*
         answers_tab1_click	问答社区-最近
         answers_tab2_click 	问答社区-最热
         answers_mytab1_click	问答社区-我问的
         answers_mytab2_click	问答社区-我查看过的
         answers_dock1_click	问答社区-大家问
         answers_dock2_click	问答社区-我的问答
         answers_addquestion_click	问答社区-提问
         answers_giftlist_click	问答社区-回答中的商品
         answers_openanswer_click	问答社区-显示全部
         answers_offanswer_click	问答社区-收起
         answers_like_click	问答社区-点赞
        */
        
        static let answers_tab1_click       = "answers_tab1_click"
        static let answers_tab2_click       = "answers_tab2_click"
        static let answers_mytab1_click     = "answers_mytab1_click"
        static let answers_mytab2_click     = "answers_mytab2_click"
        static let answers_dock1_click      = "answers_dock1_click"
        static let answers_dock2_click      = "answers_dock2_click"
        static let answers_addquestion_click = "answers_addquestion_click"
        static let answers_giftlist_click   = "answers_giftlist_click"
        static let answers_openanswer_click = "answers_openanswer_click"
        static let answers_offanswer_click  = "answers_offanswer_click"
        static let answers_like_click       = "answers_like_click"
    }
    
    //推送统计
    struct Other {
        
        static let tuisong_ios_daodashu = "tuisong_ios_daodashu"
        
        static let tuisong_ios = "tuisong_ios"
    }
    
}
