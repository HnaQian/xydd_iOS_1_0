//
//  Utils.swift
//  Limi
//
//  Created by 米志强 on 15/9/19.
//  Copyright © 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit
import CoreData

class Utils: NSObject {
    
    /**
     url重组 ,忽略url中原有的参数，使用新的传进来的参数，然后拼接成新的url返回
     
     - parameter url:       urlString
     - parameter parameter: 新的参数
     
     - returns: 新的[NSURL new]
     */
    class func recombinationUrl(_ url:String,parameter:[String:AnyObject]) ->URL?
    {
        var urlStr = ""
        
        if let u = URL(string: url){
            
            var keyValues = [String]()
            
            urlStr = u.scheme! + "://" + u.host! + u.path + "?"
            
            for (key,value) in parameter
            {
                keyValues.append("\(key)=\(value)")
            }
            
            if keyValues.count > 0{
                for index in 0...keyValues.count - 1{
                    
                    if index == keyValues.count - 1{
                        urlStr  = urlStr + keyValues[index]
                    }else{
                        urlStr  = urlStr + keyValues[index] + "&"
                    }
                }
            }
        }
        
        return URL(string: urlStr)
    }
    
    
    /**
     * 给定的版本号是否大于等于当前的版本号
     *
     */
    ///注：version的格式：1.3.5，不能超过6位数字,例如：1.2.3.4.5.6.7(wrong!) 1.2.3.4.5.6(true!)
    class func versionGreatThanCurrentVersion(_ version:String)->Bool{
        
        //当前版本
        let currentVersion = (Bundle.main.infoDictionary! as [String: AnyObject])["CFBundleShortVersionString"] as! String
        print(currentVersion)
        var numCurrentVersion = currentVersion.replacingOccurrences(of: ".", with: "")
        print(numCurrentVersion)
        if numCurrentVersion.characters.count < 6{
            for _ in numCurrentVersion.characters.count..<6{
                numCurrentVersion = numCurrentVersion.appending("0")
            }
        }
        
        //给定的版本
        var numVersion = version.replacingOccurrences(of: ".", with: "")
        numVersion = NSString(string: numVersion).replacingOccurrences(of: " ", with: "")
        print(numVersion)
        if numVersion.characters.count < 6{
            for _ in numVersion.characters.count..<6{
                numVersion = numVersion.appending("0")
            }
        }
        
        return numVersion.intValue >= numCurrentVersion.intValue
    }
    
    
    class func imageWithColor(_ Color : UIColor) ->UIImage {
        let rect :CGRect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0)
        UIGraphicsBeginImageContext(rect.size)
        let context : CGContext = UIGraphicsGetCurrentContext()!
        
        
        context.setFillColor(Color.cgColor)
        context.fill(rect);
        
        let image : UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
        
    }
    
    
    class func detectionTelphoneSimple(_ str : String?) ->String{
        
        var mstr = self.preCheckWord(str)
        if mstr == NULL_STRING{return ""}
        
        mstr = mstr.replacingOccurrences(of: "-", with:"")
        mstr = mstr.replacingOccurrences(of: " ", with:"")
        
        if mstr.hasPrefix("17951"){
            mstr = (mstr as NSString).substring(from: 5)
        }
        else if mstr.hasPrefix("0086"){
            mstr = (mstr as NSString).substring(from: 4)
        }
        else if mstr.hasPrefix("+86"){
            mstr = (mstr as NSString).substring(from: 3)
        }
        else if mstr.hasPrefix("19389"){
            mstr = (mstr as NSString).substring(from: 5)
        }
        else if mstr.hasPrefix("12593"){
            mstr = (mstr as NSString).substring(from: 5)
        }
        else if mstr.hasPrefix("17911"){
            mstr = (mstr as NSString).substring(from: 5)
        }
        else if mstr.hasPrefix("17901"){
            mstr = (mstr as NSString).substring(from: 5)
        }
        else if mstr.hasPrefix("10193"){
            mstr = (mstr as NSString).substring(from: 5)
        }
        else if mstr.hasPrefix("86"){
            mstr = (mstr as NSString).substring(from: 2)
        }
        let whitespace = CharacterSet.whitespacesAndNewlines
        mstr = mstr.trimmingCharacters(in: whitespace)
        return mstr;
        
    }
    
    class func isValiableMobile(phoneNumber:String) ->Bool {
        let mobile = phoneNumber.replacingOccurrences(of: " ", with: "")
        if mobile.characters.count != 11 {
            return false
        }else {
            let cm_num = "[1][3-8]\\d{9}"
            
            let pred1 = NSPredicate(format: "SELF MATCHES  %@", cm_num)
            let isMatch1 = pred1.evaluate(with: mobile)
            
            if isMatch1  {
                return true
            }else {
                return false
            }
        }
        return false
    }
    
    class func showError(context:UIView,errorStr : String,delay: Double = 2.0){
        let hud : ProgressHUD = ProgressHUD(view: context)
        context.addSubview(hud)
        hud.mode = .text
        hud.detailsLabel.text = errorStr
        hud.detailsLabel.numberOfLines = 2
        hud.show(true)
        hud.hide(true, afterDelay:delay)
    }
    
    class func imageResize(_ imageObj:UIImage, sizeChange:CGSize)-> UIImage {
        
        let hasAlpha = false
        let scale: CGFloat = 0.0 // Automatically use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        imageObj.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext() // !!!
        return scaledImage!
    }
}


extension Utils
{
    
    class func  subLastCharacter(_ words:String?) -> String
    {
        let mwords = self.preCheckWord(words)
        
        if mwords == NULL_STRING{return NULL_STRING}
        
        let startIndex = mwords.characters.index(mwords.startIndex, offsetBy: mwords.characters.count - 1) //swift 2.0+
        let endIndex = mwords.characters.index(mwords.endIndex, offsetBy: -mwords.characters.count + 1) //swift 2.0+
        
        //纯汉字 取纯汉字的最后一个字
        if self.checkChinese(mwords)
        {
            return mwords.substring(from: startIndex)
        }else{
            
            /*
             非纯汉字开头的取第一个字符，字母强制转化成大写，汉字和数字则直接显示
             
             若取到的字符非字母/汉字/数字，设置为缺省的默认头像
             
             */
            let target = mwords.substring(to: endIndex)
            
            //数字 或者 汉字
            if (self.checkNumber(target) || self.checkChinese(target))
            {
                return target
            }
            
            
            //英文
            if (self.checkEng(target))
            {
                //如果是小写英文
                if( self.checkSmallEng(target))
                {
                    return  target.uppercased()
                }
                
                return target;
            }
        }
        
        return NULL_STRING
    }
    
    
    
    ///判断是纯数字 检测长度为1-19位
    class func checkNumber(_ words:String)-> Bool
    {
        let mwords = self.preCheckWord(words)
        
        if mwords == NULL_STRING{return false}
        
        let check = "^[0-9]{1,19}$"
        
        do {
            
            let regex = try NSRegularExpression(pattern: check, options: NSRegularExpression.Options.caseInsensitive)
            
            let res = regex.matches(in: mwords, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, words.characters.count))
            
            for checkingRes in res
            {
                if mwords == (mwords as NSString).substring(with: checkingRes.range){return  true}
            }
        }
        catch {
            print(error)
        }
        
        return false
    }
    
    ///判断是纯中文 检测长度为1-19位
    class func checkChinese(_ words:String?)-> Bool
    {
        let mwords = self.preCheckWord(words)
        
        if mwords == NULL_STRING{return false}
        
        let check = "^[\\u4e00-\\u9fa5]{1,19}$"
        
        do {
            
            let regex = try NSRegularExpression(pattern: check, options: NSRegularExpression.Options.caseInsensitive)
            
            let res = regex.matches(in: mwords, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, mwords.characters.count))
            
            for checkingRes in res
            {
                if mwords == (mwords as NSString).substring(with: checkingRes.range){return  true}
            }
        }
        catch {
            print(error)
        }
        
        return false
    }
    
    
    
    ///判断是纯小写英文 检测长度为1-19位
    class func checkSmallEng(_ words:String?)-> Bool
    {
        let mwords = self.preCheckWord(words)
        
        if mwords == NULL_STRING{return false}
        
        let check = "^[a-z]{1,19}$"
        
        do {
            
            let regex = try NSRegularExpression(pattern: check, options: NSRegularExpression.Options.caseInsensitive)
            
            let res = regex.matches(in: mwords, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, mwords.characters.count))
            
            for checkingRes in res {
                
                if mwords == (mwords as NSString).substring(with: checkingRes.range){return  true}
                
            }
        }
        catch {
            print(error)
        }
        
        return false
    }
    
    ///判断是纯英文 检测长度为1-19位
    class func checkEng(_ words:String?)-> Bool
    {
        let mwords = self.preCheckWord(words)
        //NULL_STRING
        if mwords == NULL_STRING{return false}
        
        let check = "^[A-Za-z]{1,19}$"
        
        do {
            
            let regex = try NSRegularExpression(pattern: check, options: NSRegularExpression.Options.caseInsensitive)
            
            let res = regex.matches(in: mwords, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, mwords.characters.count))
            
            for checkingRes in res
            {
                if mwords == (mwords as NSString).substring(with: checkingRes.range){return  true}
            }
        }
        catch {
            print(error)
        }
        
        return false
    }
    
    
    ///检查密码
    class func checPassWord(_ words :String?)-> Bool
    {
        let mwords = self.preCheckWord(words)
        
        if mwords == NULL_STRING{return false}
        
        let check = "^[a-zA-Z0-9]{6,20}+$"
        
        do {
            
            let regex = try NSRegularExpression(pattern: check, options: NSRegularExpression.Options.caseInsensitive)
            
            let res = regex.matches(in: mwords, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, mwords.characters.count))
            
            for checkingRes in res
            {
                if mwords == (mwords as NSString).substring(with: checkingRes.range){return  true}
            }
        }
        catch {
            print(error)
        }
        
        return false
    }
    
    ///检查姓名
    class func checkName(_ words:String?)->Bool
    {
        let mwords = self.preCheckWord(words)
        
        if mwords == NULL_STRING{return false}
        
        let check = "^[\\u4e00-\\u9fa5A-Za-z]{2,16}$"
        
        do {
            
            let regex = try NSRegularExpression(pattern: check, options: NSRegularExpression.Options.caseInsensitive)
            
            let res = regex.matches(in: mwords, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, mwords.characters.count))
            
            for checkingRes in res
            {
                if mwords == (mwords as NSString).substring(with: checkingRes.range){return  true}
            }
        }
        catch {
            print(error)
        }
        
        return false
    }
    
    ///检查昵称
    class func checkNiName(_ words:String?) ->Bool{
        
        let mwords = self.preCheckWord(words)
        
        if mwords == NULL_STRING{return false}
        
        let check = "^[a-zA-Z\\d\\_\\-\\u2E80-\\u9FFF]{0,16}$"
        
        
        do {
            
            let regex = try NSRegularExpression(pattern: check, options: NSRegularExpression.Options.caseInsensitive)
            
            let res = regex.matches(in: mwords, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, mwords.characters.count))
            
            for checkingRes in res
            {
                if mwords == (mwords as NSString).substring(with: checkingRes.range){return  true}
            }
        }
        catch {
            print(error)
        }
        
        return false
    }
    
    ///过滤传进来的字符是否可以进行检查操作
    class func preCheckWord(_ words:String?) ->String
    {
        if words == nil
        {
            return NULL_STRING
        }
        if words?.characters.count == 0
        {
            return NULL_STRING
        }
        
        return words!
    }
}



extension Utils
{
    class func htmalTrusletToText(_ words:String?) -> String
    {
        var mwords = self.preCheckWord(words)
        
        if mwords == NULL_STRING{return ""}
        
        mwords = NSString(string: mwords).replacingOccurrences(of: "&#39", with: "")
        mwords = NSString(string: mwords).replacingOccurrences(of: "<p>", with: "")
        mwords = NSString(string: mwords).replacingOccurrences(of: "</p>", with: "")
        
        return mwords
    }
    
    
    class func containsString(_ words:String?,inString string:String) -> Bool
    {
        let mwords = self.preCheckWord(words)
        if mwords == NULL_STRING{return false}
        
        let mstring = self.preCheckWord(string)
        if mstring == NULL_STRING{return false}
        
        let contentLenth = NSString(string: mstring).length
        let mlenth = NSString(string: mwords).length
        
        var isContain = false
        
        for i in 0 ... contentLenth - mlenth
        {
            let subStr = NSString(string: mstring).substring(with: NSMakeRange(i, mlenth))
            
            if mwords == subStr
            {
                isContain = true
                
                break
            }
        }
        
        return isContain
    }
    
    
    /**
     计算价格的小数点位数：最多两位
     */
    class func decimalCount(_ price:String?) -> Int
    {
        let mprice = self.preCheckWord(price)
        
        if mprice == NULL_STRING {return 0}
        
        var count:Int = 0
        
        var priceCharAry:[String] = NSString(string: mprice).components(separatedBy: ".")
        
        if priceCharAry.count == 1{
            return 0
        }
        else if priceCharAry.count == 2{
            
            if (priceCharAry[1].characters.count < 2){
                
                priceCharAry[1] = NSString(string: priceCharAry[1]).appending("0")
            }
        }else if priceCharAry.count > 2
        {
            fatalError("price字符中只能存在一个小数点")
        }
        
        let decimalString:NSString = NSString(string: priceCharAry[1]).substring(to: 2) as NSString
        let decimalTenStr:NSString = decimalString.substring(to: 1) as NSString
        let decimalHaStr:NSString = decimalString.substring(from: 1) as NSString
        let decimalTen = decimalTenStr.intValue
        let decimalha = decimalHaStr.intValue
        
        if (decimalTen == 0 && decimalha == 0){
            count = 0
        }
        else if (decimalTen > 0 && decimalha == 0){
            count = 1
        }else if decimalha > 0
        {
            count = 2
        }
        
        return count
    }
    
    
    
    class func updateViewControllersOfNavViewController(_ navigationVC:UINavigationController?,exceptVCs:[UIViewController.Type]){
        
        if let nav = navigationVC{
            var newViewControllers = [UIViewController]()
            
            for VC in nav.viewControllers{
                var isExcept = false
                for type in exceptVCs{
                    if  VC.isKind(of: type) == true{
                        isExcept = true
                    }
                }
                if !isExcept{newViewControllers.append(VC)}
            }
            
            nav.viewControllers = newViewControllers
            newViewControllers.last?.hidesBottomBarWhenPushed = true
        }
    }
    
    
    /**
     联系客服
     
     - parameter contentView: 提示窗位置
     */
    class func callServicer(_ contentView:UIView)
    {
        let device = NSString(string: UIDevice.current.model).substring(to: 4)
        
        if device != "iPho"{
            Utils.showError(context: contentView, errorStr: "对不起！该设备无电话功能")
            
            return
        }
        //        let request: NSFetchRequest = NSFetchRequest(entityName: "SettingInfo")
        //
        //        let datasource : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(request)!
        //        var phone : String = "021-68411255"
        //        if datasource.count > 0{
        //
        //            let settingInfo = datasource[0] as! SettingInfo
        //            phone = settingInfo.service_phone!
        //
        //        }
        
        
        let phoneURL:URL = URL(string: "tel:" + SystemInfoRequestManager.shareInstance().service_number)!
        
        let requestURL = Foundation.URLRequest(url: phoneURL)
        
        let webview = UIWebView(frame: CGRect.zero)
        
        webview.loadRequest(requestURL)
        
        contentView.addSubview(webview)
    }
    
    class func scaleImage(_ imageUrl:String,width:CGFloat,height:CGFloat) ->String{
        return imageUrl + "?imageView2/0/w/" + "\(Int(width))" + "/h/" + "\(Int(height))"
    }
    
    
    ///保留到double的有效小数位
    class func checkDecimal(_ market_price:Double) -> String{
        
        if market_price > 0{
            let floatPrice = roundf(Float(market_price))
            if floatPrice == Float(market_price){
                return String(format: "￥%.0f", market_price)
            }
            else{
                let priceStr1 : NSString = String(format: "%.2f", market_price) as NSString
                let priceStr2 : NSString = String(format: "%.1f", market_price) as NSString
                let floatPrice1 : Float = priceStr1.floatValue
                let floatPrice2 : Float = priceStr2.floatValue
                if floatPrice1 == floatPrice2{
                    return String(format: "￥%.1f", market_price)
                }
                else{
                    return String(format: "￥%.2f", market_price)
                }
            }
        }
        else{
            return "￥0"
        }
        
        //        let str = NSString(string: String(format: "%f", price))
        //        var index = 0
        //
        //        for var i = str.length - 1 ; i >= 0; i = i - 1{
        //
        //            let char = str.substringWithRange(NSMakeRange(i, 1))
        //
        //            if char != "0"{
        //                if char == "."{
        //                    index = i - 1
        //                }else{index = i}
        //
        //                break
        //            }
        //        }
        //
        //        if index == 0{return ""}
        //
        //        return "￥" + str.substringToIndex(index + 1)
    }
}


/*
 
 
 */
/**
 判断代理是否实现代理函数
 
 - parameter delegate: 代理
 - parameter selector: 代理函数
 - parameter success:  实现的自定义操作，可缺
 
 - returns: 代理为空或者代理函数未实现都将返回false
 */
func Delegate_Selector(_ delegate:NSObjectProtocol?,_ selector:Selector,psuccess success:(()->Void)! = nil) -> Bool
{
    if let del = delegate
    {
        if del.responds(to: selector) == true
        {
            if let suc = success
            {
                suc()
            }
            return true
            
        }else
        {
            print("\n[Delegate_Selector] = false,如果确定delegate已经实现所需代理函数，please：\n1.检查selector的拼写中是否漏掉':' \n例如:Delegate_Selector(delegate,selector:\" didSelected: \"),拼写成:Delegate_Selector(delegate,selector:\" didSelected \") ,\n2.两个参数以上的拼写规则:\nfunc addressDidDeleteSuccess(requestManager:LMRequestManager,result:LMResultMode,mark:String) \nselector = \"addressDidDeleteSuccess:result:mark:\"\n")
        }
    }
    return false
}


func Target_Selector(_ target:AnyObject?,_ selector:Selector,psuccess success:(()->Void)! = nil) -> Bool
{
    if let del = target
    {
        if del.responds(to: selector) == true
        {
            if let suc = success
            {
                suc()
            }
            return true
            
        }
    }
    return false
}



//内测控制中心
class DevelopTestManager: NSObject {
    
    //添加提示语
    static func refreshDevelopeTipe(){
        
        if Constant.appSwitchKey == 0{
            if let window  = UIApplication.shared.keyWindow{
                window.addSubview(developTipeLbl)
            }
            
            developTipeLbl.font = Constant.Theme.Font_17
            developTipeLbl.text = Constant.httpSwitchKey == 1 ? "[正式]":"[测试]"
            developTipeLbl.textColor = Constant.httpSwitchKey == 1 ? UIColor.blue : UIColor.orange
        }else{
            
            if developTipeLbl.superview != nil{
                developTipeLbl.removeFromSuperview()
            }
        }
    }
    
    
    //正/测服务器切换按钮 此按钮放在app中的：关于心意点点(AboutViewController.swift) 页面中
    static func getSwitchBtn() ->UIButton?{
        
        var btn:SwitchButton?
        
        struct SigleInstance {
            static var btn:SwitchButton?
        }
        
        if Constant.appSwitchKey == 0{
            btn = SigleInstance.btn == nil ? SwitchButton() : SigleInstance.btn!
        }
        
        btn?.checkTipe()
        
        return btn
    }
    
    
    
    //提示语
    fileprivate static let developTipeLbl:UILabel = {
        let lbl = UILabel(frame:CGRect(x: 0,y: 0,width: 120,height: 30))
        lbl.ix = 20
        lbl.iy = Constant.ScreenSizeV2.SCREEN_HEIGHT - 79
        return lbl
    }()
    
    
    
    fileprivate class SwitchButton: UIButton {
        
        fileprivate let tipeLbl:UILabel = {
            let lbl = UILabel(frame:CGRect(x: 0,y: 0,width: Constant.ScreenSizeV2.SCREEN_WIDTH*2/3,height: 40))
            lbl.iy = 44
            lbl.numberOfLines = 0
            lbl.text = "为避免因数据混乱而造成不可预知的Bug,请在修改之后,把APP杀掉再次进入"
//            lbl.textAlignment = NSTextAlignment.Center
            lbl.font = Constant.Theme.Font_13
            lbl.textColor = UIColor.red
            return lbl
        }()
        
        convenience init() {
            self.init(frame: CGRect(x: 0,y: 0,width: Constant.ScreenSizeV2.SCREEN_WIDTH*4/6 ,height: 44))
            self.iy = Constant.ScreenSizeV2.SCREEN_HEIGHT - 240
            self.ix = Constant.ScreenSizeV2.SCREEN_WIDTH/6
            self.layer.cornerRadius = 3
            
//            self.titleEdgeInsets.bottom = 6
//            self.titleEdgeInsets.top = -6
            
            self.addSubview(tipeLbl)
            
            checkTipe()
        }
        
        override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
            Constant.setHttpSwitchKey(Constant.httpSwitchKey == 1 ? 0 : 1)
            DevelopTestManager.refreshDevelopeTipe()
            checkTipe()
        }
        
        func checkTipe(){
            self.setTitle(Constant.httpSwitchKey == 1 ? "点击切换至 [测试] 服务器":"点击切换至 [正式] 服务器", for: UIControlState())
            self.backgroundColor = Constant.httpSwitchKey == 1 ? UIColor.blue : UIColor.orange
        }
    }
}
