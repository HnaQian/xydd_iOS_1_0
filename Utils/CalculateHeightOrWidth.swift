//
//  CalculateHeightOrWidth.swift
//  Limi
//
//  Created by 千云锋 on 16/9/7.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//计算label或者button高度或者宽度

import Foundation


class CalculateHeightOrWidth: NSObject {
    
    //计算高度
    class func getLabOrBtnHeigh(_ labelStr:NSString, font:UIFont, width:CGFloat) -> CGFloat {
        
        let statusLabelText: NSString = labelStr
        let option:NSStringDrawingOptions = .usesLineFragmentOrigin
        let size = CGSize(width: width, height: 0)
        let strSize = statusLabelText.boundingRect(with: size, options: option, attributes: [NSFontAttributeName:font], context: nil).size
        return strSize.height
        
    }
    //计算宽度
    class func getLabOrBtnWidth(_ labelStr:NSString,font:UIFont,height:CGFloat) -> CGFloat {
        
        let statusLabelText: NSString = labelStr
        let size = CGSize(width: 900, height: height)
        let dic = NSDictionary(object: font, forKey: NSFontAttributeName as NSCopying)
        let strSize = statusLabelText.boundingRect(with: size, options: .usesLineFragmentOrigin, attributes: dic as? [String : AnyObject], context: nil).size
        return strSize.width + 4
        
    }

    
}
