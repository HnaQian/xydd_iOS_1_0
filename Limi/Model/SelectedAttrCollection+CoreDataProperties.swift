//
//  SelectedAttrCollection+CoreDataProperties.swift
//  Limi
//
//  Created by ZhiQiangMi on 16/3/10.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension SelectedAttrCollection {

    @NSManaged var attrItemIds: String?
    @NSManaged var goodsNum: String?
    @NSManaged var id: String?
    @NSManaged var price: String?
    @NSManaged var cart: BasketItem?

}
