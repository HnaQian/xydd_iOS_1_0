//
//  AccountManager.swift
//  Limi
//
//  Created by 倪晅 on 16/1/13.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit
import CoreData

class AccountManager: NSObject {
    
    private static var __once: () = { AccountManager.Static.instance = AccountManager() }()
    struct Static {
        static var instance : AccountManager? = nil
        static var token : Int = 0
    }
    class var shared: AccountManager {
        get {
            _ = AccountManager.__once
            
            return Static.instance!
        }
    }

    func isLogin() -> Bool {
        return UserDefaults.standard.string(forKey: "UserToken") != "" && UserDefaults.standard.string(forKey: "UserToken") != nil
    }
    
    func isEverLaunched() -> Bool {
        return UserDefaults.standard.bool(forKey: "everLaunched") == true
    }
    
    func isFirstTapReminderBar() -> Bool {
        let key = "isFirstTapReminderBar"
        let isTapped = UserDefaults.standard.bool(forKey: key)
        if !isTapped {
            UserDefaults.standard.set(true, forKey: key)
        }
        UserDefaults.standard.synchronize()
        return !isTapped
    }
    
    func isDadAdd() -> Bool {
        return UserDefaults.standard.bool(forKey: "father_status")
    }
    
    func isMontherAdd() -> Bool {
        return UserDefaults.standard.bool(forKey: "mother_status")
    }
    
    func setDadAdd(_ state : Bool){
        UserDefaults.standard.set(state, forKey: "father_status")
        UserDefaults.standard.synchronize()
    }
    
    func setMontherAdd(_ state : Bool){
        UserDefaults.standard.set(state, forKey: "mother_status")
        UserDefaults.standard.synchronize()
    }
    
    func removeParentReminder(){
        UserDefaults.standard.removeObject(forKey: "father_status")
        UserDefaults.standard.removeObject(forKey: "mother_status")
        UserDefaults.standard.set(false, forKey: "MotherIsHadDelete")
        UserDefaults.standard.set(false, forKey: "FatherIsHadDelete")
        UserDefaults.standard.synchronize()
    }
    
    func hasGender() -> Bool {
        let request: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        let datasource : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(request)!
        if datasource.count > 0 {
            let userInfo = datasource[0] as! User
            return userInfo.gender > 0
        }
        return false
    }
    
    func setGender(_ gender: Int64, successHandler:(()->Void)? = nil, failureHandler:(()->Void)? = nil) {
        let request: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        let datasource : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(request)!
        if datasource.count > 0{
            let userInfo = datasource[0] as! User
            userInfo.gender = gender
            CoreDataManager.shared.save(successHandler, failureHandler: failureHandler)
        } else {
            let entity = NSEntityDescription.entity(forEntityName: "User", in: CoreDataManager.shared.managedObjectContext)
            request.entity = entity
            do {
                try! CoreDataManager.shared.managedObjectContext.fetch(request)
                let userInfo: User = NSEntityDescription.insertNewObject(forEntityName: "User", into: CoreDataManager.shared.managedObjectContext) as! User
                userInfo.gender = gender
                CoreDataManager.shared.save(successHandler, failureHandler: failureHandler)
            }
        }
    }
    
    func getGender() -> Int64 {
        let request: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        let datasource : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(request)!
        if datasource.count > 0{
            let userInfo = datasource[0] as! User
            return userInfo.gender
        }
        return 0
    }
    
    func setBirthday(_ dateType: Int64, date: String, successHandler:(()->Void)? = nil, failureHandler:(()->Void)? = nil) {
        let request: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        let datasource : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(request)!
        if datasource.count > 0{
            let userInfo = datasource[0] as! User
            userInfo.birthday_type = dateType
            userInfo.birthday = date
            CoreDataManager.shared.save(successHandler, failureHandler: failureHandler)
        } else {
            let entity = NSEntityDescription.entity(forEntityName: "User", in: CoreDataManager.shared.managedObjectContext)
            request.entity = entity
            do {
                try! CoreDataManager.shared.managedObjectContext.fetch(request)
                let userInfo: User = NSEntityDescription.insertNewObject(forEntityName: "User", into: CoreDataManager.shared.managedObjectContext) as! User
                userInfo.birthday_type = dateType
                userInfo.birthday = date
                CoreDataManager.shared.save(successHandler, failureHandler: failureHandler)
            }
        }
    }
    
}
