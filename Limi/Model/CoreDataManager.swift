//
//  CoreDataManager.swift
//  Limi
//
//  Created by ZhiQiangMi on 15/10/20.
//  Copyright © 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit
import CoreData

class CoreDataManager: NSObject {
    
    struct Static {
        static var instance : CoreDataManager? = nil
        static var token : Int = 0
    }
    
    private static var __once: () = { CoreDataManager.Static.instance = CoreDataManager() }()
    
    let kStoreName = "Mind.sqlite"
    let kStoreNameNew = "Limi.sqlite"
    let kModmName = "Limi"
    
    var _managedObjectContext: NSManagedObjectContext?
    var _managedObjectModel: NSManagedObjectModel?
    var _persistentStoreCoordinator: NSPersistentStoreCoordinator?
    
    class var shared:CoreDataManager{
        get {
            _ = CoreDataManager.__once
            
            return Static.instance!
        }
    }
    
    /**
     Initializes CoreData Manager. Basically, creates the first NSManagedObjectContext
     
     - parameter style: The style of the bicycle
     - parameter gearing: The gearing of the bicycle
     - parameter handlebar: The handlebar of the bicycle
     - parameter centimeters: The frame size of the bicycle, in centimeters
     
     - returns: nothing.
     */
    
    func initialize(){
        self.managedObjectContext
    }
    
    // MARK: Core Data stack
    
    var managedObjectContext: NSManagedObjectContext{
        
        if Thread.isMainThread {
            
            if (_managedObjectContext == nil) {
                if let coordinator = self.persistentStoreCoordinator  {
                    _managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
                    _managedObjectContext!.persistentStoreCoordinator = coordinator
                }
                
                return _managedObjectContext!
            }
            
        }else{
            
            var threadContext : NSManagedObjectContext? = Thread.current.threadDictionary["NSManagedObjectContext"] as? NSManagedObjectContext;
            
            if threadContext == nil {
                threadContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
                threadContext!.parent = _managedObjectContext
                threadContext!.name = Thread.current.description
                
                Thread.current.threadDictionary["NSManagedObjectContext"] = threadContext
                
                NotificationCenter.default.addObserver(self, selector:#selector(CoreDataManager.contextWillSave(_:)) , name: NSNotification.Name.NSManagedObjectContextWillSave, object: threadContext)
                
            }else{
                print("using old context")
            }
            return threadContext!;
        }
        
        return _managedObjectContext!
    }
    
    
    
    // Returns the managed object model for the application.
    // If the model doesn't already exist, it is created from the application's model.
    var managedObjectModel: NSManagedObjectModel {
        if _managedObjectModel == nil {
            let modelURL = Bundle.main.url(forResource: kModmName, withExtension: "momd")
            _managedObjectModel = NSManagedObjectModel(contentsOf: modelURL!)
        }
        return _managedObjectModel!
    }
    
    
    // Returns the persistent store coordinator for the application.
    // If the coordinator doesn't already exist, it is created and the application's store added to it.
    var persistentStoreCoordinator: NSPersistentStoreCoordinator? {
        if _persistentStoreCoordinator == nil {
            let storeURL = self.applicationDocumentsDirectory.appendingPathComponent(kStoreName)
            let storeURLNew = self.applicationDocumentsDirectory.appendingPathComponent(kStoreNameNew)
            _persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
            if FileManager.default.fileExists(atPath: storeURL.path){
                do{
                    try FileManager.default.removeItem(atPath: storeURL.path)}catch{
                        
                }
                do {
                    
                    try _persistentStoreCoordinator!.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeURLNew, options: self.databaseOptions())
                } catch let error as NSError {
                    print(error.domain)
                }
            }
            else{
                do {
                    
                    try _persistentStoreCoordinator!.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeURLNew, options: self.databaseOptions())
                } catch let error as NSError {
                    print(error.domain)
                }
            }
            
            
        }
        return _persistentStoreCoordinator!
    }
    
    
    
    // MARK: fetches
    
    func executeFetchRequest(_ request:NSFetchRequest<NSFetchRequestResult>)-> Array<AnyObject>?{
        
        var results:Array<AnyObject>?
        self.managedObjectContext.performAndWait{
            var fetchError:NSError?
            do {
                results = try self.managedObjectContext.fetch(request)
            } catch let error as NSError {
                fetchError = error
                results = nil
            } catch {
                fatalError()
            }
            
            if let error = fetchError {
                print("Warning!! \(error.description)")
            }
        }
        return results
        
    }
    
    
    func executeFetchRequest(_ request:NSFetchRequest<NSFetchRequestResult>, completionHandler:@escaping (_ results: Array<AnyObject>?) -> Void)-> (){
        
        self.managedObjectContext.perform{
            var fetchError:NSError?
            var results:Array<AnyObject>?
            do {
                results = try self.managedObjectContext.fetch(request)
            } catch let error as NSError {
                fetchError = error
                results = nil
            } catch {
                fatalError()
            }
            
            if let error = fetchError {
                print("Warning!! \(error.description)")
            }
            
            completionHandler(results)
        }
        
    }
    
    ///获取表 如果数据表不存在，自动创建一个新表
    /**
    entityType      :目标数据表
    
    sholdReBuild    :是否强制重建(不管存在不存在) 可不传 不传默认不强制创建新表 不可传nil
    */
    ///注：返回值可能为 nil
    func getDBManager(_ entityType : Constant.CoreDataType,sholdReBuild:Bool! = false)-> NSManagedObject?
    {
        let entityName = Constant.coreDataTableEntityNames[entityType.rawValue]
        
        let request: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        
        let entity = NSEntityDescription.entity(forEntityName: entityName, in: self.managedObjectContext)
        
        request.entity = entity
        
        var results:Array<AnyObject>?
        
        var manager:NSManagedObject!
        do{
            results = try self.managedObjectContext.fetch(request)
            
            //如果强制重建
            if sholdReBuild == true{
                
                manager = NSEntityDescription.insertNewObject(forEntityName: entityName, into: CoreDataManager.shared.managedObjectContext)
                
            }else{
                
                if let mags = results{
                    
                    if mags.count > 0{
                        
                        for managedObject in results!{
                            manager = managedObject as! NSManagedObject
                        }
                        
                    }else{
                        
                        manager = NSEntityDescription.insertNewObject(forEntityName: entityName, into: CoreDataManager.shared.managedObjectContext)
                    }
                    
                }else{
                    
                    manager = NSEntityDescription.insertNewObject(forEntityName: entityName, into: CoreDataManager.shared.managedObjectContext)
                }
            }
            
        }catch let error as NSError {
            
            print("Warning!! \(error.description)")
            
        }catch{fatalError()}
        
        return manager
    }
    
    
    ///更新数据
    /**
    newData             :目标数据
    
    entityType          :插入数据的目标数据表
    
    shouldReBuild       :是否需要重建表 可不传 不传默认不重建 不可传nil
    
    successHandler      :成功的回调  可不传 可传nil
    
    failureHandler      :失败的回调  可不传 可传nil
    */
    ///注:如果数据表不存在会自动新建表
    func update(_ newData : JSON,entityType : Constant.CoreDataType,shouldReBuild : Bool! = false ,successHandler:(()->Void)? = nil,failureHandler:(()->Void)? = nil){
        
        let entityName = Constant.coreDataTableEntityNames[entityType.rawValue]
        
        let request: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        
        let entity = NSEntityDescription.entity(forEntityName: entityName, in: self.managedObjectContext)
        
        request.entity = entity
        
        var fetchError:NSError?
        
        var results:Array<AnyObject>?
        
        var manager:NSManagedObject!
        
        do {
            
            results = try self.managedObjectContext.fetch(request)
            
            //如需重建 删除旧表创建新表
            if shouldReBuild == true{
                
                for managedObject in results!{
                    self.managedObjectContext.delete(managedObject as! NSManagedObject)
                }
                
                manager = NSEntityDescription.insertNewObject(forEntityName: entityName, into: CoreDataManager.shared.managedObjectContext)
                
            }else{
                
                //如果表不存在，新建表
                if let mags = results{
                    
                    if mags.count > 0{
                        for managedObject in results!{
                            manager = managedObject as! NSManagedObject
                        }
                        
                    }else{
                        manager = NSEntityDescription.insertNewObject(forEntityName: entityName, into: CoreDataManager.shared.managedObjectContext)
                    }
                    
                }else{
                    manager = NSEntityDescription.insertNewObject(forEntityName: entityName, into: CoreDataManager.shared.managedObjectContext)
                }
            }
            
            
            if manager == nil
            {
                if failureHandler != nil
                {
                    failureHandler!()
                }
                
                return
            }
            
            switch entityType{
                case Constant.CoreDataType.userInfoType://用户信息表
                
                let user: User = manager as! User
                if let avatar = newData["avatar"].string{
                    user.avatar = avatar
                }
                if let birthday = newData["birthday"].string{
                    user.birthday = birthday
                }
                if let birthday_type = newData["birthday_type"].int{
                    user.birthday_type = Int64(birthday_type)
                }
                if let birthday_wiki = newData["birthday_wiki"].string{
                    user.birthday_wiki = birthday_wiki
                }
                if let coupon_num = newData["coupon_num"].int{
                    user.coupon_num = Int64(coupon_num)
                }
                if let gender = newData["gender"].int{
                    user.gender = Int64(gender)
                }
                if let has_password = newData["has_password"].bool{
                    user.has_password = has_password
                }
                if let nick_name = newData["nick_name"].string{
                    user.nick_name = nick_name
                }
                if let order_num = newData["order_num"].int{
                    user.order_num = Int64(order_num)
                }
                if let user_name = newData["user_name"].string{
                    user.user_name = user_name
                }
                if let point = newData["point"].int {
                    user.point = Int64(point)
                }
                
                if let openArr : [JSON] = newData["user_open"].array{
                    for i in 0 ..< openArr.count
                    {
                        let open : JSON = openArr[i]
                        if open["Type"].intValue == 2{
                            user.qq_bind_status = true
                            if let id = open["Id"].string {
                                user.qq_bind_id = id
                            }
                        }
                        else if open["Type"].intValue == 3{
                            user.wx_bind_status = true
                            if let id = open["Id"].string {
                                user.wx_bind_id = id
                            }
                        }
                    }
                }
                
                
            case Constant.CoreDataType.userAddressType://用户地址表
                print("用户地址")
                
                
            case Constant.CoreDataType.homeAAGiftChoiceType://首页和选礼数据缓存表
                print("首页和选礼")
                
            default:
                break
            }
            self.save({Void in
                if successHandler != nil
                {
                    successHandler!()
                }},failureHandler:{Void in
                    if failureHandler != nil
                    {
                        failureHandler!()
                    }})
        } catch let error as NSError {
            if failureHandler != nil
            {
                failureHandler!()
            }
            fetchError = error
            results = nil
        } catch {
            if failureHandler != nil
            {
                failureHandler!()
            }
            fatalError()
        }
        
        if let error = fetchError {
            print("Warning!! \(error.description)")
        }
    }
    
    // #pragma mark - save methods
    
    func save(_ successHandler:(()->Void)? = nil,failureHandler:(()->Void)? = nil) {
        
        let context:NSManagedObjectContext = self.managedObjectContext;
        if context.hasChanges {
            //           context.performBlock{
            context.performAndWait{
                
                var saveError:NSError?
                let saved: Bool
                do {
                    try context.save()
                    saved = true
                } catch let error as NSError {
                    saveError = error
                    saved = false
                } catch {
                    fatalError()
                }
                
                if !saved {
                    if failureHandler != nil
                    {
                        failureHandler!()
                    }
                    if let error = saveError{
                        print("Warning!! Saving error \(error.description)")
                    }
                }
                else{
                    if successHandler != nil
                    {
                        successHandler!()
                    }
                }
                
                if context.parent != nil {
                    //                  context.parentContext!.performBlock{
                    context.parent!.performAndWait{
                        var saveError:NSError?
                        let saved: Bool
                        do {
                            try context.parent!.save()
                            saved = true
                        } catch let error as NSError {
                            saveError = error
                            saved = false
                        } catch {
                            fatalError()
                        }
                        
                        if !saved{
                            if failureHandler != nil
                            {
                                failureHandler!()
                            }
                            if let error = saveError{
                                print("Warning!! Saving parent error \(error.description)")
                            }
                        }
                        else{
                            if successHandler != nil
                            {
                                successHandler!()
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    
    
    
    func contextWillSave(_ notification:Notification){
        
        let context : NSManagedObjectContext! = notification.object as! NSManagedObjectContext
        let insertedObjects : NSSet = context.insertedObjects as NSSet
        
        if insertedObjects.count != 0 {
            var obtainError:NSError?
            
            do {
                try context.obtainPermanentIDs(for: insertedObjects.allObjects as! [NSManagedObject])
            } catch let error as NSError {
                obtainError = error
            }
            if let error = obtainError {
                print("Warning!! obtaining ids error \(error.description)")
            }
        }
        
    }
    
    
    // #pragma mark - Utilities
    
    
    func deleteEntity(_ object:NSManagedObject)-> () {
        object.managedObjectContext! .delete(object)
    }
    
    
    
    // #pragma mark - Application's Documents directory
    
    // Returns the URL to the application's Documents directory.
    var applicationDocumentsDirectory: URL {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.endIndex-1] as URL
    }
    
    
    func databaseOptions() -> Dictionary <String,Bool> {
        var options =  Dictionary<String,Bool>()
        options[NSMigratePersistentStoresAutomaticallyOption] = true
        options[NSInferMappingModelAutomaticallyOption] = true
        return options
    }
}

