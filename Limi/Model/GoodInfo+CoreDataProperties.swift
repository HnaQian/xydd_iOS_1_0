//
//  GoodInfo+CoreDataProperties.swift
//  Limi
//
//  Created by ZhiQiangMi on 16/3/10.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension GoodInfo {

    @NSManaged var discountPrice: String?
    @NSManaged var gid: String?
    @NSManaged var goodsImage: String?
    @NSManaged var goodsName: String?
    @NSManaged var totalNum: Int64
    @NSManaged var cart: BasketItem?

}
