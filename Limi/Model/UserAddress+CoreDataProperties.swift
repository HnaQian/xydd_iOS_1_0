//
//  UserAddress+CoreDataProperties.swift
//  Limi
//
//  Created by ZhiQiangMi on 16/3/7.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension UserAddress {

    @NSManaged var address: String?
    @NSManaged var addressId: Int64
    @NSManaged var area: String?
    @NSManaged var area_id: Int64
    @NSManaged var city: String?
    @NSManaged var city_id: Int64
    @NSManaged var is_default: Bool
    @NSManaged var mobile: String?
    @NSManaged var name: String?
    @NSManaged var province: String?
    @NSManaged var province_id: Int64

}
