//
//  HomeAAGiftChoice+CoreDataProperties.swift
//  Limi
//
//  Created by ZhiQiangMi on 16/3/7.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension HomeAAGiftChoice {

    @NSManaged var giftChoiceLayout: NSObject?
    @NSManaged var homeLayout: NSObject?
    @NSManaged var homeRmind: NSObject?
    
}
