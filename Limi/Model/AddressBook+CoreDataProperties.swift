//
//  AddressBook+CoreDataProperties.swift
//  Limi
//
//  Created by 千云锋 on 16/8/21.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension AddressBook {

    @NSManaged var birthday: String?
    @NSManaged var name: String?
    @NSManaged var phoneNumber: String?
    @NSManaged var style: String?
    @NSManaged var birthdayStyle: String?

}
