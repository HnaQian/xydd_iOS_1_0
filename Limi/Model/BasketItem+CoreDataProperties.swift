//
//  BasketItem+CoreDataProperties.swift
//  Limi
//
//  Created by ZhiQiangMi on 16/3/10.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension BasketItem {

    @NSManaged var attributeStr: String?
    @NSManaged var insertDate: TimeInterval
    @NSManaged var planCount: Int64
    @NSManaged var goodInfo: GoodInfo?
    @NSManaged var selectedAttrCollection: SelectedAttrCollection?

}
