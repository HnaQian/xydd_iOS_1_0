//
//  User+CoreDataProperties.swift
//  Limi
//
//  Created by maohs on 16/6/1.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension User {

    @NSManaged var avatar: String?
    @NSManaged var avatarData: Data?
    @NSManaged var birthday: String?
    @NSManaged var birthday_type: Int64
    @NSManaged var birthday_wiki: String?
    @NSManaged var coupon_num: Int64
    @NSManaged var gender: Int64
    @NSManaged var has_password: Bool
    @NSManaged var nick_name: String?
    @NSManaged var order_num: Int64
    @NSManaged var qq_bind_status: Bool
    @NSManaged var qq_bind_id: String?
    @NSManaged var qq_head_image_url: String?
    @NSManaged var qq_nickName: String?
    @NSManaged var user_name: String?
    @NSManaged var wx_bind_status: Bool
    @NSManaged var wx_bind_id: String?
    @NSManaged var wx_head_image_url: String?
    @NSManaged var wx_nickName: String?
    @NSManaged var point: Int64

}
