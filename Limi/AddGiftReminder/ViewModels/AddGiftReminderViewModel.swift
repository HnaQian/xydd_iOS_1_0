//
//  AddGiftReminderViewModel.swift
//  Limi
//
//  Created by 倪晅 on 16/1/8.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

enum AddGiftReminderSituation: NSInteger {
    case none = 0
    case birthday = 1
    case marriageDay
    case loveDay
    case singleMarriageDay
    case singleMoveHouseDay
    case singleChildDay
    case firstMonth
    case singleVisitDay
    case companyDay
    case singlePromptDay
    case singleGraduationDay
    
    func isAnniversary() -> Bool {
        if (self == AddGiftReminderSituation.marriageDay ||
            self == AddGiftReminderSituation.loveDay ||
            self == AddGiftReminderSituation.companyDay
            ) {
                return true
        }
        return false
    }
}

enum AddGiftReminderRelation: NSInteger {
    case none = 0
    case father = 1
    case mother
    case friend
    case schollMate
    case colleague
    case client
    case partner
    case boss
    case teacher
    case brotherSister
    case relative
    case eldership
    case child
    case wife
    case girlFriend
    case husband
    case boyFriend
}

enum AddGiftReminderGender: NSInteger {
    case unknown = 0
    case male
    case female
}

enum AddGiftReminderNotification: NSInteger {
    case currentDay = 0
    case preOneDay = 1
    case preThreeDay = 3
    case preOneWeek = 7
    case preTwoWeek = 14
    case preOneMonth = 30
}

class AddGiftReminderViewModel: NSObject {
    
    var isForUpdate: Bool = false
    var id: NSInteger = 0
    
    var serverData: NSDictionary? {
        didSet {
            self.allSituations = self.serverData!["scene"] as? [String: String]
            self.allRelations = self.serverData!["relation"] as? [String: String]
            self.relationGender = self.serverData!["relation_gender"] as? [String:  [Int]]
            self.sceneFrequency = self.serverData!["scene_frequency"] as? [String:  Int]
            self.sceneGenderRelationList = self.serverData!["scene_gender_relation_list"] as? [String:  [String: [Int]]]
            self.sceneViewList = self.serverData!["scene_view_list"] as? [String:  [String: [String: AnyObject]]]
        }
    }
    
    var situation: Observable<NSInteger> = Observable(0)
    var relation: Observable<NSInteger> = Observable(0)
    var gender: Observable<NSInteger> = Observable(1)
    var notifications: [NSInteger] = [AddGiftReminderNotification.currentDay.rawValue, AddGiftReminderNotification.preOneWeek.rawValue]
    var name: Observable<String> = Observable("")
    var mobile: Observable<String> = Observable("")
    var avatar: String = ""
    var date: String = ""
    var displayDate: Observable<String> = Observable("")
    var dateType: Int32 = 1
    
    var allSituations: [String: String]?
    var allRelations: [String: String]?
    var relationGender: [String: [Int]]?
    var sceneFrequency: [String: Int]?
    var sceneGenderRelationList: [String:  [String: [Int]]]?
    var sceneViewList: [String:  [String: [String: AnyObject]]]?
    
    func getAllSituationsForTotast() -> [[String: AnyObject]] {
        var situations = [[String: AnyObject]]()
        for (situationId, situationName) in self.allSituations! {
            situations.append([situationId: situationName as AnyObject])
        }
        return situations
    }
    
    func getAllRelationsForTotast() -> [[String: AnyObject]] {
        var relations = [[String: AnyObject]]()
        let situation = String(self.situation.value)
        let userGender = String(AccountManager.shared.getGender())
        let relationIds = self.sceneGenderRelationList![situation]![userGender]! as [Int]
        relationIds.forEach { (relationId) -> () in
            let relationName = self.getRelationNameFor(relationId)
            relations.append([String(relationId): relationName as AnyObject])
        }
        return relations
    }
    
    func getSituationNameFor(_ enumName: NSInteger) -> String {
        return self.allSituations![String(enumName)]!
    }
    
    func getRelationNameFor(_ enumName: NSInteger) -> String {
        return self.allRelations![String(enumName)]!
    }

}
