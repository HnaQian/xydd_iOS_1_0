//
//  AddGiftReminderHeaderView.swift
//  Limi
//
//  Created by 倪晅 on 16/1/8.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class AddGiftReminderHeaderView: UIView {
    
    fileprivate var _avatarImage: UIImageView?
    fileprivate var _pickAvatarButton: UIButton?
    
    var avatarImage: UIImageView {
        get {
            if _avatarImage == nil {
                _avatarImage = UIImageView(image: UIImage(named: "add_gift_reminder_camera"))
                _avatarImage!.layer.cornerRadius = 38
                _avatarImage!.layer.masksToBounds = true
                _avatarImage!.layer.borderWidth = 0.5
                _avatarImage!.layer.borderColor = UIColor(rgba: "#EAEAEA").cgColor
            }
            return _avatarImage!
        }
    }
    
    var pickAvatarButton: UIButton {
        get {
            if _pickAvatarButton == nil {
                _pickAvatarButton = UIButton(type: .custom)
            }
            return _pickAvatarButton!
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor(rgba: "#F2F3F5")
        
        self.addSubview(self.avatarImage)
        self.addSubview(self.pickAvatarButton)
        
        self.avatarImage.snp_makeConstraints { (make) -> Void in
            let _ = make.center.equalTo(self)
            let _ = make.width.height.equalTo(76)
        }
        self.pickAvatarButton.snp_makeConstraints { (make) -> Void in
            let _ = make.edges.equalTo(self.avatarImage)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
