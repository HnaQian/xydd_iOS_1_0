//
//  AddGiftReminderGenderCell.swift
//  Limi
//
//  Created by 倪晅 on 16/1/8.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class AddGiftReminderGenderCell: AddGiftReminderBaseCell {
    
    fileprivate var _maleButton: UIButton?
    fileprivate var _femaleButton: UIButton?
    fileprivate var _unknownButton: UIButton?
    
    var maleButton: UIButton {
        get {
            if _maleButton == nil {
                _maleButton = self.createGenderButton("男")
            }
            return _maleButton!
        }
    }
    
    var femaleButton: UIButton {
        get {
            if _femaleButton == nil {
                _femaleButton = self.createGenderButton("女")
            }
            return _femaleButton!
        }
    }
    
    var unknownButton: UIButton {
        get {
            if _unknownButton == nil {
                _unknownButton = UIButton(type: .custom)
                _unknownButton = self.createGenderButton("未知")
            }
            return _unknownButton!
        }
    }
    
    override var reminderViewModel: AddGiftReminderViewModel? {
        didSet {
            print(reminderViewModel!.gender.value)
//            if reminderViewModel!.gender.value != AddGiftReminderGender.Unknown.rawValue {
                self.contentView.isHidden = false
                self.setup(withGender: reminderViewModel!.gender.value)
//            }
            let _ = reminderViewModel!.gender.afterChange += { (_, newValue) -> () in
                self.contentView.isHidden = false
                self.setup(withGender: newValue)
            }
        }
    }

    required init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.contentView.isHidden = true
        
        self.titleText = "性别"
        self.cellName = AddGiftReminderCellType.gender.cellName()
        self.rightView.addSubview(self.maleButton)
        self.rightView.addSubview(self.femaleButton)
        self.rightView.addSubview(self.unknownButton)
        
        self.maleButton.snp_makeConstraints { (make) -> Void in
            let _ = make.left.centerY.equalTo(self.rightView)
            let _ = make.width.equalTo(50)
            let _ = make.height.equalTo(30)
        }
        self.femaleButton.snp_makeConstraints { (make) -> Void in
            let _ = make.left.equalTo(self.maleButton.snp_right).offset(30 * Constant.ScreenSize.SCREEN_WIDTH_SCALE)
            let _ = make.centerY.equalTo(self.rightView)
            let _ = make.width.equalTo(50)
            let _ = make.height.equalTo(30)
        }
        self.unknownButton.snp_makeConstraints { (make) -> Void in
            let _ = make.left.equalTo(self.femaleButton.snp_right).offset(30 * Constant.ScreenSize.SCREEN_WIDTH_SCALE)
            let _ = make.centerY.equalTo(self.rightView)
            let _ = make.width.equalTo(60)
            let _ = make.height.equalTo(30)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func createGenderButton(_ title: String) -> UIButton {
        let button: UIButton = UIButton(type: .custom)
        button.contentHorizontalAlignment = .left
        button.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0)
        button.setTitle(title, for: UIControlState())
        button.setTitleColor(Constant.Theme.Color6, for: UIControlState())
        button.titleLabel?.font = Constant.Theme.Font_15
        button.setImage(UIImage(named: "noChoose"), for: UIControlState())
        button.addTarget(self, action: #selector(AddGiftReminderGenderCell.didTapGenderButton(_:)), for: .touchUpInside)
        
        return button
    }
    
    func didTapGenderButton(_ sender: UIButton) {
        switch sender {
        case self.maleButton:
            self.reminderViewModel!.gender <- AddGiftReminderGender.male.rawValue
        case self.femaleButton:
            self.reminderViewModel!.gender <- AddGiftReminderGender.female.rawValue
        case self.unknownButton:
            self.reminderViewModel!.gender <- AddGiftReminderGender.unknown.rawValue
        default:
            break
        }
    }
    
    func setup(withGender gender: NSInteger?) {
        if gender != nil {
            let maleImageName: String = gender == AddGiftReminderGender.male.rawValue ? "add_gift_reminder_choose" : "add_gift_reminder_unchoose"
            let femaleImageName: String = gender == AddGiftReminderGender.female.rawValue ? "add_gift_reminder_choose" : "add_gift_reminder_unchoose"
            let unknownImageName: String = gender == AddGiftReminderGender.unknown.rawValue ? "add_gift_reminder_choose" : "add_gift_reminder_unchoose"
            self.maleButton.setImage(UIImage(named: maleImageName), for: UIControlState())
            self.femaleButton.setImage(UIImage(named: femaleImageName), for: UIControlState())
            self.unknownButton.setImage(UIImage(named: unknownImageName), for: UIControlState())
        }
    }

}

