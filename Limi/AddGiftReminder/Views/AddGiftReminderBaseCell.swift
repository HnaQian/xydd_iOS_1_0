//
//  AddGiftReminderBaseCell.swift
//  Limi
//
//  Created by 倪晅 on 16/1/8.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

protocol AddGiftReminderBaseCellDelegate: BaseTableCellDelegate, AnyObject {
    func didTapSetSituation()
    func didTapSetRelation()
    func didTapSetDate()
    func didTapAddressBook()
}

class AddGiftReminderBaseCell: BaseTableCell {
    
    fileprivate var _titleLabel: UILabel?
    fileprivate var _rightView: UIView?
    fileprivate var _setButton: UIButton?
    fileprivate var _borderBottom: UIView?
    
    var cellName: String?
    
    var realDelegate: AddGiftReminderBaseCellDelegate?
    var reminderViewModel: AddGiftReminderViewModel? {
        didSet {
            let situation = reminderViewModel!.situation.value
            if situation > 0 {
                if self.cellName != nil {
                    let dict = self.reminderViewModel!.sceneViewList![String(situation)]![self.cellName!]
                    self.titleText = dict!["title"] as? String
                }
            }
            let _ = reminderViewModel!.situation.afterChange += { (_, newValue) -> () in
                if self.cellName != nil {
                    self.contentView.isHidden = false
                    let dict = self.reminderViewModel!.sceneViewList![String(newValue)]![self.cellName!]
                    self.titleText = dict!["title"] as? String
                }
            }
        }
    }

    override var delegate: BaseTableCellDelegate? {
        didSet {
            self.realDelegate = delegate as? AddGiftReminderBaseCellDelegate
        }
    }
    
    var titleText: String? {
        didSet {
            self.titleLabel.text = titleText
        }
    }
    
    var titleLabel: UILabel {
        get {
            if _titleLabel == nil {
                _titleLabel = UILabel()
                _titleLabel!.textColor = UIColor(rgba: "555555")
                _titleLabel!.font = UIFont.systemFont(ofSize: 15)
                _titleLabel!.textAlignment = .right
            }
            return _titleLabel!
        }
    }
    
    var setButton: UIButton {
        get {
            if _setButton == nil {
                _setButton = UIButton(type: .custom)
                _setButton!.setTitle("点击设置", for: UIControlState())
                _setButton!.setTitleColor(UIColor.lightGray, for: UIControlState())
                _setButton!.setTitleColor(Constant.Theme.Color2, for: .selected)
                _setButton!.titleLabel?.font = UIFont.systemFont(ofSize: 15)
                _setButton!.contentHorizontalAlignment = .left
            }
            return _setButton!
        }
    }
    
    var rightView: UIView {
        get {
            if _rightView == nil {
                _rightView = UIView()
            }
            return _rightView!
        }
    }
    
    var borderBottom: UIView {
        get {
            if _borderBottom == nil {
                _borderBottom = UIView()
                _borderBottom!.backgroundColor = UIColor(rgba: "#DEDEDE")
            }
            return _borderBottom!
        }
    }

    required init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
                
        self.contentView.addSubview(self.titleLabel)
        self.contentView.addSubview(self.rightView)
        self.contentView.addSubview(self.borderBottom)
        
        
        self.titleLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.left.equalTo(self.contentView)//.offset(15)
            let _ = make.centerY.equalTo(self.contentView)
            let _ = make.width.equalTo(82)
        }
        self.rightView.snp_makeConstraints { (make) -> Void in
            if Constant.DeviceType.IS_IPHONE_6P {
                let _ = make.left.equalTo(self.titleLabel.snp_right).offset(30)
            } else {
                let _ = make.left.equalTo(self.titleLabel.snp_right).offset(15)
            }
            let _ = make.top.bottom.equalTo(self.contentView)
            let _ = make.right.equalTo(self.contentView).offset(-15)
        }
        self.borderBottom.snp_makeConstraints { (make) -> Void in
            let _ = make.left.equalTo(self.contentView).offset(20)
            let _ = make.right.bottom.equalTo(self.contentView)
            let _ = make.height.equalTo(0.5)
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    


}
