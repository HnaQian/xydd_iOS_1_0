//
//  AddGiftReminderRelationCell.swift
//  Limi
//
//  Created by 倪晅 on 16/1/8.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class AddGiftReminderRelationCell: AddGiftReminderBaseCell {
    
    override var reminderViewModel: AddGiftReminderViewModel? {
        didSet {
            if reminderViewModel!.relation.value != 0 {
                self.setup(withRelation: self.reminderViewModel?.relation.value)
            }
            reminderViewModel!.relation.afterChange += { (_, newValue) -> () in
                self.setup(withRelation: newValue)
            }
        }
    }

    required init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.titleText = "TA是我的"
        self.cellName = AddGiftReminderCellType.relation.cellName()
        
        self.rightView.addSubview(self.setButton)
        self.setButton.snp_makeConstraints { (make) -> Void in
            let _ = make.left.right.centerY.equalTo(self.rightView)
        }
        
        self.setButton.addTarget(self.realDelegate, action: Selector(("didTapSetRelation")), for: .touchUpInside)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(withRelation relation: NSInteger?) {
        if relation != nil {
            self.setButton.isSelected = true
            self.setButton.setTitle(self.reminderViewModel?.getRelationNameFor(relation!), for: UIControlState())
        }
    }

}
