//
//  AddGiftReminderRecipient Cell.swift
//  Limi
//
//  Created by 倪晅 on 16/1/8.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class AddGiftReminderRecipientCell: AddGiftReminderBaseCell {
    
    fileprivate var _nameField: UITextField?
    fileprivate var _contactButton: UIButton?
    
    var nameField: UITextField {
        get {
            if _nameField == nil {
                _nameField = UITextField()
                _nameField!.delegate = self
                _nameField!.clearButtonMode = UITextFieldViewMode.whileEditing
                _nameField!.font = UIFont.systemFont(ofSize: 15)
                _nameField!.attributedPlaceholder = NSAttributedString(string: "请输入名字", attributes: [
                    NSForegroundColorAttributeName: UIColor.lightGray
                    ])
            }
            return _nameField!
        }
    }
    
    var contactButton: UIButton {
        get {
            if _contactButton == nil {
                _contactButton = UIButton(type: .custom)
                _contactButton!.setBackgroundImage(UIImage(named: "add_gift_reminder_contact"), for: UIControlState())
                _contactButton!.addTarget(self.realDelegate, action: Selector(("didTapAddressBook")), for: .touchUpInside)
            }
            return _contactButton!
        }
    }
    
    override var reminderViewModel: AddGiftReminderViewModel? {
        didSet {
            if reminderViewModel!.name.value != "" {
                self.nameField.text = reminderViewModel!.name.value
            }
            let _ = reminderViewModel!.name.afterChange += { (_, newValue) -> () in
                self.nameField.text = newValue
            }
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    required init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.titleText = "收礼人"
        self.cellName = AddGiftReminderCellType.recipient.cellName()
        
        self.rightView.addSubview(self.nameField)
        self.rightView.addSubview(self.contactButton)
        
        self.nameField.snp_makeConstraints { (make) -> Void in
            let _ = make.left.centerY.equalTo(self.rightView)
            let _ = make.right.equalTo(self.contactButton.snp_left).offset(-15)
        }
        self.contactButton.snp_makeConstraints { (make) -> Void in
            let _ = make.right.centerY.equalTo(self.rightView)
            let _ = make.width.equalTo(22)
            let _ = make.height.equalTo(23)
        }
    }

}

extension AddGiftReminderRecipientCell: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.reminderViewModel!.name <- textField.text!
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
