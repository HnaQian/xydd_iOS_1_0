//
//  AddGiftReminderDateCell.swift
//  Limi
//
//  Created by 倪晅 on 16/1/8.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class AddGiftReminderDateCell: AddGiftReminderBaseCell {
    
    override var reminderViewModel: AddGiftReminderViewModel? {
        didSet {
            if reminderViewModel!.date != "" {
                self.setButton.isSelected = true
                if reminderViewModel?.dateType == 1{
                self.setButton.setTitle(reminderViewModel!.displayDate.value, for: UIControlState())
                }
                else{
                    if (self.reminderViewModel!.date as NSString).substring(to: 4) == "1600"{
                        self.setButton.setTitle(reminderViewModel!.displayDate.value, for: UIControlState())
                    }
                    else{
                        let formatter = DateFormatter()
                        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                        let endDate: Date? = formatter.date(from: reminderViewModel!.date)
                        let time = formatter.string(from: endDate!)
                        let time2 = CalendarUtil.alTolunarYear(time)
                        
                        self.setButton.setTitle(time2, for: UIControlState())
                    }
                }
            }
            let _ = reminderViewModel!.displayDate.afterChange += { (_, newValue) -> () in
                self.setButton.isSelected = true
                print(newValue)
                self.setButton.setTitle(newValue, for: UIControlState())
            }
        }
    }
    
    required init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.titleText = "赠送时间"
        self.cellName = AddGiftReminderCellType.date.cellName()
        
        self.rightView.addSubview(self.setButton)
        self.setButton.snp_makeConstraints { (make) -> Void in
            let _ = make.left.right.centerY.equalTo(self.rightView)
        }
        self.borderBottom.snp_updateConstraints { (make) -> Void in
            let _ = make.left.equalTo(self.contentView)
        }
        
        self.setButton.addTarget(self.realDelegate, action: Selector(("didTapSetDate")), for: .touchUpInside)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
