//
//  AddGiftReminderDatePickerView.swift
//  Limi
//
//  Created by 倪晅 on 16/1/9.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class AddGiftReminderDatePickerView: DatePickerView {

    var reminderViewModel: AddGiftReminderViewModel?
    
    var defaultDate: Date {
        get {
            var date = Date()
            if self.reminderViewModel!.situation.value == AddGiftReminderSituation.birthday.rawValue {
                date = NSDate(year: 1980, month: 1, day: 1) as Date
            }
            if AddGiftReminderSituation(rawValue: self.reminderViewModel!.situation.value)!.isAnniversary() {
                date = NSDate(year: 2010, month: 1, day: 1) as Date
            }
            return date
        }
    }
    
    init(frame: CGRect, type: Bool, yearStart: NSInteger, yearEnd: NSInteger, toolBarHeight: CGFloat, pickerHeight: CGFloat, viewModel: AddGiftReminderViewModel) {
        self.reminderViewModel = viewModel
        var currentDate = Date()
        if self.reminderViewModel!.date != "" {
            let str = (self.reminderViewModel?.date)!
            currentDate = NSDate(string: str, formatString: "YYYY-MM-dd hh:mm:ss") as Date
        } else {
            if self.reminderViewModel!.situation.value == AddGiftReminderSituation.birthday.rawValue {
                currentDate = NSDate(year: 1980, month: 1, day: 1) as Date
            }
            if AddGiftReminderSituation(rawValue: self.reminderViewModel!.situation.value)!.isAnniversary() {
                currentDate = NSDate(year: 2010, month: 1, day: 1) as Date
            }
        }
        super.init(frame: CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: 300), type: self.reminderViewModel!.dateType == 2, display: currentDate, yearStart: 1905, yearEnd: yearEnd,toolBarHeight:Constant.TOOLBAR_HEIGHT,pickerHeight:Constant.PICKER_HEIGHT,isGuideBirthdayView:false)
        
        let situation = self.reminderViewModel!.situation.value
        if situation > 0 {
            let situationViewList = self.reminderViewModel!.sceneViewList![String(situation)]
            let hideYear = situationViewList![AddGiftReminderCellType.date.cellName()]!["hide_year"] as! Bool
            self.hideYearButton.isHidden = !hideYear
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
