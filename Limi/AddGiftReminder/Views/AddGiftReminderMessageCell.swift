//
//  AddGiftReminderMessageCell.swift
//  Limi
//
//  Created by 倪晅 on 16/1/9.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class AddGiftReminderMessageCell: AddGiftReminderBaseCell {

    fileprivate var _phoneField: UITextField?
    
    var phoneField: UITextField {
        get {
            if _phoneField == nil {
                _phoneField = UITextField()
                _phoneField!.placeholder = "填写后可发送祝福短信"
                _phoneField!.delegate = self
                _phoneField!.clearButtonMode = UITextFieldViewMode.whileEditing
                _phoneField!.keyboardType = UIKeyboardType.numberPad
                _phoneField!.font = UIFont.systemFont(ofSize: 15)
                _phoneField!.attributedPlaceholder = NSAttributedString(string: "请输入手机号码", attributes: [
                    NSForegroundColorAttributeName: UIColor.lightGray
                    ])
            }
            return _phoneField!
        }
    }
    
    override var reminderViewModel: AddGiftReminderViewModel? {
        didSet {
            if reminderViewModel!.mobile.value != "" {
                self.phoneField.text = reminderViewModel!.mobile.value
            }
            let _ = reminderViewModel!.mobile.afterChange += { (_, newValue) -> () in
                self.phoneField.text = newValue
            }
        }
    }
    
    required init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.titleText = "手机号码"
        self.cellName = AddGiftReminderCellType.message.cellName()
        
        self.rightView.addSubview(self.phoneField)
        
        self.phoneField.snp_makeConstraints { (make) -> Void in
            let _ = make.left.centerY.equalTo(self.rightView)
            let _ = make.right.equalTo(self.rightView).offset(-20)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}

extension AddGiftReminderMessageCell: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.reminderViewModel!.mobile <- textField.text!
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
