//
//  AddGiftReminderNotificationCell.swift
//  Limi
//
//  Created by 倪晅 on 16/1/9.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class AddGiftReminderNotificationCell: AddGiftReminderBaseCell {
    
    fileprivate var _currentDayButton: UIButton?
    fileprivate var _preOneDayButton: UIButton?
    fileprivate var _preThreeDayButton: UIButton?
    fileprivate var _preOneWeekButton: UIButton?
    fileprivate var _preTwoWeekButton: UIButton?
    fileprivate var _preOneMonthButton: UIButton?
    
    override var reminderViewModel: AddGiftReminderViewModel? {
        didSet {
            for subView in self.rightView.subviews {
                let tag = subView.tag
                let button = subView as? UIButton
                if button != nil && reminderViewModel!.notifications.index(of: tag) != nil {
                    button!.isSelected = true
                    button!.layer.borderColor = Constant.Theme.Color1.cgColor
                }
            }
        }
    }
    
    var currentDayButton: UIButton {
        get {
            if _currentDayButton == nil {
                _currentDayButton = self.createNoticeButton("当天")
                _currentDayButton!.tag = AddGiftReminderNotification.currentDay.rawValue
            }
            return _currentDayButton!
        }
    }
    
    var preOneDayButton: UIButton {
        get {
            if _preOneDayButton == nil {
                _preOneDayButton = self.createNoticeButton("提前一天")
                _preOneDayButton!.tag = AddGiftReminderNotification.preOneDay.rawValue
            }
            return _preOneDayButton!
        }
    }
    
    var preThreeDayButton: UIButton {
        get {
            if _preThreeDayButton == nil {
                _preThreeDayButton = self.createNoticeButton("提前三天")
                _preThreeDayButton!.tag = AddGiftReminderNotification.preThreeDay.rawValue
            }
            return _preThreeDayButton!
        }
    }
    
    var preOneWeekButton: UIButton {
        get {
            if _preOneWeekButton == nil {
                _preOneWeekButton = self.createNoticeButton("提前一周")
                _preOneWeekButton!.tag = AddGiftReminderNotification.preOneWeek.rawValue
            }
            return _preOneWeekButton!
        }
    }
    
    var preTwoWeekButton: UIButton {
        get {
            if _preTwoWeekButton == nil {
                _preTwoWeekButton = self.createNoticeButton("提前两周")
                _preTwoWeekButton!.tag = AddGiftReminderNotification.preTwoWeek.rawValue
            }
            return _preTwoWeekButton!
        }
    }
    
    var preOneMonthButton: UIButton {
        get {
            if _preOneMonthButton == nil {
                _preOneMonthButton = self.createNoticeButton("提前一个月")
                _preOneMonthButton!.tag = AddGiftReminderNotification.preOneMonth.rawValue
            }
            return _preOneMonthButton!
        }
    }
    
    let notificationButtonHeight: CGFloat = {
        return 28
    }()
    
    let notificationButtonWidth: CGFloat = {
        if Constant.DeviceType.IS_IPHONE_6P {
            return 84
        }
        return 68
    }()

    required init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.titleText = "提醒"
        self.cellName = AddGiftReminderCellType.notification.cellName()
        
        let lineView = UIView(frame: CGRect(x: 0,y: 10,width: Constant.ScreenSize.SCREEN_WIDTH,height: 0.5))
        lineView.backgroundColor = UIColor(rgba: "#eaeaea")
        self.addSubview(lineView)
        
        
        self.rightView.addSubview(self.currentDayButton)
        self.rightView.addSubview(self.preOneDayButton)
        self.rightView.addSubview(self.preThreeDayButton)
        self.rightView.addSubview(self.preOneWeekButton)
        self.rightView.addSubview(self.preTwoWeekButton)
        self.rightView.addSubview(self.preOneMonthButton)
        
        self.titleLabel.snp_remakeConstraints { (make) -> Void in
            let _ = make.left.equalTo(self.contentView)//.offset(15)
            let _ = make.centerY.equalTo(self.currentDayButton)
            let _ = make.width.equalTo(82)
        }
        self.currentDayButton.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.rightView).offset(25)
            let _ = make.left.equalTo(self.rightView)
           let _ =  make.width.equalTo(notificationButtonWidth)
            let _ = make.height.equalTo(notificationButtonHeight)
        }
        self.preOneDayButton.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.currentDayButton)
            let _ = make.centerX.equalTo(self.rightView)
            let _ = make.width.equalTo(notificationButtonWidth)
            let _ = make.height.equalTo(notificationButtonHeight)
        }
        self.preThreeDayButton.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.currentDayButton)
            let _ = make.right.equalTo(self.rightView)
            let _ = make.width.equalTo(notificationButtonWidth)
            let _ = make.height.equalTo(notificationButtonHeight)
        }
        self.preOneWeekButton.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.currentDayButton.snp_bottom).offset(10)
            let _ = make.left.equalTo(self.rightView)
            let _ = make.width.equalTo(notificationButtonWidth)
            let _ = make.height.equalTo(notificationButtonHeight)
        }
        self.preTwoWeekButton.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.preOneWeekButton)
            let _ = make.centerX.equalTo(self.rightView)
            let _ = make.width.equalTo(notificationButtonWidth)
            let _ = make.height.equalTo(notificationButtonHeight)
        }
        self.preOneMonthButton.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.preOneWeekButton)
            let _ = make.right.equalTo(self.rightView)
            let _ = make.width.equalTo(notificationButtonWidth)
            let _ = make.height.equalTo(notificationButtonHeight)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.contentView.addTopBorder(withHeight: 10, andColor: UIColor(rgba: Constant.common_background_color))
    }
    
    fileprivate func createNoticeButton(_ title: String) -> UIButton {
        let button: UIButton = UIButton(type: .custom)
        button.setTitle(title, for: UIControlState())
        button.setBackgroundImage(UIImage.add_image(with: Constant.Theme.Color12), for: UIControlState())
        button.setTitleColor(UIColor(rgba: "#555555"), for: UIControlState())
        button.setBackgroundImage(UIImage.add_image(with: Constant.Theme.Color1), for: .selected)
        button.setTitleColor(UIColor.white, for: .selected)
        button.addTarget(self, action: #selector(AddGiftReminderNotificationCell.didTapNotificationButton(_:)), for: .touchUpInside)
        button.titleLabel!.font = UIFont.systemFont(ofSize: 13)
        button.layer.cornerRadius = 5
        button.layer.borderColor = Constant.Theme.Color9.cgColor
        button.layer.borderWidth = 1
        button.layer.masksToBounds = true
        
        return button
    }
    
    func didTapNotificationButton(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        let tag = sender.tag
        if sender.isSelected {
            self.reminderViewModel!.notifications.append(tag)
            sender.layer.borderColor = Constant.Theme.Color1.cgColor
        } else {
            sender.layer.borderColor = Constant.Theme.Color9.cgColor
            self.reminderViewModel!.notifications = (self.reminderViewModel!.notifications.filter({ (ele) -> Bool in
                return ele != tag
            }))
        }
    }
    
}
