//
//  AddGiftReminderSituationCell.swift
//  Limi
//
//  Created by 倪晅 on 16/1/8.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class AddGiftReminderSituationCell: AddGiftReminderBaseCell {
    
    override var reminderViewModel: AddGiftReminderViewModel? {
        didSet {
            if reminderViewModel!.situation.value != 0 {
                self.setup(withSituation: self.reminderViewModel?.situation.value)
            }
            let _ = reminderViewModel!.situation.afterChange += { (_, newValue) -> () in
                self.setup(withSituation: newValue)
            }
        }
    }

    required init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.titleText = "赠送场合"
        let lineView = UIView(frame: CGRect(x: 0,y: 0,width: Constant.ScreenSize.SCREEN_WIDTH,height: 0.5))
        lineView.backgroundColor = UIColor(rgba: "#eaeaea")
        self.addSubview(lineView)
        
        self.rightView.addSubview(self.setButton)
        self.setButton.snp_makeConstraints { (make) -> Void in
            let _ = make.left.right.centerY.equalTo(self.rightView)
        }
        self.setButton.addTarget(self.realDelegate, action: Selector(("didTapSetSituation")), for: .touchUpInside)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(withSituation situation: NSInteger?) {
        if situation != nil {
            self.setButton.isSelected = true
            self.setButton.setTitle(self.reminderViewModel?.getSituationNameFor(situation!), for: UIControlState())
        }
    }

}
