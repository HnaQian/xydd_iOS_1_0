//
//  AddGiftReminderViewController.swift
//  Limi
//
//  Created by 倪晅 on 16/1/8.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit
import CoreData
import ContactsUI
import AVFoundation
import Photos

class AddGiftReminderViewController: BaseTableViewController,CNContactPickerDelegate {
    
    var isTest: Bool = false
    let popTotastView = PopTotastView(frame: Constant.ScreenSize.SCREEN_BOUNDS)
    var imagePicker:UIImagePickerController? = UIImagePickerController()
    var avatarImage: UIImage?
    
    fileprivate var _headerView: AddGiftReminderHeaderView?
    fileprivate var _datePicker: AddGiftReminderDatePickerView?
    
    var headerView: AddGiftReminderHeaderView {
        get {
            if _headerView == nil {
                _headerView = AddGiftReminderHeaderView(frame: CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: 150))
                _headerView!.pickAvatarButton.addTarget(self, action: #selector(AddGiftReminderViewController.didTapAvatar), for: .touchUpInside)
            }
            return _headerView!
        }
    }
    
    var datePicker: AddGiftReminderDatePickerView {
        get {
            _datePicker = AddGiftReminderDatePickerView(frame: CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: 300), type: false, yearStart: 1905, yearEnd: 2049,toolBarHeight:Constant.TOOLBAR_HEIGHT,pickerHeight:Constant.PICKER_HEIGHT, viewModel: self.reminderViewModel)
            
                _datePicker!.delegate = self
            return _datePicker!
        }
    }
    
    var reminderViewModel: AddGiftReminderViewModel = AddGiftReminderViewModel()
    
    init() {
        super.init(nibName: nil, bundle: nil)
        
        self.commonInit()
    }
    
    init(withReminderViewModel reminderViewModel: AddGiftReminderViewModel) {
        super.init(nibName: nil, bundle: nil)
        
        self.reminderViewModel = reminderViewModel
        self.commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    fileprivate func commonInit() {
        self.title = "添加送礼提醒"
        if self.reminderViewModel.isForUpdate {
            self.title = "编辑送礼提醒"
        }
        self.useRefreshControl = false
        self.dataSource = AddGiftReminderViewDataSource(delegate: self, viewModelClass: BaseTableCellViewModel.self, reminderViewModel: self.reminderViewModel)
        self.hidesBottomBarWhenPushed = true
    }
    
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.gesturBackEnable = false
        self.tableView.tableHeaderView = self.headerView
        
        if self.reminderViewModel.avatar != "" {
            
            let imageScale : String = (self.reminderViewModel.avatar) as String + "?imageView2/0/w/" + "\(Int(76*Constant.ScreenSize.SCALE_SCREEN))" + "/h/" + "\(Int(76*Constant.ScreenSize.SCALE_SCREEN))"
            
            self.headerView.avatarImage.af_setImageWithURL(URL(string: imageScale)!)
        }
        
        self.popTotastView.alpha = 0
        self.popTotastView.isHidden = true
        UIApplication.shared.keyWindow?.addSubview(popTotastView)
        
        self.navigationItem.rightBarButtonItem = CommonWidgets.NavigationRightBtn.newRightBtn("保存", target: self, action: #selector(AddGiftReminderViewController.didTapSave))
//        self.navigationController?.navigationBar.barStyle
        
        let _ = self.reminderViewModel.situation.afterChange += { (_, newValue) -> () in
            self.tableView.reloadData()
        }
        let _ = self.reminderViewModel.relation.afterChange += { (_, newValue) -> () in
            self.tableView.reloadData()
        }
        
        let hud : ProgressHUD = ProgressHUD(view: self.view)
        self.view.addSubview(hud)
        hud.show(true)
        let _ = LMRequestManager.requestByDataAnalyses(HTTPMethod.get, context:self.view,URLString: Constant.JTAPI.user_relation_relation_v2,isShowErrorStatuMsg:true,isNeedHud : false,successHandler: {data, status, msg in
            hud.hide(true)
            if status == 200 {
                self.reminderViewModel.serverData = data["data"] as? NSDictionary
                if self.reminderViewModel.situation.value > 0 {
                    self.dataSource?.refresh()
                }
            }
            },failureHandler: {Void in
                 hud.hide(true)
        })
        
        if self.reminderViewModel.situation.value == 0 {
            self.dataSource!.refresh()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func didTapAvatar() {
        self.imagePicker!.delegate = self
        self.imagePicker!.allowsEditing = true
        LCActionSheet(title: nil, buttonTitles: ["拍照","我的相册"], redButtonIndex: -1) { (buttonIndex) -> Void in
            if buttonIndex == 1 {
                let authStatus:AVAuthorizationStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
                
                if(authStatus == AVAuthorizationStatus.denied || authStatus == AVAuthorizationStatus.restricted) {
                    let alertController = UIAlertController(title: "温馨提示",
                                                            message: "请到“设置-隐私-相机”中打开心意点点获取手机相机的授权才能使用此功能", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "好的", style: .default,
                                                 handler: {
                                                    action in
                                                    return
                    })
                    
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: {
                        
                    })
                }else {
                    self.openCamera()
                }

            }
            if buttonIndex == 2 {
                let library:PHAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
                if(library == PHAuthorizationStatus.denied || library == PHAuthorizationStatus.restricted){
                    let alertController = UIAlertController(title: "温馨提示",
                                                            message: "请到“设置-隐私-照片”中打开心意点点读取手机相册的授权才能使用此功能", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "好的", style: .default,
                                                 handler: {
                                                    action in
                                                    return
                    })
                    
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: { 
                        
                    })
                    
                }else {
                    
                    self.openGallary()
                }

            }
        }.show()
    }
    
    
    
    func openCamera() {
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)) {
            BaseViewController.staticNavigationBarAppearance(true)
            self.imagePicker!.sourceType = UIImagePickerControllerSourceType.camera
            self.imagePicker!.cameraViewTransform = CGAffineTransform(scaleX: 1.5, y: 1.5)//缩放控制
            self .present(self.imagePicker!, animated: true, completion: nil)
        } else {
            openGallary()
        }
    }
    
    func openGallary() {
        self.imagePicker!.sourceType = UIImagePickerControllerSourceType.photoLibrary
        BaseViewController.staticNavigationBarAppearance(true)
        self.present(self.imagePicker!, animated: true, completion: nil)
    }
    
    func didTapSave() {
        
        Statistics.count(Statistics.GiftRemind.remind_add_finish_click)
        
        self.hideKeyBoard()
        
        self.uploadHeaderImage(self.avatarImage)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = super.tableView(tableView, heightForRowAt: indexPath)
        let cellType = AddGiftReminderCellType(rawValue: (indexPath as NSIndexPath).row)!
        let addDataSource = self.dataSource as! AddGiftReminderViewDataSource
        if addDataSource.isCellHidden(withCellType: cellType) {
            return 0
        }
        if cellType == AddGiftReminderCellType.notification {
            return 106
        }
        return height
    }
    
    override func backItemAction() {
        if self.reminderViewModel.situation.value != 0 ||
            self.reminderViewModel.mobile.value != "" ||
            self.reminderViewModel.avatar != "" ||
            self.reminderViewModel.date != "" ||
            self.reminderViewModel.relation.value != 0 {
                self.showLcactionSheet()
        } else {
           let _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    fileprivate func hideKeyBoard() {
        let dataSource = self.dataSource as! AddGiftReminderViewDataSource
        dataSource.recipientCell?.nameField.resignFirstResponder()
        dataSource.messageCell?.phoneField.resignFirstResponder()
    }
    
    func showLcactionSheet() {
        LCActionSheet(title: "确定要放弃填写送礼提醒吗?", buttonTitles: ["放弃填写"], cancelTitle: "继续填写", redButtonIndex: 0, clicked: { (buttonIndex) -> Void in
            
            if buttonIndex == 1
            {
               let _ = self.navigationController?.popViewController(animated: true)
            }
            
        }).show()
    }
    
}

extension AddGiftReminderViewController: AddGiftReminderBaseCellDelegate {
    
    func didTapSetSituation() {
        self.hideKeyBoard()
        self.popTotastView.addCell(self.reminderViewModel.getAllSituationsForTotast()) { (dic) -> Void in
            self.reminderViewModel.situation <- NSInteger(dic.keys.first!)!
        }
        popTotastView.showTableView()
        self.popTotastView.table.contentOffset = CGPoint(x: 0, y: 0)
        self.popTotastView.alpha = 1
        self.popTotastView.isHidden = false
    }
    
    func didTapSetRelation() {
        self.hideKeyBoard()
        if self.reminderViewModel.situation.value == AddGiftReminderSituation.none.rawValue {
            Utils.showError(context: self.view, errorStr: "请先选择场合")
            return
        }
        self.popTotastView.addCell(self.reminderViewModel.getAllRelationsForTotast()) { (dic) -> Void in
            self.reminderViewModel.relation <- NSInteger(dic.keys.first!)!
            let relation = self.reminderViewModel.relation.value
            let genderArray = self.reminderViewModel.relationGender![String(relation)]
            self.reminderViewModel.gender <- genderArray!.first!
        }
        popTotastView.showTableView()
        self.popTotastView.table.contentOffset = CGPoint(x: 0, y: 0)
        self.popTotastView.alpha = 1
        self.popTotastView.isHidden = false
    }
    
    func didTapSetDate() {
        self.hideKeyBoard()
        if self.reminderViewModel.situation.value == AddGiftReminderSituation.none.rawValue {
            Utils.showError(context: self.view, errorStr: "请先选择场合")
            return
        }
        self.presentSemiView(self.datePicker, withOptions: nil,
            completion: {
        })
    }
    
    func didTapAddressBook() {
        self.navigationBarAppearance(true)
        
        if #available(iOS 9.0, *) {
            let newPicker = CNContactPickerViewController()
            newPicker.delegate = self
            self.present(newPicker, animated: true, completion: {
                
            })
        } else {
            // Fallback on earlier versions
            let picker = ABPeoplePickerNavigationController()
            
            picker.peoplePickerDelegate = self
            
            if ((UIDevice.current.systemVersion as NSString).floatValue >= 8.0) {
                picker.predicateForSelectionOfPerson = NSPredicate(value:false)
            }
            
            picker.displayedProperties = [3]
            
            self.present(picker, animated: true) { () -> Void in
            }
        }
    }
    
    //选中某一属性
    @available(iOS 9.0, *)
    @objc(contactPicker:didSelectContactProperty:)
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contactProperty: CNContactProperty) {
        let firstName = contactProperty.contact.givenName
        let name = contactProperty.contact.familyName
        self.reminderViewModel.name <- (name + firstName)
        
        if contactProperty.value != nil {
           let tempPhoneNumber = contactProperty.value as! CNPhoneNumber
            let phoneNumber = tempPhoneNumber.stringValue
            self.reminderViewModel.mobile <- Utils.detectionTelphoneSimple(phoneNumber)
        }
    }
    
    @available(iOS 9.0, *)
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        //去除地址选择界面
        picker.dismiss(animated: true, completion: { () -> Void in
            
        })
    }
}

extension AddGiftReminderViewController: DatePickerViewDelegate {
    
    func notifyNewCalendar(_ cal: XYDDCalendar!, type _type: Int32, date: Date!, isYearHidden _isYearHidden: Bool, onlyMonthDay monthDay: String!) {
        self.reminderViewModel.dateType = _type
        let chineseMonths: [String] = ["正月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "冬月", "腊月"];
        let chineseDays: [String] = ["初一", "初二", "初三", "初四", "初五", "初六", "初七", "初八", "初九", "初十", "十一", "十二", "十三", "十四", "十五", "十六", "十七", "十八", "十九", "二十", "廿一", "廿二", "廿三", "廿四", "廿五", "廿六", "廿七", "廿八", "廿九", "三十"];
        if _isYearHidden {
            self.reminderViewModel.date = "1600-" + monthDay + " 00:01:00"
            if _type == 1 {
                self.reminderViewModel.displayDate <- monthDay
            } else {
                let components = monthDay.components(separatedBy: "-")
                let month = NSInteger(components[0] as String)
                let day = NSInteger(components[1] as String)
                self.reminderViewModel.displayDate <- (chineseMonths[month! - 1] +  chineseDays[day! - 1])
            }
        } else {
            let formatter = DateFormatter()
            formatter.dateFormat = "YYYY-MM-dd"
            let stringData = formatter.string(from: date)
            self.reminderViewModel.date = stringData + " 00:01:00"
            if _type == 1 {
                self.reminderViewModel.displayDate <- stringData
            } else {
                let chineseYear = CalendarUtil.alTolunarYear(stringData)
                self.reminderViewModel.displayDate <- chineseYear!
            }
        }
    }
    
}

extension AddGiftReminderViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        UINavigationBar.appearance().tintColor = UIColor(rgba: Constant.titlebar_color)
        picker.dismiss(animated: true, completion: nil)
        if let imageOri : UIImage = info[UIImagePickerControllerEditedImage] as? UIImage{
           gcd.async(.default) {
                let newSize : CGSize = CGSize(width: Constant.ScreenSize.SCREEN_WIDTH, height: Constant.ScreenSize.SCREEN_WIDTH)
                UIGraphicsBeginImageContext(newSize)
                imageOri.draw(in: CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: Constant.ScreenSize.SCREEN_WIDTH))
                // Get the new image from the context
                let image : UIImage = UIGraphicsGetImageFromCurrentImageContext()!;
                // End the context
                UIGraphicsEndImageContext();
                DispatchQueue.main.async { // 2
                    self.headerView.avatarImage.image = image
                    self.avatarImage = image
                }
            }
        } else {
            return
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        UINavigationBar.appearance().tintColor = UIColor(rgba: Constant.titlebar_color)
        picker .dismiss(animated: true, completion: nil)
    }
}

extension AddGiftReminderViewController: ABPeoplePickerNavigationControllerDelegate {
    
    func peoplePickerNavigationController(_ peoplePicker: ABPeoplePickerNavigationController,
        didSelectPerson person: ABRecord, property: ABPropertyID, identifier: ABMultiValueIdentifier) {

            let multiValue: ABMultiValue = ABRecordCopyValue(person, property).takeRetainedValue()
            var index = ABMultiValueGetIndexForIdentifier(multiValue, identifier)
            if index == -1 {
                index = 0 as CFIndex
            }
            let phone = ABMultiValueCopyValueAtIndex(multiValue, index).takeRetainedValue() as? String ?? ""
            let first = ABRecordCopyValue(person, kABPersonFirstNameProperty)?.takeRetainedValue() as? String ?? ""
            let last  = ABRecordCopyValue(person, kABPersonLastNameProperty)?.takeRetainedValue() as? String ?? ""
            self.reminderViewModel.name <- (last + first)
            self.reminderViewModel.mobile <- Utils.detectionTelphoneSimple(phone)
    }
    
    //取消按钮点击
    func peoplePickerNavigationControllerDidCancel(_ peoplePicker: ABPeoplePickerNavigationController) {
        //去除地址选择界面
        peoplePicker.dismiss(animated: true, completion: { () -> Void in
            
        })
    }
    
    func peoplePickerNavigationController(_ peoplePicker: ABPeoplePickerNavigationController,
        shouldContinueAfterSelectingPerson person: ABRecord) -> Bool {
            return false
    }
    
    func peoplePickerNavigationController(_ peoplePicker: ABPeoplePickerNavigationController,
        shouldContinueAfterSelectingPerson person: ABRecord, property: ABPropertyID,
        identifier: ABMultiValueIdentifier) -> Bool {
            return false
    }
    
}

extension AddGiftReminderViewController {
    
    func uploadHeaderImage(_ image: UIImage?) {
        if image != nil {
            let data = UIImageJPEGRepresentation(image!, 0.8)
            ImageUploader.uploadimage(context:self.view,data: data!) { [weak self](id,domain) -> Void in
                self!.reminderViewModel.avatar = id!
                self!.uploadInfo()
            }
        } else {
           self.uploadInfo()
        }
    }
    
    func uploadInfo() {

        
        var params = [String: AnyObject]()
        let situation = self.reminderViewModel.situation.value
        if situation == 0 {
            Utils.showError(context: self.view, errorStr: "请选择赠送场合")
            return
        }
        let relation = self.reminderViewModel.relation.value
        if self.reminderViewModel.isForUpdate && self.reminderViewModel.id > 0 {
            params["id"] = self.reminderViewModel.id as AnyObject?
        }
        params["name"] = self.reminderViewModel.name.value as AnyObject?
        params["mobile"] = self.reminderViewModel.mobile.value as AnyObject?
        params["avatar"] = self.reminderViewModel.avatar as AnyObject?
        params["gender"] = self.reminderViewModel.gender.value as AnyObject?
        params["date_type"] = Int(self.reminderViewModel.dateType) as AnyObject?
        params["date"] = self.reminderViewModel.date as AnyObject?
        params["relation"] = relation as AnyObject?
        params["scene"] = situation as AnyObject?
        params["period"] = self.reminderViewModel.notifications as AnyObject?
        params["remark"] = "" as AnyObject?
        
        let situationViewList = self.reminderViewModel.sceneViewList![String(situation)]
        let allCellTypes = [
            AddGiftReminderCellType.recipient,
            AddGiftReminderCellType.relation,
            AddGiftReminderCellType.gender,
            AddGiftReminderCellType.date,
            AddGiftReminderCellType.notification,
            AddGiftReminderCellType.message,
        ]
        for cellType in allCellTypes {
            var isValid = true
            let required = situationViewList![cellType.cellName()]!["required"] as! Bool
            switch cellType {
            case .recipient, .date, .message:
                if required && params[cellType.paramName()] as! String == "" {
                    isValid = false
                }
            case .gender, .relation:
                if required && params[cellType.paramName()] as! NSInteger == 0 {
                    isValid = false
                }
            case .notification:
                let notification = params[cellType.paramName()] as! NSArray
                if required && notification.count == 0 {
                    isValid = false
                }
            default:
                break
            }
            if !isValid {
                Utils.showError(context: self.view, errorStr: "请填写" + cellType.cellCnName())
                return
            }
        }
        
        if  (self.reminderViewModel.date as NSString).substring(to: 4) != "1600"{
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            var selectedDate = formatter.date(from: self.reminderViewModel.date)
            let zone = TimeZone.current
            var interval = zone.secondsFromGMT(for: selectedDate!)
            selectedDate = selectedDate!.addingTimeInterval(TimeInterval(interval))
            
            var currentDate = Date()
            var tmpdataString = (currentDate as NSDate).formattedDate(withFormat: "YYYY-MM-dd")
            tmpdataString = tmpdataString! + " 00:00:00";
            currentDate = formatter.date(from: tmpdataString!)!
            interval = zone.secondsFromGMT(for: currentDate)
            currentDate = currentDate.addingTimeInterval(TimeInterval(interval))
            
            let result : ComparisonResult = currentDate.compare(selectedDate!)
            if self.reminderViewModel.situation.value == 1 || self.reminderViewModel.situation.value == 2 || self.reminderViewModel.situation.value == 3 || self.reminderViewModel.situation.value == 9{
                if result.rawValue != 1 {
                    Utils.showError(context: self.view, errorStr: "所选时间应该小于今天")
                    return
                }
            }
            else{
                if result.rawValue != -1{
                    Utils.showError(context: self.view, errorStr: "所选时间应该大于今天")
                    return
                }
            }
        }
        var url = Constant.JTAPI.user_relation_add_v2
        if self.reminderViewModel.isForUpdate {
            url = Constant.JTAPI.user_relation_update_v2
        }
        let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: url, parameters: params, isNeedUserTokrn: true, isShowErrorStatuMsg: true, isNeedHud: true, successHandler: {data, status, msg in
            if status != 200 {
                return
            }
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "Guide_Empty_Contact_Change_Title"), object: nil)
            if self.reminderViewModel.isForUpdate {
                let userInfo: Dictionary<String,Bool>! = [
                    "isAddBirthWikiButton": self.reminderViewModel.situation.value == 1 ? true : false,
                ]
                
                NotificationCenter.default.post(name: Notification.Name(rawValue: "isNeedAddBirthWiki"), object: nil,userInfo: userInfo)
            }
            if situation == AddGiftReminderSituation.birthday.rawValue {
                if relation == AddGiftReminderRelation.father.rawValue || relation == AddGiftReminderRelation.mother.rawValue {
                    if relation == AddGiftReminderRelation.father.rawValue{
                        AccountManager.shared.setDadAdd(true)
                    }
                    if relation == AddGiftReminderRelation.mother.rawValue{
                        AccountManager.shared.setMontherAdd(true)
                    }
                }
            }
//            
//            let request: NSFetchRequest = NSFetchRequest(entityName: "SettingInfo")
//            let datasource : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(request)!
//            
//            var giftScene = ""
//            if datasource.count > 0{
//                let settingInfo = datasource[0] as! SettingInfo
//                giftScene = settingInfo.gift_scene!
//            }
//            
            if let giftScene = SystemInfoRequestManager.shareInstance().giftRemind_detail_sence{
                var object: [String: AnyObject]? = nil
                if !self.reminderViewModel.isForUpdate {
                    let id = data["data"]!["id"] as? NSInteger
                    object = [
                        "Id": id! as AnyObject,
                        "Scene": situation as AnyObject,
                        "Url": (String(giftScene) + String(id!)) as AnyObject,
                    ]
                }
                NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.notification_add_gift_reminder_success), object: object)
            }
            
           let _ = self.navigationController?.popViewController(animated: self.reminderViewModel.isForUpdate)
        })
    }

}
