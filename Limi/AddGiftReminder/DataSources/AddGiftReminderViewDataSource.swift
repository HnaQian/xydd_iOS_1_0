//
//  AddGiftReminderViewDataSource.swift
//  Limi
//
//  Created by 倪晅 on 16/1/8.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

enum AddGiftReminderCellType: NSInteger {
    case situation
    case recipient
    case relation
    case gender
    case date
    case notification
    case message
    
    func cellName() -> String {
        switch self {
        case AddGiftReminderCellType.situation:
            return "scene"
        case AddGiftReminderCellType.recipient:
            return "receiver"
        case AddGiftReminderCellType.relation:
            return "relation"
        case AddGiftReminderCellType.gender:
            return "gender"
        case AddGiftReminderCellType.date:
            return "gift_time"
        case AddGiftReminderCellType.notification:
            return "remind"
        case AddGiftReminderCellType.message:
            return "mobile"
        }
    }
    
    func paramName() -> String {
        switch self {
        case AddGiftReminderCellType.situation:
            return "scene"
        case AddGiftReminderCellType.recipient:
            return "name"
        case AddGiftReminderCellType.relation:
            return "relation"
        case AddGiftReminderCellType.gender:
            return "gender"
        case AddGiftReminderCellType.date:
            return "date"
        case AddGiftReminderCellType.notification:
            return "period"
        case AddGiftReminderCellType.message:
            return "mobile"
        }
    }
    
    func cellCnName() -> String {
        switch self {
        case AddGiftReminderCellType.situation:
            return "赠送场合"
        case AddGiftReminderCellType.recipient:
            return "收礼人"
        case AddGiftReminderCellType.relation:
            return "关系"
        case AddGiftReminderCellType.gender:
            return "性别"
        case AddGiftReminderCellType.date:
            return "赠送时间"
        case AddGiftReminderCellType.notification:
            return "提醒"
        case AddGiftReminderCellType.message:
            return "手机号码"
        }
    }
}

class AddGiftReminderViewDataSource: BaseTableDataSource {
    
    var reminderViewModel: AddGiftReminderViewModel?
    
    var recipientCell: AddGiftReminderRecipientCell?
    var messageCell: AddGiftReminderMessageCell?
    
    init(delegate: BaseTableDataSourceDelegate, viewModelClass: BaseTableCellViewModel.Type?, reminderViewModel: AddGiftReminderViewModel) {
        super.init(delegate: delegate, viewModelClass: viewModelClass)
        
        self.reminderViewModel = reminderViewModel
    }
    
    override func fetchRefreshDataWithSuccess(_ success: @escaping TableDataSourceSuccessBlock, failure: @escaping TableDataSourceFailureBlock) {
        let list = NSMutableArray()
        list.add(AddGiftReminderCellType.situation.rawValue)
        list.add(AddGiftReminderCellType.recipient.rawValue)
        list.add(AddGiftReminderCellType.relation.rawValue)
        list.add(AddGiftReminderCellType.gender.rawValue)
        list.add(AddGiftReminderCellType.date.rawValue)
        list.add(AddGiftReminderCellType.notification.rawValue)
        list.add(AddGiftReminderCellType.message.rawValue)
        
        success(list as [AnyObject])
    }
    
    override func createViewModelWithModel(_ model: AnyObject) -> BaseTableCellViewModel {
        var cellClass = BaseTableCell.self
        switch model as! NSInteger {
        case AddGiftReminderCellType.situation.rawValue:
            cellClass = AddGiftReminderSituationCell.self
        case AddGiftReminderCellType.recipient.rawValue:
            cellClass = AddGiftReminderRecipientCell.self
        case AddGiftReminderCellType.relation.rawValue:
            cellClass = AddGiftReminderRelationCell.self
        case AddGiftReminderCellType.gender.rawValue:
            cellClass = AddGiftReminderGenderCell.self
        case AddGiftReminderCellType.date.rawValue:
            cellClass = AddGiftReminderDateCell.self
        case AddGiftReminderCellType.notification.rawValue:
            cellClass = AddGiftReminderNotificationCell.self
        case AddGiftReminderCellType.message.rawValue:
            cellClass = AddGiftReminderMessageCell.self
        default:
            break
        }
        return BaseTableCellViewModel.init(model: model, cellClass: cellClass)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: AddGiftReminderBaseCell = super.tableView(tableView, cellForRowAt: indexPath) as! AddGiftReminderBaseCell
        let cellType = AddGiftReminderCellType(rawValue: (indexPath as NSIndexPath).row)!
        
        cell.reminderViewModel = self.reminderViewModel
        cell.contentView.isHidden = self.isCellHidden(withCellType: cellType)
        if (indexPath as NSIndexPath).row == AddGiftReminderCellType.recipient.rawValue {
            self.recipientCell = cell as? AddGiftReminderRecipientCell
        } else if (indexPath as NSIndexPath).row == AddGiftReminderCellType.message.rawValue {
            self.messageCell = cell as? AddGiftReminderMessageCell
        }
        
        return cell
    }
    
    override func fixedHeightForCell() -> CGFloat {
        return 60
    }
    
    func isCellHidden(withCellType cellType: AddGiftReminderCellType) -> Bool {
        if cellType == .situation {
            return false
        }
        let cellName = cellType.cellName()
        let situation = self.reminderViewModel!.situation.value
        if situation > 0 {
            if self.reminderViewModel!.sceneViewList != nil {
                let show = self.reminderViewModel!.sceneViewList![String(situation)]![cellName]!["show"] as! Bool
                if cellType == .gender {
                    let relation = self.reminderViewModel!.relation.value
                    if relation > 0 {
                        let genderArray = self.reminderViewModel!.relationGender![String(relation)]
                        return genderArray!.count == 1// || !show
                    }
                }
                return !show
            }
            return false
        } else {
            if cellType == .gender {
                return true
            }
        }
        return false
    }

}
