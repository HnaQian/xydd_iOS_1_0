//
//  CommonTableView.swift
//  Limi
//
//  Created by meimao on 15/12/25.
//  Copyright © 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//攻略

import UIKit

public enum ArticleFavType: Int
{
    case fav
    case cancelFav
    case none
}

class CommonTableView: UIView,TacticDetailDelegate,UITableViewDelegate,UITableViewDataSource {
    
    var userVC: UIViewController?  // 控制自身交互的viewController

    var type: ArticleFavType? // 点击 “喜欢”按钮做不同相应处理
    var stowBool: Bool = false // 是否是收藏界面  false 不是    true 是  默认false
    var tableView: UITableView!
    var tableViewCellHeight:CGFloat = HomeArticleTableViewCell.height
    
    weak var delegate:CommonTableViewDelegate?
    weak var commonDelegate:CommonScrollViewDelegate?
    // 数据
    var articleList:[JSON]! {
        didSet
        {
            articleArray = [HomCellBaseDataModel]()
            if let tempArray = articleList {
                var isCommonArticle = true
                for index in 0 ..< tempArray.count {
                    if let tempData = tempArray[index].orignData {
                        if let array = JSON(tempData as AnyObject)["goods_list"].arrayObject {
                            if array.count > 0 {
                                //判断是否是达人模式
                                isCommonArticle = false
                            }
                        }
                        if isCommonArticle {
                            self.articleArray.append(HomeCommonArticleModel(tempData as! [String:AnyObject]))
                        }else {
                            self.articleArray.append(HomeStyle2ArticleModel(tempData as! [String:AnyObject]))
                        }
                        isCommonArticle = true
                    }
                }
                self.tableView.reloadData()
            }
        }
    }
    
    //数据model
    fileprivate var articleArray = [HomCellBaseDataModel]()
    
    ///创建新tableview
    /**
    data          :非必须，可传nil   不传默认: nil      请求类型
    
    backView      :非必传，可传nil   不传默认: 固定背景图  传nil表示不需要背景， 不传表示默认背景 传view表示自定义
    
    type          :非必须，枚举    不传默认：.None
    
    userVC        :非必须，可传nil  不传默认：nil       控制CommonTableView交互的控制器，nil表示点击cell无反应
    
    stowBool      :非必须，不可传nil  不传默认：false    true表示 “我喜欢的” 数据类型，false表示普通数据类型
    */
    class func newCommonTableView(_ data: [JSON]? = nil, backView: UIView? = NullDataTipeView.newNullDataTipeView(), type: ArticleFavType = .none, userVC: UIViewController? = nil,stowBool: Bool = false) -> CommonTableView
    {
        let tabView = CommonTableView()
        
        tabView.articleList = data
        
        tabView.type = type
        
        if userVC != nil
        {
            tabView.userVC = userVC
        }
        
        tabView.stowBool = stowBool
        
        return tabView
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        tableView = UITableView(frame: CGRect.zero, style: UITableViewStyle.plain)
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        
        tableView.register(HomeArticleTableViewCell.self, forCellReuseIdentifier: HomeArticleTableViewCell.reuseIdentifier)
        tableView.register(HomeArticleSpecialStyle1Cell.self, forCellReuseIdentifier: HomeArticleSpecialStyle1Cell.reuseIdentifier)
        tableView.register(HomeArticleSpecialStyle2Cell.self, forCellReuseIdentifier: HomeArticleSpecialStyle2Cell.reuseIdentifier)
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        tableView.backgroundColor = UIColor.clear
        
        tableView.delegate = self
        tableView.dataSource = self
        
        self.addSubview(tableView)
        
        tableView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self)
            let _ = make.left.equalTo(self)
            let _ = make.right.equalTo(self)
            let _ = make.bottom.equalTo(self)
        }
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    // TacticDetailDelegate代理方法
    
    func detailFavBtnClick(_ index: Int,isAdd: Bool){
        
        if self.type == .fav
        {
            let indexPath = IndexPath(row: index, section: 0)
            let data: HomeCommonArticleModel = self.articleArray[index] as! HomeCommonArticleModel
            if isAdd{
                data.collect_num += 1
            }
            else{
                data.collect_num -= 1
            }
            self.articleArray[index] = data
            self.tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.top)
        }
        
    }
    
    // 代理方法
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.articleArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return self.articleArray[(indexPath as NSIndexPath).row].getCellHeight()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:HomeBaseTableViewCell = tableView.dequeueReusableCell(withIdentifier: self.articleArray[(indexPath as NSIndexPath).row].classMap().reuseIdentifier) as! HomeBaseTableViewCell
        
        let data = articleArray[(indexPath as NSIndexPath).row]
        cell.refreshWithData(data)
        cell.delegate = self
        
        return cell
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if self.articleList.count > 0 && self.userVC != nil{
            
            self.delegate?.commonTableViewDidSelected!(self.articleArray[(indexPath as NSIndexPath).row] as! HomeCommonArticleModel)
           
            let model = self.articleArray[(indexPath as NSIndexPath).row]
            
            switch model.getModelType() {
                
            case 1,2:
                //攻略
                if let mmodel = model as? HomeCommonArticleModel {
                    let tacticDetailVC = TacticDetailViewController()
                    tacticDetailVC.article_id = mmodel.id
                    
                    tacticDetailVC.hidesBottomBarWhenPushed = true
                    self.userVC!.navigationController?.pushViewController(tacticDetailVC, animated: true)
                }
            default :
                break
            }
            
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.commonDelegate?.commonScrollViewDidScroll!(scrollView)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        self.commonDelegate?.commonScrollViewDidEndDragging!(scrollView)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        IQKeyboardManager.shared().resignFirstResponder()
        self.commonDelegate?.commonScrollViewWillBeginDragging!(scrollView)
    }
    
}

extension CommonTableView:HomeBaseTableViewCellDelegate {
    ///攻略
    func homeTableViewCellSelected(_ articleID:Int) {
        
    }
    ///商品
    func homeTableViewCellGoodsSelected(_ goodsID:Int) {
        let goodsdetail = GoodsDetailViewController.newGoodsDetailWithID(goodsID)
        goodsdetail.hidesBottomBarWhenPushed = true
        self.userVC!.navigationController?.pushViewController(goodsdetail, animated: true)
    }
    ///自选礼盒
    func homeTableViewCellPackageSelected(_ href:String) {
        
    }
}


@objc protocol CommonTableViewDelegate:NSObjectProtocol {
    @objc optional func commonTableViewDidSelected(_ data:HomeCommonArticleModel)
    
}

@objc protocol CommonScrollViewDelegate:NSObjectProtocol {
    @objc optional func commonScrollViewDidScroll(_ scrollView: UIScrollView)
    @objc optional func commonScrollViewDidEndDragging(_ scrollView: UIScrollView)
    @objc optional func commonScrollViewWillBeginDragging(_ scrollView:UIScrollView)
}
