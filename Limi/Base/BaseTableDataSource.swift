//
//  BaseTableDataSource.swift
//  Limi
//
//  Created by 倪晅 on 16/1/5.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

typealias TableDataSourceSuccessBlock = ([AnyObject]) -> Void
typealias TableDataSourceFailureBlock = (NSError?) -> Void

protocol BaseTableDataSourceDelegate: BaseTableCellDelegate {
    func dataSource(_ dataSource: BaseTableDataSource, didFinishRefreshWithError error: NSError?)
    func dataSource(_ dataSource: BaseTableDataSource, didFinishLoadMoreWithError error: NSError?)
    func dataSource(_ dataSource: BaseTableDataSource, didFinishNoMoreDataWithError error: NSError?)
    func changeCellWhenOnClickImportantButton(_ indexpath:IndexPath,isdelete:Bool,changedData:BaseTableCellViewModel)
}

class BaseTableDataSource: NSObject {
    
    var type: NSInteger = 0
    
    var data: NSMutableArray = NSMutableArray()
    var delegate: BaseTableDataSourceDelegate?
    var viewModelClass: BaseTableCellViewModel.Type! = BaseTableCellViewModel.self
    var cellClass: BaseTableCell.Type! = BaseTableCell.self
    
    init(delegate: BaseTableDataSourceDelegate, viewModelClass: BaseTableCellViewModel.Type?) {
        self.delegate = delegate
        if viewModelClass != nil {
            self.viewModelClass = viewModelClass!
        }
    }
    
    func refresh() {
        self.fetchRefreshDataWithSuccess({ (page: [AnyObject]) -> Void in
            self.data.removeAllObjects()
            print(page)
            let list = page as NSArray
            if list.count > 0 {
                list.forEach({ (model: Any) -> () in
                    if let dict = model as? NSDictionary{
                    if dict["situation"] != nil && dict["relation"] != nil {
                        if (dict["relation"] as! NSInteger == AddGiftReminderRelation.mother.rawValue && UserDefaults.standard.bool(forKey: "MotherIsHadDelete") == false) || (dict["relation"] as! NSInteger == AddGiftReminderRelation.father.rawValue && UserDefaults.standard.bool(forKey: "FatherIsHadDelete") == false) || dict["relation"] as! NSInteger == AddGiftReminderRelation.none.rawValue{
                            let viewModel: BaseTableCellViewModel = self.createViewModelWithModel(model as AnyObject)
                            self.data.add(viewModel)
                        }
                    }
                    else{
                        let viewModel: BaseTableCellViewModel = self.createViewModelWithModel(model as AnyObject)
                        self.data.add(viewModel)
                        }
                    }else {
                        let viewModel: BaseTableCellViewModel = self.createViewModelWithModel(model as AnyObject)
                        self.data.add(viewModel)
                    }
                } )
            }
            
            self.delegate?.dataSource(self, didFinishRefreshWithError: nil)
        }) { (error: NSError?) -> Void in
            self.delegate?.dataSource(self, didFinishRefreshWithError: error)
        }
    }
    
    func loadMore() {
        self.fetchMoreDataWithSuccess({ (page: [AnyObject]) -> Void in
            let list = page as NSArray
            if list.count > 0 {
                list.forEach({ (model: Any) -> () in
                    let viewModel: BaseTableCellViewModel = self.createViewModelWithModel(model as AnyObject)
                    self.data.add(viewModel)
                } )
                self.delegate?.dataSource(self, didFinishLoadMoreWithError: nil)
            } else {
                self.delegate?.dataSource(self, didFinishNoMoreDataWithError: nil)
            }
            }) { (error: NSError?) -> Void in
                self.delegate?.dataSource(self, didFinishLoadMoreWithError: error)
        }
    }
    
    func fetchRefreshDataWithSuccess(_ success: @escaping TableDataSourceSuccessBlock, failure: @escaping TableDataSourceFailureBlock) {
        
    }
    
    func fetchMoreDataWithSuccess(_ success: @escaping TableDataSourceSuccessBlock, failure: @escaping TableDataSourceFailureBlock) {
        
    }
    
    func createViewModelWithModel(_ model: AnyObject) -> BaseTableCellViewModel {
        return BaseTableCellViewModel.init(model: model, cellClass: self.cellClass)
    }
    
    func dataAtIndexPath(_ indexPath: IndexPath) -> BaseTableCellViewModel? {
        if (indexPath as NSIndexPath).section > 0 {
            return nil
        }
        return self.dataAtIndex((indexPath as NSIndexPath).row)
    }
    
    func dataAtIndex(_ index: NSInteger) -> BaseTableCellViewModel? {
        if self.data.count > index {
            return self.data[index] as? BaseTableCellViewModel
        }
        return nil
    }
    
    func removeData(_ viewModel: BaseTableCellViewModel) {
        self.data.remove(viewModel)
    }
    
    func fixedHeightForCell() -> CGFloat {
        return 44;
    }

}

extension BaseTableDataSource: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellViewModel: BaseTableCellViewModel = self.dataAtIndexPath(indexPath)!
        var cell: BaseTableCell? = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(cellViewModel.cellClass)) as? BaseTableCell
        if cell == nil {
            cell = cellViewModel.cellClass.init(style: .default, reuseIdentifier: NSStringFromClass(cellViewModel.cellClass))
        }
        cell!.indexPath = indexPath
        cell!.viewModel = cellViewModel
        cell!.delegate = self.delegate
        return cell!;
    }
}
