//
//  BaseTableView.swift
//  Limi
//
//  Created by 程巍巍 on 5/19/15.
//  Copyright (c) 2015 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit


extension UITableViewCell {
    class func heightWithData(_ data: [String: AnyObject], width: CGFloat)-> CGFloat{
        return 44
    }
    
    func reloadData(_ data: BaseDataMode){}
}



class BaseTableViewCell: UITableViewCell {
    
    class func nibName()->String! {
        return nil
    }
    
    var dataSource: BaseDataMode!
    func reloadDataSource(_ data: BaseDataMode) {
        self.dataSource = data
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        defaultInit()
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override func awakeFromNib() {
        self.defaultInit()
    }
    
    func defaultInit(){
        
    }
}
class BaseTableView: UITableView {
    
    var cellSelectedHandler: ((_ cell: BaseTableViewCell, _ tableView: BaseTableView)->Void)?

    // dataArray element must contain "type" item
    var dataArray: [BaseDataMode]? {
        didSet{
            self.reloadData()
        }
    }
    var cellClassMap: [String: BaseTableViewCell.Type]! {
        didSet{
            var nibNameMap = [String: String]()
            for (identifier, cellClass) in cellClassMap! {
                self.register(cellClass, forCellReuseIdentifier: identifier)
                if let nibName = cellClass.nibName() {
                    if var _ = Bundle.main.path(forResource: nibName, ofType: "nib") {
                        nibNameMap[identifier] = nibName
                    }
                }
            }
            self.cellNibMap = nibNameMap
        }
    }
    
    fileprivate var cellNibMap: [String: String]! {
        didSet{
            for (identifier, nibName) in cellNibMap! {
                self.register(UINib(nibName: nibName, bundle: nil), forCellReuseIdentifier: identifier)
            }
        }
    }
    
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: UITableViewStyle.plain)
        defaultInit()
    }
    
    convenience init(){
        self.init(frame: CGRect.zero, style: UITableViewStyle.plain)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func defaultInit() {
        self.showsHorizontalScrollIndicator = false
        self.showsVerticalScrollIndicator = false

    }
}

extension BaseTableView {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray != nil ? dataArray!.count : 0
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let cell = tableView.cellForRow(at: indexPath) as! BaseTableViewCell
        self.cellSelectedHandler?(cell, self)
    }
}
