//
//  BaseTableCell.swift
//  Limi
//
//  Created by 倪晅 on 16/1/5.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

protocol BaseTableCellDelegate:NSObjectProtocol {
   
}

class BaseTableCell: UITableViewCell {
    
    var indexPath: IndexPath?
    var viewModel: BaseTableCellViewModel?
    var delegate: BaseTableCellDelegate?
    
    required override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = .none
    }
    
    
    func refreshWithData(_ dataModel:BaseDataMode){}
    

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
