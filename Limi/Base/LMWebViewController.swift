//
//  LMWebViewController.swift
//  Limi
//
//  Created by Richie on 16/7/22.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit
import WebKit

/**
 webView页面
 */
class LMWebViewController: BaseViewController {
    struct JSData {
        static func newJSData(_ url:URL) ->JSData{
            var jsdata = JSData()
            jsdata.url = url
            jsdata.host = url.host
            jsdata.scheme = url.scheme!
            jsdata.params = CMDQueryStringSerialization.dictionary(withQueryString: url.query) as AnyObject?
            
            return jsdata
        }
        
        var url : URL!
        var host : String?
        var scheme : String = ""
        var params : AnyObject?
    }
    
    
    var currentJSData:JSData?{return _currentJSData}
    ///导航栏的左上角的items
    var leftItemsView:NavigationItemMenu?{return _leftItemsView}
    ///导航栏的右上角的items
    var rightItemsView:NavigationItemMenu?{return _rightItemsView}
    ///webView
    var webView:WKWebView {return _webView}
    
    var isHadEntryKSL: Bool{return _isHadEntryKSL}
    
    var isStopInLogin : Bool = false
    
    //分享
    //分享的图片资源地址
    var shareImgSrc    = ""
    //分享描述
    var shareDesc   = ""
    //标题
    var shareTitle1 = ""
    var shareTitle2 = ""
    //超链
    var shareArticleHref = ""
    
    
    fileprivate var _currentJSData:JSData?
    fileprivate var _leftItemsView:NavigationItemMenu?
    fileprivate var _rightItemsView:NavigationItemMenu?
    fileprivate let _webView = WKWebView(frame: UIScreen.main.bounds)
    
    fileprivate var callBack = ""
    fileprivate var _isHadEntryKSL: Bool = false
    
    var isfirstIn = true
    
    
    ///返回顶部按钮
    let scrollToTopButton:UIButton = {
        let button = UIButton(type: .custom)
        let space_X = Constant.ScreenSizeV2.SCREEN_WIDTH - 54
        let space_Y = Constant.ScreenSizeV2.SCREEN_HEIGHT / 2 * 1.8 - 22 - 64
        button.frame = CGRect(x: space_X, y: space_Y, width: 44, height: 44)
        button.setImage(UIImage(named: "goods_detail_float_button"), for: UIControlState())
        button.isHidden = true
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(webView)
        webView.iheight = Constant.ScreenSize.SCREEN_HEIGHT - MARGIN_64
        
        self.webView.isOpaque = false
        self.webView.navigationDelegate = self
        self.webView.scrollView.delegate = self
        self.webView.backgroundColor = UIColor.clear
        
        scrollToTopButton.addTarget(self, action: #selector(LMWebViewController.scrollToTop), for: .touchUpInside)
        self.view.addSubview(scrollToTopButton)
    }
    
    /**返回顶部动作*/
    func scrollToTop(){
        self.webView.scrollView.scrollRectToVisible(CGRect(x: 0, y: 0, width: 1, height: 1), animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if self.webView.isLoading {
            self.webView.stopLoading()
        }
        self.isStopInLogin = true
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.isStopInLogin = false
    }
    
    
    /**添加左上角的按钮 传入的View的frame由View自己决定*/
    func setLeftItems(_ icons:[UIView]){
        
        if icons.count > 0{
            _leftItemsView = NavigationItemMenu.newNavigationMenu(icons)
            _leftItemsView?.delegate = self
        }
    }
    
    func showLeftItems(){
        if let view = self.leftItemsView{
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: view)
        }
    }
    
    func hiddenLeftItems(){
        self.navigationItem.leftBarButtonItem = nil
    }
    
    
    /**添加右上角的按钮 传入的View的frame由View自己决定*/
    func setRightItems(_ icons:[UIView]){
        if icons.count > 0{
            _rightItemsView = NavigationItemMenu.newNavigationMenu(icons)
            _rightItemsView?.delegate = self
        }
    }
    
    func showRightItems(){
        if let view = self.rightItemsView{
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: view)
        }
    }
    
    func hiddenRightItems(){
        self.navigationItem.rightBarButtonItem = nil
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    deinit{
        self.webView.scrollView.delegate = nil
        self.webView.navigationDelegate = nil
    }
}


extension LMWebViewController : LMWebViewWidgetsDelegate,WKNavigationDelegate,UIScrollViewDelegate{
    
    
    /**注：子类重写需调用父类 super.scrollViewDidScroll(_:) */
    func scrollViewDidScroll(_ scrollView: UIScrollView) {}
    
    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: () -> Void) {
        //
        completionHandler()
        let alert = UIAlertController(title: "提示", message: "\(message)", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "确定", style: .default, handler:nil))
        alert.addAction(UIAlertAction(title: "取消", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    // MARK: -webView JS交互
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
        if let murl = navigationAction.request.url{
            _currentJSData = JSData.newJSData(murl)
        }else{
            _currentJSData = nil
        }
        
        
        
        if navigationAction.request.url!.scheme == "local" {
            if navigationAction.request.url!.host == "appShare" {
                if let params : NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: navigationAction.request.url!.query) as NSDictionary?{
                    if let configData  = params.object(forKey: "data") as? String{
                        
                        if let configJsonData = String(configData).parseJSONString{
                            if let title1 : String = configJsonData["title"] as? String{
                                shareTitle1 = title1
                            }
                            if let title2 : String = configJsonData["title2"] as? String{
                                shareTitle2 = title2
                            }
                            if let shareDesc : String = configJsonData["desc"] as? String{
                                self.shareDesc = shareDesc
                            }
                            if let shareImg : String = configJsonData["img"] as? String{
                                self.shareImgSrc = shareImg
                            }
                            if let shareFarticle_item : String = configJsonData["url"] as? String{
                                self.shareArticleHref = shareFarticle_item
                                if let tempDic : NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: shareFarticle_item) as NSDictionary?{
                                    
                                    if let id  = tempDic.object(forKey: "id") as? String{
                                        Statistics.count(Statistics.Tractic.article_detail_share_click, andAttributes: ["aid":String(id)])
                                    }
                                }
                               
                            }
                            if let shareCallBack : String = configJsonData["callback"] as? String{
                                self.callBack = shareCallBack
                            }
                            
                            if let sharetype : String = configJsonData["type"] as? String{
                                if sharetype == "common"{
                                    
                                    if self.shareImgSrc != "" && self.shareArticleHref != ""{
                                        self.shareConfig(Constant.ShareContentType.mediaType.rawValue)
                                    }
                                }
                                else if sharetype == "img"{
                                    if self.shareImgSrc != ""{
                                        self.shareConfig(Constant.ShareContentType.imageType.rawValue)
                                    }
                                }
                                else if sharetype == "title"{
                                    if self.shareTitle1 != ""{
                                        self.shareConfig(Constant.ShareContentType.textType.rawValue)
                                    }
                                }
                            }
                            else{
                                Utils.showError(context: self.view, errorStr: "数据获取失败")
                            }
                            decisionHandler(.cancel)
                        }
                    }
                }
            }
                
            else if navigationAction.request.url!.host == "share_config"{
                if let params : NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: navigationAction.request.url!.query) as NSDictionary?{
                    
                    if let title  = params.object(forKey: "title") as? String{
                        self.shareTitle1 = title
                    }
                    
                    if let title2  = params.object(forKey: "title2") as? String{
                        self.shareTitle2 = title2
                    }
                    
                    if let desc  = params.object(forKey: "desc") as? String{
                        self.shareDesc = desc
                    }
                    
                    if let shareImg  = params.object(forKey: "img") as? String{
                        self.shareImgSrc = shareImg
                    }
                    
                    if let url  = params.object(forKey: "url") as? String{
                        self.shareArticleHref = url
                    }
                    if self.shareDesc != "" && self.shareImgSrc != "" && self.shareArticleHref != ""{
                        self.shareConfig(Constant.ShareContentType.mediaType.rawValue)
                    }
                    else{
                        Utils.showError(context: self.view, errorStr: "数据获取失败")
                    }
                    
                    decisionHandler(.cancel)
                }
            }
            else if navigationAction.request.url!.host == "appPay"{
                if UserInfoManager.didLogin == true{
                    if let params : NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: navigationAction.request.url!.query) as NSDictionary?{
                        if let configData = params.object(forKey: "data") as? String{
                            if let configJsonData = String(configData).parseJSONString{
                                if let oid : Int = configJsonData["oid"] as? Int{
                                    self.navigationController?.pushViewController(PayViewController.newPayViewController(oid,canclePay:.forward), animated: true)
                                }
                            }
                        }
                    }
                }
                else{
                    self.tipAndLogin("登录之后才能完成付款")
                }
                
            }
                
            else if navigationAction.request.url!.host == "appLogin"{
                if let params : NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: navigationAction.request.url!.query) as NSDictionary?{
                    if let configData  = params.object(forKey: "data") as? String{
                        if let configJsonData = String(configData).parseJSONString{
                            if let shareCallBack : String = configJsonData["callback"] as? String{
                                self.callBack = shareCallBack
                            }
                        }
                    }
                }
                self.Inlogin()
            }
                
            else if navigationAction.request.url!.host == "appPhoneBook"{
                if let params : NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: navigationAction.request.url!.query) as NSDictionary?{
                    if let configData = params.object(forKey: "data"){
                        if let configJsonData = String(describing: configData).parseJSONString{
                            if let callBack : String = configJsonData["callback"] as? String{
                                self.callBack = callBack
                            }
                        }
                    }
                }
                self.openContact()
            }
                
            else if navigationAction.request.url!.host == "appReceiveCoupon"{
                if UserInfoManager.didLogin == true{
                    if let params : NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: navigationAction.request.url!.query) as NSDictionary?{
                        if let configData = params.object(forKey: "data"){
                            if let configJsonData = String(describing: configData).parseJSONString{
                                if let coupon_ids : NSArray = configJsonData["coupon_ids"] as? NSArray{
                                    self.receCouponWithQuery(coupon_ids)
                                    if let callBack = configJsonData["callback"] as? String{
                                        self.callBack = callBack
                                    }
                                }}}
                    }
                    
                }
                else{
                    self.tipAndLogin("")
                }
            }
                
            else if navigationAction.request.url!.host == "appToast"{
                if let params : NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: navigationAction.request.url!.query) as NSDictionary?{
                    if let configData = params.object(forKey: "data"){
                        if let configJsonData = String(describing: configData).parseJSONString{
                            if let tip : String = configJsonData["content"] as? String{
                                if let callBack = configJsonData["callback"] as? String{
                                    self.callBack = callBack
                                }
                                self.showToast(tip)
                            }
                            
                        }
                    }
                }
            }
                
                
            else if navigationAction.request.url!.host == "index"
            {
                let userInfo: Dictionary<String,Int>! = [
                    "index": 0,
                    ]
                NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.show_rootController), object: nil, userInfo: userInfo)
               let _ = self.navigationController?.popToRootViewController(animated: true)
                
            }
                
            else if navigationAction.request.url!.host ==  "appGiftService"{
                
                Statistics.count(Statistics.Order.order_service_finish_click)
                
                if let params : NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: navigationAction.request.url!.query) as NSDictionary?{
                    
                    if let configData = params.object(forKey: "data"),
                        let configJsonData = String(describing: configData).parseJSONString,
                        let service_ids = configJsonData["service_ids"] as? NSArray,
                        let service_prices = configJsonData["price"] as? NSArray{
                        
                        var ids = [Int]()
                        var prices = [Double]()
                        
                        for id in service_ids{
                            if let idstr = (id as? NSString){
                                ids.append(idstr.integerValue)
                            }
                        }
                        
                        for price in service_prices{
                            if let  p = price as? Double{
                                prices.append(p)
                            }
                        }
                        
                        WishSendUnitView.updateServiceCost(prices)
                        WishSendUnitView.updateServiceids(ids)
                        WishSendUnitView.serviceIdsEffected = true
                    }
                   let _ = self.navigationController?.popViewController(animated: true)
                }
                else{
                    Utils.showError(context: self.view, errorStr: "数据获取失败")
                }
                
                
                
            }
                
            else{
                if EventDispatcher.isPureNumberWithGoodsId(navigationAction.request.url!) {
                    EventDispatcher.dispatch("\(navigationAction.request.url!)", onNavigationController: self.navigationController)
                }else {
                    Utils.showError(context: self.view, errorStr: "数据获取失败")
                }
                
            }
            decisionHandler(.cancel)
            
        }
        else if EventDispatcher.isNeedAppProcess(navigationAction.request.url!){
            
            if let params : NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: navigationAction.request.url!.query) as NSDictionary?{
                if let share_config = params.object(forKey: "service_ids") as? NSArray{
                    var ids = [Int]()
                    for id in share_config{
                        if let idstr = (id as? NSString){
                            ids.append(idstr.integerValue)
                        }
                        if ids.count == 3{
                            WishSendUnitView.saveTempServiceids(ids)
                            WishSendUnitView.serviceIdsEffected = false
                        }
                    }
                }
            }
            
            if EventDispatcher.isPureNumberWithGoodsId(navigationAction.request.url!) {
                 EventDispatcher.dispatch("\(navigationAction.request.url!)", onNavigationController: self.navigationController)
            }else {
                Utils.showError(context: self.view, errorStr: "数据获取失败")
            }
            decisionHandler(.cancel)
        }
            
        else {
            //navigationAction.request.url!.absoluteString.contains("article_item") || navigationAction.request.url!.absoluteString.contains("goods")
            if self.isfirstIn{
                self.isfirstIn = false
                decisionHandler(.allow)
            }else {
                //跳到webView页面
                self.actualWebView(url:navigationAction.request.url!)
                decisionHandler(.cancel)
            }
            
        }
        
    }
    
    func tipAndLogin(_ tip:String){

    }
    
    func Inlogin(){
    }
    
    func openContact()
    {
    }
    
    func actualWebView(url:URL) {
        
    }
    
    func receCouponWithQuery(_ coupon_ids : NSArray){
        //let array1 = coupon_ids.componentsSeparatedByString(",")
        let array1 = coupon_ids
        var couponIdArr: [Int] = []
        for item in array1 {
            couponIdArr.append(item as! Int)
        }
        if !couponIdArr.isEmpty {
            self.receiveCoupon(couponIdArr, webView: webView)
        }
    }
    
    // 领取优惠券
    func receiveCoupon(_ array: [Int] ,webView: WKWebView) {

        
        
    }
    
    func showToast(_ toast: String){

    }
    
    // MARK: -分享入口
    func shareConfig(_ shareType:Int){
        
        let Objs : NSArray = [MenuLabel.createlabelIconName("share_icon_wechat_friend", title: "微信好友"),MenuLabel.createlabelIconName("share_icon_wechat_timeline", title: "微信朋友圈"),MenuLabel.createlabelIconName("share_icon_qq_friend", title: "QQ好友"),MenuLabel.createlabelIconName("share_snapshot_icon_wechat_friend", title: "微信快照"),MenuLabel.createlabelIconName("share_snapshot_icon_wechat_timeline", title: "朋友圈快照"),MenuLabel.createlabelIconName("share_icon_qq_zone", title: "QQ空间")]
        HyPopMenuView.creatingPopMenuObjectItmes(Objs as! [MenuLabel],isGeneral:false, topView: nil, selectdCompletionBlock: {index  in
            if index == 0{
                self.shareToWx(Int32(WXSceneSession.rawValue),shareType: shareType)
            }
            else if index == 1{
                self.shareToWx(Int32(WXSceneTimeline.rawValue),shareType: shareType)
            }
            else if index == 2{
                self.shareToQQ(true,shareType: shareType)
            }else if index == 3 {
                //微信快照
                self.shareSnapshotToWx(Int32(WXSceneSession.rawValue))
                
            }else if index == 4 {
                //朋友圈快照
                self.shareSnapshotToWx(Int32(WXSceneTimeline.rawValue))
            }
            else if index == 5 {
                self.shareToQQ(false,shareType: shareType)
            }
            
            }
        )
    }
    
    // MARK: -微信分享
    func shareToWx(_ wxScene:Int32,shareType:Int){
        let wechatShare : LDSDKWXServiceImpl = LDSDKManager.getShareService(LDSDKPlatformType.weChat) as! LDSDKWXServiceImpl
        var shareDict:NSDictionary = NSDictionary()
        if shareType == Constant.ShareContentType.mediaType.rawValue || shareType == Constant.ShareContentType.imageType.rawValue{
            
            OriginalDownLoad().downloadWithUrlString(context:self.view,urlString: shareImgSrc, completionHandle: { (data) -> Void in
                if let mdata = data
                {
                    
                    let avatarImage = UIImage(data: mdata as Data)
                    var title = ""
                    if wxScene == Int32(WXSceneTimeline.rawValue){
                        if self.shareTitle2 != ""{
                            title = self.shareTitle2
                        }
                        else{
                            title = self.shareTitle1
                        }
                        
                    }
                    else{
                        title = self.shareTitle1
                    }
                    
                    if shareType == Constant.ShareContentType.mediaType.rawValue{
                        shareDict = [LDSDKShareContentTitleKey : title,   LDSDKShareContentDescriptionKey : self.shareDesc,LDSDKShareContentWapUrlKey : self.shareArticleHref,LDSDKShareContentImageKey : avatarImage!]
                    }
                    else{
                        shareDict = [LDSDKShareContentTitleKey : "",   LDSDKShareContentDescriptionKey : "",LDSDKShareContentImageKey : avatarImage!]
                    }
                    wechatShare.share(withContent: shareDict as! [AnyHashable: Any], shareModule:wxScene == Int32(WXSceneSession.rawValue) ? 1 : 2, onComplete: {(success,error) in
                        if error != nil{
                            Utils.showError(context: self.view, errorStr: (error?.localizedDescription)!)
                            let javaScriptString : NSString = (String(format: "%@('{\"status\":400,\"msg\":\"分享失败\",data:\"\"}')", self.callBack) as NSString)
                            self.webView.evaluateJavaScript(javaScriptString as String, completionHandler: nil)
                        }
                        else{
                            let javaScriptString : NSString = (String(format: "%@('{\"status\":200,\"msg\":\"分享成功\",data:\"\"}')", self.callBack) as NSString)
                            self.webView.evaluateJavaScript(javaScriptString as String, completionHandler: nil)
                            
                        }
                        
                    })
                }
            })
        }else if shareType == Constant.ShareContentType.textType.rawValue{
            shareDict = [LDSDKShareContentTextKey : self.shareTitle1]
            wechatShare.share(withContent: shareDict as! [AnyHashable: Any], shareModule:wxScene == Int32(WXSceneSession.rawValue) ? 1 : 2, onComplete: {(success,error) in
                if error != nil{
                    Utils.showError(context: self.view, errorStr: (error?.localizedDescription)!)
                    let javaScriptString : NSString = (String(format: "%@('{\"status\":400,\"msg\":\"分享失败\",data:\"\"}')", self.callBack) as NSString)
                    self.webView.evaluateJavaScript(javaScriptString as String, completionHandler: nil)
                }
                else{
                    let javaScriptString : NSString = (String(format: "%@('{\"status\":200,\"msg\":\"分享成功\",data:\"\"}')", self.callBack) as NSString)
                    self.webView.evaluateJavaScript(javaScriptString as String, completionHandler: nil)
                    
                }
                
            })
        }
        
        
    }
    
    // MARK: -QQ分享
    func shareToQQ(_ isShareToQQ : Bool,shareType:Int){
        let qqShare : LDSDKQQServiceImpl = LDSDKManager.getShareService(LDSDKPlatformType.QQ) as! LDSDKQQServiceImpl
        var shareDict:NSDictionary = NSDictionary()
        
        if shareType == Constant.ShareContentType.mediaType.rawValue || shareType == Constant.ShareContentType.imageType.rawValue{
            if shareImgSrc.characters.count > 0  {
                if shareType == Constant.ShareContentType.mediaType.rawValue{
                    shareDict = [LDSDKShareContentTitleKey : self.shareTitle1,   LDSDKShareContentDescriptionKey : self.shareDesc,LDSDKShareContentWapUrlKey : self.shareArticleHref,LDSDKShareContentImageUrlKey : shareImgSrc]
                    qqShare.share(withContent: shareDict as! [AnyHashable: Any], shareModule:isShareToQQ == true ? 1 : 2, onComplete: {(success,error) in
                        if error != nil{
                            Utils.showError(context: self.view, errorStr: (error?.localizedDescription)!)
                            let javaScriptString : NSString = (String(format: "%@('{\"status\":400,\"msg\":\"分享失败\",data:\"\"}')", self.callBack) as NSString)
                            self.webView.evaluateJavaScript(javaScriptString as String, completionHandler: nil)
                        }
                        else{
                            let javaScriptString : NSString = (String(format: "%@('{\"status\":200,\"msg\":\"分享成功\",data:\"\"}')", self.callBack) as NSString)
                            self.webView.evaluateJavaScript(javaScriptString as String, completionHandler: nil)
                            
                        }
                        
                    })
                }
                else{
                    OriginalDownLoad().downloadWithUrlString(context:self.view,urlString: shareImgSrc, completionHandle: { (data) -> Void in
                        
                        if let mdata = data
                        {
                            let avatarImage = UIImage(data: mdata as Data)
                            if isShareToQQ{
                                shareDict = [LDSDKShareContentTitleKey : self.shareTitle1,   LDSDKShareContentDescriptionKey : self.shareDesc,LDSDKShareContentImageKey : avatarImage!]
                            }
                            else{
                                shareDict = [LDSDKShareContentTitleKey : self.shareTitle1,   LDSDKShareContentDescriptionKey : self.shareDesc,LDSDKShareContentWapUrlKey : "http://www.xinyidiandian.com",LDSDKShareContentImageUrlKey : self.shareImgSrc]
                            }
                            qqShare.share(withContent: shareDict as! [AnyHashable: Any], shareModule:isShareToQQ == true ? 1 : 2, onComplete: {(success,error) in
                                if error != nil{
                                    Utils.showError(context: self.view, errorStr: (error?.localizedDescription)!)
                                    let javaScriptString : NSString = (String(format: "%@('{\"status\":400,\"msg\":\"分享失败\",data:\"\"}')", self.callBack) as NSString)
                                    self.webView.evaluateJavaScript(javaScriptString as String, completionHandler: nil)
                                }
                                else{
                                    let javaScriptString : NSString = (String(format: "%@('{\"status\":200,\"msg\":\"分享成功\",data:\"\"}')", self.callBack) as NSString)
                                    self.webView.evaluateJavaScript(javaScriptString as String, completionHandler: nil)
                                    
                                }
                                
                            })
                        }})
                }
                
                
            }
        }
        else if shareType == Constant.ShareContentType.textType.rawValue{
            shareDict = [LDSDKShareContentTextKey : self.shareTitle1]
            qqShare.share(withContent: shareDict as! [AnyHashable: Any], shareModule:isShareToQQ == true ? 1 : 2, onComplete: ({(success,error) in
                if error != nil{
                    Utils.showError(context: self.view, errorStr: (error?.localizedDescription)!)
                    let javaScriptString : NSString = (String(format: "%@('{\"status\":400,\"msg\":\"分享失败\",data:\"\"}')", self.callBack) as NSString)
                    self.webView.evaluateJavaScript(javaScriptString as String, completionHandler: nil)
                }
                else{
                    let javaScriptString : NSString = (String(format: "%@('{\"status\":200,\"msg\":\"分享成功\",data:\"\"}')", self.callBack) as NSString)
                    self.webView.evaluateJavaScript(javaScriptString as String, completionHandler: nil)
                    
                }
                
            }))
        }
    }
    
    // MARK: -微信快照
    func shareSnapshotToWx(_ wxScene:Int32) {
        
    }
    
}
