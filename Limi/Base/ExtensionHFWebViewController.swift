//
//  ExtensionHFWebViewController.swift
//  Limi
//
//  Created by Richie on 16/7/18.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

/**
 给webview添加HeadView和FootView
 */

import UIKit
import WebKit

class ExtensionHFWebViewController: LMWebViewController {
    
    
    //MARK: OUTER 外部调用的属性
    var head_height:CGFloat {return _head_height}
    var foot_height:CGFloat {return _foot_height}
    
    fileprivate let minBoundceHeight:CGFloat = Constant.ScreenSizeV2.SCREEN_HEIGHT/2
    
    
    var refreshFootHeight:CGFloat {return self.webView.scrollView.refreshFooter != nil ? self.webView.scrollView.refreshFooter.iheight : 0}

    fileprivate var _head_height:CGFloat = 0
    fileprivate var _foot_height:CGFloat = 0
    
    var headView:UIView? {return _headView}
    var footView:UIView? {return _footView}

    
    
    //内部调用
    /**监听webView.scrollView.contentSize的变化*/
    fileprivate let KVO_CONTENTSIZE = "contentSize"
    //headView
    fileprivate var _headView:UIView?
    //footView
    fileprivate var _footView:UIView?

    fileprivate var oldOffset_Y:CGFloat = 0
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.webView.scrollView.showsVerticalScrollIndicator = true
        //添加监听
        webView.scrollView.addObserver(self, forKeyPath: KVO_CONTENTSIZE, options: NSKeyValueObservingOptions.new, context: nil)
    }
    

    
    
    //MARK: OUTER 外部调用的函数
    /**刷新头视图高度*/
    func refreshHeadHeight(_ newHeadHeight:CGFloat){
        oldOffset_Y = self.webView.scrollView.offsetY
        
        let oldInsetT = self.head_height
        let newInsetT = newHeadHeight
        
        if oldInsetT != newInsetT{
            //高度不同，把self.webView.scrollView的底部内边距的高度重置
            self.webView.scrollView.insetT -= oldInsetT
            self.webView.scrollView.insetT += newInsetT
            self.webView.scrollView.offsetY = oldOffset_Y
            _head_height = newInsetT
        }
        
        self.refreshUI()
    }
    
    /**刷新脚视图高度*/
    func refreshFootHeight(_ newFootHeight:CGFloat){
        oldOffset_Y = self.webView.scrollView.offsetY
        
        let oldInsetB = self.foot_height
        let newInsetB = newFootHeight
        
        if oldInsetB != newInsetB{
            
            //高度不同，把self.webView.scrollView的底部内边距的高度重置
            self.webView.scrollView.insetB -= oldInsetB
            self.webView.scrollView.insetB += newInsetB
            self.webView.scrollView.offsetY = oldOffset_Y
            _foot_height = newInsetB
        }
        
        self.refreshUI()
    }
    
    
    
    /**设置HeadView*/
    func setHeadView(_ headView:UIView){
        oldOffset_Y = self.webView.scrollView.offsetY
        
        let oldInsetT = self.head_height
        let newInsetT = headView.iheight
        
        
        if let oldHeadView = self.headView{
            //如果传进来的foot是同一个
            if oldHeadView == headView{
                //高度没有变化，不做任何处理
                if oldInsetT == newInsetT{
                    return
                }else{
                    //高度不同，把self.webView.scrollView的底部内边距的高度重置
                    self.webView.scrollView.insetT -= oldInsetT
                    self.webView.scrollView.insetT += newInsetT
                    _head_height = newInsetT
                }
            }else{
                //如果不是同一个foot，把老的移除，添加新的
                oldHeadView.removeFromSuperview()
                self.webView.scrollView.insetT -= oldInsetT
                
                self.webView.scrollView.addSubview(headView)
                self.webView.scrollView.insetT += newInsetT
                _head_height = newInsetT
            }
            
        }else{
            //如果以前没有设置过footView
            self.webView.scrollView.addSubview(headView)
            self.webView.scrollView.insetT += newInsetT
            _head_height = newInsetT
            
            //第一次加载视图，使它显示出来
            self.webView.scrollView.offsetY = -self.webView.scrollView.insetT
        }
        
        self.webView.scrollView.offsetY = oldOffset_Y
        self._headView = headView
        self.refreshUI()
    }
    
    
    /**设置FootView*/
    func setFootView(_ footView:UIView){
        oldOffset_Y = self.webView.scrollView.offsetY
        
        let oldInsetB = self.foot_height
        let newInsetB = footView.iheight
        
        if let oldFootView = self.footView{
            //如果传进来的foot是同一个
            if oldFootView == footView{
                //高度没有变化，不做任何处理
                if oldInsetB == newInsetB{
                    return
                }else{
                    //高度不同，把self.webView.scrollView的底部内边距的高度重置
                    self.webView.scrollView.insetB -= oldInsetB
                    self.webView.scrollView.insetB += newInsetB
                    _foot_height = newInsetB
                }
            }else{
                //如果不是同一个foot，把老的移除，添加新的
                oldFootView.removeFromSuperview()
                self.webView.scrollView.insetB -= oldInsetB
                
                self.webView.scrollView.addSubview(footView)
                self.webView.scrollView.insetB += newInsetB
                _foot_height = newInsetB
            }
            
        }else{
            //如果以前没有设置过footView
            self.webView.scrollView.addSubview(footView)
            self.webView.scrollView.insetB += newInsetB
            _foot_height = newInsetB
        }
        
        self.webView.scrollView.offsetY = oldOffset_Y
        self._footView = footView
        self.refreshUI()
    }
    
    
    override func scrollToTop() {
//        UIView.animateWithDuration(0.25) { 
            self.webView.scrollView.offsetY = -self.head_height
            self.scrollViewDidScroll(self.webView.scrollView)
//        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    deinit {
        webView.scrollView.removeObserver(self, forKeyPath: KVO_CONTENTSIZE)
    }
    
}



extension ExtensionHFWebViewController{
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        //webView.contentSize changed
        if keyPath == KVO_CONTENTSIZE {
            print(change?.description)
        }
        self.refreshUI()
//        self.webView.scrollView.contentW = Constant.ScreenSizeV2.SCREEN_WIDTH
    }

    /**注：子类重写需调用父类 super.scrollViewDidScroll(_:) */
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        super.scrollViewDidScroll(scrollView)
        
        if scrollView.offsetY + self.head_height > Constant.ScreenSizeV2.SCREEN_HEIGHT - (MARGIN_64 + MARGIN_44) {
            if self.scrollToTopButton.isHidden {
                self.scrollToTopButton.isHidden = false
            }
        }else {
            if !self.scrollToTopButton.isHidden {
                self.scrollToTopButton.isHidden = true
            }
        }
        
        refreshUI()
    }
    
    fileprivate func refreshUI(){
        
        /**
         在webView滑动到下方的时候
         隐藏head
         */
        
        if let mfootView = self.footView{
            mfootView.iy = self.webView.scrollView.contentH
        }
        
        
        if let mheadView = self.headView{
            mheadView.iy = -self.head_height
        }

        
        if webView.scrollView.contentH < minBoundceHeight{
            return
        }
        
        
        if webView.scrollView.offsetY > 0{
            
            if webView.scrollView.offsetY <= minBoundceHeight{
                self.webView.scrollView.insetT = 0
                self.webView.scrollView.insetB = 0
                
            }else if (webView.scrollView.offsetY <= (webView.scrollView.contentH - webView.iheight - minBoundceHeight)){
                self.webView.scrollView.insetT = 0
                self.webView.scrollView.insetB = 0
                
            }else if webView.scrollView.offsetY >= (webView.scrollView.contentH - webView.iheight - minBoundceHeight){
                
                self.webView.scrollView.insetT = self.foot_height + self.refreshFootHeight
                self.webView.scrollView.insetB = self.foot_height + self.refreshFootHeight
            }
            
        }else{
            self.webView.scrollView.insetT = self.head_height
            self.webView.scrollView.insetB = self.head_height
        }
    }
}
