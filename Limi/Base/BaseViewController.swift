//
//  BaseViewController.swift
//  Limi
//
//  Created by 程巍巍 on 4/29/15.
//  Copyright (c) 2015 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit


class BaseViewController: UIViewController,UIGestureRecognizerDelegate {
    

    
    var eventURL: URL?
    let backButton = UIButton()
    var navigationbarSeperatorLine: UIView?
    var isAuthingAddressBook : Bool = false

    fileprivate var _addressBook:ABAddressBook!
    fileprivate let loginDialog = LoginDialog()
    
    var navigationBarStyle:Int = 0{didSet{
        
            if navigationBarStyle == 1{
                
                //透明
                self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "titlebarGrey"), for: UIBarMetrics.default)
            }else if navigationBarStyle == 0{
                
                //不透明
                self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "titlebar"), for: UIBarMetrics.default)
            }
        }}
    
    var addressBook: ABAddressBook!{

        if _addressBook != nil{
            ABAddressBookRevert(_addressBook)
        }

        
        return self.getAddressBooks()
    }
    
    fileprivate func getAddressBooks()-> ABAddressBook!{

        let authStatus: ABAuthorizationStatus = ABAddressBookGetAuthorizationStatus()
        if _addressBook == nil || authStatus == .authorized {
            let ab: ABAddressBook = ABAddressBookCreateWithOptions(nil, nil).takeRetainedValue()
            
             _addressBook = ab
        }

        return _addressBook
    }
    
    
    var gesturBackEnable:Bool = true{
        didSet{
            self.navigationController?.interactivePopGestureRecognizer!.isEnabled = self.gesturBackEnable
        }
    }//是否开启右滑返回功能 默认开启
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        updateNaviBarColor()
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.view.backgroundColor = UIColor(rgba: Constant.common_background_color)
        
        self.customBackButton()
        if self.navigationItem.title == nil || self.navigationItem.title == ""
        {
            self.navigationItem.title = "心意点点"
            if let params : NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: eventURL?.query) as NSDictionary?{
                if let title  = params.object(forKey: "_title")as? String {
                    self.navigationItem.title = title
                }
            }
            
        }
        
        self.navigationController?.interactivePopGestureRecognizer!.delegate = self
    }
    
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
    }
    
    func customBackButton()
    {
        backButton.frame = CGRect(x: 0, y: 0, width: 36, height: 36)
        backButton.setBackgroundImage(UIImage(named: "common_back_icon"), for: UIControlState())
        backButton.addTarget(self, action: #selector(BaseViewController.backItemAction), for: UIControlEvents.touchUpInside)
        backButton.clipsToBounds = true
        backButton.tintColor = UIColor.red
        
        let item = UIBarButtonItem(customView: backButton)
        
        self.navigationItem.leftBarButtonItem = item
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if appDelegate.loginType == Constant.InLoginType.couponType.rawValue && appDelegate.isLoginSuccess == true{
            appDelegate.loginType = -1
            let couponList = CouponListViewController()
            couponList.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(couponList, animated: true)
        }
        else if appDelegate.loginType == Constant.InLoginType.packageItemType.rawValue && appDelegate.isLoginSuccess == true{
            appDelegate.loginType = -1
            if let url = UserDefaults.standard.url(forKey: "appRouteMLink"){
                let parserUrl = EventDispatcher.preParserURLWithNotInMap(url)
                 EventDispatcher.dispatch("\(parserUrl)", onNavigationController: appDelegate.currentViewController?.navigationController)
                UserDefaults.standard.removeObject(forKey: "appRouteMLink")
                UserDefaults.standard.synchronize()
            }
        }
        else if appDelegate.loginType == Constant.InLoginType.packageGuessType.rawValue && appDelegate.isLoginSuccess == true{
            appDelegate.loginType = -1
            if let url = UserDefaults.standard.url(forKey: "appRouteMLink"){
                let parserUrl = EventDispatcher.preParserURLWithNotInMap(url)
                EventDispatcher.dispatch("\(parserUrl)", onNavigationController: appDelegate.currentViewController?.navigationController)
                UserDefaults.standard.removeObject(forKey: "appRouteMLink")
                UserDefaults.standard.synchronize()
            }
        }
        
    }
    
    
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.interactivePopGestureRecognizer!.isEnabled = gesturBackEnable
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.currentViewController = self
        print("==========\(self)")
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // back
    func backItemAction()
    {
        let navi = self.navigationController
        
        let _ = navi?.popViewController(animated: true)
    }
    
    // navigationbar backgroundcolor
    func updateNaviBarColor()
    {
        let image = UIImage(named: "titlebar")
        self.navigationController?.navigationBar.setBackgroundImage(image, for: UIBarMetrics.default)
    }
    
    
    func getNavigationbarSeperatorLine()-> UIView?
    {
        let barbackground = self.navigationController?.navigationBar.subviews[0]
        
        let siv = barbackground?.subviews[0]
        
        return siv
    }
    
    
    func navigationBarAppearance(_ shouldSet:Bool)
    {
        if shouldSet
        {
            let image = UIImage(named: "titlebar")
            UINavigationBar.appearance().tintColor = UIColor.black
            UINavigationBar.appearance().setBackgroundImage(image, for: UIBarMetrics.default)
        }else
        {
            UINavigationBar.appearance().tintColor = UIColor.white
            UINavigationBar.appearance().setBackgroundImage(nil, for: UIBarMetrics.default)
        }
    }
       
    func didAuthAddressBook() {
        isAuthingAddressBook = true
        let authStatus: ABAuthorizationStatus = ABAddressBookGetAuthorizationStatus()
        switch authStatus {
        case .denied, .restricted:
            self.openAlertWhenNotAuthorized()
            isAuthingAddressBook = false
        case .authorized://已经授权
            self.pushContactViewController()
            isAuthingAddressBook = false
        case .notDetermined:
            isAuthingAddressBook = false
            ABAddressBookRequestAccessWithCompletion(self.addressBook, { (granted, error) -> Void in
                DispatchQueue.main.async {
                    if !granted {
                        let vc = GuideRefuseContactViewController()
                        vc.hidesBottomBarWhenPushed = true
                        self.navigationController?.pushViewController(vc, animated: true)

                    } else {
                        self.pushContactViewController()
                    }
                }
                
            })
        }
    }
    
    func requestAddressBookAccess() {
        ABAddressBookRequestAccessWithCompletion(addressBook) {
            (granted, error) in
            DispatchQueue.main.async {
                if !granted {
                    self.openAlertWhenNotAuthorized()
                } else {
                    self.pushContactViewController()
                }
            }
        }
    }
    
    func openAlertWhenNotAuthorized() {
        let alertController = UIAlertController(
            title: "无法获取通讯录权限",
            message: "请到\"设置-隐私-通讯录\"中打开心意点点读取手机通讯录的授权",
            preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "以后再说", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        let openAction = UIAlertAction(title: "前往“设置”", style: .default) { (action) in
            if let url = URL(string:UIApplicationOpenSettingsURLString) {
                UIApplication.shared.openURL(url)
            }
        }
        alertController.addAction(openAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func pushContactViewController() {
        let addVC = AddFromBookViewController()
        addVC.hidesBottomBarWhenPushed = true
        addVC.updateType = 0
        addVC.sysContacts = ABAddressBookCopyArrayOfAllPeople(addressBook).takeRetainedValue() as NSArray
        self.navigationController?.pushViewController(addVC, animated: true)
    }
    
    class func staticNavigationBarAppearance(_ shouldSet:Bool)
    {
        if shouldSet
        {
            let image = UIImage(named: "titlebar")
            
            UINavigationBar.appearance().tintColor = UIColor.black
            
            UINavigationBar.appearance().setBackgroundImage(image, for: UIBarMetrics.default)
        }else
        {
            UINavigationBar.appearance().tintColor = UIColor.white
            
            UINavigationBar.appearance().setBackgroundImage(nil, for: UIBarMetrics.default)
        }
    }
    
    
    ///用户登录
    func jumpToLogin(_ loginType:Constant.InLoginType,tipe:String?){
        
        let time: TimeInterval = 0.5
        
        if tipe != nil{
            Utils.showError(context: self.navigationController!.view, errorStr: tipe!)
        }
        
        let delay = DispatchTime.now() + Double(Int64(time * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        
        DispatchQueue.main.asyncAfter(deadline: delay) {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            
            appDelegate.loginType = loginType.rawValue
            self.loginDialog.controller = self
            self.loginDialog.show()
        }
    }
    
    
}


class BaseView: UIView
{
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor(rgba: Constant.common_background_color)
        
        defaultInit()
    }
    
    
    
    required init(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)!
        
        defaultInit()
    }
    
    func defaultInit(){
        
    }
}
