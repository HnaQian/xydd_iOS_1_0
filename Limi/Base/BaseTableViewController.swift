//
//  BaseTableViewController.swift
//  Limi
//
//  Created by 倪晅 on 16/1/5.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


enum viewControllerType {
    case All
    case Important
    case Birthday
    case Holiday
    case Memorial
    
}



class BaseTableViewController: BaseViewController, BaseTableDataSourceDelegate {
    
    
    
    weak var parentController : UIViewController?
    fileprivate var _tableView: UITableView?
    
    var useRefreshControl: Bool = true//
    var useLoadMoreControl: Bool = false
    var tableViewInsets: UIEdgeInsets = UIEdgeInsets.zero
    fileprivate var lastIndexPath = IndexPath(row: 0, section: 0)
    
    fileprivate var loginView: GiftReminderLoginView?
    
    var viewType:viewControllerType = .All
    
    
    var dataSource: BaseTableDataSource? {
        didSet {
            self.tableView.dataSource = dataSource
        }
    }
    
    var tableView: UITableView {
        get {
            if _tableView == nil {
                _tableView = UITableView(frame: CGRect.zero, style: .plain)
                _tableView!.autoresizingMask = [.flexibleHeight, .flexibleHeight]
                _tableView!.delegate = self;
                _tableView!.backgroundView = nil;
                _tableView!.separatorStyle = .none;
            }
            return _tableView!
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
 
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = {
            let footerView: UIView = UIView.init(frame: CGRect.zero)
            footerView.backgroundColor = UIColor.clear
            return footerView
        }()
        self.view.addSubview(self.tableView)
        
        self.setTableViewUI()
        
        if self.useRefreshControl {

            self.tableView.addHeaderRefresh({ [weak self]() -> Void in
                self?.dataSource?.refresh()
            })
            
        }
        if self.useLoadMoreControl {
            self.tableView.addFooterRefresh({ [weak self]() -> Void in
                self?.dataSource?.loadMore()
                })
            
        }
        
       
    }
   
    
    func setTableViewUI()
    {
        self.tableView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.view)
            let _ = make.left.equalTo(self.view)
            let _ = make.right.equalTo(self.view)
            let _ = make.bottom.equalTo(self.view)
        }
    }
    
    func pullRefresh() {
        if self.useRefreshControl {

            self.tableView.beginHeaderRefresh()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension BaseTableViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.dataSource == nil {
            return 0
        }
        return self.dataSource!.fixedHeightForCell()
    }
  
    func dataSource(_ dataSource: BaseTableDataSource, didFinishRefreshWithError error: NSError?) {
        self.tableView.reloadData()
        
        if self.useRefreshControl && self.tableView.isHeaderRefreshing
        {
            self.tableView.endHeaderRefresh()
        }
        if self.useRefreshControl
        {
            self.tableView.endFooterRefresh()
        }

        if self.viewType == .Important {
            self.tableView.showTipe(UIScrollTipeMode.noMuchMore)
        }
    }
    
    func dataSource(_ dataSource: BaseTableDataSource, didFinishLoadMoreWithError error: NSError?) {

        self.tableView.endFooterRefresh()
        
        if error == nil {
            self.tableView.reloadData()
        }
    }
    
    func dataSource(_ dataSource: BaseTableDataSource, didFinishNoMoreDataWithError error: NSError?) {

        self.tableView.showTipe(UIScrollTipeMode.noMuchMore)
    }
    
    func changeCellWhenOnClickImportantButton(_ indexpath: IndexPath,isdelete:Bool,changedData:BaseTableCellViewModel) {
        
        //必须是重要且isdelete=true时删除，其余都是刷新
        if self.viewType == .Important && isdelete{
            isChangeOfAll = 1
            isChangeOfMemorial = 1
            isChangeOfHoliday = 1
            isChangeOfBirthday = 1
            dataSource?.data.removeObject(at: (indexpath as NSIndexPath).row)
            if dataSource?.data.count == 0 {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "Request"), object: nil)
            }else{
                self.tableView.reloadData()
            }
        }
        else{
            /*
             var isChangeOfImpor = 0
             var isChangeOfAll = 0
             var isChangeOfBirthday = 0
             var isChangeOfHoliday = 0
             var isChangeOfMemorial = 0
             */
            if self.viewType == .All {
                isChangeOfImpor = 1
                isChangeOfMemorial = 1
                isChangeOfHoliday = 1
                isChangeOfBirthday = 1
            }else if self.viewType == .Birthday {
                isChangeOfImpor = 1
                isChangeOfAll = 1
            }else if self.viewType == .Holiday {
                isChangeOfImpor = 1
                isChangeOfAll = 1
            }else if self.viewType == .Memorial {
                isChangeOfImpor = 1
                isChangeOfAll = 1
            }
            
            
            self.dataSource?.data[(indexpath as NSIndexPath).row] = changedData
            
            if (lastIndexPath as NSIndexPath).row != (indexpath as NSIndexPath).row  && (lastIndexPath as NSIndexPath).row < dataSource?.data.count{
                self.tableView.reloadRows(at: [lastIndexPath], with: .none)
            }
            lastIndexPath = indexpath
        }
 
    }
}



