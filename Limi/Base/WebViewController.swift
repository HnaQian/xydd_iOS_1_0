//
//  WebViewController.swift
//  Limi
//
//  Created by 程巍巍 on 5/11/15.
//  Copyright (c) 2015 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit
import WebKit
import CoreData
import ContactsUI
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


@objc protocol TacticDetailDelegate{
    @objc optional func detailFavBtnClick(_ index: Int,isAdd: Bool)
    @objc optional func detailFavCanaelBtnClick(_ index: Int)
}

class WebViewController: BaseViewController,WKNavigationDelegate,WKUIDelegate,ABPeoplePickerNavigationControllerDelegate,LCActionSheetDelegate,UIScrollViewDelegate, PCDropdownMenuViewDataSource, PCDropdownMenuViewDelegate,CNContactPickerDelegate,LoginDialogDelegate {
    
    var type: Int? = 0
    var webView = WKWebView()
    fileprivate var progressView: UIProgressView!
    let backItem : UIButton = UIButton(type: .custom)
    let closeItem : UIButton = UIButton(type: .custom)
    let titlelabel = TitleLabel()
    var shareTitle = ""
    var shareTitle2 = ""
    var shareDesc = ""
    var shareImg = ""
    var shareFarticle_item = ""
    var callBack = ""
    var shareType = 0
    var RemindDetailId : Int = 0
    var isHaveBirthdayWiki : Bool = false
    fileprivate var menuView:UIView?
    var isStopInLogin : Bool = false
    var isFirstDecidePolicy: Bool = true
    var isHadEntryKSL: Bool = false
    let dropdownMenuView = PCDropdownMenuView()
    let menuWidth: CGFloat = 100.0
    var items: [PCDropdownMenuItem] = []
    
    
    //攻略详情参数
    var itemId: Int?
    let collectionButton = UIButton(type: .custom)
    var delegate: TacticDetailDelegate?
    var index : Int = 0
    var isCollect : Bool = false
    var backTopBtn = UIButton(frame:CGRect(x: 0, y: 0, width: 50, height: 50))//回到顶部Button
    var collectionStatus : Bool = false
    var tacticDetail: JSON?
    var packageItem: JSON?
    var isLogin : Bool = false
    fileprivate let loginDialog = LoginDialog()
    
    init(URL: Foundation.URL?){
        super.init(nibName: nil, bundle: nil)
        self.url = URL
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    var url: URL!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        initData()
    }
    
    func initData() {
        self.webView.navigationDelegate = self
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        webView.uiDelegate = self
        webView.addObserver(self, forKeyPath: "estimatedProgress", options: .new, context: nil)
        
        if self.type == Constant.WebViewType.tacticDetailType.rawValue{
            webView.scrollView.delegate = self
            self.navigationItem.title = ""
            UIApplication.shared.setStatusBarStyle(.default, animated: true)
        }
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if appDelegate.loginType == Constant.InLoginType.webViewType.rawValue && appDelegate.isLoginSuccess == true{
            appDelegate.loginType = -1
            if self.type == Constant.WebViewType.tacticDetailType.rawValue{
                self.loadTacticData(false)
            }
            else if self.type == Constant.WebViewType.packageItemType.rawValue{
                var javaScriptString = ""
                if let token : String = UserDefaults.standard.string(forKey: "UserToken"){
                    print(token)
                    print(self.callBack)
                    javaScriptString = (String(format: "%@('{\"status\":200,\"msg\":\"登录成功\",\"data\":{\"token\":\"%@\"}}')", self.callBack,token))
                }
                else{
                    javaScriptString = (String(format: "%@('{\"status\":400,\"msg\":\"登录失败\",\"data\":\"\"}')", self.callBack))
                }
                
                self.webView.evaluateJavaScript(javaScriptString as String, completionHandler: { (object, error) -> Void in
                    if error == nil && object != nil{
                        print(object)
                        print(error)
                    }
                })
                
                self.loadPageItemData(false)
            }
            else{
                var javaScriptString = ""
                if let token : String = UserDefaults.standard.string(forKey: "UserToken"){
                    print(token)
                    print(self.callBack)
                    javaScriptString = (String(format: "%@('{\"status\":200,\"msg\":\"登录成功\",\"data\":{\"token\":\"%@\"}}')", self.callBack,token))
                }
                else{
                    javaScriptString = (String(format: "%@('{\"status\":400,\"msg\":\"登录失败\",\"data\":\"\"}')", self.callBack))
                }
                
                self.webView.evaluateJavaScript(javaScriptString as String, completionHandler: { (object, error) -> Void in
                    if error == nil && object != nil{
                        //                self.webView.reload()
                    }
                })
                
            }
        }
        else if (appDelegate.loginType == Constant.InLoginType.webViewPackageCollectType.rawValue || appDelegate.loginType == Constant.InLoginType.webViewTacticDetailCollectType.rawValue) && appDelegate.isLoginSuccess == true{
            appDelegate.loginType = -1
            var collectionStatus : Bool = false
            if self.type == Constant.WebViewType.packageItemType.rawValue{
                collectionStatus = self.packageItem?["collect_status"].bool ?? false
            }
            else{
                collectionStatus = self.tacticDetail?["Collect_status"].bool ?? false
            }
            if collectionStatus == false{
                if let id = self.itemId {
                    
                    let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.collect_add, parameters: ["collect_type": (self.type == Constant.WebViewType.tacticDetailType.rawValue ? FavouriteType.article.rawValue : FavouriteType.packageItem.rawValue) as AnyObject, "obj_id": [id] as AnyObject], isNeedUserTokrn: true, isShowErrorStatuMsg: true,successHandler: {data, status, msg in
                        if status == 200
                        {
                            if self.type == Constant.WebViewType.tacticDetailType.rawValue{
                                self.delegate?.detailFavBtnClick!((self.index), isAdd: true)
                                self.tacticDetail?["Collect_status"].bool = true
                            }
                            else{
                                self.packageItem?["collect_status"].bool = true
                            }
                            self.collectionButton.isUserInteractionEnabled = true
                            
                            self.isCollect = true
                            
                            self.collectionButton.setBackgroundImage(UIImage(named: "collection_article_selected"), for: UIControlState())
                            let javaScriptString : NSString = "addLoveNum(1)"
                            self.webView.evaluateJavaScript(javaScriptString as String, completionHandler: nil)
                            Utils.showError(context: self.view, errorStr: "添加喜欢成功")
                        }
                        
                    })
                    
                    
                }
            }
            
        }
        
        self.isStopInLogin = false

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.type == Constant.WebViewType.tacticDetailType.rawValue{
            updateNaviBarColor()
            self.webView.scrollView.delegate = nil
            self.navigationItem.title = ""
        }
        
        self.webView.navigationDelegate = nil
        webView.uiDelegate = nil
        webView.removeObserver(self, forKeyPath: "estimatedProgress")
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.isLoginSuccess = false
        
        self.isStopInLogin = true
    }
    
    override func customBackButton() {
        let backView : UIView = UIView(frame: CGRect(x: 0, y: 0, width: 80, height: 44))
        backItem.frame = CGRect(x: 0, y: 4, width: 36, height: 36)
        backItem.setBackgroundImage(UIImage(named: "common_back_icon"), for: UIControlState())
        backItem.addTarget(self, action: #selector(WebViewController.clickedBackItem), for: UIControlEvents.touchUpInside)
        backView.addSubview(backItem)
        closeItem.frame = CGRect(x: 44, y: 4, width: 36, height: 36)
        closeItem.setBackgroundImage(UIImage(named: "webview_cancel"), for: UIControlState())
        closeItem.addTarget(self, action: #selector(WebViewController.clickedCloseItem), for: UIControlEvents.touchUpInside)
        closeItem.isHidden = true
        backView.addSubview(closeItem)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backView)
    }
    
    
    func isAddBirthWiki(_ notification: Notification)
    {
        let user_info:NSDictionary = (notification as NSNotification).userInfo! as NSDictionary
        if let isAdd = user_info.object(forKey: "isAddBirthWikiButton") as? Bool{
            self.isHaveBirthdayWiki = isAdd
            self.addRightItem(Constant.WebViewType.remindDetailType.rawValue,collectionStatus: false)
        }
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "isNeedAddBirthWiki"), object: nil)
    }
    
    
    
    
    func actionSheet(_ actionSheet: LCActionSheet!, didClickedButtonAt buttonIndex: Int) {
        
        if buttonIndex == 1{
            self.deleteRemind()
        }
    }
    
    func deleteRemind()
    {
       let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.user_relation_del, parameters: ["id":self.RemindDetailId as AnyObject], isNeedUserTokrn: true,isShowErrorStatuMsg: true, isNeedHud: true, hudTitle: "正在删除送礼提醒", successHandler: {data, status, msg in
            
            if status == 200
            {
                Utils.showError(context: self.view, errorStr: "删除成功")
                NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.notification_add_gift_reminder_success), object: nil)
               let _ = self.navigationController?.popViewController(animated: true)
            }
            
        })
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.translatesAutoresizingMaskIntoConstraints = false
        loginDialog.delegate = self
        if url != nil{
            if Utils.containsString("cardList", inString: url.absoluteString){
                //送贺卡
                if Utils.containsString("id=", inString: url.absoluteString)
                {
                    let tempArray = url.absoluteString.components(separatedBy: "id=")
                    if tempArray.count > 1 {
                        if (String(tempArray[1])?.contains("&"))! {
                            let tempArray2 = String(tempArray[1])?.components(separatedBy: "&")
                            Statistics.count(Statistics.Goods.goods_detail_share_click, andAttributes: ["holidayId":String(describing: tempArray2?[0])])
                        }else {
                            Statistics.count(Statistics.Goods.goods_detail_share_click, andAttributes: ["holidayId":String(tempArray[1])])
                        }
                    }
                }
            }
        }
        
        if self.type != Constant.WebViewType.tacticDetailType.rawValue{
            if url != nil {
                self.webView.load(Foundation.URLRequest(url: url))
            }
            
        }else{
            if let id = itemId{
                Statistics.count(Statistics.Tractic.article_detail_click, andAttributes: ["articleId":"\(id)"])
            }
        }
        self.view.addSubview(self.webView)
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "|-0-[webView]-0-|", options: [], metrics: nil, views: ["webView": webView]))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[webView]-0-|", options: [], metrics: nil, views: ["webView": webView]))
        if self.type == Constant.WebViewType.remindDetailType.rawValue{
            
            self.navigationItem.titleView = titlelabel
            self.navigationItem.title = nil
            self.addRightItem(Constant.WebViewType.remindDetailType.rawValue,collectionStatus: false)
            resetTitleViewWidth()
            
            NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: Constant.notification_add_gift_reminder_success), object: nil, queue: OperationQueue.main) { (notification) -> Void in
                self.webView.reload()
            }
        }
        else if self.type == Constant.WebViewType.tacticDetailType.rawValue{
            self.navigationItem.title = ""
            //关闭手势返回
            self.gesturBackEnable = false
            if let params : NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: eventURL?.query) as NSDictionary?{
                if let id : AnyObject  = params.object(forKey: "id") as AnyObject?{
                    itemId = id.intValue
                }
            }
            
            self.loadTacticData(true)
            
            backTopBtn.isHidden = true
            
            backTopBtn.iright = Constant.ScreenSize.SCREEN_WIDTH - backTopBtn.iwidth * 0.5
            
            backTopBtn.ibottom = Constant.ScreenSize.SCREEN_HEIGHT - backTopBtn.iwidth * 1.5
            
            backTopBtn.addTarget(self, action: #selector(WebViewController.backTopBtnAction(_:)), for: UIControlEvents.touchUpInside)
            
            backTopBtn.setImage(UIImage(named: "goods_detail_float_button"), for: UIControlState())
            
            self.view.addSubview(backTopBtn)
            
        }
        else if self.type == Constant.WebViewType.packageItemType.rawValue{
            self.navigationItem.title = ""
            //关闭手势返回
            self.gesturBackEnable = false
            if let params : NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: eventURL?.query) as NSDictionary?{
                if let id : AnyObject  = params.object(forKey: "id") as AnyObject?{
                    itemId = id.intValue
                }
            }
            
            self.loadPageItemData(true)
            
        }
        else{
            self.navigationItem.titleView = titlelabel
            self.navigationItem.title = nil
            resetTitleViewWidth()
        }
        
        progressChanged(0.2)
    }
    
    func wechatLoginSuccess() {
        initData()
    }
    
    func resetTitleViewWidth() {
        let leftViewbounds = self.navigationItem.leftBarButtonItem?.customView?.bounds
        let rightViewbounds = self.navigationItem.rightBarButtonItem?.customView?.bounds
        
        var frame:CGRect
        var maxWidth = leftViewbounds?.size.width > rightViewbounds?.size.width ? leftViewbounds?.size.width : rightViewbounds?.size.width
        maxWidth! += CGFloat(20)
        frame = titlelabel.frame
        frame.size.width = Constant.ScreenSize.SCREEN_WIDTH - maxWidth! * 2
        titlelabel.frame = frame
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if let newValue = change![NSKeyValueChangeKey.newKey] as? NSNumber {
            progressChanged(newValue <= 0.2 ? 0.2 : newValue)
        }
    }
    fileprivate func progressChanged(_ newValue: NSNumber) {
        if progressView == nil {
            progressView = UIProgressView()
            progressView.tintColor = UIColor(rgba: Constant.common_red_color)
            progressView.translatesAutoresizingMaskIntoConstraints = false
            self.view.addSubview(progressView)
            
            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "|-0-[progressView]-0-|", options: [], metrics: nil, views: ["progressView": progressView]))
            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[topGuide]-0-[progressView(2)]", options: [], metrics: nil, views: ["progressView": progressView, "topGuide": self.topLayoutGuide]))
        }
        
        progressView.progress = newValue.floatValue
        if progressView.progress == 1 {
            progressView.progress = 0
            UIView.animate(withDuration: 0.2, animations: { () -> Void in
                self.progressView.alpha = 0
            })
        } else if progressView.alpha == 0 {
            UIView.animate(withDuration: 0.2, animations: { () -> Void in
                self.progressView.alpha = 1
            })
        }
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        self.webView.stopLoading()
        
        
    }
    
    func addRightItem(_ type : Int,collectionStatus : Bool){
        if type == Constant.WebViewType.remindDetailType.rawValue{
            let backView : UIView = UIView(frame: CGRect(x: 0, y: 0, width: 80, height: 44))
            let BirthWikiButton = UIButton(type: .custom)
            BirthWikiButton.frame = CGRect(x: 0, y: 4, width: 36, height: 36)
            BirthWikiButton.setBackgroundImage(UIImage(named: "birthday_lib"), for: UIControlState())
            BirthWikiButton.addTarget(self, action: #selector(WebViewController.BirthWikiButtonClick), for: UIControlEvents.touchUpInside)
            backView.addSubview(BirthWikiButton)
            
            let RemindDetailEditButton = UIButton(type: .custom)
            RemindDetailEditButton.frame = CGRect(x: 44, y: 4, width: 36, height: 36)
            RemindDetailEditButton.setBackgroundImage(UIImage(named: "messagercenter_right_icon"), for: UIControlState())
            RemindDetailEditButton.addTarget(self, action: #selector(WebViewController.RemindDetailEditButtonClick), for: UIControlEvents.touchUpInside)
            backView.addSubview(RemindDetailEditButton)
            if self.isHaveBirthdayWiki{
                self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: backView)
            }
            else{
                self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: RemindDetailEditButton)
            }
        }
        else if type == Constant.WebViewType.tacticDetailType.rawValue{
            collectionButton.frame = CGRect(x: 0, y: 4, width: 36, height: 36)
            collectionButton.setBackgroundImage(UIImage(named: collectionStatus == true ? "collection_article_selected" : "collection_article_normal" ), for: UIControlState())
            collectionButton.addTarget(self, action: #selector(WebViewController.collect), for: UIControlEvents.touchUpInside)
            
            
            let shareButton = UIButton(type: .custom)
            shareButton.frame = CGRect(x: 0, y: 4, width: 36, height: 36)
            shareButton.setBackgroundImage(UIImage(named: "common_share_icon"), for: UIControlState())
            shareButton.addTarget(self, action: #selector(WebViewController.share), for: UIControlEvents.touchUpInside)
            self.navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: shareButton),UIBarButtonItem(customView: collectionButton)]
        }
        else if type == Constant.WebViewType.packageItemType.rawValue{
            collectionButton.frame = CGRect(x: 0, y: 4, width: 36, height: 36)
            collectionButton.setBackgroundImage(UIImage(named: collectionStatus == true ? "collection_article_selected" : "collection_article_normal" ), for: UIControlState())
            collectionButton.addTarget(self, action: #selector(WebViewController.collect), for: UIControlEvents.touchUpInside)
            
            
            let shareButton = UIButton(type: .custom)
            shareButton.frame = CGRect(x: 0, y: 4, width: 36, height: 36)
            shareButton.setBackgroundImage(UIImage(named: "common_share_icon"), for: UIControlState())
            shareButton.addTarget(self, action: #selector(WebViewController.share), for: UIControlEvents.touchUpInside)
            self.navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: shareButton),UIBarButtonItem(customView: collectionButton)]
        }
            
        else{
            let shareButton = UIButton(type: .custom)
            shareButton.frame = CGRect(x: 0, y: 4, width: 36, height: 36)
            shareButton.setBackgroundImage(UIImage(named: "common_share_icon"), for: UIControlState())
            shareButton.addTarget(self, action: #selector(WebViewController.share), for: UIControlEvents.touchUpInside)
            
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: shareButton)
        }
        
    }
    
    func loadTacticData(_ isNeedRefresh : Bool) {
        
        var params = [String: AnyObject]()
        params["article_id"] = self.itemId as AnyObject?
        let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.article_detail, parameters: params, isNeedUserTokrn: true, isShowErrorStatuMsg: true, successHandler: {data, status, msg in
            if status != 200 {
                return
            }
            if let tacticInfo: JSON = JSON(data as AnyObject)["data"]["article"] as JSON?{
                self.tacticDetail = tacticInfo
                self.showView(isNeedRefresh)
            }
        })
    }
    
    func loadPageItemData(_ isNeedRefresh : Bool) {
        
        var params = [String: AnyObject]()
        params["package_id"] = self.itemId as AnyObject?
//        print(self.itemId
        let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.package_item, parameters: params, isNeedUserTokrn: true, isShowErrorStatuMsg: true, successHandler: {data, status, msg in
            if status != 200 {
                return
            }
            if let packageInfo: JSON = JSON(data as AnyObject)["data"]["package"] as JSON?{
                self.packageItem = packageInfo
                self.showView(isNeedRefresh)
            }
        })
    }
    
    func showView(_ isNeedRefresh : Bool){
        if self.type == Constant.WebViewType.tacticDetailType.rawValue{
            if let id = tacticDetail?["Id"].int {
                self.itemId = id
            }
            
            if let title = tacticDetail?["Title"].stringValue{
                self.navigationItem.titleView = self.titlelabel
                resetTitleViewWidth()
                self.titlelabel.text = title
            }
            if isNeedRefresh{
            if let detailURL = tacticDetail?["Detail"].string {
                if let url = URL(string: detailURL) {
                    if Utils.containsString("need_token=1", inString: url.absoluteString)
                    {
                        if let token : String = UserDefaults.standard.string(forKey: "UserToken"){
                            let newUrl =  String(format: "%@&token=%@", url.absoluteString,token)
                            webView.load(Foundation.URLRequest(url: URL(string: newUrl)!))
                        }
                    }
                    else{
                        webView.load(Foundation.URLRequest(url: url))
                    }
                }
            }
            }
            collectionStatus = tacticDetail?["Collect_status"].bool ?? false
            self.addRightItem(Constant.WebViewType.tacticDetailType.rawValue,collectionStatus: collectionStatus)
        }
        else if self.type == Constant.WebViewType.packageItemType.rawValue{
            if let id = packageItem?["id"].int {
                self.itemId = id
            }
            
            if let title = packageItem?["title"].stringValue{
                self.navigationItem.titleView = self.titlelabel
                resetTitleViewWidth()
                self.titlelabel.text = title
            }
            if isNeedRefresh{
            if let detailURL = packageItem?["url"].string {
                if let url = URL(string: detailURL) {
                    if Utils.containsString("need_token=1", inString: url.absoluteString)
                    {
                        if let token : String = UserDefaults.standard.string(forKey: "UserToken"){
                            let newUrl =  String(format: "%@&token=%@", url.absoluteString,token)
                            webView.load(Foundation.URLRequest(url: URL(string: newUrl)!))
                        }
                    }
                    else{
                        webView.load(Foundation.URLRequest(url: url))
                    }
                }
            }
            }
            collectionStatus = packageItem?["collect_status"].bool ?? false
            self.addRightItem(Constant.WebViewType.packageItemType.rawValue,collectionStatus: collectionStatus)
        }
        
    }
    
    
    
    func backTopBtnAction(_ btn:UIButton)//回到顶部
    {
        self.webView.scrollView.scrollRectToVisible(self.webView.scrollView.frame, animated: true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        if scrollView.contentOffset.y > self.view.iheight
        {
            backTopBtn.isHidden = false
        }else
        {
            backTopBtn.isHidden = true
        }
    }
    
    
    
    func share(){
        if self.isHadEntryKSL{
            let shareConfigString : NSString = "app_share_config()"
            webView.evaluateJavaScript(shareConfigString as String, completionHandler: { (object, error) -> Void in
                if error == nil && object != nil{
                    
                }
            })
            
        }
        else{
            let shareConfigString : NSString = "js_share()"
            webView.evaluateJavaScript(shareConfigString as String, completionHandler: nil)
        }
    }
    
    func collect(){
        
        if let id = self.itemId {
            if UserInfoManager.didLogin == true{
                collectionButton.isUserInteractionEnabled = false
                var collectionStatus : Bool = false
                if self.type == Constant.WebViewType.tacticDetailType.rawValue{
                    collectionStatus = self.tacticDetail?["Collect_status"].bool ?? false
                }
                else{
                    collectionStatus = self.packageItem?["collect_status"].bool ?? false
                }
                
                if collectionStatus{
                    let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.collect_del, parameters: ["collect_type": self.type == Constant.WebViewType.tacticDetailType.rawValue ?FavouriteType.article.rawValue as AnyObject : FavouriteType.packageItem.rawValue as AnyObject, "obj_id": id as AnyObject], isNeedUserTokrn: true, isShowErrorStatuMsg: true,successHandler: {data, status, msg in
                        if status == 200
                        {
                            if self.type == Constant.WebViewType.tacticDetailType.rawValue{
                                self.delegate?.detailFavBtnClick!((self.index), isAdd: false)
                                self.tacticDetail?["Collect_status"].bool = false
                            }
                            else{
                                self.packageItem?["collect_status"].bool = false
                            }
                            self.isCollect = false
                            self.collectionButton.isUserInteractionEnabled = true
                            
                            
                            self.collectionButton.setBackgroundImage(UIImage(named: "collection_article_normal"), for: UIControlState())
                            
                            let javaScriptString : NSString = "addLoveNum(0)"
                            self.webView.evaluateJavaScript(javaScriptString as String, completionHandler: nil)
                            Utils.showError(context: self.view, errorStr: "取消喜欢成功")
                        }
                    })
                    
                }
                else{
                    Statistics.count(Statistics.Tractic.article_detail_like_click, andAttributes: ["aid":String(id)])
                    
                    let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.collect_add, parameters: ["collect_type": self.type == Constant.WebViewType.tacticDetailType.rawValue ?FavouriteType.article.rawValue as AnyObject : FavouriteType.packageItem.rawValue as AnyObject, "obj_id": [id] as AnyObject], isNeedUserTokrn: true, isShowErrorStatuMsg: true,successHandler: {data, status, msg in
                        if status == 200
                        {
                            if self.type == Constant.WebViewType.tacticDetailType.rawValue{
                                self.delegate?.detailFavBtnClick!((self.index), isAdd: true)
                                self.tacticDetail?["Collect_status"].bool = true
                            }
                            else{
                                self.packageItem?["collect_status"].bool = true
                            }
                            self.collectionButton.isUserInteractionEnabled = true
                            
                            self.isCollect = true
                            
                            self.collectionButton.setBackgroundImage(UIImage(named: "collection_article_selected"), for: UIControlState())
                            let javaScriptString : NSString = "addLoveNum(1)"
                            self.webView.evaluateJavaScript(javaScriptString as String, completionHandler: nil)
                            Utils.showError(context: self.view, errorStr: "添加喜欢成功")
                        }
                        
                    })
                }
            }
            else{
               
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                if self.type == Constant.WebViewType.tacticDetailType.rawValue{
                    appDelegate.loginType = Constant.InLoginType.webViewTacticDetailCollectType.rawValue
                }
                else if self.type == Constant.WebViewType.packageItemType.rawValue{
                    appDelegate.loginType = Constant.InLoginType.webViewPackageCollectType.rawValue
                }
                self.loginDialog.controller = self
                self.loginDialog.show()
                
                if (type == Constant.WebViewType.tacticDetailType.rawValue) || (type == Constant.WebViewType.packageItemType.rawValue)
                {
                    Utils.showError(context: self.navigationController!.view, errorStr: "登录后才能添加喜欢")
                }
            }
        }
        
        
    }
    
    func BirthWikiButtonClick(){
        let birthWikiUrl = SystemInfoRequestManager.shareInstance().birthday_wiki_other
        
        let url = String(format: "%@?id=%d",birthWikiUrl,self.RemindDetailId)
        let webVC = WebViewController(URL: URL(string: url)!)
        self.navigationController?.pushViewController(webVC, animated: true)
    }
    
    
    func RemindDetailEditButtonClick(){
        dropdownMenuView.dataSource = self
        dropdownMenuView.delegate = self
        
        items = [PCDropdownMenuItem(name: "编辑", icon: nil),
                 PCDropdownMenuItem(name: "删除", icon: nil)]
        dropdownMenuView.showWithAnimate(true)
    }
    
    func getIsShowShare(){
        let shareConfigString : NSString = "js_menu()"
        webView.evaluateJavaScript(shareConfigString as String, completionHandler: nil)
    }
    
    
    
    func clickedBackItem(){
        if (self.webView.canGoBack) {
            self.webView.goBack()
            self.closeItem.isHidden = false
        }else{
            self.clickedCloseItem()
        }
    }
    
    
    func clickedCloseItem(){
        //        if self.type == Constant.WebViewType.TacticDetailType.rawValue{
        ////            if self.isCollect == false{
        ////                self.delegate?.detailFavCanaelBtnClick!(self.index)
        ////            }
        //        }
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping () -> Void) {
        //
        completionHandler()
        let alert = UIAlertController(title: "提示", message: "\(message)", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "确定", style: .default, handler:nil))
        alert.addAction(UIAlertAction(title: "取消", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if (self.webView.canGoBack) {
            self.closeItem.isHidden = false
        }
        print(navigationAction.request.url)
        if navigationAction.request.url!.scheme == "local"
        {
            //自选礼盒
            if navigationAction.request.url!.host == "appPackageGoods"{
                decisionHandler(.cancel)
                //如果没有登录去登录
                if !UserInfoManager.didLogin{
                    self.Inlogin()
                }else{
                    //获取数据 goods_list
                    if let params : NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: navigationAction.request.url!.query) as NSDictionary?{

                        if let configData = params.object(forKey: "data"),
                            let configJsonData = String(describing: configData).parseJSONString{
                            print(configJsonData)
                            if let goods_list = configJsonData["goods_list"] as? [AnyObject]{
                                
                                if goods_list.count > 0{
                                    
                                    var giftList = [GoodsPackageModel]()
                                    for dic in goods_list{
                                        giftList.append(GoodsPackageModel(dic as! [String:AnyObject]))
                                    }
                                    
                                    let VC =  OrderGeneratorViewControllerV1.orderGenerator(giftList, sourceType: 3)
                                    VC.setJspParameters(configJsonData as! [String : AnyObject])
                                    self.navigationController?.pushViewController(VC, animated: true)
                                }
                            }
                            
                            print(configJsonData)
                        }
                    }
                    
                    decisionHandler(.cancel)
                }
            }
                //分享
            else if navigationAction.request.url!.host == "appShare"{
                
                if self.type == Constant.WebViewType.tacticDetailType.rawValue{
                    
                    if let id = self.itemId{
                        Statistics.count(Statistics.Tractic.article_detail_share_click, andAttributes: ["aid":String(id)])
                    }
                }else{
                    
                    if url != nil{
                        Statistics.count(Statistics.GiftRemind.remindshare, andAttributes: ["url":url.absoluteString])
                        Statistics.count(Statistics.GiftRemind.remind_sharepicture_click, andAttributes: ["url":url.absoluteString])
                    }
                }
                
                if let params : NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: navigationAction.request.url!.query) as NSDictionary?{
                    if let configData  = params.object(forKey: "data") as? String{
                        if let configJsonData = String(configData).parseJSONString{
                            if let title1 : String = configJsonData["title"] as? String{
                                self.shareTitle = title1
                            }
                            if let title2 : String = configJsonData["title2"] as? String{
                                self.shareTitle2 = title2
                            }
                            if let shareDesc : String = configJsonData["desc"] as? String{
                                self.shareDesc = shareDesc
                            }
                            if let shareImg : String = configJsonData["img"] as? String{
                                self.shareImg = shareImg
                            }
                            if let shareFarticle_item : String = configJsonData["url"] as? String{
                                self.shareFarticle_item = shareFarticle_item
                            }
                            if let shareCallBack : String = configJsonData["callback"] as? String{
                                self.callBack = shareCallBack
                            }
                            
                            if let sharetype : String = configJsonData["type"] as? String{
                                if sharetype == "common"{
                                    
                                    if self.shareImg != "" && self.shareFarticle_item != ""{
                                        self.shareConfig(Constant.ShareContentType.mediaType.rawValue)
                                    }
                                }
                                else if sharetype == "img"{
                                    if self.shareImg != ""{
                                        self.shareConfig(Constant.ShareContentType.imageType.rawValue)
                                    }
                                }
                                else if sharetype == "title"{
                                    if self.shareTitle != ""{
                                        self.shareConfig(Constant.ShareContentType.textType.rawValue)
                                    }
                                }
                            }
                            else{
                                Utils.showError(context: self.view, errorStr: "数据获取失败")
                            }
                            self.isFirstDecidePolicy = false
                            decisionHandler(.cancel)
                        }
                        
                    }
                }
            }
            else if navigationAction.request.url!.host == "share_config"{
                if let params : NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: navigationAction.request.url!.query) as NSDictionary?{
                    
                    if let title  = params.object(forKey: "title") as? String{
                        self.shareTitle = title
                    }
                    
                    if let title2  = params.object(forKey: "title2") as? String{
                        self.shareTitle2 = title2
                    }
                    
                    if let desc  = params.object(forKey: "desc") as? String{
                        self.shareDesc = desc
                    }
                    
                    if let shareImg  = params.object(forKey: "img") as? String{
                        self.shareImg = shareImg
                    }
                    
                    if let url  = params.object(forKey: "url") as? String{
                        self.shareFarticle_item = url
                    }
                    if self.shareDesc != "" && self.shareImg != "" && self.shareFarticle_item != ""{
                        self.shareConfig(Constant.ShareContentType.mediaType.rawValue)
                    }
                    else{
                        Utils.showError(context: self.view, errorStr: "数据获取失败")
                    }
                    
                    decisionHandler(.cancel)
                    
                }
            }
                
            else if navigationAction.request.url!.host == "appPay"{
                if UserInfoManager.didLogin == true{
                    if let params : NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: navigationAction.request.url!.query) as NSDictionary?{
                        if let configData = params.object(forKey: "data") as? String{
                            if let configJsonData = String(configData).parseJSONString{
                                if let oid : Int = configJsonData["oid"] as? Int{
                                    self.navigationController?.pushViewController(PayViewController.newPayViewController(oid,canclePay:.forward), animated: true)
                                }
                            }
                        }
                    }
                }
                else{
                    self.tipAndLogin("登录之后才能完成付款")
                }
                
            }
            else if navigationAction.request.url!.host == "appLogin"{
                if let params : NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: navigationAction.request.url!.query) as NSDictionary?{
                    if let configData  = params.object(forKey: "data") as? String{
                        if let configJsonData = String(configData).parseJSONString{
                            if let shareCallBack : String = configJsonData["callback"] as? String{
                                self.callBack = shareCallBack
                            }
                        }
                    }
                }
                self.Inlogin()
            }
            else if navigationAction.request.url!.host == "appPhoneBook"{
                if let params : NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: navigationAction.request.url!.query) as NSDictionary?{
                    if let configData = params.object(forKey: "data"){
                        if let configJsonData = String(describing: configData).parseJSONString{
                            if let callBack : String = configJsonData["callback"] as? String{
                                self.callBack = callBack
                            }
                        }
                    }
                }
                self.openContact()
            }
            else if navigationAction.request.url!.host == "appReceiveCoupon"{
                if UserInfoManager.didLogin == true{
                    if let params : NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: navigationAction.request.url!.query) as NSDictionary?{
                        if let configData = params.object(forKey: "data"){
                            if let configJsonData = String(describing: configData).parseJSONString{
                                if let coupon_ids : NSArray = configJsonData["coupon_ids"] as? NSArray{
                                    self.receCouponWithQuery(coupon_ids)
                                    if let callBack = configJsonData["callback"] as? String{
                                        self.callBack = callBack
                                    }
                                }}}
                    }
                    
                }
                else{
                    self.tipAndLogin("")
                }
            }
            else if navigationAction.request.url!.host == "appToast"{
                if let params : NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: navigationAction.request.url!.query) as NSDictionary?{
                    if let configData = params.object(forKey: "data"){
                        if let configJsonData = String(describing: configData).parseJSONString{
                            if let tip : String = configJsonData["content"] as? String{
                                if let callBack = configJsonData["callback"] as? String{
                                    self.callBack = callBack
                                }
                                self.showToast(tip)
                            }
                            
                        }
                    }
                }
            }
            else if navigationAction.request.url!.host == "appMenu"{
                if let params : NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: navigationAction.request.url!.query) as NSDictionary?{
                    if let configData = params.object(forKey: "data"){
                        if let configJsonData = String(describing: configData).parseJSONString{
                            if let share = configJsonData["share"] as? [String:AnyObject]{
                                if let show = share["show"] as? Bool{
                                    if show {
                                        self.addRightItem(Constant.WebViewType.commonType.rawValue,collectionStatus: false)
                                    }
                                    else{
                                        
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else if navigationAction.request.url!.host == "index"
            {
                let userInfo: Dictionary<String,Int>! = [
                    "index": 0,
                    ]
                NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.show_rootController), object: nil, userInfo: userInfo)
               let _ =  self.navigationController?.popToRootViewController(animated: true)
                
            }else if navigationAction.request.url!.host ==  "appGiftService"{
                
                Statistics.count(Statistics.Order.order_service_finish_click)
                
                if let params : NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: navigationAction.request.url!.query) as NSDictionary?{
                    
                    if let configData = params.object(forKey: "data"),
                        let configJsonData = String(describing: configData).parseJSONString,
                        let service_ids = configJsonData["service_ids"] as? NSArray,
                        let service_prices = configJsonData["price"] as? NSArray{
                        
                        var ids = [Int]()
                        var prices = [Double]()
                        
                        for id in service_ids{
                            if let idstr = (id as? NSString){
                                ids.append(idstr.integerValue)
                            }
                        }
                        
                        for price in service_prices{
                            if let  p = price as? Double{
                                prices.append(p)
                            }
                        }
                        
                        WishSendUnitView.updateServiceCost(prices)
                        WishSendUnitView.updateServiceids(ids)
                        WishSendUnitView.serviceIdsEffected = true
                    }
                    let _ = self.navigationController?.popViewController(animated: true)
                }
                else{
                    Utils.showError(context: self.view, errorStr: "数据获取失败")
                }
                
                
                
            }
            else{
                
                if EventDispatcher.isPureNumberWithGoodsId(navigationAction.request.url!) {
                    EventDispatcher.dispatch("\(navigationAction.request.url!)", onNavigationController: self.navigationController)
                }else {
                    Utils.showError(context: self.view, errorStr: "数据获取失败")
                }
            }
            self.isFirstDecidePolicy = false
            decisionHandler(.cancel)
        }
        else if EventDispatcher.isNeedAppProcess(navigationAction.request.url!){
            
            if let params : NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: navigationAction.request.url!.query) as NSDictionary?{
                if let share_config = params.object(forKey: "service_ids") as? NSArray{
                    var ids = [Int]()
                    for id in share_config{
                        if let idstr = (id as? NSString){
                            ids.append(idstr.integerValue)
                        }
                        if ids.count == 3{
                            if  Utils.containsString("article_item", inString: (navigationAction.request as NSURLRequest).url!.absoluteString) {
                                Statistics.count(Statistics.GiftRemind.remind_article_click,andAttributes: ["id":String(describing: params["id"])])
                            }
                            WishSendUnitView.saveTempServiceids(ids)
                            WishSendUnitView.serviceIdsEffected = false
                        }
                    }
                }else {
                    //送鲜花
                    if params.allKeys.count == 2 {
                        Statistics.count(Statistics.Service.service_package_select_click,andAttributes: ["id":String(describing: params["id"])])
                         Statistics.count(Statistics.GiftRemind.remind_flowerbutton_click,andAttributes: ["id":String(describing: params["id"])])
                    }
                }
            }
            
            if  Utils.containsString("goods_list", inString: (navigationAction.request as NSURLRequest).url!.absoluteString) {
                if Utils.containsString("code=", inString: (navigationAction.request as NSURLRequest).url!.absoluteString) {
                     Statistics.count(Statistics.GiftRemind.remind_giftbutton_click)
                }else {
                    Statistics.count(Statistics.GiftRemind.remind_giftlist_click)
                }
               
            }
            if EventDispatcher.isPureNumberWithGoodsId(navigationAction.request.url!) {
                EventDispatcher.dispatch("\(navigationAction.request.url!)", onNavigationController: self.navigationController)
            }else {
                Utils.showError(context: self.view, errorStr: "数据获取失败")
            }
            self.isFirstDecidePolicy = false
            decisionHandler(.cancel)
        }
        else if let params : NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: navigationAction.request.url!.query) as NSDictionary?{
            if let share_config = params.object(forKey: "share_config"){
                if share_config as! String == "ksl"{
                    //ji
                    if self.isFirstDecidePolicy == false{
                        EventDispatcher.dispatch("\(navigationAction.request.url!)", onNavigationController: self.navigationController)
                        self.isFirstDecidePolicy = false
                        decisionHandler(.cancel)
                    }
                    else{
                        self.isHadEntryKSL = true
                        self.addRightItem(Constant.WebViewType.commonType.rawValue,collectionStatus: false)
                        self.isFirstDecidePolicy = false
                        decisionHandler(.allow)
                    }
                }
                else{
                    self.isFirstDecidePolicy = false
                    decisionHandler(.allow)
                }
            }
            else{
                print(navigationAction.request.url?.description)
                self.isFirstDecidePolicy = false
                decisionHandler(.allow)
            }
        }
        else{
            self.isFirstDecidePolicy = false
            decisionHandler(.allow)
        }
    }
    
    
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        var title : String = "心意点点"
        let javaScriptString : NSString = "document.title"
        
        self.getIsShowShare()
        webView.evaluateJavaScript(javaScriptString as String, completionHandler: { (object, error) -> Void in
            if error == nil && object != nil{
                title = object as! String
                self.navigationItem.titleView = self.titlelabel
                self.resetTitleViewWidth()
                self.titlelabel.text = title
            }
        })
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error){
    }
    
    func shareConfig(_ shareType:Int){
        
        let Objs : NSArray = [MenuLabel.createlabelIconName("share_icon_wechat_friend", title: "微信好友"),MenuLabel.createlabelIconName("share_icon_wechat_timeline", title: "微信朋友圈"),MenuLabel.createlabelIconName("share_icon_qq_friend", title: "QQ好友"),MenuLabel.createlabelIconName("share_icon_qq_zone", title: "QQ空间")]
        HyPopMenuView.creatingPopMenuObjectItmes(Objs as! [MenuLabel],isGeneral:true, topView: nil, selectdCompletionBlock: {index  in
            if index == 0{
                self.shareToWx(Int32(WXSceneSession.rawValue),shareType: shareType)
            }
            else if index == 1{
                self.shareToWx(Int32(WXSceneTimeline.rawValue),shareType: shareType)
            }
            else if index == 2{
                self.shareToQQ(true,shareType: shareType)
            }
            else if index == 3{
                self.shareToQQ(false,shareType: shareType)
            }
            
            }
        )
    }
    
    
    func receCouponWithQuery(_ coupon_ids : NSArray){
        //let array1 = coupon_ids.componentsSeparatedByString(",")
        let array1 = coupon_ids
        var couponIdArr: [Int] = []
        for item in array1 {
            couponIdArr.append(item as! Int)
        }
        if !couponIdArr.isEmpty {
            self.receiveCoupon(couponIdArr, webView: webView)
        }
    }
    
    // 领取优惠券
    func receiveCoupon(_ array: [Int] ,webView: WKWebView) {
        let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.add_user_coupon, parameters: [ "coupon_id": array as AnyObject], isNeedUserTokrn: true,isShowErrorStatuMsg: true, isNeedHud: true, hudTitle: "正在领取",successHandler: {data, status, msg in
            if status != 200
            {
                let javaScriptString : NSString = (String(format: "%@('{\"status\":400,\"msg\":\"领取失败\",\"data\":\"\"}')", self.callBack) as NSString)
                
                self.webView.evaluateJavaScript(javaScriptString as String, completionHandler: nil)
                
                return
            }
            else{
                Utils.showError(context: self.view, errorStr: "领取成功")
                let uid : Int = UserDefaults.standard.integer(forKey: "UserUid")
                if uid != 0 {
                    let javaScriptString : String = (String(format: "%@({\"status\":200,\"msg\":\"领取成功\",\"data\":{\"uid\":\"%d\",\"coupon_ids\":\"%@\"}})", self.callBack,uid,array.debugDescription))
                    self.webView.evaluateJavaScript(javaScriptString as String, completionHandler: { (object, error) -> Void in
                        if error == nil && object != nil{
                        }
                    })
                }
                
                
                
            }
            
            
            
        })
        
        
    }
    
    
    func shareToWx(_ wxScene:Int32,shareType:Int){
        let wechatShare : LDSDKWXServiceImpl = LDSDKManager.getShareService(LDSDKPlatformType.weChat) as! LDSDKWXServiceImpl
        var shareDict:NSDictionary = NSDictionary()
        if shareType == Constant.ShareContentType.mediaType.rawValue || shareType == Constant.ShareContentType.imageType.rawValue{
            
            OriginalDownLoad().downloadWithUrlString(context:self.view,urlString: shareImg, completionHandle: { (data) -> Void in
                if let mdata = data
                {
                    
                    let avatarImage = UIImage(data: mdata as Data)
                    var title = ""
                    if wxScene == Int32(WXSceneTimeline.rawValue){
                        if self.shareTitle2 != ""{
                            title = self.shareTitle2
                        }
                        else{
                            title = self.shareTitle
                        }
                        
                    }
                    else{
                        title = self.shareTitle
                    }
                    
                    if shareType == Constant.ShareContentType.mediaType.rawValue{
                        shareDict = [LDSDKShareContentTitleKey : title,   LDSDKShareContentDescriptionKey : self.shareDesc,LDSDKShareContentWapUrlKey : self.shareFarticle_item,LDSDKShareContentImageKey : avatarImage!]
                    }
                    else{
                        shareDict = [LDSDKShareContentTitleKey : "",   LDSDKShareContentDescriptionKey : "",LDSDKShareContentImageKey : avatarImage!]
                    }
                    wechatShare.share(withContent: shareDict as! [AnyHashable: Any], shareModule:wxScene == Int32(WXSceneSession.rawValue) ? 1 : 2, onComplete: {(success,error) in
                        if error != nil{
                            Utils.showError(context: self.view, errorStr: (error?.localizedDescription)!)
                            let javaScriptString : NSString = (String(format: "%@('{\"status\":400,\"msg\":\"分享失败\",data:\"\"}')", self.callBack) as NSString)
                            self.webView.evaluateJavaScript(javaScriptString as String, completionHandler: nil)
                        }
                        else{
                            let javaScriptString : NSString = (String(format: "%@('{\"status\":200,\"msg\":\"分享成功\",data:\"\"}')", self.callBack) as NSString)
                            self.webView.evaluateJavaScript(javaScriptString as String, completionHandler: nil)
                            
                        }
                        
                    })
                }
            })
        }else if shareType == Constant.ShareContentType.textType.rawValue{
            shareDict = [LDSDKShareContentTextKey : self.shareTitle]
            wechatShare.share(withContent: shareDict as! [AnyHashable: Any], shareModule:wxScene == Int32(WXSceneSession.rawValue) ? 1 : 2, onComplete: {(success,error) in
                if error != nil{
                    Utils.showError(context: self.view, errorStr: (error?.localizedDescription)!)
                    let javaScriptString : NSString = (String(format: "%@('{\"status\":400,\"msg\":\"分享失败\",data:\"\"}')", self.callBack) as NSString)
                    self.webView.evaluateJavaScript(javaScriptString as String, completionHandler: nil)
                }
                else{
                    let javaScriptString : NSString = (String(format: "%@('{\"status\":200,\"msg\":\"分享成功\",data:\"\"}')", self.callBack) as NSString)
                    self.webView.evaluateJavaScript(javaScriptString as String, completionHandler: nil)
                    
                }
                
            })
        }
        
        
    }
    
    func shareToQQ(_ isShareToQQ : Bool,shareType:Int){
        let qqShare : LDSDKQQServiceImpl = LDSDKManager.getShareService(LDSDKPlatformType.QQ) as! LDSDKQQServiceImpl
        var shareDict:NSDictionary = NSDictionary()
        
        if shareType == Constant.ShareContentType.mediaType.rawValue || shareType == Constant.ShareContentType.imageType.rawValue{
            if shareImg.characters.count > 0  {
                if shareType == Constant.ShareContentType.mediaType.rawValue{
                    shareDict = [LDSDKShareContentTitleKey : self.shareTitle,   LDSDKShareContentDescriptionKey : self.shareDesc,LDSDKShareContentWapUrlKey : self.shareFarticle_item,LDSDKShareContentImageUrlKey : shareImg]
                    qqShare.share(withContent: shareDict as! [AnyHashable: Any], shareModule:isShareToQQ == true ? 1 : 2, onComplete: {(success,error) in
                        if error != nil{
                            Utils.showError(context: self.view, errorStr: (error?.localizedDescription)!)
                            let javaScriptString : NSString = (String(format: "%@('{\"status\":400,\"msg\":\"分享失败\",data:\"\"}')", self.callBack) as NSString)
                            self.webView.evaluateJavaScript(javaScriptString as String, completionHandler: nil)
                        }
                        else{
                            let javaScriptString : NSString = (String(format: "%@('{\"status\":200,\"msg\":\"分享成功\",data:\"\"}')", self.callBack) as NSString)
                            self.webView.evaluateJavaScript(javaScriptString as String, completionHandler: nil)
                            
                        }
                        
                    })
                }
                else{
                    OriginalDownLoad().downloadWithUrlString(context:self.view,urlString: shareImg, completionHandle: { (data) -> Void in
                        
                        if let mdata = data
                        {
                            let avatarImage = UIImage(data: mdata as Data)
                            if isShareToQQ{
                                shareDict = [LDSDKShareContentTitleKey : self.shareTitle,   LDSDKShareContentDescriptionKey : self.shareDesc,LDSDKShareContentImageKey : avatarImage!]
                            }
                            else{
                                shareDict = [LDSDKShareContentTitleKey : self.shareTitle,   LDSDKShareContentDescriptionKey : self.shareDesc,LDSDKShareContentWapUrlKey : "http://www.xinyidiandian.com",LDSDKShareContentImageUrlKey : self.shareImg]
                            }
                            qqShare.share(withContent: shareDict as! [AnyHashable: Any], shareModule:isShareToQQ == true ? 1 : 2, onComplete: {(success,error) in
                                if error != nil{
                                    Utils.showError(context: self.view, errorStr: (error?.localizedDescription)!)
                                    let javaScriptString : NSString = (String(format: "%@('{\"status\":400,\"msg\":\"分享失败\",data:\"\"}')", self.callBack) as NSString)
                                    self.webView.evaluateJavaScript(javaScriptString as String, completionHandler: nil)
                                }
                                else{
                                    let javaScriptString : NSString = (String(format: "%@('{\"status\":200,\"msg\":\"分享成功\",data:\"\"}')", self.callBack) as NSString)
                                    self.webView.evaluateJavaScript(javaScriptString as String, completionHandler: nil)
                                    
                                }
                                
                            })
                        }})
                }
                
                
            }
        }
        else if shareType == Constant.ShareContentType.textType.rawValue{
            shareDict = [LDSDKShareContentTextKey : self.shareTitle]
            qqShare.share(withContent: shareDict as! [AnyHashable: Any], shareModule:isShareToQQ == true ? 1 : 2, onComplete: ({(success,error) in
                if error != nil{
                    Utils.showError(context: self.view, errorStr: (error?.localizedDescription)!)
                    let javaScriptString : NSString = (String(format: "%@('{\"status\":400,\"msg\":\"分享失败\",data:\"\"}')", self.callBack) as NSString)
                    self.webView.evaluateJavaScript(javaScriptString as String, completionHandler: nil)
                }
                else{
                    let javaScriptString : NSString = (String(format: "%@('{\"status\":200,\"msg\":\"分享成功\",data:\"\"}')", self.callBack) as NSString)
                    self.webView.evaluateJavaScript(javaScriptString as String, completionHandler: nil)
                    
                }
                
            }))
        }
        
        
        
    }
    
    func tipAndLogin(_ tip:String){
        if tip != ""{
            Utils.showError(context: self.view, errorStr: tip)
        }
        let time: TimeInterval = 1.0
        let delay = DispatchTime.now() + Double(Int64(time * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delay) {
            if !self.isStopInLogin{
                self.Inlogin()
            }
        }
    }
    
    func Inlogin(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.loginType = Constant.InLoginType.webViewType.rawValue
        
        self.loginDialog.controller = self
        self.loginDialog.show()
    }
    
    func openContact()
    {
        self.navigationBarAppearance(true)
        if #available(iOS 9.0, *) {
            let newPicker = CNContactPickerViewController()
            newPicker.delegate = self
            self.present(newPicker, animated: true, completion: {
                
            })
        } else {
            let picker = ABPeoplePickerNavigationController()
            picker.displayedProperties = [3]
            picker.peoplePickerDelegate = self
            
            if ((UIDevice.current.systemVersion as NSString).floatValue >= 8.0)
            {
                picker.predicateForSelectionOfPerson = NSPredicate(value:false)
            }
            self.present(picker, animated: true) { () -> Void in
                
            }
        }

    }
    
    @available(iOS 9.0, *)
    @objc(contactPicker:didSelectContactProperty:)
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contactProperty: CNContactProperty) {
        let firstName = contactProperty.contact.givenName
        let name = contactProperty.contact.familyName
        var phoneNumber = ""
        if contactProperty.value != nil {
            let tempPhoneNumber = contactProperty.value as! CNPhoneNumber
            phoneNumber = tempPhoneNumber.stringValue
        }
        
        var javaScriptString : NSString = ""
        if phoneNumber != "" && Utils.detectionTelphoneSimple(phoneNumber).characters.count == 11{
            javaScriptString = (String(format: "%@('{\"status\":200,\"msg\":\"选择成功\",\"data\":{\"name\":\"%@\",\"mobile\":\"%@\"}}')", self.callBack,(name + firstName),Utils.detectionTelphoneSimple(phoneNumber)) as NSString)
        }
        else{
            javaScriptString = (String(format: "%@('{\"status\":400,\"msg\":\"选择失败\",\"data\":\"\")", self.callBack) as NSString)
        }
        self.webView.evaluateJavaScript(javaScriptString as String, completionHandler: { (object, error) -> Void in
            if error == nil && object != nil{
            }
        })
        
    }
    
    @available(iOS 9.0, *)
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        //去除地址选择界面
        picker.dismiss(animated: true, completion: { () -> Void in
            
        })
    }
    
    func peoplePickerNavigationController(_ peoplePicker: ABPeoplePickerNavigationController,
                                          didSelectPerson person: ABRecord) {
        
        /* Get all the phone numbers this user has */
        let unmanagedPhones = ABRecordCopyValue(person, kABPersonPhoneProperty)
        let phones: ABMultiValue =
            Unmanaged.fromOpaque(unmanagedPhones!.toOpaque()).takeUnretainedValue()
                as NSObject as ABMultiValue
        
        let countOfPhones = ABMultiValueGetCount(phones)
        
        for index in 0..<countOfPhones{
            let unmanagedPhone = ABMultiValueCopyValueAtIndex(phones, index)
            let _ = Unmanaged.fromOpaque(
                (unmanagedPhone?.toOpaque())!).takeUnretainedValue() as NSObject as! String
        }
    }
    
    func peoplePickerNavigationController(_ peoplePicker: ABPeoplePickerNavigationController,
                                          didSelectPerson person: ABRecord, property: ABPropertyID,
                                                          identifier: ABMultiValueIdentifier) {
        
        let multiValue: ABMultiValue = ABRecordCopyValue(person, property).takeRetainedValue()
        var index = ABMultiValueGetIndexForIdentifier(multiValue, identifier)
        if index == -1{
            index = 0 as CFIndex
        }
        let phone = ABMultiValueCopyValueAtIndex(multiValue, index).takeRetainedValue() as? String ?? ""
        let first = ABRecordCopyValue(person, kABPersonFirstNameProperty)?.takeRetainedValue() as? String ?? ""
        let last  = ABRecordCopyValue(person, kABPersonLastNameProperty)?.takeRetainedValue() as? String ?? ""
        var javaScriptString : NSString = ""
        if phone != "" && Utils.detectionTelphoneSimple(phone).characters.count == 11{
            javaScriptString = (String(format: "%@('{\"status\":200,\"msg\":\"选择成功\",\"data\":{\"name\":\"%@\",\"mobile\":\"%@\"}}')", self.callBack,last + first,Utils.detectionTelphoneSimple(phone)) as NSString)
        }
        else{
            javaScriptString = (String(format: "%@('{\"status\":400,\"msg\":\"选择失败\",\"data\":\"\")", self.callBack) as NSString)
        }
        self.webView.evaluateJavaScript(javaScriptString as String, completionHandler: { (object, error) -> Void in
            if error == nil && object != nil{
            }
        })
        
        
        // self.reminderViewModel.name <- last + first
        
        }
    
    //取消按钮点击
    func peoplePickerNavigationControllerDidCancel(_ peoplePicker: ABPeoplePickerNavigationController) {
        peoplePicker.dismiss(animated: true, completion: { () -> Void in
            let javaScriptString : NSString = (String(format: "%@('{\"status\":400,\"msg\":\"选择失败\",\"data\":\"\"}')", self.callBack) as NSString)
            self.webView.evaluateJavaScript(javaScriptString as String, completionHandler: nil)
            
        })
    }
    
    func peoplePickerNavigationController(_ peoplePicker: ABPeoplePickerNavigationController,
                                          shouldContinueAfterSelectingPerson person: ABRecord) -> Bool {
        return false
    }
    
    func peoplePickerNavigationController(_ peoplePicker: ABPeoplePickerNavigationController,
                                          shouldContinueAfterSelectingPerson person: ABRecord, property: ABPropertyID,
                                                                             identifier: ABMultiValueIdentifier) -> Bool {
        return false
    }
    
    func showToast(_ toast: String){
        print(self.callBack)
        let javaScriptString = (String(format: "%@('{\"status\":200,\"msg\":\"提示成功\",\"data\":\"\"}')", self.callBack))
        self.webView.evaluateJavaScript(javaScriptString as String, completionHandler: { (object, error) -> Void in
            if error == nil && object != nil{
            }
        })
        Utils.showError(context: self.view, errorStr: toast)
    }
    
    
    class TitleLabel: UIView {
        var text = ""{didSet{
            titlelab.text = text}}
        
        let titlelab = CustomLabel(font: 17, textColorHex: "#12171F")
        
        init() {
            super.init(frame: CGRect(x: 0,y: 0,width: Constant.ScreenSize.SCREEN_WIDTH * 0.3,height: 44))
            
            self.setupUI()
        }
        
        override func layoutSubviews() {
            layoutIfNeeded()
        }
        
        override func layoutIfNeeded() {
            titlelab.frame = self.bounds
        }
        
        func setupUI()
        {
            // CGRectMake(10, 0, self.iwidth, self.iheight)
            titlelab.frame = self.bounds
            self.addSubview(titlelab)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        
        
    }
    
    // MARK: - PCDropdownMenuViewDataSource
    
    func numberOfRowInDropdownMenuView(_ view: PCDropdownMenuView) -> Int {
        return items.count
    }
    
    func dropdownMenuView(_ view: PCDropdownMenuView, itemForRowAtIndexPath indexPath: IndexPath) -> PCDropdownMenuItem {
        return items[(indexPath as NSIndexPath).row]
    }
    
    func dropdownMenuViewArrowImageOffset(_ view: PCDropdownMenuView) -> UIOffset {
        return UIOffset(horizontal: menuWidth - 25.0, vertical: 0.0)
    }
    
    func dropdownMenuViewContentFrame(_ view: PCDropdownMenuView) -> CGRect {
        let isPortrait = UIInterfaceOrientationIsPortrait(UIApplication.shared.statusBarOrientation)
        let x: CGFloat = Constant.ScreenSize.SCREEN_WIDTH - menuWidth - 5.0
        let y: CGFloat = isPortrait ? 64.0 : (self.navigationController?.navigationBar.frame.size.height ?? 44.0)
        return CGRect(x: x, y: y, width: menuWidth, height: 0.0)
    }
    
    func dropdownMenuViewArrowImage(_ view: PCDropdownMenuView) -> UIImage? {
        return nil
    }
    
    // MARK: - PCDropdownMenuViewDelegate
    
    func dropdownMenuViewDidSelectedItem(_ view: PCDropdownMenuView, inIndex index: Int) {
        view.hiddenWithAnimate(true)
        
        //let item = items[index]
        if index == 0{
            //跳入编辑 带id
            let params = [
                "id": self.RemindDetailId
            ]
            let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.user_relation_one_v2, parameters: params as [String : AnyObject]?, isNeedUserTokrn: true,isShowErrorStatuMsg: true, isNeedHud: true, hudTitle: "", successHandler: {data, status, msg in
                if status != 200 {
                    return
                }
                let dataDic = data["data"] as! [String: AnyObject]
                let vm = AddGiftReminderViewModel()
                vm.id = self.RemindDetailId
                vm.isForUpdate = true
                if let relation = dataDic["relation"] as? [String: AnyObject] {
                    vm.avatar = relation["Avatar"] as! String
                    vm.situation <- (relation["Scene"] as! NSInteger)
                    vm.relation <- (relation["Relation"] as! NSInteger)
                    vm.name <- (relation["Name"] as! String)
                    vm.gender <- (relation["Gender"] as! NSInteger)
                    vm.mobile <- (relation["Mobile"] as! String)
                    vm.dateType = Int32(relation["Date_type"] as! NSInteger)
                    let dateTime = relation["Date_time"] as! String
                    vm.date = dateTime + " 00:01:00"
                    let components = dateTime.components(separatedBy: "-")
                    if components.count == 3 {
                        if components[0] == "1600" {
                            vm.displayDate <- (components[1] + "-" + components[2])
                        } else {
                            vm.displayDate <- dateTime
                        }
                    }
                }
                if let periods = dataDic["remind_period_list"] as? [[String: AnyObject]] {
                    var notifications = [NSInteger]()
                    for period in periods {
                        notifications.append(period["Period"] as! NSInteger)
                    }
                    vm.notifications = notifications
                }
                NotificationCenter.default.addObserver(
                    self,
                    selector: #selector(WebViewController.isAddBirthWiki(_:)),
                    name: NSNotification.Name(rawValue: "isNeedAddBirthWiki"),
                    object: nil)
                let vc = AddGiftReminderViewController(withReminderViewModel: vm)
                self.navigationController?.pushViewController(vc, animated: true)
            })
        }
        else if index == 1{
            let sheet: LCActionSheet = LCActionSheet(title: "是否确定要删除这条送礼提醒?", buttonTitles: ["确定"], redButtonIndex: -1, delegate: self)
            
            sheet.show()
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
}


