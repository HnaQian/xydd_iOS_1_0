//
//  BaseTableCellViewModel.swift
//  Limi
//
//  Created by 倪晅 on 16/1/5.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class BaseTableCellViewModel: NSObject {
    
    var model: AnyObject?
    var cellClass: BaseTableCell.Type
    
    init(model: AnyObject, cellClass: BaseTableCell.Type) {
        self.model = model
        self.cellClass = cellClass
    }
    
}
