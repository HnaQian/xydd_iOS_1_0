//
//  CommonCollectionView.swift
//  Limi
//
//  Created by meimao on 15/12/25.
//  Copyright © 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//商品

import UIKit

public enum GoodsFavType: Int
{
    case fav
    case cancelFav
    case none
}

class CommonCollectionView: UIView,UICollectionViewDelegate,UICollectionViewDataSource {
    var collectionView: UICollectionView!
    
    var userVC: UIViewController?  // 控制自身交互的viewController
    var type: GoodsFavType?   // 点击 “喜欢”按钮做不同相应处理
    var stowBool: Bool = false // 是否是收藏界面  false 不是    true 是  默认false
    
    weak var delegate:CommonCollectionViewDelegate?
    weak var commonDelegate:CommonScrollViewDelegate?
    
    // 数据1
    var goodsList: [JSON]! {
        didSet
        {
            goodsArray = [GoodsModel]()
            if let tempArray = goodsList
            {
                for index in 0 ..< tempArray.count {
                    if let detailData = tempArray[index].orignData {
                        self.goodsArray.append(GoodsModel(detailData as! [String:AnyObject]))
                    }
                }
                self.collectionView.reloadData()
            }
            
        }
    }

    
    //数据model 
    fileprivate var goodsArray:[GoodsModel]!
    
    // collectionView的表头
    var headerView = UIView() {
        didSet
        {
            self.collectionView.reloadData()
        }
    }
    
    ///参数中设置初始值的都可以在使用中省略此参数，省略后参数将使用默认值，如果传值不可传nil
    ///参数中设置初始值且使用可选符号 "?"，此参数可以省略，省略后参数将使用默认值，如果传值可传nil
    ///参数中没有初始值且未加可选符号 "?"，此参数不可省略，如果传值不可传nil
    ///参数中没有初始值且加可选符号 "?"，此参数不可省略，如果传值可传nil
    ///参数列表：
    /**
    data          :非必须，可传nil   不传默认: nil      请求类型
    
    backView      :非必传，可传nil   不传默认: 固定背景图  传nil表示不需要背景， 不传表示默认背景 传view表示自定义
    
    type          :非必须，枚举    不传默认：.None
    
    userVC        :非必须，可传nil  不传默认：nil       控制CommonTableView交互的控制器，nil表示点击cell无反应
    
    stowBool      :非必须，不可传nil  不传默认：false    true表示 “我喜欢的” 数据类型，false表示普通数据类型
    
    headerView    :非必须，不可传nil  不传默认：UIView()
    */
    
    class func newCommonCollectionView(_ data: [JSON]? = nil, backView: UIView? = NullDataTipeView.newNullDataTipeView(), type: GoodsFavType = .none, userVC: UIViewController? = nil, stowBool: Bool = false, headerView: UIView = UIView()) -> CommonCollectionView
    {
        let collView = CommonCollectionView()
        
        collView.goodsList = data
        
        collView.type = type
        
        collView.userVC = userVC
        
        collView.stowBool = stowBool
        
        collView.headerView = headerView
        
        return collView
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        

        setUI()
        
        setAttar()
        
    }

    required init?(coder aDecoder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
        
    }
    
    func setUI()
    {
        // 初始化 collectionview
        let flowLayoutWidth = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * 6
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.minimumLineSpacing = flowLayoutWidth
        flowLayout.minimumInteritemSpacing = flowLayoutWidth
//        flowLayout.sectionInset = UIEdgeInsetsMake(flowLayoutWidth, flowLayoutWidth, flowLayoutWidth, flowLayoutWidth)
        flowLayout.scrollDirection = .vertical
        
        let pointY:CGFloat = 20 - 44 - 36
        
        let frame = CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: Constant.ScreenSize.SCREEN_HEIGHT - pointY)
        
        collectionView = UICollectionView(frame: frame, collectionViewLayout: flowLayout)
        
        collectionView.contentSize = CGSize(width: Constant.ScreenSize.SCREEN_WIDTH, height: Constant.ScreenSize.SCREEN_HEIGHT - pointY)
        // 初始化表头size, 高度为0
        flowLayout.headerReferenceSize = CGSize(width: Constant.ScreenSize.SCREEN_WIDTH,height: 0)
        headerView.frame.size = CGSize(width: Constant.ScreenSize.SCREEN_WIDTH,height: 0)
        
        
        collectionView.backgroundColor = UIColor.clear
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.showsHorizontalScrollIndicator = false
        self.collectionView.showsVerticalScrollIndicator = false
        
        self.collectionView.register(GoodsListCustomCell.self, forCellWithReuseIdentifier: GoodsListCustomCell.reuseIdentifier)
        
        self.collectionView.register(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "Header")

        
        self.addSubview(collectionView)
        collectionView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self).offset(flowLayoutWidth)
            let _ = make.left.equalTo(self)
            let _ = make.right.equalTo(self)
            let _ = make.bottom.equalTo(self)
        }
    }
    
    func setAttar()
    {
        
    }
    
    func goodsDetailCancelCollectClick(_ index: Int)
    {
        if self.type == .cancelFav
        {
            goodsList.remove(at: index)
            goodsArray.remove(at: index)
            self.collectionView.reloadData()
        }
    }
    
    func goodsDetailFavBtnClick(_ index: Int,isAdd: Bool)
    {
        if self.type == .fav
        {
            let indexPath = IndexPath(row: index, section: 0)
            let data: GoodsModel = goodsArray[index]
            if isAdd{
                data.collect_num += 1
            }
            else{
                data.collect_num -= 1
            }
            goodsArray[index] = data
            self.collectionView.reloadItems(at: [indexPath])
        }
        
    }
    
    
    // UICollectionView的代理方法
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        
        if goodsArray.count > 0 && userVC != nil {
            
//            let gid = stowBool ? goodsArray[indexPath.item].data!.gid : goodsArray[indexPath.item].gid
            let gid = goodsArray[(indexPath as NSIndexPath).item].gid
            self.delegate?.commonCollectionViewDidSelected!(gid)

            if ((userVC?.isKind(of: TacticDetailViewController.self)) != nil) {
                Statistics.count(Statistics.Tractic.article_detail_recommend_click, andAttributes: ["goodsId":String(gid)])
            }
            
            if ((userVC?.isKind(of: GiftChoiceGoodsViewController.self)) != nil) {
                 Statistics.count(Statistics.GiftChoice.selectgift_giftlist_click, andAttributes: ["goodsId":String(gid)])
            }
            
            let goods = GoodsDetailViewController.newGoodsDetailWithID(gid)
//            let goods = GoodsGeneralViewController()
//            goods.index = indexPath.item
//            goods.goodsId = gid
//            goods.delegate = self
            goods.hidesBottomBarWhenPushed = true
            self.userVC!.navigationController?.pushViewController(goods, animated: true)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return goodsArray != nil ? goodsArray.count : 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GoodsListCustomCell.reuseIdentifier, for: indexPath) as! GoodsListCustomCell
        
//        let data = stowBool ? goodsArray[indexPath.item] : goodsArray[indexPath.item]
        let data = goodsArray[(indexPath as NSIndexPath).item]        
        cell.reloadData(data)
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize{
        let flowLayoutWidth = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * 6
        return CGSize(width: (Constant.ScreenSize.SCREEN_WIDTH - flowLayoutWidth)/2.0, height: (Constant.ScreenSize.SCREEN_WIDTH - flowLayoutWidth)/2.0 + 60)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        var v : UICollectionReusableView! = nil
        if kind == UICollectionElementKindSectionHeader {
            v = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier:"Header", for:indexPath)
            
            v.addSubview(self.headerView)
            
        }
        return v
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.commonDelegate?.commonScrollViewDidScroll!(scrollView)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        self.commonDelegate?.commonScrollViewDidEndDragging!(scrollView)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        IQKeyboardManager.shared().resignFirstResponder()
        self.commonDelegate?.commonScrollViewWillBeginDragging!(scrollView)
    }
}


@objc protocol CommonCollectionViewDelegate:NSObjectProtocol {
    @objc optional func commonCollectionViewDidSelected(_ data:Int)
   
}
