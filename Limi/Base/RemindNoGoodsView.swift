//

//  RemindNoGoodsView.swift

//  Limi

//

//  Created by meimao on 15/11/6.

//  Copyright © 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.

//



import UIKit



class RemindNoGoodsView: BaseView {
    
    
    
    var imageView = UIImageView(image: UIImage(named: "gift"))
    
    var label = UILabel()
    
    
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)

        label.addAttribute(font: 15, textAlignment: NSTextAlignment.center, textColor: "#999999", text: "啊噢~网络开小差啦，过会儿再试试~")
        
        self.addSubview(imageView)
        
        self.addSubview(label)
        
        
        imageView.snp_makeConstraints { (make) -> Void in
            let _ = make.centerX.equalTo(self)
            let _ = make.bottom.equalTo(self.snp_centerY).offset(-5)
        }

        
        label.snp_makeConstraints { (make) -> Void in
            let _ = make.centerX.equalTo(self)
            let _ = make.top.equalTo(self.snp_centerY).offset(5)
        }

    }
    
    
    
    required init(coder aDecoder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
        
    }
    
    
    
}
