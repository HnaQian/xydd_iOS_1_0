//
//  WebViewWidgets.swift
//  Limi
//
//  Created by Richie on 16/7/21.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import Foundation

import UIKit

/**
 LMWebViewController 所用到的一些空间

 */
class BaseWebViewWidgetView:UIView{
    weak var delegate:LMWebViewWidgetsDelegate?
}



class NavigationItemMenu:BaseWebViewWidgetView{
    
    fileprivate var icons:[UIView] = []
    
    fileprivate let btn_edg:CGFloat = 36
    
    class func newNavigationMenu(_ icons:[UIView]) ->NavigationItemMenu{
        let navigation = NavigationItemMenu()
        navigation.icons = icons
        navigation.setupUI(icons)
        return navigation
    }
    
    
     func setupUI(_ icons:[UIView]){
        
        var tempBtn:UIButton!
        
        var index =  0
        
        for icon in icons{
            icon.isUserInteractionEnabled = false
            let btn = UIButton(frame:CGRect(x: 0,y: 0,width: btn_edg,height: btn_edg))
            btn.addSubview(icon)
            
            btn.tag = 100 + index
            btn.addTarget(self, action: #selector(NavigationItemMenu.btnAction(_:)), for: UIControlEvents.touchUpInside)
            
            if tempBtn != nil{
                btn.ix = tempBtn.iright + Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * 48
            }
            
            self.addSubview(btn)
            tempBtn = btn
            index += 1
        }
        
        if let btn  = tempBtn{
            self.iwidth = btn.iright
            self.iheight = btn.iheight
        }else{
            self.iwidth = btn_edg
            self.iheight = btn_edg
        }
    }
    
    
    @objc fileprivate func btnAction(_ btn:UIButton){
        let index = btn.tag - 100
        
        let _ = Delegate_Selector(delegate, #selector(LMWebViewWidgetsDelegate.lmWebViewNavigationItemMenuDidselected(_:selectedIndex:))) {
            self.delegate!.lmWebViewNavigationItemMenuDidselected!(self, selectedIndex: index)
        }
    }
}




/**自定义导航栏*/
class LMCustomNavigationBar:UIView{

    fileprivate let leftItem:UIView = {
        let view = UIView()
        return view
    }()
    
    fileprivate let rightItem:UIView = {
        let view = UIView()
        return view
    }()
    
    fileprivate let title:UILabel = {
        let lbl = UILabel()
        
        lbl.textAlignment = NSTextAlignment.center
        lbl.font = Constant.Theme.Font_21
        
        return lbl
    }()
    
    
    init(frame: CGRect, scrollView:UIScrollView) {
        super.init(frame: frame)
        self.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: Constant.ScreenSizeV2.MARGIN_64)
        self.addSubview(leftItem)
        self.addSubview(title)
        self.addSubview(rightItem)
        let color : UIColor = UIColor(colorLiteralRed: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1)

        self.backgroundColor = color
        
        //布局
        leftItem.snp_makeConstraints { (make) in
            let _ = make.left.equalTo(Constant.ScreenSizeV2.MARGIN_30)
            let _ = make.centerY.equalTo(42)
        }
        title.snp_makeConstraints { (make) in
            let _ = make.centerY.equalTo(42)
            let _ = make.left.equalTo(leftItem.snp_right).offset(Constant.ScreenSizeV2.MARGIN_10)
            let _ = make.right.equalTo(rightItem.snp_left).offset(Constant.ScreenSizeV2.MARGIN_10)
        }
        rightItem.snp_makeConstraints { (make) in
            let _ = make.centerY.equalTo(42)
            let _ = make.right.equalTo(Constant.ScreenSizeV2.MARGIN_30)
        }
    }

    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}






@objc protocol LMWebViewWidgetsDelegate:NSObjectProtocol{
    @objc optional func lmWebViewNavigationItemMenuDidselected(_ navigationView:NavigationItemMenu,selectedIndex:Int)
}
