//
//  AskFriendBirthdayViewController.swift
//  Limi
//
//  Created by 千云锋 on 16/8/15.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//询问好友生日

import UIKit
import ContactsUI

private let separatLineColor = UIColor(rgba: Constant.common_separatLine_color)

///发送类型 模块内全局变量
private var limitSendMsgType:SendMsgType!

///当前类对象 模块内全局变量
private var limitHeartWishVC:AskFriendBirthdayViewController!

class AskFriendBirthdayViewController: BaseViewController {

    fileprivate var changeSendModeView:ChangeSendModeView!//切换发送渠道视图
    
    fileprivate var sendMessageContentView:SendMessageContentView!//发送的信息内容
    
    fileprivate let lastBtn = CustomButton(type: 1)//上一个
    fileprivate let nextbtn = CustomButton(type: 2)//下一个
    
    fileprivate let contentView = UIView()
    
    fileprivate let sendMessageButton = UIButton()//发送信息按钮
    
    fileprivate let sendManager = AskFriendBirthdaySendMsgManager()
    
    fileprivate let changeWishWordAlertMsg = "切换到别的祝福语模板，会丢失之前编辑的内容，是否确定要切换？"
    
    fileprivate let existAlertMsg = "退出当前界面，会丢失之前编辑的内容，是否确定要切换"
    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
////        sendMessageContentView = SendMessageContentView()
//        
////        changeSendModeView = ChangeSendModeView(sendMessageContentView: sendMessageContentView)
//        self.view.addSubview(changeSendModeView)
//        
//        changeSendModeView.snp_makeConstraints { (make) in
//            make.top.equalTo(self.view)
//            make.left.equalTo(self.view)
//            make.right.equalTo(self.view)
//            make.height.equalTo(Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 100)
//        }
//        
//        // Do any additional setup after loading the view.
//    }
    
    
    override func viewDidLoad()
    {

        super.viewDidLoad()
        //关闭手势返回
        self.title = "询问好友生日"
        self.gesturBackEnable = false
        self.view.backgroundColor = UIColor.white
        self.view.addSubview(contentView)
        contentView.snp_makeConstraints { (make) -> Void in
            let _ = make.left.equalTo(self.view)
            let _ = make.width.equalTo(Constant.ScreenSizeV2.SCREEN_WIDTH)
            let _ = make.top.equalTo(self.view)
            let _ = make.height.equalTo(Constant.ScreenSizeV2.SCREEN_HEIGHT - Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 100 - 64)
        }
        self.setupUI()
//        self.loadWishList()
    }
    
    /*
    //MARK:加载祝福语
    func loadWishList()
    {
        var param = [String:AnyObject]()
        
        param["type"] = limitPresentType == .Personal ? 1 : 2
        
        param["id"] = limitPresentInfo["Id"].intValue
        
        LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.bless_list, parameters:param, isNeedUserTokrn: true, isShowErrorStatuMsg: true,successHandler: {[weak self](data,status,msg) in
            
            if status != 200
            {
                return
            }
            
            let subData = JSON(data["data"]!)
            
            limitData = subData
            
            if limitPresentType == .Personal
            {
                limitPresentInfo = subData["user_relation"]
            }else
            {
                limitPresentInfo = subData["holiday"]
            }
            
            self!.setupUI()
            
            if let list = subData["list"].array
            {
                if list.count > 0
                {
                    self!.sendMessageContentView.wishList = list
                    
                    self!.sendMessageContentView.wishContent = list[0]["Content"].string!
                }
            }
            
            if limitPresentType == .Personal
            {
                if let senceName = subData["name"].string
                {
                    self!.headView.personalView.senceNameLabel.text = senceName
                }
            }
            
            self!.headView.headBgImageName = subData["background_img"].string
            })
    }
    
    */
    //MARK:切换祝福语
    func changWishWord(_ btn:UIButton)
    {
        sendMessageContentView.changeDirection = btn == lastBtn ? 1 : 2
        
        if sendMessageContentView.wishContentDidChange
        {
            LCActionSheet(title: changeWishWordAlertMsg, buttonTitles: ["放弃编辑"], redButtonIndex: -1, clicked: { (buttonIndex) -> Void in
                
                if buttonIndex == 1
                {
                    self.sendMessageContentView.changeWishWord()
                }
                
            }).show()
        }else
        {
            sendMessageContentView.changeWishWord()
        }
    }
    
    
    
    //MARK:发送信息
    func sendMessageBtnAction()
    {
        //if UserInfoManager.didLogin == false{return}
        
        let tempContent = self.sendMessageContentView.wishContent
        
        if let content = tempContent
        {
            if content.characters.count < 1
            {
                Utils.showError(context: self.view, errorStr: "你还没有填写祝福语哦")
                return
            }
        }else
        {
            Utils.showError(context: self.view, errorStr: "你还没有填写祝福语哦")
            return
        }
        
        
        self.sendManager.heartWishVC = self
        
//        LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.bless_send, isShowErrorStatuMsg:true,parameters: ["id":limitMessageId], isNeedUserTokrn: true)
//        
//        if limitSendMsgType == SendMsgType.WeChat
//        {
//            self.sendManager.sendMessage(.WeChat, message: tempContent!)
//        }else
//        {
//            self.sendManager.sendMessage(.Message, message: tempContent!, mobile: sendMessageContentView.wishPeopleMobile)
//        }
    }
    
    
    //MARK:返回操作
    override func backItemAction()
    {
        
        if sendMessageContentView != nil
        {
//            if self.sendMessageContentView.wechatView.wechatContentField.isFirstResponder(){
//                sendMessageContentView.wechatView.wechatContentField.resignFirstResponder()
//            }
//            
//            if sendMessageContentView.messageView.messageContentField.isFirstResponder(){
//                sendMessageContentView.messageView.messageContentField.resignFirstResponder()
//            }
//            
            if sendMessageContentView.wishContentDidChange
            {
                LCActionSheet(title: existAlertMsg, buttonTitles: ["放弃编辑"], redButtonIndex: -1, clicked: { (buttonIndex) -> Void in
                    
                    if buttonIndex == 1
                    {
                       let _ = self.navigationController?.popViewController(animated: true)
                        
                        self.cusDinit()
                    }
                    
                }).show()
                
            }else
            {
                let _ = self.navigationController?.popViewController(animated: true)
                
                self.cusDinit()
            }
            
        }else
        {
            let _ = self.navigationController?.popViewController(animated: true)
            
            self.cusDinit()
        }
    }

    //MARK:界面布局
    func setupUI()
    {
        
        sendMessageContentView = SendMessageContentView()
        
        changeSendModeView = ChangeSendModeView(sendMessageContentView: sendMessageContentView)
        
        lastBtn.addTarget(self, action: #selector(AskFriendBirthdayViewController.changWishWord(_:)), for: UIControlEvents.touchUpInside)
        
        nextbtn.addTarget(self, action: #selector(AskFriendBirthdayViewController.changWishWord(_:)), for: UIControlEvents.touchUpInside)
        
        sendMessageButton.addTarget(self, action: #selector(AskFriendBirthdayViewController.sendMessageBtnAction), for: UIControlEvents.touchUpInside)
        
        sendMessageButton.setTitle("发送给好友", for: UIControlState())
        
        sendMessageButton.setTitleColor(UIColor(rgba: Constant.common_C12_color), for: UIControlState())
        
        sendMessageButton.titleLabel?.font = UIFont.systemFont(ofSize: Constant.common_F2_font)
        
        sendMessageButton.backgroundColor = UIColor(rgba: Constant.common_C1_color)
        
        
        for view in[sendMessageContentView,changeSendModeView,lastBtn,nextbtn] as [Any]
        {
            contentView.addSubview(view as! UIView)
        }
        
        self.view.addSubview(sendMessageButton)
        
        changeSendModeView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.view)
            let _ = make.left.equalTo(self.view)
            let _ = make.height.equalTo(Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 100)
            let _ = make.width.equalTo(self.view)
        }
        
        self.sendMessageContentView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(changeSendModeView.snp_bottom)
            let _ = make.height.equalTo(self.view.snp_width).multipliedBy(0.8)
            let _ = make.left.equalTo(self.view)
            let _ = make.width.equalTo(self.view).multipliedBy(2)
        }
        
        lastBtn.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.sendMessageContentView.snp_bottom).offset(8)
            let _ = make.left.equalTo(self.sendMessageContentView)
            let _ = make.width.equalTo(80)
            let _ = make.height.equalTo(30)
        }
        
        nextbtn.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(lastBtn)
            let _ = make.right.equalTo(sendMessageContentView.snp_centerX)
            let _ = make.height.equalTo(lastBtn)
            let _ = make.width.equalTo(lastBtn)
        }
        
        sendMessageButton.snp_makeConstraints { (make) -> Void in
            let _ = make.bottom.equalTo(self.view)
            let _ = make.left.equalTo(self.view)
            let _ = make.right.equalTo(self.view)
            let _ = make.height.equalTo(Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 100)
        }
        
    }
    
    
    
    ///销毁全局变量
    func cusDinit()
    {

        limitSendMsgType = SendMsgType.message
        
        limitHeartWishVC = nil
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}

extension AskFriendBirthdayViewController{
    //MARK:改变发送渠道视图
    class ChangeSendModeView:UIView{
        fileprivate var sendMessageContentView:SendMessageContentView!
        
        fileprivate let wechatButton = UIButton()
        
        fileprivate let redLineView = UIView()
        
        fileprivate let bottomLineView = UIView()
        
        fileprivate let messageButton = UIButton()
        
        fileprivate let sendStyleView = UIView()
        
        init(sendMessageContentView:SendMessageContentView)
        {
            super.init(frame: CGRect.zero)
            self.sendMessageContentView = sendMessageContentView
            self.changeSendModeViewSetupUI()
        }
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            changeSendModeViewSetupUI()
        }
        
        
        fileprivate let blackColor = Constant.Theme.Color2
        
        func changeSendModeViewSetupUI()
        {
            self.backgroundColor = UIColor.white
            
            self.isUserInteractionEnabled = true
            
            for view in [bottomLineView, wechatButton, redLineView, messageButton]
            {
                sendStyleView.addSubview(view)
            }
            sendStyleView.backgroundColor = Constant.Theme.Color12
            self.addSubview(sendStyleView)
            
            wechatButton.setTitle("微信询问", for: UIControlState())
            wechatButton.setTitleColor(Constant.Theme.Color2, for: UIControlState())
            
            messageButton.setTitle("短信询问", for: UIControlState())
            messageButton.setTitleColor(Constant.Theme.Color1, for: UIControlState())
            
            redLineView.backgroundColor = Constant.Theme.Color1
            bottomLineView.backgroundColor = Constant.Theme.Color10
            
            wechatButton.addTarget(self, action: #selector(ChangeSendModeView.switchSendType(_:)), for: UIControlEvents.touchUpInside)
            messageButton.addTarget(self, action: #selector(ChangeSendModeView.switchSendType(_:)), for: UIControlEvents.touchUpInside)
            
            messageButton.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(sendStyleView)
                let _ = make.height.equalTo(35)
                let _ = make.left.equalTo(sendStyleView)
                let _ = make.width.equalTo(sendStyleView).multipliedBy(0.5)
            }
            
            wechatButton.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(messageButton)
                let _ = make.height.equalTo(messageButton)
                let _ = make.left.equalTo(messageButton.snp_right)
                let _ = make.width.equalTo(sendStyleView).multipliedBy(0.5)
            }
            
            redLineView.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(wechatButton.snp_bottom)
                let _ = make.centerX.equalTo(messageButton)
                let _ = make.height.equalTo(2)
                let _ = make.width.equalTo(90)
                let _ = make.bottom.equalTo(sendStyleView)
            }
            
            bottomLineView.snp_makeConstraints { (make) in
                let _ = make.left.equalTo(sendStyleView)
                let _ = make.right.equalTo(sendStyleView)
                let _ = make.height.equalTo(0.5)
                let _ = make.bottom.equalTo(sendStyleView)

            }
            
            sendStyleView.snp_makeConstraints { (make) in
                let _ = make.top.equalTo(self)
                let _ = make.right.equalTo(self)
                let _ = make.left.equalTo(self)
                let _ = make.height.equalTo(Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 200)
                let _ = make.bottom.equalTo(self)
            }
            
        }
        
        //MARK:改变发送信息渠道
        func switchSendType(_ btn:UIButton)
        {
            
            if btn == self.wechatButton
            {
                if limitSendMsgType != SendMsgType.weChat
                {
                    self.sendMessageContentView.changeDisplayMode(SendMsgType.weChat)
                    
                    limitSendMsgType = SendMsgType.weChat
                    
                    UIView.animate(withDuration: 0.25, animations: { () -> Void in
                        self.redLineView.transform = CGAffineTransform(translationX: self.wechatButton.ix, y: 0)
                    }) 
                    self.messageButton.setTitleColor(blackColor, for: UIControlState())
                    
                    self.wechatButton.setTitleColor(Constant.Theme.Color1, for: UIControlState())
                }
            }else
            {
                if limitSendMsgType != SendMsgType.message
                {
                    self.sendMessageContentView.changeDisplayMode(SendMsgType.message)
                    
                    UIView.animate(withDuration: 0.25, animations: { () -> Void in
                        self.redLineView.transform = CGAffineTransform(translationX: self.messageButton.ix, y: 0)
                    }) 
                    self.wechatButton.setTitleColor(blackColor, for: UIControlState())
                    
                    self.messageButton.setTitleColor(Constant.Theme.Color1, for: UIControlState())
                    
                    limitSendMsgType = SendMsgType.message
                }
            }
        }
        
        
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    
    //MARK:发送祝福视图
    class  SendMessageContentView :UIView
    {
        var wishList:[JSON]!
        
        var wishIndex = 0
        
        var viewMode:SendMsgType = .message
        
        var changeDirection = 1//祝福语切换方向：1：前一条 2：换一条
        
        var wishContentDidChange:Bool{
            
            get{
                if let list = wishList {
                    
                    if list.count > 0{
                        
                        if wishIndex < list.count{
                            
                            return wishContent != list[wishIndex]["Content"].string
                        }
                    }
                }
                return false
            }
        }
        
        //祝福内容
        var wishContent:String?{
            get
            {return (viewMode == .weChat ? wechatView.weChatContent : messageView.messageContent)!}
            
            set(newValue)
            {if viewMode == .weChat {wechatView.weChatContent = newValue }else{ messageView.messageContent = newValue}}
        }
        
        
        //接收方姓名
        var wishPeopleMobile:String?{
            get
            {return messageView.messageMobileField.text}
            
            set(newValue)
            {messageView.messageMobileField.text = newValue}
        }
        
        
        //接收方手机号码
        var wishPeopleName:String?{
            get
            {return messageView.messageNameLabel.text}
            
            set(newValue)
            {messageView.messageNameLabel.text = newValue}
        }
        
        
        init()
        {
            super.init(frame: CGRect.zero)
            heartWishSetupUI()
            
        }

        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        ///改变现实模式
        /**
         viewMode : 视图模式：.WeChat :微信微信现实模式 .Message :短信发送
         */
        ///
        func changeDisplayMode(_ viewMode:SendMsgType)
        {
            self.viewMode = viewMode
            
            if viewMode == .weChat
            {
                wechatView.weChatContent = messageView.messageContent
            }else
            {
                messageView.messageContent = wechatView.weChatContent
            }
            
            let index:CGFloat = viewMode == .message ? -0.5 : 0
            
            UIView.animate(withDuration: 0.25, animations: { () -> Void in
                self.transform = CGAffineTransform(translationX: self.iwidth * index, y: 0)
            }) 
        }
        
        
        
        ///切换wish内容
        /**
         changeType : 1：前一条  2：后一条
         */
        func changeWishWord()
        {
            if let list = wishList
            {
                if list.count != 0
                {
                    if changeDirection == 1
                    {
                        wishIndex -= 1
                        
                        wishIndex = wishIndex < 0 ? wishList.count - 1 : wishIndex
                    }
                    else
                    {
                        wishIndex += 1
                        
                        wishIndex = wishIndex > wishList.count - 1 ? 0 : wishIndex
                    }
                    
                }else{return}
                
            }else{return}
            
            UIView.animate(withDuration: 0.0, delay: 0, options: UIViewAnimationOptions(), animations: {Void in
                
                if self.viewMode == .weChat
                {
                    self.wechatView.wechatContentField.alpha = 0
                }else
                {
                    self.messageView.messageContentField.alpha = 0
                }
                
                
                }, completion: {Void in
                    
//                    self.wishContent = self.wishList[self.wishIndex]["Content"].string!
//                    
//                    limitMessageId = self.wishList[self.wishIndex]["Id"].intValue
                    
                    UIView.animate(withDuration: 0.45, delay: 0, options: UIViewAnimationOptions.curveEaseIn, animations: {Void in
                        
                        if self.viewMode == .weChat
                        {
                            self.wechatView.wechatContentField.alpha = 1
                        }else
                        {
                            self.messageView.messageContentField.alpha = 1
                        }
                        }, completion:nil)
            })
        }
        
        
        
        
        fileprivate let wechatView = WechatView()
        
        fileprivate let messageView = MessageView()
        
        
        func heartWishSetupUI()
        {
            self.backgroundColor = UIColor.white
            //初始化短信发送联系人
            self.messageView.messageNameLabel.text = "风清扬"
            
            self.messageView.messageMobileField.text = "（18615473596）"
            
            
            self.addSubview(wechatView)
            self.addSubview(messageView)
            wechatView.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(self).offset(8)
                let _ = make.left.equalTo(self).offset(8)
                let _ = make.bottom.equalTo(self)
                let _ = make.width.equalTo(self).multipliedBy(0.5).offset(-16)
            }
            
            messageView.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(self)
                let _ = make.left.equalTo(self.snp_centerX).offset(8)
                let _ = make.bottom.equalTo(self)
                let _ = make.width.equalTo(self).multipliedBy(0.5).offset(-16)
            }
            
        }
        
        
        
        //MARK:微信消息视图
        class WechatView:UIView,UITextViewDelegate
        {
            ///微信消息内容
            var weChatContent:String?{
                get{
                    return wechatContentField.text
                }
                set(newValue){
                
                self.wechatContentField.text = newValue
                
                oldFieldText = newValue
                }
            }
            
            var oldFieldText:String?
            
            fileprivate let wechatContentField = UITextView()
 
            init()
            {
                super.init(frame: CGRect.zero)
                
                self.wechatSetupUI()
            }
            
            fileprivate func wechatSetupUI()
            {

                
                wechatContentField.delegate = self
                
                wechatContentField.textColor = UIColor(rgba: Constant.common_C7_color)
                
                wechatContentField.font = UIFont.systemFont(ofSize: Constant.common_F2_font)
                
                self.clipsToBounds = true
                self.backgroundColor = UIColor.white
                self.layer.borderColor = separatLineColor.cgColor
                self.layer.cornerRadius = 3
                self.layer.borderWidth = 0.5
    
                self.addSubview(wechatContentField)
                
                wechatContentField.snp_makeConstraints { (make) -> Void in
                    let _ = make.top.equalTo(self)
                    let _ = make.bottom.equalTo(self)
                    let _ = make.left.equalTo(self).offset(3)
                    let _ = make.right.equalTo(self)
                }

                
            }
            
            
            
            func textViewDidChange(_ textView: UITextView)
            {
                if textView.text.characters.count <= 180
                {
                    oldFieldText = textView.text
                }else
                {
                    textView.text = oldFieldText
                }
            }
            
            
            required init?(coder aDecoder: NSCoder) {
                fatalError("init(coder:) has not been implemented")
            }
            
        }
        
        
        //MARK:短信消息视图
        class MessageView:UIView,UITextViewDelegate,CNContactPickerDelegate,ABPeoplePickerNavigationControllerDelegate
        {
            var oldFieldText:String?
            
            ///短信消息内容
            var messageContent:String?{get{return messageContentField.text}
                
                set(newValue)
                {
                    self.messageContentField.text = newValue
                    
                    oldFieldText = newValue
                    
                    self.caluteChartCount(newValue!.characters.count)
                }}
            
            fileprivate let bgView = UIView()//联系人背景条纹
            
            fileprivate let peopleContentView = UIView()//联系人控件组
            
            fileprivate let messageMobileField = UITextField()//联系人电话号码输入框
            
            fileprivate let messageNameLabel = CustomLabel(font: Constant.common_F4_font, textColorHex: Constant.Theme.Color14)//
            
            fileprivate let openAddressBookImageView = UIImageView(image: UIImage(named: "heartWish_contactIcon"))//打开通讯录按钮
            
            fileprivate let messageContentTipLabel = CustomLabel(font: 11, textColorHex: Constant.Theme.Color8)//还可以写xx字的提示
            
            fileprivate let messageContentField = UITextView()//短信内容
            
            fileprivate let messageBgView = UIView()//短信内容输入框和提示文字的背景
            
            
            init()
            {
                super.init(frame: CGRect.zero)
                self.messageSetupUI()
            }
            
            fileprivate func messageSetupUI()
            {
                self.isUserInteractionEnabled = true
                peopleContentView.isUserInteractionEnabled = true
                
                messageContentTipLabel.textAlignment = NSTextAlignment.right
                //            messageContentTipLabel.backgroundColor = UIColor.redColor()
                
                messageContentField.delegate = self
                peopleContentView.layer.borderColor = separatLineColor.cgColor
                peopleContentView.backgroundColor = UIColor.white
                peopleContentView.layer.cornerRadius = 3
                peopleContentView.layer.borderWidth = 0.5
                
                messageContentField.textColor = UIColor(rgba: Constant.common_C7_color)
                messageContentField.font = UIFont.systemFont(ofSize: Constant.common_F2_font)
                
                messageBgView.clipsToBounds = true
                messageBgView.layer.borderColor = separatLineColor.cgColor
                messageBgView.backgroundColor = UIColor.white
                messageBgView.layer.cornerRadius = 3
                messageBgView.layer.borderWidth = 0.5
                
                
                peopleContentView.isUserInteractionEnabled = true
                
                openAddressBookImageView.isUserInteractionEnabled = true
                
                openAddressBookImageView.contentMode = UIViewContentMode.center
                
                openAddressBookImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(MessageView.openAddressAction)))
                
                
                for view in [peopleContentView,messageBgView]
                {
                    self.addSubview(view)
                }
                
                
                for view in [messageContentField,messageContentTipLabel]
                {
                    messageBgView.addSubview(view)
                }
                messageMobileField.font = UIFont.systemFont(ofSize: Constant.common_F5_font)
                for view in [messageMobileField,messageNameLabel,openAddressBookImageView]
                {
                    peopleContentView.addSubview(view)
                }
                peopleContentView.snp_makeConstraints { (make) -> Void in
                    let _ = make.top.equalTo(self).offset(8)
                    let _ = make.left.equalTo(self)
                    let _ = make.right.equalTo(self)
                    let _ = make.height.equalTo(Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 90)
                }
                
                messageBgView.snp_makeConstraints { (make) -> Void in
                    peopleContentView.isHidden = false
                    let _ = make.top.equalTo(peopleContentView.snp_bottom).offset(8)
                    let _ = make.left.equalTo(peopleContentView)
                    let _ = make.right.equalTo(peopleContentView)
                    let _ = make.bottom.equalTo(self)
                }

                messageNameLabel.snp_makeConstraints { (make) -> Void in
                    let _ = make.left.equalTo(peopleContentView).offset(Constant.ScreenSizeV2.MARGIN_30)
                    let _ = make.centerY.equalTo(peopleContentView)
                }
                
                messageMobileField.snp_makeConstraints { (make) -> Void in
                    let _ = make.top.equalTo(messageNameLabel)
                    let _ = make.bottom.equalTo(messageNameLabel)
                    let _ = make.right.equalTo(openAddressBookImageView.snp_left).offset(-8)
                    let _ = make.left.lessThanOrEqualTo(messageNameLabel.snp_right).offset(8)
                }
                
                openAddressBookImageView.snp_makeConstraints { (make) -> Void in
                    let _ = make.top.equalTo(messageNameLabel)
                    let _ = make.bottom.equalTo(messageNameLabel)
                    let _ = make.right.equalTo(peopleContentView).offset(-8)
                    let _ = make.width.equalTo(openAddressBookImageView.snp_height)
                }
                
                messageContentField.snp_makeConstraints { (make) -> Void in
                    let _ = make.top.equalTo(messageBgView)
                    let _ = make.left.equalTo(messageBgView).offset(3)
                    let _ = make.right.equalTo(messageBgView)
                    let _ = make.bottom.equalTo(messageContentTipLabel.snp_top)
                }
                
                messageContentTipLabel.snp_makeConstraints { (make) -> Void in
                    let _ = make.height.equalTo(14)
                    let _ = make.bottom.equalTo(messageBgView).offset(-4)
                    let _ = make.left.equalTo(messageBgView).offset(8)
                    let _ = make.right.equalTo(messageBgView).offset(-10)
                }
                
            }
            
            
            
            ///打开通讯录
            func openAddressAction()
            {
                
                if #available(iOS 9.0, *) {
                    let newPicker = CNContactPickerViewController()
                    newPicker.delegate = self
                    limitHeartWishVC.present(newPicker, animated: true, completion: {
                        
                    })
                } else {
                    let picker = ABPeoplePickerNavigationController()
                    //                picker.displayedProperties = [3]
                    picker.peoplePickerDelegate = self
                    
                    //                if ((UIDevice.currentDevice().systemVersion as NSString).floatValue >= 8.0)
                    //                {
                    //                    picker.predicateForSelectionOfPerson = NSPredicate(value:false)
                    //                }
                    //
                    //                limitHeartWishVC.navigationBarAppearance(true)
                    
                    limitHeartWishVC.present(picker, animated: true) {Void in}                }
              
            }
            
            @available(iOS 9.0, *)
            @objc(contactPicker:didSelectContactProperty:)
            func contactPicker(_ picker: CNContactPickerViewController, didSelect contactProperty: CNContactProperty) {
                let firstName = contactProperty.contact.givenName
                let name = contactProperty.contact.familyName
                self.messageNameLabel.text = (name + firstName)
                
                if contactProperty.value != nil {
                    let tempPhoneNumber = contactProperty.value as! CNPhoneNumber
                    let phoneNumber = tempPhoneNumber.stringValue
                    self.messageMobileField.text = Utils.detectionTelphoneSimple(phoneNumber)
                }
                
            }
            
            @available(iOS 9.0, *)
            func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
                //去除地址选择界面
                picker.dismiss(animated: true, completion: { () -> Void in
                    
                })
            }
            
            // peoplePickerNavigationController 代理方法
            func peoplePickerNavigationController(_ peoplePicker: ABPeoplePickerNavigationController,
                                                  didSelectPerson person: ABRecord) {}
            
            func peoplePickerNavigationController(_ peoplePicker: ABPeoplePickerNavigationController,
                                                  didSelectPerson person: ABRecord, property: ABPropertyID,
                                                                  identifier: ABMultiValueIdentifier) {
                
                let multiValue: ABMultiValue = ABRecordCopyValue(person, property).takeRetainedValue()
                var index = ABMultiValueGetIndexForIdentifier(multiValue, identifier)
                if index == -1{
                    index = 0 as CFIndex
                }
                let phone = ABMultiValueCopyValueAtIndex(multiValue, index).takeRetainedValue() as? String ?? ""
                
                let first = ABRecordCopyValue(person, kABPersonFirstNameProperty)?.takeRetainedValue() as? String ?? ""
                let last  = ABRecordCopyValue(person, kABPersonLastNameProperty)?.takeRetainedValue() as? String ?? ""
                
                self.messageNameLabel.text = last + first
                
                self.messageMobileField.text = Utils.detectionTelphoneSimple(phone)
            }
            
            //取消按钮点击
            func peoplePickerNavigationControllerDidCancel(_ peoplePicker: ABPeoplePickerNavigationController) {
                
                peoplePicker.dismiss(animated: true, completion: { () -> Void in
                    
                })
            }
            
            func peoplePickerNavigationController(_ peoplePicker: ABPeoplePickerNavigationController,
                                                  shouldContinueAfterSelectingPerson person: ABRecord) -> Bool {
                return false
            }
            
            func peoplePickerNavigationController(_ peoplePicker: ABPeoplePickerNavigationController,
                                                  shouldContinueAfterSelectingPerson person: ABRecord, property: ABPropertyID,
                                                                                     identifier: ABMultiValueIdentifier) -> Bool {
                return false
            }
            
            
            
            func textViewDidChange(_ textView: UITextView)
            {
                if textView.text.characters.count <= 180
                {
                    oldFieldText = textView.text
                }else
                {
                    textView.text = oldFieldText
                }
                
                self.caluteChartCount(textView.text.characters.count)
            }
            
            
            
            //根据内容长度显示提示语
            func caluteChartCount(_ charLen:Int)
            {
                if charLen <= 70
                {
                    let str : String = "还可以写 " + String(70 - charLen) + " 字"
                    let attributeString = NSMutableAttributedString(string: str)
                    attributeString.setAttributes([NSForegroundColorAttributeName: UIColor(rgba: "#999999"),NSFontAttributeName : UIFont.systemFont(ofSize: 11.0)], range: NSMakeRange(0, 4))
                    
                    attributeString.setAttributes([NSForegroundColorAttributeName: UIColor(rgba: "#555555"),NSFontAttributeName : UIFont.systemFont(ofSize: 11.0)], range: NSMakeRange(4, str.characters.count - 5))
                    
                    attributeString.setAttributes([NSForegroundColorAttributeName: UIColor(rgba: "#999999"),NSFontAttributeName : UIFont.systemFont(ofSize: 11.0)], range: NSMakeRange(str.characters.count - 1, 1));
                    
                    messageContentTipLabel.attributedText = attributeString;
                    
                }else if charLen > 70 && charLen < 140
                {
                    messageContentTipLabel.text = "已超出70字，将拆分成2条短信发送"
                }else if charLen > 140 && charLen <= 180
                {
                    messageContentTipLabel.text = "已超出140字，将拆分成3条短信发送"
                }else
                {
                    messageContentTipLabel.text = "已超出180字，请缩减字数"
                }
            }
            
            required init?(coder aDecoder: NSCoder) {
                fatalError("init(coder:) has not been implemented")
            }
        }
    }
    
    class CustomLabel:UILabel
    {
        
        ///为UILabel添加属性
        /*
         *   font            :label的文字尺寸 可不传    默认17
         *   autoFont        :在iphone5之前的文字与iphone6之后的文字大小 的差
         *   textColorHex    :label的文字颜色 可不传    默认是黑色
         *   textAlignment   :label的内容布局 可不传    默认是居中
         */
        ///
        
        
        class func newLabel(_ font:CGFloat,autoFont:CGFloat! = 0,textColor:UIColor = UIColor.black,textAlignment: NSTextAlignment = NSTextAlignment.center) ->CustomLabel{
            
            let lbl = CustomLabel(frame:CGRect.zero)
            
            lbl.setFont(font - autoFont, laterIph6: font)
            
            lbl.textColor = textColor
            
            lbl.textAlignment = textAlignment
            
            return lbl
        }
        
        
        override init(frame: CGRect) {
            super.init(frame: frame)
        }
        
        
        init(font:CGFloat,autoFont:CGFloat! = 0,textColorHex:UIColor,textAlignment: NSTextAlignment = NSTextAlignment.center)
        {
            super.init(frame: CGRect.zero)
            
            self.setFont(font - autoFont, laterIph6: font)
            
            self.textColor = textColorHex
            
            self.textAlignment = textAlignment
        }
        
        
        
        func setFont(_ beforeIph5s:CGFloat! = 0,laterIph6:CGFloat! = 0)
        {
            if Constant.DeviceType.IS_IPHONE_4_OR_LESS == true || Constant.DeviceType.IS_IPHONE_5 == true
            {
                if beforeIph5s != 0
                {
                    self.font = UIFont.systemFont(ofSize: beforeIph5s)
                }
                
            }else
            {
                if laterIph6 != 0
                {
                    self.font = UIFont.systemFont(ofSize: laterIph6)
                }
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    class LineView:UIView
    {
        
        init()
        {
            super.init(frame: CGRect.zero)
            
            self.iheight = 0.5
            
            self.backgroundColor = UIColor(rgba: Constant.common_separatLine_color)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
 
    //MARK:改变发送渠道视图
    fileprivate class CustomButton:UIButton
    {
        var type:Int = 0//1：前一条  2：后一条
        init(type:Int)
        {
            super.init(frame: CGRect.zero)
            self.type = type
            self.customButtonSetupUI()
        }
        func customButtonSetupUI()
        {
            let icon = UIImageView()
            let text = CustomLabel(font: Constant.common_F4_font, textColorHex: Constant.Theme.Color6)
            icon.contentMode = UIViewContentMode.center
            
            self.addSubview(icon)
            self.addSubview(text)
            
            if type == 1
            {
                icon.image = UIImage(named: "heartWish_left")
                text.text = "上一条"
                icon.snp_makeConstraints(closure: { (make) -> Void in
                    let _ = make.left.equalTo(self)
                    let _ = make.top.equalTo(self)
                    let _ = make.height.equalTo(self)
                    let _ = make.width.equalTo(self.snp_height)
                })
                
                text.snp_makeConstraints(closure: { (make) -> Void in
                    let _ = make.left.equalTo(icon.snp_right)
                    let _ = make.top.equalTo(self)
                    let _ = make.height.equalTo(self)
                    let _ = make.right.equalTo(self)
                })
                
                
            }else
            {
                icon.image = UIImage(named: "heartWish_right")
                text.text = "下一条"
                text.snp_makeConstraints(closure: { (make) -> Void in
                    let _ = make.left.equalTo(self)
                    let _ = make.top.equalTo(self)
                    let _ = make.height.equalTo(self)
                    let _ = make.right.equalTo(icon.snp_left)
                })
                
                icon.snp_makeConstraints(closure: { (make) -> Void in
                    let _ = make.right.equalTo(self)
                    let _ = make.top.equalTo(self)
                    let _ = make.height.equalTo(self)
                    let _ = make.width.equalTo(self.snp_height)
                })
                
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }

}



