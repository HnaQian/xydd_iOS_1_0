//
//  SetupRemindTimeViewController.swift
//  Limi
//
//  Created by 千云锋 on 16/8/15.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//  提醒设置

import UIKit

class SetupRemindTimeViewController: BaseViewController {
    
    fileprivate let dataSource = ["当天", "提前1天", "提前三天", "提前7天", "提前15天", "提前30天", "不提醒"]
    fileprivate var cellBool = [false, false, false, false, false, false, false]

    fileprivate let tableView = UITableView()
    
    fileprivate let bottomButtom:UIButton = {
        let btn = UIButton()
        btn.frame = CGRect(x: Constant.ScreenSizeV2.SCREEN_WIDTH - Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * 230, y: Constant.ScreenSizeV2.MARGIN_10, width: Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * 200, height: Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 80)
        btn.setTitle("导入完成", for: UIControlState())
        btn.setTitleColor(UIColor.white, for: UIControlState())
        btn.backgroundColor = Constant.Theme.Color1
        btn.layer.cornerRadius = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * 10
        return btn
    }()
    
    fileprivate let linesView:UIView = {
        let line = UIView()
        line.backgroundColor = Constant.Theme.Color8
        line.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: 0.5)
        return line
    }()
    
    fileprivate let bottomView:UIView = {
        let bottom = UIView()
        bottom.backgroundColor = UIColor.white
        bottom.frame = CGRect(x: 0, y: Constant.ScreenSizeV2.SCREEN_HEIGHT - 64 - Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 100, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 100)
        return bottom
    }()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "提醒设置"
        
        bottomButtom.addTarget(self, action: #selector(SetupRemindTimeViewController.addBirthday(_:)), for: UIControlEvents.touchUpInside)
        bottomView.addSubview(bottomButtom)
        bottomView.addSubview(linesView)
        self.view.addSubview(bottomView)
        
        tableView.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: Constant.ScreenSizeV2.SCREEN_HEIGHT - 64 - Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 100)
        tableView.showsVerticalScrollIndicator = false
        tableView.delegate = self
        tableView.dataSource = self
        self.view.addSubview(tableView)
        tableView.register(SetupRemindTimeTableViewCell.self, forCellReuseIdentifier: SetupRemindTimeTableViewCell.identifier)
        
        tableView.separatorStyle = .none

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addBirthday(_ button:UIButton) {
        let vc = AddSuccessRemindViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }

}

extension SetupRemindTimeViewController: UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SetupRemindTimeTableViewCell = tableView.dequeueReusableCell(withIdentifier: SetupRemindTimeTableViewCell.identifier) as! SetupRemindTimeTableViewCell
        cell.reloadLabelData(self.dataSource[(indexPath as NSIndexPath).row], style: self.cellBool[(indexPath as NSIndexPath).row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120 * Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath as NSIndexPath).row == self.cellBool.count - 1 {
            self.cellBool[(indexPath as NSIndexPath).row] = !self.cellBool[(indexPath as NSIndexPath).row]
            for i in 0..<self.cellBool.count - 1 {
                self.cellBool[i] = false
            }
        }else{
            self.cellBool[(indexPath as NSIndexPath).row] = !self.cellBool[(indexPath as NSIndexPath).row]
            self.cellBool[self.cellBool.count - 1] = false
        }
        
        self.tableView.reloadData()
    }
}

extension SetupRemindTimeViewController {

    class SetupRemindTimeTableViewCell: UITableViewCell {
        static let identifier = "SetupRemindTimeTableViewCell"

        fileprivate let optionalButton:UIImageView = {
            let btn = UIImageView()
            btn.frame.size = CGSize(width: Constant.ScreenSizeV2.MARGIN_36, height: Constant.ScreenSizeV2.MARGIN_36)
            return btn
        }()
        
        fileprivate let nameLabel:UILabel = {
            let lbl = UILabel()
            lbl.font = UIFont.systemFont(ofSize: Constant.common_F2_font)
            lbl.textColor = Constant.Theme.Color14
            return lbl
        }()

        fileprivate let lineView:UIView = {
            let view = UIView()
            view.backgroundColor = Constant.Theme.Color10
            return view
        }()
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)
            self.selectionStyle = .none
            setupCell()
            
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func setupCell() {
            self.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 120)
            let _ = [optionalButton, nameLabel, lineView].map{self.addSubview($0)}
            
            optionalButton.snp_makeConstraints { (make) in
                let _ = make.left.equalTo(self).offset(Constant.ScreenSizeV2.MARGIN_30)
                let _ = make.centerY.equalTo(self)
                let _ = make.width.equalTo(Constant.ScreenSizeV2.MARGIN_36)
                let _ = make.height.equalTo(Constant.ScreenSizeV2.MARGIN_36)
            }
            
            nameLabel.snp_makeConstraints { (make) in
                let _ = make.left.equalTo(optionalButton.snp_right).offset(Constant.ScreenSizeV2.MARGIN_20)
                let _ = make.centerY.equalTo(self)
            }
            
            lineView.snp_makeConstraints { (make) in
                let _ = make.bottom.equalTo(self)
                let _ = make.left.equalTo(self)
                let _ = make.right.equalTo(self)
                let _ = make.height.equalTo(0.5)
            }
        }
        
        func reloadLabelData(_ data:String, style:Bool) {
            nameLabel.text = data
            optionalButton.image = UIImage(named: !style ? "add_gift_reminder_unchoose" : "choosed")
        }
    }
    
}


