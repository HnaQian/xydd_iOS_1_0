//
//  FindNowBirthdayRemindViewController.swift
//  Limi
//
//  Created by 千云锋 on 16/8/13.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//   发现好友生日

import UIKit

class FindNowBirthdayRemindViewController: BaseViewController {

    fileprivate let tableView = UITableView()
//    fileprivate let dataSource = [] //总的数据数组
//    fileprivate let dataNowFind = [] // 存放新发现的好友信息
//    fileprivate let dataAlreadyFind = [] // 已发现但是未添加的好友信息
//    fileprivate let dataNotFind = []
    
    fileprivate let bottomButtom:UIButton = {
        let btn = UIButton()
        btn.frame = CGRect(x: Constant.ScreenSizeV2.MARGIN_30, y: Constant.ScreenSizeV2.MARGIN_10, width: Constant.ScreenSizeV2.SCREEN_WIDTH - Constant.ScreenSizeV2.MARGIN_60, height: Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 80)
        btn.setTitle("全部添加", for: UIControlState())
        btn.setTitleColor(UIColor.white, for: UIControlState())
        btn.backgroundColor = Constant.Theme.Color1
        btn.layer.cornerRadius = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * 10
        return btn
    }()
    
    fileprivate let linesView:UIView = {
        let line = UIView()
        line.backgroundColor = Constant.Theme.Color8
        line.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: 0.5)
        return line
    }()
    
    fileprivate let bottomView:UIView = {
        let bottom = UIView()
        bottom.frame = CGRect(x: 0, y: Constant.ScreenSizeV2.SCREEN_HEIGHT - 64 - Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 100, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 100)
        return bottom
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "发现好友生日"
        bottomView.backgroundColor = UIColor.white
        bottomButtom.addTarget(self, action: #selector(FindNowBirthdayRemindViewController.addBirthday(_:)), for: UIControlEvents.touchUpInside)
        bottomView.addSubview(bottomButtom)
        bottomView.addSubview(linesView)
        self.view.addSubview(bottomView)
        
        tableView.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: Constant.ScreenSizeV2.SCREEN_HEIGHT - 64 - Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 100)
        tableView.showsVerticalScrollIndicator = false
        tableView.delegate = self
        tableView.dataSource = self
        self.view.addSubview(tableView)
        tableView.register(FindNewBirthdayTableViewCell.self, forCellReuseIdentifier: FindNewBirthdayTableViewCell.identifier)
        tableView.register(NotFindBirthdayTableViewCell.self, forCellReuseIdentifier: NotFindBirthdayTableViewCell.identifier)

        tableView.separatorStyle = .none
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    
    func addBirthday(_ button:UIButton) {
        let vc = SetupRemindTimeViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }

 
}

extension FindNowBirthdayRemindViewController: UITableViewDelegate,UITableViewDataSource, FinNowBirthdayRemindTableViewCellDelegate {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 3
        }else{
            return 5
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (indexPath as NSIndexPath).section == 0 || (indexPath as NSIndexPath).section == 1 {
            let cell: FindNewBirthdayTableViewCell = tableView.dequeueReusableCell(withIdentifier: FindNewBirthdayTableViewCell.identifier) as! FindNewBirthdayTableViewCell
            return cell
        }else{
            let cell: NotFindBirthdayTableViewCell = tableView.dequeueReusableCell(withIdentifier: NotFindBirthdayTableViewCell.identifier) as! NotFindBirthdayTableViewCell
            cell.askFriendButton.tag = 3333 + (indexPath as NSIndexPath).row
            cell.delegate = self
            return cell

        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = Constant.Theme.Color10
        headerView.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: Constant.ScreenSizeV2.MARGIN_60)
        let headerLabel = UILabel()
        headerLabel.textColor = Constant.Theme.Color14
        headerLabel.font = Constant.Theme.Font_13
        headerView.addSubview(headerLabel)
        switch section {
        case 0:
            headerLabel.text = "新发现的好友生日"
        case 1:
            headerLabel.text = "未添加的好友生日"
        case 2:
            headerLabel.text = "未发现的好友生日"
        default: break
            
        }
        
        headerLabel.snp_makeConstraints { (make) in
            let _ = make.left.equalTo(Constant.ScreenSizeV2.MARGIN_30)
            let _ = make.centerY.equalTo(headerView)
        }
        
        return headerView
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return Constant.ScreenSizeV2.MARGIN_60
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140 * Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
    
    func onClickAskFriendButton(_ button: UIButton) {
        let vc = AskFriendsBirthdayViewController.newAskFriendsBirthdayViewController("风清扬", phoneNumber: "1865473596")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}


extension FindNowBirthdayRemindViewController {
    
    class NotFindBirthdayTableViewCell: UITableViewCell {
        static let identifier = "NotFindBirthdayTableView"
        
        fileprivate let peopleImageView:UIImageView = {
            let imageView = UIImageView()
            imageView.layer.cornerRadius = Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 45
            imageView.layer.masksToBounds = true
            imageView.image = UIImage(named:"accountIcon")
            return imageView
        }()
        
        fileprivate let nameLabel:UILabel = {
            let lbl = UILabel()
            lbl.text = "风清扬"
            lbl.font = UIFont.boldSystemFont(ofSize: Constant.common_F4_font)
            lbl.textColor = Constant.Theme.Color2
            return lbl
        }()
        
        fileprivate let phoneNumberLabel:UILabel = {
            let lbl = UILabel()
            lbl.text = "(18615473596)"
            lbl.font = Constant.Theme.Font_13
            lbl.textColor = Constant.Theme.Color7
            return lbl
        }()
        
        fileprivate let askFriendButton:UIButton = {
            let btn = UIButton()
            btn.setTitle("询问好友", for: UIControlState())
            btn.setTitleColor(Constant.Theme.Color1, for: UIControlState())
            btn.layer.cornerRadius = Constant.ScreenSizeV2.MARGIN_10
            btn.layer.borderWidth = 0.5
            btn.layer.borderColor = Constant.Theme.Color1.cgColor
            return btn
        }()
        
        fileprivate let lineView:UIView = {
            let view = UIView()
            view.backgroundColor = Constant.Theme.Color10
            return view
        }()
        
        
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)
            self.selectionStyle = .none
            
            setupCell()
            
        }
        var delegate:FinNowBirthdayRemindTableViewCellDelegate?
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func onClick(_ button:UIButton) {
            delegate?.onClickAskFriendButton(button)
        }
        
        func setupCell() {
            self.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 140)
            
            askFriendButton.addTarget(self, action: #selector(NotFindBirthdayTableViewCell.onClick(_:)), for: UIControlEvents.touchUpInside)
            let _ = [peopleImageView, nameLabel, phoneNumberLabel, askFriendButton, lineView].map{self.addSubview($0)}
            
            peopleImageView.snp_makeConstraints { (make) in
                let _ = make.left.equalTo(self).offset(Constant.ScreenSizeV2.MARGIN_30)
                let _ = make.centerY.equalTo(self)
                let _ = make.width.equalTo(90 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
                let _ = make.height.equalTo(90 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
            }
            
            nameLabel.snp_makeConstraints { (make) in
                let _ = make.left.equalTo(peopleImageView.snp_right).offset(Constant.ScreenSizeV2.MARGIN_16)
                let _ = make.centerY.equalTo(self)
            }
            
            phoneNumberLabel.snp_makeConstraints { (make) in
                let _ = make.left.equalTo(nameLabel.snp_right).offset(Constant.ScreenSizeV2.MARGIN_16)
                let _ = make.centerY.equalTo(self)
            }
            
            askFriendButton.snp_makeConstraints { (make) in
                let _ = make.right.equalTo(self).offset(-Constant.ScreenSizeV2.MARGIN_30)
                let _ = make.centerY.equalTo(self)
                let _ = make.height.equalTo(Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 70)
                let _ = make.width.equalTo(Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * 220)
            }
            
            lineView.snp_makeConstraints { (make) in
                let _ = make.bottom.equalTo(self)
                let _ = make.left.equalTo(self)
                let _ = make.right.equalTo(self)
                let _ = make.height.equalTo(0.5)
            }
        }
    }
    
    class FindNewBirthdayTableViewCell:UITableViewCell {
        
        static let identifier = "FindNewBirthdayTableViewCell"
        fileprivate let optionalButton:UIButton = {
            let btn = UIButton()
            btn.frame.size = CGSize(width: Constant.ScreenSizeV2.MARGIN_36, height: Constant.ScreenSizeV2.MARGIN_36)
            btn.setImage(UIImage(named:"add_gift_reminder_unchoose"), for: UIControlState())
            btn.setImage(UIImage(named:"choosed"), for: UIControlState.selected)
            return btn
        }()
        
        fileprivate let peopleImageView:UIImageView = {
            let imageView = UIImageView()
            imageView.layer.cornerRadius = Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 45
            imageView.layer.masksToBounds = true
            imageView.image = UIImage(named:"accountIcon")
            return imageView
        }()
        
        fileprivate let nameLabel:UILabel = {
            let lbl = UILabel()
            lbl.text = "风清扬"
            lbl.font = UIFont.boldSystemFont(ofSize: Constant.common_F4_font)
            lbl.textColor = Constant.Theme.Color2
            return lbl
        }()
        
        fileprivate let phoneNumberLabel:UILabel = {
            let lbl = UILabel()
            lbl.text = "(18615473596)"
            lbl.font = Constant.Theme.Font_13
            lbl.textColor = Constant.Theme.Color7
            return lbl
        }()
        
        fileprivate let birthdayLabel:UILabel = {
            let lbl = UILabel()
            lbl.text = "1995年3月12日"
            lbl.font = Constant.Theme.Font_13
            lbl.textColor = Constant.Theme.Color14
            return lbl
        }()
        
        fileprivate let lineView:UIView = {
            let view = UIView()
            view.backgroundColor = Constant.Theme.Color10
            return view
        }()
        
        fileprivate let infoView = UIView()
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)
            self.selectionStyle = .none
            setupCell()
            
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func onClick(_ button:UIButton) {
            button.isSelected = !button.isSelected
        }
        
        func setupCell() {
            self.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 140)
            let _ = [nameLabel, phoneNumberLabel, birthdayLabel].map{infoView.addSubview($0)}
            optionalButton.isSelected = true
            optionalButton.addTarget(self, action: #selector(FindNewBirthdayTableViewCell.onClick(_:)), for: UIControlEvents.touchUpInside)
            let _ = [optionalButton, peopleImageView, infoView, lineView].map{self.addSubview($0)}
            
            optionalButton.snp_makeConstraints { (make) in
                let _ = make.left.equalTo(self).offset(Constant.ScreenSizeV2.MARGIN_30)
                let _ = make.centerY.equalTo(self)
                let _ = make.width.equalTo(Constant.ScreenSizeV2.MARGIN_36)
                let _ = make.height.equalTo(Constant.ScreenSizeV2.MARGIN_36)
            }
            
            peopleImageView.snp_makeConstraints { (make) in
                let _ = make.centerY.equalTo(self)
                let _ = make.left.equalTo(optionalButton.snp_right).offset(Constant.ScreenSizeV2.MARGIN_20)
                let _ = make.width.equalTo(90 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
                let _ = make.height.equalTo(90 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
            }
            
            nameLabel.snp_makeConstraints { (make) in
                let _ = make.top.equalTo(infoView)
                let _ = make.left.equalTo(infoView)
            }
            
            phoneNumberLabel.snp_makeConstraints { (make) in
                let _ = make.centerY.equalTo(nameLabel)
                let _ = make.left.equalTo(nameLabel.snp_right).offset(Constant.ScreenSizeV2.MARGIN_16)
                let _ = make.right.equalTo(infoView)
            }
            
            birthdayLabel.snp_makeConstraints { (make) in
                let _ = make.top.equalTo(nameLabel.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_13)
                let _ = make.left.equalTo(infoView)
                let _ = make.bottom.equalTo(infoView)
            }
            
            infoView.snp_makeConstraints { (make) in
                let _ = make.centerY.equalTo(self)
                let _ = make.left.equalTo(peopleImageView.snp_right).offset(Constant.ScreenSizeV2.MARGIN_16)
            }
            
            lineView.snp_makeConstraints { (make) in
                let _ = make.bottom.equalTo(self)
                let _ = make.left.equalTo(self)
                let _ = make.right.equalTo(self)
                let _ = make.height.equalTo(0.5)
            }
        }
        
        func reloadData() {
            
        }
    }
}

protocol FinNowBirthdayRemindTableViewCellDelegate {
    func onClickAskFriendButton(_ button:UIButton)
}









