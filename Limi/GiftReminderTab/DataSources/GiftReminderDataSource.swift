//
//  GiftReminderDataSource.swift
//  Limi
//
//  Created by 倪晅 on 16/1/10.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class GiftReminderDataSource: BaseTableDataSource {
    
    var lastTime: String?
    var birthdayCount: Observable<NSInteger> = Observable(0)
    
    fileprivate var isFirst = true
    
    override func fetchRefreshDataWithSuccess(_ success: @escaping TableDataSourceSuccessBlock, failure: @escaping TableDataSourceFailureBlock) {
        let parameters = [
            "type": self.type
        ]
        let appDelegate = UIApplication.shared.delegate as! AppDelegate

        self.loadLocalData(appDelegate.currentViewController!.view,withParameters: parameters as [String : AnyObject], success: success, failure: failure)
        

    }
    
    override func fetchMoreDataWithSuccess(_ success: @escaping TableDataSourceSuccessBlock, failure: @escaping TableDataSourceFailureBlock) {
        var parameters = [String:AnyObject]()
        if self.lastTime != nil {
            parameters = [
                "last_time": self.lastTime! as AnyObject,
                "type": self.type as AnyObject
            ]
            let appDelegate = UIApplication.shared.delegate as! AppDelegate

            self.loadLocalData(appDelegate.currentViewController!.view,withParameters: parameters, success: success, failure: failure)
            
                
        }
    }
    
    
    fileprivate func loadLocalData(_ context:UIView,withParameters parameters: [String:AnyObject], success: @escaping TableDataSourceSuccessBlock, failure: @escaping TableDataSourceFailureBlock)
    {
        if isFirst == true
        {
//            if let homeDB = CoreDataManager.shared.getDBManager(Constant.CoreDataType.homeAAGiftChoiceType)
//            {
//                let db = homeDB as! HomeAAGiftChoice
//                
//                if let list = db.homeRmind as? NSMutableArray
//                {
//                    let last = list.lastObject as! NSDictionary
//                    let lastGiftTime = last["Gift_time"] as? String
//                    
//                    if lastGiftTime != nil {
//                        self.lastTime = lastGiftTime! + " 00:00:00"
//                    }
//                    
//                    success(list as [AnyObject])
//                }
//            }
            
            isFirst = false
            
            self.request(context,withParameters: parameters, success: success, failure: failure)
        }else
        {
            self.request(context,withParameters: parameters, success: success, failure: failure)
        }
    }
    
    
    fileprivate func request(_ context:UIView,withParameters parameters: [String:AnyObject], success: @escaping TableDataSourceSuccessBlock, failure: @escaping TableDataSourceFailureBlock) {
        var requestURL: String = ""
        var parames: [String:AnyObject] = parameters
        if self.type == 4 {
            requestURL = Constant.JTAPI.important_remind_list
            parames["type"] = 0 as AnyObject?

        }else{
            requestURL = AccountManager.shared.isLogin() ? Constant.JTAPI.user_relation_list : Constant.JTAPI.user_relation_list_v2
        }
        
        
        let _ = LMRequestManager.requestByDataAnalyses(context:context,URLString: requestURL, parameters: parames, isNeedUserTokrn: AccountManager.shared.isLogin() ? true : false,successHandler: {data,status,msg in
            print(data)
            if status != 200 {
                failure(nil)
                return
            }
            
            let dict = data["data"] as! NSDictionary
            if let _ = dict["birthday_num"] as? NSInteger {
                self.birthdayCount <- (dict["birthday_num"] as! NSInteger)
            }
            let fatherBirthday = [
                "situation": AddGiftReminderSituation.birthday.rawValue,
                "relation": AddGiftReminderRelation.father.rawValue
            ]
            let motherBirthday = [
                "situation": AddGiftReminderSituation.birthday.rawValue,
                "relation": AddGiftReminderRelation.mother.rawValue
            ]
            let list: NSMutableArray = []
            if let list1 = dict["list"] as? NSArray {
                
                for any in list1 {
                    list.add(any)
                }

                if let tipDict = dict["tip_list"] as? NSDictionary {
                    if let _ = parameters["last_time"] as? String {
                    } else {
                        if self.type == 0 {
                            let fatherReminderStatus = tipDict["father_status"] as! Bool
                            let motherReminderStatus = tipDict["mother_status"] as! Bool
                            if !motherReminderStatus {
                                list.insert(motherBirthday, at: 0)
                                AccountManager.shared.setMontherAdd(false)
                            } else {
                                AccountManager.shared.setMontherAdd(true)
                            }
                            if !fatherReminderStatus {
                                list.insert(fatherBirthday, at: 0)
                                AccountManager.shared.setDadAdd(false)
                            } else {
                               AccountManager.shared.setDadAdd(true)
                            }
                        }
                    }
                } else {
                    if let _ = parameters["last_time"] as? String {
                    } else {
                        if self.type == 0 {
                            if AccountManager.shared.isMontherAdd() == false {
                                list.insert(motherBirthday, at: 0)
                            }
                            if AccountManager.shared.isDadAdd() == false{
                                list.insert(fatherBirthday, at: 0)
                            }
                        }
                    }
                }
                
//                if let homeDB = CoreDataManager.shared.getDBManager(Constant.CoreDataType.homeAAGiftChoiceType)
//                {
//                    let db = homeDB as! HomeAAGiftChoice
//                    db.homeRmind = list
//                    CoreDataManager.shared.save()
//                }
                
                if list.count != 0 {
                
                    let last = list.lastObject as! NSDictionary
                    let lastGiftTime = last["Gift_time"] as? String
                    if lastGiftTime != nil {
                        self.lastTime = lastGiftTime! + " 00:00:00"
                    }
                }
                success(list as [AnyObject])
            } else {
                
                success([])
            }
            }){Void in failure(nil)}
    }
    
    override func createViewModelWithModel(_ model: AnyObject) -> BaseTableCellViewModel {
        var cellClass = BaseTableCell.self
        let dict = model as! NSDictionary
        if dict["situation"] != nil && dict["relation"] != nil {
            cellClass = GiftReminderAddTableCell.self
        } else {
            cellClass = GiftReminderTableCell.self
        }
        return BaseTableCellViewModel.init(model: model, cellClass: cellClass)
    }
    
    override func fixedHeightForCell() -> CGFloat {
        return 100
    }
    
    func readBy(_ id: NSInteger) -> NSInteger {
        var index = 0
        for viewModel in self.data {
            let vm = viewModel as? BaseTableCellViewModel
            if let dict = vm!.model as? [String: AnyObject] {
                if let dictId = dict["Id"] as? NSInteger {
                    if dictId == id {
                        return index
                    }
                }
            }
            index += 1
        }
        return index
    }
    
}
