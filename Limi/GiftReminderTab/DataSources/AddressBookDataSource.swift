//
//  AddressBookDataSource.swift
//  Limi
//
//  Created by 千云锋 on 16/8/18.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import Foundation
import CoreData


class AddressBookDataSource: NSObject {
    
    fileprivate var onceCount = 100//一次上传的数据量
    var sourceArray = NSMutableArray()//筛选过后的合法联系人
    var screenedArray = [[String:String]]()
    
    fileprivate let should = 100
    var allRound = 0 //记录有多少组  一组100个数据
    
    static let sharedInstance = AddressBookDataSource()
    
    fileprivate override init() {}
    
    internal func analyzeSysContacts(_ sysContacts:NSArray) {
        if sysContacts.count > 0 {
            
            self.analyzeSysContacts1(sysContacts)
            
        }
    }
    //No.1.1 初步过滤数据，上传服务器check
    fileprivate func analyzeSysContacts1(_ sysContacts:NSArray)
    {
        if sysContacts.count < 1{return}
        
        //过滤无号码联
        for contact in sysContacts
        {
            let dict = AddressBookManager.analiyseConnectCard(contact as ABRecord!)
            
            if  (dict?["Name"] as! String) != "null" && (dict?["PhoneNumbers"] as! NSArray).count > 0 //是否有手机号码
            {
                sourceArray.add(createParems(dict?.copy() as! NSDictionary))//初步筛选数据 //整理数据   让每一个电话号都可以有对应的姓名和生日 分解了一个联系人多个手机号的情况，从而得到了源数据（也就是要上传的数据） sourceArray德玛数量也就是通讯录电话号的个数   而不是联系人的个数（一个联系人课能有多个电话号）
                print(sourceArray)
//                screenedArray.addObject(dict)
            }
        }
        
        //以手机号为单位  再次详细解析通讯录
        analiyseAddressBook()

        //存到本地数据库
        preserveLocalDatabass()
        
        //上传服务器，比对数据database
        self.supLoad()
        
        
        
    }
    
    func preserveLocalDatabass() {
        
        let context = CoreDataManager.shared.managedObjectContext
        
        
        for data in screenedArray {
            
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName:"AddressBook")

            let entity = NSEntityDescription.entity(forEntityName: "AddressBook", in: context)
            fetchRequest.entity = entity
            ///  判断查询对象是否为空 防止崩溃
            if (entity != nil) {
                ///  查询结果
                do{
                    /// 成功
                    let qwqwrr = try context.fetch(fetchRequest) as? [AddressBook]
                    var index = 0
                    for info in qwqwrr! {
                        if info.phoneNumber == data["phoneNumber"] {
                            index = 1
                            continue
                        }
                    }
                    
                    if index == 0 {
                        //创建一个实例并给属性赋值
                        let addressBookInfo = NSEntityDescription.insertNewObject(forEntityName: "AddressBook", into: context)as! AddressBook
                        addressBookInfo.birthday = data["birthday"]
                        addressBookInfo.name = data["name"]
                        addressBookInfo.phoneNumber = data["phoneNumber"]
                        addressBookInfo.birthdayStyle = data["birthday_type"]
                        if data["birthday"] == "null" {
                            addressBookInfo.style = "3"
                        }else {

                            addressBookInfo.style = "1"
                        }

                        gcd.async(.main) {
                            NotificationCenter.default.post(name: Notification.Name(rawValue: "discoverAddressBookChange"), object: nil)
                        }
                        
                    }
                    
                }catch{
                    /// 失败
                    fatalError("查询失败：\(error)")
                }
            }else{
                ///  查询对象不存在
                print("查询失败：查询不存在")
            }

            
            
        }
        
        do {
            try context.save()
            print("----保存成功----")
        } catch {
            fatalError("不能保存：\(error)")
        }
        
        
    }
    
    
    fileprivate func analiyseAddressBook() {
        var oAddressBookArr:[[String:String]] = [[String:String]]()

        for i in 0 ..< sourceArray.count
        {
            
            let userInfo = sourceArray[i] as! [String:AnyObject]
            let phoneAry = userInfo["mobile"] as! [String]//电话数组
            
            let nameAry = userInfo["name"] as! [String]//姓名数组
            
            let birthDayAry = userInfo["birthday"] as! [String]//生日数组
            
            let birthdayTypeAry = userInfo["birthday_type"] as! [String]//生日类型数组
            
            
            for j in 0 ..< phoneAry.count
            {
                var dic = [String:String]()
                dic["name"] = nameAry[j]
                dic["birthday"] = birthDayAry[j]
                dic["phoneNumber"] = phoneAry[j]
                dic["birthday_type"] = birthdayTypeAry[j]
                if birthDayAry[j] == "null" {
                    dic["type"] = "3"
                }else{
                    dic["type"] = "2"
                }
                oAddressBookArr.append(dic)
            }

        }
        screenedArray = oAddressBookArr
        print(screenedArray)
    }
    
    //上传服务器，比对数据
    fileprivate func supLoad()
    {
        var ophoneAry:[String]?//电话数组
        
        var onameAry:[String]?//姓名数组
        
        var obirthDayAry:[String]?//生日数组
        
        var obirthdayTypeAry:[String]?//生日类型数组
        
        var index_start = 0//开始位置
        
        var index_end = 0//结束位置
        
        var isfirstUpLoad = true
        
        
        
        allRound = sourceArray.count
        
        if sourceArray.count < onceCount
        {
            allRound = 1
        }else
        {
            if sourceArray.count % onceCount == 0
            {
                allRound = sourceArray.count/onceCount
            }else
            {
                allRound = sourceArray.count/onceCount + 1
            }
        }
        
        for i in 0 ..< sourceArray.count
        {
            if i == 0
            {
                ophoneAry = [String]()//电话数组
                
                onameAry = [String]()//姓名数组
                
                obirthDayAry = [String]()//生日数组
                
                obirthdayTypeAry = [String]()//生日类型数组
            }
            
            
            let userInfo = sourceArray[i] as! [String:AnyObject]
            let phoneAry = userInfo["mobile"] as! [String]//电话数组
            
            let nameAry = userInfo["name"] as! [String]//姓名数组
            
            let birthDayAry = userInfo["birthday"] as! [String]//生日数组
            
            let birthdayTypeAry = userInfo["birthday_type"] as! [String]//生日类型数组
            
            
            for j in 0 ..< phoneAry.count
            {
                ophoneAry?.append(phoneAry[j])//电话数组
                
                onameAry?.append(nameAry[j])//姓名数组
                
                obirthDayAry?.append(birthDayAry[j])//生日数组
                
                obirthdayTypeAry?.append(birthdayTypeAry[j])//生日类型数组
            }
            
            
            //每 onceCount 个数据上传到服务器一次  记录上传的次数，没个现成返回一次，返回的次数等于上传的次数就说明已经完成了
            if i != 0 && (i % onceCount == 0 || i == sourceArray.count - 1)//100个数据一组
            {
                index_start = isfirstUpLoad == true ? 0 : index_end
                
                index_end = i
                
                var params = [String:AnyObject]()
                
                params["birthday_type"] = obirthdayTypeAry as AnyObject?
                
                params["mobile"] = ophoneAry as AnyObject?
                
                params["birthday"] = obirthDayAry as AnyObject?
                
                params["name"] = onameAry as AnyObject?
                
                updatePresentRemaind(params)
                ophoneAry = [String]()//电话数组
                
                onameAry = [String]()//姓名数组
                
                obirthDayAry = [String]()//生日数组
                
                obirthdayTypeAry = [String]()//生日类型数组
                
                isfirstUpLoad = false
            }else if sourceArray.count == 1//只有一个联系人
            {
                index_start = 0
                
                index_end = 0
                
                var params = [String:AnyObject]()
                
                params["birthday_type"] = obirthdayTypeAry as AnyObject?
                
                params["mobile"] = ophoneAry as AnyObject?
                
                params["birthday"] = obirthDayAry as AnyObject?
                
                params["name"] = onameAry as AnyObject?
                
                updatePresentRemaind(params)
            }
        }
    }
    
    //No.2 本地数据上传服务器
    fileprivate func updatePresentRemaind(_ params:[String: AnyObject])
    {
        let urlStr = Constant.JTAPI.user_relation_upload
        let _ = LMRequestManager.requestByDataAnalyses(context:UIApplication.shared.keyWindow!,URLString: urlStr, parameters: params, isNeedUserTokrn: true, isShowErrorStatuMsg: true, successHandler: {data,status,msg in

        })
    }


    //No.1.2.1 创建数据字典
    func createParems(_ userInfo:NSDictionary)->[String:AnyObject]
    {
        var params = [String:AnyObject]()
        
        var phone =  [String]()
        
        for i in 0 ..< (userInfo["PhoneNumbers"] as! NSArray).count
        {
            phone.append(((userInfo["PhoneNumbers"] as! NSArray)[i] as? String)!)
        }
        
        
        params["mobile"] = phone as AnyObject?
        
        params = createParameter(userInfo,params:params,count:phone.count)
        
        return params
    }
    
    
    
    //No.1.2.2 创建其它参数字典
    func createParameter(_ userInfo:NSDictionary, params:[String: AnyObject],count:Int)->[String: AnyObject]
    {
        var names =  [String]()
        
        var birthday_types =  [String]()
        
        var birthdays =  [String]()
        
        var genders =  [String]()
        
        
        for _ in 0 ..< count
        {
            names.append((userInfo["Name"] as? String)!)
            
            birthdays.append((userInfo["Birthday"] as? String)!)
            
            birthday_types.append("1")
            
            genders.append("0")
        }
        var total_params = params
        total_params["name"] = names as AnyObject?
        
        total_params["birthday_type"] = birthday_types as AnyObject?
        
        total_params["birthday"] = birthdays as AnyObject?
        
        return total_params
    }

    

}


