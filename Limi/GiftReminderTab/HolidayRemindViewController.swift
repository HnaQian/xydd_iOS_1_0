//
//  HolidayRemindViewController.swift
//  Limi
//
//  Created by ZhiQiangMi on 16/4/11.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class HolidayRemindViewController: BaseTableViewController {
    
    fileprivate var _emptyListView: GiftReminderEmptyListView?
    fileprivate var viewModel : BaseTableCellViewModel?
    fileprivate var isLogin: Bool = UserInfoManager.didLogin
    fileprivate let guideAuthvc = GuideAuthContactViewController()
    var navigationVC:UINavigationController?
    override var navigationController: UINavigationController? { return navigationVC}
    
    
    var emptyListView: GiftReminderEmptyListView {
        get {
            if _emptyListView == nil {
                _emptyListView = GiftReminderEmptyListView()
                _emptyListView!.isHidden = true
            }
            return _emptyListView!
        }
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
        
        self.useLoadMoreControl = true
        
        self.dataSource = GiftReminderDataSource(delegate: self, viewModelClass: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewType = .Holiday
        self.gesturBackEnable = false
        self.tableView.backgroundColor = UIColor(rgba:Constant.common_background_color)
        
        
        self.tableView.addSubview(self.emptyListView)
        self.emptyListView.snp_makeConstraints { (make) -> Void in
            let _ = make.centerX.equalTo(self.tableView)
            let _ = make.centerY.equalTo(self.tableView)
        }
        
        self.tableView.register(GiftReminderTableCell.self, forCellReuseIdentifier: NSStringFromClass(GiftReminderTableCell.self))
        
//        let dataSource = self.dataSource as! GiftReminderDataSource
        self.dataSource?.type = 2
        self.pullRefresh()
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: Constant.notification_login_success), object: nil, queue: OperationQueue.main) { (notification) -> Void in
            self.pullRefresh()
        }
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: Constant.notification_logout_success), object: nil, queue: OperationQueue.main) { (notification) -> Void in
            self.pullRefresh()
        }
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: Constant.notification_import_contact_success), object: nil, queue: OperationQueue.main) { (notification) -> Void in
            self.pullRefresh()
        }
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: Constant.notification_add_gift_reminder_success), object: nil, queue: OperationQueue.main) { (notification) -> Void in
            self.pullRefresh()
        }

        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "Request"), object: nil, queue: OperationQueue.main) { (notification) -> Void in
            self.pullRefresh()
        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let viewModel = self.dataSource!.dataAtIndexPath(indexPath)
        let dict = viewModel!.model as! NSDictionary
        if let url : String = dict.object(forKey: "Url") as? String
        {
            if url != ""{
                let webVC = WebViewController(URL: URL(string: url)!)
                if let Uid : Int = dict.object(forKey: "Uid") as? Int{
                    if Uid != 0{
                        if let scene : Int = dict.object(forKey: "Scene") as? Int{
                            if scene == 1{
                                webVC.isHaveBirthdayWiki = true
                            }
                            else{
                                webVC.isHaveBirthdayWiki = false
                            }
                            if let id : Int = dict.object(forKey: "Id") as? Int{
                                webVC.RemindDetailId = id
                            }
                            
                            webVC.type = Constant.WebViewType.remindDetailType.rawValue
                        }
                    }
                }
                
                if let scene : Int = dict.object(forKey: "Scene") as? Int,
                    let id : Int = dict.object(forKey: "Id") as? Int{
                    if scene != 1{
                        Statistics.count(Statistics.GiftRemind.remind_holiday_click, andAttributes: ["remindId":"\(id)"])
                    }
                }
                
                webVC.hidesBottomBarWhenPushed = true
                webVC.shareType = Constant.ShareContentType.imageType.rawValue
                self.navigationVC?.pushViewController(webVC, animated: true)
                
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    override func dataSource(_ dataSource: BaseTableDataSource, didFinishRefreshWithError error: NSError?) {
        super.dataSource(dataSource, didFinishRefreshWithError: error)
        
        self.emptyListView.isHidden = dataSource.data.count > 0
    }
    
}
