//
//  GiftReminderGenderChooseView.swift
//  Limi
//
//  Created by 倪晅 on 16/1/12.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

protocol GiftReminderGenderChooseViewDelegate {
    func didTapGenderChoose(_ gender: Int,purposeType:Int)
    func didTapClose()
}

class GiftReminderGenderChooseView: UIView {
    
    fileprivate var _closeButton: UIButton?
    fileprivate var _titleLabel: UILabel?
    fileprivate var _maleButton: UIButton?
    fileprivate var _femaleButton: UIButton?
    var _purposeType: Int = 0
    var delegate: GiftReminderGenderChooseViewDelegate?
    
    var closeButton: UIButton {
        get {
            if _closeButton == nil {
                _closeButton = UIButton(type: .custom)
                _closeButton!.setImage(UIImage(named: "new_close_button"), for: UIControlState())
                _closeButton!.addTarget(self, action: #selector(GiftReminderGenderChooseView.didTapClose), for: .touchUpInside)
            }
            return _closeButton!
        }
    }
    
    var titleLabel: UILabel {
        get {
            if _titleLabel == nil {
                _titleLabel = UILabel()
                if self._purposeType == 0{
                _titleLabel!.text = "添加送礼提醒前，请设置你的性别"
                }
                else if self._purposeType == 1{
                  _titleLabel!.text = "导入联系人前，请设置你的性别"
                }
                _titleLabel!.numberOfLines = 0
                _titleLabel!.textAlignment = .center
            }
            return _titleLabel!
        }
    }
    
    var maleButton: UIButton {
        get {
            if _maleButton == nil {
                _maleButton = UIButton.init(type: .custom)
                _maleButton!.setTitleColor(Constant.Theme.Color7, for: UIControlState())
                _maleButton!.titleLabel?.font = Constant.Theme.Font_15
                _maleButton!.tag = AddGiftReminderGender.male.rawValue
                _maleButton!.addTarget(self, action: #selector(GiftReminderGenderChooseView.didTapGenderChoose(_:)), for: .touchUpInside)
            }
            return _maleButton!
        }
    }
    
    var femaleButton: UIButton {
        get {
            if _femaleButton == nil {
                _femaleButton = UIButton.init(type: .custom)
                _femaleButton!.setTitleColor(Constant.Theme.Color7, for: UIControlState())
                _femaleButton!.titleLabel?.font = Constant.Theme.Font_15
                _femaleButton!.tag = AddGiftReminderGender.female.rawValue
                _femaleButton!.addTarget(self, action: #selector(GiftReminderGenderChooseView.didTapGenderChoose(_:)), for: .touchUpInside)
            }
            return _femaleButton!
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.white
        self.layer.cornerRadius = 10
        self.layer.masksToBounds = true
        
        self.addSubview(self.closeButton)
        self.addSubview(self.titleLabel)
        self.addSubview(self.maleButton)
        self.addSubview(self.femaleButton)
        
        self.closeButton.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self).offset(10)
            let _ = make.right.equalTo(self).offset(-10)
        }
        self.titleLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self).offset(40)
            let _ = make.left.equalTo(self).offset(20)
            let _ = make.right.equalTo(self).offset(-20)
        }
        self.maleButton.snp_makeConstraints { (make) -> Void in
            let _ = make.right.equalTo(self.snp_centerX).offset(-30 * Constant.ScreenSize.SCREEN_WIDTH_SCALE)
            let _ = make.top.equalTo(self.titleLabel.snp_bottom).offset(50)
            let _ = make.width.equalTo(100)
            let _ = make.height.equalTo(125)
        }
        self.femaleButton.snp_makeConstraints { (make) -> Void in
            let _ = make.left.equalTo(self.snp_centerX).offset(30 * Constant.ScreenSize.SCREEN_WIDTH_SCALE)
            let _ = make.top.equalTo(self.titleLabel.snp_bottom).offset(50)
            let _ = make.width.equalTo(100)
            let _ = make.height.equalTo(125)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func didTapGenderChoose(_ sender: UIButton) {
        let gender = sender.tag
        if self.delegate != nil {
            self.delegate!.didTapGenderChoose(gender,purposeType: _purposeType)
        }
    }
    
    func didTapClose() {
        if self.delegate != nil {
            self.delegate!.didTapClose()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.maleButton.set(image: UIImage(named: "guide_gender_male"), title: "帅哥", titlePosition: .bottom, additionalSpacing: 10, state: UIControlState())
        self.femaleButton.set(image: UIImage(named: "guide_gender_female"), title: "美女", titlePosition: .bottom, additionalSpacing: 10, state: UIControlState())
    }

}
