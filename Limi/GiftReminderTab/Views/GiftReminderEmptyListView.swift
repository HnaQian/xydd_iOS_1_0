//
//  GiftReminderEmptyListView.swift
//  Limi
//
//  Created by 倪晅 on 16/1/22.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class GiftReminderEmptyListView: UIView {
    
    fileprivate var _emptyImage: UIImageView?
    fileprivate var _emptyLabel: UILabel?
    
    var emptyImage: UIImageView {
        get {
            if _emptyImage == nil {
                _emptyImage = UIImageView(image: UIImage(named: "search_biaoQing"))
            }
            return _emptyImage!
        }
    }
    
    var emptyLabel: UILabel {
        get {
            if _emptyLabel == nil {
                _emptyLabel = UILabel()
                _emptyLabel!.font = Constant.Theme.Font_15
                _emptyLabel!.textColor = Constant.Theme.Color6
                _emptyLabel!.numberOfLines = 0
                _emptyLabel!.textAlignment = .center
            }
            return _emptyLabel!
        }
    }
    
    override var isHidden: Bool {
        didSet {
            if AccountManager.shared.isLogin() {
                self.emptyLabel.text = "很抱歉，你尚未添加任何提醒，添加试试吧"
            } else {
                self.emptyLabel.text = "很抱歉，未登录用户暂不支持记录提醒信息呢"
                
            }
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor(rgba: "#F2F3F5")
        
        self.addSubview(self.emptyImage)
        self.addSubview(self.emptyLabel)
        
        self.emptyImage.snp_makeConstraints { (make) -> Void in
            let _ = make.centerX.equalTo(self)
            let _ = make.bottom.equalTo(self.emptyLabel.snp_top).offset(-20)
//            make.height.equalTo(93)
//            make.width.equalTo(96)
        }
        self.emptyLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.bottom.equalTo(self.snp_centerY)
            let _ = make.left.equalTo(self).offset(10)
            let _ = make.right.equalTo(self).offset(-10)
            let _ = make.height.equalTo(Constant.Theme.Font_15.lineHeight)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
