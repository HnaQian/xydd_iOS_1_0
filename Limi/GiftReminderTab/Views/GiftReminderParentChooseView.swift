//
//  GiftReminderGenderChooseView.swift
//  Limi
//
//  Created by 倪晅 on 16/1/12.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

protocol GiftReminderParentChooseViewDelegate {
    func didTapParentChoose(_ relation: Int)
    func didTapParentImport()
    func didTapClose()
}

class GiftReminderParentChooseView: UIView {
    
    fileprivate var _closeButton: UIButton?
    fileprivate var _titleLabel: UILabel?
    fileprivate var _fatherButton: UIButton?
    fileprivate var _motherButton: UIButton?
    fileprivate var _importButton: UIButton?
    
    var delegate: GiftReminderParentChooseViewDelegate?
    
    var closeButton: UIButton {
        get {
            if _closeButton == nil {
                _closeButton = UIButton(type: .custom)
                _closeButton!.setImage(UIImage(named: "new_close_button"), for: UIControlState())
                _closeButton!.addTarget(self, action: #selector(GiftReminderParentChooseView.didTapClose), for: .touchUpInside)
            }
            return _closeButton!
        }
    }
    
    var titleLabel: UILabel {
        get {
            if _titleLabel == nil {
                _titleLabel = UILabel()
                _titleLabel!.text = "你尚未添加任何生日\n可以尝试添加下亲密家人的生日哦"
                _titleLabel!.textAlignment = .center
                _titleLabel!.numberOfLines = 3
                _titleLabel!.textColor = Constant.Theme.Color6
                _titleLabel!.font = Constant.Theme.Font_17
            }
            return _titleLabel!
        }
    }
    
    var fatherButton: UIButton {
        get {
            if _fatherButton == nil {
                _fatherButton = UIButton.init(type: .custom)
                _fatherButton!.setTitleColor(Constant.Theme.Color7, for: UIControlState())
                _fatherButton!.titleLabel?.font = Constant.Theme.Font_15
                _fatherButton!.tag = AddGiftReminderRelation.father.rawValue
                _fatherButton!.addTarget(self, action: #selector(GiftReminderParentChooseView.didTapParentChoose(_:)), for: .touchUpInside)
            }
            return _fatherButton!
        }
    }
    
    var motherButton: UIButton {
        get {
            if _motherButton == nil {
                _motherButton = UIButton.init(type: .custom)
                _motherButton!.setTitleColor(Constant.Theme.Color7, for: UIControlState())
                _motherButton!.titleLabel?.font = Constant.Theme.Font_15
                _motherButton!.tag = AddGiftReminderRelation.mother.rawValue
                _motherButton!.addTarget(self, action: #selector(GiftReminderParentChooseView.didTapParentChoose(_:)), for: .touchUpInside)
            }
            return _motherButton!
        }
    }
    
    var importButton: UIButton {
        get {
            if _importButton == nil {
                _importButton = UIButton(type: .custom)
                _importButton!.setTitle("从通讯录导入", for: UIControlState())
                _importButton!.setTitleColor(Constant.Theme.Color1, for: UIControlState())
                _importButton!.addTarget(self, action: #selector(GiftReminderParentChooseView.didTapParentImport), for: .touchUpInside)
            }
            return _importButton!
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.white
        self.layer.cornerRadius = 5
        self.layer.masksToBounds = true
        
        self.addSubview(self.closeButton)
        self.addSubview(self.titleLabel)
        self.addSubview(self.fatherButton)
        self.addSubview(self.motherButton)
        self.addSubview(self.importButton)
        
        self.closeButton.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self).offset(10)
            let _ = make.right.equalTo(self).offset(-10)
        }
        self.titleLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self).offset(40)
            let _ = make.left.equalTo(self).offset(20)
            let _ = make.right.equalTo(self).offset(-20)
        }
        self.fatherButton.snp_makeConstraints { (make) -> Void in
            let _ = make.right.equalTo(self.snp_centerX).offset(-30 * Constant.ScreenSize.SCREEN_WIDTH_SCALE)
            let _ = make.top.equalTo(self.titleLabel.snp_bottom).offset(50)
            let _ = make.width.equalTo(100)
            let _ = make.height.equalTo(100)
        }
        self.motherButton.snp_makeConstraints { (make) -> Void in
            let _ = make.left.equalTo(self.snp_centerX).offset(30 * Constant.ScreenSize.SCREEN_WIDTH_SCALE)
            let _ = make.top.equalTo(self.titleLabel.snp_bottom).offset(50)
            let _ = make.width.equalTo(100)
            let _ = make.height.equalTo(100)
        }
        self.importButton.snp_makeConstraints { (make) -> Void in
            let _ = make.centerX.equalTo(self)
            let _ = make.bottom.equalTo(self).offset(-20)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func didTapParentChoose(_ sender: UIButton) {
        let relation = sender.tag
        if self.delegate != nil {
            self.delegate!.didTapParentChoose(relation)
        }
    }
    
    func didTapParentImport() {
        if self.delegate != nil {
            self.delegate!.didTapParentImport()
        }
    }
    
    func didTapClose() {
        if self.delegate != nil {
            self.delegate!.didTapClose()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.fatherButton.set(image: UIImage(named: "guide_family_father"), title: "老爸", titlePosition: .bottom, additionalSpacing: 10, state: UIControlState())
        self.motherButton.set(image: UIImage(named: "guide_family_mother_small"), title: "老妈", titlePosition: .bottom, additionalSpacing: 10, state: UIControlState())
    }

}
