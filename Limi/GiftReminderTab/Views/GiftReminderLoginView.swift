//
//  GiftReminderLoginView.swift
//  Limi
//
//  Created by 倪晅 on 16/1/13.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

protocol GiftReminderLoginViewDelegate {
    func didTapLoginButton(_ isQuick: Bool)
    func didTapClose()
}

class GiftReminderLoginView: UIView {

    fileprivate var _closeButton: UIButton?
    fileprivate var _titleLabel: UILabel?
    fileprivate var _maleButton: UIButton?
    fileprivate var _femaleButton: UIButton?
    fileprivate var _firstTipView: GiftReminderLoginTipView?
    fileprivate var _secondTipView: GiftReminderLoginTipView?
    fileprivate var _thirdTipView: GiftReminderLoginTipView?
    fileprivate var _quickLoginButton: UIButton?
    fileprivate var _loginButton: UIButton?
    
    var delegate: GiftReminderLoginViewDelegate?
    
    var closeButton: UIButton {
        get {
            if _closeButton == nil {
                _closeButton = UIButton(type: .custom)
                _closeButton!.setImage(UIImage(named: "new_close_button"), for: UIControlState())
                _closeButton!.addTarget(self, action: #selector(GiftReminderLoginView.didTapClose), for: .touchUpInside)
            }
            return _closeButton!
        }
    }
    
    var titleLabel: UILabel {
        get {
            if _titleLabel == nil {
                _titleLabel = UILabel()
                _titleLabel!.text = "登录后\n小蜜能为你提供更多的服务哦"
                _titleLabel!.numberOfLines = 2
                _titleLabel!.textAlignment = .center
                _titleLabel!.textColor = Constant.Theme.Color6
                _titleLabel!.font = Constant.Theme.Font_17
            }
            return _titleLabel!
        }
    }
    
    var firstTipView: GiftReminderLoginTipView {
        get {
            if _firstTipView == nil {
                _firstTipView = GiftReminderLoginTipView(imageName: "gift_reminder_login_gift", tipText: "记住属于你的送礼事件")
            }
            return _firstTipView!
        }
    }
    
    var secondTipView: GiftReminderLoginTipView {
        get {
            if _secondTipView == nil {
                _secondTipView = GiftReminderLoginTipView(imageName: "gift_reminder_login_alarm", tipText:"给你温馨的送礼提醒")
            }
            return _secondTipView!
        }
    }
    
    var thirdTipView: GiftReminderLoginTipView {
        get {
            if _thirdTipView == nil {
                _thirdTipView = GiftReminderLoginTipView(imageName: "gift_reminder_login_safe", tipText: "保存数据，永不丢失")
            }
            return _thirdTipView!
        }
    }
    
    var quickLoginButton: UIButton {
        get {
            if _quickLoginButton == nil {
                _quickLoginButton = UIButton(type: .custom)
                _quickLoginButton!.setTitle("免注册快捷登录", for: UIControlState())
                _quickLoginButton!.setTitleColor(Constant.Theme.Color12, for: UIControlState())
                _quickLoginButton!.titleLabel?.font = Constant.Theme.Font_17
                _quickLoginButton!.backgroundColor = UIColor(rgba: "#FF3A48")
                _quickLoginButton!.layer.cornerRadius = 5
                _quickLoginButton!.layer.masksToBounds = true
                _quickLoginButton!.tag = 1
                _quickLoginButton!.addTarget(self, action: #selector(GiftReminderLoginView.didTapLoginButton(_:)), for: .touchUpInside)
            }
            return _quickLoginButton!
        }
    }
    
    var loginButton: UIButton {
        get {
            if _loginButton == nil {
                _loginButton = UIButton(type: .custom)
                _loginButton!.setTitle("用户名密码登录", for: UIControlState())
                _loginButton!.setTitleColor(Constant.Theme.Color1, for: UIControlState())
                _loginButton!.titleLabel?.font = Constant.Theme.Font_17
                _loginButton!.backgroundColor = UIColor.white
                _loginButton!.layer.cornerRadius = 5
                _loginButton!.layer.borderWidth = 0.5
                _loginButton!.layer.borderColor = Constant.Theme.Color1.cgColor
                _loginButton!.layer.masksToBounds = true
                _loginButton!.tag = 0
                _loginButton!.addTarget(self, action: #selector(GiftReminderLoginView.didTapLoginButton(_:)), for: .touchUpInside)
            }
            return _loginButton!
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.white
        self.layer.cornerRadius = 10
        self.layer.masksToBounds = true
        
        self.addSubview(self.closeButton)
        self.addSubview(self.titleLabel)
        self.addSubview(self.firstTipView)
        self.addSubview(self.secondTipView)
        self.addSubview(self.thirdTipView)
        self.addSubview(self.quickLoginButton)
        self.addSubview(self.loginButton)
        
        self.closeButton.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self).offset(10)
            let _ = make.right.equalTo(self).offset(-10)
        }
        self.titleLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.left.equalTo(self).offset(20)
            let _ = make.right.equalTo(self).offset(-20)
            let _ = make.top.equalTo(self).offset(40)
        }
        self.firstTipView.snp_makeConstraints { (make) -> Void in
            if Int(Constant.ScreenSize.SCREEN_HEIGHT) <= 568{
                let _ = make.top.equalTo(self.titleLabel.snp_bottom).offset(30)
            }
            else{
            let _ = make.top.equalTo(self.titleLabel.snp_bottom).offset(40)
            }
            let _ = make.centerX.equalTo(self)
            let _ = make.width.equalTo(180)
            let _ = make.height.equalTo(20)
        }
        self.secondTipView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.firstTipView.snp_bottom).offset(20)
            let _ = make.centerX.equalTo(self)
            let _ = make.width.equalTo(180)
            let _ = make.height.equalTo(20)
        }
        self.thirdTipView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.secondTipView.snp_bottom).offset(20)
            let _ = make.centerX.equalTo(self)
            let _ = make.width.equalTo(180)
            let _ = make.height.equalTo(20)
        }
        self.quickLoginButton.snp_makeConstraints { (make) -> Void in
            let _ = make.centerX.equalTo(self)
            let _ = make.width.equalTo(240)
            let _ = make.height.equalTo(44)
            if Int(Constant.ScreenSize.SCREEN_HEIGHT) <= 568{
                let _ = make.top.equalTo(self.thirdTipView.snp_bottom).offset(30)
            }
            else{
                let _ = make.top.equalTo(self.thirdTipView.snp_bottom).offset(40)
            }
            
        }
        self.loginButton.snp_makeConstraints { (make) -> Void in
            let _ = make.centerX.equalTo(self)
            let _ = make.width.equalTo(240)
            let _ = make.height.equalTo(44)
            if Int(Constant.ScreenSize.SCREEN_HEIGHT) <= 568{
                let _ = make.top.equalTo(self.quickLoginButton.snp_bottom).offset(20)
            }
            else{
                let _ = make.top.equalTo(self.quickLoginButton.snp_bottom).offset(20)
            }
            
            
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func didTapLoginButton(_ sender: UIButton) {
        let isQuick = sender.tag > 0
        if self.delegate != nil {
            self.delegate!.didTapLoginButton(isQuick)
        }
    }
    
    func didTapClose() {
        if self.delegate != nil {
            self.delegate!.didTapClose()
        }
    }
    
    fileprivate func createTipLabel() -> UILabel {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = Constant.Theme.Color7
        label.font = Constant.Theme.Font_15
        return label
    }
    
}

class GiftReminderLoginTipView: UIView {
    
    fileprivate var _iconImage: UIImageView?
    fileprivate var _tipLabel: UILabel?
    
    fileprivate var imageName: String
    fileprivate var tipText: String
    
    var iconImage: UIImageView {
        get {
            if _iconImage == nil {
                _iconImage = UIImageView(image: UIImage(named: self.imageName))
            }
            return _iconImage!
        }
    }
    
    var tipLabel: UILabel {
        get {
            if _tipLabel == nil {
                _tipLabel = UILabel()
                _tipLabel!.text = self.tipText
                _tipLabel!.textColor = Constant.Theme.Color7
                _tipLabel!.font = Constant.Theme.Font_15
            }
            return _tipLabel!
        }
    }
    
    init(imageName: String, tipText: String) {
        self.imageName = imageName
        self.tipText = tipText
        super.init(frame: CGRect.zero)
        
        self.addSubview(self.iconImage)
        self.addSubview(self.tipLabel)
        
        self.iconImage.snp_makeConstraints { (make) -> Void in
            let _ = make.left.equalTo(self)
            let _ = make.centerY.equalTo(self)
            let _ = make.width.height.equalTo(20)
        }
        self.tipLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.left.equalTo(self.iconImage.snp_right).offset(10)
            let _ = make.centerY.equalTo(self)
            let _ = make.height.equalTo(Constant.Theme.Font_15.lineHeight)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
