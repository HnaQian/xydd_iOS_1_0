//
//  GiftReminderTableCell.swift
//  Limi
//
//  Created by 倪晅 on 16/1/10.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit



class GiftReminderTableCell: GiftReminderBaseTableCell {

    fileprivate var _avatarLabel: UILabel?
    fileprivate var _titleView: UIView?
    fileprivate var _titleLabel: YYLabel?
    fileprivate var _subTitleLabel: UILabel?
    fileprivate var _firstTag: UILabel?
    fileprivate var _secondTag: UILabel?
    fileprivate var _thirdTag: UILabel?
    fileprivate var _dateIntervalLabel: TTTAttributedLabel?
    fileprivate var _dateLabel: TTTAttributedLabel?
    fileprivate var _crownImage: UIImageView?
    
    fileprivate let markImage:UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFit
        image.image = UIImage(named: "gift_reminder_cell_markImage")
        return image
    }()
    
//    private let myData:JSON?
    
    //如果不需要左滑标为重要 设置为false
    var needSwipeDelete:Bool = true
    
    var itemID = 0
    
    var avatarLabel: UILabel {
        if _avatarLabel == nil {
            _avatarLabel = UILabel()
            _avatarLabel!.layer.cornerRadius = 28
            _avatarLabel!.layer.masksToBounds = true
            _avatarLabel!.backgroundColor = UIColor.blue
            _avatarLabel!.textColor = UIColor.white
            _avatarLabel!.textAlignment = .center
            _avatarLabel!.font = Constant.Theme.Font_23
//            _avatarLabel!.hidden = true
        }
        return _avatarLabel!
    }
    var titleText: NSMutableAttributedString {
        get {
            let title = "春节"
            let image = UIImage(named: "lunar_calendar")
            let text = NSMutableAttributedString()
            let font = Constant.Theme.Font_17
            text.append(NSAttributedString(string: title, attributes: nil))
            text.append(NSMutableAttributedString.yy_attachmentString(withContent: image, contentMode: .center, attachmentSize: image!.size, alignTo: font, alignment: .center))
            text.yy_font = font
            return text
        }
    }
    
    var subTitleLabel: UILabel {
        get {
            if _subTitleLabel == nil {
                _subTitleLabel = UILabel()
                _subTitleLabel!.text = "西方传统节日"
                _subTitleLabel!.font = Constant.Theme.Font_13
                _subTitleLabel!.textColor = UIColor(rgba: "#555555")
            }
            return _subTitleLabel!
        }
    }
    
    var firstTag: UILabel {
        get {
            if _firstTag == nil {
                _firstTag = UILabel()
                _firstTag!.backgroundColor = UIColor(rgba: "#FA9740")
                _firstTag!.textColor = UIColor.white
                _firstTag!.font = Constant.Theme.Font_11
                _firstTag!.layer.cornerRadius = 3
                _firstTag!.layer.masksToBounds = true
            }
            return _firstTag!
        }
    }
    
    var secondTag: UILabel {
        get {
            if _secondTag == nil {
                _secondTag = UILabel()
                _secondTag!.backgroundColor = UIColor(rgba: "#FF3A48")
                _secondTag!.textColor = UIColor.white
                _secondTag!.font = Constant.Theme.Font_11
                _secondTag!.layer.cornerRadius = 3
                _secondTag!.layer.masksToBounds = true
            }
            return _secondTag!
        }
    }
    
    var thirdTag: UILabel {
        get {
            if _thirdTag == nil {
                _thirdTag = UILabel()
                _thirdTag!.backgroundColor = UIColor(rgba: "#FF3A48")
                _thirdTag!.textColor = UIColor.white
                _thirdTag!.font = Constant.Theme.Font_11
                _thirdTag!.layer.cornerRadius = 3
                _thirdTag!.layer.masksToBounds = true
            }
            return _thirdTag!
        }
    }
    
    var dateIntervalLabel: TTTAttributedLabel {
        get {
            if _dateIntervalLabel == nil {
                _dateIntervalLabel = TTTAttributedLabel(frame: CGRect.zero)
                _dateIntervalLabel!.font = Constant.Theme.Font_17
                _dateIntervalLabel!.textColor = UIColor(rgba: "#12171F")
                _dateIntervalLabel!.textAlignment = .right
            }
            return _dateIntervalLabel!
        }
    }
    
    var dateLabel: TTTAttributedLabel {
        get {
            if _dateLabel == nil {
                _dateLabel = TTTAttributedLabel(frame: CGRect.zero)

                _dateLabel!.font = Constant.Theme.Font_13
                
                _dateLabel!.textColor = UIColor.lightGray
                _dateLabel!.textAlignment = .right
            }
            return _dateLabel!
        }
    }
    
    var titleLabel: YYLabel {
        get {
            if _titleLabel == nil {
                _titleLabel = YYLabel()
            }
            return _titleLabel!
        }
    }
    
    var titleView: UIView {
        get {
            if _titleView == nil {
                _titleView = UIView()
            }
            return _titleView!
        }
    }
    
    var crownImage: UIImageView {
        get {
            if _crownImage == nil {
                _crownImage = UIImageView(image: UIImage(named: "gift_reminder_self_crown"))
                _crownImage!.isHidden = false
            }
            return _crownImage!
        }
    }
    
    override var indexPath: IndexPath? {
        didSet {
            if (indexPath! as NSIndexPath).row % 2 == 0 {
                self.backView.backgroundColor = Constant.Theme.Color12
            } else {
                self.backView.backgroundColor = Constant.Theme.Color11
            }
        }
    }
    
    let markButton:UIButton = {
        let btn = UIButton(type: .custom)
        btn.backgroundColor = UIColor(rgba: Constant.common_C10_color)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: Constant.common_F5_font)
        btn.setTitle("标为取消", for: .selected)
        btn.setImage(UIImage(named: "gift_reminder_cell_mark_select"), for: .selected)
        btn.setTitle("标为重要", for: UIControlState())
        btn.setTitleColor(UIColor(rgba: Constant.common_C6_color), for: UIControlState())
        btn.setImage(UIImage(named:"gift_reminder_cell_mark"), for: UIControlState())
        btn.imageEdgeInsets = UIEdgeInsetsMake(-20, 18, 0, 0)
        btn.titleEdgeInsets = UIEdgeInsetsMake(30, -28, 0, 0)
        
        return btn
    }()
    
    override var viewModel: BaseTableCellViewModel? {
        didSet {
            let dict = viewModel!.model as! NSDictionary
            self.titleLabel.attributedText = self.createTitleText(dict)
            let situation = dict["Scene"] as! NSInteger
            let uid = dict["Uid"] as! NSInteger
            
            if uid == 0 && situation == AddGiftReminderSituation.birthday.rawValue {
                self.crownImage.isHidden = false
            } else {
                self.crownImage.isHidden = true
            }
            if situation == AddGiftReminderSituation.none.rawValue {
                self.subTitleLabel.text = dict["Holiday_slogan"] as? String
            } else {
                self.subTitleLabel.text = dict["Scene_name"] as? String
            }
            
            let data = JSON(dict as AnyObject)
            print(data)
            let sence = data["Scene"].int

            itemID = data["Id"].int!
            
            UIView.animate(withDuration: 0.3, animations: {
                self.backView.ix = 0
            })
            if data["Is_important"] == 1 {
                self.titleView.addSubview(self.markImage)
                
                self.markImage.snp_makeConstraints { (make) in
                    let _ = make.left.equalTo(self.titleLabel.snp_right).offset(16 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
                    let _ = make.centerY.equalTo(self.titleLabel)
                    let _ = make.width.equalTo(24 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
                    let _ = make.height.equalTo(24 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
                }
                
                self.markButton.isSelected = true
            }else{
                self.markImage.removeFromSuperview()
                self.markButton.isSelected = false
            }
            
            avatarImage.image = nil
            avatarLabel.text = nil
            avatarLabel.backgroundColor = UIColor.clear
            avatarImage.backgroundColor = UIColor.clear
            
            if sence == 1{
                
                if let imageName = data["Avatar"].string
                {
                    if imageName.characters.count > 0
                    {
                        let imageScale : String = imageName + "?imageView2/0/w/" + "\(Int(94*Constant.ScreenSize.SCALE_SCREEN))" + "/h/" + "\(Int(94*Constant.ScreenSize.SCALE_SCREEN))"
                        
                        self.avatarImage.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "loadingDefault"))
                    }
                    else
                    {
                        let name = Utils.subLastCharacter(data["Name"].string)
                        
                        if name != NULL_STRING{
                            
                            self.avatarLabel.text = name
                            
                            let index = (data["Id"].int)!%10
                            
                            let hex = Constant.randomColor[index]
                            
                            self.avatarLabel.backgroundColor = UIColor(rgba:hex)
                            
                        }else{
                            self.avatarImage.image = UIImage(named: "gift_reminder_situation_birthday_default")
                        }
                    }
                }
                else
                {
                    let name = Utils.subLastCharacter(data["Name"].string)
                    
                    if name != "null"{
                        
                        self.avatarLabel.text = name
                        
                        let index = (data["Id"].int)!%10
                        
                        let hex = Constant.randomColor[index]
                        
                        self.avatarLabel.backgroundColor = UIColor(rgba:hex)
                        
                    }else{
                        self.avatarImage.image = UIImage(named: "gift_reminder_situation_birthday_default")
                    }
                }
            }else
            {
                if let imageName = data["Avatar"].string
                {
                    let imageScale : String = imageName + "?imageView2/0/w/" + "\(Int(94*Constant.ScreenSize.SCALE_SCREEN))" + "/h/" + "\(Int(94*Constant.ScreenSize.SCALE_SCREEN))"
                    
                    self.avatarImage.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "loadingDefault"))
                }
                else
                {
                    self.avatarImage.image =  UIImage(named: "gift_reminder_situation_birthday_default")
                }
            }
            
            let tag = dict["Scene_tag"] as! String
            if tag == "" {
                self.firstTag.isHidden = true
                self.secondTag.isHidden = true
                self.thirdTag.isHidden = true
                self.subTitleLabel.snp_updateConstraints(closure: { (make) -> Void in
                    let _ = make.left.equalTo(self.avatarImage.snp_right).offset(10)
                    let _ = make.top.equalTo(self.titleLabel.snp_bottom).offset(8)
                })
            } else {
                let tagComponents = tag.components(separatedBy: ",")
                if tagComponents.count == 1{
                    self.firstTag.isHidden = false
                    self.secondTag.isHidden = true
                    self.thirdTag.isHidden = true
                    self.firstTag.text =  " " + tagComponents[0] + " "
                    let size1 : CGSize = self.firstTag.font.sizeOfString(self.firstTag.text!, constrainedToWidth: Double(Constant.ScreenSize.SCREEN_WIDTH - 140 - 86))
                    let size2 : CGSize = self.subTitleLabel.font.sizeOfString(self.subTitleLabel.text!, constrainedToWidth: Double(Constant.ScreenSize.SCREEN_WIDTH - 140 - 86))
                    if size1.width + size2.width > Constant.ScreenSize.SCREEN_WIDTH - 140 - 86{
                        self.subTitleLabel.snp_remakeConstraints(closure: { (make) -> Void in
                            let _ = make.right.lessThanOrEqualTo(self.titleView)
                            let _ = make.height.equalTo(16)
                            let _ = make.left.equalTo(self.avatarImage.snp_right).offset(10)
                            let _ = make.top.equalTo(self.titleLabel.snp_bottom).offset(26)
                        })
                    }
                    else{
                    self.subTitleLabel.snp_remakeConstraints(closure: { (make) -> Void in
                        let _ = make.top.equalTo(self.firstTag.snp_top)
                        let _ = make.left.equalTo(self.avatarImage.snp_right).offset(size1.width + 12)
                        let _ = make.right.lessThanOrEqualTo(self.titleView)
                        let _ = make.height.equalTo(16)
                    })
                    }
                } else if tagComponents.count == 2 {
                    self.firstTag.isHidden = false
                    self.secondTag.isHidden = false
                    self.thirdTag.isHidden = true
                    self.firstTag.text = " " + tagComponents[0] + " "
                    self.secondTag.text = " " + tagComponents[1] + " "
                    let size1 : CGSize = self.firstTag.font.sizeOfString(self.firstTag.text!, constrainedToWidth: Double(Constant.ScreenSize.SCREEN_WIDTH - 140 - 86))
                    let size2 : CGSize = self.secondTag.font.sizeOfString(self.secondTag.text!, constrainedToWidth: Double(Constant.ScreenSize.SCREEN_WIDTH - 140 - 86))
                    let size3 : CGSize = self.subTitleLabel.font.sizeOfString(self.subTitleLabel.text!, constrainedToWidth: Double(Constant.ScreenSize.SCREEN_WIDTH - 140 - 86))
                    if size1.width + size2.width + size3.width > Constant.ScreenSize.SCREEN_WIDTH - 140 - 86{
                        self.subTitleLabel.snp_remakeConstraints(closure: { (make) -> Void in
                            let _ = make.right.lessThanOrEqualTo(self.titleView)
                            let _ = make.height.equalTo(16)
                            let _ = make.left.equalTo(self.avatarImage.snp_right).offset(10)
                            let _ = make.top.equalTo(self.titleLabel.snp_bottom).offset(26)
                        })
                    }
                    else{
                        self.subTitleLabel.snp_remakeConstraints(closure: { (make) -> Void in
                            let _ = make.top.equalTo(self.firstTag.snp_top)
                            let _ = make.right.lessThanOrEqualTo(self.titleView)
                            let _ = make.height.equalTo(16)
                            let _ = make.left.equalTo(self.avatarImage.snp_right).offset(size1.width +  size2.width + 18)
                        })
                    }
                } else if tagComponents.count == 3{
                    self.firstTag.isHidden = false
                    self.secondTag.isHidden = false
                    self.thirdTag.isHidden = false
                    self.firstTag.text = " " + tagComponents[0] + " "
                    self.secondTag.text = " " + tagComponents[1] + " "
                    self.thirdTag.text = " " + tagComponents[2] + " "
                    let size1 : CGSize = self.firstTag.font.sizeOfString(self.firstTag.text!, constrainedToWidth: Double(Constant.ScreenSize.SCREEN_WIDTH - 140 - 86))
                    let size2 : CGSize = self.secondTag.font.sizeOfString(self.secondTag.text!, constrainedToWidth: Double(Constant.ScreenSize.SCREEN_WIDTH - 140 - 86))
                    let size3 : CGSize = self.thirdTag.font.sizeOfString(self.thirdTag.text!, constrainedToWidth: Double(Constant.ScreenSize.SCREEN_WIDTH - 140 - 86))
                    let size4 : CGSize = self.subTitleLabel.font.sizeOfString(self.subTitleLabel.text!, constrainedToWidth: Double(Constant.ScreenSize.SCREEN_WIDTH - 140 - 86))
                    if size1.width + size2.width + size3.width + size4.width > Constant.ScreenSize.SCREEN_WIDTH - 140 - 86{
                        self.subTitleLabel.snp_remakeConstraints(closure: { (make) -> Void in
                            let _ = make.right.lessThanOrEqualTo(self.titleView)
                            let _ = make.height.equalTo(16)
                            let _ = make.left.equalTo(self.avatarImage.snp_right).offset(10)
                            let _ = make.top.equalTo(self.titleLabel.snp_bottom).offset(26)
                        })
                    }
                    else{
                        self.subTitleLabel.snp_remakeConstraints(closure: { (make) -> Void in
                            let _ = make.top.equalTo(self.firstTag.snp_top)
                            let _ = make.right.lessThanOrEqualTo(self.titleView)
                            let _ = make.height.equalTo(16)
                            let _ = make.left.equalTo(self.avatarImage.snp_right).offset(size1.width +  size2.width +  size3.width + 18)
                        })
                    }

                }
            }
            
            let dateInterval = dict["Between_day"] as! Int
            var dateIntervalText = String(dateInterval) + "天后"
            if dateInterval == 0 {
                dateIntervalText = "今天"
            } else if dateInterval == 1 {
                dateIntervalText = "明天"
            }
            self.dateIntervalLabel.setText(dateIntervalText, afterInheritingLabelAttributesAndConfiguringWith: { (mutableAttributedString) -> NSMutableAttributedString! in
                var range: NSRange = NSString(string: mutableAttributedString!.string).range(of: String(dateInterval))
                if dateInterval < 2 {
                    range = NSString(string: (mutableAttributedString?.string)!).range(of: dateIntervalText)
                }
                mutableAttributedString?.addAttribute(kCTFontAttributeName as String, value: Constant.Theme.Font_25, range: range)
                if dateInterval <= 30 {
                    mutableAttributedString?.addAttribute(kCTForegroundColorAttributeName as String, value: UIColor(rgba: "#FF3A48"), range: range)
                }
                return mutableAttributedString
            })
            
            let (fullText, solarText) = self.createDateText(dict)
            self.dateLabel.setText(fullText) { (mutableAttributedString) -> NSMutableAttributedString! in
                let range: NSRange = NSString(string: mutableAttributedString!.string).range(of: solarText)
                mutableAttributedString?.addAttribute(kCTForegroundColorAttributeName as String, value: UIColor(rgba: "#555555"), range: range)
                return mutableAttributedString
            }
            
        }
    }

    required init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let swipeLeftGesture = UISwipeGestureRecognizer(target: self, action:#selector(GiftReminderTableCell.cellSwipeAction(_:)))
        swipeLeftGesture.direction = .left
        backView.addGestureRecognizer(swipeLeftGesture)
        
        let swipeRightGesture = UISwipeGestureRecognizer(target: self, action: #selector(GiftReminderTableCell.cellSwipeAction(_:)))
        swipeRightGesture.direction = .right
        backView.addGestureRecognizer(swipeRightGesture)
        self.backView.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: 100)
        
        backView.addSubview(self.avatarLabel)
        backView.addSubview(self.titleView)
        self.titleView.addSubview(self.titleLabel)
        

        self.titleView.addSubview(self.subTitleLabel)
        self.titleView.addSubview(self.firstTag)
        self.titleView.addSubview(self.secondTag)
        self.titleView.addSubview(self.thirdTag)
        backView.addSubview(self.dateIntervalLabel)
        backView.addSubview(self.dateLabel)
        backView.addSubview(self.crownImage)
        
        self.contentView.addSubview(markButton)
        self.contentView.sendSubview(toBack: markButton)
        markButton.addTarget(self, action: #selector(GiftReminderTableCell.markButtonClicked(_:)), for: .touchUpInside)
        
        self.avatarLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.edges.equalTo(self.avatarImage)
        }
        self.titleView.snp_makeConstraints { (make) -> Void in
            let _ = make.right.lessThanOrEqualTo(self.dateLabel.snp_left)
            let _ = make.left.equalTo(self.avatarImage.snp_right).offset(10)
            let _ = make.centerY.equalTo(self.backView)
            let _ = make.height.equalTo(Constant.Theme.Font_17.lineHeight + 16 + 5)
        }
        self.titleLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.titleView)
            let _ = make.left.equalTo(self.titleView)
            let _ = make.height.equalTo(Constant.Theme.Font_17.lineHeight)
            let _ = make.width.lessThanOrEqualTo(Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * 200)
        }
        
        
        self.firstTag.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.titleLabel.snp_bottom).offset(8)
            let _ = make.left.equalTo(self.titleView)
            let _ = make.height.equalTo(16)
        }
        self.secondTag.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.firstTag.snp_top)
            let _ = make.left.equalTo(self.firstTag.snp_right).offset(5)
            let _ = make.height.equalTo(16)
        }
        self.thirdTag.snp_makeConstraints { (make) -> Void in
            let _ = make.centerY.equalTo(self.firstTag)
            let _ = make.left.equalTo(self.secondTag.snp_right).offset(5)
            let _ = make.height.equalTo(16)
        }
        self.subTitleLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.titleLabel.snp_bottom).offset(8)
            let _ = make.left.equalTo(self.titleView).offset(135)
            let _ = make.right.lessThanOrEqualTo(self.titleView)
            let _ = make.height.equalTo(16)
        }
        self.dateIntervalLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.centerY.equalTo(self.titleLabel)
            let _ = make.right.equalTo(self.backView).offset(-20)
            let _ = make.left.equalTo(self.titleView.snp_right)
        }
        self.dateLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.centerY.equalTo(self.subTitleLabel)
            let _ = make.right.equalTo(self.backView).offset(-20)
            let _ = make.width.equalTo(190 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE).priorityHigh()
        }
        self.crownImage.snp_makeConstraints { (make) -> Void in
            let _ = make.left.equalTo(self.avatarImage.snp_centerX)
            let _ = make.top.equalTo(self.avatarImage).offset(-14)
            let _ = make.width.equalTo(29)
            let _ = make.height.equalTo(26)
        }
        
        self.markButton.snp_makeConstraints { (make) in
            let _ = make.right.equalTo(self.contentView)
            let _ = make.top.equalTo(self.contentView)
            let _ = make.bottom.equalTo(self.contentView)
            let _ = make.width.equalTo(140 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
        }
    }

    func cellSwipeAction(_ gesture:UISwipeGestureRecognizer) {
        if !needSwipeDelete {
            return
        }
        if gesture.direction == .left {
            UIView.animate(withDuration: 0.3, animations: {
                self.backView.ix = -140 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
            })
            (self.delegate as? BaseTableDataSourceDelegate)?.changeCellWhenOnClickImportantButton(self.indexPath!,isdelete: false,changedData:viewModel!)
        }else if gesture.direction == .right{
            //关闭
            UIView.animate(withDuration: 0.3, animations: {
                self.backView.ix = 0
            })
        }
    }
    
    func markButtonClicked(_ button:UIButton) {
        
        if UserInfoManager.didLogin {
            UIView.animate(withDuration: 0.3, animations: {
                self.backView.ix = 0
            })
            let dict = viewModel!.model as! NSDictionary
            var data = JSON(dict as AnyObject)
            if button.isSelected {
                button.isSelected = !button.isSelected
                
                data["Is_important"] = 2
                viewModel!.model = data.object
                self.markImage.removeFromSuperview()
                (self.delegate as? BaseTableDataSourceDelegate)?.changeCellWhenOnClickImportantButton(self.indexPath!, isdelete: true,changedData:viewModel!)
                //发送请求
                sendRequestToServer(2)
            }else{
                button.isSelected = !button.isSelected
                data["Is_important"] = 1
                viewModel!.model = data.object
                self.titleView.addSubview(self.markImage)
                
                self.markImage.snp_makeConstraints { (make) in
                    let _ = make.left.equalTo(self.titleLabel.snp_right).offset(16 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
                    let _ = make.centerY.equalTo(self.titleLabel)
                    let _ = make.width.equalTo(24 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
                    let _ = make.height.equalTo(24 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
                }
                
                (self.delegate as? BaseTableDataSourceDelegate)?.changeCellWhenOnClickImportantButton(self.indexPath!, isdelete: false,changedData:viewModel!)
                
                //发送请求
                sendRequestToServer(1)
            }

        }else {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "makeUserLogin"), object: nil)
        }
        
        
    }
    
    
    func sendRequestToServer(_ isImportant:Int){
        
        let params:[String:Int] = ["item_id":itemID, "is_important":isImportant]
        
        let _ = LMRequestManager.requestByDataAnalyses(context: self, URLString: Constant.JTAPI.mark_is_important, parameters: params as [String : AnyObject]?, isNeedUserTokrn: true, successHandler: { (data, status, msg) in
            
            if status == 200{
                print("标记为重要成功")
            }else{
                print("失败")
            }
            
            }) { 
                
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func createTitleText(_ dict: NSDictionary) -> NSMutableAttributedString {
        let title = dict["Name"] as! String
        let dateType = dict["Date_type"] as! Int
        let image = UIImage(named: "lunar_calendar")
        let text = NSMutableAttributedString()
        let font = Constant.Theme.Font_17
        text.append(NSAttributedString(string: title, attributes: nil))
        if dateType == 2 {
            text.yy_appendString(" ")
            text.append(NSMutableAttributedString.yy_attachmentString(withContent: image, contentMode: .center, attachmentSize: image!.size, alignTo: font, alignment: .center))
        }
        text.yy_font = font
        return text
    }
    
    fileprivate func createDateText(_ dict: NSDictionary) -> (String, String) {
        
        let dateTime = dict["Gift_time"] as! String
        let dateTimeComponents = dateTime.components(separatedBy: "-") as NSArray
        
        let month = dateTimeComponents[1] as! String
        let day = UInt(dateTimeComponents[2] as! String)
    
        let solarText = String(Int(month)!) + "月" + String(day!) + "日"

        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let endDate: Date? = formatter.date(from: dateTime)
        let time = formatter.string(from: endDate!)
        let chinesetime = CalendarUtil.alTolunar(time)
        let fullText = solarText + " / " + chinesetime!
        return (fullText, solarText)
    }
    
}
