//
//  GiftReminderBaseTableCell.swift
//  Limi
//
//  Created by 倪晅 on 16/1/10.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class GiftReminderBaseTableCell: BaseTableCell {

    fileprivate var _avatarImage: UIImageView?
    var backView = UIView()
    
    
    var avatarImage: UIImageView {
        get {
            if _avatarImage == nil {
                _avatarImage = UIImageView()
                _avatarImage!.layer.cornerRadius = 28
                _avatarImage!.layer.masksToBounds = true
            }
            return _avatarImage!
        }
    }

    required init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backView.backgroundColor = UIColor.white
        self.contentView.addSubview(backView)
        backView.addSubview(self.avatarImage)
        
        backView.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: 44)
//        backView.snp_makeConstraints { (make) in
//            make.left.equalTo(self.contentView)
//            make.top.equalTo(self.contentView)
//            make.right.equalTo(self.contentView)
//            make.bottom.equalTo(self.contentView)
//        }
        
        self.avatarImage.snp_makeConstraints { (make) -> Void in
            let _ = make.left.equalTo(backView).offset(20)
            let _ = make.centerY.equalTo(backView)
            let _ = make.width.height.equalTo(56)
        }
        
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.contentView.addBottomBorder(withHeight: 1, andColor: UIColor(rgba: "#F1F1F1"))
    }

}
