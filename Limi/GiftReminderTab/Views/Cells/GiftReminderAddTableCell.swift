//
//  GiftReminderAddTableCell.swift
//  Limi
//
//  Created by 倪晅 on 16/1/10.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

protocol GiftReminderAddTableCellDelegate {
    func didTapCellDeleteButton(withViewModel viewModel: BaseTableCellViewModel)
    func didTapCellAddButton(withViewModel viewModel: BaseTableCellViewModel)
}

class GiftReminderAddTableCell: GiftReminderBaseTableCell {
    
    fileprivate var _deleteButton: UIButton?
    fileprivate var _addButton: UIButton?
    fileprivate var _titleView: UIView?
    fileprivate var _titleLabel: UILabel?
    fileprivate var _subTitleLabel: UILabel?
    
    var realDelegate: GiftReminderAddTableCellDelegate?
    
    var deleteButton: UIButton {
        get {
            if _deleteButton == nil {
                _deleteButton = UIButton(type: .custom)
                _deleteButton!.setImage(UIImage(named: "gift_reminder_cell_delete1"), for: UIControlState())
                _deleteButton!.addTarget(self, action: #selector(GiftReminderAddTableCell.didTapDeleteButton), for: .touchUpInside)
            }
            return _deleteButton!
        }
    }
    
    var addButton: UIButton {
        get {
            if _addButton == nil {
                _addButton = UIButton(type: .custom)
                _addButton!.setTitle("去添加", for: UIControlState())
                _addButton!.backgroundColor = UIColor.white
                _addButton!.titleLabel!.font = UIFont.systemFont(ofSize: 15)
                _addButton!.layer.cornerRadius = 5
                _addButton!.layer.masksToBounds = true
                _addButton!.layer.borderWidth = 1
                _addButton!.layer.borderColor = UIColor(rgba: "#FF3A48").cgColor
                _addButton!.setTitleColor(UIColor(rgba: "#FF3A48"), for: UIControlState())
                _addButton!.addTarget(self, action: #selector(GiftReminderAddTableCell.didTapAddButton), for: .touchUpInside)
            }
            return _addButton!
        }
    }
    
    var titleView: UIView {
        get {
            if _titleView == nil {
                _titleView = UIView()
            }
            return _titleView!
        }
    }
    
    var titleLabel: UILabel {
        get {
            if _titleLabel == nil {
                _titleLabel = UILabel()
                _titleLabel!.font = Constant.Theme.Font_17
                _titleLabel!.textColor = Constant.Theme.Color2
            }
            return _titleLabel!
        }
    }
    
    var subTitleLabel: UILabel {
        get {
            if _subTitleLabel == nil {
                _subTitleLabel = UILabel()
                _subTitleLabel!.text = "生日还未添加哦"
                _subTitleLabel!.font = Constant.Theme.Font_13
                _subTitleLabel!.textColor = Constant.Theme.Color7
            }
            return _subTitleLabel!
        }
    }
    
    override var viewModel: BaseTableCellViewModel? {
        didSet {
            let dict = viewModel!.model as! NSDictionary
            let relation = dict["relation"] as! NSInteger
            if relation == AddGiftReminderRelation.father.rawValue {
                self.titleLabel.text = "爸爸"
                self.avatarImage.image = UIImage(named: "gift_reminder_situation_father")
                
            }
            if relation == AddGiftReminderRelation.mother.rawValue {
                self.titleLabel.text = "妈妈"
                self.avatarImage.image = UIImage(named: "gift_reminder_situation_mother")
            }
        }
    }
    
    
    override var indexPath: IndexPath? {
        didSet {
            if (indexPath! as NSIndexPath).row % 2 == 0 {
                self.contentView.backgroundColor = Constant.Theme.Color12
            } else {
                self.contentView.backgroundColor = Constant.Theme.Color11
            }
        }
    }
    
    override var delegate: BaseTableCellDelegate? {
        didSet {
            self.realDelegate = delegate as? GiftReminderAddTableCellDelegate
        }
    }
    
    required init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backView.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: 100)
        self.contentView.addSubview(self.titleView)
        self.titleView.addSubview(self.titleLabel)
        self.titleView.addSubview(self.subTitleLabel)
        self.contentView.addSubview(self.deleteButton)
        self.contentView.addSubview(self.addButton)
        
        self.titleView.snp_makeConstraints { (make) -> Void in
            let _ = make.centerY.equalTo(self.avatarImage)
            let _ = make.height.equalTo(Constant.Theme.Font_17.lineHeight + Constant.Theme.Font_13.lineHeight + 5)
            let _ = make.right.equalTo(self.addButton.snp_left)
            let _ = make.left.equalTo(self.avatarImage.snp_right).offset(10)
        }
        self.titleLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.titleView)
            let _ = make.left.equalTo(self.titleView)
            let _ = make.height.equalTo(Constant.Theme.Font_17.lineHeight)
        }
        self.subTitleLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.bottom.equalTo(self.titleView)
            let _ = make.left.equalTo(self.titleView)
            let _ = make.height.equalTo(Constant.Theme.Font_13.lineHeight)
        }
        self.deleteButton.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.contentView).offset(10)
            let _ = make.right.equalTo(self.contentView).offset(-5)
            let _ = make.width.equalTo(14)
            let _ = make.height.equalTo(14)
        }
        self.addButton.snp_makeConstraints { (make) -> Void in
            let _ = make.centerY.equalTo(self.contentView)
            let _ = make.right.equalTo(self.contentView).offset(-20)
            let _ = make.width.equalTo(76)
            let _ = make.height.equalTo(28)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func didTapAddButton() {
        if self.realDelegate != nil {
            self.realDelegate!.didTapCellAddButton(withViewModel: self.viewModel!)
        }
    }
    
    func didTapDeleteButton() {
        if self.realDelegate != nil {
            self.realDelegate!.didTapCellDeleteButton(withViewModel: self.viewModel!)
        }
    }

}
