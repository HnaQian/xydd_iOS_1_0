//
//  GiftReminderAddPopMenu.swift
//  Limi
//
//  Created by 倪晅 on 16/1/11.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

protocol GiftReminderAddPopMenuDelegate {
    func didTapMenuButton()
    func didTapAddButton()
    func didTapAddressButton()
}

class GiftReminderAddPopMenu: UIView {

    fileprivate var _menuButton: UIButton?
    fileprivate var _addButton: UIButton?
    fileprivate var _addressBookButton: UIButton?
    
    var isOpen: Bool = false
    
    var delegate: GiftReminderAddPopMenuDelegate?
    
    var menuButton: UIButton {
        get {
            if _menuButton == nil {
                _menuButton = UIButton(type: .custom)
                _menuButton!.setImage(UIImage(named: "gift_reminder_pop_menu"), for: UIControlState())
                _menuButton!.addTarget(self, action: #selector(GiftReminderAddPopMenu.didTapMenu), for: .touchUpInside)
            }
            return _menuButton!
        }
    }
    
    var addButton: UIButton {
        if _addButton == nil {
            _addButton = UIButton(type: .custom)
            _addButton!.setImage(UIImage(named: "gift_reminder_menu_add"), for: UIControlState())
            _addButton!.addTarget(self, action: #selector(GiftReminderAddPopMenu.didTapAdd), for: .touchUpInside)
        }
        return _addButton!
    }
    
    var addressBookButton: UIButton {
        if _addressBookButton == nil {
            _addressBookButton = UIButton(type: .custom)
            _addressBookButton!.setImage(UIImage(named: "gift_reminder_menu_address"), for: UIControlState())
            _addressBookButton!.addTarget(self, action: #selector(GiftReminderAddPopMenu.didTapAddress), for: .touchUpInside)
        }
        return _addressBookButton!
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(self.addButton)
        self.addSubview(self.addressBookButton)
        self.addSubview(self.menuButton)
        
        self.menuButton.snp_makeConstraints { (make) -> Void in
            let _ = make.top.left.equalTo(self)
//            make.edges.equalTo(self)
            let _ = make.width.height.equalTo(52)
        }
        self.addButton.snp_makeConstraints { (make) -> Void in
            let _ = make.top.left.equalTo(self.menuButton)
//            make.edges.equalTo(self.menuButton)
            let _ = make.width.height.equalTo(52)
        }
        self.addressBookButton.snp_makeConstraints { (make) -> Void in
            let _ = make.top.left.equalTo(menuButton)
//            make.edges.equalTo(self.menuButton)
            let _ = make.width.height.equalTo(52)
        }
    }
    
    func open() {
        self.isOpen = true
        self.setNeedsUpdateConstraints()
        self.updateConstraintsIfNeeded()
        UIView.animate(withDuration: 0.3,
            animations: { () -> Void in
                self.addressBookButton.snp_updateConstraints(closure: { (make) -> Void in
                    let _ = make.top.equalTo(self.menuButton).offset(-40)
                })
                self.addButton.snp_updateConstraints(closure: { (make) -> Void in
                    let _ = make.top.equalTo(self.menuButton).offset(-80)
                })
                self.menuButton.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI_4))
                self.layoutIfNeeded()
            }, completion: { (finished) -> Void in
        }) 
    }
    
    func close() {
        self.isOpen = false
        self.setNeedsUpdateConstraints()
        self.updateConstraintsIfNeeded()
        UIView.animate(withDuration: 0.3,
            animations: { () -> Void in
                self.addButton.snp_updateConstraints { (make) -> Void in
                    let _ = make.top.equalTo(self.menuButton)
                }
                self.addressBookButton.snp_updateConstraints { (make) -> Void in
                    let _ = make.top.equalTo(self.menuButton)
                }
                self.menuButton.transform = CGAffineTransform(rotationAngle: 0)
                self.layoutIfNeeded()
            }, completion: { (finished) -> Void in
        }) 
    }
    
    func didTapAdd() {
        if self.delegate != nil {
            self.close()
            self.delegate!.didTapAddButton()
        }
    }
    
    func didTapAddress() {
        if self.delegate != nil {
            self.close()
            self.delegate!.didTapAddressButton()
        }
    }
    
    func didTapMenu() {
        self.isOpen = !self.isOpen
        if self.delegate != nil {
            self.delegate!.didTapMenuButton()
        }
        self.setNeedsUpdateConstraints()
        self.updateConstraintsIfNeeded()
        UIView.animate(withDuration: 0.3,
            animations: { () -> Void in
                if self.isOpen {
                    self.addressBookButton.snp_updateConstraints(closure: { (make) -> Void in
                        let _ = make.top.equalTo(self.menuButton).offset(-92)
                    })
                    self.addButton.snp_updateConstraints(closure: { (make) -> Void in
                        
                        let _ = make.top.equalTo(self.menuButton).offset(-184)
                    })
                    self.menuButton.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI_4))
                } else {
                    self.addButton.snp_updateConstraints { (make) -> Void in
                        let _ = make.top.equalTo(self.menuButton)
                    }
                    self.addressBookButton.snp_updateConstraints { (make) -> Void in
                        let _ = make.top.equalTo(self.menuButton)
                    }
                    self.menuButton.transform = CGAffineTransform(rotationAngle: 0)
                }
                self.layoutIfNeeded()
            }, completion: { (finished) -> Void in
        }) 
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        if (self.menuButton.frame.contains(point)) {
            return self.menuButton
        }
        if (self.addressBookButton.frame.contains(point)) {
            return self.addressBookButton
        }
        if (self.addButton.frame.contains(point)) {
            return self.addButton
        }
        return super.hitTest(point, with: event);
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
