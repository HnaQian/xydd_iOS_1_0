//
//  AddSuccessRemindViewController.swift
//  Limi
//
//  Created by 千云锋 on 16/8/15.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
// 导入完成

import UIKit

class AddSuccessRemindViewController: BaseViewController {

    fileprivate let successImageView:UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named:"gift_reminder_success")
        return iv
    }()
    
    //恭喜！
    fileprivate let successLabel:UILabel = {
        let lbl = UILabel()
        lbl.text = "恭喜"
        lbl.font = UIFont.systemFont(ofSize: Constant.common_F1_font)
        lbl.textColor = Constant.Theme.Color14
        return lbl
    }()
    
    fileprivate let remindCount = "176"
    
    fileprivate let remindCountLabel:UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        lbl.textColor = Constant.Theme.Color14
        return lbl
    }()
    
    fileprivate let remindSuccessLabel:UILabel = {
        let lbl = UILabel()
        lbl.text = "点心将及时提醒您维护好核心人脉。"
        lbl.font = UIFont.systemFont(ofSize: Constant.common_F5_font)
        lbl.textColor = Constant.Theme.Color7
        return lbl
    }()
    
    fileprivate let successButton:UIButton = {
        let btn = UIButton()
        btn.setTitle("进入送礼提醒", for: UIControlState())
        btn.titleLabel?.font = UIFont.systemFont(ofSize: Constant.common_F2_font)
        btn.setTitleColor(Constant.Theme.Color12, for: UIControlState())
        btn.backgroundColor = Constant.Theme.Color1
        btn.layer.cornerRadius = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * 10
        return btn
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "导入完成"
        self.view.backgroundColor = Constant.Theme.Color5
        
        setupUI()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupUI() {
        
        remindCountLabel.text = "您已成功导入\(remindCount)条送礼提醒，"
        let attributedString = NSMutableAttributedString(string: remindCountLabel.text! as String)
        attributedString.addAttribute(NSForegroundColorAttributeName, value: Constant.Theme.Color1, range: NSRange(location: 6, length: remindCount.characters.count))
        remindCountLabel.attributedText = attributedString
        
        let _ = [successImageView, successLabel, remindCountLabel, remindSuccessLabel, successButton].map{self.view.addSubview($0)}
        
        successImageView.snp_makeConstraints { (make) in
            let _ = make.centerX.equalTo(self.view)
            let _ = make.top.equalTo(self.view).offset(Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 200)
            let _ = make.width.equalTo(Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 140)
            let _ = make.height.equalTo(Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 140)
        }
        
        successLabel.snp_makeConstraints { (make) in
            let _ = make.centerX.equalTo(self.view)
            let _ = make.top.equalTo(successImageView.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_30)
        }
        
        remindCountLabel.snp_makeConstraints { (make) in
            let _ = make.centerX.equalTo(self.view)
            let _ = make.top.equalTo(successLabel.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_30)
        }
        
        remindSuccessLabel.snp_makeConstraints { (make) in
            let _ = make.centerX.equalTo(self.view)
            let _ = make.top.equalTo(remindCountLabel.snp_bottom).offset(Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 80)
        }
        
        successButton.snp_makeConstraints { (make) in
            let _ = make.centerX.equalTo(self.view)
            let _ = make.top.equalTo(remindSuccessLabel.snp_bottom).offset(Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 230)
            let _ = make.width.equalTo(Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 320)
            let _ = make.height.equalTo(Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 80)
        }
    }
    
    func addBirthday(_ button:UIButton) {
        let vc = SetupRemindTimeViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
