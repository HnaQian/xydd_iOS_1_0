//
//  AllRemindViewController.swift
//  Limi
//
//  Created by ZhiQiangMi on 16/4/11.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class AllRemindViewController: BaseTableViewController {
    
    fileprivate var _parentChooseView: GiftReminderParentChooseView?
    fileprivate var _emptyListView: GiftReminderEmptyListView?
    fileprivate var _genderChooseView: GiftReminderGenderChooseView?
    fileprivate var isDisappeared: Bool = false
    fileprivate var isNotCanDisappearMenu: Bool = false
    fileprivate var isFirstGetBirthNumber: Bool = false
    fileprivate var viewModel : BaseTableCellViewModel?
    fileprivate var isLogin: Bool = UserInfoManager.didLogin
    fileprivate let guideAuthvc = GuideAuthContactViewController()
    var navigationVC:UINavigationController?
    override var navigationController: UINavigationController? { return navigationVC}
    
    fileprivate let loginDialog = LoginDialog()
    
    var genderChooseView: GiftReminderGenderChooseView {
        get {
            if _genderChooseView == nil {
                _genderChooseView = GiftReminderGenderChooseView(frame: CGRect(x: 0, y: 0, width: 354 * Constant.ScreenSize.SCREEN_WIDTH_SCALE, height: 294));
                _genderChooseView!.delegate = self
            }
            return _genderChooseView!
        }
    }

    var parentChooseView: GiftReminderParentChooseView {
        get {
            if _parentChooseView == nil {
                _parentChooseView = GiftReminderParentChooseView(frame: CGRect(x: 0, y: 0, width: 354 * Constant.ScreenSize.SCREEN_WIDTH_SCALE, height: 352));
                _parentChooseView!.delegate = self
            }
            return _parentChooseView!
        }
    }
    

    
    var emptyListView: GiftReminderEmptyListView {
        get {
            if _emptyListView == nil {
                _emptyListView = GiftReminderEmptyListView()
                _emptyListView!.isHidden = true
            }
            return _emptyListView!
        }
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
        
        self.useLoadMoreControl = true
        
        self.dataSource = GiftReminderDataSource(delegate: self, viewModelClass: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewType = .All
        self.gesturBackEnable = false
        self.tableView.backgroundColor = UIColor(rgba:Constant.common_background_color)
        
        self.tableView.addSubview(self.emptyListView)
        self.emptyListView.snp_makeConstraints { (make) -> Void in
            let _ = make.centerX.equalTo(self.tableView)
            let _ = make.centerY.equalTo(self.tableView)
        }
        
        self.tableView.register(GiftReminderAddTableCell.self, forCellReuseIdentifier: NSStringFromClass(GiftReminderAddTableCell.self))
        self.tableView.register(GiftReminderTableCell.self, forCellReuseIdentifier: NSStringFromClass(GiftReminderTableCell.self))
        
//        let dataSource = self.dataSource as! GiftReminderDataSource
        self.dataSource?.type = 0
        self.pullRefresh()
        
        let listDataSource = self.dataSource as! GiftReminderDataSource
        var isFirstTap = AccountManager.shared.isFirstTapReminderBar()
        if UserInfoManager.didLogin == false{
            if isFirstTap {
                self.isNotCanDisappearMenu = true
                loginDialog.controller = self
                loginDialog.show()
                NotificationCenter.default.post(name: Notification.Name(rawValue: "gift_remind_open_login_menu"), object: nil)
            }
        }
        let _ = listDataSource.birthdayCount.afterChange += { (_, newValue) -> () in
            if AccountManager.shared.isLogin() {
                print(listDataSource.birthdayCount)
                if newValue <= 0 {
                    if isFirstTap &&  self.isFirstGetBirthNumber == false{
                        isFirstTap = false
                        self.presentAuthContact()
                    } else {
                        let firstLaunchTime = UserDefaults.standard.object(forKey: "firstLaunchTime") as! Date
                        if (Date() as NSDate).days(from: firstLaunchTime) > 30 && listDataSource.type == 0 && listDataSource.lastTime == nil && UserDefaults.standard.bool(forKey: "parentChooseViewIsAppear") == false{
                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(5 * Int64(NSEC_PER_SEC)) / Double(NSEC_PER_SEC),
                                           execute: { () -> Void in
                                            if !self.isDisappeared {
                                                self.ma_presentSemiView(self.parentChooseView)
                                                UserDefaults.standard.set(true, forKey: "parentChooseViewIsAppear")
                                                UserDefaults.standard.synchronize()
                                            }
                            })
                        }
                        
                    }
                }
            }
            self.isFirstGetBirthNumber = true
        }

        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: Constant.notification_login_success), object: nil, queue: OperationQueue.main) { (notification) -> Void in
            self.pullRefresh()
        }
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: Constant.notification_logout_success), object: nil, queue: OperationQueue.main) { (notification) -> Void in
            self.pullRefresh()
        }
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: Constant.notification_import_contact_success), object: nil, queue: OperationQueue.main) { (notification) -> Void in
            self.pullRefresh()
        }
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: Constant.notification_add_gift_reminder_success), object: nil, queue: OperationQueue.main) { (notification) -> Void in
            self.pullRefresh()
        }
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "Request"), object: nil, queue: OperationQueue.main) { (notification) -> Void in
            self.pullRefresh()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.isDisappeared = false
        
        if self.isLogin != UserInfoManager.didLogin{
            self.pullRefresh()
            self.isLogin = UserInfoManager.didLogin
            
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.isNotCanDisappearMenu = false
        self.isDisappeared = true
    }
    
    
    
    func presentAuthContact() {
        let animation = CATransition()
        animation.duration = 0.3
        animation.type = kCATransitionMoveIn
        animation.subtype = kCATransitionFromTop
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        self.navigationVC?.view.layer.add(animation, forKey: "")
        guideAuthvc.hidesBottomBarWhenPushed = false
        self.navigationVC?.pushViewController(guideAuthvc, animated: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let viewModel = self.dataSource!.dataAtIndexPath(indexPath)
        let dict = viewModel!.model as! NSDictionary
        if let url : String = dict.object(forKey: "Url") as? String
        {
            if url != ""{
                let webVC = WebViewController(URL: URL(string: url)!)
                if let Uid : Int = dict.object(forKey: "Uid") as? Int{
                    if Uid != 0{
                        if let scene : Int = dict.object(forKey: "Scene") as? Int{
                            if scene == 1{
                                webVC.isHaveBirthdayWiki = true
                            }
                            else{
                                webVC.isHaveBirthdayWiki = false
                            }
                            
                            if let id : Int = dict.object(forKey: "Id") as? Int{
                                webVC.RemindDetailId = id
                            }

                            webVC.type = Constant.WebViewType.remindDetailType.rawValue
                        }
                    }
                }
                
                if let scene : Int = dict.object(forKey: "Scene") as? Int,
                    let id : Int = dict.object(forKey: "Id") as? Int{
                    if scene != 1{
                        Statistics.count(Statistics.GiftRemind.remind_holiday_click, andAttributes: ["remindId":"\(id)"])
                    }
                }
                
                webVC.hidesBottomBarWhenPushed = true
                webVC.shareType = Constant.ShareContentType.imageType.rawValue
                self.navigationVC?.pushViewController(webVC, animated: true)
                
            }
        }
    }
    
    
    override func dataSource(_ dataSource: BaseTableDataSource, didFinishRefreshWithError error: NSError?) {
        super.dataSource(dataSource, didFinishRefreshWithError: error)
        
        self.emptyListView.isHidden = dataSource.data.count > 0
    }
}


extension AllRemindViewController: GiftReminderParentChooseViewDelegate {
    
    func didTapParentChoose(_ relation: Int) {
        self.ma_dismissSemiView()

        let vm = AddGiftReminderViewModel()
        vm.situation <- AddGiftReminderSituation.birthday.rawValue
        vm.relation <- relation
        let vc = AddGiftReminderViewController(withReminderViewModel: vm)
        self.navigationVC?.pushViewController(vc, animated: true)
    }
    
    func didTapParentImport() {
        self.ma_dismissSemiView()
        self.didAuthAddressBook()
    }
    
}

extension AllRemindViewController: GiftReminderAddTableCellDelegate {
    
    func didTapCellAddButton(withViewModel viewModel: BaseTableCellViewModel) {
        if AccountManager.shared.isLogin() {
            if AccountManager.shared.hasGender() {
                let vm = AddGiftReminderViewModel()
                let model = viewModel.model as! NSDictionary
                vm.situation <- (model["situation"] as! NSInteger)
                vm.relation <- (model["relation"] as! NSInteger)
                
                Statistics.count((model["relation"] as! Int) == 1 ? Statistics.GiftRemind.remind_father_click : Statistics.GiftRemind.remind_monther_click)
                
                let vc = AddGiftReminderViewController(withReminderViewModel: vm)
                self.navigationVC?.pushViewController(vc, animated: true)
            } else {
                self.viewModel = viewModel
                self.genderChooseView._purposeType = 0
                self.ma_presentSemiView(self.genderChooseView)
            }
        } else {
            loginDialog.controller = self
            loginDialog.show()
        }
    }
    
    func didTapCellDeleteButton(withViewModel viewModel: BaseTableCellViewModel) {
        let dict = viewModel.model as! NSDictionary
        let relation = dict["relation"] as! NSInteger
        if relation == AddGiftReminderRelation.father.rawValue{
            UserDefaults.standard.set(true, forKey: "FatherIsHadDelete")
            UserDefaults.standard.synchronize()
            AccountManager.shared.setDadAdd(false)
        }
        if relation == AddGiftReminderRelation.mother.rawValue{
            UserDefaults.standard.set(true, forKey: "MotherIsHadDelete")
            UserDefaults.standard.synchronize()
            AccountManager.shared.setMontherAdd(false)
        }
        self.dataSource!.removeData(viewModel)
        self.tableView.reloadData()
    }
    
}

extension AllRemindViewController: GiftReminderGenderChooseViewDelegate {
    
    func didTapGenderChoose(_ gender: Int,purposeType:Int) {
       let _ =  LMRequestManager.requestByDataAnalyses(context:UIApplication.shared.keyWindow!,URLString: Constant.JTAPI.user_gender, parameters: ["gender": gender as AnyObject], isNeedUserTokrn: true, isShowErrorStatuMsg: true, isNeedHud: true, successHandler: {data, status, msg in
            if status == 200 {
                AccountManager.shared.setGender(Int64(gender), successHandler: { () -> Void in
                    
                    self.ma_dismissSemiView()
                    if purposeType == 0{
                        if self.viewModel != nil{
                            let vm = AddGiftReminderViewModel()
                            let model = self.viewModel!.model as! NSDictionary
                            vm.situation <- (model["situation"] as! NSInteger)
                            vm.relation <- (model["relation"] as! NSInteger)
                            let vc = AddGiftReminderViewController(withReminderViewModel: vm)
                            self.navigationVC?.pushViewController(vc, animated: true)
                        }
                        else{
                            let vc = AddGiftReminderViewController()
                            self.navigationVC?.pushViewController(vc, animated: true)
                        }
                    }
                    else if purposeType == 1{
                        self.didAuthAddressBook()
                    }
                    
                    }, failureHandler: { () -> Void in
                })
            }
        })
    }
    
    func didTapClose() {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "gift_remind_close_login_menu"), object: nil)
        self.ma_dismissSemiView()
    }
}


//extension AllRemindViewController: GiftReminderLoginViewDelegate {
//    func didTapLoginButton(_ isQuick: Bool) {
//        self.ma_dismissSemiView()
//        
//        loginDialog.controller = self
//        loginDialog.show()
//    }
//}


