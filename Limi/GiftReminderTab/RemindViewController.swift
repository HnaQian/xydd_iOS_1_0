//
//  RemindViewController.swift
//  Limi
//
//  Created by ZhiQiangMi on 16/4/11.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit


//0不刷新   1刷新
var isChangeOfImpor = 0
var isChangeOfAll = 0
var isChangeOfBirthday = 0
var isChangeOfHoliday = 0
var isChangeOfMemorial = 0

private let PI:CGFloat = 3.1415926
class RemindViewController: BaseViewController,PagingMenuControllerDelegate {
    
    fileprivate var menuContentView = UIView()
    fileprivate var frostedView = UIView()
    fileprivate var menuBtnAry = [GiftRemindMenuButton]()
    fileprivate var _genderChooseView: GiftReminderGenderChooseView?
    fileprivate var viewModel : BaseTableCellViewModel?
    
    fileprivate var _rightItemsView:NavigationItemMenu? //导航栏右边按钮
    fileprivate let loginDialog = LoginDialog()
    
    var genderChooseView: GiftReminderGenderChooseView {
        get {
            if _genderChooseView == nil {
                _genderChooseView = GiftReminderGenderChooseView(frame: CGRect(x: 0, y: 0, width: 354 * Constant.ScreenSize.SCREEN_WIDTH_SCALE, height: 294));
                _genderChooseView!.delegate = self
            }
            return _genderChooseView!
        }
    }

    fileprivate let findButton:UIButton = {
    
        let btn = UIButton()
        btn.setImage(UIImage(named:"gift_reminder_find"), for: UIControlState())
        btn.setImage(UIImage(named:"gift_reminder_find_red"), for: UIControlState.selected)
        btn.frame = CGRect(x: 0, y: 0, width: 36, height: 36)
        
        return btn
    }()
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.menuContentView.isHidden = false
    }

    func heartBtnClick(_ notification:Notification){

        findButton.isSelected = true

    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "送礼提醒"
        self.gesturBackEnable = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(RemindViewController.heartBtnClick(_:)), name: NSNotification.Name(rawValue: "discoverAddressBookChange"), object: nil)
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "gift_remind_close_login_menu"), object: nil, queue: OperationQueue.main) { (notification) -> Void in
            self.menuContentView.isHidden = false
        }
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "gift_remind_open_login_menu"), object: nil, queue: OperationQueue.main) { (notification) -> Void in
            self.menuContentView.isHidden = true
        }
        

        self.findButton.addTarget(self, action: #selector(RemindViewController.findButtonOnClick(_:)), for: UIControlEvents.touchUpInside)
        //todo此版本暂不开发
//        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: self.findButton)
        
        let allRemind = AllRemindViewController()
        allRemind.navigationVC = self.navigationController
        allRemind.title = "最近"
        let importantRemind = ImportantRenindViewController()
        importantRemind.navigationVC = self.navigationController
        importantRemind.title = "重要"
        let birthdayRemind = BirthdayRemindViewController()
        birthdayRemind.navigationVC = self.navigationController
        birthdayRemind.title = "生日"
        let holidayRemind = HolidayRemindViewController()
        holidayRemind.navigationVC = self.navigationController
        holidayRemind.title = "节日"
        let memorialDayRemind = MemorialDayRemindViewController()
        memorialDayRemind.navigationVC = self.navigationController
        memorialDayRemind.title = "纪念日"
        let viewControllers = [allRemind,importantRemind,birthdayRemind,holidayRemind,memorialDayRemind]
        
        let options = PagingMenuOptions()
        options.menuHeight = 44
        options.scrollEnabled = false
        
        if Constant.ScreenSize.SCREEN_WIDTH >= 414 {
            options.menuDisplayMode = .standard(widthMode: .fixed(width: 42), centerItem: false, scrollingMode: .pagingEnabled)
            
        }else{
            options.menuDisplayMode = .standard(widthMode: .fixed(width: Constant.ScreenSize.SCREEN_WIDTH >= 375 ? 34 : 23.5), centerItem: false, scrollingMode: .pagingEnabled)
        }
        
        let pagingMenuController = PagingMenuController(viewControllers: viewControllers, options: options)
        pagingMenuController.delegate = self
        self.addChildViewController(pagingMenuController)
        self.view.addSubview(pagingMenuController.view)
        pagingMenuController.didMove(toParentViewController: self)
        
        
        self.view.addSubview(menuContentView)
        menuContentView.snp_makeConstraints { (make) -> Void in
            let _ = make.bottom.equalTo(self.view).offset(-MARGIN_49-MARGIN_10 * 2)
            let _ = make.right.equalTo(self.view).offset(-MARGIN_13)
            let _ = make.height.equalTo(72)
            let _ = make.width.equalTo(70)
        }
        
        let titles = ["导入提醒","新增提醒",""]
        let imageNames = ["gift_reminder_menu_address","gift_reminder_menu_add","gift_reminder_pop_menu"]
        
        for index in 0...2{
            
            let actionBtn = GiftRemindMenuButton()
            menuContentView.addSubview(actionBtn)
            
            actionBtn.title = titles[index]
            actionBtn.imageName = imageNames[index]
            actionBtn.tag = 100 + index
            if index != 2{
                actionBtn.alpha = 0
            }else{
                actionBtn.layer.anchorPoint = CGPoint(x: 0.5,y: 0.35)
            }
            actionBtn.snp_makeConstraints(closure: { (make) -> Void in
                let _ = make.bottom.equalTo(menuContentView)
                let _ = make.right.equalTo(menuContentView)
                let _ = make.height.equalTo(actionBtn.iheight)
                let _ = make.width.equalTo(actionBtn.iwidth)
            })
            
            actionBtn.addTarget(self, action: #selector(RemindViewController.menuBtnOnClick(_:)))
            
            menuBtnAry.append(actionBtn)
        }
        
        //ExtraLight Dark
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        // 毛玻璃视图
        let effectView = UIVisualEffectView(effect: blurEffect)
        effectView.frame = (self.navigationController?.tabBarController?.view.bounds)!
        effectView.alpha = 0.9
        
        frostedView.frame = (UIApplication.shared.keyWindow?.bounds)!
        frostedView.addSubview(effectView)
        frostedView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(RemindViewController.frostedViewTapGesAction)))
        
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "makeUserLogin"), object: nil, queue: OperationQueue.main) { (notification) -> Void in
//            self.ma_presentSemiView(self.loginView)
            self.jumpToLogin(Constant.InLoginType.defaultType, tipe: nil)
        }
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: Constant.notification_add_gift_reminder_success), object: nil, queue: OperationQueue.main) { (notification) -> Void in
            
            DispatchQueue.main.async(execute: { () -> Void in
                if let dict = notification.object as? NSDictionary {
                    if let url : String = dict.object(forKey: "Url") as? String
                    {
                        if url != ""{
                            let webVC = WebViewController(URL: URL(string: url)!)
                            if let scene : Int = dict.object(forKey: "Scene") as? Int{
                                if scene == 1{
                                    webVC.isHaveBirthdayWiki = true
                                }
                                else{
                                    webVC.isHaveBirthdayWiki = false
                                }
                                if let id : Int = dict.object(forKey: "Id") as? Int{
                                    webVC.RemindDetailId = id
                                }
                                webVC.type = Constant.WebViewType.remindDetailType.rawValue
                            }
                            webVC.hidesBottomBarWhenPushed = true
                            webVC.shareType = Constant.ShareContentType.imageType.rawValue

                            self.navigationController?.pushViewController(webVC, animated: true)
                            
                        }
                    }
                }
            })
        }
       
    }
    
    func findButtonOnClick(_ button:UIButton) {
        
        if AccountManager.shared.isLogin() {
            if !AccountManager.shared.hasGender() {
                self.genderChooseView._purposeType = 1
                self.ma_presentSemiView(self.genderChooseView)
            }
            else {
                isAuthingAddressBook = true
                let authStatus: ABAuthorizationStatus = ABAddressBookGetAuthorizationStatus()
                switch authStatus {
                case .denied, .restricted:
                    self.openAlertWhenNotAuthorized()
                    isAuthingAddressBook = false
                case .authorized://已经授权
                    self.findButton.isSelected = false
                    let vc = FindNowBirthdayRemindViewController()
                    vc.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(vc, animated: true)
                default:break
                }
            }
        }else {
            self.loginDialog.controller = self
            self.loginDialog.show()
        }
       
    }
    
    @objc func menuBtnOnClick(_ menuBtn:GiftRemindMenuButton){
        
        switch menuBtn.tag{
        case 100:print("导入提醒")
        
        Statistics.count(Statistics.GiftRemind.remind_import_click)
        if AccountManager.shared.isLogin() {
            if !AccountManager.shared.hasGender() {
                self.genderChooseView._purposeType = 1
                self.ma_presentSemiView(self.genderChooseView)
            }
            else {
                self.didAuthAddressBook()
            }
        }else {
            self.loginDialog.controller = self
            self.loginDialog.show()
        }
        case 101:print("新增提醒")
        
        Statistics.count(Statistics.GiftRemind.remind_add_click)
        if AccountManager.shared.isLogin() {
            if !AccountManager.shared.hasGender() {
                self.genderChooseView._purposeType = 0
                self.ma_presentSemiView(self.genderChooseView)
            } else {
                self.navigationController?.pushViewController(AddGiftReminderViewController(), animated: true)
            }
        }else {
            self.loginDialog.controller = self
            self.loginDialog.show()
        }
            
            
        case 102:print("展开menu")
            
        default :break
        }
        self.showMenuBtn()
    }
    
    fileprivate var didSpread = false
    
    @objc func frostedViewTapGesAction(){
        didSpread = true
        showMenuBtn()
    }
    
    func showMenuBtn(){
        
        Statistics.count(Statistics.GiftRemind.remind_new_click)
        
        if didSpread == false{
            
            if  let window = UIApplication.shared.keyWindow{
                
                frostedView.removeFromSuperview()
                menuContentView.removeFromSuperview()
                
                window.addSubview(frostedView)
                menuContentView.frame = window.bounds
                
                window.addSubview(menuContentView)
                menuContentView.snp_remakeConstraints { (make) -> Void in
                    let _ = make.bottom.equalTo(window).offset(-MARGIN_49-MARGIN_10 * 2)
                    let _ = make.right.equalTo(window).offset(-MARGIN_13)
                    let _ = make.height.equalTo(72 * 4)
                    let _ = make.width.equalTo(70)
                }
            }
            
            UIView.animate(withDuration: 0.25, animations: { () -> Void in
                
                for actionBtn in self.menuBtnAry{
                    if actionBtn.tag != 102
                    {
                        actionBtn.transform = CGAffineTransform(translationX: 0, y: -CGFloat(actionBtn.tag - 99) * 90)
                        actionBtn.alpha = 1
                    }else{
                        actionBtn.transform = CGAffineTransform(rotationAngle: PI/4)
                    }
                }
                
                }, completion: { (over) -> Void in
                    
            })
            
        }else{
            
            frostedView.removeFromSuperview()
            menuContentView.removeFromSuperview()
            
            self.view.addSubview(menuContentView)
            menuContentView.snp_remakeConstraints { (make) -> Void in
                let _ = make.bottom.equalTo(self.view).offset(-MARGIN_49-MARGIN_10 * 2)
                let _ = make.right.equalTo(self.view).offset(-MARGIN_13)
                let _ = make.height.equalTo(72)
                let _ = make.width.equalTo(70)
            }
            
            
            UIView.animate(withDuration: 0.25, animations: { () -> Void in
                
                for actionBtn in self.menuBtnAry{
                    if actionBtn.tag != 102
                    {
                        actionBtn.transform = CGAffineTransform(translationX: 0, y: 0)
                        actionBtn.alpha = 0
                    }else{
                        actionBtn.transform = CGAffineTransform(rotationAngle: 0)
                    }
                }
                
                }, completion: { (over) -> Void in
                    
            })
            
        }
        
        didSpread = !didSpread
    }
    
    
    class GiftRemindMenuButton:UIView {
        
        var title:String?{didSet{titleLbl.text = title}}
        
        var imageName:String?{didSet{
            if let name = imageName{
                actionBtn.setImage(UIImage(named: name), for: UIControlState())
            }
            }
        }
        
        fileprivate let titleLbl = UILabel()
        
        fileprivate let actionBtn = UIButton()
        
        init(){
            super.init(frame: CGRect(x: 0, y: 0, width: 70, height: 72))
            self.setupUI()
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        fileprivate var selector:Selector?
        fileprivate weak var target:AnyObject?
        
        func setupUI(){
            self.isUserInteractionEnabled = true
            self.addSubview(titleLbl)
            self.addSubview(actionBtn)
            
            titleLbl.font  = UIFont.systemFont(ofSize: Constant.common_F5_font)
            titleLbl.textColor = UIColor(rgba: Constant.common_C12_color)
            titleLbl.textAlignment = NSTextAlignment.center
            
            actionBtn.frame = CGRect(x: 9, y: 0, width: 52, height: 52)
            titleLbl.frame = CGRect(x: 0, y: actionBtn.iheight, width: self.iwidth, height: 20)
            
            actionBtn.setImage(UIImage(named: ""), for: UIControlState())
            actionBtn.addTarget(self, action: #selector(GiftRemindMenuButton.actionBtnOnClick(_:)), for: UIControlEvents.touchUpInside)
        }
        
        func addTarget(_ target: AnyObject?, action: Selector){
            self.target = target
            self.selector = action
        }
        
        func actionBtnOnClick(_ btn:UIButton)
        {
            if let tag = target,
                let sel = selector{
                
                let _ = Target_Selector(tag, sel){Void in let _ = tag.perform(sel,with:self)}
            }
        }
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func willMoveToPageMenuController(_ menuController: UIViewController, previousMenuController: UIViewController) {
        print("123")
    }
    
    
    func didMoveToPageMenuController(_ menuController: UIViewController, previousMenuController: UIViewController) {
        
        let tabTitleAry = ["全部","重要", "生日","节日","纪念日"]
        var currentIndex = 0
        
        if menuController.isKind(of: AllRemindViewController.self){
            if isChangeOfAll == 1{
                NotificationCenter.default.post(name: Notification.Name(rawValue: "Request"), object: nil)
                isChangeOfAll = 0
            }
            currentIndex = 0
        }else if menuController.isKind(of: ImportantRenindViewController.self){
            currentIndex = 1
            if isChangeOfImpor == 1 {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "Request"), object: nil)
                isChangeOfImpor = 0
            }
            
        }else if menuController.isKind(of: BirthdayRemindViewController.self){

            if isChangeOfBirthday == 1{
                NotificationCenter.default.post(name: Notification.Name(rawValue: "Request"), object: nil)
                isChangeOfBirthday = 0
            }
            currentIndex = 2
        }else if menuController.isKind(of: HolidayRemindViewController.self){

            if isChangeOfHoliday == 1{
                NotificationCenter.default.post(name: Notification.Name(rawValue: "Request"), object: nil)
                isChangeOfHoliday = 0
            }
            currentIndex = 3
        }else if menuController.isKind(of: MemorialDayRemindViewController.self){
            if isChangeOfMemorial == 1{
                NotificationCenter.default.post(name: Notification.Name(rawValue: "Request"), object: nil)
                isChangeOfMemorial = 0
            }
            
            currentIndex = 4
        }
        
        if currentIndex >= 0 && currentIndex < tabTitleAry.count{
            Statistics.count(Statistics.GiftRemind.remind_tab_click, andAttributes: ["tab":tabTitleAry[currentIndex]])
        }
    }

}

extension RemindViewController: GiftReminderGenderChooseViewDelegate {
    
    func didTapGenderChoose(_ gender: Int,purposeType:Int) {
        let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.user_gender, parameters: ["gender": gender as AnyObject], isNeedUserTokrn: true, isShowErrorStatuMsg: true, isNeedHud: true, hudTitle: "正在提交", successHandler: {data, status, msg in
            if status == 200 {
                AccountManager.shared.setGender(Int64(gender), successHandler: { () -> Void in
                    Utils.showError(context: self.view, errorStr: "设置成功")
                    
                    self.ma_dismissSemiView()
                    if purposeType == 0{
                        if self.viewModel != nil{
                            let vm = AddGiftReminderViewModel()
                            let model = self.viewModel!.model as! NSDictionary
                            vm.situation <- (model["situation"] as! NSInteger)
                            vm.relation <- (model["relation"] as! NSInteger)
                            let vc = AddGiftReminderViewController(withReminderViewModel: vm)
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                        else{
                            let vc = AddGiftReminderViewController()
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                    else if purposeType == 1{
                        self.didAuthAddressBook()
                    }
                    
                    }, failureHandler: { () -> Void in
                })
            }
        })
    }
    
    func didTapClose() {
        self.ma_dismissSemiView()
    }
}

//extension RemindViewController: GiftReminderLoginViewDelegate {
//    func didTapLoginButton(_ isQuick: Bool) {
//        self.ma_dismissSemiView()
//        
//        self.loginDialog.controller = self
//        self.loginDialog.show()
//    }
//}
