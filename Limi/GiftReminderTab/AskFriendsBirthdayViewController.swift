//
//  AskFriendsBirthdayViewController.swift
//  Limi
//
//  Created by 千云锋 on 16/8/22.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit
import AddressBook
import AddressBookUI
import ContactsUI

private var friendName:String = ""
private var friendPhoneNumber:String = ""

///发送类型 模块内全局变量
private var sendMsgType:SendMsgType!

private var askFriendsBirthdayVC:AskFriendsBirthdayViewController!
class AskFriendsBirthdayViewController: BaseViewController, AskFriendBirthdayViewControllerDelegete {

    fileprivate let sendMessageView:SendMessageView = SendMessageView()
    fileprivate let topSelectedButtonView:TopSelectedButtonView = TopSelectedButtonView()
    
    fileprivate let bottomButtom:UIButton = {
        let btn = UIButton()
        btn.frame = CGRect(x: Constant.ScreenSizeV2.MARGIN_30, y: Constant.ScreenSizeV2.MARGIN_10, width: Constant.ScreenSizeV2.SCREEN_WIDTH - Constant.ScreenSizeV2.MARGIN_60, height: Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 80)
        btn.setTitle("发送给好友", for: UIControlState())
        btn.setTitleColor(UIColor.white, for: UIControlState())
        btn.backgroundColor = Constant.Theme.Color1
        btn.layer.cornerRadius = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * 10
        return btn
    }()
    
    fileprivate let linesView:UIView = {
        let line = UIView()
        line.backgroundColor = Constant.Theme.Color8
        line.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: 0.5)
        return line
    }()
    
    fileprivate let bottomView:UIView = {
        let bottom = UIView()
        bottom.frame = CGRect(x: 0, y: Constant.ScreenSizeV2.SCREEN_HEIGHT - 64 - Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 100, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 100)
        return bottom
    }()
    
    class func newAskFriendsBirthdayViewController(_ name:String, phoneNumber:String) -> AskFriendsBirthdayViewController {
        let askVC = AskFriendsBirthdayViewController()
        askFriendsBirthdayVC = askVC
        friendName = name
        friendPhoneNumber = phoneNumber
        return askVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        self.title = "询问好友生日"
        
        bottomView.backgroundColor = UIColor.white
        bottomButtom.addTarget(self, action: #selector(AskFriendsBirthdayViewController.sendFriendOnClick(_:)), for: UIControlEvents.touchUpInside)
        bottomView.addSubview(bottomButtom)
        bottomView.addSubview(linesView)
        self.view.addSubview(bottomView)
        
        self.view.addSubview(sendMessageView)
        self.topSelectedButtonView.delegate = self
        self.view.addSubview(topSelectedButtonView)
        // Do any additional setup after loading the view.
    }

    func sendFriendOnClick(_ button:UIButton) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func topButtonViewChangeDelegete(_ sendStyle: SendMsgType) {
        if sendStyle == .message {
            UIView.animate(withDuration: 0.25, animations: { () -> Void in
                self.sendMessageView.includeSendView.contentOffset = CGPoint(x: 0, y: 0)
            }) 
        }else{
            UIView.animate(withDuration: 0.25, animations: { () -> Void in
                self.sendMessageView.includeSendView.contentOffset = CGPoint(x: Constant.ScreenSizeV2.SCREEN_WIDTH, y: 0)
            }) 
        }
    }
  
}



extension AskFriendsBirthdayViewController {

    //MARK:-短信  微信按钮
    class TopSelectedButtonView: UIView {
        
        var delegate:AskFriendBirthdayViewControllerDelegete?
        
        fileprivate let sendMessageView:SendMessageView = SendMessageView()
        
        let messageButton:UIButton = {
            let btn = UIButton()
            btn.setTitle("短信询问", for: UIControlState())
            btn.titleLabel?.font = Constant.Theme.Font_17
            btn.setTitleColor(Constant.Theme.Color1, for: UIControlState.selected)
            btn.setTitleColor(Constant.Theme.Color2, for: UIControlState())
            return btn
        }()
        
        let wechatButton:UIButton = {
            let btn = UIButton()
            btn.setTitle("微信询问", for: UIControlState())
            btn.titleLabel?.font = Constant.Theme.Font_17
            btn.setTitleColor(Constant.Theme.Color1, for: UIControlState.selected)
            btn.setTitleColor(Constant.Theme.Color2, for: UIControlState())
            return btn
        }()
        
        fileprivate let redLineView:UIView = {
            let line = UIView()
            line.backgroundColor = Constant.Theme.Color1
            return line
        }()
        
        fileprivate let bottomLineView:UIView = {
            let line = UIView()
            line.backgroundColor = Constant.Theme.Color9
            return line
        }()
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            self.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 100)
            setupUI()
        }
        //点击事件
        func onClick(_ button:UIButton) {
 
            if button == self.wechatButton{
                if !wechatButton.isSelected{
                    wechatButton.isSelected = true
                    sendMsgType = .weChat
                    messageButton.isSelected = false
                    self.delegate?.topButtonViewChangeDelegete(sendMsgType)
                    UIView.animate(withDuration: 0.25, animations: { () -> Void in
                        
                        self.redLineView.transform = CGAffineTransform(translationX: Constant.ScreenSizeV2.SCREEN_WIDTH / 2, y: 0)
                    }) 
                    
                }
            }else {
                if !messageButton.isSelected{
                    messageButton.isSelected = true
                    sendMsgType = .message
                    wechatButton.isSelected = false
                    self.delegate?.topButtonViewChangeDelegete(sendMsgType)
                    UIView.animate(withDuration: 0.25, animations: { () -> Void in
                        
                        self.redLineView.transform = CGAffineTransform(translationX: 0, y: 0)
                    }) 
                    
                }

            }

        }
 
        func setupUI() {
            messageButton.isSelected = true
            sendMsgType = .message
            messageButton.addTarget(self, action: #selector(TopSelectedButtonView.onClick(_:)), for: UIControlEvents.touchUpInside)
            wechatButton.addTarget(self, action: #selector(TopSelectedButtonView.onClick(_:)), for: UIControlEvents.touchUpInside)
            let _ = [messageButton, wechatButton, redLineView, bottomLineView].map{self.addSubview($0)}
            
            let widthButton = CalculateHeightOrWidth.getLabOrBtnWidth((messageButton.titleLabel?.text)! as NSString, font: (messageButton.titleLabel?.font)!, height: 100)
            
            messageButton.snp_makeConstraints { (make) in
                let _ = make.centerY.equalTo(self)
                let _ = make.left.equalTo((Constant.ScreenSizeV2.SCREEN_WIDTH / 2 - widthButton) / 2)
                let _ = make.width.equalTo(widthButton)
            }
            wechatButton.snp_makeConstraints { (make) in
                let _ = make.centerY.equalTo(self)
                let _ = make.right.equalTo(-(Constant.ScreenSizeV2.SCREEN_WIDTH / 2 - widthButton) / 2)
                let _ = make.width.equalTo(widthButton)
            }
            redLineView.snp_makeConstraints { (make) in
                let _ = make.left.equalTo(messageButton.snp_left)
                let _ = make.bottom.equalTo(self)
                let _ = make.width.equalTo(widthButton)
                let _ = make.height.equalTo(1)
            }
            
            bottomLineView.snp_makeConstraints { (make) in
                let _ = make.left.equalTo(self)
                let _ = make.bottom.equalTo(self)
                let _ = make.right.equalTo(self)
                let _ = make.height.equalTo(0.5)
            }
        }
        
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    class SendMessageView: UIView {
        
        fileprivate let messageSend:MessageSend = MessageSend()
        fileprivate let wechatSend:WechatSend = WechatSend()
        
        //承载发送内容的View
        let includeSendView:UIScrollView = {
            let scrollView = UIScrollView()
            scrollView.bounces = false
            scrollView.isPagingEnabled = true
            scrollView.isScrollEnabled = false
            scrollView.showsHorizontalScrollIndicator = false
            scrollView.contentSize = CGSize(width: Constant.ScreenSizeV2.SCREEN_WIDTH * 2, height: Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 550)
            return scrollView
        }()
        
        //上一条
        fileprivate let leftImageView:UIImageView = {
            let imageView = UIImageView()
            imageView.image = UIImage(named:"gift_reminder_left")
            return imageView
        }()
        fileprivate let leftButton:UIButton = {
            let btn = UIButton()
            btn.setTitle("上一条", for: UIControlState())
            btn.titleLabel?.font = Constant.Theme.Font_15
            btn.setTitleColor(Constant.Theme.Color6, for: UIControlState())
            return btn
        }()
        
        //下一条
        fileprivate let rightImageView:UIImageView = {
            let imageView = UIImageView()
            imageView.image = UIImage(named:"gift_reminder_right")
            return imageView
        }()
        fileprivate let rightButton:UIButton = {
            let btn = UIButton()
            btn.setTitle("下一条", for: UIControlState())
            btn.titleLabel?.font = Constant.Theme.Font_15
            btn.setTitleColor(Constant.Theme.Color6, for: UIControlState())
            return btn
        }()
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            self.frame = CGRect(x: 0, y: Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 100, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 700)
            self.backgroundColor = UIColor.white
            setupUI()
            
        }
        
        func setupUI(){
            self.includeSendView.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 550)
            self.includeSendView.contentSize = CGSize(width: Constant.ScreenSizeV2.SCREEN_WIDTH * 2, height: Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 550)
            self.messageSend.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 550)
            self.wechatSend.frame = CGRect(x: Constant.ScreenSizeV2.SCREEN_WIDTH, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 550)
            includeSendView.addSubview(messageSend)
            includeSendView.addSubview(wechatSend)

            let _ = [includeSendView, leftImageView, leftButton, rightImageView, rightButton].map{self.addSubview($0)}

            leftImageView.snp_makeConstraints { (make) in
                let _ = make.top.equalTo(includeSendView.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_30)
                let _ = make.left.equalTo(self).offset(Constant.ScreenSizeV2.MARGIN_30)
                let _ = make.height.equalTo(Constant.ScreenSizeV2.MARGIN_30)
                let _ = make.width.equalTo(Constant.ScreenSizeV2.MARGIN_30)
            }

            rightImageView.snp_makeConstraints { (make) in
                let _ = make.top.equalTo(includeSendView.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_30)
                let _ = make.right.equalTo(self).offset(-Constant.ScreenSizeV2.MARGIN_30)
                let _ = make.height.equalTo(Constant.ScreenSizeV2.MARGIN_30)
                let _ = make.width.equalTo(Constant.ScreenSizeV2.MARGIN_30)
            }
            
            leftButton.snp_makeConstraints { (make) in
                let _ = make.top.equalTo(leftImageView)
                let _ = make.height.equalTo(Constant.ScreenSizeV2.MARGIN_30)
                let _ = make.left.equalTo(leftImageView.snp_right)
            }

            rightButton.snp_makeConstraints { (make) in
                let _ = make.top.equalTo(leftImageView)
                let _ = make.right.equalTo(rightImageView.snp_left)
                let _ = make.height.equalTo(Constant.ScreenSizeV2.MARGIN_30)
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }

        //短信发送
        class MessageSend: UIView, UITextFieldDelegate ,ABPeoplePickerNavigationControllerDelegate,CNContactPickerDelegate {

            fileprivate let messageNameLabel:UILabel = {
                let lbl = UILabel()
                lbl.text = "风清扬"
                lbl.textColor = Constant.Theme.Color14
                lbl.font = Constant.Theme.Font_15
                return lbl
            }()
            //联系人电话号码输入框
            fileprivate let messageMobileField:UITextField = {
                let tf = UITextField()
                tf.keyboardType = .numberPad
                tf.text = "18615473596"
                tf.font = Constant.Theme.Font_13
                tf.textColor = Constant.Theme.Color14
                return tf
            }()
            
            //通讯录button
            fileprivate let addressButton:UIButton = {
                let btn = UIButton()
                btn.setImage(UIImage(named:"heartWish_contactIcon"), for: UIControlState())
                return btn
            }()
            
            //联系人控件组
            fileprivate let peopleContentView:UIView = {
                let view = UIView()
                view.layer.borderWidth = 0.5
                view.layer.borderColor = Constant.Theme.Color9.cgColor
                return view
            }()
            
            //短信内容的view
            fileprivate let messageContentView:UIView = {
                let view = UIView()
                view.layer.borderWidth = 0.5
                view.layer.borderColor = Constant.Theme.Color9.cgColor
                return view
            }()
            
            //短信内容
            fileprivate let messageContentField:UITextView = {
                let tv = UITextView()
                tv.text = ""
                tv.font = Constant.Theme.Font_15
                tv.textColor = Constant.Theme.Color6
                return tv
            }()
            
            //网址Label
            fileprivate let webURLLabel:UILabel = {
                let lbl = UILabel()
                lbl.text = "询问链接：www.baidu.eeeeeeeecon"
                lbl.textColor = Constant.Theme.Color8
                lbl.font = Constant.Theme.Font_13
                return lbl
            }()
            
            //字数限制
            fileprivate let wordNumberLabel:UILabel = {
                let lbl = UILabel()
                lbl.text = "还可以写10字"
                lbl.textColor = Constant.Theme.Color8
                lbl.font = Constant.Theme.Font_13
                return lbl
            }()
            
            
            override init(frame: CGRect) {
                super.init(frame: frame)
                
                setupUI()
            }
            
            func onClickAddressBook(_ button:UIButton) {
                if #available(iOS 9.0, *) {
                    let newPicker = CNContactPickerViewController()
                    newPicker.delegate = self
                    askFriendsBirthdayVC.present(newPicker, animated: true, completion: {
                        
                    })
                } else {
                    //弹出通讯录联系人选择界面
                    let picker = ABPeoplePickerNavigationController()
                    picker.displayedProperties = [3]
                    picker.peoplePickerDelegate = self
                    
                    if ((UIDevice.current.systemVersion as NSString).floatValue >= 8.0) {
                        picker.predicateForSelectionOfPerson = NSPredicate(value:false)
                    }
                    
                    askFriendsBirthdayVC.present(picker, animated: true, completion: nil)
                }
            }
            
            @available(iOS 9.0, *)
            @objc(contactPicker:didSelectContactProperty:)
            func contactPicker(_ picker: CNContactPickerViewController, didSelect contactProperty: CNContactProperty) {
                let firstName = contactProperty.contact.givenName
                let name = contactProperty.contact.familyName
                self.messageNameLabel.text = (name + firstName)
                
                if contactProperty.value != nil {
                    let tempPhoneNumber = contactProperty.value as! CNPhoneNumber
                    let phoneNumber = tempPhoneNumber.stringValue
                    self.messageMobileField.text = Utils.detectionTelphoneSimple(phoneNumber)
                }
                
            }
            
            @available(iOS 9.0, *)
            func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
                //去除地址选择界面
                picker.dismiss(animated: true, completion: { () -> Void in
                    
                })
            }

            
            func setupUI(){
                
                addressButton.addTarget(self, action: #selector(MessageSend.onClickAddressBook(_:)), for: UIControlEvents.touchUpInside)
                
                peopleContentView.addSubview(messageNameLabel)
                peopleContentView.addSubview(messageMobileField)
                peopleContentView.addSubview(addressButton)
                messageContentView.addSubview(messageContentField)
                messageContentView.addSubview(webURLLabel)
                messageContentView.addSubview(wordNumberLabel)
                self.addSubview(peopleContentView)
                self.addSubview(messageContentView)
                
                peopleContentView.snp_makeConstraints { (make) in
                    let _ = make.top.equalTo(self).offset(Constant.ScreenSizeV2.MARGIN_30)
                    let _ = make.left.equalTo(self).offset(Constant.ScreenSizeV2.MARGIN_30)
                    let _ = make.right.equalTo(self).offset(-Constant.ScreenSizeV2.MARGIN_30)
                    let _ = make.height.equalTo(Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 90)
                }
                
                messageContentView.snp_makeConstraints { (make) in
                    let _ = make.top.equalTo(peopleContentView.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_30)
                    let _ = make.left.equalTo(self).offset(Constant.ScreenSizeV2.MARGIN_30)
                    let _ = make.right.equalTo(self).offset(-Constant.ScreenSizeV2.MARGIN_30)
                    let _ = make.height.equalTo(Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 400)
                }
                
                let labelWidth = CalculateHeightOrWidth.getLabOrBtnWidth(messageNameLabel.text! as NSString, font: messageNameLabel.font, height: 60)
                
                messageNameLabel.snp_makeConstraints { (make) in
                    let _ = make.left.equalTo(peopleContentView).offset(Constant.ScreenSizeV2.MARGIN_30)
                    let _ = make.centerY.equalTo(peopleContentView)
                    let _ = make.width.equalTo(labelWidth)
                }
                
                messageMobileField.snp_makeConstraints { (make) in
                    let _ = make.left.equalTo(messageNameLabel.snp_right)
                    let _ = make.centerY.equalTo(peopleContentView)
                    let _ = make.right.equalTo(addressButton.snp_left).offset(-Constant.ScreenSizeV2.MARGIN_40)
                    let _ = make.height.equalTo(Constant.ScreenSizeV2.MARGIN_60)
                }
                addressButton.snp_makeConstraints { (make) in
                    let _ = make.right.equalTo(peopleContentView).offset(-Constant.ScreenSizeV2.MARGIN_30)
                    let _ = make.centerY.equalTo(peopleContentView)
                    let _ = make.height.equalTo(Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 50)
                    let _ = make.width.equalTo(Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * 50)
                }
                
                messageContentField.snp_makeConstraints { (make) in
                    let _ = make.top.equalTo(messageContentView).offset(Constant.ScreenSizeV2.MARGIN_30)
                    let _ = make.left.equalTo(messageContentView).offset(Constant.ScreenSizeV2.MARGIN_30)
                    let _ = make.right.equalTo(messageContentView).offset(-Constant.ScreenSizeV2.MARGIN_30)
                    let _ = make.height.equalTo(Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 270)
                }
                
                wordNumberLabel.snp_makeConstraints { (make) in
                    let _ = make.right.equalTo(messageContentView).offset(-Constant.ScreenSizeV2.MARGIN_30)
                    let _ = make.bottom.equalTo(messageContentView).offset(-Constant.ScreenSizeV2.MARGIN_20)
                }
                
                webURLLabel.snp_makeConstraints { (make) in
                    let _ = make.bottom.equalTo(messageContentView).offset(-Constant.ScreenSizeV2.MARGIN_20)
                    let _ = make.left.equalTo(messageContentView).offset(Constant.ScreenSizeV2.MARGIN_30)
                    let _ = make.width.equalTo(Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * 350)
                }
                
                
                
            }
            
            required init?(coder aDecoder: NSCoder) {
                fatalError("init(coder:) has not been implemented")
            }
            
            //MARK:-ABPeoplePickerNavigationControllerDelegate
            func peoplePickerNavigationController(_ peoplePicker: ABPeoplePickerNavigationController,
                                                  didSelectPerson person: ABRecord, property: ABPropertyID,
                                                                  identifier: ABMultiValueIdentifier) {
                
                let multiValue: ABMultiValue = ABRecordCopyValue(person, property).takeRetainedValue()
                var index = ABMultiValueGetIndexForIdentifier(multiValue, identifier)
                if index == -1{
                    index = 0 as CFIndex
                }
                let phone = ABMultiValueCopyValueAtIndex(multiValue, index).takeRetainedValue() as? String ?? ""
                
                let first = ABRecordCopyValue(person, kABPersonFirstNameProperty)?.takeRetainedValue() as? String ?? ""
                let last  = ABRecordCopyValue(person, kABPersonLastNameProperty)?.takeRetainedValue() as? String ?? ""
                
                self.messageNameLabel.text = last + first
                
                self.messageMobileField.text = Utils.detectionTelphoneSimple(phone)
            }
            
            //取消按钮点击
            func peoplePickerNavigationControllerDidCancel(_ peoplePicker: ABPeoplePickerNavigationController) {
                
                peoplePicker.dismiss(animated: true, completion: { () -> Void in
                    
                })
            }
            
            func peoplePickerNavigationController(_ peoplePicker: ABPeoplePickerNavigationController,
                                                  shouldContinueAfterSelectingPerson person: ABRecord) -> Bool {
                return false
            }
            
            func peoplePickerNavigationController(_ peoplePicker: ABPeoplePickerNavigationController,
                                                  shouldContinueAfterSelectingPerson person: ABRecord, property: ABPropertyID,
                                                                                     identifier: ABMultiValueIdentifier) -> Bool {
                return false

            }
        }
        
        //微信发送
        class WechatSend: UIView {
            
            //wei信内容的view
            fileprivate let messageContentView:UIView = {
                let view = UIView()
                view.layer.borderWidth = 0.5
                view.layer.borderColor = Constant.Theme.Color9.cgColor
                return view
            }()
            
            //wei信内容
            fileprivate let messageContentField:UITextView = {
                let tv = UITextView()
                tv.text = ""
                tv.font = Constant.Theme.Font_15
                tv.textColor = Constant.Theme.Color6
                return tv
            }()
            
            //网址Label
            fileprivate let webURLLabel:UILabel = {
                let lbl = UILabel()
                lbl.text = "询问链接：www.baidu.eeeeeeeecon"
                lbl.textColor = Constant.Theme.Color8
                lbl.font = Constant.Theme.Font_13
                return lbl
            }()
            
            //字数限制
            fileprivate let wordNumberLabel:UILabel = {
                let lbl = UILabel()
                lbl.text = "还可以写10字"
                lbl.textColor = Constant.Theme.Color8
                lbl.font = Constant.Theme.Font_13
                return lbl
            }()

            
            override init(frame: CGRect) {
                super.init(frame: frame)
                setupUI()
            }
            
            func setupUI(){
                messageContentView.addSubview(messageContentField)
                messageContentView.addSubview(webURLLabel)
                messageContentView.addSubview(wordNumberLabel)
                self.addSubview(messageContentView)
                
                messageContentView.snp_makeConstraints { (make) in
                    let _ = make.top.equalTo(self).offset(Constant.ScreenSizeV2.MARGIN_30)
                    let _ = make.left.equalTo(self).offset(Constant.ScreenSizeV2.MARGIN_30)
                    let _ = make.right.equalTo(self).offset(-Constant.ScreenSizeV2.MARGIN_30)
                    let _ = make.height.equalTo(Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 520)
                }
                messageContentField.snp_makeConstraints { (make) in
                    let _ = make.top.equalTo(messageContentView).offset(Constant.ScreenSizeV2.MARGIN_30)
                    let _ = make.left.equalTo(messageContentView).offset(Constant.ScreenSizeV2.MARGIN_30)
                    let _ = make.right.equalTo(messageContentView).offset(-Constant.ScreenSizeV2.MARGIN_30)
                    let _ = make.height.equalTo(Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 420)
                }
                
                wordNumberLabel.snp_makeConstraints { (make) in
                    let _ = make.right.equalTo(messageContentView).offset(-Constant.ScreenSizeV2.MARGIN_30)
                    let _ = make.bottom.equalTo(messageContentView).offset(-Constant.ScreenSizeV2.MARGIN_20)
                }
                
                webURLLabel.snp_makeConstraints { (make) in
                    let _ = make.bottom.equalTo(messageContentView).offset(-Constant.ScreenSizeV2.MARGIN_20)
                    let _ = make.left.equalTo(messageContentView).offset(Constant.ScreenSizeV2.MARGIN_30)
                    let _ = make.width.equalTo(Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * 350)
                }

            }
            
            required init?(coder aDecoder: NSCoder) {
                fatalError("init(coder:) has not been implemented")
            }

        }
        

    }

}

protocol AskFriendBirthdayViewControllerDelegete {
    func topButtonViewChangeDelegete(_ sendStyle:SendMsgType)
//    func openAddressBookDelegate()
}











