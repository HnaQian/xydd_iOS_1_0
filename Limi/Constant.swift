
//
//  Constant.swift
//  Limi
//
//  Created by 程巍巍 on 3/30/15.
//  Copyright (c) 2015 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//  全局静态变量
//  为避免命名冲突，所有全局静态变量在 Constant 命名空间下

import Foundation

struct Constant
{
    
    static let NETWORK_ERR = "网络连接异常"
    static let NETWORK_DATA_ERR = "数据异常"
    static let NETWORK_NO = "请检查网络连接"
    
    //个推sdk
    static let kGtAppId:String = "rb7wW0iQC77d2EJ5ueVV82"
    static let kGtAppKey:String = "mDSaK5xOBo58OYlHaB2uu"
    static let kGtAppSecret:String = "7ErqClrqmAAJFpAsWt2Cr4"
    
    //魔窗sdk
    static let MW_APPKEY = "22JFZA5ZQCC4CVLA10GU2HV5ONU9C8W8"
    
    //个推测试账号 一般情况下不要替换
//        static let kGtAppId:String = "axsdzZ2FCH79VTf4xuhq82"
//        static let kGtAppKey:String = "sDcWGsQC3N78GeerfdS3l6"
//        static let kGtAppSecret:String = "1qG78KqAX1ArksDdfxD1u8"
    
    /**
     *   友盟 SDK appkey
     */
    
    static let UMENG_APPKEY_DIS = "5506cc86fd98c5d5a800002e"
    //    static let UMENG_APPKEY_DEV = "5742ba0f67e58ece0b001f93"
    
    /**
     *   Tencent 开放平台
     */
    static let TENCENT_APPID = "1104431215"
    static let TENCENT_APPKEY = "8Cd8QJSzn5qivdBQ"
    
    
    /**
     *   阿里
     */
    static let ALI_APPKEY = "23238662"
    
    
    /**
     *   wechat
     */
    //旧的
//    static let WECHAT_APPID = "wxd116fccf47677cd1"
//    static let WECHAT_APPSECRET = "0f5a70cfd0a668185ede1e09e97adb68"
    //新的
    static let WECHAT_APPID = "wx554c594a6a757774"
    static let WECHAT_APPSECRET = "8e939d03840763f97c20394bc90cfc79"
    
    /**
     *   七鱼
     */
    static let QIYU_APPKEY = "590ca790fbc33dff769242402d607269"
    static var QIYU_APPNAME = appSwitchKey == 1 ? "心意点点_ios_正式" : "心意点点_ios_测试"
    
    static var UNREAD = 0
    
    /**
     *    设备信息统计
     */
    static let DEVICE_APPKEY = "210b7fbc22b75eda5bd50fec4ffde926"
    
    /**
     1：应用发布到AppStore，发布不可以在app中任意切换正/测服务器
     0：内部测试和使用，可以在app中任意切换正/测服务器 具体如何修改参考：DevelopTestManager
     */
    ///注意: 发布不之前 appSwitchKey的值必须为:1  httpSwitchKey的值不做要求
    static let appSwitchKey = 1
    
    // 1：正式服务器  0：测试服务器 
    static func setHttpSwitchKey(_ key:Int){
        UserDefaults.standard.set(key, forKey: "httpSwitchKey")
        UserDefaults.standard.synchronize()
    }
    
    //httpKey
    static var httpSwitchKey:Int{
        if let key = UserDefaults.standard.object(forKey: "httpSwitchKey"){
            return key as! Int
        }
        return 0
    }
    
    static let versionCode = "1401"
    
    /**
     *   接口 地址
     */
    struct JTAPI{
        
        fileprivate static let HOST_TEST = "http://test.api.giftyou.me/"
        fileprivate static let HOST_RELEASE = "https://api.giftyou.me/"

        fileprivate static let NEW_HOST_TEST = "http://test.api.xydd.co/"
        fileprivate static let NEW_HOST_RELEASE = "https://api.xydd.co/"
        
        fileprivate static var HOST:String{
            if appSwitchKey == 1{return HOST_RELEASE}
            return httpSwitchKey == 1 ? HOST_RELEASE : HOST_TEST
        }
        
        fileprivate static var NEW_HOST:String{
            if appSwitchKey == 1{return NEW_HOST_RELEASE}
            return httpSwitchKey == 1 ? NEW_HOST_RELEASE : NEW_HOST_TEST
        }
        
        static let PrivateHost = "wechat.giftyou.me"
        
        static let PrivateTestHost = "test.wechat.giftyou.me"
        
        static let MWShortUrlHost = "s.mlinks.cc"
        
        static let pop_ad = HOST + "v1/common/pop_ad"
        
        static var layout: String {return HOST + "v1/layout"}
        
        //绑定手机号
        static var regOpen: String {return HOST + "v1/reg/open"}
        
        //提交推送token
        static var commitToken: String {return HOST + "v1/common/app_token"}
        
        //是否存在同类型的第三方账号
        static var isUserNameExistByOpen: String {return HOST + "v1/reg/is_user_name_exist_by_open"}
        
        //短信验正码
        static var sms: String {return HOST + "v1/sms"}
        
        //语音验证码
        static var voice_sms: String {return NEW_HOST + "v1/sms/index"}
        static var sms_check: String {return HOST + "v1/sms/check"}
        
        //系统信息
        static var setting_info: String {return HOST + "v1/setting/info"}
        
        /**
         上传图像
         
         PRD
         v1/user/avatar
         
         NOW
         v1/upload/image
         
         */
        static var image_upload: String {return HOST + "v1/upload/image"}
        
        /**
         *   用户账号管理
         */
        static var register: String {return HOST + "v1/reg"}//注册
        static var login: String {return HOST + "v1/login"}//登录
        static var login_findPassword: String {return HOST + "v1/login/find_password"}
        static var login_resetPassword: String {return HOST + "v1/login/reset_password"}
        static var login_open: String {return HOST + "v1/login/open"}
        static var user_info_v2: String {return HOST + "v2/user"}
        static var user_nick_name: String {return HOST + "v1/user/nick_name"}
        static var user_bind_mobile: String {return HOST + "v1/user/bind_mobile"}
        static var user_bind_open: String {return HOST + "v1/user/bind_open"}
        static var user_unbind_open: String {return HOST + "v1/user/unbind_open"}
        static var user_avatar: String {return HOST + "v1/user/avatar"}
        static var user_gender: String {return HOST + "v1/user/gender"}
        static var user_birthday: String {return HOST + "v1/user/birthday"}
        static var login_sms:String {return HOST + "v1/login/sms"}//短信登录
        static var syn_userSetting:String {return HOST + "v1/user/setting"}//同步用户信息
        // 订单详情
        static var order_item: String {return HOST + "v1/order/item"}
        // 收货地址管理
        static var address_list: String {return HOST + "v1/address/list"}
        static var address_add: String { return HOST + "v1/address/add"}
        static var address_del: String {return HOST + "v1/address/del"}
        static var address_update: String {return HOST + "v1/address/update"}
        static var address_default: String {return HOST + "v1/address/default"}
        
        // 购物车
        static var gift_cart_add: String {return HOST + "v1/gift_basket/add"}
        static var gift_cart_update_num: String { return HOST + "v1/gift_basket/update_num"}
        static var gift_cart_update: String {return HOST + "v1/gift_basket/update"}
        static var gift_cart_del: String {return HOST + "v1/gift_basket/del"}
        static var gift_cart_list: String {return HOST + "v1/gift_basket/list"}
        static var gift_cart_num: String {return HOST + "v1/gift_basket/num"}//获取购物车商品数量
        
        // 收藏
        static var collect_add: String {return HOST + "v1/collect/add"}
        static var collect_del: String {return HOST + "v1/collect/del"}
        static var collect_list: String {return NEW_HOST + "v1/collect/list"}
        
        
        //签到列表
        static let daily_sign_list = NEW_HOST + "v1/checkin/list"
        static let daily_sign_add = NEW_HOST + "v1/checkin/add"
        //积分列表
        static let point_goodslist = NEW_HOST + "v1/point/goodslist"
        static let point_list = NEW_HOST + "v1/point/list"
        static let point_charge = NEW_HOST + "v1/point/addorder"
        // 留言反馈
        static var feedback: String {return HOST + "v1/setting/feedback"}
        
        /**
         首页
         */
        static var home_page:String {return HOST + "v1/layout/index"}
        
        static var gift_page:String {return HOST + "v1/layout/gift"}
        
        static var birthday_page:String {return HOST + "v1/layout/birthday"}
        
        static var wedding_page:String {return HOST + "v1/layout/wedding"}
        static var abroad_page:String {return HOST + "v1/layout/abroad"}
        static var user_relation_index_v2:String {return HOST + "v2/user_relation/index"}//首页送礼提心
        /**
         商品
         */
        static var goods: String {return HOST + "v1/goods"}
        
        static let goods_spec = NEW_HOST + "v1/goods/spec"
        
        static let goods_relation = NEW_HOST + "v1/goods/relationOrderGoods"
        
        static let goods_other = NEW_HOST + "v1/goods/otherEventGoods"
        
        static var goods_list: String {return HOST + "v1/goods/list"}
        
        static var goods_list_bytag: String {return HOST + "v1/goods/get_list_by_tag"}
        
        static var goods_best_item: String { return HOST + "v1/goods/best_item" }
        
        static var order_commit: String {return HOST + "v1/order"}
        
        static var order_commitV2: String {return HOST + "v2/order"}
        
        static var order_ExpressTipe: String {return HOST + "v1/gift_service/get_logistics"}
        
        static var order_list: String { return HOST + "v2/order/list" }//订单列表
        
        static var order_listV1:String{return HOST + "v1/order/gift_list"}
        
        static var order_num: String { return HOST + "v1/order/num" }//订单数量
        
        static var order_sold: String { return HOST + "v2/order/sold" }
        
        static var order_prepay: String { return HOST + "v1/pay/prepay" }
        
        static var order_cancel: String { return HOST + "v1/order/del" }
        
        static var notify_add: String { return HOST + "v1/notify/add" }
        
        
        //优惠券/红包
        static var user_coupon_list:String {return HOST + "v2/coupon/user_coupon_list"}//获取用户优惠券列表
        
        static var order_coupon_list:String {return HOST + "v2/coupon/order_coupon_list"}//获取订单可用优惠券列表
        
        static var add_user_coupon:String {return HOST + "v1/coupon/add_user_coupon"}//领取优惠券
        
        static var one_user_coupon:String {return HOST + "v1/coupon/one_user_coupon"} //优惠券详情
        
        static var get_distribute_coupon:String {return HOST + "v1/coupon/get_distribute_coupon"} //派发优惠券
        static var get_distribute_coupon2:String {return HOST + "v2/coupon/get_distribute_coupon"} //派发优惠券2
        static var exchange_coupon:String{return HOST + "v1/coupon/exchange_coupon"} //兑换优惠券
        
        
        //送礼提醒
        static var user_relation_info:String {return HOST + "v1/user_relation/info"}//新手引导
        
        static var set_beginner_guide:String {return HOST + "v1/user_relation/set_beginner_guide"}//设置新手引导状态
        
        static var user_relation_upload:String {return HOST + "v1/user_relation/upload"}//上传通讯录
        
        static var user_relation_import:String {return HOST + "v1/user_relation/import"}//导入联系人
        
        static var user_relation_relation:String {return HOST + "v1/user_relation/relation"}//关系信息
        static var user_relation_relation_v2:String {return HOST + "v2/user_relation/relation"}//关系信息
        
        static var user_relation_add:String {return HOST + "v1/user_relation/add"}//添加送礼提醒
        static var user_relation_add_v2:String {return HOST + "v2/user_relation/add"}//添加送礼提醒
        
        static var user_relation_one:String {return HOST + "v1/user_relation/one"}//获取送礼提醒
        static var user_relation_one_v2:String {return HOST + "v2/user_relation/one"}//获取送礼提醒
        
        static var user_relation_update:String {return HOST + "v1/user_relation/update"}//更新联系人
        static var user_relation_update_v2:String {return HOST + "v2/user_relation/update"}//更新联系人
        
        static var user_relation_del:String {return HOST + "v1/user_relation/del"}//删除联系人
        
        static var user_relation_list:String {return HOST + "v1/user_relation/list"}//送礼提醒列表
        static var user_relation_list_v2:String {return HOST + "v2/user_relation/list"}//送礼提醒列表
        
        static var user_relation_index:String {return HOST + "v1/user_relation/index"}//首页送礼提醒列表
        
        static var detail_users:String {return HOST + "v1/user_relation/detail"}//联系人送礼提醒详情
        
        static var detail_holiday:String {return HOST + "v1/user_relation/holiday_detail"}//节日送礼提醒详情
        
        static var detail_users_v2:String {return HOST + "v2/user_relation/detail"}//联系人送礼提醒详情
        
        static var detail_holiday_v2:String {return HOST + "v2/user_relation/holiday_detail"}//节日送礼提醒详情
        
        static var detail_users_goodslist:String {return HOST + "v1/user_relation/goods"}//联系人送礼提醒详情中的商品列表
        
        static var detail_holiday_goodslist:String {return HOST + "v1/user_relation/holiday_goods"}//节日送礼提醒详情的商品列表
        
        static var get_gift_remind:String {return HOST + "v1/user_relation/get_gift_remind"}//获取送礼提醒时间
        
        static var set_gift_remind:String {return HOST + "v1/user_relation/set_gift_remind"}//设置送礼提醒时间
        static var mark_is_important:String {return NEW_HOST + "v1/Shonglinotice/markImportant"}//送礼提醒-添加或取消重要标识
        
        static var important_remind_list:String {return NEW_HOST + "v1/Shonglinotice/importantItemsList"}//送礼提醒重要的列表
        
        //文章
        static var recommend_list: String { return HOST + "v1/master/recommend_list" }
        
        //心意榜单
        static let top_goods_list = HOST + "v1/top/goods_list"
        
        /**
         极品
         */
        static var best: String {return HOST + "v1/goods/best"}
        static var best_attr: String {return HOST + "v1/goods/best_attr"}
        
        
        /**
         主题
         */
        static var article_list: String {return HOST + "v1/article/list"}
        static let oldarticle_detail:String = HOST + "v1/article"
        static var article_detail: String {return HOST + "v2/article/"}
        
        static var package_item: String {return NEW_HOST + "v1/package/info"}
        
        /**
         message
         */
        //消息类型列表
        static var message_list: String {return NEW_HOST + "v1/message/list"}
        //类型中的消息列表
        static var message_typeList :String {return NEW_HOST + "v1/message/msgList"}
        static var message_markread: String {return HOST + "v1/message/read"}
        static var message_del: String {return HOST + "v1/message/del"}
        static var message_unread_count: String {return HOST + "v1/message/unread_num"}
        
        static var share: String { return HOST + "v1/share" }
        
        /**
         搜索热词
         */
        static var searchHot_word: String {return HOST + "v1/search/hot_word"}
//        static var search: String {return HOST + "v1/search"}
        //搜索商品
        static let  search_goods = NEW_HOST + "v1/search/goods"
        //搜索攻略
        static let  search_article = NEW_HOST + "v1/search/article"
        
        /**
         选礼
         **/
        static let choosegift_getFilterItems = NEW_HOST + "v1/choosegift/getFilterItems"
        static let choosegift_filterGoodsByReceiverInfo = NEW_HOST + "v1/choosegift/filterGoodsByReceiverInfo"
        /**
         新用户设置
         */
        static var newUser_info: String {return HOST + "v1/user_relation/info"}
        
        static var newUser_tip: String {return HOST + "v1/user_relation/tips"}
        
        static var newUser_set: String {return HOST + "v1/user_relation/set_beginner_guide"}
        
        
        ///心意祝福语
        static var bless_list: String {return HOST + "v1/bless/list"}
        
        ///发送心意祝福
        static var bless_send: String {return HOST + "v1/bless/send"}
        
        //MARK:通过标签搜索商品
        static var goods_tag: String {return HOST + "v1/search/goods_tag"}
        
        //MARK:通过标签搜索文章
        static var article_tag: String {return HOST + "v1/search/article_tag"}
        
        //生日标签
        static var birthday_tag_list: String {return HOST + "v1/birthday/tag_list"}
        
        //开发票
        static var invoice_commit : String{return HOST + "v1/user_invoice/make"}
        //发票列表
        static var invoice_list : String{return HOST + "v1/user_invoice/list"}
        //新增发票
        static var invoice_add : String{return HOST + "v1/user_invoice/add"}
        //删除发票
        static var invoice_del : String{return HOST + "v1/user_invoice/del"}
        
        //推荐商品
        static var goodsOfrecommend_list: String { return HOST + "v1/goods/recommend_list" }
        
        //订单链接
        static var logistic_link:String {
            if httpSwitchKey == 1 {
                return "http://www.giftyou.me/admin/neworder/view"
            }
            return "http://test.www.giftyou.me/admin/neworder/view"
        }
        
        //上传音频
        static var voice_upload : String{return HOST + "v1/upload/audio"}
        //获取音频
        static var voice_info : String{return HOST + "v1/gift_service/get_audio"}
        
        //添加录音祝福
        static var voice_add : String{return HOST + "v1/gift_service/add_audio"}
        
        
        //话费
        static let fees_segment = HOST + "v2/fees/segment"
        static let fees_order   = HOST + "v2/fees/order"
        static let fees_list    = HOST + "v2/fees/list"
        //流量
        static let flow_list    = HOST + "v1/flow/list"
        static let flow_order   = HOST + "v1/flow/order"
        static let flow_segment = HOST + "v1/flow/segment"
        //充值可用红包
        static let recharge_coupon_list = HOST + "v1/coupon/mobile_coupon_list"
        
        
        //获取选择的心意服务信息
        static let giftservice = NEW_HOST + "v1/giftservice/list"
        
        //获取文章中的商品
        static let article_goods = NEW_HOST + "v1/article/goods"
        
        //获取文章相关推荐商品
        static let article_recommandGoods = NEW_HOST + "v1/article/recommendGoods"
        
        /*
         * 问答社区
         */
        //大家问
        static let common_asklist = NEW_HOST + "v1/sns/askList"
        //我的问答
        static let my_asklist = NEW_HOST + "v1/sns/myAskList"
        //查看，点赞
        static let check_like = NEW_HOST + "v1/sns/addNum"
        //检查动态推送
        static let check_dynamic = NEW_HOST + "v1/sns/checkNotice"
        //动态消息列表
        static let dynamic_noticelist = NEW_HOST + "v1/sns/noticeList"
        //更新成已读
        static let update_noticeStatus = NEW_HOST + "v1/sns/updateNoticeStatus"
        //用户提问
        static let submitQuestion = NEW_HOST + "v1/sns/addQuestion"
        
        //提交用户浏览行为信息
        static let submitScanning = NEW_HOST + "v1/Semuserbehaviour/submitBehaviour"
    }
    
    /**
     *   状态码说明
     */
    static let JTAPIStatus: [Int: String] = [
        200: "",
        10003: "手机号不存在"
    ]
    
    /**
     字体
     */
    struct CustomFont {
        static func FZXiYuan(size: CGFloat) ->UIFont {
            let font = UIFont(name: "FZXiYuan-M01S", size: size)
            assert(font != nil, "Custom font not found: FZXiYuan-M01S")
            return font!
        }
        
        static func Default(size: CGFloat) -> UIFont {
            return UIFont.systemFont(ofSize: size)
        }
        static var DefaultFontName: String {
            return ".HelveticaNeueInterface-Regular"
        }
    }
    
    // 时间定义
    static let autoScrollInterval: TimeInterval = 3.5
    
    //颜色定义
    static let common_red_color = "#E13636"
    static let common_white_color = "#ffffff"
    static let common_background_color = "#F5F5F5"
    static let titlebar_color = "#FCFCFC"
    static let common_borderColor_color = "#EAEAEA"
    static let segment_borderColor_color = "#808080"
    static let common_not_selected_color = "#656565"
    static let common_separatLine_color = "#EAEAEA"
    
    static let common_C1_color = "#FF3A48"
    static let common_C2_color = "#12171F"
    static let common_C3_color = "#414955"
    static let common_C4_color = "#7D8BA4"
    static let common_C5_color = "#F2F3F5"
    static let common_C6_color = "#555555"
    static let common_C7_color = "#999999"
    static let common_C8_color = "#B6B6B6"
    static let common_C9_color = "#DDDDDD"
    static let common_C10_color = "#EAEAEA"
    static let common_C11_color = "#FCFCFC"
    static let common_C12_color = "#FFFFFF"
    static let common_C13_color = "#000000"
    // 颜色为 C1  或者 #E13636 时添加蒙板后的颜色号
    static let common_C100_color = "#CA3030"
    // 颜色为 白色  或者 #FFFFFF 时添加蒙板后的颜色号
    static let common_C101_color = "#E6E6E6"
    static let grayColor = UIColor(red:0.94, green: 0.94, blue: 0.94, alpha: 1)
    
    static let YEAR_START = 1901//滚轮显示的起始年份
    static let YEAR_END = 2049//滚轮显示的结束年份
    static let PICKER_HEIGHT: CGFloat = 260 // 滚动轮的高度
    static let TOOLBAR_HEIGHT: CGFloat = 40 // 滚动轮上方工具栏高度
    
    static let NAVBAR_CHANGE_POINT : CGFloat = 200
    
    
    // 字体大小定义
    static let common_F1_font: CGFloat = 19
    static let common_F2_font: CGFloat = 17
    static let common_F3_font: CGFloat = 16
    static let common_F4_font: CGFloat = 15
    static let common_F5_font: CGFloat = 13
    static let common_F6_font: CGFloat = 11
    static let common_F7_font: CGFloat = 10
    
    // 10中随机色
    static let randomColor = ["#D47661","#D46161","#D49261","#D4A361","#9B6BAF","#D4617D","#6888BA","#D4619A","#61B79C","#5C9FBA"]
    
    //间距
    static let MARGIN:CGFloat = 10
    
    //通知
    static let weixin_share_result = "weixin_share_result"
    static let goods_detail_collection = "goods_detail_collection"
    static let notifationMessageReceiveHandle = "notifation_message_receive"
    static let notifationTouchReceiveHandle = "notifation_touch_receive"
    static let datePickerDoneBtnClick = "datepicker_doneBtnClick"
    static let search_get_placeName = "search_get_placeName"
    static let notification_login_success = "notification_login_success"
    static let shopBasketShouldShowInvalidGoods = "shopBasketShouldShowInvalidGoods"
    static let goodsGenerShouldReloadData = "goodsGenerShouldReloadData"
    static let add_good_to_basket = "add_good_to_basket"
    static let notification_logout_success = "notification_logout_success"
    static let notification_add_gift_reminder_success = "notification_add_gift_reminder_success"
    static let notification_import_contact_success = "notification_import_contact_success"
    static let shop_cart_item_add = "shop_cart_item_add"
    static let shop_cart_item_reduce = "shop_cart_item_reduce"
    static let go_to_shop_cart = "go_to_shop_cart"
    static let cart_selected = "cart_selected"
    static let show_mainTabBarController = "show_mainTabBarController"
    static let show_rootController = "show_rootController"
    static let shopBasketDataReloadFinish = "shopBasketDataReloadFinish"
    static let flash_saleDataReload = "flash_saleDataReload"
    static let tactic_collectnumber = "tactic_collectnumber"
    static let usercenter_changed = "usercenter_changed"
    static let orderList_changed = "orderList_changed"

    //第三方登录
    enum OpenLoginType :Int{
        case wx = 3
        case qq = 2
    }
    
    static let ALPHANUM = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
    
    
    //内部登录触发
    enum InLoginType :Int{
        case goodsGeneraType = 0
        case goodsDetailType
        case chargeType
        case orderType
        case webViewType
        case webViewPackageCollectType
        case webViewTacticDetailCollectType
        case userCenterType
        case accountManagerType
        case shopCartType
        case leaderBoardType
        case dailySignType
        case pointMallType
        case couponType
        case packageItemType
        case packageGuessType
        case consultType
        case commonQuestionType
        case myQuestionType
        case defaultType //没有后续操作可选择此默认项
    }
    
    //分享类型
    enum ShareContentType :Int{
        case mediaType = 0
        case imageType
        case textType
    }
    
    //进入WebViewController的类型
    enum WebViewType :Int{
        case commonType = 0
        case birthWikiType
        case remindDetailType
        case tacticDetailType
        case packageItemType
    }
    
    
    //数据表枚举 注意：枚举值需要与 数据表名称数组 保持正确的映射
    enum CoreDataType :Int{
        case userInfoType = 0//用户信息
        case setInfoType//设置
        case userAddressType//用户地址数据
        case homeAAGiftChoiceType//首页数据和选礼数据
        case basketItemType//首页数据和选礼数据
    }
    
    
    
    //MARK: Theme-主题
    struct Theme {
        static let Color1 = UIColor(rgba: "#FF3A48")
        static let Color2 = UIColor(rgba: "#12171F")
        static let Color3 = UIColor(rgba: "#414955")
        static let Color4 = UIColor(rgba: "#7D8BA4")
        static let Color5 = UIColor(rgba: "#F2F3F5")
        static let Color6 = UIColor(rgba: "#555555")
        static let Color7 = UIColor(rgba: "#999999")
        static let Color8 = UIColor(rgba: "#B6B6B6")
        static let Color9 = UIColor(rgba: "#DDDDDD")
        static let Color10 = UIColor(rgba: "#EAEAEA")
        static let Color11 = UIColor(rgba: "#FCFCFC")
        static let Color12 = UIColor(rgba: "#FFFFFF")
        static let Color13 = UIColor(rgba: "#000000")
        static let Color14 = UIColor(rgba: "#333333")
        static let Color15 = UIColor(rgba: "#F5F5F5")
        static let Color16 = UIColor(rgba: "#F0F0F0")
        static let Color17 = UIColor(rgba: "#666666")
        static let Color18 = UIColor(rgba: "#EEEEEE")
        
        static let ColorSeparatLine = UIColor(rgba: "#EAEAEA")
        
        
        //font_31 数字“31”是在iphone6上的字号
        static var Font_34:UIFont{return UIFont.systemFont(ofSize: self.FontSize_34)}

        static var Font_31:UIFont{return UIFont.systemFont(ofSize: self.FontSize_31)}
        
        static var Font_29:UIFont{return UIFont.systemFont(ofSize: self.FontSize_29)}
                
        static var Font_27:UIFont{return UIFont.systemFont(ofSize: self.FontSize_27)}
        
        static var Font_25:UIFont{return UIFont.systemFont(ofSize: self.FontSize_25)}
        
        static var Font_23:UIFont{return UIFont.systemFont(ofSize: self.FontSize_23)}
        
        static var Font_21:UIFont{return UIFont.systemFont(ofSize: self.FontSize_21)}
        
        static var Font_19:UIFont{return UIFont.systemFont(ofSize: self.FontSize_19)}
        
        static var Font_17:UIFont{return UIFont.systemFont(ofSize: self.FontSize_17)}
        
        static var Font_16:UIFont{return UIFont.systemFont(ofSize: self.FontSize_16)}
        
        static var Font_15:UIFont{return UIFont.systemFont(ofSize: self.FontSize_15)}
        
        static var Font_14:UIFont{return UIFont.systemFont(ofSize: self.FontSize_14)}
        
        static var Font_13:UIFont{return UIFont.systemFont(ofSize: self.FontSize_13)}
        
        static var Font_11:UIFont{return UIFont.systemFont(ofSize: self.FontSize_11)}
        
        static var Font_10:UIFont{return UIFont.systemFont(ofSize: self.FontSize_10)}
        
        
        
        
        //font_31 数字“31”是在iphone6上的字号
        static var FontSize_34:CGFloat{return self.getFontSizeForScreenSizeByScal(34)}
        
        static var FontSize_31:CGFloat{return self.getFontSizeForScreenSizeByScal(31)}
        
        static var FontSize_29:CGFloat{return self.getFontSizeForScreenSizeByScal(29)}
        
        static var FontSize_27:CGFloat{return self.getFontSizeForScreenSizeByScal(27)}
        
        static var FontSize_25:CGFloat{return self.getFontSizeForScreenSizeByScal(25)}
        
        static var FontSize_23:CGFloat{return self.getFontSizeForScreenSizeByScal(23)}
        
        static var FontSize_21:CGFloat{return self.getFontSizeForScreenSizeByScal(21)}
        
        static var FontSize_19:CGFloat{return self.getFontSizeForScreenSizeByScal(19)}
        
        static var FontSize_17:CGFloat{return self.getFontSizeForScreenSizeByScal(17)}
        
        static var FontSize_16:CGFloat{return self.getFontSizeForScreenSizeByScal(16)}
        
        static var FontSize_15:CGFloat{return self.getFontSizeForScreenSizeByScal(15)}
        
        static var FontSize_14:CGFloat{return self.getFontSizeForScreenSizeByScal(14)}
        
        static var FontSize_13:CGFloat{return self.getFontSizeForScreenSizeByScal(13)}
        
        static var FontSize_11:CGFloat{return self.getFontSizeForScreenSizeByScal(11)}
        
        static var FontSize_10:CGFloat{return self.getFontSizeForScreenSizeByScal(10)}
        
        
        /**
         通过确定iphone6(s)上的的字号，来确定其它设备上的字号
         默认iphone6plus的字号比iphone6(s)上的大一号
         iphone5/5s/se/4s/4 上的设备字号比iphone(s)上的字号小一号
         */
        ///定好phone6的文字尺寸，剩下的都是递减量 返回font
        static func getFontForScreenSizeByScal(_ iph6Font:CGFloat,iph6pScal:CGFloat = -2,iph5Scal:CGFloat = 2,iph4Scal:CGFloat = 2)->UIFont{
            return UIFont.systemFont(ofSize: self.getFontSizeForScreenSizeByScal(iph6Font, iph6pScal: iph6pScal, iph5Scal: iph5Scal, iph4Scal: iph4Scal))
        }
        
        //定好phone6的文字尺寸，剩下的都是递减量 返回font size
        static func getFontSizeForScreenSizeByScal(_ iph6Font:CGFloat,iph6pScal:CGFloat = -2,iph5Scal:CGFloat = 2,iph4Scal:CGFloat = 2)->CGFloat{
            return self.getFontForScreenSize(iph6Font-iph6pScal, iph6: iph6Font, iph5: iph6Font-iph5Scal, iph4: iph6Font-iph4Scal)
        }
        
        //依次为：iphone6p,iphone6,iphone5(5s,se),iphone4(s)
        static func getFontForScreenSize(_ iph6p:CGFloat,iph6:CGFloat,iph5:CGFloat,iph4:CGFloat)->CGFloat{
            
            var font:CGFloat = 0
            
            if DeviceType.IS_IPHONE_6P{
                font = iph6p
            }
            else if DeviceType.IS_IPHONE_6{
                font = iph6
            }
            else if DeviceType.IS_IPHONE_5{
                font = iph5
            }
            else if DeviceType.IS_IPHONE_4_OR_LESS{
                font = iph4
            }
            
            //最小字号为：7
            return font > 0 ? font : 7
        }
    }
    /*
     private let tipe_nodata_article = "报告大王，攻略被妖精抓走了!"
     private let tipe_nodata_goods = "报告大王，礼品被妖精抓走了!"
     */
    struct TipeWords {
        static let nullData_article = "报告大王，攻略被妖精抓走了!"
        static let nullData_goods = "报告大王，礼品被妖精抓走了!"
        static let noMoreData = "已显示全部内容"
        
        //订单
        static let myOrder_all_nullData = "您还没有订单哦!"
        static let myOrder_waitCheck_nullData = "您还没有待收货订单哦!"
        static let myOrder_done_nullData = "您还没有已完成订单哦!"
        static let myOrder_waitPay_nullData = "您还没有待付款订单哦!"
        
        //红包
        static let myCoupon_useable_nullData = "你还没有未使用的红包"
        static let myCoupon_overDue_nullData = "你还没有已使用的红包"
        static let myCoupon_used_nullData = "你还没有已过期的红包"
    }
    
    
    struct TipeIcon {
        
        static let nullData_article = "search_biaoQing"
        
        static let nullData_goods = "search_biaoQing"
        //订单
        static let myOrder_nullData = "search_biaoQing"
        //红包
        static let myCoupon_nullData = "search_biaoQing"
    }
    
    //数据表名称数组
    static let coreDataTableEntityNames = ["User","SettingInfo","UserAddress","HomeAAGiftChoice"]
    
    
    struct APP_LCA
    {
        //通知列表
        static let WillEnterForeground = "applicationWillEnterForeground"//即将进入前台
        static let DidBecomeActive = "applicationDidBecomeActive"//进入前台活动状态
        
        static let WillResignActive = "applicationWillResignActive"//即将进入非活动状态
        static let DidEnterBackground = "applicationDidEnterBackground"//进入后台
        
        static let WillTerminate = "applicationWillTerminate"//即将被销毁
        
    }
    
    
    static let IS_IOS7: Bool = !IS_IOS8
    
    static let IS_IOS8: Bool = floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_7_1
    
    
    //app：1.3.7以前的标尺
    struct ScreenSize
    {
        static let SCREEN_BOUNDS:CGRect     = UIScreen.main.bounds
        static let SCALE_SCREEN:CGFloat     = UIScreen.main.scale
        static let SCREEN_WIDTH:CGFloat     = UIScreen.main.bounds.size.width
        static let SCREEN_HEIGHT:CGFloat    = UIScreen.main.bounds.size.height
        
        static let SCREEN_MAX_LENGTH:CGFloat = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        static let SCREEN_MIN_LENGTH:CGFloat = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        
        static let SCREEN_HEIGHT_SCALE:CGFloat  = ScreenSize.SCREEN_HEIGHT / 736
        static let SCREEN_WIDTH_SCALE:CGFloat   = ScreenSize.SCREEN_WIDTH / 414
    }
    
    
    //app：1.3.7+  iphone6s尺寸： 750x1334 （一倍图：375x667 设计图在一倍图上标尺）
    struct ScreenSizeV1
    {
        static let SCALE_SCREEN     = UIScreen.main.scale
        static let SCREEN_WIDTH     = UIScreen.main.bounds.size.width
        static let SCREEN_HEIGHT    = UIScreen.main.bounds.size.height
        
        static let SCREEN_MAX_LENGTH:CGFloat    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        static let SCREEN_MIN_LENGTH:CGFloat    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        static let SCREEN_WIDTH_SCALE:CGFloat   = ScreenSize.SCREEN_WIDTH / 375
        static let SCREEN_HEIGHT_SCALE:CGFloat  = ScreenSize.SCREEN_HEIGHT / 667
        
        static let SCREEN_BOUNDS: CGRect = UIScreen.main.bounds
        
        //各种间距
        static let MARGIN_4:CGFloat    = 4 * SCREEN_WIDTH_SCALE
        static let MARGIN_8:CGFloat    = 8  * SCREEN_WIDTH_SCALE
        static let MARGIN_10:CGFloat   = 10 * SCREEN_WIDTH_SCALE
        static let MARGIN_13:CGFloat   = 13 * SCREEN_WIDTH_SCALE
        static let MARGIN_15:CGFloat   = 15 * SCREEN_WIDTH_SCALE
        static let MARGIN_20:CGFloat   = 20 * SCREEN_WIDTH_SCALE
        static let MARGIN_30:CGFloat   = 30 * SCREEN_WIDTH_SCALE
        static let MARGIN_44:CGFloat   = 44 * SCREEN_WIDTH_SCALE
        static let MARGIN_49:CGFloat   = 49 * SCREEN_WIDTH_SCALE
        static let MARGIN_64:CGFloat   = 49 * SCREEN_WIDTH_SCALE
        static let LINE_HEIGHT:CGFloat   = 0.5 * SCREEN_WIDTH_SCALE
    }
    
    
    //app：1.3.8+  iphone6s尺寸： 750x1334
    struct ScreenSizeV2
    {
        static let SCALE_SCREEN     = UIScreen.main.scale
        static let SCREEN_WIDTH     = UIScreen.main.bounds.size.width
        static let SCREEN_HEIGHT    = UIScreen.main.bounds.size.height
        
        static let SCREEN_MAX_LENGTH:CGFloat    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        static let SCREEN_MIN_LENGTH:CGFloat    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        static let SCREEN_WIDTH_SCALE:CGFloat   = ScreenSize.SCREEN_WIDTH / 750
        static let SCREEN_HEIGHT_SCALE:CGFloat  = ScreenSize.SCREEN_HEIGHT / 1334
        
        static let SCREEN_BOUNDS: CGRect = UIScreen.main.bounds
        
        //各种间距
        static let MARGIN_4:CGFloat    = 4 * SCREEN_WIDTH_SCALE
        static let MARGIN_8:CGFloat    = 8  * SCREEN_WIDTH_SCALE
        static let MARGIN_10:CGFloat   = 10 * SCREEN_WIDTH_SCALE
        static let MARGIN_13:CGFloat   = 13 * SCREEN_WIDTH_SCALE
        static let MARGIN_15:CGFloat   = 15 * SCREEN_WIDTH_SCALE
        static let MARGIN_16:CGFloat   = 16 * SCREEN_WIDTH_SCALE
        static let MARGIN_20:CGFloat   = 20 * SCREEN_WIDTH_SCALE
        static let MARGIN_26:CGFloat   = 26 * SCREEN_WIDTH_SCALE
        static let MARGIN_30:CGFloat   = 30 * SCREEN_WIDTH_SCALE
        static let MARGIN_36:CGFloat   = 36 * SCREEN_WIDTH_SCALE
        static let MARGIN_38:CGFloat   = 38 * SCREEN_WIDTH_SCALE
        static let MARGIN_40:CGFloat   = 40 * SCREEN_WIDTH_SCALE
        static let MARGIN_44:CGFloat   = 44 * SCREEN_WIDTH_SCALE
        static let MARGIN_49:CGFloat   = 49 * SCREEN_WIDTH_SCALE
        static let MARGIN_60:CGFloat   = 60 * SCREEN_WIDTH_SCALE
        static let MARGIN_64:CGFloat   = 64 * SCREEN_WIDTH_SCALE
        static let MARGIN_68:CGFloat   = 68 * SCREEN_WIDTH_SCALE
        static let MARGIN_150:CGFloat   = 150 * SCREEN_WIDTH_SCALE

        static let LINE_HEIGHT:CGFloat   = 0.5 * SCREEN_WIDTH_SCALE
    }
    
    
    struct DeviceType
    {
        static let IS_IPHONE_4_OR_LESS =  UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
        static let IS_IPHONE_5 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
        static let IS_IPHONE_6 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
        static let IS_IPHONE_6P = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    }
    
}


let MARGIN_4:CGFloat    = 4
let MARGIN_8:CGFloat    = 8
let MARGIN_13:CGFloat   = 13
let MARGIN_10:CGFloat   = 10
let MARGIN_20:CGFloat   = 20
let MARGIN_30:CGFloat   = 30
let MARGIN_44:CGFloat   = 44
let MARGIN_49:CGFloat   = 49
let MARGIN_64:CGFloat   = 64
let LINE_HEIGHT:CGFloat   = 0.5
