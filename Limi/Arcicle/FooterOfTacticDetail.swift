//
//  FooterOfTacticDetail.swift
//  Limi
//
//  Created by maohs on 16/7/19.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class FooterOfTacticDetail: BaseUnitView {
    
    var footerHeight:CGFloat = 0
    
    fileprivate let titleLabel:UILabel = {
        let lbl = UILabel()
        lbl.font = Constant.Theme.Font_15
        lbl.textColor = UIColor(rgba: "#333333")
        lbl.textAlignment = .center
        lbl.text = "相关推荐"
        return lbl
    }()
    
    fileprivate let leftLineView:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(rgba: Constant.common_C8_color)
        return view
    }()
    
    fileprivate let rightLineView:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(rgba: Constant.common_C8_color)
        return view
    }()
    
   var collectionView:CommonCollectionView!
    
    func reloadData(_ goodsArray:[JSON]){
        self.recommandGoodsArray = goodsArray
        if recommandGoodsArray.count > 0 {
            updateUI()
            
        }
    }
    
    fileprivate var recommandGoodsArray = [JSON]()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        collectionView = CommonCollectionView.newCommonCollectionView(userVC:nil)
        
        for view in [leftLineView,titleLabel,rightLineView,collectionView] {
            view.clipsToBounds = true
            self.addSubview(view)
        }
        
        
    }
    
    func updateUI() {
        
        titleLabel.frame = CGRect(x: 0, y: 80 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE, width: 70, height: 15)
        titleLabel.icenterX = Constant.ScreenSizeV2.SCREEN_WIDTH / 2
        
        leftLineView.frame = CGRect(x: titleLabel.ileft - 45, y: titleLabel.icenterY, width: 40, height: 1)
        rightLineView.frame = CGRect(x: titleLabel.iright + 5, y: titleLabel.icenterY, width: 40, height: 1)
        
        let height = CGFloat(ceilf(Float(self.recommandGoodsArray.count) / 2)) * (Constant.ScreenSizeV2.SCREEN_WIDTH / 2 + 62)
        self.collectionView.frame = CGRect(x: 0, y: titleLabel.ibottom + 30 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: height)
        self.collectionView.goodsList = self.recommandGoodsArray
        self.collectionView.collectionView.isScrollEnabled = false
        self.collectionView.collectionView.iheight = self.collectionView.iheight
        
        self.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: self.collectionView.ibottom)
        self.footerHeight = self.iheight
        
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
