//
//  TacticListViewController.swift
//  Limi
//
//  Created by maohs on 16/10/8.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class ServiceListViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {

    let tableview = UITableView()
    
    private let dataArray = [
        ["service_remind_left","service_remind","送礼提醒","帮你记重要日子"],
        ["service_consult_left","service_consult","在线礼物顾问","解决您的送礼烦恼"],
        ["service_packing_left","service_packing","礼品包装服务","送TA不一样的心意"]]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.title = "服务"
        
        self.view.addSubview(tableview)
        tableview.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: Constant.ScreenSizeV2.SCREEN_HEIGHT - 64 - 44)
        tableview.dataSource = self
        tableview.delegate = self
        tableview.backgroundColor = UIColor.clear
        tableview.backgroundView = nil
        tableview.separatorStyle = .none
        tableview.rowHeight = ServiceListCell.height
        tableview.tableFooterView = UIView()
        tableview.showsVerticalScrollIndicator = false

        tableview.register(ServiceListCell.self, forCellReuseIdentifier: ServiceListCell.reuseIdentifier)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: UITableView DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell: ServiceListCell = tableView.dequeueReusableCell(withIdentifier: ServiceListCell.reuseIdentifier, for: indexPath) as! ServiceListCell
        cell.reloadData(data: self.dataArray[indexPath.row])
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            Statistics.count(Statistics.Service.service_remind_click)
            let remindVc = RemindViewController()
            remindVc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(remindVc, animated: true)
        }else if indexPath.row == 1 {
            if UserInfoManager.didLogin {
                Statistics.count(Statistics.Service.service_online_click)
                let giftAdvisor = GiftAdvisorViewController()
                giftAdvisor.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(giftAdvisor, animated: true)
            }else {
                self.jumpToLogin(Constant.InLoginType.consultType, tipe: nil)
            }
            
        }else {
            if let url = SystemInfoRequestManager.shareInstance().service_list_introduction{
                Statistics.count(Statistics.Service.service_package_click, andAttributes:["package":url])
                let webVC = WebViewController(URL: URL(string: "http://m.xydd.co/shop/articleitem?id=1433")!)
                webVC.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(webVC, animated: true)
            }
        }
    }
    
}
