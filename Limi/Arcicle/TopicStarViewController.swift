//
//  TopicStarViewController.swift
//  Limi
//
//  Created by 程巍巍 on 6/24/15.
//  Copyright (c) 2015 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class TopicStarViewController: BaseViewController {
    
    var tableView = UITableView()
    var listSource = [JSON]()
    var coverSource = JSON(NSNull())
    
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        tableView.delegate = nil
        tableView.dataSource = nil
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "星品"
        
        self.view.addSubview(tableView)
        tableView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.view)
            let _ = make.bottom.equalTo(self.view)
            let _ = make.left.equalTo(self.view)
            let _ = make.right.equalTo(self.view)
        }
        
        tableView.register(Cover.self, forCellReuseIdentifier: "Cover")
        tableView.register(ListCell.self, forCellReuseIdentifier: "ListCell")
        tableView.separatorStyle = .none
        
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        weak var weakSelf = self as TopicStarViewController
        
        tableView.addHeaderRefresh { () -> Void in
            
           gcd.async(.default) {
                weakSelf?.loadList(0)
            }
        }
        
        
        tableView.addFooterRefresh { () -> Void in
            
           gcd.async(.default) {
                if let id = weakSelf?.listSource.last?["Id"].int {
                    weakSelf?.loadList(id)
                }
            }
        }
        
        tableView.beginHeaderRefresh()
        
        
        loadLayout()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // 加载布局
    func loadLayout() {
        let _ = LMRequestManager.requestByDataAnalyses(HTTPMethod.get, context:self.view,URLString: Constant.JTAPI.layout + "/star",isShowErrorStatuMsg:true, successHandler: {data, status, msg in
            if status == 200 {
                self.coverSource = JSON(data as AnyObject)["data"]["layout"][0]
                self.tableView.reloadData()
            }
        })
        
    }
    
    func loadList(_ lastId: Int) {
        let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.article_list, isNeedUserTokrn: true,isShowErrorStatuMsg: true,successHandler: {data, status, msg in
            self.tableView.closeAllRefresh()
            if status == 200 {
                if let list = JSON(data as AnyObject)["data"]["list"].array {
                    // 如果 lastId == 0 表示刷新 否则是追加数据
                    if lastId == 0 {
                        self.listSource = list
                        self.tableView.reloadData()
                    }else{
                        if list.count == 0 {
                            if let text : String = data["msg"] as? String{
                                Utils.showError(context: self.view, errorStr: text)
                            }
                        }else{
                            for json in list {
                                self.listSource.append(json)
                            }
                            self.tableView.reloadData()
                        }
                    }
                }
                else{
                    if self.listSource.count > 0{
                        self.tableView.showTipe(UIScrollTipeMode.noMuchMore)
                    }
                }
            }
        })
        
        
    }
}

extension TopicStarViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return listSource.count + 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellType: TableCell.Type = (indexPath as NSIndexPath).section == 0 ? Cover.self : ListCell.self
        return cellType.heightForWidth(tableView.frame.width)
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 8
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: (indexPath as NSIndexPath).section == 0 ? "Cover" : "ListCell", for: indexPath) as! TableCell
        cell.data = ((indexPath as NSIndexPath).section == 0 ? coverSource : listSource[(indexPath as NSIndexPath).section - 1])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if (indexPath as NSIndexPath).section == 0 {
            if let href = coverSource["attributes"]["href"].string {
                EventDispatcher.dispatch(href, onNavigationController: self.navigationController)
            }
        }else {
            if let id = listSource[(indexPath as NSIndexPath).section - 1]["Id"].int {
                let href = "http://wechat.giftyou.me/v1/article/article_item?id=\(id)&app=1"
                EventDispatcher.dispatch(href, onNavigationController: self.navigationController)
            }
        }
    }
    
}

extension TopicStarViewController {
    class TableCell: UITableViewCell {
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)
            self.selectionStyle = .none
            defaultInit()
        }
        
        required init(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func defaultInit() {
            
        }
        var data: JSON?
        
        class func heightForWidth(_ width: CGFloat)->CGFloat {
            return 0
        }
    }
    class Cover: TableCell {
        var imageCover = UIImageView()
        
        override class func heightForWidth(_ width: CGFloat)->CGFloat {
            return width * 0.5
        }
        override var data: JSON? {
            didSet {
                if let src = data?["attributes"]["src"].string {
                    let imageScale : String = src + "?imageView2/0/w/" + "\(Int(Constant.ScreenSize.SCREEN_WIDTH*Constant.ScreenSize.SCALE_SCREEN))" + "/h/" + "\(Int(Constant.ScreenSize.SCREEN_WIDTH*0.50*Constant.ScreenSize.SCALE_SCREEN))"
                    
                    imageCover.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "loadingDefault"))
                }
            }
        }
        
        override func defaultInit() {
            self.backgroundColor = UIColor(rgba: Constant.common_background_color)
            
            self.contentView.addSubview(imageCover)
            
            imageCover.translatesAutoresizingMaskIntoConstraints = false

            imageCover.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(self.contentView)
                let _ = make.right.equalTo(self.contentView)
                let _ = make.top.equalTo(self.contentView)
                let _ = make.bottom.equalTo(self.contentView)
            }
        }
    }
    
    class ListCell: TableCell {
        var content = UIView()
        var imageCover = UIImageView()
        var titleLabel = UILabel()
        var seperatorLine = UIView()
        var subTitleLabel = UILabel()
        
        override var data: JSON? {
            didSet {
                if let src = data?["Cover"].string {
                    let imageScale : String = src + "?imageView2/0/w/" + "\(Int((Constant.ScreenSize.SCREEN_WIDTH - 20)*Constant.ScreenSize.SCALE_SCREEN))" + "/h/" + "\(Int((Constant.ScreenSize.SCREEN_WIDTH - 20)*0.55*Constant.ScreenSize.SCALE_SCREEN))"
                    imageCover.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "loadingDefault"))
                }
                if let title = data?["Title"].string {
                    titleLabel.attributedText = NSAttributedString(string: "<t font='\(Constant.CustomFont.DefaultFontName)' size='15' color='Black' stroke='-0.5'>\(title)</t>")
                }else{
                    titleLabel.attributedText = nil
                }
                
                if let title = data?["Sub_title"].string {
                    subTitleLabel.attributedText = NSAttributedString(string: "<t font='\(Constant.CustomFont.DefaultFontName)' size='13'>\(title)</t>")
                }else{
                    subTitleLabel.attributedText = nil
                }
            }
        }
        
        override class func heightForWidth(_ width: CGFloat)->CGFloat {
            return width * 0.5 + 8 + 19 + 26
        }
        
        override func defaultInit() {
            
            self.backgroundColor = UIColor.clear
            content.backgroundColor = UIColor.white
            
            self.contentView.addSubview(content)
            content.translatesAutoresizingMaskIntoConstraints = false

            content.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(self.contentView).offset(10)
                let _ = make.right.equalTo(self.contentView).offset(10)
                let _ = make.top.equalTo(self.contentView)
                let _ = make.bottom.equalTo(self.contentView)
            }
            seperatorLine.backgroundColor = UIColor(rgba: "#CCCCCC")
            subTitleLabel.textColor = UIColor(rgba:"#656565")
            
            for view in [imageCover, titleLabel, seperatorLine, subTitleLabel] {
                content.addSubview(view)
            }
            

            imageCover.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(content)
                let _ = make.right.equalTo(content)
                let _ = make.top.equalTo(content)
                let _ = make.height.equalTo(imageCover.snp_width).multipliedBy(0.5)
            }
            
            titleLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.centerX.equalTo(content)
                let _ = make.top.equalTo(imageCover.snp_bottom).offset(6)
            }
            seperatorLine.snp_makeConstraints { (make) -> Void in
                let _ = make.centerX.equalTo(content)
                let _ = make.top.equalTo(titleLabel.snp_bottom).offset(4)
                let _ = make.width.equalTo(titleLabel)
                let _ = make.height.equalTo(0.5)
            }

            subTitleLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.centerX.equalTo(content)
                let _ = make.top.equalTo(seperatorLine.snp_bottom).offset(6)
            }
        }
    }
}
