//
//  TopicCustomViewController.swift
//  Limi
//
//  Created by 程巍巍 on 6/24/15.
//  Copyright (c) 2015 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class TopicCustomViewController: BaseViewController {

    
    var topicView: TopicView! {
        return self.view as! TopicView
    }
    override func loadView() {
        super.loadView()
        self.view = TopicView()
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "定制"
        
        loadLayout()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var layoutSource: [JSON]! {
        didSet{
            for layout in layoutSource {
                if let cls = layout["attributes"]["class"].string {
                    if cls == "img" {
                        if let src = layout["attributes"]["src"].string {
                            let imageScale : String = src + "?imageView2/0/w/" + "\(Int(Constant.ScreenSize.SCREEN_WIDTH*Constant.ScreenSize.SCALE_SCREEN))" + "/h/" + "\(Int(Constant.ScreenSize.SCREEN_WIDTH*0.50*Constant.ScreenSize.SCALE_SCREEN))"
                            
                            topicView.imageCover.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "loadingDefault"))
                        }
                    }else if cls == "goods_item" {
                        if let goods = layout["children"].array {
                            topicView.goods = goods
                            topicView.nav = self.navigationController!
                        }
                    }
                }
            }
        }
    }

    // 加载布局
    func loadLayout() {

        let _ = LMRequestManager.requestByDataAnalyses(HTTPMethod.get,context:self.view,URLString:  Constant.JTAPI.layout + "/custom", isShowErrorStatuMsg:true,isNeedHud : true,successHandler: {data, status, msg in

            if status == 200
            {
                if let list = JSON(data as AnyObject)["data"]["layout"].array {
                    self.layoutSource = list
                }
            }
            

        })
        
    }
}

extension TopicCustomViewController {
    class TopicView: BaseView {
        var nav : UINavigationController = UINavigationController()
        var scrollView = UIScrollView()
        var imageCover = UIImageView()
        var contentView = UIView()
        
        class ImageView: UIImageView {
            var data: JSON?
        }
        
        var goods: [JSON]! {
            didSet {
                var lastView: UIView = imageCover
                for good in goods {
                    let imageview = ImageView()
                    imageview.data = good
                    if let src = good["attributes"]["src"].string {
                        let imageScale : String = src + "?imageView2/0/w/" + "\(Int((Constant.ScreenSize.SCREEN_WIDTH - 20)*Constant.ScreenSize.SCALE_SCREEN))" + "/h/" + "\(Int((Constant.ScreenSize.SCREEN_WIDTH - 20)*0.55*Constant.ScreenSize.SCALE_SCREEN))"
                        imageview.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "loadingDefault"))
                    }
                    if let _ = good["attributes"]["href"].string {
                        imageview.isUserInteractionEnabled = true
                        imageview.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(TopicView.imageViewTaped(_:))))
                    }
                    
                    if let title = good["attributes"]["title"].string {
                        let titleLabel = UILabel()
                        titleLabel.textColor = UIColor.white
                        titleLabel.font = Constant.CustomFont.Default(size: 19)
                        titleLabel.text = title
                        imageview.addSubview(titleLabel)
                        titleLabel.snp_makeConstraints(closure: { (make) -> Void in
                            let _ = make.centerX.equalTo(imageview)
                            let _ = make.bottom.equalTo(imageview.snp_centerY).offset(-2)
                        })
                    }
                    
                    if let title = good["attributes"]["English_name"].string {
                        let titleLabelEN = UILabel()
                        titleLabelEN.textColor = UIColor.white
                        titleLabelEN.font = Constant.CustomFont.Default(size: 15)
                        titleLabelEN.text = title
                        imageview.addSubview(titleLabelEN)
                        titleLabelEN.snp_makeConstraints(closure: { (make) -> Void in
                            let _ = make.centerX.equalTo(imageview)
                            let _ = make.top.equalTo(imageview.snp_centerY).offset(2)
                        })
                    }
                    
                    contentView.addSubview(imageview)

                    imageview.snp_makeConstraints(closure: { (make) -> Void in
                        let _ = make.left.equalTo(contentView).offset(8)
                        let _ = make.right.equalTo(contentView).offset(8)
                        let _ = make.top.equalTo(lastView.snp_bottom).offset(8)
                        let _ = make.height.equalTo(imageview.snp_width).multipliedBy(0.55)
                    })
                    
                    lastView = imageview
                }
                if lastView != imageCover {
                    lastView.snp_makeConstraints(closure: { (make) -> Void in
                        let _ = make.bottom.equalTo(contentView)
                    })
                }
            }
        }
        
        @objc func imageViewTaped(_ sender: UITapGestureRecognizer) {
            if let href = (sender.view as? ImageView)?.data?["attributes"]["href"].string {
                var params = [String: AnyObject]()
                if let title = (sender.view as? ImageView)?.data?["attributes"]["title"].string {
                    params["_title"] = title as AnyObject?
                }
                EventDispatcher.dispatch(href, params: params, onNavigationController: self.nav)
            }
        }
        override func defaultInit() {
            
            self.addSubview(scrollView)
            
            scrollView.showsHorizontalScrollIndicator = false
            scrollView.showsVerticalScrollIndicator = false
            
            scrollView.addSubview(contentView)
            contentView.addSubview(imageCover)
            
            scrollView.translatesAutoresizingMaskIntoConstraints = false

            scrollView.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(self)
                let _ = make.right.equalTo(self)
                let _ = make.top.equalTo(self)
                let _ = make.bottom.equalTo(self)
            }

            contentView.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(scrollView)
                let _ = make.right.equalTo(scrollView)
                let _ = make.top.equalTo(scrollView)
                let _ = make.bottom.equalTo(scrollView)
            }
            
            contentView.snp_makeConstraints { (make) -> Void in
                let _ = make.width.equalTo(scrollView)
                let _ = make.height.equalTo(scrollView)
            }


            imageCover.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(contentView)
                let _ = make.right.equalTo(contentView)
                let _ = make.top.equalTo(contentView)
                let _ = make.height.equalTo(imageCover.snp_width).multipliedBy(0.5)
                let _ = make.bottom.equalTo(contentView)
            }
        }
    }
}
