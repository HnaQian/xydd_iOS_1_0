//
//  TopicViewController.swift
//  Limi
//
//  Created by 程巍巍 on 5/13/15.
//  Copyright (c) 2015 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class TopicViewController: BaseViewController {
    
    var tableView = BaseTableView()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationItem.title = "专题"
        
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        
        self.view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "|-0-[tb]-0-|", options:[], metrics: nil, views: ["tb": tableView]))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[tb]-0-|", options:[], metrics: nil, views: ["tb": tableView]))
        
        tableView.cellClassMap = ["topic_index_list_cell": TopicIndexTableViewCell.self]
        tableView.separatorStyle = .none
        
//        tableView.dataArray = [
//            ["type": "topic_index_list_cell",
//            "src":"http://c.hiphotos.baidu.com/image/w%3D310/sign=91f5d51fcdfc1e17fdbf8a307a91f67c/10dfa9ec8a136327b08ca03a938fa0ec08fac752.jpg"]
//        ]
//        
//        for _ in 0...10 {
//            tableView.dataArray?.append(["type": "topic_index_list_cell",
//                "src":"http://c.hiphotos.baidu.com/image/w%3D310/sign=91f5d51fcdfc1e17fdbf8a307a91f67c/10dfa9ec8a136327b08ca03a938fa0ec08fac752.jpg"]
//            )
//        }
        
        tableView.cellSelectedHandler = {
            (cell: BaseTableViewCell, tableView: BaseTableView)->Void in
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension TopicViewController {
    class TopicIndexTableViewCell: BaseTableViewCell {
        var ImageView = UIImageView()
        
        override class func heightWithData(_ data: [String: AnyObject], width: CGFloat)-> CGFloat{
            return width * 0.6
        }
        
        override func reloadDataSource(_ data:BaseDataMode) {
            
//            if let url = data["src"] as? String {
//                let imageScale : String = url + "?imageView2/0/w/" + "\(Int(Constant.ScreenSize.SCREEN_WIDTH*Constant.ScreenSize.SCALE_SCREEN))" + "/h/" + "\(Int(Constant.ScreenSize.SCREEN_WIDTH*0.60*Constant.ScreenSize.SCALE_SCREEN))"
//                
//                ImageView.af_setImageWithURL(NSURL(string: imageScale)!, placeholderImage: UIImage(named: "loadingDefault"))
//            }
        }
        
        override func defaultInit() {
            self.contentView.addSubview(ImageView)
            ImageView.contentMode = UIViewContentMode.scaleToFill
            
            ImageView.translatesAutoresizingMaskIntoConstraints = false
            self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "|-8-[asiv]-8-|", options: [], metrics: nil, views: ["asiv": ImageView]))
            self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-4-[asiv]-4-|", options: [], metrics: nil, views: ["asiv": ImageView]))
        }
        
    }
}
