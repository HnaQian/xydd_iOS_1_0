//
//  TacticViewController.swift
//  Limi
//
//  Created by 程巍巍 on 7/22/15.
//  Copyright (c) 2015 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import Foundation
// 攻略界面

class TacticViewController: BaseViewController,TacticDetailDelegate {

    
    var tableViewDataSource: [JSON] = []
    
    var code_article = "allcolumn"
    
    var shouldSetHttpTitle = false//是否用网络获取的title设置为当前页面的title
    
    var tabView: CommonTableView!
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        if self.shouldSetHttpTitle == false
        {
            self.navigationItem.title = "攻略"
            
            self.navigationItem.leftBarButtonItem = nil
        }

    }
   
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setupUI()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setupUI()
    {
        tabView = CommonTableView.newCommonTableView(tableViewDataSource, type: ArticleFavType.fav, userVC: self)

        self.view.addSubview(tabView)
        
        tabView.snp_makeConstraints { (make) -> Void in
            
            let _ = make.top.equalTo(self.view)
            let _ = make.left.equalTo(self.view)
            let _ = make.right.equalTo(self.view)
            
            if self.shouldSetHttpTitle == false{
                let _ = make.bottom.equalTo(self.view).offset(-49)
            }
            else{
                let _ = make.bottom.equalTo(self.view)
            }
        }
        
        
        
        weak var weakSelf = self as TacticViewController
        
        tabView.tableView.addHeaderRefresh { () -> Void in
            
           gcd.async(.default) {
                weakSelf?.tableViewDataSource = []
                weakSelf?.loadData()
            }
            
        }

        
        tabView.tableView.addFooterRefresh { () -> Void in
           gcd.async(.default) {
                weakSelf?.loadData()
            }
        }
        

        tabView.tableView.beginHeaderRefresh()
        
        
    }
    
    func loadData() {
        
        var params = [String: AnyObject]()
        if let lastid = tableViewDataSource.last?["Id"].int {
            params["last_id"] = lastid as AnyObject?
        }
        
        params["size"] = 10 as AnyObject?

        params["code"] = code_article as AnyObject?

       let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.article_list, parameters: params, isNeedUserTokrn: true,isShowErrorStatuMsg: true, isNeedHud: false, successHandler: {data, status, msg in
            self.tabView.tableView.closeAllRefresh()
            if status == 200 {
                if let list = JSON(data as AnyObject)["data"]["list"].array {
                    for item in list {
                        self.tableViewDataSource.append(item)
                    }
                    self.tabView.articleList = self.tableViewDataSource
                }
                else{
                    if self.tableViewDataSource.count > 0{
                        
                        self.tabView.tableView.showTipe(UIScrollTipeMode.noMuchMore)
                    }
                }
                
                if self.shouldSetHttpTitle == true
                {
                    self.title = JSON(data as AnyObject)["data"]["title"].string
                }
            }
        })
    }
    
  
}


