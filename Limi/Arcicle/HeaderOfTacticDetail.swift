//
//  HeaderOfTacticDetail.swift
//  Limi
//
//  Created by maohs on 16/7/19.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

@objc protocol HeaderOfTacticDetailDelegate:NSObjectProtocol {
   @objc optional func horizontalTableViewClicked(_ gid:Int,index:Int)
   @objc optional func headerOfTacticDetailCollectButtonClicked()
}

class HeaderOfTacticDetail: BaseUnitView {
    
    weak var headerDelegate:HeaderOfTacticDetailDelegate?
    
    fileprivate var gap_height:CGFloat = 20 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
    
    fileprivate let imageView:UIImageView = {
        let image = UIImageView()
        image.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: 434 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
        image.clipsToBounds  = true
        image.contentMode = UIViewContentMode.scaleAspectFill
        return image
    }()
    
    fileprivate let headerBackView:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    fileprivate let gapView = UIView()
    
    var _imageView:UIImageView{return imageView}
    
    fileprivate let titleLabel:UILabel = {
       let lbl = UILabel()
        lbl.font = Constant.Theme.Font_13
        lbl.textColor = UIColor(rgba: Constant.common_C6_color)
        return lbl
    }()
    
    let footView:FooterBackView = {
        let view = FooterBackView()
        
        return view
    }()
    
    fileprivate var horTableView:LMHorizontalTableView!
    
    var goods_array:[TacticDetailHeaderMode]?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }
    
    override func setupUI() {
        
        self.addSubview(imageView)
        self.addSubview(headerBackView)
        self.addSubview(gapView)
        self.addSubview(footView)
        
        self.frame = imageView.frame
        
        let scale = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        
        titleLabel.frame = CGRect(x: 30 * scale,y: 30 * scale, width: imageView.iwidth, height: 15)
        
        if horTableView == nil {
            horTableView = LMHorizontalTableView(frame:CGRect(x: 30 * scale,y: titleLabel.ibottom + 20 * scale,width: Constant.ScreenSize.SCREEN_WIDTH - 30 * scale,height: TacticDetailHeaderCell.cell_height))
            horTableView!.delegate = self
            horTableView!.dataSource = self
            for view in [titleLabel,horTableView!] as [UIView] {
                view.clipsToBounds = true
                headerBackView.addSubview(view )
            }
        }
        
        headerBackView.frame = CGRect(x: 0, y: imageView.ibottom, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: horTableView!.frame.maxY + gap_height)
        gapView.frame = CGRect(x: 0, y: headerBackView.ibottom, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: gap_height)

        headerBackView.isHidden = true
        gapView.isHidden = true
        footView.isHidden = true
        
        self.footView.collectButtonHandle = {
            self.headerDelegate?.headerOfTacticDetailCollectButtonClicked!()
        }
    
    }
    
    func updateImageHeight(_ height:CGFloat) {
        imageView.iheight = height
        self.iheight = imageView.iheight
    }
    
    //type 1native   2 dispatch
    func updateImageView(_ urlString:String,type:Int) {
        imageView.iheight = Constant.ScreenSizeV2.SCREEN_HEIGHT - 64
        let imageScale : String = urlString + "?imageView2/0/w/" + "\(Int(imageView.iwidth * Constant.ScreenSizeV2.SCALE_SCREEN))" + "/h/" + "\(Int(imageView.iheight * Constant.ScreenSizeV2.SCALE_SCREEN))"
        imageView.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "Default_tactic"))
        
        if type == 1 {
            self.iheight = imageView.iheight
        }else {
            headerBackView.itop = imageView.iheight
            gapView.itop = headerBackView.ibottom
            self.iheight = gapView.ibottom
        }
        
    }
    
    func reloadHeaderData(_ data:[TacticDetailHeaderMode]?) {
        self.goods_array = data
        if self.goods_array!.count > 0 {
            titleLabel.text = "\(self.goods_array!.count)件礼物:"
            headerBackView.itop = imageView.iheight
            gapView.itop = headerBackView.ibottom
            self.iheight = gapView.ibottom
            self.headerBackView.isHidden = false
            self.gapView.isHidden = false
            horTableView!.reloadData()
        }
    }
    
    func reloadFooterData(_ data:JSON?) {
        self.footView.itop = self.iheight
        self.iheight = self.footView.ibottom
        self.footView.isHidden = false
        self.footView.reloadData(data)
    }
    
    func reloadFooterCollectData(_ data:JSON?) {
        self.footView.reloadData(data)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

//MARK: horTableView delegate
extension HeaderOfTacticDetail:LMHorizontalTableViewDataSource,LMHorizontalTableViewDelegate {
    func numberOfRows(_ horizontalTableView: LMHorizontalTableView) -> Int {
        if let temp_array = goods_array {
            return temp_array.count
        }
        return 0
    }
    
    func horizontalTableView(_ horizontalTableView: LMHorizontalTableView, cellForRow row: Int, reuseCell: ((_ reuseInfentifer: String) -> LMHorizontalTableViewCell?)) -> LMHorizontalTableViewCell {
        var cell:TacticDetailHeaderCell? = reuseCell(TacticDetailHeaderCell.identifier) as? TacticDetailHeaderCell
        
        if cell == nil{
            cell = TacticDetailHeaderCell(reuseIdentifier:TacticDetailHeaderCell.identifier)
        }
        cell?.reloadData(goods_array![row])
        
        return cell!
    }
    
    func horizontalTableView(_ horizontalTableView: LMHorizontalTableView, heightForRow row: Int) -> CGFloat{
        return TacticDetailHeaderCell.cell_height
    }
    
    func horizontalTableView(_ horizontalTableView: LMHorizontalTableView, widthForRow row: Int) -> CGFloat{
        return TacticDetailHeaderCell.cell_width
    }
    
    
    func horizontalTableView(_ horizontalTableView: LMHorizontalTableView, didSelectRow row: Int) {
        if let gid = goods_array?[row].gid{
             Statistics.count(Statistics.Tractic.article_detail_goods_click, andAttributes: ["goodsId":String(gid)])
            self.headerDelegate!.horizontalTableViewClicked!(gid,index: row)
        }
        
     }
}


extension HeaderOfTacticDetail {
    fileprivate class TacticDetailHeaderCell: LMHorizontalTableViewCell {
        static let identifier = "TacticDetailHeaderCell"
        static let cell_width:CGFloat = 220 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        static let cell_height:CGFloat = 200 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        
        fileprivate let imageView:UIImageView = {
            let image = UIImageView()
            image.image = UIImage(named: "loadingDefault")
            return image
        }()
        
        override init(reuseIdentifier: String) {
            super.init(reuseIdentifier: reuseIdentifier)
            setUpUI()
        }
        
        fileprivate func setUpUI() {
            
            self.addSubview(imageView)
            let image_width = 200 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
            imageView.frame = CGRect(x: 0, y: 0, width: image_width, height: image_width)
        }
        
        func reloadData(_ data:TacticDetailHeaderMode) {
            let imageScale : String = data.goods_img + "?imageView2/0/w/" + "\(Int(imageView.iwidth * Constant.ScreenSizeV2.SCALE_SCREEN))" + "/h/" + "\(Int(imageView.iheight * Constant.ScreenSizeV2.SCALE_SCREEN))"
            
            imageView.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "loadingDefault"))

        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
}

class FooterBackView:UIView {
    
    fileprivate let titleLabel:UILabel = {
        let lbl = UILabel()
        lbl.font = Constant.Theme.Font_19
        lbl.textColor = Constant.Theme.Color14
        lbl.textAlignment = .center
        
        return lbl
    }()
    
    fileprivate let subTitleLabel:UILabel = {
        let lbl = UILabel()
        lbl.font = Constant.Theme.Font_14
        lbl.textColor = Constant.Theme.Color7
        lbl.textAlignment = .center
        
        return lbl
    }()
    
    fileprivate let authorImage:UIImageView = {
        let image = UIImageView()
        image.clipsToBounds = true
        image.contentMode = UIViewContentMode.scaleToFill
        
        return image
    }()
    
    fileprivate let authorNameLabel:UILabel = {
        let lbl = UILabel()
        lbl.font = Constant.Theme.Font_15
        lbl.textColor = Constant.Theme.Color6
        lbl.textAlignment = .left
        
        return lbl
    }()
    
    let collectBtn:UIButton = {
        let button = UIButton(type: UIButtonType.custom)
        
        return button
    }()
    
    fileprivate let collectImage:UIImageView = {
        let image = UIImageView()
        image.isUserInteractionEnabled = true
        image.clipsToBounds = true
        image.contentMode = UIViewContentMode.scaleToFill
        
        return image
    }()
    
    fileprivate let collectNumLabel:UILabel = {
        let lbl = UILabel()
        lbl.font = Constant.Theme.Font_15
        lbl.textColor = Constant.Theme.Color6
        lbl.textAlignment = .left
        
        return lbl
    }()
    
    var collectButtonHandle:(()->Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setUpUI()
    }
    
    fileprivate func setUpUI() {
        self.backgroundColor = UIColor.white
        
        for view in [titleLabel,subTitleLabel,authorImage,authorNameLabel,collectBtn] {
            self.addSubview(view)
        }
        
        collectBtn.addSubview(collectImage)
        collectBtn.addSubview(collectNumLabel)
        let scale = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        titleLabel.frame = CGRect(x: MARGIN_20, y: 60 * scale, width: Constant.ScreenSizeV2
            .SCREEN_WIDTH - 2 * MARGIN_20, height: 20)
        subTitleLabel.frame = CGRect(x: MARGIN_20, y: titleLabel.ibottom + 20 * scale, width: titleLabel.iwidth, height: 15)
        authorImage.frame = CGRect(x: 120 * scale, y: subTitleLabel.ibottom + 20 * scale, width: 100 * scale, height: 100 * scale)
        authorImage.layer.cornerRadius = authorImage.iwidth / 2
        
        authorNameLabel.frame = CGRect(x: authorImage.iright + 10 * scale, y: authorImage.itop, width: 80, height: 15)
        authorNameLabel.icenterY = authorImage.icenterY
        
        collectBtn.frame = CGRect(x: authorNameLabel.iright + 30 * scale, y: authorImage.itop, width: 130 * scale, height: 80 * scale)
        collectBtn.icenterY = authorImage.icenterY
        
        collectImage.frame = CGRect(x: 50 * scale, y: 0, width: 30 * scale, height: 30 * scale)
        collectImage.icenterY = collectBtn.iheight / 2
        
        collectNumLabel.frame = CGRect(x: collectImage.iright + 10 * scale, y: authorImage.itop, width: 120 * scale, height: 15)
        collectNumLabel.icenterY = collectBtn.iheight / 2
        
        self.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: authorImage.ibottom + 40 * scale)
        
        collectBtn.addTarget(self, action: #selector(FooterBackView.collectBtnClicked), for: .touchUpInside)
    }
    
    func reloadData(_ data:JSON?) {
        if let title = data?["Title"].string {
            titleLabel.text = title
        }
        
        if let subTitle = data?["Sub_title"].string {
            subTitleLabel.text = subTitle
        }
        
        if let authorUrl = data?["Author"]["Avatar"].string {
            let imageScale : String = authorUrl + "?imageView2/0/w/" + "\(Int(authorImage.iwidth * Constant.ScreenSizeV2.SCALE_SCREEN))" + "/h/" + "\(Int(authorImage.iheight * Constant.ScreenSizeV2.SCALE_SCREEN))"
            authorImage.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "accountIcon"))
        }else {
            authorImage.image = UIImage(named: "accountIcon")
        }
        
        if let authorName = data?["Author"]["Name"].string {
            authorNameLabel.text = authorName
        }
        
        if let collectStatus = data?["Collect_status"].bool {
            if collectStatus {
                collectImage.image = UIImage(named:"goods_general_toolbar_heart_press")
            }else {
                collectImage.image = UIImage(named:"tactic_collectnumber")
            }
        }else {
            collectImage.image = UIImage(named:"tactic_collectnumber")
        }
        
        if let collectNum = data?["Collect_num"].int{
            collectNumLabel.text = "\(collectNum)"
        }
        
        collectBtn.isEnabled = true
    }
    
    func collectBtnClicked() {
        collectBtn.isEnabled = false
        collectButtonHandle?()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


class TacticDetailHeaderMode:BaseDataMode {
    /*
     "data": [
     {
     "gid": 67,
     "goods_img": "http://up.xydd.co/14344531381614.jpg",
     "goods_name": "生命的火种 富硒破壁灵芝孢子粉180g盒（72小袋）"
     }]
    */
    
    var gid = NULL_INT
    var goods_img = NULL_STRING
    var goods_name = NULL_STRING
}




