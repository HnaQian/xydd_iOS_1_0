//
//  SnapshotOfTacticDetail.swift
//  Limi
//
//  Created by maohs on 16/9/8.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class SnapshotOfTacticDetail: BaseUnitView {
    fileprivate let iconImage:UIImageView = {
        let image = UIImageView()
        image.contentMode = UIViewContentMode.scaleAspectFill
        image.image = UIImage(named: "snapshot_top")
        return image
    }()
    
    fileprivate let headerImage:UIImageView = {
        let image = UIImageView()
        image.clipsToBounds  = true
        image.contentMode = UIViewContentMode.scaleAspectFill
        return image
    }()
    fileprivate let authorImage:UIImageView = {
        let image = UIImageView()
        image.backgroundColor = UIColor.white
        image.layer.masksToBounds = true
        return image
    }()
    
    fileprivate let authorContentImage:UIImageView = {
        let image = UIImageView()
        image.layer.masksToBounds = true
        return image
    }()
    
    fileprivate let authorNameLabel:UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .center
        lbl.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        lbl.textColor = Constant.Theme.Color14
        return lbl
    }()
    
    fileprivate let authorTitleLabel:UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .center
        lbl.font = UIFont.boldSystemFont(ofSize: Constant.common_F3_font)
        lbl.textColor = Constant.Theme.Color14
        return lbl
    }()
    
    fileprivate let authorSubTitleLabel:UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .center
        lbl.font = UIFont.systemFont(ofSize: 14)
        lbl.textColor = UIColor(rgba: Constant.common_C7_color)
        
        return lbl
    }()
    
    fileprivate let briefLabel:UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: 15)
        lbl.textColor = UIColor(rgba: Constant.common_C6_color)
        lbl.numberOfLines = 0
        
        return lbl
    }()
    
    fileprivate let goodsView:GoodsView = {
        let view = GoodsView()
       
        return view
    }()
    
    fileprivate let qrCodeImage:UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "snapshot_qr")
        return image
    }()
    
    fileprivate let bottomQrCodeLabel:UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .center
        lbl.font = UIFont.systemFont(ofSize: 14)
        lbl.textColor = UIColor(rgba: Constant.common_C7_color)
        lbl.text = "长按识别二维码"
        
        return lbl
    }()
    
    fileprivate let bottomExplainLabel:UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .center
        lbl.font = UIFont.systemFont(ofSize: 14)
        lbl.textColor = UIColor(rgba: Constant.common_C7_color)
        lbl.text = "下载心意点点了解更多"
        
        return lbl
    }()
    
    fileprivate let scale = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setUpUI()
    }
    
    func setUpUI() {
        self.backgroundColor = UIColor.white
        for view in [iconImage,headerImage,authorImage,authorContentImage,authorNameLabel,authorTitleLabel,authorSubTitleLabel,briefLabel,goodsView,qrCodeImage,bottomQrCodeLabel,bottomExplainLabel] {
            self.addSubview(view)
        }
        iconImage.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: 80 * scale)
        headerImage.frame = CGRect(x: 0, y: iconImage.ibottom, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: 1030 * scale)
        
        authorImage.frame = CGRect(x: 0, y: headerImage.ibottom - 65 * scale, width: 130 * scale, height: 130 * scale)
        authorImage.icenterX = headerImage.icenterX
        authorImage.layer.cornerRadius = authorImage.iwidth / 2
        
        authorContentImage.frame = CGRect(x: 0, y: headerImage.ibottom - 62 * scale, width: 124 * scale, height: 124 * scale)
        authorContentImage.icenterX = headerImage.icenterX
        authorContentImage.layer.cornerRadius = authorImage.iwidth / 2
        
        authorNameLabel.frame = CGRect(x: 30 * scale, y: authorImage.ibottom + 20 * scale, width: Constant.ScreenSizeV2.SCREEN_WIDTH - 60 * scale, height: 15)
        authorTitleLabel.frame = CGRect(x: 20 * scale, y: authorNameLabel.ibottom + 70 * scale, width: Constant.ScreenSizeV2.SCREEN_WIDTH - 40 * scale, height: 15)
        authorSubTitleLabel.frame = CGRect(x: 20 * scale, y: authorTitleLabel.ibottom + 20 * scale, width: Constant.ScreenSizeV2.SCREEN_WIDTH - 40 * scale, height: 15)
        
        briefLabel.frame = CGRect(x: 40 * scale, y: authorSubTitleLabel.ibottom + 50 * scale, width: Constant.ScreenSizeV2.SCREEN_WIDTH - 80 * scale, height: 15)
        
        goodsView.frame = CGRect(x: 0, y: briefLabel.ibottom + 75 * scale, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: 15 + 480 * scale)
        
        qrCodeImage.frame = CGRect(x: 0, y: goodsView.ibottom + 95 * scale, width: 250 * scale, height: 250 * scale)
        qrCodeImage.icenterX = headerImage.icenterX
        bottomQrCodeLabel.frame = CGRect(x: 20 * scale, y: qrCodeImage.ibottom + 20 * scale, width: Constant.ScreenSizeV2.SCREEN_WIDTH - 40 * scale, height: 15)
        bottomExplainLabel.frame = CGRect(x: 20 * scale, y: bottomQrCodeLabel.ibottom + 20 * scale, width: Constant.ScreenSizeV2.SCREEN_WIDTH - 40 * scale, height: 15)
        
        self.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: bottomExplainLabel.ibottom + 100 * scale)
    }
    
    func refreshData(_ headerImageView:UIImageView,goodsData:[TacticDetailHeaderMode]?,authorData:JSON?) {
        headerImage.iheight = headerImageView.iheight
        headerImage.image = headerImageView.image
        authorImage.itop = headerImage.ibottom - 65 * scale
        authorContentImage.itop = headerImage.ibottom - 62 * scale
        authorNameLabel.itop = authorImage.ibottom + 20 * scale
        authorTitleLabel.itop = authorNameLabel.ibottom + 70 * scale
        authorSubTitleLabel.itop = authorTitleLabel.ibottom + 20 * scale
        briefLabel.itop = authorSubTitleLabel.ibottom + 50 * scale
        
        if let intro = authorData?["Intro"].string {
            //首行缩进两个字使用"        "
            briefLabel.text = "        " + intro
            let attributedText = NSAttributedString(string: briefLabel.text!, attributes: [NSFontAttributeName: briefLabel.font])
            let rect = attributedText.boundingRect(with: CGSize(width: briefLabel.iwidth, height: CGFloat.greatestFiniteMagnitude), options: NSStringDrawingOptions.usesLineFragmentOrigin, context: nil)
            briefLabel.iheight = rect.size.height
            
            goodsView.itop = briefLabel.ibottom + 75 * scale
        }else {
            goodsView.itop = authorSubTitleLabel.ibottom + 75 * scale
        }
        
        if let tempGoodsArray = goodsData {
            if tempGoodsArray.count <= 0 {
                goodsView.iheight = 0
            }else if tempGoodsArray.count <= 3 {
                goodsView.iheight = 15 + 220 * scale
            }else {
                goodsView.iheight = 15 + 480 * scale
            }
        }else {
            goodsView.iheight = 0
        }
        qrCodeImage.itop = goodsView.ibottom + 95 * scale
        bottomQrCodeLabel.itop = qrCodeImage.ibottom + 20 * scale
        bottomExplainLabel.itop = bottomQrCodeLabel.ibottom + 20 * scale
        self.iheight = bottomExplainLabel.ibottom + 100 * scale
        
        
        if let title = authorData?["Title"].string {
            authorTitleLabel.text = title
        }
        
        if let subTitle = authorData?["Sub_title"].string {
            authorSubTitleLabel.text = subTitle
        }
        
        if let authorUrl = authorData?["Author"]["Avatar"].string {
            let imageScale : String = authorUrl + "?imageView2/0/w/" + "\(Int(authorImage.iwidth * Constant.ScreenSizeV2.SCALE_SCREEN))" + "/h/" + "\(Int(authorImage.iheight * Constant.ScreenSizeV2.SCALE_SCREEN))"
            authorContentImage.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "accountIcon"))
        }else {
            authorContentImage.image = UIImage(named: "accountIcon")
        }
        
        if let authorName = authorData?["Author"]["Name"].string {
            authorNameLabel.text = authorName
        }

        
        goodsView.refreshData(goodsData)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension SnapshotOfTacticDetail {
    class GoodsView: UIView {
        fileprivate let goodsLabel:UILabel = {
            let lbl = UILabel()
            lbl.font = UIFont.systemFont(ofSize: 14)
            lbl.textColor = Constant.Theme.Color17
            return lbl
        }()
        
        fileprivate let topLeftImage:UIImageView = {
            let image = UIImageView()
            
            return image
        }()
        
        fileprivate let topMiddleImage:UIImageView = {
            let image = UIImageView()
            
            return image
        }()
        
        fileprivate let topRightImage:UIImageView = {
            let image = UIImageView()
            
            return image
        }()
        
        fileprivate let bottomLeftImage:UIImageView = {
            let image = UIImageView()
            
            return image
        }()
        
        fileprivate let bottomMiddleImage:UIImageView = {
            let image = UIImageView()
            
            return image
        }()
        
        fileprivate let bottomRightImage:UIImageView = {
            let image = UIImageView()
            
            return image
        }()
        
        fileprivate var imageArray = [UIImageView]()
        fileprivate let scale = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        override init(frame: CGRect) {
            super.init(frame: frame)
            
            setUpUI()
        }
        
        func setUpUI() {
            self.clipsToBounds = true
            for view in [goodsLabel,topLeftImage,topMiddleImage,topRightImage,bottomLeftImage,bottomMiddleImage,bottomRightImage] {
                self.addSubview(view)
            }
            goodsLabel.frame = CGRect(x: 30 * scale, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH - 60 * scale, height: 15)
            topLeftImage.frame = CGRect(x: 37.5 * scale, y: goodsLabel.ibottom + 20 * scale, width: 200 * scale, height: 200 * scale)
            topMiddleImage.frame = CGRect(x: 275 * scale, y: goodsLabel.ibottom + 20 * scale, width: 200 * scale, height: 200 * scale)
            topRightImage.frame = CGRect(x: 512.5 * scale, y: goodsLabel.ibottom + 20 * scale, width: 200 * scale, height: 200 * scale)
            bottomLeftImage.frame = CGRect(x: 37.5 * scale, y: topLeftImage.ibottom + 60 * scale, width: 200 * scale, height: 200 * scale)
            bottomMiddleImage.frame = CGRect(x: 275 * scale, y: topLeftImage.ibottom + 60 * scale, width: 200 * scale, height: 200 * scale)
            bottomRightImage.frame = CGRect(x: 512.5 * scale, y: topLeftImage.ibottom + 60 * scale, width: 200 * scale, height: 200 * scale)
            
            imageArray = [topLeftImage,topMiddleImage,topRightImage,bottomLeftImage,bottomMiddleImage,bottomRightImage]
        }
        
        func refreshData(_ data:[TacticDetailHeaderMode]?) {
            
            if let count = data?.count {
                self.goodsLabel.text = "\(count)件产品"
                if count <= 6 {
                    for index in 0 ..< count {
                        let imageScale : String = data![index].goods_img + "?imageView2/0/w/" + "\(Int(200 * scale * Constant.ScreenSizeV2.SCALE_SCREEN))" + "/h/" + "\(Int(200 * scale * Constant.ScreenSizeV2.SCALE_SCREEN))"
                        
                        imageArray[index].af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "loadingDefault"))
                    }
                }else {
                    for index in 0 ..< 5 {
                        let imageScale : String = data![index].goods_img + "?imageView2/0/w/" + "\(Int(200 * scale * Constant.ScreenSizeV2.SCALE_SCREEN))" + "/h/" + "\(Int(200 * scale * Constant.ScreenSizeV2.SCALE_SCREEN))"
                        
                        imageArray[index].af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "loadingDefault"))
                    }
                    imageArray[5].image = UIImage(named: "snapshot_more")
                }
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
    }
}
