//
//  RecommendListViewController.swift
//  Limi
//
//  Created by ZhiQiangMi on 15/8/27.
//  Copyright (c) 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class RecommendListViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet weak var tableView: UITableView!
    
    var tableViewDataSource: [JSON] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "达人推荐"
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(RecommendCell.self, forCellReuseIdentifier: "cell")
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        
        weak var weakSelf = self as RecommendListViewController
        
        tableView.addHeaderRefresh { () -> Void in
            
           gcd.async(.default) {
                weakSelf?.tableViewDataSource.removeAll(keepingCapacity: true)
                weakSelf?.loadData()
            }
        }
        
        tableView.addFooterRefresh { () -> Void in
           gcd.async(.default) {
                weakSelf?.loadData()
            }
        }
        
        tableView.beginHeaderRefresh()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadData() {
        var params = [String: AnyObject]()
        if let lastid = tableViewDataSource.last?["Id"].int {
            params["last_id"] = lastid as AnyObject?
        }
        params["size"] = 10 as AnyObject?
        
       let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.recommend_list, parameters: params,isShowErrorStatuMsg:true, successHandler: {data, status, msg in
            
            self.tableView.closeAllRefresh()
            
            if status == 200 {
                if let list = JSON(data as AnyObject)["data"]["list"].array {
                    for item in list {
                        self.tableViewDataSource.append(item)
                    }
                    self.tableView.reloadData()
                }
                else{
                    
                    if self.tableViewDataSource.count > 0
                    {
                        self.tableView.showTipe(UIScrollTipeMode.noMuchMore)
                    }
                }
            }
        })
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int{
        
        return tableViewDataSource.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 280
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let identifier = "RecommendCell"
        
        var cell: RecommendCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? RecommendCell
        
        if cell == nil {
            tableView.register(UINib(nibName: "RecommendCell", bundle: nil), forCellReuseIdentifier: identifier)
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? RecommendCell
        }
        if tableViewDataSource.count > 0{
            cell.data = tableViewDataSource[(indexPath as NSIndexPath).section]
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        var data: JSON? = tableViewDataSource[(indexPath as NSIndexPath).section]
        if let Url = data?["Url"].string{
            EventDispatcher.dispatch(Url, onNavigationController: self.navigationController)
        }
        
    }
    
    
}
