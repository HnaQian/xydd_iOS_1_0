//
//  TacticDetailViewController.swift
//  Limi
//
//  Created by maohs on 16/7/18.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit
import WebKit
import CoreData
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}


class TacticDetailViewController: ExtensionHFWebViewController,ABPeoplePickerNavigationControllerDelegate {
    
    let headerView = HeaderOfTacticDetail()
    let footerView = FooterOfTacticDetail()
    
    
    var article_id:Int?
    fileprivate var tacticInfo:JSON?
    fileprivate var recommandGoods = [JSON]()
    fileprivate var goodsModelArray = [TacticDetailHeaderMode]()
    var delegate:TacticDetailDelegate?
    
    fileprivate var collect_status:Bool = false
    var index = 0
    
    //默认拉取商品数量
    fileprivate let size = 10
    fileprivate var total = ""
    fileprivate var page = 1
    
    //分享
    fileprivate var shareImg = ""
    fileprivate var shareFarticle_item = ""
    fileprivate var callBack = ""
    
    fileprivate lazy var snapShotView:SnapshotOfTacticDetail = {
        let view = SnapshotOfTacticDetail()
        
        return view
    }()
    fileprivate let loginDialog = LoginDialog()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let imageView = UIImageView(frame: CGRect(x: 0,y: 0,width: 36,height: 36))
        imageView.image = UIImage(named: "common_share_icon")
        self.setRightItems([imageView])
        self.showRightItems()
        
        self.navigationItem.title = "心意点点"
        
        self.setHeadView(self.headerView)
        headerView.headerDelegate = self
        self.headerView._imageView.image = UIImage(named: "Default_960_500")
        
        footerView.collectionView.userVC = self

        self.loadGoodsData()
        
        weak var weakSelf = self as TacticDetailViewController
        self.webView.scrollView.addFooterRefresh {
            weakSelf?.loadRecommandGoods()
        }
        
        self.countArticleScanning()
    }

    func countArticleScanning() {
        var countArray = [[String: AnyObject]]()
        if let temp = UserDefaults.standard.array(forKey: "article_scanning") {
            countArray = temp as! [[String: AnyObject]]
        }
        var tempDict = [String: AnyObject]()
        let tempCome_from = UserDefaults.standard.integer(forKey: "come_from")
        
        
        let zone : TimeZone = TimeZone.current
        let intervalCurrent : Int = zone.secondsFromGMT(for: Date())
        let nowDate = (Date().addingTimeInterval(TimeInterval(intervalCurrent)))
        let timeInterval: Int = Int(nowDate.timeIntervalSince1970)
        
        var userInfomation: User? = nil
        let requestUserInfo: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        let datasourceUserInfo : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(requestUserInfo)!
        if datasourceUserInfo.count > 0{
            
            userInfomation = datasourceUserInfo[0] as? User
        }
        if  let mobilePhone = userInfomation?.user_name{
            tempDict = ["mobile":mobilePhone as AnyObject,"channel_id":1 as AnyObject,"item_id":article_id! as AnyObject,"view_time":timeInterval as AnyObject,"come_from":tempCome_from as AnyObject]
        }else {
            tempDict = ["mobile":"" as AnyObject,"channel_id":1 as AnyObject,"item_id":article_id! as AnyObject,"view_time":timeInterval as AnyObject,"come_from":tempCome_from as AnyObject]
        }
        
        
//        let jsonData = try! JSONSerialization.data(withJSONObject: tempDict, options: JSONSerialization.WritingOptions.prettyPrinted)
//        let jsonString: String = String(data: jsonData, encoding: String.Encoding.utf8)!
        countArray.append(tempDict)
        var article_rule = 3
        if let temp = UserDefaults.standard.object(forKey: "article_rule") {
            article_rule = temp as! Int
        }
        var shoudSubmit = false
        if article_rule == 1 {
            //每条上传
            shoudSubmit = true
        }else if article_rule == 2 {
            //每5条上传
            shoudSubmit = countArray.count < 5 ? false : true
        }else if article_rule == 3 {
            //每10条上传
            shoudSubmit = countArray.count < 10 ? false : true
        }else {
            //打开APP上传
            shoudSubmit = false
        }
        
        if shoudSubmit {
            var params = [String:AnyObject]()
            params["type"] = 1 as AnyObject?
            params["requetst_frequency"] = article_rule as AnyObject?
            let jsonData = try! JSONSerialization.data(withJSONObject: countArray, options: JSONSerialization.WritingOptions.prettyPrinted)
            let jsonString: String = String(data: jsonData, encoding: String.Encoding.utf8)!
            params["request_json"] = jsonString as AnyObject?
            let _ = LMRequestManager.requestByDataAnalyses(context:self.view, URLString: Constant.JTAPI.submitScanning,parameters: params,isNeedUserTokrn :true,successHandler: {data, status, msg in
                if status != 200 {return}
                if let tempGoodsRule = JSON(data as AnyObject)["data"]["request_rule"].arrayObject {
                    if tempGoodsRule.count >= 2 {
                        UserDefaults.standard.set(tempGoodsRule[0].object(forKey: "requetst_frequency"), forKey: "goods_rule")
                        UserDefaults.standard.set(tempGoodsRule[1].object(forKey: "requetst_frequency"), forKey: "article_rule")
                        UserDefaults.standard.synchronize()
                    }
                    UserDefaults.standard.removeObject(forKey: "article_scanning")
                    
                }
            })
        }else {
            //添加到数据库
            UserDefaults.standard.set(countArray, forKey: "article_scanning")
            UserDefaults.standard.synchronize()
        }

    }
    
    fileprivate func loadGoodsData() {
        var params = [String:AnyObject]()
        if let temp_id = self.article_id {
            params["id"] = temp_id as AnyObject?
            Statistics.count(Statistics.Tractic.article_detail_click, andAttributes: ["articleId":String(self.article_id!)])
        }
        
       let _ = LMRequestManager.requestByDataAnalyses(context: self.view, URLString: Constant.JTAPI.article_goods, parameters: params,successHandler: { (data, status, msg) in
             self.loadTacticData()
            if status != 200 {return}
                if let tempArray = JSON(data as AnyObject)["data"].arrayObject {
                    var tempData = [TacticDetailHeaderMode]()
                    for index in 0 ..< tempArray.count {
                        let detailDic = TacticDetailHeaderMode(tempArray[index] as! [String:AnyObject])
                        tempData.append(detailDic)
                    }
                    self.goodsModelArray = tempData
                    self.headerView.reloadHeaderData(tempData)

                }
        })
    }
    
    fileprivate func loadTacticData() {
        var params = [String:AnyObject]()
        if let temp_id = self.article_id {
            params["article_id"] = temp_id as AnyObject?
        }
        
       let _ = LMRequestManager.requestByDataAnalyses(context: self.view, URLString: Constant.JTAPI.oldarticle_detail, parameters: params,isNeedUserTokrn: true, isShowErrorStatuMsg: true, successHandler: { (data, status, msg) in
            if status != 200 {return}
                let tacticInfo:JSON = JSON(data as AnyObject)["data"]["article"]
                if tacticInfo != nil {
                    self.tacticInfo = tacticInfo
                    
                    if tacticInfo["Sub_cover"].string! != "" {
                        self.headerView.updateImageView(tacticInfo["Sub_cover"].string!,type: 2)

                    }else {
                        if let tempString = tacticInfo["Cover"].string {
                            let imageScale : String = tempString + "?imageView2/0/w/" + "\(Int(self.headerView._imageView.iwidth * Constant.ScreenSizeV2.SCALE_SCREEN))" + "/h/" + "\(Int(self.headerView._imageView.iheight * Constant.ScreenSizeV2.SCALE_SCREEN))"
                            
                            self.headerView._imageView.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "Default_960_500"))
                        }
                    }
                    self.updateUI()
                }
                self.headerView.reloadFooterData(self.tacticInfo)
                if let tempStatus = self.tacticInfo!["Collect_status"].bool {
                    self.collect_status = tempStatus
                }else {
                    self.collect_status = false
                }
                self.setHeadView(self.headerView)
                self.scrollToTop()
        })
        
    }
    
    fileprivate func updateUI() {
        
        if let title = self.tacticInfo?["Title"].stringValue {
            self.navigationItem.title = title
        }
        
        if let detailUrl = self.tacticInfo?["Detail"].string {
            if let url = URL(string: detailUrl) {
                if Utils.containsString("need_token=1", inString: url.absoluteString) {
                    if let token : String = UserDefaults.standard.string(forKey: "UserToken"){
                        let newUrl =  String(format: "%@&token=%@", url.absoluteString,token)
                        self.webView.load(Foundation.URLRequest(url: URL(string: newUrl)!))
                    }
                }else {
                    self.webView.load(Foundation.URLRequest(url: url))
                }
            }
        }
    }
    
    fileprivate func loadRecommandGoods() {
        var params = [String:AnyObject]()
        if let temp_id = self.article_id {
            params["id"] = temp_id as AnyObject?
        }
        params["page"] = self.page as AnyObject?
        params["size"] = self.size as AnyObject?
        
       let _ = LMRequestManager.requestByDataAnalyses(context: self.view, URLString: Constant.JTAPI.article_recommandGoods, parameters: params, isNeedUserTokrn: true,successHandler: { (data, status, msg) in
            if self.webView.scrollView.isFooterRefreshing {
                self.webView.scrollView.endFooterRefresh()
            }
            if status != 200 {return}
            
            if let tempString = JSON(data as AnyObject)["data"]["total"].string {
                self.total = tempString
            }
            self.page += 1
            
            if let tempArray = JSON(data as AnyObject)["data"]["list"].array {
                for index in 0 ..< tempArray.count {
                    let detailData = tempArray[index]
                    self.recommandGoods.append(detailData)
                }
            }
            if self.recommandGoods.count >= Int(self.total) {
                //加载结束
                self.webView.scrollView.showTipe(UIScrollTipeMode.noMuchMore)
            }
            
            self.footerView.reloadData(self.recommandGoods)
            self.setFootView(self.footerView)
            self.webView.scrollView.refreshFooter.isHidden = false
        })
    }
    
    override func shareSnapshotToWx(_ wxScene:Int32) {
        super.shareSnapshotToWx(wxScene)
        self.snapShotView.refreshData(self.headerView._imageView,
                                      goodsData:self.goodsModelArray,
                                      authorData:self.tacticInfo)
        
        let hud = ProgressHUD(view: self.view)
        self.view.addSubview(hud)
        hud.show(true)
        let delay = DispatchTime.now() + Double(Int64(2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delay) {
            hud.hide(false)
            UIGraphicsBeginImageContextWithOptions(self.snapShotView.bounds.size, false, 3.0)
            let context = UIGraphicsGetCurrentContext()
            if let temp = context {
                self.snapShotView.layer.render(in: temp)
            }
            let tempImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            let wechatShare : LDSDKWXServiceImpl = LDSDKManager.getShareService(LDSDKPlatformType.weChat) as! LDSDKWXServiceImpl
            let shareDict = [LDSDKShareContentTitleKey : "",   LDSDKShareContentDescriptionKey : "",LDSDKShareContentImageKey : tempImage!] as [String : Any]
            wechatShare.share(withContent:shareDict as [AnyHashable: Any], shareModule:wxScene == Int32(WXSceneSession.rawValue) ? 1 : 2, onComplete: {(success,error) in
                if error != nil{
                    Utils.showError(context: self.view, errorStr: (error?.localizedDescription)!)
                    let javaScriptString : NSString = (String(format: "%@('{\"status\":400,\"msg\":\"分享失败\",data:\"\"}')", self.callBack) as NSString)
                    self.webView.evaluateJavaScript(javaScriptString as String, completionHandler: nil)
                }
                else{
                    let javaScriptString : NSString = (String(format: "%@('{\"status\":200,\"msg\":\"分享成功\",data:\"\"}')", self.callBack) as NSString)
                    self.webView.evaluateJavaScript(javaScriptString as String, completionHandler: nil)
                    
                }
                
            })
        }
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}



extension TacticDetailViewController {
    
    
    func lmWebViewNavigationItemMenuDidselected(_ navigationView: NavigationItemMenu, selectedIndex: Int) {

        //分享
        if selectedIndex == 0 {
            var shareConfigString : NSString = "js_share()"
            if self.isHadEntryKSL {
                shareConfigString = "app_share_config()"
            }
            self.webView.evaluateJavaScript(shareConfigString as String, completionHandler: nil)
        }
        
    }

    func webView(_ webView: WKWebView, didFinishNavigation navigation: WKNavigation!) {
        self.loadRecommandGoods()
    }
    
    override func tipAndLogin(_ tip:String){
        Utils.showError(context: self.view,errorStr: tip)
        let time: TimeInterval = 2.0
        let delay = DispatchTime.now() + Double(Int64(time * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delay) {
            if !self.isStopInLogin{
                self.Inlogin()
            }
        }
    }
    
    override func Inlogin(){
      
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.loginType = Constant.InLoginType.defaultType.rawValue
        
        self.headerView.footView.collectBtn.isEnabled = true
        self.loginDialog.controller = self
        self.loginDialog.show()
    }
    
    override func openContact()
    {
        self.navigationBarAppearance(true)
        let picker = ABPeoplePickerNavigationController()
        picker.displayedProperties = [3]
        picker.peoplePickerDelegate = self
        
        if ((UIDevice.current.systemVersion as NSString).floatValue >= 8.0)
        {
            picker.predicateForSelectionOfPerson = NSPredicate(value:false)
        }
        self.present(picker, animated: true) { () -> Void in
            
        }

    }
    
    // 领取优惠券
    override func receiveCoupon(_ array: [Int] ,webView: WKWebView) {
        let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.add_user_coupon, parameters: [ "coupon_id": array as AnyObject], isNeedUserTokrn: true,isShowErrorStatuMsg: true, isNeedHud: true, hudTitle: "正在领取",successHandler: {data, status, msg in
            if status != 200
            {
                let javaScriptString : NSString = (String(format: "%@('{\"status\":400,\"msg\":\"领取失败\",\"data\":\"\"}')", self.callBack) as NSString)
                
                self.webView.evaluateJavaScript(javaScriptString as String, completionHandler: nil)
                
                return
            }
            else{
                Utils.showError(context: self.view, errorStr: "领取成功")
                let uid : Int = UserDefaults.standard.integer(forKey: "UserUid")
                let javaScriptString : String = (String(format: "%@({\"status\":200,\"msg\":\"领取成功\",\"data\":{\"uid\":\"%d\",\"coupon_ids\":\"%@\"}})", self.callBack,uid,array.debugDescription))
                self.webView.evaluateJavaScript(javaScriptString as String, completionHandler: { (object, error) -> Void in
                    if error == nil && object != nil{
                    }
                })
                
                
                
            }
            
            
            
        })
        
        
    }
    
    override func showToast(_ toast: String){
        print(self.callBack)
        let javaScriptString = (String(format: "%@('{\"status\":200,\"msg\":\"提示成功\",\"data\":\"\"}')", self.callBack))
        self.webView.evaluateJavaScript(javaScriptString as String, completionHandler: { (object, error) -> Void in
            if error == nil && object != nil{
            }
        })
        
        Utils.showError(context: self.view, errorStr:toast)
    }
    
    override func actualWebView(url:URL) {
        let webVC = WebViewController(URL: url)
        self.navigationController?.pushViewController(webVC, animated: true)
    }
    
    
    func collect() {
        
        if UserInfoManager.didLogin {
            if self.collect_status {
               let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.collect_del, parameters: ["collect_type":FavouriteType.article.rawValue as AnyObject, "obj_id": self.article_id! as AnyObject], isNeedUserTokrn: true, isShowErrorStatuMsg: true,successHandler: {data, status, msg in
                    self.headerView.footView.collectBtn.isEnabled = true
                    if status == 200
                    {
                        self.delegate?.detailFavBtnClick!((self.index), isAdd: false)
                        self.collect_status = false
                        
                        self.tacticInfo?["Collect_status"].bool = false
                        self.tacticInfo?["Collect_num"].int! -= 1
                        self.headerView.reloadFooterCollectData(self.tacticInfo)
                        Utils.showError(context: self.view, errorStr: "取消喜欢成功")
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue:Constant.tactic_collectnumber), object: nil)
                    }
                },failureHandler:{[weak self] in
                    self?.headerView.footView.collectBtn.isEnabled = true
               })
            }else {
                Statistics.count(Statistics.Tractic.article_detail_like_click, andAttributes: ["aid":String(self.article_id!)])
               let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.collect_add, parameters: ["collect_type": FavouriteType.article.rawValue as AnyObject, "obj_id": [self.article_id!] as AnyObject], isNeedUserTokrn: true, isShowErrorStatuMsg: true,successHandler: {data, status, msg in
                    self.headerView.footView.collectBtn.isEnabled = true
                    if status == 200
                    {
                        self.delegate?.detailFavBtnClick!((self.index), isAdd: true)
                        self.collect_status = true
                        
                        self.tacticInfo?["Collect_status"].bool = true
                        self.tacticInfo?["Collect_num"].int! += 1
                        self.headerView.reloadFooterCollectData(self.tacticInfo)
                        Utils.showError(context: self.view, errorStr: "添加喜欢成功")
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue:Constant.tactic_collectnumber), object: nil)
                    }
                    
                },failureHandler:{[weak self] in
                    self?.headerView.footView.collectBtn.isEnabled = true
                })
            }
        }else {
            self.headerView.footView.collectBtn.isEnabled = true
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.loginType = Constant.InLoginType.webViewTacticDetailCollectType.rawValue
            self.jumpToLogin(Constant.InLoginType.pointMallType, tipe: nil)
        }
       
    }
    
    
}

extension TacticDetailViewController:HeaderOfTacticDetailDelegate {
    func horizontalTableViewClicked(_ gid: Int,index:Int) {
        
        let goods = GoodsDetailViewController.newGoodsDetailWithID(gid)
        goods.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(goods, animated: true)
    }
    
    func headerOfTacticDetailCollectButtonClicked() {
        self.headerView.footView.collectBtn.isEnabled = true
        self.collect()
    }
}
