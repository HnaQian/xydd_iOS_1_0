//
//  TopicDesignerViewController.swift
//  Limi
//
//  Created by 程巍巍 on 6/25/15.
//  Copyright (c) 2015 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class TopicDesignerViewController: BaseViewController {
    
    var tableView = UITableView()
    
    var listSource = [JSON]()
    var coverSource = JSON(NSNull())
    
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        tableView.delegate = nil
        tableView.dataSource = nil
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "设计师"
        
        
        self.view.addSubview(tableView)
        tableView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.view)
            let _ = make.bottom.equalTo(self.view)
            let _ = make.left.equalTo(self.view)
            let _ = make.right.equalTo(self.view)
        }

        
        tableView.register(Cover.self, forCellReuseIdentifier: "Cover")
        tableView.register(ListCell.self, forCellReuseIdentifier: "ListCell")
        tableView.separatorStyle = .none
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        
        weak var weakSelf = self as TopicDesignerViewController
        
        
        tableView.addHeaderRefresh { () -> Void in
            
            DispatchQueue.global().async(execute: { () -> Void in
                weakSelf?.loadList(0)
            })
        }
        
        tableView.addFooterRefresh { () -> Void in
            DispatchQueue.global().async(execute: { () -> Void in
                if let id = weakSelf?.listSource.last?["Id"].int {
                    weakSelf?.loadList(id)
                }
            })
            
        }
        tableView.beginHeaderRefresh()
        loadLayout()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // 加载布局
    func loadLayout() {
       let _ = LMRequestManager.requestByDataAnalyses(.get, context:self.view, URLString: Constant.JTAPI.layout + "/designer",isShowErrorStatuMsg:true,isNeedHud : true, successHandler: {data, status, msg in
            if status == 200
            {
                if let info = JSON(data as AnyObject)["data"]["layout"].array{
                    self.coverSource = info[0]
                }
                self.tableView.reloadData()
            }

        })
    }
    
    func loadList(_ lastId: Int) {
        let params: [String: AnyObject] = ["code": "designer" as AnyObject, "last_id": lastId as AnyObject]
       let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.article_list, parameters: params, isNeedUserTokrn: true,isShowErrorStatuMsg: true, successHandler: {data, status, msg in
            self.tableView.closeAllRefresh()
            if status == 200 {
                if let list = JSON(data as AnyObject)["data"]["list"].array {
                    // 如果 lastId == 0 表示刷新 否则是追加数据
                    if lastId == 0 {
                        self.listSource = list
                        self.tableView.reloadData()
                    }else{
                        if list.count == 0 {
                            if let text = data["msg"] as? String{
                                Utils.showError(context: self.view, errorStr: text)
                            }
                        }else{
                            for json in list {
                                self.listSource.append(json)
                            }
                            self.tableView.reloadData()
                        }
                    }
                }
                else{
                    if self.listSource.count > 1
                    {
                        self.tableView.showTipe(UIScrollTipeMode.noMuchMore)
                    }
                }
            }

        })
        
    }
}

extension TopicDesignerViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return listSource.count + 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellType: TableCell.Type = (indexPath as NSIndexPath).section == 0 ? Cover.self : ListCell.self
        return cellType.heightForWidth(tableView.frame.width)
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 8
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: (indexPath as NSIndexPath).section == 0 ? "Cover" : "ListCell", for: indexPath) as! TableCell
        cell.data = ((indexPath as NSIndexPath).section == 0 ? coverSource : listSource[(indexPath as NSIndexPath).section - 1])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if (indexPath as NSIndexPath).section == 0
        {
            //            if let href = coverSource["attributes"]["href"].string {
            //                EventDispatcher.dispatch(href, onNavigationController: self.navigationController)
            //            }
        }else
        {
            if let id = listSource[(indexPath as NSIndexPath).section - 1]["Id"].int {
                let href = "http://wechat.giftyou.me/v1/article/article_item?id=\(id)&app=1"
                EventDispatcher.dispatch(href, onNavigationController: self.navigationController)
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        // 下拉
        if scrollView.contentOffset.y < 0 {
            
        }
    }
}

extension TopicDesignerViewController {
    class TableCell: UITableViewCell {
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)
            self.selectionStyle = .none
            defaultInit()
        }
        
        required init(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func defaultInit() {
            
        }
        var data: JSON?
        
        class func heightForWidth(_ width: CGFloat)->CGFloat {
            return 0
        }
    }
    class Cover: TableCell {
        var imageCover = UIImageView()
        
        override class func heightForWidth(_ width: CGFloat)->CGFloat {
            return width * 0.5
        }
        override var data: JSON? {
            didSet {
                if let src = data?["attributes"]["src"].string {
                    let imageScale : String = src + "?imageView2/0/w/" + "\(Int(Constant.ScreenSize.SCREEN_WIDTH*Constant.ScreenSize.SCALE_SCREEN))" + "/h/" + "\(Int(Constant.ScreenSize.SCREEN_WIDTH*0.50*Constant.ScreenSize.SCALE_SCREEN))"
                    imageCover.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "loadingDefault"))
                }
            }
        }
        
        override func defaultInit() {
            self.backgroundColor = UIColor(rgba: Constant.common_background_color)
            
            self.contentView.addSubview(imageCover)
            
            imageCover.translatesAutoresizingMaskIntoConstraints = false

            imageCover.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(self.contentView)
                let _ = make.right.equalTo(self.contentView)
                let _ = make.top.equalTo(self.contentView)
                let _ = make.bottom.equalTo(self.contentView)
            }
        }
    }
    
    class ListCell: TableCell {
        var content = UIView()
        var imageCover = UIImageView()
        var titleLabel = UILabel()
        var seperatorLine = UIView()
        var subTitleLabel = UILabel()
        
        override var data: JSON? {
            didSet {
                if let src = data?["Cover"].string {
                    let imageScale : String = src + "?imageView2/0/w/" + "\(Int((Constant.ScreenSize.SCREEN_WIDTH - 16)*Constant.ScreenSize.SCALE_SCREEN))" + "/h/" + "\(Int((Constant.ScreenSize.SCREEN_WIDTH - 16)*0.55*Constant.ScreenSize.SCALE_SCREEN))"
                    imageCover.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "loadingDefault"))
                }
                titleLabel.text = data?["Title"].string
                subTitleLabel.text = data?["Sub_title"].string
            }
        }
        
        override class func heightForWidth(_ width: CGFloat)->CGFloat {
            return width * 0.5
            //                + 8 + 19 + 15
        }
        
        override func defaultInit() {
            
            self.backgroundColor = UIColor.clear
            content.backgroundColor = UIColor.white
            
            titleLabel.font = Constant.CustomFont.Default(size: 19)
            titleLabel.textColor = UIColor.white
            subTitleLabel.font = Constant.CustomFont.Default(size: 15)
            subTitleLabel.textColor = UIColor.white
            imageCover.contentMode = UIViewContentMode.scaleAspectFill
            imageCover.clipsToBounds = true
            
            self.contentView.addSubview(content)
            content.translatesAutoresizingMaskIntoConstraints = false

            content.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(self.contentView).offset(8)
                let _ = make.right.equalTo(self.contentView).offset(8)
                let _ = make.top.equalTo(self.contentView)
                let _ = make.bottom.equalTo(self.contentView)
            }
            seperatorLine.backgroundColor = UIColor(rgba: "#ACACAC")
            
            for view in [imageCover, titleLabel, seperatorLine, subTitleLabel] {
                content.addSubview(view)
            }
            

            imageCover.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(content)
                let _ = make.right.equalTo(content)
                let _ = make.top.equalTo(content)
                let _ = make.height.equalTo(imageCover.snp_width).multipliedBy(0.55)
            }

            titleLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.centerX.equalTo(self)
                let _ = make.bottom.equalTo(seperatorLine.snp_top).offset(-4)
            }

            seperatorLine.snp_makeConstraints { (make) -> Void in
                let _ = make.center.equalTo(self)
                let _ = make.height.equalTo(0.5)
                let _ = make.width.equalTo(titleLabel)
            }
            subTitleLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.centerX.equalTo(self)
                let _ = make.top.equalTo(seperatorLine.snp_bottom).offset(4)
            }
        }
    }
}
