//
//  FeatureViewController.swift
//  Limi
//
//  Created by guo chen on 15/11/30.
//  Copyright © 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//专题路由类的，精选专题等各种类型的专题

import UIKit

class FeatureViewController: TacticViewController 
{
    override func viewDidLoad() 
    {
        if let params : NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: eventURL?.query) as NSDictionary?{
            if let code  = params.object(forKey: "code")as? String {
                self.code_article = code
            }
        }
        
        self.shouldSetHttpTitle = true//从获取到得数据中解析title
        
        super.viewDidLoad()

        self.navigationItem.title = ""
        
        self.tabView.tableView.frame = self.view.bounds
    }
}
