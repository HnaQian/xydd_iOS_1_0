//
//  GiftAdvisorViewController.swift
//  Limi
//
//  Created by maohs on 16/11/24.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

enum GiftButtonActionType:NSInteger {
    case who = 0
    case why
}

class GiftAdvisorViewController: BaseViewController, FFXRangeStepViewDelegate, BaseGiftButtonUnitViewDelegate {

    private let scrollView = UIScrollView()
    
    private let giftObjectArray = ["妈妈","爸爸","他","她","闺蜜","基友","小盆友","老师长辈","领导下属","合作伙伴"]
    private let giftReasonArray = ["生日","节日","纪念日","结婚","探亲访友","回赠礼物","生子白天","商务会面","讨好领导","道歉"]
    private var giftChioceParameter = [String:String]()
    
    
    
    fileprivate let priceScreenView = FFXRangeSlider(frame: CGRect(x: 10,y: 200,width: Constant.ScreenSize.SCREEN_WIDTH - 20,height: 52))//滑块
    fileprivate var moneyString = ["0", "200", "500", "1000", "2000", "2000+"]
//    fileprivate let moneyDues = [0,200,500,1000,2000,99990000]
    
    private let objectView = ButtonView()
    private let reasonView = ButtonView()
    
    private let contentLabel = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.scrollView.backgroundColor = UIColor.white
        // Do any additional setup after loading the view.
        self.navigationItem.title = "在线礼物顾问"
        
        giftChioceParameter["min"] = "0"
        giftChioceParameter["max"] = "2000+"
        giftChioceParameter["who"] = ""
        giftChioceParameter["why"] = ""
        
        self.scrollView.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: Constant.ScreenSizeV2.SCREEN_HEIGHT - 64 - Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 150)
    
        
        let whoTitle = UILabel()
        whoTitle.backgroundColor = UIColor.white
        whoTitle.frame = CGRect(x: 0, y: Constant.ScreenSizeV2.MARGIN_40, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: Constant.ScreenSizeV2.MARGIN_30)
        whoTitle.textAlignment = NSTextAlignment.center
        whoTitle.text = "您要给谁送礼？"
        whoTitle.textColor = Constant.Theme.Color2
        whoTitle.font = UIFont.boldSystemFont(ofSize: Constant.Theme.FontSize_15)
        self.scrollView.addSubview(whoTitle)
        
        objectView.calculateLinesCountAndEveryLineNumber(giftObjectArray)
        objectView.buttonType = .who
        objectView.delegate = self
        self.scrollView.addSubview(objectView)
        
        let line1 = UIView()
        line1.frame = CGRect(x: 0, y: objectView.frame.origin.y + objectView.frame.size.height, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: Constant.ScreenSizeV2.MARGIN_20)
        line1.backgroundColor = UIColor(colorLiteralRed: 242/255.0, green: 242/255.0, blue: 242/255.0, alpha: 1)
        self.scrollView.addSubview(line1)
        
        let whyTitle = UILabel()
        whyTitle.backgroundColor = UIColor.white
        whyTitle.frame = CGRect(x: 0, y: line1.frame.origin.y + line1.frame.size.height + Constant.ScreenSizeV2.MARGIN_40, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: Constant.ScreenSizeV2.MARGIN_30)
        whyTitle.textAlignment = NSTextAlignment.center
        whyTitle.text = "为什么要给Ta送礼？"
        whyTitle.textColor = Constant.Theme.Color2
        whyTitle.font = UIFont.boldSystemFont(ofSize: Constant.Theme.FontSize_15)
        self.scrollView.addSubview(whyTitle)
        
        reasonView.calculateLinesCountAndEveryLineNumber(giftReasonArray)
        reasonView.buttonType = .why
        reasonView.delegate = self
        reasonView.frame.origin = CGPoint(x: 0, y: whyTitle.frame.origin.y + whyTitle.frame.size.height + Constant.ScreenSizeV2.MARGIN_30)
        self.scrollView.addSubview(reasonView)
        
        let line2 = UIView()
        line2.frame = CGRect(x: 0, y: reasonView.frame.origin.y + reasonView.frame.size.height, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: Constant.ScreenSizeV2.MARGIN_20)
        line2.backgroundColor = UIColor(colorLiteralRed: 242/255.0, green: 242/255.0, blue: 242/255.0, alpha: 1)
        self.scrollView.addSubview(line2)
        
        let priceTitle = UILabel()
        priceTitle.backgroundColor = UIColor.white
        priceTitle.frame = CGRect(x: 0, y: line2.frame.origin.y + line2.frame.size.height + Constant.ScreenSizeV2.MARGIN_40, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: Constant.ScreenSizeV2.MARGIN_30)
        priceTitle.textAlignment = NSTextAlignment.center
        priceTitle.text = "您选择礼物的预算是？"
        priceTitle.textColor = Constant.Theme.Color2
        priceTitle.font = UIFont.boldSystemFont(ofSize: Constant.Theme.FontSize_15)
        self.scrollView.addSubview(priceTitle)
        
        priceScreenView.stepView.delegate = self
        priceScreenView.stepView.color = UIColor(rgba: Constant.common_C8_color)
        priceScreenView.stepView.activeColor = UIColor(rgba: Constant.common_C6_color)
        priceScreenView.steps = moneyString as NSArray
        priceScreenView.handleTintColor = UIColor(rgba: "#F9868E")//两个球的初始颜色
        priceScreenView.handleBorderWidth = 2//两个球表边宽度
        priceScreenView.handleBorderColor = UIColor(rgba: "#FFC3C3")//两个球描边
        priceScreenView.trackTintColor = UIColor(rgba: Constant.common_C10_color)//两个球开区间颜色
        priceScreenView.selectedTrackTintColor = UIColor(rgba: Constant.common_C1_color)//两个球闭区间颜色
        priceScreenView.frame = CGRect(x: 15,y: priceTitle.frame.origin.y + priceTitle.frame.size.height + Constant.ScreenSizeV2.MARGIN_40,width: Constant.ScreenSize.SCREEN_WIDTH - 30,height: 52)
        self.scrollView.addSubview(priceScreenView)

        self.scrollView.contentSize = CGSize(width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: priceScreenView.frame.origin.y + priceScreenView.frame.size.height + Constant.ScreenSizeV2.MARGIN_40)
        
        self.view.addSubview(scrollView)
        
        let contentView = UIView()
        contentView.backgroundColor = UIColor.white
        contentView.frame = CGRect(x: 0, y: Constant.ScreenSizeV2.SCREEN_HEIGHT - 64 - Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 150, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: Constant.ScreenSizeV2.MARGIN_60)
        contentView.layer.borderWidth = 0.5
        contentView.layer.borderColor = Constant.Theme.Color10.cgColor
        
        contentLabel.frame = CGRect(x: 0, y: Constant.ScreenSizeV2.MARGIN_40, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: Constant.ScreenSizeV2.MARGIN_60)
        contentLabel.center.y = contentView.frame.size.height/2
        contentLabel.setTitle("0-2000+元", for: UIControlState.normal)
        contentLabel.titleLabel?.font = Constant.Theme.Font_15
        contentLabel.setTitleColor(Constant.Theme.Color14, for: UIControlState.normal)
        contentView.addSubview(contentLabel)
        self.view.addSubview(contentView)
        
        let choiceButton = UIButton()
        choiceButton.backgroundColor = Constant.Theme.Color1
        choiceButton.frame = CGRect(x: 0, y: Constant.ScreenSizeV2.SCREEN_HEIGHT - 64 - Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 90, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 90)
        choiceButton.setTitle("选好了", for: UIControlState.normal)
        choiceButton.setTitleColor(UIColor.white, for: UIControlState.normal)
        choiceButton.titleLabel?.font = Constant.Theme.Font_19
        choiceButton.setImage(UIImage(named:"jiantou"), for: UIControlState.normal)
        let lblWidth = CalculateHeightOrWidth.getLabOrBtnWidth("选好了", font: Constant.Theme.Font_19, height: 60)
        choiceButton.imageEdgeInsets = UIEdgeInsetsMake(0, lblWidth + Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * 170 - 20, 0, 0)
        choiceButton.titleEdgeInsets = UIEdgeInsetsMake(0, -20, 0, 0)
        choiceButton.addTarget(self, action: #selector(GiftAdvisorViewController.choiceButtonOnClicked), for: .touchUpInside)
        self.view.addSubview(choiceButton)
        
        
    }
    
    
    func choiceButtonOnClicked() {
        if UserInfoManager.didLogin {
            Statistics.count(Statistics.Service.service_online_select_click)
            let source = QYSource()
            source.title = "商品详情"
            source.urlString = ""
            let commodityInfo = QYCommodityInfo()
            commodityInfo.title = ""
            var commodityInfoDesc = ""
            if self.giftChioceParameter["who"] != "" {
                commodityInfoDesc += "送礼对象 " + self.giftChioceParameter["who"]! + "\n"
            }
            if self.giftChioceParameter["why"] != "" {
                commodityInfoDesc += "送礼场合 " + self.giftChioceParameter["why"]! + "\n"
            }
            commodityInfoDesc += "预算 " + giftChioceParameter["min"]! + "-" + self.giftChioceParameter["max"]! + "元"
            commodityInfo.desc = commodityInfoDesc
            //页面跳转
            let qyvc = QYSDK.shared().sessionViewController()
            qyvc?.navigationItem.leftBarButtonItem = self.qyCustomBackButton()
            qyvc?.sessionTitle = "礼物顾问"
            qyvc?.commodityInfo = commodityInfo
            qyvc?.source = source
            qyvc?.groupId = 235271
            QYSDK.shared().customActionConfig().linkClickBlock = {
                [weak self](QYLinkClickBlock) -> Void in
                let temp = QYLinkClickBlock! as String
                EventDispatcher.dispatch(temp, onNavigationController: self?.navigationController)
            }
            self.navigationController?.pushViewController(qyvc!, animated: true)
        }else {
            self.jumpToLogin(Constant.InLoginType.consultType, tipe: nil)
        }

    }
    
    func qyCustomBackButton() -> UIBarButtonItem
    {
        let backButton = UIButton(type: UIButtonType.custom)
        backButton.frame = CGRect(x: 0, y: 0, width: 36, height: 36)
        backButton.setBackgroundImage(UIImage(named: "common_back_icon"), for: UIControlState())
        backButton.addTarget(self, action: #selector(GiftAdvisorViewController.backButtonClicked), for: UIControlEvents.touchUpInside)
        backButton.clipsToBounds = true
        backButton.tintColor = UIColor.red
        
        let item = UIBarButtonItem(customView: backButton)
        
        return item
    }
    
    
    func backButtonClicked(){
        let _ = self.navigationController?.popViewController(animated: true)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }



    //MARK:delegate

    var moneyDuesIndex:IndexSet!
    
    func didSelectedIndexSet(_ activeIndexes: IndexSet!) {
        
        Statistics.count(Statistics.Service.service_online_price_click)
        if activeIndexes.first != nil {
            self.giftChioceParameter["min"] = self.moneyString[activeIndexes.first!]
            

        }
        if activeIndexes.last != nil {
            self.giftChioceParameter["max"] = self.moneyString[activeIndexes.last!]
        }
        var labelText:String = ""
        if self.giftChioceParameter["who"] != "" {
            labelText += self.giftChioceParameter["who"]! + "，"
        }
        if self.giftChioceParameter["why"] != "" {
            labelText += self.giftChioceParameter["why"]! + "，"
        }
        labelText += self.giftChioceParameter["min"]! + "-" + self.giftChioceParameter["max"]! + "元"
        contentLabel.setTitle(labelText, for: UIControlState.normal)
    }
    
    func baseGiftButtonUnitViewTouchdAction(_ actionType: GiftButtonActionType, content: String?) {
        
        if actionType == .who {
            if content != nil {
                self.giftChioceParameter["who"] = content
                Statistics.count(Statistics.Service.service_online_who_click, andAttributes: ["who":content!])
            }else{
                self.giftChioceParameter["who"] = ""
            }
        }else{
            if content != nil {
                Statistics.count(Statistics.Service.service_online_occasion_click, andAttributes: ["why":content!])
                self.giftChioceParameter["why"] = content
            }else{
                self.giftChioceParameter["why"] = ""
            }
        }
        var labelText:String = ""
        if self.giftChioceParameter["who"] != "" {
            labelText += self.giftChioceParameter["who"]! + "，"
        }else {
            
        }
        if self.giftChioceParameter["why"] != "" {
            labelText += self.giftChioceParameter["why"]! + "，"
        }
        labelText += self.giftChioceParameter["min"]! + "-" + self.giftChioceParameter["max"]!
        contentLabel.setTitle(labelText, for: UIControlState.normal)
        if self.giftChioceParameter["who"] == "他" || self.giftChioceParameter["who"] == "她" {
            contentLabel.setImage(UIImage(named:"black_heart"), for: UIControlState.normal)
        }else {
            contentLabel.setImage(nil, for: UIControlState.normal)
        }
    }

}
extension GiftAdvisorViewController {
    
    class ButtonView: UIView {
        
        fileprivate var dataSourceName:[String] = []
        fileprivate var linesCount = 1
        fileprivate var lineWidth:CGFloat = 0.0
        fileprivate var numberArr:[Int] = [-1]
        fileprivate var buttonArr:[UIButton] = []
        fileprivate var lineViewArr:[UIView] = []
        fileprivate var widthArr:[CGFloat] = []
        var buttonType:GiftButtonActionType = .who
        
        weak var delegate:BaseGiftButtonUnitViewDelegate?
        
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            self.backgroundColor = UIColor.white
            self.clipsToBounds = true
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }

        
        func calculateLinesCountAndEveryLineNumber(_ dataArr:[String]) {
            
            for i in 0..<dataArr.count {
                let button = createBtn(dataArr[i])
                
                var widthButton = CalculateHeightOrWidth.getLabOrBtnWidth((button.titleLabel?.text)! as NSString, font: Constant.Theme.Font_15, height: Constant.ScreenSizeV2.MARGIN_60) + Constant.ScreenSizeV2.MARGIN_68
                
                if dataArr[i] == "他" || dataArr[i] == "她" {
                    button.setImage(UIImage(named:"grey_heart"), for: UIControlState.normal)
                    button.setImage(UIImage(named:"white_heart"), for: UIControlState.selected)
                    widthButton += Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * 34
                }
                lineWidth = lineWidth + widthButton + Constant.ScreenSizeV2.MARGIN_40
                buttonArr.append(button)
                widthArr.append(widthButton)
                if lineWidth > Constant.ScreenSizeV2.SCREEN_WIDTH - Constant.ScreenSizeV2.MARGIN_20{
                    linesCount += 1
                    numberArr.append(i - 1)
                    
                    lineWidth = widthButton + Constant.ScreenSizeV2.MARGIN_40
                }
                
            }
            numberArr.append(dataArr.count - 1)
            self.frame = CGRect(x: 0, y: Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * 100, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * (10 + CGFloat(linesCount) * 90))
            compositionButton()
        }
        
        func compositionButton() {
            
            for i in 0...linesCount - 1 {
                let lineView = UIView()
                lineViewArr.append(lineView)
                self.addSubview(lineView)
                for j in numberArr[i] + 1...numberArr[i + 1] {
                    lineView.addSubview(buttonArr[j])
                    
                    
                    
                    if j == numberArr[i] + 1 {
                        
                        buttonArr[j].snp_makeConstraints(closure: { (make) in
                            let _ = make.top.equalTo(lineView)
                            let _ = make.left.equalTo(lineView)
                            let _ = make.width.equalTo(widthArr[j])
                            let _ = make.bottom.equalTo(lineView)
                            let _ = make.height.equalTo(Constant.ScreenSizeV2.MARGIN_60)
                        })
                        
                        
                    }else if j == numberArr[i + 1] {
                        buttonArr[j].snp_makeConstraints(closure: { (make) in
                            let _ = make.top.equalTo(lineView)
                            let _ = make.left.equalTo(buttonArr[j - 1].snp_right).offset(Constant.ScreenSizeV2.MARGIN_40)
                            let _ = make.right.equalTo(lineView)
                            let _ = make.width.equalTo(widthArr[j])
                            let _ = make.bottom.equalTo(lineView)
                            let _ = make.height.equalTo(Constant.ScreenSizeV2.MARGIN_60)
                        })
                    }else {
                        buttonArr[j].snp_makeConstraints(closure: { (make) in
                            let _ = make.top.equalTo(lineView)
                            let _ = make.left.equalTo(buttonArr[j - 1].snp_right).offset(Constant.ScreenSizeV2.MARGIN_40)
                            let _ = make.width.equalTo(widthArr[j])
                            let _ = make.bottom.equalTo(lineView)
                            let _ = make.height.equalTo(Constant.ScreenSizeV2.MARGIN_60)
                            
                        })
                    }
                    
                    
                    
                }
                
                if lineView.subviews.count == 1 {
                    lineView.subviews[0].snp_makeConstraints(closure: { (make) in
                        let _ = make.top.equalTo(lineView)
                        let _ = make.left.equalTo(lineView)
                        let _ = make.bottom.equalTo(lineView)
                        let _ = make.right.equalTo(lineView)
                        let _ = make.height.equalTo(Constant.ScreenSizeV2.MARGIN_60)
                        
                    })
                }
                
                
                if i == 0 {
                    lineView.snp_makeConstraints(closure: { (make) in
                        let _ = make.top.equalTo(self)
                        let _ = make.centerX.equalTo(self)
                    })
                }else {
                    lineView.snp_makeConstraints(closure: { (make) in
                        let _ = make.top.equalTo(lineViewArr[i - 1].snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_30)
                        let _ = make.centerX.equalTo(self)
                    })
                }
            }
            
        }
        
        
        func createBtn(_ title: String) -> UIButton {
            let btn = UIButton()
            //            btn.backgroundColor = UIColor.whiteColor()
            btn.setTitle(title, for: UIControlState())
        
            btn.setTitleColor(Constant.Theme.Color7, for: UIControlState())
            btn.setTitleColor(UIColor.white, for: UIControlState.selected)
            btn.titleLabel?.font = Constant.Theme.Font_15
            btn.layer.cornerRadius = Constant.ScreenSizeV2.MARGIN_30
            btn.layer.borderWidth = 0.5
            btn.addTarget(self, action: #selector(ButtonView.onClick(_:)), for: UIControlEvents.touchUpInside)
            btn.layer.borderColor = Constant.Theme.Color7.cgColor
            btn.contentEdgeInsets.left = 34 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
            btn.contentEdgeInsets.right = 34 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
            
            return btn
        }
        
        
        func onClick(_ button:UIButton) {
            if button.isSelected {
                button.isSelected = false
                button.backgroundColor = Constant.Theme.Color12
                button.layer.borderWidth = 0.5
                delegate?.baseGiftButtonUnitViewTouchdAction(buttonType, content: "")
            }else{
                for btn in buttonArr{
                    btn.isSelected = false
                    btn.backgroundColor = Constant.Theme.Color12
                    btn.layer.borderWidth = 0.5
                }
                button.isSelected = true
                button.layer.borderWidth = 0
                button.backgroundColor = Constant.Theme.Color1
                delegate?.baseGiftButtonUnitViewTouchdAction(buttonType, content: button.titleLabel?.text)
            }
        }
    }
    
   
    
}
//MARK:GiftChoiceOptionViewController 点击代理
protocol BaseGiftButtonUnitViewDelegate:NSObjectProtocol {
    func baseGiftButtonUnitViewTouchdAction(_ actionType:GiftButtonActionType,content:String?)
}
