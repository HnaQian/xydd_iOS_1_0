//
//  CommunityViewController.swift
//  Limi
//
//  Created by maohs on 16/8/10.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

var outIndex = 0
class CommunityViewController: UITabBarController {
    
    let myQuestionVC = MyQuestionViewController()
    
    fileprivate var isHaveMessage:Bool = false

    var isInsideTurning:Bool = false
    fileprivate let loginDialog = LoginDialog()
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.navigationBar.isTranslucent = true
        
        if UserInfoManager.didLogin {
            //已登录则检查我的问答的状态
            checkMyQuestionStatus()
        }
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if appDelegate.loginType == Constant.InLoginType.myQuestionType.rawValue && appDelegate.isLoginSuccess == true{
            appDelegate.loginType = -1
            if UserInfoManager.didLogin {
               self.selectedIndex = 1
            }else {
                self.selectedIndex = 0
            }
        }else {
            if !isInsideTurning {
                self.selectedIndex = outIndex
            }
            
        }
        isInsideTurning = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.delegate = self
        self.navigationController?.interactivePopGestureRecognizer!.isEnabled = false
        self.view.backgroundColor = UIColor.white
        self.navigationItem.hidesBackButton = true
        
        // 大家问
        let commonQuestionVC = CommonQuestionViewController()
        commonQuestionVC.tabBarItem.tag = 0
        commonQuestionVC.tabBarItem = self.tabBarItem(PageName: "commonQuestion")
        
        //我的问答
        myQuestionVC.tabBarItem.tag = 1
        myQuestionVC.tabBarItem = self.tabBarItem(PageName: "myQuestion")
        myQuestionVC.delegate = self
        
        self.viewControllers = [commonQuestionVC, myQuestionVC]
    }
    
    func checkMyQuestionStatus() {
        
       let _ = LMRequestManager.requestByDataAnalyses( context: self.view, URLString: Constant.JTAPI.check_dynamic,isNeedUserTokrn :true,successHandler: { (data, status, msg) in
           
            if status != 200 {return}
            if let tempData = JSON(data as AnyObject)["data"].int {
                self.myQuestionVC.statusType = tempData
                if tempData != 0 {
                    self.isHaveMessage = true
                     self.myQuestionVC.tabBarItem = UITabBarItem(title: "我的问答", image: UIImage(named: "community_tabbar_myquestionMessage")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal), tag: 1)
                    self.isHaveMessage = false
                }else {
                    self.isHaveMessage = false
                     self.myQuestionVC.tabBarItem = UITabBarItem(title: "我的问答", image: UIImage(named: "community_tabbar_myQuestion")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal), tag: 1)
                }
                self.myQuestionVC.tabBarItem.selectedImage = UIImage(named: "community_tabbar_myQuestion_selected")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
                self.myQuestionVC.tabBarItem.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor(rgba: "#ff3a48"),  NSFontAttributeName: Constant.CustomFont.Default(size: 11)], for: UIControlState.selected)
                self.myQuestionVC.tabBarItem.setTitleTextAttributes([NSFontAttributeName: Constant.CustomFont.Default(size: 11)], for: UIControlState())
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension CommunityViewController:UITabBarControllerDelegate,MyQuestionViewControllerDelegate {
    
    fileprivate func tabBarItem(PageName name: String)->UITabBarItem
    {
        let titleMap = ["commonQuestion": "大家问", "myQuestion": "我的问答"]
        let title = titleMap[name]
        let image = UIImage(named: "community_tabbar_\(name)")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        let selectedImage = UIImage(named: "community_tabbar_\(name)_selected")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        
        let barItem = UITabBarItem(title: title, image: image, selectedImage: selectedImage)
        barItem.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor(rgba: "#ff3a48"),  NSFontAttributeName: Constant.CustomFont.Default(size: 11)], for: UIControlState.selected)
        barItem.setTitleTextAttributes([NSFontAttributeName: Constant.CustomFont.Default(size: 11)], for: UIControlState())
        
        return barItem
    }
    
    func shouldReturnCommunityTabbar(_ index: Int) {
        isInsideTurning = true
        self.selectedIndex = index
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        
        if viewController.isKind(of: MyQuestionViewController.self) {
            if !UserInfoManager.didLogin {
                self.jumpToLogin(Constant.InLoginType.myQuestionType, tipe: nil)
                return false
            }
            Statistics.count(Statistics.Answers.answers_dock2_click)
        }else {
            Statistics.count(Statistics.Answers.answers_dock1_click)
            if UserInfoManager.didLogin {
                //已登录则检查我的问答的状态
                checkMyQuestionStatus()
            }
        }
        return true
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
       

    }
    
    ///用户登录
    func jumpToLogin(_ loginType:Constant.InLoginType,tipe:String?){
        
        let time: TimeInterval = 0.5
        
        if tipe != nil{
            Utils.showError(context: self.navigationController!.view, errorStr: tipe!)
        }
        
        let delay = DispatchTime.now() + Double(Int64(time * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        
        DispatchQueue.main.asyncAfter(deadline: delay) {
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            
            appDelegate.loginType = loginType.rawValue
            self.loginDialog.controller = self
            self.loginDialog.show()
        }
    }

}
