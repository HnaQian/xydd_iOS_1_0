//
//  EditQuestionViewController.swift
//  Limi
//
//  Created by maohs on 16/8/3.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class EditQuestionViewController: BaseViewController {

    var lastVcTag = -1
    fileprivate let contentTextView:UITextView = {
       let textView = UITextView()
        textView.layer.cornerRadius = 2.0
        textView.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        textView.layer.borderColor = UIColor(rgba: Constant.common_C8_color).cgColor
        textView.layer.borderWidth = 0.5
        textView.layer.masksToBounds = true
        textView.backgroundColor = UIColor.white
        

        return textView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = "提问"
        self.view.backgroundColor = UIColor.white
        //提交
        self.navigationItem.rightBarButtonItem = CommonWidgets.NavigationRightBtn.newRightBtn("提交", target: self, action: #selector(EditQuestionViewController.rightBarButtonClicked), color: Constant.common_C2_color, font: Constant.common_F4_font)
        
        self.view.addSubview(contentTextView)
        
        let scale = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        contentTextView.frame = CGRect(x: 30 * scale, y: 30 * scale, width: Constant.ScreenSizeV2.SCREEN_WIDTH - 2 * 30 * scale, height: 340 * scale)
        contentTextView.textContainerInset = UIEdgeInsetsMake(30 * scale, 30 * scale, 0, 30 * scale)
        contentTextView.placeHolderInset = contentTextView.textContainerInset
        contentTextView.placeHolderColor = UIColor(rgba: Constant.common_C8_color)
        contentTextView.placeHolder = "写下你的送礼困惑，将有专门的送礼达人为您解答。撩妹、表白、哄丈母娘……就算是羞羞的问题也放心大胆地说出来吧！（100字内）"
        contentTextView.placeHolderFont = UIFont.systemFont(ofSize: Constant.common_F4_font)
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UITextViewTextDidChange, object: nil, queue: OperationQueue.main) { (notification) in
            if !self.contentTextView.placeHolderTextView.isHidden {
                self.contentTextView.placeHolderTextView.isHidden = true
            }
            
            if let text = self.contentTextView.text {
                if text.characters.count > 99 {
                    self.contentTextView.text = (text as NSString).substring(to: 99)
                }
            }
        }
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UITextViewTextDidBeginEditing, object: nil, queue: OperationQueue.main) { (notification) in
           self.contentTextView.placeHolderTextView.isHidden = false
        }
        
        let time: TimeInterval = 1.0
        let delay = DispatchTime.now() + Double(Int64(time * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delay) {
            self.contentTextView.becomeFirstResponder()
        }
    }
    
    func rightBarButtonClicked() {
        submitData()
    }
    
    func submitData() {
        var params = [String:AnyObject]()
        params["content"] = contentTextView.text as AnyObject?
        let _ = LMRequestManager.requestByDataAnalyses( context: self.view, URLString: Constant.JTAPI.submitQuestion,parameters:params, isNeedUserTokrn: true, isShowErrorStatuMsg: true, isShowSuccessStatuMsg: true, isNeedHud: true,successHandler: { (data, status, msg) in
            if status != 200 {return}
            outIndex = self.lastVcTag
            let _ = self.navigationController?.popViewController(animated: true)
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UITextViewTextDidChange, object: nil)
    }
    
}
