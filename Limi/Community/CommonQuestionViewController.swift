//
//  CommonQuestionViewController.swift
//  Limi
//
//  Created by maohs on 16/8/3.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//大家问
import UIKit

class CommonQuestionViewController: BaseViewController {
    
    //状态栏
    fileprivate let statusView:UIView = {
        let view = UIView()
        view.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: 20)
        view.backgroundColor = UIColor.white
        return view
    }()
    
    //大家的问答
    fileprivate var commonPageScrollView = LazyPageScrollView(verticalDistance: 2, tabItemType: TabItemType.customView)
   
    fileprivate let commonNewTableView:QuestionListView = {
       let view = QuestionListView(frame: CGRect.zero, type: 1, tag: 1)
        return view
    }()
    
    fileprivate let commonHotTableView:QuestionListView = {
        let view = QuestionListView(frame: CGRect.zero, type: 1, tag: 2)
        return view
    }()
    
    fileprivate let leftBackButton:UIButton = {
        let btn = UIButton(type: .custom)
        let scale = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        btn.frame = CGRect(x: 30 * scale, y: 25, width: 36, height: 36)
        btn.setBackgroundImage(UIImage(named: "common_back_icon"), for: UIControlState())
        btn.clipsToBounds = true
        return btn
    }()
    
    //悬浮按钮
    fileprivate let levitateQuestionButton:UIButton = {
        let btn = UIButton(type: .custom)
        let scale = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        btn.frame = CGRect(x: Constant.ScreenSizeV2.SCREEN_WIDTH - 45 * scale - 100 * scale, y: Constant.ScreenSizeV2.SCREEN_HEIGHT - 260 * scale, width: 100 * scale, height: 100 * scale)
        btn.backgroundColor = UIColor(rgba: Constant.common_C13_color)
        btn.alpha = 0.7
        btn.layer.cornerRadius = 50 * scale
        btn.layer.masksToBounds = true
        
        btn.setTitle("提问", for: UIControlState())
        btn.setTitleColor(UIColor(rgba: Constant.common_C12_color), for: UIControlState())
        btn.titleLabel?.font = UIFont.systemFont(ofSize: Constant.common_F6_font)
        btn.setImage( UIImage(named: "questionButton"), for: UIControlState())
        btn.titleEdgeInsets = UIEdgeInsetsMake(25, -10 - Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE * 6, 0, 0)
        btn.imageEdgeInsets = UIEdgeInsetsMake(-12, 10 + Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE * 4, 0, 0)
        
        return btn
    }()
    
    //变量
    fileprivate var new_last_id = 0
    fileprivate var hot_last_id = 0
    
    fileprivate var newTableViewData = [CommunityCommonModel]()
    fileprivate var hotTableViewData = [CommunityCommonModel]()
    
    fileprivate var newTotal = 0
    fileprivate var hotTotal = 0
    
    fileprivate var isFirstIn:Bool = true
    
    fileprivate let size = 20
    
    fileprivate var currentIndex = 0
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if appDelegate.loginType == Constant.InLoginType.commonQuestionType.rawValue && appDelegate.isLoginSuccess == true{
            appDelegate.loginType = -1
        }

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //状态栏
        self.view.addSubview(statusView)
        //滑动视图
        self.view.addSubview(commonPageScrollView)
        
        var frame = self.view.bounds
        frame.origin.y = 20
        frame.size.height = frame.size.height - MARGIN_20 - MARGIN_49
        commonPageScrollView.segment(["最近","最热"], views: [commonNewTableView,commonHotTableView], frame: frame)
        commonPageScrollView.delegate = self
        
        commonNewTableView.delegate = self
        commonHotTableView.delegate = self
        
        //返回按钮
        self.view.addSubview(leftBackButton)
        leftBackButton.addTarget(self, action: #selector(CommonQuestionViewController.leftBackButtonClicked), for: .touchUpInside)
        
        //悬浮按钮
        self.view.addSubview(levitateQuestionButton)
        levitateQuestionButton.addTarget(self, action: #selector(CommonQuestionViewController.levitateQuestionButtonClicked), for: .touchUpInside)
        
        commonNewTableView.tableview.addHeaderRefresh { 
            [weak self]() -> Void in
            self?.loadNewData(true)
        }
        
        commonHotTableView.tableview.addHeaderRefresh { 
            [weak self]() -> Void in
            self?.loadHotData(true)
        }
        
        commonNewTableView.tableview.addFooterRefresh { 
            [weak self]() -> Void in
            self?.loadNewData(false)
        }
        
        commonHotTableView.tableview.addFooterRefresh { 
            [weak self]() -> Void in
            self?.loadHotData(false)
        }
        
        commonNewTableView.tableview.beginHeaderRefresh()
    }

    func leftBackButtonClicked() {
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    func levitateQuestionButtonClicked() {
        if UserInfoManager.didLogin {
            Statistics.count(Statistics.Answers.answers_addquestion_click)
            let editQuestionVc = EditQuestionViewController()
            editQuestionVc.lastVcTag = 0
            editQuestionVc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(editQuestionVc, animated: true)
        }else {
            self.jumpToLogin(Constant.InLoginType.commonQuestionType, tipe: nil)
        }
       
    }
    
    func loadNewData(_ isRefresh:Bool) {
        var params = [String:AnyObject]()
        params["last_id"] = isRefresh ? 0 as AnyObject : self.new_last_id as AnyObject?
        params["type"] = 1 as AnyObject?
        
        let _ = LMRequestManager.requestByDataAnalyses(context: self.view, URLString: Constant.JTAPI.common_asklist, parameters: params,isNeedUserTokrn :true,successHandler: { (data, status, msg) in
            self.commonNewTableView.tableview.endHeaderRefresh()
            self.commonNewTableView.tableview.endFooterRefresh()
            if status != 200 {
                self.commonNewTableView.reloadUI(false)
                self.commonNewTableView.tableview.showTipe(UIScrollTipeMode.nullData)
                return
            }
            
            if let dataList = JSON(data as AnyObject)["data"]["list"].arrayObject {
                self.commonNewTableView.reloadUI(false)
                if isRefresh {
                    self.newTableViewData = [CommunityCommonModel]()
                }
                
                if let total = JSON(data as AnyObject)["data"]["total"].string {
                    self.newTotal = (total as NSString).integerValue
                }
                
                for index in 0 ..< dataList.count {
                    
                    if let reply = JSON(dataList as AnyObject)[index]["sns_reply"].dictionaryObject {
                        if let replyType = JSON(reply as AnyObject)["reply_type"].int {
                            if replyType == 1 {
                                //文字
                                self.newTableViewData.append(CommunityWordsModel(dataList[index] as! [String:AnyObject]))
                            }else if replyType == 2 {
                                //语音
                                self.newTableViewData.append(CommunitySpeechModel(dataList[index] as! [String:AnyObject]))
                            }
                        }
                    }else {
                        //未被回复
                        self.newTableViewData.append(CommunityNoneResponseModel(dataList[index] as! [String:AnyObject]))
                    }
                    if index == dataList.count - 1 {
                        if let lastId = JSON(dataList[index] as AnyObject)["id"].int {
                            self.new_last_id = lastId
                        }
                    }
                }
                
                if dataList.count < self.size || self.newTableViewData.count == self.newTotal{
                    self.commonNewTableView.tableview.showTipe(UIScrollTipeMode.noMuchMore)
                }
            }else {
                self.commonNewTableView.reloadUI(true)
                self.commonNewTableView.tableview.removeFooterRefresh()
            }
            self.commonNewTableView.reloadData(self.newTableViewData)
        }) {
            self.commonNewTableView.reloadUI(false)
            self.commonNewTableView.tableview.showTipe(UIScrollTipeMode.lostConnect)
        }
    }
    
    func loadHotData(_ isRefresh:Bool) {
        var params = [String:AnyObject]()
        params["last_id"] = isRefresh ? 0 as AnyObject: self.hot_last_id as AnyObject?
        params["type"] = 2 as AnyObject?
        let _ = LMRequestManager.requestByDataAnalyses(context: self.view, URLString: Constant.JTAPI.common_asklist, parameters: params, isNeedUserTokrn :true,successHandler: { (data, status, msg) in
            self.commonHotTableView.tableview.endHeaderRefresh()
            self.commonHotTableView.tableview.endFooterRefresh()
            if status != 200 {
                self.commonHotTableView.reloadUI(false)
                self.commonHotTableView.tableview.showTipe(UIScrollTipeMode.nullData)
                return
            }
            
            if let dataList = JSON(data as AnyObject)["data"]["list"].arrayObject {
                self.commonHotTableView.reloadUI(false)
                if isRefresh {
                    self.hotTableViewData = [CommunityCommonModel]()
                }
                
                if let total = JSON(data as AnyObject)["data"]["total"].string {
                    self.hotTotal = (total as NSString).integerValue
                }
                
                for index in 0 ..< dataList.count {
                    
                    if let reply = JSON(dataList as AnyObject)[index]["sns_reply"].dictionaryObject {
                        if let replyType = JSON(reply as AnyObject)["reply_type"].int {
                            if replyType == 1 {
                                //文字
                                self.hotTableViewData.append(CommunityWordsModel(dataList[index] as! [String:AnyObject]))
                            }else if replyType == 2 {
                                //语音
                                self.hotTableViewData.append(CommunitySpeechModel(dataList[index] as! [String:AnyObject]))
                            }
                        }
                    }else {
                        //未被回复
                        self.hotTableViewData.append(CommunityNoneResponseModel(dataList[index] as! [String:AnyObject]))
                    }
                    if index == dataList.count - 1 {
                        if let lastId = JSON(dataList[index] as AnyObject)["id"].int {
                            self.hot_last_id = lastId
                        }
                    }
                    
                    if dataList.count < self.size || self.hotTableViewData.count == self.hotTotal{
                        self.commonHotTableView.tableview.showTipe(UIScrollTipeMode.noMuchMore)
                    }
                }
            }else {
                self.commonHotTableView.reloadUI(true)
                self.commonHotTableView.tableview.removeFooterRefresh()
            }
            self.commonHotTableView.reloadData(self.hotTableViewData)
        }) {
            self.commonHotTableView.reloadUI(false)
            self.commonHotTableView.tableview.showTipe(UIScrollTipeMode.lostConnect)
        }
    }
    
    func submitStateData(_ id:Int,type:Int = 1) {
        var params = [String:AnyObject]()
        params["id"] = id as AnyObject?
        params["type"] = type as AnyObject?

        let _ = LMRequestManager.requestByDataAnalyses( context: self.view, URLString: Constant.JTAPI.check_like, parameters: params, isNeedUserTokrn: true, successHandler: { (data, status, msg) in
            
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension CommonQuestionViewController:LazyPageScrollViewDelegate,QuestionListViewDelegate {
    func lazyPageScrollView(_ pageScrollView: LazyPageScrollView, changeIndex index: Int) {
        if index == 1 {
            if self.isFirstIn {
                self.commonHotTableView.tableview.beginHeaderRefresh()
                isFirstIn = false
            }
            self.commonNewTableView.viewWillChange = !self.commonNewTableView.viewWillChange
            Statistics.count(Statistics.Answers.answers_tab2_click)
        }else {
            Statistics.count(Statistics.Answers.answers_tab1_click)
            self.commonHotTableView.viewWillChange = !self.commonHotTableView.viewWillChange
        }
        self.currentIndex = index
    }
    
    func questionListViewPraiseItemClicked(_ data: CommunityCommonModel, index: Int, type: Int, tag: Int) {
        if UserInfoManager.didLogin {
            submitStateData(data.id, type: 2)
            if tag == 1 {
                //最近的
                self.newTableViewData[index] = data
            }else {
                //最新的
                self.hotTableViewData[index] = data
            }
        }else {
            self.jumpToLogin(Constant.InLoginType.commonQuestionType, tipe: nil)
        }
        
    }
    
    func questionListViewSkimItemClicked(_ id: Int) {
        submitStateData(id)
    }
    
    func questionListViewGoodsItemClicked(_ id: Int) {
        let goods = GoodsDetailViewController.newGoodsDetailWithID(id)
        goods.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(goods, animated: true)
    }
    
}
