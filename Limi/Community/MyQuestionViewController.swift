//
//  MyQuestionViewController.swift
//  Limi
//
//  Created by maohs on 16/8/10.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//我的问答
import UIKit

@objc public protocol MyQuestionViewControllerDelegate:NSObjectProtocol {
    @objc optional func shouldReturnCommunityTabbar(_ index:Int)
}

class MyQuestionViewController: BaseViewController {

    //视图
    //状态栏
    fileprivate let statusView:UIView = {
        let view = UIView()
        view.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: 20)
        view.backgroundColor = UIColor.white
        return view
    }()
    
    //我的问答
    fileprivate var myPageScrollView = LazyPageScrollView(verticalDistance: 2, tabItemType: TabItemType.customView)
    
    //我问的
    fileprivate let myAskedTableView:QuestionListView = {
        let view = QuestionListView(frame: CGRect.zero,type: 2, tag: 1)
        return view
    }()
    
    //我查看的
    fileprivate let mySeenTableView:QuestionListView = {
        let view = QuestionListView(frame: CGRect.zero,type: 2, tag: 2)
        return view
    }()
    
    fileprivate let leftBackButton:UIButton = {
        let btn = UIButton(type: .custom)
        let scale = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        btn.frame = CGRect(x: 30 * scale, y: 25, width: 36, height: 36)
        btn.setBackgroundImage(UIImage(named: "common_back_icon"), for: UIControlState())
        btn.clipsToBounds = true
        return btn
    }()
    
    fileprivate let promptStatusView = PromptStateView(frame: CGRect.zero, type: 0)
    
    //悬浮按钮
    fileprivate let levitateQuestionButton:UIButton = {
        let btn = UIButton(type: .custom)
        let scale = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        btn.frame = CGRect(x: Constant.ScreenSizeV2.SCREEN_WIDTH - 45 * scale - 100 * scale, y: Constant.ScreenSizeV2.SCREEN_HEIGHT - 260 * scale, width: 100 * scale, height: 100 * scale)
        btn.backgroundColor = UIColor(rgba: Constant.common_C13_color)
        btn.alpha = 0.7
        btn.layer.cornerRadius = 50 * scale
        btn.layer.masksToBounds = true
        
        btn.setTitle("提问", for: UIControlState())
        btn.setTitleColor(UIColor(rgba: Constant.common_C12_color), for: UIControlState())
        btn.titleLabel?.font = UIFont.systemFont(ofSize: Constant.common_F6_font)
        btn.setImage( UIImage(named: "questionButton"), for: UIControlState())
        btn.titleEdgeInsets = UIEdgeInsetsMake(25, -10 - Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE * 6, 0, 0)
        btn.imageEdgeInsets = UIEdgeInsetsMake(-12, 10 + Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE * 4, 0, 0)
        
        return btn
    }()
    
    //变量
    fileprivate var isFirstIn:Bool = true
    fileprivate var asked_last_id = 0
    fileprivate var seen_last_id = 0
    
    fileprivate var askedTableViewData = [CommunityCommonModel]()
    fileprivate var seenTableViewData = [CommunityCommonModel]()
    
    fileprivate var asked_total = 0
    fileprivate var seen_total = 0
    
    fileprivate let size = 20
    
    //0--没有推送   1--回复   2--删除
    var statusType = 0 {
        didSet {
            
        }
    }
    
    fileprivate var currentIndex = 0
    
    weak var delegate:MyQuestionViewControllerDelegate?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        promptStatusView.iy = -promptStatusView.iheight
        if statusType != 0 {
            promptStatusView.show(statusType)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        promptStatusView.dismiss()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //状态栏
        self.view.addSubview(statusView)
        //滑动视图
        self.view.addSubview(myPageScrollView)
        var frame = self.view.bounds
        frame.origin.y = 20
        frame.size.height = frame.size.height - MARGIN_20 - MARGIN_49
         myPageScrollView.segment(["我问的","我查看过的"], views: [myAskedTableView,mySeenTableView], frame: frame)
        myPageScrollView.delegate = self
        
        myAskedTableView.delegate = self
        mySeenTableView.delegate = self
        
        //返回按钮
        self.view.addSubview(leftBackButton)
        leftBackButton.addTarget(self, action: #selector(CommonQuestionViewController.leftBackButtonClicked), for: .touchUpInside)
        
        //悬浮按钮
        self.view.addSubview(levitateQuestionButton)
        levitateQuestionButton.addTarget(self, action: #selector(MyQuestionViewController.levitateQuestionButtonClicked), for: .touchUpInside)
        
        self.view.addSubview(promptStatusView)
        promptStatusView.delegate = self
//        promptStatusView.iy = -promptStatusView.iheight
//        if statusType != 0 {
//            promptStatusView.show(statusType)
//        }
        
        myAskedTableView.tableview.addHeaderRefresh {
            [weak self]() -> Void in
            self?.loadAskedData(true)
        }
        
        mySeenTableView.tableview.addHeaderRefresh {
            [weak self]() -> Void in
            self?.loadSeenData(true)
        }
        
        myAskedTableView.tableview.addFooterRefresh {
            [weak self]() -> Void in
            self?.loadAskedData(false)
        }
        
        mySeenTableView.tableview.addFooterRefresh {
            [weak self]() -> Void in
            self?.loadSeenData(false)
        }
        
        myAskedTableView.tableview.beginHeaderRefresh()
    }
    
    func leftBackButtonClicked() {
       let _ = self.navigationController?.popViewController(animated: true)
    }
    
    func levitateQuestionButtonClicked() {
        Statistics.count(Statistics.Answers.answers_addquestion_click)
        let editQuestionVc = EditQuestionViewController()
        editQuestionVc.lastVcTag = 1
        editQuestionVc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(editQuestionVc, animated: true)
    }
    
    func loadAskedData(_ isRefresh:Bool) {
        var params = [String:AnyObject]()
        params["last_id"] = isRefresh ? 0 as AnyObject : self.asked_last_id as AnyObject?
        params["type"] = 1 as AnyObject?
        
       let _ = LMRequestManager.requestByDataAnalyses(context: self.view, URLString: Constant.JTAPI.my_asklist, parameters: params, isNeedUserTokrn : true, successHandler: { (data, status, msg) in
            self.myAskedTableView.tableview.endHeaderRefresh()
            self.myAskedTableView.tableview.endFooterRefresh()
            if status != 200 {
                self.myAskedTableView.reloadUI(false)
                self.myAskedTableView.tableview.showTipe(UIScrollTipeMode.nullData)
                return
            }
            
            if let dataList = JSON(data as AnyObject)["data"]["list"].arrayObject {
                self.myAskedTableView.reloadUI(false)
                if isRefresh {
                    self.askedTableViewData = [CommunityCommonModel]()
                }
                
                if let total = JSON(data as AnyObject)["data"]["total"].string {
                    self.asked_total = (total as NSString).integerValue
                }
                
                for index in 0 ..< dataList.count {
                    
                    if let reply = JSON(dataList as AnyObject)[index]["sns_reply"].dictionaryObject {
                        if let replyType = JSON(reply as AnyObject)["reply_type"].int {
                            if replyType == 1 {
                                //文字
                                self.askedTableViewData.append(CommunityWordsModel(dataList[index] as! [String:AnyObject]))
                            }else if replyType == 2 {
                                //语音
                                self.askedTableViewData.append(CommunitySpeechModel(dataList[index] as! [String:AnyObject]))
                            }
                        }
                    }else {
                        //未被回复
                        self.askedTableViewData.append(CommunityNoneResponseModel(dataList[index] as! [String:AnyObject]))
                    }
                    if index == dataList.count - 1 {
                        if let lastId = JSON(dataList[index] as AnyObject)["id"].int {
                            self.asked_last_id = lastId
                        }
                    }
                }
                
                if dataList.count < self.size || self.askedTableViewData.count == self.asked_total{
                    self.myAskedTableView.tableview.showTipe(UIScrollTipeMode.noMuchMore)
                }
            }else {
                self.myAskedTableView.reloadUI(true)
                self.myAskedTableView.tableview.removeFooterRefresh()
            }
            self.myAskedTableView.reloadData(self.askedTableViewData)
            
        }) {
            self.myAskedTableView.reloadUI(false)
            self.myAskedTableView.tableview.showTipe(UIScrollTipeMode.lostConnect)
        }
    }
    
    func loadSeenData(_ isRefresh:Bool) {
        var params = [String:AnyObject]()
        params["last_id"] = isRefresh ? 0 as AnyObject : self.seen_last_id as AnyObject?
        params["type"] = 2 as AnyObject?
        let _ = LMRequestManager.requestByDataAnalyses(context: self.view, URLString: Constant.JTAPI.my_asklist, parameters: params, isNeedUserTokrn : true, successHandler: { (data, status, msg) in
            self.mySeenTableView.tableview.endHeaderRefresh()
            self.mySeenTableView.tableview.endFooterRefresh()
            self.mySeenTableView.reloadUI(true)
            if status != 200 {
                self.mySeenTableView.reloadUI(false)
                self.mySeenTableView.tableview.showTipe(UIScrollTipeMode.nullData)
                return
            }

            if let dataList = JSON(data as AnyObject)["data"]["list"].arrayObject {
                self.mySeenTableView.reloadUI(false)
                if isRefresh {
                    self.seenTableViewData = [CommunityCommonModel]()
                }
                
                if let total = JSON(data as AnyObject)["data"]["total"].string {
                    self.seen_total = (total as NSString).integerValue
                }
                
                for index in 0 ..< dataList.count {
                    
                    if let reply = JSON(dataList as AnyObject)[index]["sns_reply"].dictionaryObject {
                        if let replyType = JSON(reply as AnyObject)["reply_type"].int {
                            if replyType == 1 {
                                //文字
                                self.seenTableViewData.append(CommunityWordsModel(dataList[index] as! [String:AnyObject]))
                            }else if replyType == 2 {
                                //语音
                                self.seenTableViewData.append(CommunitySpeechModel(dataList[index] as! [String:AnyObject]))
                            }
                        }
                    }else {
                        //未被回复
                        self.seenTableViewData.append(CommunityNoneResponseModel(dataList[index] as! [String:AnyObject]))
                    }
                    if index == dataList.count - 1 {
                        if let lastId = JSON(dataList[index] as AnyObject)["id"].int {
                            self.seen_last_id = lastId
                        }
                    }
                }
                
                if dataList.count < self.size || self.seenTableViewData.count == self.seen_total{
                    self.mySeenTableView.tableview.showTipe(UIScrollTipeMode.noMuchMore)
                }
            }else {
                self.mySeenTableView.reloadUI(true)
                self.mySeenTableView.tableview.removeFooterRefresh()
            }
            self.mySeenTableView.reloadData(self.seenTableViewData)

        }) {
            self.mySeenTableView.reloadUI(false)
           self.mySeenTableView.tableview.showTipe(UIScrollTipeMode.lostConnect)
        }
    }

    func submitStateData(_ id:Int,type:Int = 1) {
        var params = [String:AnyObject]()
        params["id"] = id as AnyObject?
        params["type"] = type as AnyObject?
        
        let _ = LMRequestManager.requestByDataAnalyses( context: self.view, URLString: Constant.JTAPI.check_like, parameters: params, isNeedUserTokrn: true, successHandler: { (data, status, msg) in
            
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

@objc public protocol PromptStateViewDelegate:NSObjectProtocol {
    @objc optional func promptStateViewClicked(_ type:Int)
}

class PromptStateView: UIView {
    
    fileprivate let titleLabel:UILabel = {
       let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        lbl.textColor = UIColor(rgba: Constant.common_C12_color)
        lbl.textAlignment = .center
        lbl.isUserInteractionEnabled = true
        return lbl
    }()
    
    let deleteButton:UIButton = {
       let btn = UIButton(type: .custom)
        btn.setImage(UIImage(named: "questionStateDelete"), for: UIControlState())
        return btn
    }()
    
    weak var delegate:PromptStateViewDelegate?
    
    fileprivate var scale = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
    //1--回复   2--删除
    fileprivate var type = 0
    init(frame: CGRect,type:Int) {
        super.init(frame: frame)
        self.type = type
        setUpUI()
    }
    
    func setUpUI() {
        self.backgroundColor = UIColor(rgba: Constant.common_C1_color)
        self.alpha = 0.8
        self.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: 80 * scale)
        
        self.addSubview(titleLabel)
        self.addSubview(deleteButton)
        
        if self.type == 1 {
            titleLabel.text = "您提的问题已被回复"
        }else if self.type == 2 {
            titleLabel.text = "您提的问题已被删除"
        }else {
            titleLabel.text = ""
        }
        
        titleLabel.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH - 120 * scale, height: 80 * scale)
        titleLabel.icenterX = Constant.ScreenSizeV2.SCREEN_WIDTH / 2
        deleteButton.frame = CGRect(x: Constant.ScreenSizeV2.SCREEN_WIDTH - 100 * scale, y: 0, width: 80 * scale, height: 80 * scale)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(PromptStateView.promptViewClicked))
        titleLabel.addGestureRecognizer(tapGesture)
        
        deleteButton.addTarget(self, action: #selector(PromptStateView.deleteButtonClicked), for: .touchUpInside)
    }
    
    func promptViewClicked() {
        delegate?.promptStateViewClicked!(1)
    }
    
    func deleteButtonClicked() {
        delegate?.promptStateViewClicked!(2)
    }
    
    func show(_ type:Int) {
        self.type = type
        if self.type == 1 {
            titleLabel.text = "您提的问题已被回复"
        }else if self.type == 2 {
            titleLabel.text = "您提的问题已被删除"
        }else {
            titleLabel.text = ""
        }
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 1, options: .curveLinear, animations: { 
                self.layer.position = CGPoint(x: self.layer.position.x, y: self.layer.position.y + 80 * self.scale + 64)
            }) { (finish) in
                
        }
    }
    
    func dismiss() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 1, options: .curveLinear, animations: {
            self.layer.position = CGPoint(x: self.layer.position.x, y: self.layer.position.y - 80 * self.scale - 64)
        }) { (finish) in
            
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}


extension MyQuestionViewController:LazyPageScrollViewDelegate,QuestionListViewDelegate,PromptStateViewDelegate {
    
    func promptStateViewClicked(_ type: Int) {
        if type == 1 {
            //回复
            let myQuestionStateVc = MyQuestionStateViewController()
            myQuestionStateVc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(myQuestionStateVc, animated: true)
        }
        self.statusType = 0
        promptStatusView.dismiss()
    }
    
    func lazyPageScrollView(_ pageScrollView: LazyPageScrollView, changeIndex index: Int) {
        if index == 1 {
            if isFirstIn {
                self.mySeenTableView.tableview.beginHeaderRefresh()
                isFirstIn = false
            }
            self.myAskedTableView.viewWillChange = !self.myAskedTableView.viewWillChange
            Statistics.count(Statistics.Answers.answers_mytab2_click)
        }else {
            Statistics.count(Statistics.Answers.answers_mytab1_click)
            self.mySeenTableView.viewWillChange = !self.mySeenTableView.viewWillChange
        }
        self.currentIndex = index
    }
    
    func questionListViewEmptyButtonClicked(_ type: Int, tag: Int) {
        if tag == 1 {
            //我问的
            let editQuestionVc = EditQuestionViewController()
            editQuestionVc.lastVcTag = 1
            editQuestionVc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(editQuestionVc, animated: true)
        }else {
            //我查看的
            delegate?.shouldReturnCommunityTabbar!(0)
        }
        
    }
    
    func questionListViewPraiseItemClicked(_ data: CommunityCommonModel, index: Int, type: Int, tag: Int) {
        
        submitStateData(data.id, type: 2)
        if tag == 1 {
            //我问的
            self.askedTableViewData[index] = data
        }else {
            //我查看的
            self.seenTableViewData[index] = data
        }
    }
    
    
    func questionListViewSkimItemClicked(_ id: Int) {
        submitStateData(id)
    }
    
    func questionListViewGoodsItemClicked(_ id: Int) {
        let goods = GoodsDetailViewController.newGoodsDetailWithID(id)
        goods.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(goods, animated: true)
    }
    
}
