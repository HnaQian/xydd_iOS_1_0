//
//  MyQuestionStateViewController.swift
//  Limi
//
//  Created by maohs on 16/8/3.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//问答动态
import UIKit

class MyQuestionStateViewController: BaseViewController {

    fileprivate let questionTableView = QuestionListView(frame: CGRect.zero, type: 0, tag: 0)
    
    fileprivate let footer = QuestionListFooter()
    
    fileprivate var questionStateData = [CommunityCommonModel]()
    fileprivate var lastId = 0
    fileprivate let size = 20
    fileprivate var total = 0
    fileprivate var firstClickMoreButton:Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = "问答动态"
        self.view.addSubview(questionTableView)
        var frame = self.view.bounds
        frame.size.height -= 64
        questionTableView.frame = frame
        questionTableView.delegate = self
        footer.moreButton.addTarget(self, action: #selector(MyQuestionStateViewController.moreButtonClicked), for: .touchUpInside)
        loadData(true)
    }

    func loadData(_ isRefresh:Bool) {
        var params = [String:AnyObject]()
        params["last_id"] = isRefresh ? (0 as AnyObject?) : (self.lastId as AnyObject?)
        params["type"] = isRefresh ? (1 as AnyObject?) : (2 as AnyObject?)
        
        let _ = LMRequestManager.requestByDataAnalyses(context: self.view, URLString: Constant.JTAPI.dynamic_noticelist, parameters: params, isNeedUserTokrn: true, isShowErrorStatuMsg: true, isNeedHud: true,successHandler: { (data, status, msg) in
            
            if status != 200 {return}
            if isRefresh {
                self.updateDataState()
            }
            
            if let dataList = JSON(data as AnyObject)["data"]["list"].arrayObject {
                self.questionTableView.reloadUI(false)
                if isRefresh || self.firstClickMoreButton{
                    self.questionStateData = [CommunityCommonModel]()
                }
                
                //保证第三次不需要初始化
                if isRefresh == false && self.firstClickMoreButton {
                    self.firstClickMoreButton = false
                }
                
                if let total = JSON(data as AnyObject)["data"]["total"].string {
                    self.total = (total as NSString).integerValue
                }
                
                for index in 0 ..< dataList.count {
                    
                    if index == dataList.count - 1 {
                        if let lastId = JSON(dataList[index] as AnyObject)["id"].int {
                            self.lastId = lastId
                        }
                    }
                    
                    if let statusString = JSON(dataList as AnyObject)[index]["status"].string {
                        if (statusString as NSString).integerValue == 2 {
                            //删除
                            self.questionStateData.append(CommunityNoneResponseModel(dataList[index] as! [String:AnyObject]))
                            continue
                        }
                    }
                    
                    if let reply = JSON(dataList as AnyObject)[index]["sns_reply"].dictionaryObject {
                        if let replyType = JSON(reply as AnyObject)["reply_type"].int {
                            if replyType == 1 {
                                //文字
                                self.questionStateData.append(CommunityWordsModel(dataList[index] as! [String:AnyObject]))
                            }else if replyType == 2 {
                                //语音
                                self.questionStateData.append(CommunitySpeechModel(dataList[index] as! [String:AnyObject]))
                            }
                        }
                    }
                }
                
                if !isRefresh && self.questionStateData.count == self.total{
                    self.questionTableView.tableview.tableFooterView = nil
                }else {
                    self.questionTableView.tableview.tableFooterView = self.footer
                }
            }else {
             self.questionTableView.tableview.tableFooterView = nil
            }
            self.questionTableView.reloadData(self.questionStateData)
            
        })
    }
    
    func updateDataState() {
       let _ = LMRequestManager.requestByDataAnalyses(context: self.view, URLString: Constant.JTAPI.update_noticeStatus, isNeedUserTokrn: true, isShowErrorStatuMsg: true, isNeedHud: true, successHandler: { (data, status, msg) in
            
        })
    }
    
    func submitStateData(_ id:Int,type:Int = 1) {
        var params = [String:AnyObject]()
        params["id"] = id as AnyObject?
        params["type"] = type as AnyObject?
        
       let _ = LMRequestManager.requestByDataAnalyses( context: self.view, URLString: Constant.JTAPI.check_like, parameters: params, isNeedUserTokrn: true, successHandler: { (data, status, msg) in
            
        })
    }
    
    func moreButtonClicked() {
        loadData(false)
    }
    
    
    override func backItemAction() {
        super.backItemAction()
        outIndex = 1
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension MyQuestionStateViewController:QuestionListViewDelegate {
    func questionListViewPraiseItemClicked(_ data: CommunityCommonModel, index: Int, type: Int, tag: Int) {
        submitStateData(data.id, type: 2)
        self.questionStateData[index] = data
    }
    
    func questionListViewSkimItemClicked(_ id: Int) {
        submitStateData(id)
    }
    
    func questionListViewGoodsItemClicked(_ id: Int) {
        let goods = GoodsDetailViewController.newGoodsDetailWithID(id)
        goods.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(goods, animated: true)
    }

}

class QuestionListFooter: UIView {
    
    let moreButton:UIButton = {
       let btn = UIButton(type: .custom)
        btn.setTitle("查看更多", for: UIControlState())
        btn.titleLabel?.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        btn.setTitleColor(UIColor(rgba: Constant.common_C6_color), for: UIControlState())
        
        btn.layer.cornerRadius = 3.0
        btn.layer.borderWidth = 1.0
        btn.layer.borderColor = UIColor(rgba: Constant.common_C6_color).cgColor
        btn.layer.masksToBounds = true
        
        return btn
    }()
    
    override init(frame: CGRect) {
        super.init(frame: CGRect.zero)
        
        setUpUI()
    }
    
    func setUpUI() {
        let scale = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        self.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: 110 * scale)
        
        self.addSubview(moreButton)
        moreButton.frame = CGRect(x: 0, y: 20 * scale, width: 220 * scale, height: 70 * scale)
        moreButton.icenterX = self.icenterX
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
