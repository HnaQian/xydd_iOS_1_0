//
//  QuestionListView.swift
//  Limi
//
//  Created by maohs on 16/8/10.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

@objc protocol QuestionListViewDelegate:NSObjectProtocol {
    @objc optional func questionListViewEmptyButtonClicked(_ type:Int,tag:Int)
    @objc optional func questionListViewPraiseItemClicked(_ data: CommunityCommonModel, index: Int,type:Int,tag:Int)
    @objc optional func questionListViewSkimItemClicked(_ id:Int)
    @objc optional func questionListViewGoodsItemClicked(_ id: Int)
}

class QuestionListView: UIView,UITableViewDataSource,UITableViewDelegate {
    
    fileprivate var emptyView:QuesTionListHeader?
    
    //常规变量
    
    weak var delegate:QuestionListViewDelegate?
    
    let tableview = UITableView()
    fileprivate var type = 0
    fileprivate var viewTag = 0
    
    fileprivate var lastIndex = 0
    
    var viewWillChange:Bool = false {
        didSet {
            changeSpeechState()
        }
    }
    
    fileprivate var tabViewArray = [CommunityCommonModel]()
    
    //type  1 大家问       2 我的问答
    //tag   1 最近/我问的   2 最热/我查看过的
    init(frame:CGRect,type:Int,tag:Int) {
        super.init(frame: frame)
        self.viewTag = tag
        self.type = type
        setUpUI()
    }
    
    func setUpUI() {
        self.addSubview(tableview)
        tableview.snp_updateConstraints(closure: { (make) in
            let _ = make.top.equalTo(self)
            let _ = make.left.equalTo(self)
            let _ = make.right.equalTo(self)
            let _ = make.bottom.equalTo(self)
        })
        tableview.dataSource = self
        tableview.delegate = self
        tableview.backgroundColor = UIColor.clear
        tableview.backgroundView = nil
        tableview.separatorStyle = .none
        
        tableview.register(CommunityNoneResponseCell.self, forCellReuseIdentifier: CommunityNoneResponseCell.reuseIdentifier)
        tableview.register(CommunitySpeechCell.self, forCellReuseIdentifier: CommunitySpeechCell.reuseIdentifier)
        tableview.register(CommunityWordsCell.self, forCellReuseIdentifier: CommunityWordsCell.reuseIdentifier)
    }
    
    func setUpEmptyUI () {
        if let _ = self.emptyView {
            
        }else {
            self.emptyView = QuesTionListHeader()
            tableview.tableHeaderView = self.emptyView
            self.emptyView!.emptyButton.addTarget(self, action: #selector(QuestionListView.emptyButtonClicked), for: .touchUpInside)
        }
    }
    
    func emptyButtonClicked() {
        delegate?.questionListViewEmptyButtonClicked!(self.type, tag: self.viewTag)
    }
    
    func reloadUI(_ isEmpty:Bool) {
        if !isEmpty {
            self.tableview.tableHeaderView = nil
            return
        }
        let tempTuple = (self.type,self.viewTag)
         setUpEmptyUI()
        switch tempTuple {
        case (1,_):
            self.emptyView!.emptyTitleLabel.text = "达人闭关修炼中"
            self.emptyView!.emptyContentLabel.text = "一大波精彩问答正在袭来~"
            self.emptyView!.emptyButton.isHidden = true
        case (2,1):
            self.emptyView!.emptyTitleLabel.text = "你还没有问过"
            self.emptyView!.emptyContentLabel.text = "表害羞，勇敢的提问吧~"
            self.emptyView!.emptyButton.setTitle("立即提问", for: UIControlState())
            self.emptyView!.emptyButton.isHidden = false
        case (2,2):
            self.emptyView!.emptyTitleLabel.text = "你还没有听过"
            self.emptyView!.emptyContentLabel.text = "不想听听达人们Sexy的声音嘛？"
            self.emptyView!.emptyButton.setTitle("去听听", for: UIControlState())
            self.emptyView!.emptyButton.isHidden = false
        default:
            break
        }
        
    }
    
    func reloadData(_ dataArray:[CommunityCommonModel]) {
        self.tabViewArray = [CommunityCommonModel]()
        self.tabViewArray = dataArray
        self.tableview.reloadData()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tabViewArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.tabViewArray.count > 0 {
            if self.tabViewArray[(indexPath as NSIndexPath).row].isRetract {
                return self.tabViewArray[(indexPath as NSIndexPath).row].retractCellHeight
            }
            return self.tabViewArray[(indexPath as NSIndexPath).row].totalCellHeight
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:CommunityBaseCell = tableView.dequeueReusableCell(withIdentifier: self.tabViewArray[(indexPath as NSIndexPath).row].classMap().reuseIdentifier) as! CommunityBaseCell
        cell.refreshWithData(self.tabViewArray[(indexPath as NSIndexPath).row],index: (indexPath as NSIndexPath).row)
        cell.delegate = self
        return cell
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension QuestionListView:CommunityBaseCellDelegate {
    func communityBaseCellPraiseButtonChanged(_ data: CommunityCommonModel, index: Int) {
        delegate?.questionListViewPraiseItemClicked!(data, index: index, type: self.type, tag: self.viewTag)
    }
    
    func communityBaseCellSkimButtonChanged(_ id: Int ,type: Int, index: Int) {
        if type == 1 {
            //文字  需要扩展或折叠
            self.tabViewArray[index].isRetract = !self.tabViewArray[index].isRetract
            let indexPath = IndexPath(row: index, section: 0)
            self.tableview.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
        }else if type == 2 {
            self.tabViewArray[index].is_play = !self.tabViewArray[index].is_play
            if lastIndex != index {
                self.tabViewArray[lastIndex].is_play = false
                let indexPath = IndexPath(row: lastIndex, section: 0)
                self.tableview.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
            }
         
            LMOnlineAudio.shareInstance.onlineAudioFinished = { [weak self] in
                self?.tabViewArray[index].is_play = false
                let indexPath = IndexPath(row: index, section: 0)
                self?.tableview.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
            }
        }
        lastIndex = index
        delegate?.questionListViewSkimItemClicked!(id)
    }
    override func willMove(toWindow newWindow: UIWindow?) {
        super.willMove(toWindow: newWindow)
        if self.tabViewArray.count > lastIndex {
            self.tabViewArray[lastIndex].is_play = false
            let indexPath = IndexPath(row: lastIndex, section: 0)
            self.tableview.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
            LMOnlineAudio.shareInstance.stopAudio()
        }
    }
    
    func changeSpeechState() {
        if self.tabViewArray.count > lastIndex {
            self.tabViewArray[lastIndex].is_play = false
            let indexPath = IndexPath(row: lastIndex, section: 0)
            self.tableview.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
            LMOnlineAudio.shareInstance.stopAudio()
        }
    }
    
    func communityBaseCellGoodsClicked(_ id: Int) {
       delegate?.questionListViewGoodsItemClicked!(id)
    }
    

}

class QuesTionListHeader:UIView {
    
    fileprivate let emptyImage:UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "hornImage")
        
        return image
    }()
    
    let emptyTitleLabel:UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        lbl.textColor = Constant.Theme.Color14
        lbl.textAlignment = .center
        
        return lbl
    }()
    let emptyContentLabel:UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        lbl.textColor = Constant.Theme.Color14
        lbl.textAlignment = .center
        
        return lbl
    }()
    
    let emptyButton:UIButton = {
        let btn = UIButton(type: .custom)
        btn.setTitleColor(UIColor(rgba: Constant.common_C1_color), for: UIControlState())
        btn.titleLabel?.font = UIFont.systemFont(ofSize: Constant.common_F2_font)
        btn.layer.borderColor = UIColor(rgba: Constant.common_C1_color).cgColor
        btn.layer.borderWidth = 1.0
        btn.layer.cornerRadius = 3.0
        btn.layer.masksToBounds = true
        
        return btn
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setUpUI()
    }
    
    func setUpUI() {
        for view in [emptyImage,emptyTitleLabel,emptyContentLabel,emptyButton] {
            self.addSubview(view)
        }
        let scale = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        emptyImage.frame = CGRect(x: 0, y: 200 * scale, width: 290 * scale, height: 240 * scale)
        emptyImage.icenterX = Constant.ScreenSizeV2.SCREEN_WIDTH / 2
        
        emptyTitleLabel.frame = CGRect(x: 0, y: emptyImage.ibottom, width: Constant.ScreenSizeV2.SCREEN_WIDTH - MARGIN_20, height: 15)
        emptyTitleLabel.icenterX = Constant.ScreenSizeV2.SCREEN_WIDTH / 2
        
        emptyContentLabel.frame = CGRect(x: 0, y: emptyTitleLabel.ibottom + scale * 10, width: Constant.ScreenSizeV2.SCREEN_WIDTH - MARGIN_20, height: 15)
        emptyContentLabel.icenterX = Constant.ScreenSizeV2.SCREEN_WIDTH / 2
        
        
        emptyButton.frame = CGRect(x: 0, y: emptyContentLabel.ibottom + 20 * scale, width: 270 * scale, height: 80 * scale)
        emptyButton.icenterX = Constant.ScreenSizeV2.SCREEN_WIDTH / 2

        self.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: Constant.ScreenSizeV2.SCREEN_HEIGHT - MARGIN_64 - MARGIN_49)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
