//
//  CommunitySpecialModel.swift
//  Limi
//
//  Created by maohs on 16/8/11.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class CommunityNoneResponseModel: CommunityCommonModel {
    
    
    override func classMap() ->CommunityBaseCell.Type {
        return CommunityNoneResponseCell.self
    }
    
    override func getModelType()->Int{
        return 1
    }
    
}

class CommunitySpeechModel: CommunityCommonModel {
    
    
    override func classMap() ->CommunityBaseCell.Type {
        return CommunitySpeechCell.self
    }
    
    override func getModelType() ->Int {
        return 2
    }
}

class CommunityWordsModel: CommunityCommonModel {
    
    override func classMap() ->CommunityBaseCell.Type {
        return CommunityWordsCell.self
    }
    
    override func getModelType() -> Int {
        return 3
    }
}