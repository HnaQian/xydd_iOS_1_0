//
//  HomeStyle2ArticleModel.swift
//  Limi
//
//  Created by Richie on 16/6/13.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//达人攻略:带商品的攻略
import UIKit

class HomeStyle2ArticleModel: HomeCommonArticleModel {
    
    override func classMap() -> HomeBaseTableViewCell.Type {
        return HomeArticleSpecialStyle2Cell.self
    }
    
    override func getCellHeight() ->CGFloat{return HomeArticleSpecialStyle2Cell.height}
    
    override func getModelType() -> Int {
        return 2
    }
}
