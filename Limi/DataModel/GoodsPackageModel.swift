//
//  GoodsPackageModel.swift
//  Limi
//
//  Created by Richie on 16/6/21.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

//来自自选礼盒web页面的的 goodsList中得商品模型
import UIKit

class GoodsPackageModel: BaseDataMode {
    
    /*
     'goods_list':[
     {
     'gid': 4100,
     'goods_sub_id':4544,
     'img':'http://up.xydd.co/14447184538185.jpg',
     'name':'拍立得钻石菱格迷你相册',
     'attr':'颜色:灰色;长度:240mm',
     'price':'120.5'
     },
     */
    
    
    var gid:Int32     =  0
    
    var img     = NULL_STRING
    
    var name    = NULL_STRING
    
    var attr    = NULL_STRING
    
    var price:Float32   = 0
    
    var discount_price:Float32   = 0
    
    var goods_sub_id:Int32 = 0
}


extension GoodsPackageModel:GoodsGenericModel{
    
    
    var goodsId:Int{return Int(self.gid)}
    
    var goodsSubId: Int{return Int(self.goods_sub_id)}
    
    //商品名称
    var goodsName:String{return self.name}
    
    var goodsImage:String{return self.img}
    
    var goodsOfOrderType:Int{return 4}
    
    ///Extension: 提交订单时的数量:选择的商品规格数量
    var selectedGoodsSubModelCount:Int{return 1}
    
    ///Extension: 提交订单时的单价:选择的规格价格(也可能没有选择规格，返回结果已经把此逻辑计算在内)
    var selelctedGoodsSubModelPrice:Double{return Double(self.discount_price)}
    
    ///Extension: 原价：失效的价格
    var expirationPrice:Double{return Double(self.price)}
    
    /// Extension: 现价：未选商品规格的实际支付价格
    var currentPrice:Double{return Double(self.price)}
    
    /// Extension: 当前选择的规格的名称数组
    var selectedGoodsAttrNames:[String]{return [attr]}
    
    private var selectedGoodsSubIds:[Int32]{return [goods_sub_id]}
}
