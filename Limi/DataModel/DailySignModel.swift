//
//  DailySignModel.swift
//  Limi
//
//  Created by maohs on 16/5/26.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class DailySignModel: BaseDataMode {
    /*
     "user_point": 0,
     "checkin_times": 1,
     "next_day": 4,
     "next_point": 10,
     "day_tips": "2016年05月25日",
     "day_now": "2016-05-25",
     "rule_url": "http://test.wechat.giftyou.me/issue_checkinRule.html",
     "date_list": [
     {
     "checkin_times" : 0,
     "gift_tip" : 0,
     "week" : 7,
     "day" : 1,
     "checkin_status" : 0,
     "date" : "2016-05-01"
     },
     ]
     */
    
    
    var day_now     = NULL_STRING               //今天的日期
    var day_tips    = NULL_STRING              //今天的年月日
    var rule_url    = NULL_STRING              //签到规则链接
    var next_day    = NULL_INT                 //距离下次奖励天数
    var next_point  = NULL_INT               //下次特殊奖励的积分数
    var user_point  = NULL_INT               //用户积分
    
    var checkin_times = NULL_INT            //连续签到天数
    
    var date_list:[DailySignDate_listModel]?
    
    
    override func customAnalyis(_ data: [String : AnyObject]) {
        
        if let dateArray = data["date_list"] as? [[String :AnyObject]] {
            if dateArray.count > 0 {
                date_list = [DailySignDate_listModel]()
                for index in 0 ..< dateArray.count {
                    let singleDict = DailySignDate_listModel(dateArray[index])
                    date_list?.append(singleDict)
                }
            }
        }
    }
    
}

class DailySignDate_listModel: BaseDataMode {
    
    var day     = NULL_INT                  //当前月份  天
    var week    = NULL_INT                 //当前星期
    var date    = NULL_STRING              //当前月份  详细时间
    
    var gift_tip        = NULL_INT         //额外奖励  0,5,10,15等
    var checkin_times   = NULL_INT        //签到天数
    var checkin_status  = NULL_INT       //签到状态  0未签到  1已签到
}
