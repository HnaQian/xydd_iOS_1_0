//
//  HomeStyle1ArticleModel.swift
//  Limi
//
//  Created by Richie on 16/6/13.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//package

//自选礼盒
import UIKit

class HomeStyle1ArticleModel: HomCellBaseDataModel {
    
    
    /*
     {
     attributes =                     {
     img = "http://up.xinyidiandian.com/14645669813225.jpg";
     "package_id" = 21;
     positon = 4;
     };
     children =                     (
     {
     img = "http://up.xydd.co/14448132968309.jpg";
     name = "CHANEL\U9999\U5948\U513f \U53ef\U53ef\U5c0f\U59d0\U5507\U818f \U6c34\U4eae\U7cfb\U5217\U53e3\U7ea2";
     },
     */
    
    var attributes: HomeStyle1ArticleAttributes?
    var children:[HomeStyle1ArticleGoods]?
    
    override func customAnalyis(_ data: [String : AnyObject]) {
        
        if  let attr = JSON(data as AnyObject)["attributes"].dictionaryObject{
            self.attributes = HomeStyle1ArticleAttributes(attr)
        }
        
        if  let ary = JSON(data as AnyObject)["children"].arrayObject{
            if ary.count > 0{
                self.children = [HomeStyle1ArticleGoods]()
                for dic in ary{
                    self.children?.append(HomeStyle1ArticleGoods(dic as! [String:AnyObject]))
                }
            }
        }
    }
    
    
    override func classMap() -> HomeBaseTableViewCell.Type {
        return HomeArticleSpecialStyle1Cell.self
    }
    
    override func getModelType() -> Int {
        return 3
    }
    
    
    override func getCellHeight() ->CGFloat{
        
        if let list = children{
            if list.count > 0{
                return HomeArticleSpecialStyle1Cell.height
            }
        }
        return HomeArticleSpecialStyle1Cell.nullListHeight
    }
}

class HomeStyle1ArticleGoods: BaseDataMode {
    var img  = NULL_STRING
    var name = NULL_STRING
}


class HomeStyle1ArticleAttributes: BaseDataMode {
    /*
     href = "http://test.wechat.giftyou.me/package/item?id=21&need_token=1";
     img = "http://up.xydd.co/14645669813225.jpg";
     "package_id" = 21;
     position = 4;
     
     href="http://test.wechat.giftyou.me/package/item?id=1&need_token=1";
     img="http://up.xydd.co/14664982157886.png";
     "package_id"=1;
     position=2;
     
     */
    var href        = NULL_STRING
    var img         = NULL_STRING
    var position    = NULL_INT
    var package_id  = NULL_INT
}
