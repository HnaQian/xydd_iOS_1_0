//
//  HomeRecommandTracticModel.swift
//  Limi
//
//  Created by Richie on 16/5/18.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//
//首页推荐攻略的dataModel

import UIKit

class HomeRecommandTracticModel: BaseDataMode {
    /*
     href = "http://wechat.giftyou.me/article_item?id=745&app=1";
     src = "http://up.xinyidiandian.com/14620715026574.jpg";
     text = "\U72ec\U5bb6\U793c\U76d211";
     "text_color" = "#7AAE79";
     title = "\U5927\U793c\U5305";
     "title_color" = "#12171F";
     */
    
    var href    = NULL_STRING
    var src     = NULL_STRING
    var text    = NULL_STRING
    var title   = NULL_STRING
    
    var top     = NULL_FLOAT
    var left    = NULL_FLOAT
    var right   = NULL_FLOAT
    var width   = NULL_FLOAT
    var height  = NULL_FLOAT
    var bottom  = NULL_FLOAT
    
    var text_color  = NULL_STRING
    var title_color = NULL_STRING
}
