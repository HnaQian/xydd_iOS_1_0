//
//  BaseDataMode.swift
//  Limi
//
//  Created by Richie on 16/3/4.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//所有数据模型的基类

import UIKit

//空int
let NULL_INT:Int = 0

//Double
let NULL_DOUBLE:Double = 0.0
let NULL_FLOAT:Float = 0.0

//空字符串的标记
let NULL_STRING = ""

class BaseDataMode: NSObject {
    
    fileprivate var _original_data:[String:AnyObject]?
    
    /// 原始数据：init中的参数
    var ORIGINAL_DATA:[String:AnyObject]?{return _original_data}
    
    init(_ data:[String:AnyObject]! = nil) {
        super.init()
        _original_data = data
        
        
        if data != nil{
            for (key,value) in data{
                if let _  = value as? String{print("\(key) = value is string : \(value)")}
                else if let _  = value as? Int{print("\(key) = value is Int : \(value)")}
                else if let _  = value as? Double{print("\(key) = value is double : \(value)")}
            }
            
            if !self.totalCustomAnalyis(data){
                self.setValuesForKeys(data)
                self.customAnalyis(data)
            }
        }
    }
    
    /**
     自定义解析数据，会自动填充匹配的字段
     */
    func customAnalyis(_ data:[String:AnyObject]){}
    
    /**
     自定义数据解析，不会自动填充匹配的字段
     */
    ///注意：子类重新此函数返回值必须为：true
    func totalCustomAnalyis(_ data:[String:AnyObject])->Bool{return false}
    
    override func setValue(_ value: Any?, forUndefinedKey key: String) {}
    
    override func setNilValueForKey(_ key: String) {}
}


