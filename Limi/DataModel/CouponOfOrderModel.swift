//
//  CouponOfOrderModel.swift
//  Limi
//
//  Created by Richie on 16/3/18.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//订单中的红包

import UIKit

class CouponOfOrderModel: CouponModel {
    
    /*
     "Uid": 71,
     "Oid": 116557269,
     "Discount": 0,
     "Content": "",
     "Use_time": "2016-03-14 11:40:01",
     "Status": 2,
     "Status_name": ""
     */
    
    
    /*
     "User_coupon": {
     "Id": 392274,
     "Name": "lyc1",
     "Cid": 191,
     "Uid": 71,
     "Oid": 116557269,
     "Par_value": 10,
     "Min_value": 0,
     "Discount": 0,
     "Min_title": "",
     "Scope": 1,
     "Scope_name": "www",
     "Content": "",
     "Url": "",
     "Is_discount": 1,
     "Start_time": "2016-03-14 00:00:00",
     "End_time": "2016-03-21 23:59:59",
     "Use_time": "2016-03-14 11:40:01",
     "Time_title": "",
     "Status": 2,
     "Status_name": ""
     },
     */
    
    
    var uid = NULL_INT
    
    var oid = NULL_INT
    
    var status  = NULL_INT
    
    var use_time    = NULL_STRING
    
    var status_name = NULL_STRING
}
