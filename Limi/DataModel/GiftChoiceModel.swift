//
//  GiftChoiceModel.swift
//  Limi
//
//  Created by 千云锋 on 16/8/9.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit


class GiftChoicePeopleModel: BaseDataMode {

    
    /*
     "ge_xin" =     (
     {
     name = "\U4e2a\U602701";
     sort = 1;
     },
     {
     name = "\U4e2a\U602702";
     sort = 2;
     }
     );
     img = "http://xinyidiandian.com/images/desc/desc1.png";
     name = "\U8eab\U4efd01";
     sort = 1;
     */
    var ge_xin  = [GiftGexinModel]()
    var name    = NULL_STRING
//    var sort    = NULL_STRING
    var img     = NULL_STRING
    
    
    override func customAnalyis(_ data: [String : AnyObject]){
        if let attributes = data["ge_xin"] as? [[String:AnyObject]]{
            if attributes.count > 0{
                self.ge_xin = [GiftGexinModel]()
                let _ = attributes.map{self.ge_xin.append(GiftGexinModel($0))}
            }
        }

    
    }
    
}

class GiftGexinModel: BaseDataMode {
    
    var name    = NULL_STRING
//    var sort    = NULL_INT
    
}

class GiftChoiceSupplementModel: BaseDataMode {
    var sex        = [GiftChoiceSubModel]()
    var age        = [GiftChoiceSubModel]()
    var xin_zhuo   = [GiftChoiceSubModel]()
    var chang_he   = [GiftChoiceSubModel]()
    
    override func customAnalyis(_ data: [String : AnyObject]){
        if let attributes = data["sex"] as? [[String:AnyObject]]{
            if attributes.count > 0{
                self.sex = [GiftChoiceSubModel]()
                let _ = attributes.map{self.sex.append(GiftChoiceSubModel($0))}
            }
        }
        if let attributes = data["age"] as? [[String:AnyObject]]{
            if attributes.count > 0{
                self.age = [GiftChoiceSubModel]()
                let _ = attributes.map{self.age.append(GiftChoiceSubModel($0))}
            }
        }
        if let attributes = data["xin_zhuo"] as? [[String:AnyObject]]{
            if attributes.count > 0{
                self.xin_zhuo = [GiftChoiceSubModel]()
                let _ = attributes.map{self.xin_zhuo.append(GiftChoiceSubModel($0))}
            }
        }
        if let attributes = data["chang_he"] as? [[String:AnyObject]]{
            if attributes.count > 0{
                self.chang_he = [GiftChoiceSubModel]()
                let _ = attributes.map{self.chang_he.append(GiftChoiceSubModel($0))}
            }
        }
    }
}

class GiftChoiceSubModel: BaseDataMode {
    var name    = NULL_STRING
//    var sort    = NULL_INT
    var value   = NULL_STRING
}
