//
//  GoodsOfBasketModel.swift
//  Limi
//
//  Created by Richie on 16/3/15.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//购物篮中的商品model

import UIKit

class GoodsOfBasketModel: BaseDataMode {
    
    /*
     
     "Id": 1,
     "Uid": 133512,
     "Goods_id": 7840,
     "Goods_sub_id": 0,
     "Price": 0.04,
     "Num": 7,
     goods{}
     "Goods_status": true,
     "Price_tip": "*单价已下降0.03元",
     "Num_tip": ""
     */
    
    var id      = NULL_INT
    
    var uid     = NULL_INT
    
    var num     = NULL_INT
    
    var price   = NULL_DOUBLE
    
    var num_tip     = NULL_STRING
    
    var goods_id    = NULL_INT
    
    var price_tip   = NULL_STRING
    
    var goods_sub_id = NULL_INT
    
    var goods_status = true
    
    
    var goods:GoodsModel!
    
    override func customAnalyis(_ data: [String : AnyObject]) {
        
        if let goods = data["Goods"] as? [String : AnyObject]{
            self.goods = GoodsModel(goods)
        }
        
        print(data["Num_tip"])
        
        self.goods?.setSelectedSpecification(self.selelctedGoodsSubModel, selectedGoodsSubModelCount: self.num)
    }
    
    
    
    
    ///Extension:
    
    var indexPath:IndexPath!
    
    var row:Int{return indexPath.row}
    
    var section:Int{return indexPath.section}
    
    var name:String{return self.goods.goods_name}
    
    ///是否被选中
    var didSelected = false
    
    ///总花费
    var totalCost:Double{return self.price * Double(self.selectedGoodsSubModelCount)}
    
    ///数据是否被删除 注：只用在用户未登录状态下，在保存数据时此标记用来提示是否删除本地数据
    var didDelete = false
}


extension GoodsOfBasketModel:GoodsGenericModel{
    
    var goodsName:String{return self.goods!.goods_name}
    var goodsImage:String{return self.goods!.goods_image}
    var goodsId:Int{return self.goods_id}
    var goodsOfOrderType:Int{return 1}
    
    var selelctedGoodsSubModel:GoodsSubModel?{
        
        if self.goods_sub_id != NULL_INT{
            
            if let  subs = self.goods!.goods_sub{
                for sub in subs{
                    if sub.id == goods_sub_id{
                        return sub
                    }
                }
            }
        }
        
        return nil
    }
    
    var selectedGoodsSubModelCount:Int{return self.num}
    
    var selelctedGoodsSubModelPrice:Double{return self.price}
    
    var expirationPrice:Double{return self.goods!.discount_price}
    
    var currentPrice:Double{return self.goods!.currentPrice}
    
    var selectedGoodsAttrNames:[String]{return self.goods!.selectedGoodsAttrNames}
}
