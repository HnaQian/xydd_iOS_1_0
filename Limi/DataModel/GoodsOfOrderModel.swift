//
//  GoodsOfOrderModel.swift
//  Limi
//
//  Created by Richie on 16/3/16.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//订单列表中的商品模型

import UIKit

class GoodsOfOrderModel: GoodsModel {
    
    /*
     "Order_status": 1,
     "Has_event": 0,
     "Goods_image": http: //up.xinyidiandian.com/14355579707760.jpg,
     "Goods_id": 161,
     "Has_attr": 1,
     "Goods_num": 1,
     "Price": 0.02,
     "Goods_event": <null>,
     "Order_price": 0.02,
     "Id": 3529,
     "Order_status_name": 待付款,
     "Express_url": http: //test.wechat.giftyou.me/order/express?goods_oid=3529,
     "Goods_attr"
     */
    
    /*
     "Goods_items": [
     {
     "Goods_id": 7840,
     "Goods_name": "赞助我们 土豪扔个钢镚吧",
     "Goods_image": "http://up.xinyidiandian.com/14522543049735.jpg",
     "Price": 1.01,
     "Order_price": 2.02,
     "Goods_num": 2,
     "Has_attr": true,
     "Goods_attr": [
     {
     "Goods_id": 7840,
     "Attr_value_id": 5819,
     "Attr_value_name": "绿色",
     "Attr_value_price": 0
     }
     ],
     "Has_event": false,
     "Goods_event": null,
     "Order_status": 1,
     "Order_status_name": "待付款",
     "Express_url": "http://test.wechat.giftyou.me/order/express?goods_oid=3487"
     },
     */
    
    
    var goods_id    = NULL_INT
    
    var goods_sub_id = NULL_INT
    
    var order_price = NULL_DOUBLE
    
    var express_url = NULL_STRING
    
    var order_status  = NULL_INT
    
    var order_status_name = NULL_STRING
    
    var seletedAttrOfGoods:[SeletedAttrOfGoods]?
    
    var update_time = NULL_STRING
    
    override func customAnalyis(_ data: [String : AnyObject]) {
        super.customAnalyis(data)
        print(data)
        if let attr = data["Goods_attr"] as? [[String:AnyObject]]{
            seletedAttrOfGoods = [SeletedAttrOfGoods]()
            let _ = attr.map{seletedAttrOfGoods?.append(SeletedAttrOfGoods($0))}
        }
        
        self.gid = self.goods_id
    }
    
    //extension: 属性名称拼接
    var goodsTransport:String{return self.express_url}
    
    var attrName:String{
        var name = ""
        
        if let attrs = seletedAttrOfGoods{
            if attrs.count > 0{
                let _ = attrs.map{name = name + $0.attr_value_name + " "}
            }
        }
        
        return name
    }
    
    //extension: GoodsGenericModel
    override var selectedGoodsSubModelCount:Int{return goods_num}
    
    override var selectedGoodsAttrNames:[String]{
        var name = [String]()
        
        if let attrs = seletedAttrOfGoods{
            if attrs.count > 0{
                let _ = attrs.map{name.append($0.attr_value_name)}
            }
        }
        return name
    }
    
    
    var goodsOfOrderStatus:Int{return self.order_status}
    
    var goodsOfOrderStatusName:String{return self.order_status_name}

    var goodsSubId:Int{return self.goods_sub_id}
    
    var actualPrice = NULL_DOUBLE
    
    override var selelctedGoodsSubModelPrice:Double{return self.price}
    
    var logisticUpdate_time:String {return self.update_time}
}


class SeletedAttrOfGoods:BaseDataMode{
    
    /*
     "Attr_value_name": 够小,
     "Goods_id": 161,
     "Attr_value_price": 0,
     "Attr_value_id": 5816
     */
    
    var attr_value_name = NULL_STRING
    
    var goods_id = NULL_INT
    
    var attr_value_price = NULL_DOUBLE
    
    var attr_value_id = NULL_INT
}
