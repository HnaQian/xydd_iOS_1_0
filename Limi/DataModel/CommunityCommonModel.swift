//
//  CommunityNoneResponseModel.swift
//  Limi
//
//  Created by maohs on 16/8/11.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class CommunityCommonModel: BaseDataMode {
    /*
     {
     id: 2,
     content: "第二次提问，问题是啥呢么问题是啥呢么问题是啥呢么问题是啥呢么问题是啥呢么问题是啥呢么问题",
     is_nice: 2,
     total_view_num: 100,
     total_like_num: 0,
     is_like: 1,
     sns_reply: {
     reply_id: 2,
     reply_type: 2,
     content: "http://up.xydd.co/1111111111.MP3",
     second: "01:45"
     },
     master: {
     name: "hyman",
     avatar: "http://up.xydd.co/14354968808021.jpg",
     intro: " 苦逼的产品经理"
     },
     goods_ids: ""
     }
     */
    var id = NULL_INT
    var content = NULL_STRING
    var is_nice = NULL_INT          //问题是否加精 1未加精 2加精
    var total_view_num = NULL_INT   //查看数量
    var total_like_num = NULL_INT   //喜欢数量
    var is_like = NULL_INT          //点赞状态 1点过赞 2没点过赞
    var status = NULL_STRING        //1正常2删除
    var is_play:Bool = false        //true 正在播放  false 没有播放
    
    var sns_reply:CommunityCommonModelReply?
    var master:CommunityCommonModelMaster?
    var goods_ids = [GoodsModel]()
    
    fileprivate let scale = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
    //是否折叠
    var isRetract:Bool = true
    fileprivate var tempRetractHeight:CGFloat = 0
    var retractCellHeight: CGFloat{
        if tempRetractHeight == 0 {
            tempRetractHeight = heightForCellWithString(true)
        }
        return tempRetractHeight
    }
    
    fileprivate var tempTotalHeight:CGFloat = 0
    var totalCellHeight:CGFloat{
        if tempTotalHeight == 0 {
            tempTotalHeight = heightForCellWithString(false)
        }
        return tempTotalHeight
    }
    
    func heightForCellWithString(_ isRetract:Bool) -> CGFloat{
        var rect = CGRect.zero
       content = content.replacingOccurrences(of: "\n", with: "")
        if is_nice == 1 {
            //未加精
            let attributedText = NSAttributedString(string: content, attributes: [NSFontAttributeName: Constant.CustomFont.Default(size: Constant.common_F2_font)])
            rect = attributedText.boundingRect(with: CGSize(width: Constant.ScreenSizeV2.SCREEN_WIDTH - 60 * scale, height: CGFloat.greatestFiniteMagnitude), options: NSStringDrawingOptions.usesLineFragmentOrigin, context: nil)
        }else {
            //加精的
            let temp = "很随" + content//由于label前边添加了图片（图片宽度大概两个文字的宽度）  在计算Label高度的时候需要用两个文字来代替填充，使其高度计算准确
            let attributedText = NSAttributedString(string: temp, attributes: [NSFontAttributeName: Constant.CustomFont.Default(size: Constant.common_F2_font)])
            rect = attributedText.boundingRect(with: CGSize(width: Constant.ScreenSizeV2.SCREEN_WIDTH - 60 * scale, height: CGFloat.greatestFiniteMagnitude), options: NSStringDrawingOptions.usesLineFragmentOrigin, context: nil)
        }
        
        if let reply = sns_reply{
            if (status as NSString).integerValue == 2 {
                //已经删除的
                return 140 * scale + rect.size.height + 15
            }
            if reply.reply_type == 1 {
                //文字
                var tempRect = CGRect.zero
                if isRetract {
                    let tempAttributedText = NSAttributedString(string: reply.content, attributes: [NSFontAttributeName: Constant.CustomFont.Default(size: Constant.common_F4_font)])
                    tempRect = tempAttributedText.boundingRect(with: CGSize(width: Constant.ScreenSizeV2.SCREEN_WIDTH - 160 * scale, height: 40), options: NSStringDrawingOptions.usesLineFragmentOrigin, context: nil)
                }else {
                    let tempAttributedText = NSAttributedString(string: reply.content, attributes: [NSFontAttributeName: Constant.CustomFont.Default(size: Constant.common_F4_font)])
                    tempRect = tempAttributedText.boundingRect(with: CGSize(width: Constant.ScreenSizeV2.SCREEN_WIDTH - 160 * scale, height: CGFloat.greatestFiniteMagnitude), options: NSStringDrawingOptions.usesLineFragmentOrigin, context: nil)
                }
                
                if goods_ids.count > 0 {
                    return rect.size.height + tempRect.size.height + 50 + (190 + 386) * scale
                }
                return rect.size.height + tempRect.size.height + 44 + 190 * scale
            }else {
                //语音
                if goods_ids.count > 0 {
                    return (260 + 386) * scale + rect.size.height + 42
                }
                return 260 * scale + rect.size.height + 28
            }
            
        }else {
            //没有回复的
           return 140 * scale + rect.size.height + 15
        }
        
    }
    
    override func customAnalyis(_ data: [String : AnyObject]) {
        if let reply = JSON(data as AnyObject)["sns_reply"].dictionaryObject {
            self.sns_reply = CommunityCommonModelReply(reply)
        }
        
        if let master = JSON(data as AnyObject)["master"].dictionaryObject {
            self.master = CommunityCommonModelMaster(master)
        }
        
        if let goodsIds = JSON(data as AnyObject)["goods_ids"].arrayObject {
            if goodsIds.count > 0 {
                goods_ids = [GoodsModel]()
                for item in goodsIds {
                    goods_ids.append(GoodsModel(item as! [String:AnyObject]))
                }
            }
        }else {
            goods_ids = [GoodsModel]()
        }
    }
    
    //需要子类复写
    func classMap() ->CommunityBaseCell.Type {
        assert(false,"需要子类复写")
        return CommunityBaseCell.self
    }
    
    //Type:1:无回复 2:语音回复 3:文字回复
    //子类需要复写
    func getModelType()->Int{return 0}
}

class CommunityCommonModelReply:BaseDataMode {
    /*
     {
     reply_id: 2,
     reply_type: 2,
     content: "http://up.xydd.co/1111111111.MP3",
     second: "01:45"
     }
     */
    var reply_id = NULL_INT
    var reply_type = NULL_INT       //问题类型 1文字 2语音
    var content = NULL_STRING
    var second = NULL_STRING
}

class CommunityCommonModelMaster:BaseDataMode {
    /*
     {
     name: "hyman",
     avatar: "http://up.xydd.co/14354968808021.jpg",
     intro: " 苦逼的产品经理"
     }
     */
    var name = NULL_STRING
    var avatar = NULL_STRING
    var intro = NULL_STRING
}
