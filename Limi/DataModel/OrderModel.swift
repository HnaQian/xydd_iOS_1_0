//
//  OrderModel.swift
//  Limi
//
//  Created by Richie on 16/3/16.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//订单模型

import UIKit

class OrderModel: BaseDataMode {
    
    /*
     data.list[i].order_type	订单类型 1为普通订单，2为话费订单  3为自选礼盒订单
     data.list[i].Goods[j].Order_price	子订单金额
     data.list[i].Goods[j].Order_status_name	子订单状态名称
     data.list[i].Goods[j].Goods_attr	商品规格
     data.list[i].Goods[j].Goods_event	商品活动
     
     */
    
    
    /*
     "Oid": 115209983,
     "Order_type": 1,
     "Order_image": "",
     "Order_name": "",
     "Order_remark": "",
     "Order_status": 5,
     "Order_status_name": "售后中",
     "Actual_price": 999,
     
     "Goods": []
     
     "Express_info": "",
     "Express": null,
     "Update_time": "2015-08-24 12:21:59"
     */
    
    var uid = NULL_INT
    
    var oid = NULL_INT
    
    var express = NULL_DOUBLE
    
    var add_time = NULL_STRING
    
    var order_type  = NULL_INT
    
    var order_name  = NULL_STRING
    
    var update_time = NULL_STRING
    
    var order_image = NULL_STRING
    
    var order_status = NULL_INT
    
    var express_info = NULL_STRING
    
    var order_remark = NULL_STRING
    
    var actual_price = NULL_DOUBLE
    
    var order_status_name = NULL_STRING
    
    
    var goods:[GoodsGenericModel]!
    
    override func customAnalyis(_ data: [String : AnyObject]) {
        
        self.goods = [GoodsGenericModel]()
        
        if order_type == 3{order_type = 2}
        if order_type == 2
        {
            let feeAndFlow = PhoneCostModel(data)
            feeAndFlow.orderType = 2
            self.goods?.append(feeAndFlow)
            
            return
        }
        
        if let goodlist = data["Goods"] as? [[String:AnyObject]]{
            if goodlist.count > 0{
                let _ = goodlist.map{self.goods?.append(GoodsOfOrderModel($0))}
            }
        }
    }
}


//话费model + 流量model
class PhoneCostModel:BaseDataMode
{
    
    /*
     "Oid": 116358927,
     "Uid": 0,
     "Order_type": 2,
     "Order_image": "http://img.xinyidiandian.com/order_type_2.png",
     "Order_name": "上海 联通 话费充值 100元",
     "Order_remark": "手机号：15618012755",
     "Order_status": 4,
     "Order_status_name": "已完成",
     "Actual_price": 96,
     "Goods": null,
     "Express_info": "",
     "Express": null,
     "Update_time": "2016-01-14 00:17:44",
     "Add_time": ""
     
     */
    
    override func customAnalyis(_ data: [String : AnyObject]) {
        
        self.name = data["Order_name"] as! String
        
        self.img = data["Order_image"] as! String
        
        self.status = data["Order_status"] as! Int
        
        self.status_name = data["Order_status_name"] as! String
        
        self.selectedPrice = data["Actual_price"] as! Double
    }
    
    
    var name = NULL_STRING
    var img = NULL_STRING
    var status = NULL_INT
    var status_name = NULL_STRING
    var orderType = 2
    var count = 1
    var selectedPrice = NULL_DOUBLE
    var phone_expirationPrice = NULL_DOUBLE
    var phone_currentPrice:Double = NULL_DOUBLE
    var attrNames = [""]
}



extension PhoneCostModel:GoodsGenericModel{
    
    //商品名称
    var goodsName:String{return self.name}
    
    var goodsImage:String{return self.img}
    
    /// 订单中使用到的商品的状态
    var goodsOfOrderStatus:Int{return self.status}
    
    /// 订单中使用到的商品的状态名称
    var goodsOfOrderStatusName:String{return self.status_name}
    
    /// 订单中使用到商品类型  1为普通订单，2为话费订单
    var goodsOfOrderType:Int{return 2}
    
    ///Extension: 提交订单时的数量:选择的商品规格数量
    var selectedGoodsSubModelCount:Int{return self.count}
    
    ///Extension: 提交订单时的单价:选择的规格价格(也可能没有选择规格，返回结果已经把此逻辑计算在内)
    var selelctedGoodsSubModelPrice:Double{return self.selectedPrice}
    
    ///Extension: 原价：失效的价格
    var expirationPrice:Double{return self.phone_expirationPrice}
    
    /// Extension: 现价：未选商品规格的实际支付价格
    var currentPrice:Double{return self.phone_currentPrice}
    
    /// Extension: 当前选择的规格的名称数组
    var selectedGoodsAttrNames:[String]{return self.attrNames}
}

