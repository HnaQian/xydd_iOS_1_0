//
//  QYUserInfo.swift
//  Limi
//
//  Created by maohs on 16/7/13.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit
import CoreData

class QYLocalUserInfo: BaseDataMode {

    static let shareInstance = QYLocalUserInfo()
    
    fileprivate var username = ""
    fileprivate var phoneNum = ""
    fileprivate var sex = 0
    fileprivate var userInfomation : User? = nil
    
    fileprivate init () {}
    
    func getLocalUserInfo() ->String {
        let requestUserInfo: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        let datasourceUserInfo : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(requestUserInfo)!
        if datasourceUserInfo.count > 0{
            userInfomation = datasourceUserInfo[0] as? User
            
            if userInfomation!.nick_name != "" && userInfomation!.nick_name != nil
            {
                username = userInfomation!.nick_name!
            }
            else
            {
                if let temp = userInfomation?.user_name {
                    username = temp
                }
            }
            
            if let temp = userInfomation?.user_name {
                phoneNum = temp
            }
            
            sex = Int(userInfomation!.gender)
        }
        
        return jointUserInfo()
        
    }
    
    fileprivate func jointUserInfo() ->String {
        var sexString = ""
        if sex == 1 {
            sexString = "男"
        }else {
            sexString = "女"
        }
        let userData = [["key":"real_name","value":username],
                        ["key":"mobile_phone","value":phoneNum],
                        ["key":"sex","value":sexString]]
        var tempData:Data?
        do {
            tempData = try JSONSerialization.data(withJSONObject: userData, options: JSONSerialization.WritingOptions.prettyPrinted)
            
        } catch {
            
        }
        let jsonData = NSString(data: tempData!, encoding: String.Encoding.utf8.rawValue)
        print(jsonData)
        if let temp = jsonData {
             return String(temp)
        }
        
        return ""
    }
    
}
