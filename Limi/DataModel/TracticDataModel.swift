//
//  TracticDataModel.swift
//  Limi
//
//  Created by Richie on 16/4/20.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//
//攻略数据模型

import UIKit

class TracticDataModel: BaseDataMode {
    
    /*
     
     "id": "554",
     "cover": "http://up.xydd.co/14657167423796.jpeg",
     "cover_title": "测试2222",
     "title": "测试2222",
     "sub_title": "测试22223333",
     "collect_num": "1246",
     "goods_list": [],
     "collect_status": false,
     "new_status": false
     
     
     
     href = "http://wechat.giftyou.me/article_item?id=745&app=1";
     src = "http://up.xinyidiandian.com/14620715026574.jpg";
     text = "\U72ec\U5bb6\U793c\U76d211";
     "text_color" = "#7AAE79";
     title = "\U5927\U793c\U5305";
     "title_color" = "#12171F";
     
     
     Author =     {
     Avatar = "http://up.xinyidiandian.com/14431778403768.jpg";
     Id = 4;
     Intro = "\U8f7b\U5ea6\U6570\U7801\U63a7";
     Name = Bailu;
     };
     "Author_id" = 4;
     "Collect_num" = 1148;
     "Collect_status" = 0;
     Cover = "http://up.xinyidiandian.com/14585450312248.jpg";
     "Cover_title" = "";
     Id = 542;
     "Sub_title" = "\U6211\U6253\U5b8c\U7b49\U6211\U7684\U7a33\U5b9a";
     Title = "\U5e74\U8d27\U5927\U4f5c\U62181";
     
     */
    
    
    var Id          = NULL_INT
    var cover       = NULL_STRING
    var title       = NULL_STRING
    var sub_title   = NULL_STRING
//    var author_id   = NULL_INT
    var cover_title = NULL_STRING
    var collect_num = NULL_INT
    
    var collect_status = NULL_INT
    
    
    override func customAnalyis(_ data: [String : AnyObject]) {
        
        if let av = data["Author"] as? [String : AnyObject]{
            author = Avatar(av)
        }
    }
    
    var author:Avatar?
}


class Avatar:BaseDataMode{
    
    /*
     Avatar = "http://up.xinyidiandian.com/14431778403768.jpg";
     Id = 4;
     Intro = "\U8f7b\U5ea6\U6570\U7801\U63a7";
     Name = Bailu;
     */
    
    var Id = NULL_INT
    var avatar = NULL_STRING
    var intro = NULL_STRING
    var name = NULL_STRING
}
