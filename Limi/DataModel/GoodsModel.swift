//
//  GoodsModel.swift
//  Limi
//
//  Created by Richie on 16/3/12.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//商品模型

import UIKit

class GoodsModel: BaseDataMode {
    
    var goods_feature:[GoodsFeatureModel]?
    
    var goods_attr:[GoodsAttributeModel]?
    
    var goods_images:[GoodsImagesModel]?
    
    var goods_sub:[GoodsSubModel]?
    
    var event:GoodsEventModel?
    
    var data:GoodsModel?
    
    //1.3.67+
    var paramsInfo:[ParamsInfoModel]?
    
    var detailInfo:[DetailInfoModel]?

    var goods_outside_platform = NULL_INT
    
    var goods_limit_num = NULL_INT
    
    var outside_platform = NULL_STRING
    
    var discount_price = NULL_DOUBLE
    
    var collect_status  = NULL_INT
    
    var goods_subtitle  = NULL_STRING
    
    var is_recommend    = NULL_INT
    
    var notify_status   = NULL_INT
    
    //1.3.12+  JSON中的market_price不再使用 ， 市场价使用price数据
    
    var market_price    = NULL_DOUBLE
    
    var goods_image     = NULL_STRING
    
    var collect_num     = NULL_INT
    
    var goods_outer     = NULL_INT
    
    var goods_cate      = NULL_STRING
    
    var goods_type      = NULL_STRING
    
    var has_feature     = NULL_INT
    
    var supplier_id     = NULL_INT
    
    var goods_link      = NULL_STRING
    
    var gift_story      = NULL_STRING
    
    var goods_name      = NULL_STRING
    
    var goods_num       = NULL_INT
    
    var share_num       = NULL_INT
    
    var has_event       = NULL_INT
    
    var has_attr        = NULL_INT
    
    var is_good         = NULL_INT
    
    var detail          = NULL_STRING
    
    var is_hot          = NULL_INT
    
    var is_new          = NULL_INT
    
    var price           = NULL_DOUBLE
    
    var status          = NULL_INT
    
    var spec            = NULL_STRING
    
    var gid             = NULL_INT
    
    var id              = NULL_INT
    
    //custom analyis
    override func customAnalyis(_ data: [String : AnyObject]) {
        
        if let attributes = data["Goods_attr"] as? [[String:AnyObject]]{
            if attributes.count > 0{
                self.goods_attr = [GoodsAttributeModel]()
                let _ = attributes.map{self.goods_attr?.append(GoodsAttributeModel($0))}
            }
        }
        
        
        if let features = data["Goods_feature"] as? [[String:AnyObject]]{
            if features.count > 0{
                self.goods_feature = [GoodsFeatureModel]()
                let _ = features.map{self.goods_feature?.append(GoodsFeatureModel($0))}
            }
        }
        
        if let event = data["Event"] as? [String:AnyObject]{
            self.event = GoodsEventModel(event)
        }
        
        
        if let subs = data["Goods_sub"] as? [[String:AnyObject]]{
            if subs.count > 0{
                self.goods_sub = [GoodsSubModel]()
                let _ = subs.map{self.goods_sub?.append(GoodsSubModel($0))}
            }
        }
        
        
        if let images = data["Goods_imgs"] as? [[String:AnyObject]]{
            if images.count > 0{
                self.goods_images = [GoodsImagesModel]()
                let _ = images.map{self.goods_images?.append(GoodsImagesModel($0))}
            }
        }
        

        if let mdata = data["Data"] as? [String:AnyObject]{
            self.data = GoodsModel(mdata)
        }
        
        if let params = data["ParamsInfo"] as? [[String:AnyObject]] {
            if params.count > 0 {
                self.paramsInfo = [ParamsInfoModel]()
                let _ = params.map{self.paramsInfo?.append(ParamsInfoModel($0))}
            }
        }

        if let detail = data["DetailInfo"] as? [[String:AnyObject]] {
            if detail.count > 0 {
                self.detailInfo = [DetailInfoModel]()
                let _ = detail.map{self.detailInfo?.append(DetailInfoModel($0))}
            }
        }

    }
    
    ///Extension: 选择的商品规格
    fileprivate var _selelctedGoodsSubModel:GoodsSubModel?
    
    ///Extension: 选择的商品规格数量
    fileprivate var _selectedGoodsSubModelCount = 1
    
    var totalCost:Double{return self.selelctedGoodsSubModelPrice * Double(self.selectedGoodsSubModelCount)}
}


class GoodsRelationModel: BaseDataMode {
    /*
     "collect_num" = 24;
     "discount_price" = 19;
     gid = 7851;
     "goods_cate" = 162;
     "goods_image" = "http://up.xydd.co/14669979528850.jpg";
     "goods_link" = "";
     "goods_name" = "\U65e5\U672c\U5c0f\U55b5\U54aa\U53d1\U536122";
     "goods_num" = 0;
     "goods_outer" = 1;
     "goods_subtitle" = "osewaya\U4e16\U8bdd\U8d85\U840c\U5c0f\U732b\U54aa\U53d1\U5939";
     "goods_type" = 1;
     "market_price" = 29;
     price = 29;
     */
    
    var gid                  = NULL_INT
   
    var goods_name           = NULL_STRING
    
    var goods_subtitle       = NULL_STRING
    
    var goods_image          = NULL_STRING
    
    var goods_cate           = NULL_STRING
    
    var goods_type           = NULL_STRING
    
    var goods_link           = NULL_STRING
    
    var goods_num            = NULL_STRING
    
    var goods_outer          = NULL_STRING
    
    var price                = NULL_DOUBLE
    
    var market_price         = NULL_DOUBLE
    
    var collect_num          = NULL_INT
    
    var discount_price       = NULL_DOUBLE
    
}


extension GoodsModel:GoodsGenericModel
{
    var goodsName:String{return self.goods_name}
    var goodsImage:String{return self.goods_image}
    
    var onlyId:Int{return self.id}
    
    var goodsId:Int{return self.gid}
    
    var goodsOfOrderType:Int{return 1}
    
    func setSelectedSpecification(_ selelctedGoodsSubModel:GoodsSubModel?,selectedGoodsSubModelCount:Int)
    {
        _selelctedGoodsSubModel = selelctedGoodsSubModel
        
        _selectedGoodsSubModelCount = selectedGoodsSubModelCount
    }
    
    var selelctedGoodsSubModel:GoodsSubModel?{return _selelctedGoodsSubModel}
    
    var selectedGoodsSubModelCount:Int{return _selectedGoodsSubModelCount}
    
    var goodsLink:String{return self.goods_link}
    
    var mostOfGoods:Int{return self.goods_num == NULL_INT ? 0 : self.goods_num}
    
    var selelctedGoodsSubModelPrice:Double{
        
        var price = self.discount_price
        
        if let subModel = selelctedGoodsSubModel{
            price += subModel.price
        }
        return price
    }
    
    var expirationPrice:Double{
        if  let _ = self.event{
            if [1,2,5].contains(self.event!.type) == true{
                return self.price
            }
        }
        
        return self.market_price
    }
    
    var currentPrice:Double{return self.discount_price}
    
    var selectedGoodsAttrNames:[String]{
        
        var names = [String]()
        
        if let subMode = self.selelctedGoodsSubModel{
            
            let attrus = NSString(string: subMode.attr_item_ids).components(separatedBy: ",")
            
            if attrus.count > 0{
                for id in attrus{
                    if let goods_attrbuts = self.goods_attr{
                        for  attrubute in goods_attrbuts{
                            if let attrbutes = attrubute.attr_value{
                                for value in attrbutes{
                                    if value.id == id.intValue{
                                        names.append(value.name)
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
        }
        return names
    }
}

//商品信息参数
class GoodsSpceInfoModel:BaseDataMode{
    var name            = NULL_STRING
    
    var value           = NULL_STRING

}



//MARK: 收藏商品的独有字段：
class GoodsDatamodel:BaseDataMode{
    var gid = NULL_INT
}


//MARK: 商品图片组
class GoodsImagesModel:BaseDataMode {
    
    override func customAnalyis(_ data: [String : AnyObject]) {
        if let img = data["Image"] as? String{
            image = img
        }
    }
    
    var image = NULL_STRING
}

//MARK: 商品特性
class GoodsFeatureModel: BaseDataMode {
    
    var id = NULL_INT
    
    var feature_color = NULL_STRING
    
    var feature_name = NULL_STRING
}

//MARK: 商品规格
class GoodsSubModel: BaseDataMode
{
    //"Attr_item_ids"="5813,5815,5817";"Goods_num"=7;Id=5727;Price=0;
    
    var attr_item_ids = NULL_STRING
    
    var Goods_num = NULL_INT
    
    var price = NULL_DOUBLE
    
    var id = NULL_INT
    
    var oriPrice = NULL_DOUBLE
    
    //extension:
    var attr_item_idsAry:[String]{return NSString(string: attr_item_ids).components(separatedBy: ",")}
}

//MARK: 商品活动
class GoodsEventModel: BaseDataMode {
    /*
     "Event": {
     "Id": 7,
     "Name": "端午大促",
     "Slogan": "跳楼价",
     "Type": 1,
     "Value": 100,
     "Point": "-100",
     "Price": 0.01,
     "Start_time": "2015-06-18 20:41:16",
     "End_time": "2015-08-18 20:41:18"
     }
     */
    
    var id      = NULL_INT
    
    var event_id = NULL_INT
    
    var name    = NULL_STRING
    
    var type    = NULL_INT
    
    var value   = NULL_INT
    
    var price   = NULL_DOUBLE
    
    var point   = NULL_STRING
    
    var slogan  = NULL_STRING
    
    var end_time    = NULL_STRING
    
    var start_time  = NULL_STRING
}


class GoodsAttributeModel: BaseDataMode {
    //"Attr_name"="\U989c\U8272";"Attr_value" = {},Id=1671;
    
    override func customAnalyis(_ data: [String : AnyObject]) {
        
        if let values = data["Attr_value"] as? [[String : AnyObject]]{
            if values.count > 0{
                attr_value = [GoodsAttrValueModel]()
                let _ = values.map{attr_value?.append(GoodsAttrValueModel($0))}
            }
        }
    }
    
    
    var id = NULL_INT
    
    var attr_name = NULL_STRING
    
    var attr_value:[GoodsAttrValueModel]?
}

class GoodsAttrValueModel: BaseDataMode {
    //  "Attr_id"=1671;Id=5813;Name="\U4eae\U9ec4";
    
    var id      = NULL_INT
    var name    = NULL_STRING
    var attr_id = NULL_INT
}

class ParamsInfoModel: BaseDataMode {
    
    override func customAnalyis(_ data: [String : AnyObject]) {
        if let tempDetail = data["detail"] as? [[String:AnyObject]] {
            if tempDetail.count > 0 {
                self.detail = [DetailParamsInfo]()
                let _ = tempDetail.map{self.detail?.append(DetailParamsInfo($0))}
            }
        }
    }
    
    var title   = NULL_STRING
    var detail:[DetailParamsInfo]?
}

class DetailParamsInfo: BaseDataMode {
    var key     = NULL_STRING
    var value   = NULL_STRING
}

class DetailInfoModel: BaseDataMode {
    var title   = NULL_STRING
    var detail:[String]?
}


//MARK: 商品的共性 主要用于cell的显示和统一的数据处理
@objc protocol GoodsGenericModel:NSObjectProtocol{
    
    //商品名称
    var goodsName:String{get}
    
    var goodsImage:String{get}
    
    /**
     订单中使用到商品类型
     *  1为普通订单，
     *  2为话费订单
     *  3为话费订单
     *  4为自选礼盒web页面传来的Model
     *  5为心意祝福服务商品model
     */
    ///注：到目前为止 type = 3尚未用到，app中的流量订单使用话费订单的model
    var goodsOfOrderType:Int{get}
    
    ///Extension: 提交订单时的数量:选择的商品规格数量
    var selectedGoodsSubModelCount:Int{get}
    
    ///Extension: 提交订单时的单价:选择的规格价格(也可能没有选择规格，返回结果已经把此逻辑计算在内)
    var selelctedGoodsSubModelPrice:Double{get}
    
    ///Extension: 原价：失效的价格
    var expirationPrice:Double{get}
    
    /// Extension: 现价：未选商品规格的实际支付价格
    var currentPrice:Double{get}
    
    /// Extension: 当前选择的规格的名称数组
    var selectedGoodsAttrNames:[String]{get}
    
    //更新时间
    @objc optional var logisticUpdate_time:String{get}
    
    ///选择的商品规格
    @objc optional var selelctedGoodsSubModel:GoodsSubModel?{get}
    
    @objc optional var goodsId:Int{get}
    
    @objc optional var onlyId:Int{get}
    
    //自选礼盒需要
    @objc optional var goodsSubId:Int{get}
    
    /// 订单中使用到的商品的状态
    @objc optional var goodsOfOrderStatus:Int{get}
    
    /// 订单中使用到的商品的状态名称
    @objc optional var goodsOfOrderStatusName:String{get}
    
    /// 订单中使用到商品类型  1为普通订单，2为话费订单
    @objc optional var goodsTransport:String{get}
    
    /// 商品的库存
    @objc optional var mostOfGoods:Int{get}
    
    //商品链接
    @objc optional var goodsLink:String{get}
    
    /**
     Extension:
     设置此用户选择的此商品的规格和数量
     
     - parameter selelctedGoodsSubModel:     规格，没有传空
     - parameter selectedGoodsSubModelCount: 数量，最小为1
     */
    @objc optional  func setSelectedSpecification(_ selelctedGoodsSubModel:GoodsSubModel?,selectedGoodsSubModelCount:Int)
    
}
