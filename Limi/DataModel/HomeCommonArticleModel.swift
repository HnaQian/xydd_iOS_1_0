//
//  HomeCommonArticleModel.swift
//  Limi
//
//  Created by Richie on 16/6/13.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//普通攻略
import UIKit

class HomeCommonArticleModel: HomCellBaseDataModel {
    
    /*
     {
     Author =             {
     Avatar = "http://up.xydd.co/14356515978242.png";
     Id = 6;
     Intro = "\U7231\U5fc3\U6cdb\U6ee5\U7684\U4e2d\U5e74\U5927\U53d4";
     Name = sam;
     };
     "Author_id" = 6;
     "Collect_num" = 982;
     "Collect_status" = 0;
     "Is_new" : true;
     Cover = "http://up.xydd.co/14569947884395.png";
     "Cover_title" = "\U751f\U65e5";
     Id = 491;
     "Sub_title" = "lyc-test";
     Title = lyc;
     }
     */
    var author:HomeCommonArticleModelAuthor?
    
    var id      = NULL_INT
    var cover   = NULL_STRING
    
    //type 1:普通攻略，显示攻略图 2:达人攻略，显示作者头像
    var type        = NULL_INT
    var title       = NULL_STRING
    var Is_new      = NULL_INT
//    var Author_id   = NULL_INT
    var sub_title   = NULL_STRING
    var new_status  = NULL_INT
    var cover_title = NULL_STRING
    var collect_num = NULL_INT
    var sub_cover   = NULL_STRING
    var data:HomeCommonArticleModel?
    
    var collect_status = NULL_INT
    
    var goods_list:[GoodsModel]?
    
    override func customAnalyis(_ data: [String : AnyObject]) {
        if let array = JSON(data as AnyObject)["Goods_list"].arrayObject{
            if array.count > 0 {
                goods_list = [GoodsModel]()
                for item in array{
                    goods_list?.append(GoodsModel(item as! [String:AnyObject]))
                }
            }
        }else if let array = JSON(data as AnyObject)["goods_list"].arrayObject{
            if array.count > 0 {
                goods_list = [GoodsModel]()
                for item in array{
                    goods_list?.append(GoodsModel(item as! [String:AnyObject]))
                }
            }
        }
        
        if let author = JSON(data as AnyObject)["Author"].dictionaryObject{
            self.author = HomeCommonArticleModelAuthor(author)
        }else if let author = JSON(data as AnyObject)["author"].dictionaryObject {
            self.author = HomeCommonArticleModelAuthor(author)
        }
        
        if let mdata = data["Data"] as? [String:AnyObject]{
            self.data = HomeCommonArticleModel(mdata)
        }else if let mdata = data["data"] as? [String:AnyObject]{
            self.data = HomeCommonArticleModel(mdata)
        }
    }
    
    override func getModelType() -> Int {
        return 1
    }
    
    override func classMap() -> HomeBaseTableViewCell.Type {
        return HomeArticleTableViewCell.self
    }
    
    override func getCellHeight() ->CGFloat{return HomeArticleTableViewCell.height}
    
}


class HomeCommonArticleModelAuthor:BaseDataMode{
    
    var id      = NULL_INT
    var name    = NULL_STRING
    var intro   = NULL_STRING
    var avatar  = NULL_STRING
}
