//
//  LeaderBoardModel.swift
//  Limi
//
//  Created by Richie on 16/5/16.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//心意榜单数据

import UIKit

class LeaderBoardModel: BaseDataMode {
    
    /*
     "Id": 2,
     "Type": 1,
     "Goods_id": 4968,
     "Goods": {
     "Gid": 4968,
     "Goods_name": "【满78包邮】上海特产沈大成传统糕点伴手礼 芝麻红豆团+太白松仁金桔山楂红果",
     "Goods_subtitle": "",
     "Goods_image": "http://up.xinyidiandian.com/14449867544721.jpg",
     "Market_price": 0,
     "Price": 45,
     "Goods_outer": 1,
     "Collect_status": false,
     "Collect_num": 12,
     "Has_feature": false,
     "Goods_feature": null
     },
     "Collect_num": 2221111,
     "Collect_status": false,
     "Description": "我是描述啊啊啊啊啊啊啊啊啊啊啊"
     */
    
    var id          = NULL_INT
    
    var type        = NULL_INT
    
    var goods_id    = NULL_INT
    
    var Description = NULL_STRING
    
    var collect_num = NULL_INT
    
    var collect_status = false
    
    var goods:GoodsModel?
    var indexPath:IndexPath!
    
    override func customAnalyis(_ data: [String : AnyObject]) {
        if let mgoods = data["Goods"] as? [String:AnyObject]{
            goods = GoodsModel(mgoods)
        }
    }
    
    //extension:
    var goodBtnShouldAnimated = false
}


