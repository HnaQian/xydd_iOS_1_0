//
//  CouponModel.swift
//  Limi
//
//  Created by Richie on 16/3/14.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//红包

import UIKit

class CouponModel: BaseDataMode {
    
    /*
     Id: 1,
     Cid: 3,
     Name: "中秋礼包",
     Par_value: 100,
     Min_value: 0,
     Min_title: "无限制",
     Scope: 1,
     Scope_name: "中秋佳节",
     Url: "http://wechat.giftyou.me/index?app=1",
     Start_time: "2015-08-06 18:34:11",
     End_time: "2015-09-07 19:34:11",
     Time_title: "2015.08.06 -- 2015.09.07"
     },
     */
    
    
    
    var id  = NULL_INT
    
    var cid = NULL_INT
    
    var url = NULL_STRING
    
    var name  = NULL_STRING
    
    var scope = NULL_INT
    
    var discount    = NULL_DOUBLE
    
    var end_time    = NULL_STRING
    
    var start_time  = NULL_STRING
    
    var min_title   = NULL_STRING
    
    var min_value   = NULL_INT
    
    var par_value   = NULL_DOUBLE
    
    var time_title  = NULL_STRING
    
    var scope_name  = NULL_STRING
    
    var content = NULL_STRING
    
    //extention:
    var indexPath:IndexPath!
    //extention:
    var rowIndex:Int = 0
    
    var isSelected = false
    
    fileprivate var tempExternHeight:CGFloat = 0
    var externCellHeight: CGFloat{
        if tempExternHeight == 0 {
            tempExternHeight = heightForCell()
        }
        return tempExternHeight
    }
    
    var cellHeight = 270 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
    
    func heightForCell() ->CGFloat {
        let font = UIFont.systemFont(ofSize: Constant.Theme.getFontSizeForScreenSizeByScal(12))
        var height = CalculateHeightOrWidth.getLabOrBtnHeigh(content as NSString, font: font, width: (Constant.ScreenSizeV2.SCREEN_WIDTH - 120 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE))
        height = height + (48 + 270) * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        return height
    }
}
