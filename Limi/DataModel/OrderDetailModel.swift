//
//  OrderDetailModel.swift
//  Limi
//
//  Created by Richie on 16/3/18.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class OrderDetailModel: BaseDataMode {
    /*
     "Oid": 116557269,
     "Order_type": 1,
     "Order_image": "",
     "Order_name": "",
     "Order_remark": "测试数据备份",
     "Sell_price": 2.12,
     "Discount_price": 10,
     "Actual_price": 0,
     "Pay_type": 1,
     "Pay_time": "",
     "Receiver_name": "刘超",
     
     data.order.Service_price	心意服务金额
     data.order.Discount_price	优惠金额
     data.order.Actual_price	支付金额
     data.order.Service_name	心意服务名称
     data.order.Service_url	心意服务地址，地址为空字符串表示没有心意服务
     
     */
    
    var service_price = NULL_DOUBLE
    
    var service_name = NULL_STRING
    
    var service_url = NULL_STRING
    
    var discount_price = NULL_DOUBLE
    
    var receiver_name = NULL_STRING
    
    var actual_price = NULL_DOUBLE
    
    var order_remark = NULL_STRING
    
    var order_image = NULL_STRING
    
    var order_type  = NULL_INT
    
    var order_name  = NULL_STRING
    
    var sell_price  = NULL_DOUBLE
    
    var pay_type    = NULL_INT
    
    var pay_time    = NULL_STRING
    
    var oid         = NULL_INT
    var invoice_tip = NULL_STRING
    /*
     "Receiver_mobile": "18602157278",
     "Order_remark": 北京移动
     "Province_id": 110000,
     "City_id": 110100,
     "Area_id": 110101,
     "Province_name": "北京",
     "City_name": "北京市",
     "Area_name": "东城区",
     "Address": "松涛路647号",
     "Invoice_head": "",
     "Invoice_text": "",
     "Express_info": "暂无物流信息",
     "Express_com": "",
     "Express_nu": "",
     
     */
    var order_status_name = NULL_STRING
    
    var receiver_mobile = NULL_STRING
    
    var province_name = NULL_STRING
    
    var invoice_head = NULL_STRING
    
    var invoice_text = NULL_STRING
    
    var express_info = NULL_STRING
    
    var order_status = NULL_INT
    
    var express_com  = NULL_STRING
    
    var express_nu   = NULL_STRING
    
    var province_id = NULL_INT
    
    var city_name   = NULL_STRING
    
    var area_name   = NULL_STRING
    
    var city_id     = NULL_INT
    
    var area_id     = NULL_INT
    
    var address     = NULL_STRING
    
    var add_time    = NULL_STRING
    
    var update_time = NULL_STRING
    
    
    override func customAnalyis(_ data: [String : AnyObject]) {
        
        if self.order_type == 3{self.order_type = 2}
        
        if let coupon = data["User_coupon"] as? [String:AnyObject]{
            couponOfOrderModel = CouponOfOrderModel(coupon)
        }
        
        if self.order_type == 2{
            
            goods = [GoodsGenericModel]()
            let feeAndFlow = PhoneCostModel(data)
            feeAndFlow.orderType = 2
            goods.append(feeAndFlow)
            return
        }
        
        if let list = data["Goods_items"] as? [[String:AnyObject]]{
            if list.count > 0{
                goods = [GoodsGenericModel]()
                for item in list{
                    let orderGoods = GoodsOfOrderModel(item)
                    orderGoods.update_time = self.update_time
//                    print(orderGoods.ORIGINAL_DATA)
                    goods.append(orderGoods)
                }
            }
        }
    }
    
    var goods:[GoodsGenericModel]!
    
    var couponOfOrderModel:CouponOfOrderModel?
    
    //Extension:
    //是否有地址
    var isHaveAddress:Bool{return (address != NULL_STRING && address != "")}
    
    //是否有红包
    var isHaveCoupon:Bool{return couponOfOrderModel != nil}
    
    //是否支付方式
    var isHavePayType:Bool{return self.order_status > 1}
    
    //是否有心意服务
    var isHaveWishSendServices:Bool{return (service_url != NULL_STRING && service_url != "")}
    
    //是否有发票
    var isHaveInvoice:Bool{return (self.invoice_head != NULL_STRING && self.invoice_head != "")}
    
    //总价格
}
