//
//  GiftChoiceUnitModel.swift
//  Limi
//
//  Created by guo chen on 16/1/14.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//选礼的单元model  (Deprecated from 1.3.*)


import UIKit

class GiftChoiceUnitModel: NSObject {
    /*
     
     name = img;
     attributes =                     {
     article = "http://wechat.giftyou.me/article_list?type=1&code=grace&app=1";
     "bg_color" = "";
     "font_color" = "";
     goods = "\"\U6587\U827a\"";
     height = "";
     src = "http://up.xinyidiandian.com/14429220827194.png";
     title = "\U6587\U827a\U8303";
     width = "";
     
     */
    
    
    init(dataDic:JSON){
        super.init()
        self.analyisesDataDic(dataDic)
    }
    
    var width:CGFloat   = 0
    
    var height:CGFloat  = 0
    
    
    var src:String      = ""
    
    var name:String     = ""
    
    var title:String    = ""
    
    var goods:String    = ""
    
    var article:String  = ""
    
    
    var bg_color:String     = Constant.common_C9_color
    
    var font_color:String   = Constant.common_C6_color
    
    
    fileprivate func analyisesDataDic(_ dataDic:JSON)
    {
        if let width = dataDic["attributes"]["width"].string
        {
            if width.characters.count > 0
            {
                self.width = CGFloat((width as NSString).intValue)
            }
        }
        
        
        
        if let height = dataDic["attributes"]["height"].string
        {
            if height.characters.count > 0
            {
                self.height = CGFloat((height as NSString).intValue)
            }
        }
        
        if let src = dataDic["attributes"]["src"].string{
            self.src = src
        }
        if let title = dataDic["attributes"]["title"].string{
            self.title = title
        }
        if let article = dataDic["attributes"]["article"].string{
            self.article = article
        }
        if let goods = dataDic["attributes"]["goods"].string{
            self.goods = goods
        }
        
        if let name = dataDic["name"].string{
            self.name = name
        }
        
        if let color = dataDic["attributes"]["bg_color"].string
        {
            if color.characters.count == 6
            {
                self.bg_color = "#" + color
            }
        }
        
        if let color = dataDic["attributes"]["font_color"].string
        {
            if color.characters.count == 6
            {
                self.font_color = "#" + color
            }
        }
        
    }
    
}
