//
//  RechargeModel.swift
//  Limi
//
//  Created by Richie on 16/5/25.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//话费充值，各种话费
import UIKit

class RechargeModel: BaseDataMode {
    
    var id      = NULL_INT
    var name    = NULL_STRING
    var price   = NULL_DOUBLE
    var status  = false
    
    //1:是特惠，2:非特惠
    var is_discount     = NULL_INT
    var check_status    = false
    
    override func customAnalyis(_ data: [String : AnyObject]) {
        if check_status && canSelected{
            self.didSelected = true
        }
    }
    
    
    //Extention:
    var indexPath:IndexPath!
    
    ///是否已经跟服务器校验过数据
    var didChecked = false
    
    var didSelected = false
    
    var canSelected:Bool{return status}
}


class RechargeOfFlowDataModel:RechargeModel{
    /*
     有手机号的时候请求数据
     token	false	string	token值
     mobile	true	string	手机号
     id	false	int	产品ID，传此值时，token必传
     
     
     
     data.rule_url	规则地址
     data.tips	提示语
     data.fees_list[i].Is_discount	是否有优惠，1有 2无
     
     
     流量
     无手机号
     "data": {
     "ad_list": [
     {
     "Id": 6,
     "Img": "http://up.xinyidiandian.com/14462042305726.jpg",
     "Url": "http://wechat.giftyou.me/help/feeHelp"
     }
     ],
     "list": [
     {
     "Id": 7,
     "Name": "20M",
     "Is_discount": 2
     },
     
     有手机号
     "data": {
     "address": "上海 联通",
     "list": [
     {
     "Id": 7,
     "Name": "20M",
     "Is_discount": 2,
     "Price": 30
     },
     
     */
}




class RechargeOfWordsDataModel: RechargeModel {
    
    
    /**
     //有手机号的时候传
     token	false	string	token值
     mobile	true	string	手机号
     id	false	int	产品ID，传此值时，token必须传
     
     data.ad_list	广告列表
     data.rule_url	规则地址
     data.tips	提示语
     data.list[i].Is_discount	是否有优惠，1有 2无
     
     无手机号
     "ad_list": [
     {
     "Id": 6,
     "Img": "http://up.xinyidiandian.com/14462042305726.jpg",
     "Url": "http://wechat.giftyou.me/help/feeHelp"
     }
     ],
     "list": [
     {
     "Id": 1,
     "Name": "30元",
     "Is_discount": 2
     },
     
     
     
     
     有手机号
     "data": {
     "address":"北京 移动",
     "list": [
     {
     "Id": 1,
     "Name": "30元",
     "Is_discount": 1,
     "Price": 25,
     "Status": false
     },
     
     */
}
