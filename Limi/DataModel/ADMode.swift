//
//  ADMode.swift
//  Limi
//
//  Created by guo chen on 16/1/16.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//
//广告模型

import UIKit

class ADMode: NSObject {
    
    
    /*
     {
     attributes={
     height=89;width=414;
     };children=({
     attributes={
     href="http://test.wechat.giftyou.me/app/receivecoupon?code=Beginner&need_uid=1";src="http://up.xinyidiandian.com/14554651107501.jpg";
     };name=img;
     });name=ad;
     },
     
     
     bottom = 1;
     height = 300;
     href = "http://wechat.giftyou.me/article_item?id=815&app=1";
     left = 1;
     right = 1;
     src = "http://up.xydd.co/14664868649004.jpg";
     top = 1;
     width = 960;
     */
    
    
    var id = 0
    ///超链
    var href = ""
    ///名称
    var name = ""
    ///图片
    var src = ""
    
    //判断是否显示新手红包
    var switch_btn = ""
    
    var top = NULL_INT
    var bottom = NULL_INT
    var left = NULL_INT
    var right = NULL_INT
    
    var width = NULL_INT
    var height = NULL_INT
    
    override init() {
        super.init()
    }
    
    init(dataDic:JSON)
    {
        super.init()
        
        self.analyiseData(dataDic)
    }
    
    
    func analyiseData(_ dataDic:JSON)
    {
        if let mhref = dataDic["attributes"]["href"].string{
            if mhref.characters.count > 0{
                self.href = mhref
            }
        }
        
        if let mname = dataDic["name"].string{
            if mname.characters.count > 0{
                self.name = mname
            }
        }
        
        if let msrc = dataDic["attributes"]["src"].string{
            if msrc.characters.count > 0{
                self.src = msrc
            }
        }
        
        if let switch_btn = dataDic["attributes"]["switch_btn"].string{
            if switch_btn.characters.count > 0{
                self.switch_btn = switch_btn
            }
        }
        
        if let Id = dataDic["Id"].int{
            self.id = Id
        }
        
        if let img = dataDic["Img"].string{
            self.src = img
        }
        
        if let href = dataDic["Url"].string{
            self.href = href
        }
        
        if let img = dataDic["src"].string{
            self.src = img
        }
        
        if let href = dataDic["href"].string{
            self.href = href
        }
        
        if let top = dataDic["top"].string {
            self.top = NSString(string:top).integerValue
        }
        
        if let bottom = dataDic["bottom"].string {
            self.bottom = NSString(string:bottom).integerValue
        }
        
        if let left = dataDic["left"].string {
            self.left = NSString(string:left).integerValue
            //            self.left = left
        }
        
        if let right = dataDic["right"].string {
            self.right = NSString(string:right).integerValue
            //            self.right = right
        }
        
        if let width = dataDic["width"].string {
            self.width = NSString(string:width).integerValue
            //            self.width = width
        }
        
        if let height = dataDic["height"].string {
            self.height = NSString(string:height).integerValue
            //            self.height = height
        }
    }
}
