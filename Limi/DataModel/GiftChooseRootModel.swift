//
//  GiftChooseAttibuteModel.swift
//  Limi
//
//  Created by maohs on 16/3/31.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//选礼

import UIKit

class GiftChooseRootModel: BaseDataMode {
    /**
     "attributes": {
     },
     "ad": [],
     "children": [
     ]
     */
    
    
    var ad:[ADMode]?
    
    var children:[GiftChooseChildModel]?
    
    var attributes:GiftChooseRootAttributeModle?
    
    
    override func customAnalyis(_ data: [String : AnyObject]) {
        
        if let attri = data["attributes"] as? [String:AnyObject] {
            attributes = GiftChooseRootAttributeModle(attri)
        }
        
        if let adArray = data["ad"] as? [[String:AnyObject]] {
            if adArray.count > 0 {
                ad = [ADMode]()
            }
        }
        
        
        if let childArray = data["children"] as? [[String:AnyObject]] {
            if childArray.count > 0 {
                children = [GiftChooseChildModel]()
                for index in 0...childArray.count - 1{
                    let child = GiftChooseChildModel(childArray[index])
                    child.row = index
                    children?.append(child)
                }
            }
            
        }
        
    }
    
    
    //extension:
    
    //是否应该展开
    var shouldSpread = false
    
    fileprivate let sectionFixNum = 6
    
    
    var  rows:Int{
        
        var row = 0
        
        if let childs = self.children
        {
            if shouldSpread
            {
                row = childs.count
            }else{
                if childs.count > sectionFixNum{
                    row = sectionFixNum
                }else
                {
                    row = childs.count
                }
            }
        }
        
        return row
    }
    
}

class GiftChooseRootAttributeModle: BaseDataMode {
    /*
     "title": [
     {
     "name" : "为啥送",
     "English_name" : "WHY"
     }
     ]
     */
    var titleAry:[GiftChooseTitleItemModle]?
    override func customAnalyis(_ data: [String : AnyObject]) {
        if let ary = data["title"] as? [[String:AnyObject]] {
            if ary.count > 0 {
                titleAry = [GiftChooseTitleItemModle]()
                let _ = ary.map{titleAry?.append(GiftChooseTitleItemModle($0))}
            }
        }
    }
    
    class GiftChooseTitleItemModle: BaseDataMode {
        var name = NULL_STRING
        var english_name = NULL_STRING
    }
}

