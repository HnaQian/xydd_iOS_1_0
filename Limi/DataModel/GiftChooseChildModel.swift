//
//  GiftChooseModel.swift
//  Limi
//
//  Created by maohs on 16/3/31.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//选礼的子model

import UIKit

class GiftChooseChildModel: BaseDataMode {
    
    /**
     {
     "attributes": {
     },
     "name": "img"
     */
    
    
    var row     = 0
    
    var name    = NULL_STRING
    
    var attributes:GiftChooseItemAttributeModel?
    
    
    
    override func customAnalyis(_ data: [String : AnyObject]) {
        
        if let attr = data["attributes"] as? [String:AnyObject]{
            attributes = GiftChooseItemAttributeModel(attr)
        }
    }
    
    class GiftChooseItemAttributeModel:BaseDataMode{
        /*
         "height": "618",
         "article": "\"生日\"",
         "bg_color": "",
         "src": "http://up.xinyidiandian.com/14554652753699.jpg?imageView2/3/w/618/h/618",
         "goods": "\"生日\"",
         "title": "生日1",
         "width": "618",
         "font_color": ""
         */
        
        var src     = NULL_STRING
        var goods   = NULL_STRING
        var title   = NULL_STRING
        var width   = NULL_STRING
        var height  = NULL_STRING
        var article = NULL_STRING
        
        var bg_color    = NULL_STRING
        var font_color  = NULL_STRING
    }
    
}

