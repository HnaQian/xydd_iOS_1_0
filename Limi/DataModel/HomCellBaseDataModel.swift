//
//  HomCellBaseDataModel.swift
//  Limi
//
//  Created by Richie on 16/6/13.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
class HomCellBaseDataModel: BaseDataMode {
    
    //需要子类复写
    func classMap()->HomeBaseTableViewCell.Type{
        assert(false, "需要子类重写")
        return HomeBaseTableViewCell.self
    }
    
    //Type:1:攻略 2:带商品的攻略 3:自选礼盒
    //子类需要复写
    func getModelType()->Int{return 0}
    func getCellHeight()->CGFloat{return 0}
}
