//
//  GiftServiceModel.swift
//  Limi
//
//  Created by Richie on 16/6/21.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

//心意服务model
import UIKit

class GiftServiceModel: BaseDataMode {
    
    /*
     "card": {
     "choose_service_id": 53,
     "choose_title": "浓浓的圣诞气氛",
     "choose_price": 15.5,
     "attr": "A4纸质",
     "image": "http://up.xinyidiandian.com/default.jpg"
     },
     "audio": {
     "choose_service_id": 57,
     "second": 3
     
     
     
     attr = "\U683c\U8bf4\U660e";
     "choose_price" = 22;
     "choose_service_id" = 40;
     "choose_title" = "\U793c\U76d2\U670d\U52a1\U670d\U52a1\U526f\U6807\U9898";
     image = "http://up.xydd.co/14606238626245.jpg";
     */
    
    var attr    = NULL_STRING
    var image   = NULL_STRING
    var second  = NULL_INT //时间 专用于录音
    
    var choose_title        = NULL_STRING
    var choose_price        = NULL_DOUBLE
    var choose_service_id   = NULL_INT
    
    override init(_ data: [String : AnyObject]!) {
        super.init(data)
    }
    
    /**
     type:  1:box 2:card 3:audio
     */
    init(data: [String : AnyObject]!,type:Int){
        super.init(data)
        self.type = type
    }
    
    //extension:
    //1:box 2:card 3:audio
    var type = 0
    
    var name = ""
}


extension GiftServiceModel:GoodsGenericModel{
    
    //商品名称
    var goodsName:String{return self.choose_title}
    
    var goodsImage:String{return self.image}
    
    var goodsOfOrderType:Int{return 5}
    
    ///Extension: 提交订单时的数量:选择的商品规格数量
    var selectedGoodsSubModelCount:Int{return 1}
    
    ///Extension: 提交订单时的单价:选择的规格价格(也可能没有选择规格，返回结果已经把此逻辑计算在内)
    var selelctedGoodsSubModelPrice:Double{return self.choose_price}
    
    ///Extension: 原价：失效的价格
    var expirationPrice:Double{return self.choose_price}
    
    /// Extension: 现价：未选商品规格的实际支付价格
    var currentPrice:Double{return self.choose_price}
    
    /// Extension: 当前选择的规格的名称数组
    var selectedGoodsAttrNames:[String]{return [attr]}
}
