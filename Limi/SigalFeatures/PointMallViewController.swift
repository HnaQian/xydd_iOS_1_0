//
//  PointMallViewController.swift
//  Limi
//
//  Created by maohs on 16/5/31.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//积分商城
import UIKit
import CoreData

class PointMallViewController: BaseViewController,UICollectionViewDataSource,UICollectionViewDelegate,PointMallTitleViewDelegate,DailySignCollectionViewCellDelegate,PointMallDialogDelegate {
    
    static let count = 4   //每次加载的个数
    
    fileprivate var pointListArray = [DetailListMode]()
    
    fileprivate var dialogView = PointMallDialog()
    
    fileprivate var userInfomation : User? = nil
    
    fileprivate var dialogInfo = [String:AnyObject]()
    
    var titleView = PointMallTitleView()
    
    lazy var flowLayout:UICollectionViewFlowLayout = {
        var tempFlowLayout = UICollectionViewFlowLayout()
        tempFlowLayout.sectionInset = UIEdgeInsetsMake(MARGIN_10, MARGIN_10, MARGIN_10, MARGIN_10)
        tempFlowLayout.headerReferenceSize = CGSize(width: Constant.ScreenSizeV1.SCREEN_WIDTH,height: 243 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE)
        return tempFlowLayout
    }()

    lazy var collectionView: UICollectionView = {
        let frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV1.SCREEN_WIDTH, height: self.view.iheight - 64 * Constant.ScreenSizeV1.SCREEN_HEIGHT_SCALE)
        var tempCollectionView = UICollectionView(frame: frame, collectionViewLayout: self.flowLayout)
        tempCollectionView.delegate = self
        tempCollectionView.dataSource = self
        tempCollectionView.bounces = true
        tempCollectionView.alwaysBounceVertical = true
        
        tempCollectionView.register(DailySignCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        tempCollectionView.register(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "header")

        
        tempCollectionView.backgroundColor = UIColor.clear
        return tempCollectionView
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let requestUserInfo: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        let datasourceUserInfo : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(requestUserInfo)!
        if datasourceUserInfo.count > 0{
            userInfomation = datasourceUserInfo[0] as? User
        }
        else{
            userInfomation = nil
        }
        self.resetPointMallTitleView(userInfomation)
        var shouldShowError = false
        if UserInfoManager.didLogin == true {
            shouldShowError = true
        }
        let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.user_info_v2,isNeedUserTokrn: true, isShowErrorStatuMsg: shouldShowError, successHandler: { (data, status, msg) -> Void in
            if status != 200
            {
                return
            }
            
            if let dataJson : JSON = JSON(data["data"]!) as JSON?{
                
                if let user : JSON = dataJson["user"] as JSON?{
                    CoreDataManager.shared.update(user, entityType: Constant.CoreDataType.userInfoType)
                     self.changeCoinLabelValue(0)
                }
            }
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.view.backgroundColor = UIColor(rgba: Constant.common_C5_color)
        self.navigationItem.title = "心意商城"
        
        self.fetchCollectionViewData(false)
        self.view.addSubview(self.collectionView)
        
        self.dialogView.delegate = self
        self.collectionView.addFooterRefresh { [weak self]() -> Void in
            self?.fetchCollectionViewData(true)
        }
    }
    
    
    func pointMallTitleViewClicked() {
        if UserInfoManager.didLogin == true
        {
            let convertNotesVC = ConvertNotesViewController()
            self.navigationController?.pushViewController(convertNotesVC, animated: true)
        }else {
            self.loginWithType()
        }
       
    }
    
    fileprivate func fetchCollectionViewData(_ loadMore:Bool) {
        var params = [String:AnyObject]()
        if self.pointListArray.count > 0 {
            params["size"] = PointMallViewController.count as AnyObject?
            params["last_id"] = self.pointListArray.last?.id as AnyObject?
        }
        
        var shouldShowError = false
        if UserInfoManager.didLogin == true {
            shouldShowError = true
        }
        let _ = LMRequestManager.requestByDataAnalyses(context: self.view, URLString:Constant.JTAPI.point_goodslist, parameters: params,isNeedUserTokrn: true,isShowErrorStatuMsg:shouldShowError, successHandler: { (data, status, msg) in
            self.collectionView.endFooterRefresh()
            if status != 200 {
                if self.pointListArray.count == 0 {
                }
                return
            }
            
            var count = 0
            
            if let tempArray = JSON(data as AnyObject)["data"]["list"].arrayObject {
                for index in 0 ..< tempArray.count {
                    let detailList = DetailListMode(tempArray[index] as! [String:AnyObject])
                    self.pointListArray.append(detailList)
                }
                count = tempArray.count
            }
            
            if count < PointMallViewController.count {
                //加载全部
                self.collectionView.endFooterRefreshWithNoMoreData()
            }
            
            self.collectionView.reloadData()
            
        })
    }
    
    //MARK: - collectionView dataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.pointListArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! DailySignCollectionViewCell
        cell.delegate = self
        cell.refreshData(self.pointListArray[(indexPath as NSIndexPath).row],type: 1)
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize{
        //172  240
        let width:CGFloat = (Constant.ScreenSize.SCREEN_WIDTH - MARGIN_10 * 3)/2.0
        let height:CGFloat = 234 * Constant.ScreenSizeV1.SCREEN_HEIGHT_SCALE
        
        return CGSize(width: width, height: height)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        var headerView : UICollectionReusableView! = nil
        switch kind {
        case UICollectionElementKindSectionHeader:
            headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "header", for: indexPath)
            
            if titleView.superview == nil{
                titleView.frame = headerView.bounds
                headerView.addSubview(titleView)
                titleView.delegate = self
            }
            
            return headerView
            
        default:
            assert(false, "Unexpected element kind")
            return UICollectionReusableView()
        }
        return headerView
    }


    func ensureButtonSelected(_ type: Int) {
        if type == 0 {
            // 立即兑换
            var params = [String:AnyObject]()
            params["goods_id"] = self.dialogInfo["id"]!
           
            let _ = LMRequestManager.requestByDataAnalyses(context: self.view, URLString:Constant.JTAPI.point_charge, parameters: params,isNeedUserTokrn: true,isShowErrorStatuMsg:true, successHandler: { (data, status, msg) in
                
                if status != 200 {
                    return
                }
                if self.view.window != nil {
                    self.dialogView.show(msg,info: String(describing: self.dialogInfo["name"]!),type: 1)
                    self.changeCoinLabelValue(Int(self.dialogInfo["point"]! as! NSNumber))
                }
                
            })
                
        }else {
            // 立即充值
            let rechargeVC = RechargeViewController()
            self.navigationController?.pushViewController(rechargeVC, animated: true)
        }
    }
    
    func dailySignCollectionViewCellClicked(_ userInfo: [String : AnyObject]) {
        self.dialogInfo = userInfo
        if UserInfoManager.didLogin == true
        {
            let tempString = "您兑换的是" + String(describing: userInfo["name"]!) + ",确认兑换吗?"
            self.dialogView.show("确认兑换",info: tempString)
        }else {
            self.loginWithType()
        }
        
    
    }

    
    func loginWithType(){
        self.jumpToLogin(Constant.InLoginType.pointMallType, tipe: nil)
    }

    func resetPointMallTitleView(_ userInfo: User?) {
        var tempCount = 0
        
        if userInfo != nil {
            
            if UserInfoManager.didLogin == true
            {
                //MARK:头像
                if userInfo!.avatar != "" && userInfo!.avatar != nil
                {
                    let imageScale : String = userInfo!.avatar! as String + "?imageView2/0/w/" + "\(Int(90*Constant.ScreenSizeV1.SCALE_SCREEN))" + "/h/" + "\(Int(90*Constant.ScreenSizeV1.SCALE_SCREEN))"
                    
                    self.titleView.userImage.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "accountIcon"))
                    
                }else
                {
                    self.titleView.userImage.image = UIImage(named: "accountIcon")
                }
            }
            else
            {
                if userInfo != nil && userInfo!.avatarData != nil {
                    self.titleView.userImage.image = UIImage(data: userInfo!.avatarData! as Data)
                }else{
                    self.titleView.userImage.image = UIImage(named: "accountIcon")
                }
            }
            
            //MARK:姓名
            var content: String?
            
            if userInfo!.nick_name != "" && userInfo!.nick_name != nil
            {
                content = userInfo!.nick_name
            }
            else
            {
                content = userInfo!.user_name
            }
            self.titleView.userNameLabel.text = Utils.htmalTrusletToText(content)
            
            //MARK:心意币
            if userInfo!.point > 0 {
                tempCount = Int(userInfo!.point)
            }
            
        }
        
        let tempString = "我的心意币 " + String(tempCount)
        let attributedString1 = NSMutableAttributedString(string: tempString as String)
        let firstAttributes = [NSForegroundColorAttributeName: UIColor(rgba: Constant.common_C1_color)]
        attributedString1.addAttributes(firstAttributes, range: NSMakeRange(6,String(tempCount).characters.count))
        self.titleView.leftCoinLabel.attributedText = attributedString1
    }

        
    func changeCoinLabelValue(_ point:Int) {
        if userInfomation != nil {
            var tempCount = Int(userInfomation!.point)
            
            tempCount -= point
            let tempString = "我的心意币 " + String(tempCount)
            let attributedString1 = NSMutableAttributedString(string: tempString as String)
            let firstAttributes = [NSForegroundColorAttributeName: UIColor(rgba: Constant.common_C1_color)]
            attributedString1.addAttributes(firstAttributes, range: NSMakeRange(6,String(tempCount).characters.count))
            self.titleView.leftCoinLabel.attributedText = attributedString1
            
            if let homeDB = CoreDataManager.shared.getDBManager(Constant.CoreDataType.userInfoType),
                let db = homeDB as? User
            {
                db.point = Int64(tempCount)
                CoreDataManager.shared.save()
            }

        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
