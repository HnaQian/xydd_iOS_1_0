//
//  RechargeViewController.swift
//  Limi
//
//  Created by Richie on 16/5/25.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit


class RechargeViewController: BaseViewController {
    
    fileprivate var rechargeOfWordsView:RechargeView!
    fileprivate var rechargeOfFlowView:RechargeView!
    
    fileprivate var currentRechargeView:RechargeView!
    
    fileprivate let bottomTool:BottomToolsBar = {
        
        let bottomTool = BottomToolsBar()
        bottomTool.title = "实付金额: "
        bottomTool.content = "￥0.00"
        bottomTool.actionBtnTitle = "立即充值"
        
        bottomTool.actionBtnStyle = 2
        bottomTool.setContent(UIColor(rgba:Constant.common_C1_color), textFont: Constant.Theme.FontSize_16)
        bottomTool.setTitle(UIColor(rgba:Constant.common_C8_color), textFont: Constant.Theme.FontSize_16)
        
        return bottomTool
        
    }()
    
    fileprivate var pageScrollView = LazyPageScrollView(verticalDistance: 0, tabItemType: TabItemType.customView)
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.navigationItem.title = "充值中心"
        
        setupUI()
    }
    
    
    fileprivate func commitOrder(){
        
        /**
         token	true	string	登录token值
         mobile	true	string	手机号
         id	true	int	产品ID
         coupon_id	true	int	优惠券ID，没有传0
         source	false	int	订单来源：1 iOS app,2 android app,3 微信,4 pc web,5 mobile web
         */
        
        let para:[String : AnyObject] = [
            "source":1 as AnyObject,
            "mobile": currentRechargeView.phoneNumber as AnyObject,
            "id": currentRechargeView.rechargeModel!.id as AnyObject,
            "coupon_id":currentRechargeView.couponModel == nil ? 0 as AnyObject : currentRechargeView.couponModel!.id as AnyObject
        ]
        let _ = LMRequestManager.requestByDataAnalyses(context: self.view, URLString: currentRechargeView.commitUrl, parameters: para, isNeedUserTokrn: true, successHandler: { (data, status, msg) in
            
            if status == 200{
                
                if let id = JSON(data as AnyObject)["data"]["id"].int{
                    if self.currentRechargeView.sumCost <= 0{
                        //跳转到选择支付结果页面
                        self.navigationController?.pushViewController(ResultOfPayViewController.newResultVC(id), animated: true)
                    }else{
                        //跳转到选择支付页面
                        self.navigationController?.pushViewController(PayViewController.newPayViewController(id,canclePay:.forward), animated: true)
                    }
                    
                    self.currentRechargeView.reloadData()
                }
            }
        })
    }
    
    //充值说明
    @objc fileprivate func introduction(){
        if let url = URL(string: currentRechargeView.rule_url){
            EventDispatcher.dispatch(URL: url, onNavigationController: self.navigationController)
        }
    }
    
    //刷新底部按钮
    fileprivate func refreshBottom(){

        if let _ = currentRechargeView.rechargeModel{
            bottomTool.actionBtnStyle = 0
            bottomTool.setContent(UIColor(rgba:Constant.common_C1_color))
            
        }else{
            bottomTool.actionBtnStyle = 2
            bottomTool.setContent(UIColor(rgba:Constant.common_C8_color))
        }
        
        bottomTool.content =  NSString(format:"￥ %.2f",currentRechargeView.sumCost) as String
    }
    
  
    fileprivate func setupUI(){
        
        self.view.backgroundColor = UIColor.white
        
        //列表
        var frame = self.view.bounds
        frame.size.height = frame.size.height - 64 - 49

        rechargeOfWordsView = BaseRechargeView(frame: CGRect(x: 0,y: 0,width: Constant.ScreenSize.SCREEN_WIDTH,height: frame.size.height - pageScrollView.tabItem.iheight),type:0)
        
        rechargeOfFlowView = BaseRechargeView(frame: CGRect(x: 0,y: 0,width: Constant.ScreenSize.SCREEN_WIDTH,height: rechargeOfWordsView.view.iheight),type:1)
        
        currentRechargeView = rechargeOfWordsView
        
        let _ = [rechargeOfFlowView,rechargeOfWordsView].map{$0?.viewController = self;$0?.delegate = self}
        
        self.view.addSubview(pageScrollView)
        pageScrollView.delegate = self
        pageScrollView.segment(["话费充值", "流量充值"], views: [rechargeOfWordsView.view,rechargeOfFlowView.view], frame: frame)
        pageScrollView.updateGapHeight(-2)
        
        //底部工具条
        self.view.addSubview(bottomTool)
        bottomTool.frame = CGRect(x: MARGIN_8, y: self.view.iheight - 49 - 64, width: self.view.iwidth - MARGIN_8 * 2, height: 49)
        bottomTool.delegate = self
        
        let lineView = UIView(frame: CGRect(x: 0,y: bottomTool.itop,width: self.view.iwidth,height: 0.5))
        lineView.backgroundColor = UIColor(rgba: Constant.common_separatLine_color)
        self.view.addSubview(lineView)
        
        //右上角的充值说明按钮
        self.navigationItem.rightBarButtonItem = CommonWidgets.NavigationRightBtn.newRightBtn("充值说明", bgImgName: "", target: self, action: #selector(RechargeViewController.introduction))
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}



extension RechargeViewController:BaseRechargeViewDelegate,OrderUnitViewDelegate,LazyPageScrollViewDelegate{
    
    //选择了某个面值card
    func rechargeViewCardsDidselected(_ rechargeView:BaseRechargeView,rechargeModel:RechargeModel,phoneNumber:String){
        refreshBottom()
    }
    
    //选择了红包
    func rechargeViewCouponDidselected(_ rechargeView:BaseRechargeView,couponModel:CouponModel,phoneNumber:String){
        refreshBottom()
    }
    
    //改变了手机号码 此时应该把总金额置为0，结算按钮置灰
    func rechargeViewCouponDidchangePhoneNumber(_ rechargeView:BaseRechargeView){
        refreshBottom()
    }
    
    
    //底部工具条
    func orderBottomToolBarAction(_ tooBar:BottomToolsBar){
        
        if tooBar.actionType == BottomToolsBar.ActionType.actionBtnTouch{
            //去支付
            if bottomTool.actionBtnStyle == 0{
                if !UserInfoManager.didLogin{
                    self.jumpToLogin(Constant.InLoginType.defaultType, tipe: nil)
                }else{
                    self.commitOrder()
                }
            }
        }
    }
    
    func lazyPageScrollView(_ pageScrollView: LazyPageScrollView, changeIndex index: Int) {
        
        let tempView = index == 0 ? rechargeOfWordsView : rechargeOfFlowView

        currentRechargeView = tempView
        
        //刷新当前的数据
        currentRechargeView.reloadData()
        refreshBottom()
    }
    
    func lazyPageScrollViewEdgeSwipeLeft(_ isLeft: Bool) {
        
    }
    
}
