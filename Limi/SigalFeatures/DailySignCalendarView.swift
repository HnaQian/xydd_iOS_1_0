//
//  DailySignCalendarView.swift
//  Limi
//
//  Created by maohs on 16/5/25.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//积分签到日历
import UIKit

public protocol DailySignCalendarViewDelegate:NSObjectProtocol {
    func DailySignCalendarViewClicked()
}

class DailySignCalendarView: BaseView,UICollectionViewDataSource,UICollectionViewDelegate,DailySignCalendarFooterDelegate {

    lazy var flowLayout:UICollectionViewFlowLayout = {
        var tempFlowLayout = UICollectionViewFlowLayout()

        tempFlowLayout.minimumLineSpacing = DailySignCalendarViewCell.interitemSpacing
        tempFlowLayout.minimumInteritemSpacing = DailySignCalendarViewCell.interitemSpacing
        
        tempFlowLayout.sectionInset = UIEdgeInsetsMake(DailySignCalendarViewCell.lineSpacing, DailySignCalendarViewCell.interitemSpacing, DailySignCalendarViewCell.lineSpacing, DailySignCalendarViewCell.interitemSpacing)
        
        tempFlowLayout.headerReferenceSize = CGSize(width: DailySignCalendarHeader.width,height: DailySignCalendarHeader.height)
        tempFlowLayout.footerReferenceSize = CGSize(width: DailySignCalendarFooter.width,height: DailySignCalendarFooter.height)
        
        return tempFlowLayout
    }()
    
    lazy var collectionView: UICollectionView = {
        let frame = CGRect(x: 20 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, y: 20 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, width: DailySignCalendarFooter.width,  height: DailySignCalendarFooter.height + DailySignCalendarHeader.height + DailySignCalendarViewCell.height * 5 + DailySignCalendarViewCell.interitemSpacing * 6)
        
        var tempCollectionView = UICollectionView(frame: frame, collectionViewLayout: self.flowLayout)
        tempCollectionView.delegate = self
        tempCollectionView.dataSource = self
        tempCollectionView.bounces = true
        tempCollectionView.alwaysBounceVertical = true
        tempCollectionView.layer.cornerRadius = 5
        tempCollectionView.layer.masksToBounds = true
        tempCollectionView.isScrollEnabled = false
        
        tempCollectionView.register(DailySignCalendarViewCell.self, forCellWithReuseIdentifier: DailySignCalendarViewCell.reuseIdentifier)
        
        //注册header
        tempCollectionView.register(DailySignCalendarHeader.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "Header")
        
        //注册footer
        tempCollectionView.register(DailySignCalendarFooter.self, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "Footer")
        
        tempCollectionView.backgroundColor = UIColor.white
        
        return tempCollectionView
    }()
    
    var dailySign:DailySignModel! {
        didSet {
            
            self.dateListArray = dailySign.date_list!
            self.firstDay_week = dateListArray[0].week
            if self.firstDay_week != 7 {
                let tempCount = self.dateListArray.count + self.firstDay_week
                if tempCount > 35 {
                    self.collectionView.iheight = (DailySignCalendarFooter.height + DailySignCalendarHeader.height + DailySignCalendarViewCell.height * 6 + DailySignCalendarViewCell.interitemSpacing * 7 )
                }
            }
            self.currentDay = dailySign.day_now.components(separatedBy: "-")[2]
            self.collectionView.reloadData()
        }
    }
    
    var dateListArray = [DailySignDate_listModel]()
    
    var firstDay_week:Int = -1
    
    var currentDay:String?
    
    weak var delegate:DailySignCalendarViewDelegate?
    
    override func defaultInit() {
        setUpUI()
    }

    func setUpUI() {
        self.backgroundColor = UIColor.clear
        self.addSubview(self.collectionView)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //判断第一天是否是星期日
        if dateListArray.count > 0 {
            
            if firstDay_week != 7 {
                return dateListArray.count + firstDay_week
            }
        }
        
        return dateListArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DailySignCalendarViewCell.reuseIdentifier, for: indexPath) as! DailySignCalendarViewCell
        if firstDay_week != -1 {
            if firstDay_week != 7 {
                if (indexPath as NSIndexPath).row >= firstDay_week {
                    cell.reloadData(dateListArray[(indexPath as NSIndexPath).row - firstDay_week], currentDay: self.currentDay!)
                }else {
                    cell.contentLabel.text = ""
                    cell.bgImageView.image = nil
                }
            }else {
                cell.reloadData(dateListArray[(indexPath as NSIndexPath).row], currentDay: self.currentDay!)
            }
        }
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize{
        
        let width:CGFloat = DailySignCalendarViewCell.width
        let height:CGFloat = DailySignCalendarViewCell.height;

        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        precondition(UICollectionElementKindSectionHeader == kind || UICollectionElementKindSectionFooter == kind ,"Unexpected element kind")
        if UICollectionElementKindSectionHeader == kind {
            let headerView:DailySignCalendarHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "Header", for: indexPath) as! DailySignCalendarHeader
            if dateListArray.count > 0 {
                headerView.currentTimeLabel.text = dailySign.day_tips
                headerView.creditLabel.text = "我的积分:" + String(dailySign.user_point) + "心意币"
            }
            return headerView

        }
        
        let footerView : DailySignCalendarFooter  = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "Footer", for: indexPath) as! DailySignCalendarFooter
        if dateListArray.count > 0 {
            footerView.resetSignButton(self.dailySign, currentDay: self.currentDay!)
        }
        footerView.delegate = self
        return footerView
        
//        switch kind {
//        case UICollectionElementKindSectionHeader:
//            let headerView:DailySignCalendarHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "Header", for: indexPath) as! DailySignCalendarHeader
//            if dateListArray.count > 0 {
//                headerView.currentTimeLabel.text = dailySign.day_tips
//                headerView.creditLabel.text = "我的积分:" + String(dailySign.user_point) + "心意币"
//            }
//            return headerView
//            
//        case UICollectionElementKindSectionFooter:
//            let footerView : DailySignCalendarFooter  = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "Footer", for: indexPath) as! DailySignCalendarFooter
//            if dateListArray.count > 0 {
//                footerView.resetSignButton(self.dailySign, currentDay: self.currentDay!)
//            }
//            footerView.delegate = self
//            return footerView
//            
//        default:
//            assert(false, "Unexpected element kind")
//        }
    }

    func DailySignCalendarFooterClicked() {
        delegate?.DailySignCalendarViewClicked()
    }
}
