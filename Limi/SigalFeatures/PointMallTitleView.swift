//
//  PointMallTitleView.swift
//  Limi
//
//  Created by maohs on 16/5/31.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class PointMallTitleView: UIView {

    fileprivate var titleView = UIView()
    
    fileprivate var bgImage = UIImageView()
    
    var userImage = UIImageView()
    
    var userNameLabel = UILabel()
    
    fileprivate var leftCoinImage = UIImageView()
    
    var leftCoinLabel = UILabel()
    
    fileprivate var notesImage = UIImageView()
    
    fileprivate var notesButton = UIButton(type: UIButtonType.custom)
    
    weak var delegate:PointMallTitleViewDelegate?
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setUpUI()
    }
    
    func setUpUI() {
        
        self.addSubview(titleView)
        titleView.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: 210 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE)
        titleView.backgroundColor = UIColor.white
        
        titleView.addSubview(bgImage)
        bgImage.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: 160 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE)
        bgImage.image = UIImage(named: "pointmallbg")
        
        bgImage.addSubview(userImage)
        userImage.frame = CGRect(x: 0, y: 30 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, width: 80 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, height: 80 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE)
        userImage.icenterX = bgImage.icenterX
        userImage.layer.borderColor = UIColor(rgba: Constant.common_C12_color).cgColor
        userImage.layer.cornerRadius = userImage.frame.width/2
        userImage.layer.masksToBounds = true
        userImage.layer.borderWidth = 2.0
        userImage.image = UIImage(named: "accountIcon")
        
        bgImage.addSubview(userNameLabel)
        userNameLabel.frame = CGRect(x: 0, y: userImage.ibottom + 12 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, width: bgImage.iwidth, height: 18 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE)
        userNameLabel.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        userNameLabel.textColor = UIColor(rgba: Constant.common_C12_color)
        userNameLabel.textAlignment = .center
        userNameLabel.text = "未登录用户"
        
        titleView.addSubview(leftCoinImage)
        leftCoinImage.frame = CGRect(x: MARGIN_20, y: bgImage.ibottom + 13 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, width: 20 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, height: 20 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE)
        leftCoinImage.image = UIImage(named: "leftcoin")
        
        titleView.addSubview(leftCoinLabel)
        leftCoinLabel.frame = CGRect(x: leftCoinImage.iright + MARGIN_4, y: leftCoinImage.itop, width: Constant.ScreenSizeV1.SCREEN_WIDTH / 2 - 30, height: leftCoinImage.iheight)
        leftCoinLabel.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        leftCoinLabel.textColor = UIColor(rgba: Constant.common_C2_color)
        
        let tempString = "我的心意币 " + String(0)
        let attributedString1 = NSMutableAttributedString(string: tempString as String)
        let firstAttributes = [NSForegroundColorAttributeName: UIColor(rgba: Constant.common_C1_color)]
        attributedString1.addAttributes(firstAttributes, range: NSMakeRange(6,1))
        leftCoinLabel.attributedText = attributedString1
        
        let seperateLineView = UIView()
        titleView.addSubview(seperateLineView)
        seperateLineView.frame = CGRect(x: 0,y: bgImage.ibottom + 16 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, width: 1, height: 24 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE)
        seperateLineView.backgroundColor = UIColor(rgba: Constant.common_C10_color)
        seperateLineView.icenterX = bgImage.icenterX
        
        titleView.addSubview(notesImage)
        notesImage.frame = CGRect(x: seperateLineView.iright + 45 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, y: leftCoinImage.itop, width: leftCoinImage.iwidth, height: leftCoinImage.iheight)
        notesImage.image = UIImage(named: "notes")
        
        titleView.addSubview(notesButton)
        notesButton.frame = CGRect(x: notesImage.iright + MARGIN_4, y: notesImage.itop, width: 80, height: notesImage.iheight)
        notesButton.setTitle("兑换记录", for: UIControlState())
        notesButton.setTitleColor(UIColor(rgba: Constant.common_C2_color), for: UIControlState())
        notesButton.titleLabel?.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        notesButton.addTarget(self, action: #selector(PointMallTitleView.notesButtonClicked(_:)), for: .touchUpInside)
        
        let seperateLine = UIView()
        self.addSubview(seperateLine)
        seperateLine.frame = CGRect(x: 0, y: titleView.ibottom, width: Constant.ScreenSizeV1.SCREEN_WIDTH, height: 1)
        seperateLine.backgroundColor = UIColor(rgba: Constant.common_C10_color)
        
        let convertLabel = UILabel()
        self.addSubview(convertLabel)
        convertLabel.frame = CGRect(x: MARGIN_10, y: titleView.ibottom + 20 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, width: 200, height: 15)
        convertLabel.font = UIFont.systemFont(ofSize: Constant.common_F5_font)
        convertLabel.textColor = UIColor(rgba: Constant.common_C6_color)
        convertLabel.text = "心意币兑换"
        
    }
    
    @objc fileprivate func notesButtonClicked(_ sender:AnyObject) {
        delegate?.pointMallTitleViewClicked()
    }
    
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}

public protocol PointMallTitleViewDelegate:NSObjectProtocol {
    func pointMallTitleViewClicked()
}
