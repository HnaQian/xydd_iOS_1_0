//
//  DailySignDialog.swift
//  Limi
//
//  Created by maohs on 16/5/30.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

public protocol DailySignDialogDelegate:NSObjectProtocol {
    func convertButtonSelected()
}

class DailySignDialog: UIView {

    fileprivate var dialogView:UIView = {
        var tempView = UIView(frame: CGRect(x: 0, y: 0, width: 290 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, height: 375 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE))
        tempView.backgroundColor = UIColor.white
        tempView.layer.cornerRadius = 5.0
        tempView.layer.masksToBounds = true
        return tempView
    }()
    
    var ensureButton:UIButton = {
        var tempButton = UIButton(type: UIButtonType.custom)
        tempButton.setTitle("朕知道了", for: UIControlState())
        tempButton.setTitleColor(UIColor(rgba: Constant.common_C1_color), for: UIControlState())
        tempButton.titleLabel?.font = UIFont.systemFont(ofSize: Constant.common_F1_font)
        tempButton.layer.borderColor = UIColor(rgba: Constant.common_C1_color).cgColor
        tempButton.layer.borderWidth = 1.0
        tempButton.layer.cornerRadius = 5.0
        tempButton.layer.masksToBounds = true
        
        return tempButton
    }()
    
    var convertButton:UIButton = {
        var tempButton = UIButton(type: UIButtonType.custom)
        tempButton.setTitle("去兑换", for: UIControlState())
        tempButton.setTitleColor(UIColor(rgba: Constant.common_C12_color), for: UIControlState())
        tempButton.titleLabel?.font = UIFont.systemFont(ofSize: Constant.common_F1_font)
        tempButton.layer.cornerRadius = 5.0
        tempButton.layer.masksToBounds = true
        tempButton.backgroundColor = UIColor(rgba: Constant.common_C1_color)
        return tempButton
    }()
    
    fileprivate var titleLabel:UILabel = {
        var tempLabel = UILabel()
        tempLabel.backgroundColor = UIColor(rgba: "#ff5b5b")
        tempLabel.textAlignment = .center
        tempLabel.font = UIFont.systemFont(ofSize: Constant.common_F1_font)
        tempLabel.textColor = UIColor(rgba: Constant.common_C12_color)
        tempLabel.text = "每日一签"
        return tempLabel
    }()
    
    fileprivate var todayView = customView()
    fileprivate var rewardView = customView()
    fileprivate var totalView = customView()
    
    weak var delegate:DailySignDialogDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setUpUI()
    }
    
    func setUpUI() {
        self.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        self.alpha = 0
        
        self.addSubview(dialogView)
        
        dialogView.addSubview(titleLabel)
        dialogView.addSubview(todayView)
        dialogView.addSubview(rewardView)
        dialogView.addSubview(totalView)
        dialogView.addSubview(ensureButton)
        dialogView.addSubview(convertButton)
        
        
        titleLabel.frame = CGRect(x: 0, y: 0, width: dialogView.iwidth, height: 50 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE)
        
        todayView.frame = CGRect(x: 0, y: titleLabel.ibottom + MARGIN_10, width: titleLabel.iwidth, height: 80 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE)
        
        rewardView.frame = CGRect(x: 0, y: todayView.ibottom, width: todayView.iwidth, height: todayView.iheight)
        
        totalView.frame = CGRect(x: 0, y: rewardView.ibottom, width: todayView.iwidth, height: todayView.iheight)
        
        ensureButton.frame = CGRect(x: 18 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, y: totalView.ibottom + 20 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, width: 120 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, height: todayView.iheight / 2.0)
        ensureButton.addTarget(self, action: #selector(DailySignDialog.ensureButtonClicked(_:)), for: .touchUpInside)
        
        convertButton.frame = CGRect(x: ensureButton.iright + 14 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, y: ensureButton.itop, width: ensureButton.iwidth, height: todayView.iheight / 2.0)
        convertButton.addTarget(self, action: #selector(DailySignDialog.convertButtonClicked(_:)), for: .touchUpInside)
        
    }
    
    @objc fileprivate func ensureButtonClicked(_ sender:AnyObject) {
        self.dismiss()
    }
    
    @objc fileprivate func convertButtonClicked(_ sender:AnyObject) {
        self.dismiss()
        delegate?.convertButtonSelected()
    }
    
    func show(_ info:[String:AnyObject]) {
        if self.superview == nil {
            if let window = UIApplication.shared.keyWindow {
                self.frame = window.bounds
                window.addSubview(self)
            }
        }
        
        todayView.coinImage.image = UIImage(named: "coin1")
        todayView.nameLabel.text = "今日签到获得"
        if info["today_point"] != nil {
           todayView.coinLabel.text = String(describing: info["today_point"]!) + "心意币"
        }
//        todayView.coinLabel.text = "10心意币"
        
        rewardView.coinImage.image = UIImage(named: "coin1")
        rewardView.nameLabel.text = "连续签到获得"
       
        
        totalView.coinImage.image = UIImage(named: "coin2")
        totalView.nameLabel.text = "累计获得"
        if info["total_point"] != nil {
            totalView.coinLabel.text = String(describing: info["total_point"]!) + "心意币"
        }
//        totalView.coinLabel.text = "20心意币"
        
        
        //是否有额外奖励
        var didExtra = false
        
        if let point = info["reward_point"] as? Int {
            if point != 0{
                didExtra = true
            }
        }
        if !didExtra {
            self.dialogView.iheight = self.dialogView.iheight - 80 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE
            rewardView.isHidden = true
            totalView.itop = todayView.ibottom
            
        }else {
            //didExtra == true
            dialogView.iheight = 375 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE
            rewardView.isHidden = false
            totalView.itop = rewardView.ibottom
            rewardView.coinLabel.text = String(describing: info["reward_point"]!) + "心意币"
        }
        ensureButton.itop = totalView.ibottom + 20 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE
        convertButton.itop = totalView.ibottom + 20 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE
        
        dialogView.center = self.center
        
        UIView.animate(withDuration: 0.25, animations: { 
            self.dialogView.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.alpha = 1
            }, completion: { (done) in
        }) 
        
    }
    
    func dismiss() {
        UIView.animate(withDuration: 0.25, animations: { 
            self.alpha = 0
            }, completion: { (done) in
             self.dialogView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        }) 
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    final class customView:UIView {
        
        var coinImage = UIImageView()
        
        var nameLabel:UILabel = {
            var tempLabel = UILabel()
            tempLabel.textAlignment = .left
            tempLabel.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
            tempLabel.textColor = UIColor(rgba: Constant.common_C6_color)
            return tempLabel
        }()
        
        var coinLabel:UILabel = {
            var tempLabel = UILabel()
            tempLabel.textAlignment = .left
            tempLabel.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
            tempLabel.textColor = UIColor(rgba: Constant.common_C1_color)
            return tempLabel
        }()
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            
            setUpUI()
        }
        
        func setUpUI() {
            
            self.addSubview(coinImage)
            self.addSubview(nameLabel)
            self.addSubview(coinLabel)
            
            coinImage.frame = CGRect(x: 50 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, y: 0, width: 80 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, height: 80 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE)
            
            nameLabel.frame = CGRect(x: coinImage.iright + 3, y: 20 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, width: 150 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, height: 15)
            
            coinLabel.frame = CGRect(x: coinImage.iright + 3, y: nameLabel.ibottom + 8 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, width: nameLabel.iwidth, height: nameLabel.iheight)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }

}
