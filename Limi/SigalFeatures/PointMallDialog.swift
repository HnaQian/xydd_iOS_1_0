//
//  PointMallDialog.swift
//  Limi
//
//  Created by maohs on 16/5/31.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

public protocol PointMallDialogDelegate:NSObjectProtocol {
    func ensureButtonSelected(_ type:Int)
}

class PointMallDialog: UIView {

    fileprivate var dialogView:UIView = {
        var tempView = UIView()
         tempView.frame = CGRect(x: 0, y: 0, width: 290 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, height: 190 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE)
        tempView.backgroundColor = UIColor(rgba: Constant.common_C12_color)
        tempView.layer.cornerRadius = 5.0
        tempView.layer.masksToBounds = true
        return tempView
    }()
    
    fileprivate var titleLabel:UILabel = {
        var tempLabel = UILabel()
        tempLabel.textAlignment = .center
        tempLabel.font = UIFont.systemFont(ofSize: Constant.common_F1_font)
        tempLabel.textColor = UIColor(rgba: Constant.common_C1_color)
        return tempLabel
    }()
    
    fileprivate var contentLabel:UILabel = {
        var tempLabel = UILabel()
        tempLabel.numberOfLines = 2
        tempLabel.textAlignment = .center
        tempLabel.font = UIFont.systemFont(ofSize: Constant.common_F5_font)
        tempLabel.textColor = UIColor(rgba: Constant.common_C6_color)
        return tempLabel
    }()
    
    fileprivate var cancelButton:UIButton = {
        var tempButton = UIButton(type: UIButtonType.custom)
        tempButton.setTitle("取消", for: UIControlState())
        tempButton.setTitleColor(UIColor(rgba: Constant.common_C2_color), for: UIControlState())
        tempButton.titleLabel?.font = UIFont.systemFont(ofSize: Constant.common_F1_font)
        tempButton.layer.borderColor = UIColor(rgba: Constant.common_C6_color).cgColor
        tempButton.layer.borderWidth = 1.0
        tempButton.layer.cornerRadius = 5.0
        tempButton.layer.masksToBounds = true
        return tempButton
    }()
    
    fileprivate var chargeButton:UIButton = {
        var tempButton = UIButton(type: UIButtonType.custom)
        tempButton.setTitle("立即兑换", for: UIControlState())
        tempButton.setTitleColor(UIColor(rgba: Constant.common_C12_color), for: UIControlState())
        tempButton.titleLabel?.font = UIFont.systemFont(ofSize: Constant.common_F1_font)
        tempButton.backgroundColor = UIColor(rgba: Constant.common_C1_color)
        tempButton.layer.cornerRadius = 5.0
        tempButton.layer.masksToBounds = true
        return tempButton
    }()
    
    weak var delegate:PointMallDialogDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setUpUI()
    }
    
    func setUpUI() {
        self.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        self.alpha = 0
        
        self.addSubview(dialogView)

        dialogView.addSubview(titleLabel)
        dialogView.addSubview(contentLabel)
        dialogView.addSubview(cancelButton)
        dialogView.addSubview(chargeButton)
        
        titleLabel.frame = CGRect(x: 0, y: 30 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, width: dialogView.iwidth, height: 17)
        
        contentLabel.frame = CGRect(x: 0, y: titleLabel.ibottom + 14 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, width: dialogView.iwidth - 40, height: 36)
        contentLabel.icenterX = dialogView.icenterX
        
        cancelButton.frame = CGRect(x: 18 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, y: dialogView.iheight - 70 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, width: 120 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, height: 40 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE)
        cancelButton.addTarget(self, action: #selector(PointMallDialog.cancelButtonClicked(_:)), for: .touchUpInside)

        
        chargeButton.frame = CGRect(x: 152 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, y: cancelButton.itop, width: cancelButton.iwidth, height: cancelButton.iheight)
        chargeButton.addTarget(self, action: #selector(PointMallDialog.chargeButtonClicked(_:)), for: .touchUpInside)
    }
    
    @objc fileprivate func cancelButtonClicked(_ sender:AnyObject) {
        self.dismiss()
    }
    
    @objc fileprivate func chargeButtonClicked(_ sender:AnyObject) {
         self.alpha = 0
        let tempButton = sender as! UIButton
        delegate?.ensureButtonSelected(tempButton.tag)
    }
    
    func show(_ title:String,info:String,type:Int = 0) {
        if self.superview == nil {
            if let window = UIApplication.shared.keyWindow {
                self.frame = window.bounds
                window.addSubview(self)
            }
        }

        chargeButton.tag = type
        if type == 0 {
            chargeButton.setTitle("立即兑换", for: UIControlState())
        }else {
            chargeButton.setTitle("立即充值", for: UIControlState())
        }
        
        titleLabel.text = title
        if info.characters.count > 0 {
           
            contentLabel.text = info
        }else {
            contentLabel.text = ""
        }
        
        dialogView.center = self.center
        dialogView.icenterY -= 20
        
        UIView.animate(withDuration: 0.25, animations: {
            self.dialogView.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.alpha = 1
        }, completion: { (done) in
        }) 
        
    }
    
    fileprivate func dismiss() {
        UIView.animate(withDuration: 0.25, animations: {
            self.alpha = 0
        }, completion: { (done) in
            self.dialogView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        }) 
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
