//
//  ConvertNotesViewController.swift
//  Limi
//
//  Created by maohs on 16/6/1.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//兑换记录

import UIKit

class ConvertNotesViewController: BaseViewController,LazyPageScrollViewDelegate,ConvertNotesTableViewDelegate {
    
    fileprivate var pageScrollView = LazyPageScrollView(verticalDistance: 3, tabItemType: TabItemType.customView)
    
    fileprivate var tabAry:[ConvertNotesTableView]!
    
    fileprivate var currentTab:ConvertNotesTableView{return _currentTab}
    
    fileprivate var _currentTab:ConvertNotesTableView!
    
    let titleView = ConvertNotesTitleView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.view.backgroundColor = UIColor(rgba: Constant.common_C5_color)
        self.navigationItem.title = "兑换记录"

        self.setupUI()
    }
    
    
    //心意说明 Action
    @objc fileprivate func rightBarButtonClicked(_ sender:AnyObject) {
        if let url = URL(string:currentTab.rule_url){
            EventDispatcher.dispatch(URL: url, onNavigationController: self.navigationController)
        }
    }
    
    
    //segmentSelected action
    func lazyPageScrollView(_ pageScrollView: LazyPageScrollView, changeIndex index: Int) {
        _currentTab = tabAry[index]
        
        self.currentTab.loadDataIfNull()
        titleView.refreshData(currentTab.user_point, tipes: currentTab.tips)
    }
    
    
    func convertNotesTableViewLoadFinish(_ tableView:ConvertNotesTableView){
        titleView.refreshData(currentTab.user_point, tipes: currentTab.tips)
    }
    
    
    fileprivate func setupUI(){
        
        self.view.addSubview(titleView)
        titleView.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV1.SCREEN_WIDTH, height: 165 * Constant.ScreenSizeV1.SCREEN_HEIGHT_SCALE)

        self.view.addSubview(pageScrollView)
        
        pageScrollView.frame = self.view.bounds
        pageScrollView.iy = titleView.iheight
        pageScrollView.iheight = self.view.iheight - titleView.iheight - 64
        
        let tab1 = ConvertNotesTableView(frame: CGRect(x: 0, y: pageScrollView.tabItem.iheight, width: pageScrollView.iwidth, height: pageScrollView.iheight - pageScrollView.tabItem.iheight - 30), type: 1)
        tab1.delegate = self
        
        let tab2 = ConvertNotesTableView(frame: tab1.bounds, type: 2)
        tab2.delegate = self
        
        _currentTab = tab1
        self.tabAry = [tab1,tab2]
        
        pageScrollView.segment(["收入","支出"], views: [tab1,tab2], frame: pageScrollView.frame)
        pageScrollView.delegate = self
        
        //心意币说明
        self.navigationItem.rightBarButtonItem = CommonWidgets.NavigationRightBtn.newRightBtn("心意币说明",target: self, action: #selector(ConvertNotesViewController.rightBarButtonClicked(_:)),color:Constant.common_C2_color,font:Constant.common_F4_font)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


//MARK: 积分列表
class ConvertNotesTableView:UIView,UITableViewDataSource,UITableViewDelegate{
    
    var dataSource: [ConvertNotesDataModel]{return _dataSource}
    
    weak var delegate:ConvertNotesTableViewDelegate?
    
    var tips:String{return _tips}
    var rule_url:String{return _rule_url}
    var user_point:Int{return _user_point}
    
    fileprivate var _dataSource = [ConvertNotesDataModel]()
    fileprivate let tableView:UITableView = UITableView()

    fileprivate var _tips = ""
    fileprivate var _rule_url = ""
    fileprivate var _user_point = 0
    
    fileprivate var type = 0
    
    //type: 1收入 2支出
    init(frame: CGRect,type:Int) {
        super.init(frame: frame)
        self.type = type
        
        setupUI()
    }
    
    
    //如果没有数据，请求数据
    func loadDataIfNull(){
        
        if self.dataSource.count == 0 {
            self.loadData(false)
        }
    }
    
    
    //请求数据
    fileprivate func loadData(_ loadMore:Bool){
        /*
         token	true	string	登录token值
         type	true	int	积分类型：1收入 2支出
         last_id	true	int	上一次列表的最后一条数据ID，默认为0
         */
        
        var para = [String:AnyObject]()
        
        para["type"] = type as AnyObject?
        
        if loadMore{
            if self.dataSource.count > 0{
                para["last_id"] = self.dataSource.last!.id as AnyObject?
            }
        }
        
        let _ = LMRequestManager.requestByDataAnalyses(context:self, URLString: Constant.JTAPI.point_list,parameters: para,isNeedUserTokrn: true,isShowErrorStatuMsg:true,successHandler: {data, status, msg in
            /*
             "rule_url" = "http://test.wechat.giftyou.me/";
             tips = "\U5fc3\U610f\U5e01\U5230\U671f\U65f6\U95f4\Uff1a2017-12-31";
             "user_point" = 0;
             */
//            self.tableView.closeTipe(UIScrollTipeMode.NullData)
            let _ = Delegate_Selector(self.delegate, #selector(ConvertNotesTableViewDelegate.convertNotesTableViewLoadFinish(_:))){Void in self.delegate!.convertNotesTableViewLoadFinish!(self)}
            
            self.tableView.endFooterRefresh()
            if status != 200{
//                if self.dataSource.count == 0{self.tableView.showTipe(UIScrollTipeMode.NullData)}
                return
            }
            
            if let rule_url = JSON(data as AnyObject)["data"]["rule_url"].string{
                self._rule_url = rule_url
            }
            if let tips = JSON(data as AnyObject)["data"]["tips"].string{
                self._tips = tips
            }
            if let user_point = JSON(data as AnyObject)["data"]["user_point"].int{
                self._user_point = user_point
            }
            var listCount = 0
            
            if let list = JSON(data as AnyObject)["data"]["list"].arrayObject{
                if list.count > 0{
                    if !loadMore{self._dataSource = [ConvertNotesDataModel]()}
                    for dic in list{
                        self._dataSource.append(ConvertNotesDataModel(dic as! [String:AnyObject]))
                    }
                    self.tableView.reloadData()
                }
                
                listCount = list.count
            }
            
            if listCount < 10{
                //加载全部
                self.tableView.showTipe(UIScrollTipeMode.noMuchMore)
            }
            
//            if self.dataSource.count == 0{self.tableView.showTipe(UIScrollTipeMode.NullData)}
            let _ = Delegate_Selector(self.delegate, #selector(ConvertNotesTableViewDelegate.convertNotesTableViewLoadFinish(_:))){Void in self.delegate!.convertNotesTableViewLoadFinish!(self)}
            
        }){Void in
            self.tableView.endFooterRefresh()
//            if self.dataSource.count == 0{self.tableView.showTipe(UIScrollTipeMode.NullData)}
            let _ = Delegate_Selector(self.delegate, #selector(ConvertNotesTableViewDelegate.convertNotesTableViewLoadFinish(_:))){Void in self.delegate!.convertNotesTableViewLoadFinish!(self)}
        }
    }
    
    
    
    //tableViewdelegate------------
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:ConvertNotesCell = tableView.dequeueReusableCell(withIdentifier: ConvertNotesCell.reuseIdentifier) as! ConvertNotesCell
        cell.refreshData(dataSource[(indexPath as NSIndexPath).row])
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ConvertNotesCell.height
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let _ = Delegate_Selector(self.delegate, #selector(ConvertNotesTableViewDelegate.convertNotesTableViewSelected(_:model:))){Void in self.delegate!.convertNotesTableViewSelected!(self,model:self.dataSource[(indexPath as NSIndexPath).row])}
    }
    //tableViewdelegate------------
    
    
    fileprivate func setupUI(){
        self.addSubview(tableView)
        tableView.frame = self.bounds
        tableView.delegate = self
        tableView.dataSource  = self
        
        tableView.backgroundColor = UIColor(rgba: Constant.common_background_color)
        
        tableView.addFooterRefresh { [weak self] in
            self?.loadData(true)
        }
        
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        tableView.register(ConvertNotesCell.self, forCellReuseIdentifier: ConvertNotesCell.reuseIdentifier)
        
        self.loadData(false)
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}


//MARK: 积分列表代理
@objc protocol ConvertNotesTableViewDelegate:NSObjectProtocol {
    @objc optional func convertNotesTableViewLoadFinish(_ tableView:ConvertNotesTableView)
    @objc optional func convertNotesTableViewSelected(_ tableView:ConvertNotesTableView,model:ConvertNotesDataModel)
}


//MARK: 顶部图片，总积分及提示语
class ConvertNotesTitleView: BaseView {
    
    fileprivate let bgImage:UIImageView = {
        let img = UIImageView(frame:CGRect(x: 0, y: 0, width: Constant.ScreenSizeV1.SCREEN_WIDTH, height: 160 * Constant.ScreenSizeV1.SCREEN_HEIGHT_SCALE))
        img.image = UIImage(named: "pointmallbg")
        
        return img
    }()
    
    
    fileprivate let coinImage:UIImageView = {
        let img = UIImageView(frame:CGRect(x: 100 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, y: 25 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, width: 90 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE,height: 90 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE))
        img.image = UIImage(named: "usablecoin")
        return img
    }()
    
    fileprivate let countLabel:UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: 36)
        lbl.textColor = UIColor(rgba: "#ffe500")
        return lbl
    }()
    
    fileprivate let promptLabel:UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        lbl.textColor = UIColor(rgba: Constant.common_C12_color)
        lbl.text = "可用心意币"
        return lbl
    }()
    
    fileprivate let tipesLabel:UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: Constant.common_F5_font)
        lbl.textColor = UIColor(rgba: Constant.common_C12_color)
        lbl.textAlignment = .center
        return lbl
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setUpUI()
    }
    
    func setUpUI() {
        
        self.backgroundColor = UIColor.white
        
        self.addSubview(bgImage)
        bgImage.addSubview(coinImage)
        bgImage.addSubview(countLabel)
        bgImage.addSubview(promptLabel)
        bgImage.addSubview(tipesLabel)
        
        coinImage.icenterY = bgImage.icenterY
        
        countLabel.frame = CGRect(x: coinImage.iright + MARGIN_20, y: 48 * Constant.ScreenSizeV1.SCREEN_HEIGHT_SCALE, width: Constant.ScreenSizeV1.SCREEN_WIDTH - coinImage.iright - MARGIN_20, height: 36)
        
        promptLabel.frame = CGRect(x: coinImage.iright + MARGIN_8, y: countLabel.ibottom + MARGIN_8, width: countLabel.iwidth, height: 15)
        
        tipesLabel.frame = CGRect(x: 0, y: bgImage.ibottom - MARGIN_20, width: bgImage.iwidth, height: 15)
    }
    
    
    func refreshData(_ count:Int,tipes:String) {
        countLabel.text = "\(count)"
        tipesLabel.text = tipes
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}




class TestView:UIView{

    var name:String?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }


}
