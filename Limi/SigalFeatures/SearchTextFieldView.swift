//
//  SearchTextFieldView.swift
//  Limi
//
//  Created by meimao on 15/11/24.
//  Copyright © 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class SearchTextFieldView: UIView {
    
    let textField:UITextField = {
        let textfield = UITextField()
        textfield.returnKeyType = UIReturnKeyType.search
        textfield.clearButtonMode = UITextFieldViewMode.always
        textfield.font = UIFont.systemFont(ofSize: 15)
        textfield.tintColor = UIColor.gray
        textfield.placeholder = "想要送什么"
        
        return textfield
    }()
    
    let textLabel:UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .center
        lbl.font = UIFont.systemFont(ofSize: 15)
        lbl.textColor = Constant.Theme.Color14
        lbl.layer.cornerRadius = 10.0
        lbl.layer.masksToBounds = true
        lbl.backgroundColor = UIColor.white
        lbl.lineBreakMode = NSLineBreakMode.byTruncatingHead
        lbl.isHidden = true
        
        return lbl
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.clipsToBounds = true
        
        self.textField.frame = self.frame
        textLabel.frame = CGRect(x: 0, y: 0, width: self.textField.frame.size.width, height: 28)
        textLabel.icenterY = textField.iheight / 2
        
        self.addSubview(textField)
        textField.addSubview(textLabel)
    }
    
    func changeTitleStyle(_ title:String) {
        textLabel.isHidden = false
        textLabel.text = title
        textField.text = title
        textField.textColor = UIColor.clear
        let option = NSStringDrawingOptions.usesLineFragmentOrigin
        let attributes = NSDictionary(object:textLabel.font, forKey: NSFontAttributeName as NSCopying)
        let size = CGSize(width: Constant.ScreenSize.SCREEN_WIDTH - 150 ,height: 28)
        let stringRect = title.boundingRect(with: size, options: option, attributes: attributes as? [String : AnyObject], context: nil)
        textLabel.iwidth = stringRect.size.width + MARGIN_10
        textLabel.ileft = MARGIN_4
        if title.characters.count == 0 {
            textLabel.iwidth = 0
        }
        textField.placeholder = ""
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
