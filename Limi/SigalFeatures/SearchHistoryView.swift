//
//  SearchHistoryView.swift
//  Limi
//
//  Created by meimao on 15/11/24.
//  Copyright © 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class SearchHistoryView: BaseView {
    
    var saveHandler: ((_ title: String) -> Void)?
    
    // 搜索热词的按钮
    var children = [UIButton]()
    
    // 搜索热词的数据
    var searchHotDataSource = [JSON]() {
        didSet {
            self.reloadData()
        }
    }
    
    // 存储搜索历史记录
    var seaArray = [String]()
    var historyButtonArray = [UIButton]()
    
    // 页面UI控件
    fileprivate let contentView:UIScrollView = {
       let view = UIScrollView()
        view.backgroundColor = UIColor.white
//        view.showsHorizontalScrollIndicator = false
//        view.userInteractionEnabled = true
        
        return view
    }()
    
    fileprivate let hotContentView:UIView = {
        let view = UIView()
        view.clipsToBounds = true
        return view
    }()
    
    fileprivate let hotWordImageView:UIImageView = {
       let imageView = UIImageView()
        imageView.image = UIImage(named: "hotsearch")
        return imageView
    }()
    
    fileprivate let hotSearchLabel:UILabel = {
       let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: 14)
        lbl.text = "大家都在送"
        lbl.tintColor = UIColor(rgba: Constant.common_C7_color)
    
        return lbl
    }()
    
    fileprivate let historyView:UIView = {
        let view = UIView()
        view.clipsToBounds = true
        return view
    }()
    
    fileprivate let searchHistoryImageView:UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "searchhistory")
        return imageView
    }()
    
    fileprivate let searchHistoryLabel:UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: 14)
        lbl.text = "最近搜索"
        lbl.tintColor = UIColor(rgba: Constant.common_C7_color)
        
        return lbl
    }()
    
    fileprivate let cleanImgView:UIImageView = {
       let imageView = UIImageView()
        imageView.image = UIImage(named: "search_clean")
        imageView.isUserInteractionEnabled = true
        
        return imageView
    }()
    
    fileprivate let cleanButton:UIButton = {
       let btn = UIButton(type: UIButtonType.custom)
        return btn
    }()
    
    var buttons = [UIButton]() {
        didSet {
            for subView in hotContentView.subviews {
                subView.removeFromSuperview()
            }
            hotContentView.addSubview(hotWordImageView)
            hotContentView.addSubview(hotSearchLabel)
            
            if buttons.count > 0 {
                hotWordImageView.snp_makeConstraints(closure: { (make) in
                    let _ = make.top.equalTo(hotContentView).offset(Constant.ScreenSizeV2.MARGIN_30)
                    let _ = make.left.equalTo(hotContentView).offset(Constant.ScreenSizeV2.MARGIN_30)
                    let _ = make.width.equalTo(Constant.ScreenSizeV2.MARGIN_30)
                    let _ = make.height.equalTo(Constant.ScreenSizeV2.MARGIN_30)
                })
                
                hotSearchLabel.snp_makeConstraints(closure: { (make) -> Void in
                    let _ = make.centerY.equalTo(hotWordImageView)
                    let _ = make.left.equalTo(hotWordImageView.snp_right).offset(Constant.ScreenSizeV2.MARGIN_10)
                })
            }
            
            var lastView: UIButton!
            var lastViewLocation:CGFloat = 0
            var btnWidth: CGFloat = 0
            var row: Int = 1
            for button in buttons {
                hotContentView.addSubview(button)
                btnWidth = widthForButton(string: button.titleLabel!.text!) + 2 * Constant.ScreenSizeV2.MARGIN_30 + Constant.ScreenSizeV2.MARGIN_4
                if lastView == nil {
                    //第一个button
                    lastViewLocation = Constant.ScreenSizeV2.MARGIN_30 + btnWidth
                    button.snp_makeConstraints(closure: { (make) -> Void in
                        
                        let _ = make.height.equalTo(28)
                        let _ = make.left.equalTo(hotContentView).offset(Constant.ScreenSizeV2.MARGIN_30)
                        let _ = make.top.equalTo(hotWordImageView.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_30)
                        let _ = make.width.equalTo(btnWidth)
                    })
                }else {
                    lastViewLocation += Constant.ScreenSizeV2.MARGIN_20 + btnWidth
                    if lastViewLocation <= Constant.ScreenSize.SCREEN_WIDTH - 2 * Constant.ScreenSizeV2.MARGIN_30 {
                        //不换行
                        button.snp_makeConstraints(closure: { (make) -> Void in
                            
                            let _ = make.height.equalTo(28)
                            let _ = make.left.equalTo(lastView.snp_right).offset(Constant.ScreenSizeV2.MARGIN_20)
                            let _ = make.top.equalTo(lastView)
                            let _ = make.width.equalTo(btnWidth)
                        })
                    }else {
                        //换行
                        button.snp_makeConstraints(closure: { (make) -> Void in
                            
                            let _ = make.height.equalTo(28)
                            let _ = make.left.equalTo(hotContentView).offset(Constant.ScreenSizeV2.MARGIN_30)
                            let _ = make.top.equalTo(lastView.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_20)
                            let _ = make.width.equalTo(btnWidth)
                        })
                        
                        row += 1
                        
                        lastViewLocation = Constant.ScreenSizeV2.MARGIN_30 + btnWidth
                    }
                }
                
                lastView = button
            }
            
        }
        
    }
    
    func widthForButton(string: String) -> CGFloat {
        let option = NSStringDrawingOptions.usesLineFragmentOrigin
        let attributes = NSDictionary(object: UIFont.systemFont(ofSize: 14), forKey: NSFontAttributeName as NSCopying)
        let size = CGSize(width: self.frame.size.width - 4 * Constant.ScreenSizeV2.MARGIN_30, height: 28)
        let stringRect = string.boundingRect(with: size, options: option, attributes: attributes as? [String : AnyObject], context: nil)
        var width = stringRect.size.width
        if width >= Constant.ScreenSizeV2.SCREEN_WIDTH - 4 * Constant.ScreenSizeV2.MARGIN_30 {
            width = Constant.ScreenSizeV2.SCREEN_WIDTH - 4 * Constant.ScreenSizeV2.MARGIN_30
        }
        return width
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(contentView)
        
        contentView.addSubview(hotContentView)
        contentView.addSubview(historyView)
        
//        contentView.snp_makeConstraints { (make) -> Void in
//            let _ = make.top.equalTo(self)
//            let _ = make.bottom.equalTo(self)
//            let _ = make.left.equalTo(self)
//            let _ = make.right.equalTo(self)
//        }
        contentView.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: Constant.ScreenSizeV2.SCREEN_HEIGHT - 64)
        hotContentView.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: 84 + Constant.ScreenSizeV2.MARGIN_30 * 5)
//        hotContentView.snp_makeConstraints { (make) -> Void in
//            let _ = make.top.equalTo(contentView)
//            let _ = make.left.equalTo(contentView)
//            let _ = make.right.equalTo(contentView)
//            let _ = make.height.equalTo(84 + Constant.ScreenSizeV2.MARGIN_30 * 5)
//        }
        historyView.frame = CGRect(x: 0, y: hotContentView.ibottom, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: Constant.ScreenSizeV2.MARGIN_30 * 4 + Constant.ScreenSizeV2.MARGIN_20 + 84)
//        historyView.snp_makeConstraints { (make) -> Void in
//            let _ = make.top.equalTo(hotContentView.snp_bottom)
//            let _ = make.left.equalTo(contentView)
//            let _ = make.right.equalTo(contentView)
//            let _ = make.height.equalTo(84 + Constant.ScreenSizeV2.MARGIN_30 * 5)
//        }
        // 获取热门搜索 数据
        self.loadData()
        
        // 获取搜索历史 数据
        self.storageSearchString(str: nil)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //
    func reloadData()
    {
        
        for item in self.searchHotDataSource
        {
            let button = customButton()
            button.addTarget(self, action: #selector(SearchHistoryView.buttonAction(_:)), for: .touchUpInside)
            
            if let str = item["Name"].string
            {
                button.setTitle(str, for: UIControlState())
            }
            
            self.children.append(button)
        }
        
        buttons = self.children
    }
    
    func customButton() -> UIButton {
        let button = UIButton(type: UIButtonType.custom)
        button.clipsToBounds = true
        
        button.contentEdgeInsets.left = 30 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        button.contentEdgeInsets.right = 30 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        button.setTitleColor(UIColor(rgba:Constant.common_C6_color), for: UIControlState())
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        button.layer.borderWidth = 0.5
        button.layer.borderColor = UIColor(rgba: Constant.common_C8_color).cgColor
        button.layer.cornerRadius = 30 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        return button
    }
    
    // 清除搜索历史
    func cleanHistory(_ sender: UIButton) {
        let arr = [String]()
        UserDefaults.standard.set(arr, forKey: "searchHistory")
        UserDefaults.standard.synchronize()
        historyView.isHidden = true
        seaArray = [String]()
        historyButtonArray = [UIButton]()
        updateHistoryViewUI()
    }
    
    // 点击热门搜索 button
    func buttonAction(_ sender: UIButton) {
        
        // 保存搜索数据
        let string = sender.titleLabel?.text
        self.saveHandler?(string!)
        self.storageSearchString(str: string)
    }
    
    // 保存搜索内容
    func storageSearchString(str: String?) {
        
        historyView.isHidden = true
        // 判断是否存储了历史记录的数组
        if let object: AnyObject = UserDefaults.standard.object(forKey: "searchHistory") as AnyObject? {
            if object is [String] {
                var arr = object as! [String]
                historyView.isHidden = arr.count == 0 ? true : false
                // 判断输入字符串是否为空
                if str != nil {
                    
                    // 判断数组内是否含有与输入str一样的元素,若含有取出下标
                    for item in arr.enumerated() {
                        if item.1 == str {
                            arr.remove(at: item.0)
                        }
                    }
                    // 判断数组内个数（数组内元素个数在10个以内）
//                    if arr.count == 10 {
//                        arr.remove(at: 0)
//                    }
                    arr.append(str!)
                    seaArray = arr.reversed()
                    updateHistoryViewUI()
                    UserDefaults.standard.set(arr, forKey: "searchHistory")
                    UserDefaults.standard.synchronize()
                }else{
                    // 反转数组
                    seaArray = arr.reversed()
                    updateHistoryViewUI()
                }
                
            }
            else{
                UserDefaults.standard.set(seaArray, forKey: "searchHistory")
                UserDefaults.standard.synchronize()
            }
        }
        else{
            //建立
            UserDefaults.standard.set(seaArray, forKey: "searchHistory")
            UserDefaults.standard.synchronize()
        }
    }
    
    // 请求搜索热词数据
    func loadData(){
        
        let _ = LMRequestManager.requestByDataAnalyses(HTTPMethod.get,context:self,URLString:  Constant.JTAPI.searchHot_word, isShowErrorStatuMsg:true,successHandler: {data,status,msg in
            
            if status == 200
            {
                if JSON(data as AnyObject)["data"]["hot"].array == nil {
                    self.hotContentView.frame.size.height = 0
                }
                if let string: String = JSON(data as AnyObject)["data"]["input"]["Name"].string
                {
                    let userInfo: Dictionary<String,String>! = [
                        "placeName": string,
                    ]
                    NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.search_get_placeName), object: nil, userInfo: userInfo)
                }
                
                if let list = JSON(data as AnyObject)["data"]["hot"].array {
                    self.searchHotDataSource = list
                }
            }
        })
    }
    
}

extension SearchHistoryView {
    func updateHistoryViewUI() {
        for subView in historyView.subviews {
            subView.removeFromSuperview()
        }
        if seaArray.count > 0 {
             addHistoryButton()
           
            for view in [searchHistoryImageView,searchHistoryLabel,cleanButton]{
                historyView.addSubview(view)
            }
            cleanButton.addSubview(cleanImgView)
            cleanButton.addTarget(self, action: #selector(SearchHistoryView.cleanHistory(_:)), for: .touchUpInside)
            searchHistoryImageView.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(historyView).offset(Constant.ScreenSizeV2.MARGIN_30)
                let _ = make.top.equalTo(historyView).offset(Constant.ScreenSizeV2.MARGIN_20)
                let _ = make.width.equalTo(Constant.ScreenSizeV2.MARGIN_30)
                let _ = make.height.equalTo(Constant.ScreenSizeV2.MARGIN_30)
            }
            
            searchHistoryLabel.snp_makeConstraints { (make) in
                let _ = make.left.equalTo(searchHistoryImageView.snp_right).offset(Constant.ScreenSizeV2.MARGIN_10)
                let _ = make.centerY.equalTo(searchHistoryImageView)
                let _ = make.height.equalTo(15)
            }
            
            cleanButton.snp_makeConstraints { (make) -> Void in
                let _ = make.centerY.equalTo(searchHistoryLabel)
                let _ = make.right.equalTo(self)
                let _ = make.width.equalTo(100 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
                let _ = make.height.equalTo(40)
            }
            
            cleanImgView.snp_makeConstraints { (make) -> Void in
                let _ = make.centerY.equalTo(cleanButton.iheight / 2)
                let _ = make.right.equalTo(cleanButton.snp_right).offset(-30 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
                let _ = make.width.equalTo(34 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
                let _ = make.height.equalTo(34 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
            }
            
            var lastView: UIButton!
            var lastViewLocation:CGFloat = 0
            var btnWidth: CGFloat = 0
            var row: Int = 1
            for button in self.historyButtonArray {
                historyView.addSubview(button)
                btnWidth = widthForButton(string: button.titleLabel!.text!) + 2 * Constant.ScreenSizeV2.MARGIN_30 + Constant.ScreenSizeV2.MARGIN_4
                if lastView == nil {
                    //第一个button
                    lastViewLocation = Constant.ScreenSizeV2.MARGIN_30 + btnWidth
                    button.snp_makeConstraints(closure: { (make) -> Void in
                        
                        let _ = make.height.equalTo(28)
                        let _ = make.left.equalTo(historyView).offset(Constant.ScreenSizeV2.MARGIN_30)
                        let _ = make.top.equalTo(searchHistoryImageView.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_30)
                        let _ = make.width.equalTo(btnWidth)
                    })
                }else {
                    lastViewLocation += Constant.ScreenSizeV2.MARGIN_20 + btnWidth
                    if lastViewLocation <= Constant.ScreenSize.SCREEN_WIDTH - 2 * Constant.ScreenSizeV2.MARGIN_30 {
                        //不换行
                        button.snp_makeConstraints(closure: { (make) -> Void in
                            
                            let _ = make.height.equalTo(28)
                            let _ = make.left.equalTo(lastView.snp_right).offset(Constant.ScreenSizeV2.MARGIN_20)
                            let _ = make.top.equalTo(lastView)
                            let _ = make.width.equalTo(btnWidth)
                        })
                    }else {
                        //换行
                        button.snp_makeConstraints(closure: { (make) -> Void in
                            
                            let _ = make.height.equalTo(28)
                            let _ = make.left.equalTo(historyView).offset(Constant.ScreenSizeV2.MARGIN_30)
                            let _ = make.top.equalTo(lastView.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_20)
                            let _ = make.width.equalTo(btnWidth)
                        })
                        
                        row += 1
                        
                        lastViewLocation = Constant.ScreenSizeV2.MARGIN_30 + btnWidth
                    }
                }
                
                lastView = button
                
                if lastView == buttons.last
                {
//                    historyView.snp_makeConstraints(closure: { (make) -> Void in
//                        let _ = make.bottom.equalTo(lastView!).offset(Constant.ScreenSizeV2.MARGIN_20)
//                    })
                }
            }
            
        }
    }
    
    func addHistoryButton() {
        self.historyButtonArray = [UIButton]()
        for item in self.seaArray
        {
            let button = customButton()
            button.addTarget(self, action: #selector(SearchHistoryView.historyButtonClicked(_:)), for: .touchUpInside)
            
            button.setTitle(item, for: UIControlState())
            
            self.historyButtonArray.append(button)
        }
        
    }
    
    func historyButtonClicked(_ sender:UIButton) {
        self.saveHandler?((sender.titleLabel?.text!)!)
        self.storageSearchString(str: sender.titleLabel?.text!)
    }
}

