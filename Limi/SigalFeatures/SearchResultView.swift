//
//  SearchResultView.swift
//  Limi
//
//  Created by meimao on 15/11/24.
//  Copyright © 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

protocol SearchResultViewDelegate:NSObjectProtocol {
    func searchResultViewclicked(_ value:String,type:Int)
}

class SearchResultView: BaseView,TacticDetailDelegate,CommonScrollViewDelegate,SearchTermsViewDelegate {
    
    weak var delegate:SearchResultViewDelegate?
    var tableViewPushHandler: ((_ viewController: UIViewController) -> Void)?
    var nav : UINavigationController = UINavigationController()
    //一次请求的数量
    let goodsSize = 10
    let articleSize = 10
    
    var goodsPage = 1 {
        didSet {
            if goodsPage == 1 {
                self.scrollToTopButton.isHidden = true
            }
        }
    }
    var articlePage = 1 {
        didSet {
            if articlePage == 1 {
                self.scrollToTopButton.isHidden = true
            }
        }
    }
    
    //商品或文章数量
    var gnum: Int = 0
    var anum: Int = 0
    
    // 接收请求返回数据
    var goodsList = [JSON]()
    var articleList = [JSON]()
    
    // tableView的表头  CollectionView的表头
    var emptTab = EmptyView()
    var emptCol = EmptyView2()
    
    var searchStr: String = "" {
        didSet {
            let emptyStr: String = "抱歉，没有找到 “\(searchStr)” 的搜索结果"
            if self.gnum == 0
            {
                emptCol.emptyLabel.text = emptyStr
                goodsColleView.headerView = emptCol
            }
            if self.anum == 0
            {
                emptTab.emptyLabel.text = emptyStr
                articleTabView.tableView.tableHeaderView = emptTab
            }
        }
    }
    
    //状态栏
    fileprivate let statueBarBegView:UIView = {
        let view = UIView(frame: CGRect(x: 0,y: 0,width: Constant.ScreenSizeV2.SCREEN_WIDTH,height: 0))
        view.alpha = 0
        return view
    }()
    
    //返回顶部按钮
     let scrollToTopButton:UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "goods_detail_float_button"), for: UIControlState())
        button.isHidden = true
        return button
    }()
    
    
    let goodsTypeView = SearchTermsView(frame: CGRect.zero, type: 1)
    let articleTypeView = SearchTermsView(frame: CGRect.zero, type: 2)
    
    var goodsColleView: CommonCollectionView!
    var articleTabView: CommonTableView!
    
    fileprivate var currentPage = 0
    fileprivate var historyY:CGFloat = 0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.isUserInteractionEnabled = true
        
        self.backgroundColor = UIColor(rgba: Constant.common_background_color)
        
        // 初始化 商品列表
        goodsColleView = CommonCollectionView.newCommonCollectionView(backView: nil, type: GoodsFavType.fav, stowBool: false, headerView: self.emptCol)
        
        // 初始化 攻略列表
        articleTabView = CommonTableView.newCommonTableView(type: ArticleFavType.fav)
        
        self.addSubview(statueBarBegView)
        
        self.addSubview(goodsTypeView)
        self.addSubview(articleTypeView)
        goodsTypeView.delegate = self
        articleTypeView.delegate = self
        goodsTypeView.itop = statueBarBegView.ibottom
        articleTypeView.itop = statueBarBegView.ibottom
        
        self.addSubview(goodsColleView)
        self.addSubview(articleTabView)
        goodsColleView.frame = CGRect(x: 0, y: goodsTypeView.ibottom + 1, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: Constant.ScreenSizeV2.SCREEN_HEIGHT - goodsTypeView.ibottom - 1 - 20)
        
        articleTabView.frame = CGRect(x: 0, y: articleTypeView.ibottom + 1, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: Constant.ScreenSizeV2.SCREEN_HEIGHT - articleTypeView.ibottom - 1 - 20)
        
        goodsColleView.commonDelegate = self
        articleTabView.commonDelegate = self
        
        goodsColleView.collectionView.bounces = false
        articleTabView.tableView.bounces = false
        
        scrollToTopButton.addTarget(self, action: #selector(SearchResultView.scrollToTop), for: .touchUpInside)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // collectionView和tableView滑动
    func commonScrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView.offsetY > Constant.ScreenSizeV2.SCREEN_HEIGHT - MARGIN_64 {
            if self.scrollToTopButton.isHidden {
                self.scrollToTopButton.isHidden = false
            }
        }else {
            if !self.scrollToTopButton.isHidden {
                self.scrollToTopButton.isHidden = true
            }
        }
        
        if scrollView.offsetY < historyY || scrollView.offsetY < 50 {
            //下滑 如果导航栏隐藏了，显示出来
            if nav.navigationBar.isHidden {
                nav.setNavigationBarHidden(false, animated: true)
                statueBarBegView.iheight = 0
                goodsTypeView.itop = statueBarBegView.ibottom
                articleTypeView.itop = statueBarBegView.ibottom
                goodsColleView.itop = goodsTypeView.ibottom + 1
                articleTabView.itop = articleTypeView.ibottom + 1
                UIView.animate(withDuration: 0.25, animations: {
                    
                })
            }
            
        }else{
            //上滑 如果导航栏显示了，隐藏
            if !nav.navigationBar.isHidden {
                nav.setNavigationBarHidden(true, animated: true)
                statueBarBegView.iheight = 20
                goodsTypeView.itop = statueBarBegView.ibottom
                articleTypeView.itop = statueBarBegView.ibottom
                goodsColleView.itop = goodsTypeView.ibottom + 1
                articleTabView.itop = articleTypeView.ibottom + 1
                UIView.animate(withDuration: 0.25, animations: {
                    
                })
            }
        }
        historyY = scrollView.offsetY
    }
    
    func commonScrollViewDidEndDragging(_ scrollView: UIScrollView) {
        
    }
    
    
    func commonScrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
    }
    
    /**返回顶部动作*/
    @objc fileprivate func scrollToTop(){
        self.goodsColleView.collectionView.scrollRectToVisible(CGRect(x: 0, y: 0, width: 1, height: 1), animated: true)
        self.articleTabView.tableView.scrollRectToVisible(CGRect(x: 0, y: 0, width: 1, height: 1), animated: true)
    }
    
}

// tableView的表头
class EmptyView: BaseView {
    var imageView = UIImageView(image: UIImage(named: "search_biaoQing"))
    var emptyLabel:UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .center
        lbl.font = UIFont.systemFont(ofSize: Constant.common_F5_font)
        lbl.textColor = UIColor(rgba: Constant.common_C6_color)
        
        return lbl
    }()
    var backView = UIView()
    var downView = UIView()
    var downLable = UILabel()
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        downLable.font = UIFont.systemFont(ofSize: 13)
        downLable.text = ""
        downLable.textColor = UIColor.gray
        
        self.addSubview(backView)
        self.addSubview(downView)
        backView.addSubview(emptyLabel)
        backView.addSubview(imageView)
        downView.addSubview(downLable)
        
        backView.backgroundColor = UIColor.white
        downView.backgroundColor = UIColor(rgba: Constant.common_background_color)
        backView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self)
            let _ = make.left.equalTo(self)
            let _ = make.right.equalTo(self)
            let _ = make.height.equalTo(320 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
        }
        
        imageView.snp_makeConstraints { (make) -> Void in
            let _ = make.center.equalTo(backView)
            let _ = make.width.equalTo(120 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
            let _ = make.height.equalTo(120 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
        }
        
        emptyLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(imageView.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_10)
            let _ = make.centerX.equalTo(backView)
            let _ = make.width.equalTo(self).offset(-MARGIN_20)
        }
        
        downView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(backView.snp_bottom)
            let _ = make.left.equalTo(self)
            let _ = make.right.equalTo(self)
            let _ = make.height.equalTo(80 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
        }
        downLable.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(downView).offset(20)
            let _ = make.left.equalTo(downView).offset(8)
        }
        
        self.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: 400 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
    }
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// collection的表头
class EmptyView2: UICollectionReusableView {
    static let viewHeight:CGFloat = 400 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
    var imageView = UIImageView(image: UIImage(named: "search_biaoQing"))
    var emptyLabel:UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .center
        lbl.font = UIFont.systemFont(ofSize: Constant.common_F5_font)
        lbl.textColor = UIColor(rgba: Constant.common_C6_color)
        
        return lbl
    }()
    var backView = UIView()
    var downView = UIView()
    var downLable = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        downLable.font = UIFont.systemFont(ofSize: 13)
        downLable.text = ""
        downLable.textColor = UIColor.gray
        
        self.addSubview(backView)
        self.addSubview(downView)
        backView.addSubview(emptyLabel)
        backView.addSubview(imageView)
        downView.addSubview(downLable)
        
        backView.backgroundColor = UIColor.white
        downView.backgroundColor = UIColor(rgba: Constant.common_background_color)
        backView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self)
            let _ = make.left.equalTo(self)
            let _ = make.right.equalTo(self)
            let _ = make.height.equalTo(320 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
        }
        
        imageView.snp_makeConstraints { (make) -> Void in
            let _ = make.center.equalTo(backView)
            let _ = make.width.equalTo(120 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
            let _ = make.height.equalTo(120 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
        }
        
        emptyLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(imageView.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_10)
            let _ = make.centerX.equalTo(backView)
            let _ = make.width.equalTo(self).offset(-MARGIN_20)
        }
        
        downView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(backView.snp_bottom)
            let _ = make.left.equalTo(self)
            let _ = make.right.equalTo(self)
            let _ = make.height.equalTo(80 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
        }
        
        downLable.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(downView).offset(20)
            let _ = make.left.equalTo(downView).offset(8)
        }
        
        
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension SearchResultView {
    func searchTermsViewClicked(_ title:String,type:Int) {
        if type == 1 {
            if title != "筛选" {
                
            }
            delegate?.searchResultViewclicked(title,type: type)
        }else {
            delegate?.searchResultViewclicked(title,type: type)
        }
    }
}
