//
//  DailySignViewController.swift
//  Limi
//
//  Created by maohs on 16/5/25.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//积分签到
import UIKit

class DailySignViewController: BaseViewController,UICollectionViewDataSource,UICollectionViewDelegate,DailySignCollectionViewFooterDelegate,DailySignCollectionViewHeaderDelegate,DailySignDialogDelegate {
    
    lazy var flowLayout:UICollectionViewFlowLayout = {
        var tempFlowLayout = UICollectionViewFlowLayout()
        tempFlowLayout.sectionInset = UIEdgeInsetsMake(MARGIN_10, MARGIN_10, MARGIN_10, MARGIN_10)
        tempFlowLayout.headerReferenceSize = CGSize(width: Constant.ScreenSizeV1.SCREEN_WIDTH,height: 540 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE)
        
        tempFlowLayout.footerReferenceSize = CGSize(width: Constant.ScreenSizeV1.SCREEN_WIDTH,height: 100 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE)
        
        return tempFlowLayout
    }()
    
    lazy var collectionView: UICollectionView = {
        let frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV1.SCREEN_WIDTH, height: self.view.iheight - 64 * Constant.ScreenSizeV1.SCREEN_HEIGHT_SCALE )
        var tempCollectionView = UICollectionView(frame: frame, collectionViewLayout: self.flowLayout)
        tempCollectionView.delegate = self
        tempCollectionView.dataSource = self
        tempCollectionView.bounces = true
        tempCollectionView.alwaysBounceVertical = true
        
        tempCollectionView.register(DailySignCollectionViewCell.self, forCellWithReuseIdentifier: "RootCell")
        //注册header
        tempCollectionView.register(DailySignCollectionViewHeader.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "RootHeader")
        
        //注册footer
        tempCollectionView.register(DailySignCollectionViewFooter.self, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "RootFooter")
        tempCollectionView.backgroundColor = UIColor.clear
        return tempCollectionView
    }()
    
    fileprivate var dailySignData:DailySignModel?
    
    fileprivate var listArray = [DetailListMode]()
    
    fileprivate var signInfo = [String:AnyObject]()
    
    fileprivate var dialogView = DailySignDialog()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.fetchCalendarData()
        self.fetchCollectionViewData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.view.backgroundColor = UIColor(rgba: "#faf2dc")
        self.navigationItem.title = "每日签到"
        self.addRightItem()
        
        self.view.addSubview(self.collectionView)
        self.collectionView.isHidden = true
        
        self.dialogView.delegate = self

    }
    
    fileprivate func fetchCalendarData() {
        let _ = LMRequestManager.requestByDataAnalyses(context: self.view, URLString:Constant.JTAPI.daily_sign_list, isNeedUserTokrn: true,isShowErrorStatuMsg:true, successHandler: { (data, status, msg) in
            if status != 200 {return}
            self.loadFinish(0)
            if let tempData : JSON = JSON(data["data"]! as AnyObject) as JSON? {
                self.dailySignData = DailySignModel(tempData.dictionaryObject!)
            }
            
        })
    }
    
    
    fileprivate func fetchCollectionViewData() {
        
        let _ = LMRequestManager.requestByDataAnalyses(context: self.view, URLString:Constant.JTAPI.point_goodslist, isNeedUserTokrn: true,isShowErrorStatuMsg:true,isNeedHud:true, successHandler: { (data, status, msg) in

            self.collectionView.closeTipe()
            if status != 200 {return}
            
            if let tempArray = JSON(data as AnyObject)["data"]["list"].arrayObject {
                self.listArray = [DetailListMode]()
                for index in 0 ..< tempArray.count {
                    let detailList = DetailListMode(tempArray[index] as! [String:AnyObject])
                    self.listArray.append(detailList)
                }
            }
            
            self.loadFinish(1)
            
        })
    }
    
    
    func loadFinish(_ type:Int){
        
        struct Count{
            static var type1 = false
            static var type2 = false
        }
        
        if type == 0{
            Count.type1 = true
        }else if type == 1 {
            Count.type2 = true
        }
        
        //两个请求完毕
        if Count.type1 && Count.type2{
            Count.type1 = false
            Count.type2 = false
            self.collectionView.reloadData()
            self.collectionView.isHidden = false
        }
    }
    
    
    //MARK: - collectionView dataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.listArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RootCell", for: indexPath) as! DailySignCollectionViewCell
        cell.refreshData(self.listArray[(indexPath as NSIndexPath).row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize{
        //172  240
        let width:CGFloat = (Constant.ScreenSize.SCREEN_WIDTH - MARGIN_10 * 3)/2.0
        let height:CGFloat = 234 * Constant.ScreenSizeV1.SCREEN_HEIGHT_SCALE
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        precondition(UICollectionElementKindSectionHeader == kind || UICollectionElementKindSectionFooter == kind, "Unexpected element kind")
        
        if UICollectionElementKindSectionHeader == kind {
            let headerView:DailySignCollectionViewHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "RootHeader", for: indexPath) as! DailySignCollectionViewHeader
            if self.dailySignData != nil {
                headerView.headerView.dailySign = self.dailySignData
                if (self.dailySignData!.date_list![0].week + self.dailySignData!.date_list!.count) > 35 {
                    self.flowLayout.headerReferenceSize = CGSize(width: Constant.ScreenSizeV1.SCREEN_WIDTH,height: 590 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE)
                    headerView.resetUI()
                }
            }
            headerView.delegate = self
            return headerView

        }
        
        let footerView : DailySignCollectionViewFooter  = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "RootFooter", for: indexPath) as! DailySignCollectionViewFooter
        footerView.delegate = self
        return footerView
//        switch kind {
//        case UICollectionElementKindSectionHeader:
//            let headerView:DailySignCollectionViewHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "RootHeader", for: indexPath) as! DailySignCollectionViewHeader
//            if self.dailySignData != nil {
//                headerView.headerView.dailySign = self.dailySignData
//                if (self.dailySignData!.date_list![0].week + self.dailySignData!.date_list!.count) > 35 {
//                    self.flowLayout.headerReferenceSize = CGSize(width: Constant.ScreenSizeV1.SCREEN_WIDTH,height: 590 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE)
//                    headerView.resetUI()
//                }
//            }
//            headerView.delegate = self
//            return headerView
//            
//        case UICollectionElementKindSectionFooter:
//            let footerView : DailySignCollectionViewFooter  = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "RootFooter", for: indexPath) as! DailySignCollectionViewFooter
//            footerView.delegate = self
//            return footerView
//            
//        default:
//            assert(false, "Unexpected element kind")
//        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        dailySignCollectionViewFooterClicked()
    }
    
    func dailySignCollectionViewFooterClicked() {
        let pointMallVC = PointMallViewController()
        pointMallVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(pointMallVC, animated: true)
    }
    
    func convertButtonSelected() {
        dailySignCollectionViewFooterClicked()
    }
    
    func dailySignCollectionViewHeaderClicked() {
        
        if UserInfoManager.didLogin == true
        {
            let _ = LMRequestManager.requestByDataAnalyses(context: self.view, URLString:Constant.JTAPI.daily_sign_add, isNeedUserTokrn: true,isShowErrorStatuMsg:true, successHandler: { (data, status, msg) in
                if status != 200 {return}
                if let tempData:JSON = JSON(data["data"]! as AnyObject) as JSON?{
                    self.signInfo = tempData.dictionaryObject!
                    if self.view.window != nil {
                        self.dialogView.show(self.signInfo)
                    }
                    
                    self.loadFinish(1)
                    self.fetchCalendarData()
                }
            })
            
        }else {
            self.loginWithType()
        }
        
    }
    
    
    func loginWithType(){
        self.jumpToLogin(Constant.InLoginType.dailySignType, tipe: nil)
    }
    
    fileprivate func addRightItem() {
        
        self.navigationItem.rightBarButtonItem = CommonWidgets.NavigationRightBtn.newRightBtn("签到规则",target: self, action: #selector(DailySignViewController.rightBarButtonClicked(_:)),color:Constant.common_C2_color,font:Constant.common_F4_font)
    }
    
    @objc fileprivate func rightBarButtonClicked(_ sender:AnyObject) {
        if self.dailySignData != nil {
            let url = URL(string: self.dailySignData!.rule_url)
            let webVC = WebViewController(URL: url)
            self.navigationController?.pushViewController(webVC, animated: true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
