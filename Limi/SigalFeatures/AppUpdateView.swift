//
//  AppUpdateView.swift
//  Limi
//
//  Created by 千云锋 on 16/7/13.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class AppUpdateView: UIView {
    
    /*
     // Only override drawRect: if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func drawRect(rect: CGRect) {
     // Drawing code
     }
     */
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setupUI(frame)
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    //使用代码块创建：关于这个属性的所有属性设置（除了布局）
    fileprivate let updateAlertContentView:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        
        view.layer.cornerRadius = 5
        view.clipsToBounds = true
        return view
    }()
    
    
    //发现新版本
    fileprivate let topTitleLabel:UILabel = {
        let lbl = UILabel()
        //设置字体
        lbl.font = UIFont.boldSystemFont(ofSize: Constant.Theme.FontSize_19)
        //设置字体颜色
        lbl.textColor = UIColor.white
        lbl.backgroundColor = Constant.Theme.Color1
        lbl.textAlignment = NSTextAlignment.center
        return lbl
    }()
    
    //最新版本
    fileprivate let editionNumberTitle:UILabel = {
        let lbl = UILabel()
        lbl.font = Constant.Theme.Font_15
        lbl.textColor = UIColor.black
        return lbl
    }()
    
    //最新版本号
    fileprivate let editionNumberContent:UILabel = {
        let lbl = UILabel()
        lbl.font = Constant.Theme.Font_15
        lbl.textColor = Constant.Theme.Color13
        return lbl
    }()
    
    //新版本大小：
    fileprivate let editionSizeTitle:UILabel = {
        let lbl = UILabel()
        lbl.font = Constant.Theme.Font_15
        lbl.textColor = UIColor.black
        return lbl
    }()//版本大小
    
    //版本大小 200M
    fileprivate let editionSizeContent:UILabel = {
        let lbl = UILabel()
        lbl.font = Constant.Theme.Font_15
        lbl.textColor = Constant.Theme.Color13
        return lbl
    }()
    
    //更新具体内容
    fileprivate let updateInfoContent:UILabel = {
        let lbl = UILabel()
        lbl.font = Constant.Theme.Font_13
        lbl.textColor = Constant.Theme.Color13
        lbl.numberOfLines = 0
        return lbl
    }()
    
    fileprivate let neverRemind:UIButton = {
        let btn = UIButton()
        btn.setTitle("不再提醒", for: UIControlState())
        btn.tag = 120766
        btn.titleLabel?.font = Constant.Theme.Font_15
        btn.setImage(UIImage(named:"noChoose"), for: UIControlState())
        btn.setTitleColor(Constant.Theme.Color7, for: UIControlState())
        return btn
    }()
    
    fileprivate let noUpdate:UIButton = {
        let btn = UIButton()
        btn.setTitle("暂不更新", for: UIControlState())
        btn.titleLabel?.font = Constant.Theme.Font_19
        btn.layer.borderWidth = 0.5
        btn.layer.borderColor = UIColor.black.cgColor
        btn.layer.cornerRadius = 5
        btn.setTitleColor(UIColor.black, for: UIControlState())
        return btn
    }()
    
    fileprivate let updateButton:UIButton = {
        let btn = UIButton()
        btn.setTitle("立即更新", for: UIControlState())
        btn.titleLabel?.font = Constant.Theme.Font_19
        btn.layer.cornerRadius = 5
        btn.setTitleColor(UIColor.white, for: UIControlState())
        btn.backgroundColor = Constant.Theme.Color1
        return btn
    }()
    
    fileprivate let notForceUpdateMenu:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    
    var scale:CGFloat = Constant.ScreenSizeV2.SCALE_SCREEN
    
    
    //MARK:外部函数调用
    
    
    //判断是否显示 以及如何显示
    /**
     autoUpdate：true: 显示此视图 false:不显示
     forceUpdate：true:强制更新  false:非强制
     
     1.3.1
     
     
     上次启动app
     1.3.2
     ：localCurrentVersionNumber   上次服务器传回版本号
     ：localShouldShowUpdate    上次用户的选择：是不是还继续提醒  是否忽略此版本更新：
     
     
     此次：
     1.3.2
     ：localCurrentVersionNumber   上次服务器传回版本号 == 此次serviceVerisonNum
     ：localShouldShowUpdate    上次用户的选择：是不是还继续提醒
     
     
     */
    class func checkUpdateShow() ->Bool{

        if SystemInfoRequestManager.shareInstance().auto_update {
            if SystemInfoRequestManager.shareInstance().version_force {
                return showUpdateView()
            }else{
                if !UserDefaults.standard.bool(forKey: "isNotShowUpdateNext"){
                    return showUpdateView()
                }
                else{
                    if UserDefaults.standard.object(forKey: "notShowUpdateVersion") as! String != Constant.versionCode{
                        UserDefaults.standard.set(false, forKey: "isNotShowUpdateNext")
                        UserDefaults.standard.synchronize()
                        return showUpdateView()
                    }
                    
                }
                
            }
        }
        
        
        return false
    }
    
    
    
    fileprivate class func showUpdateView() ->Bool {
        
        //显示：
        if let window = UIApplication.shared.keyWindow{
            for view in window.subviews{
                if view.isKind(of: AppUpdateView.self){
                    view.removeFromSuperview()
                    break
                }
            }
            
            let view = AppUpdateView(frame: window.bounds)
            
            view.editionNumberContent.text = SystemInfoRequestManager.shareInstance().version_number
            view.editionSizeContent.text = SystemInfoRequestManager.shareInstance().version_size
            view.updateInfoContent.text = SystemInfoRequestManager.shareInstance().version_info
            let attributedString = NSMutableAttributedString(string: view.updateInfoContent.text! as String)
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineSpacing = 2
            attributedString.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: NSMakeRange(0, (view.updateInfoContent.text?.characters.count)!))
            view.updateInfoContent.attributedText = attributedString
            
            print(SystemInfoRequestManager.shareInstance().version_force)
            view.showStyle(SystemInfoRequestManager.shareInstance().version_force)
            window.addSubview(view)
            return true
        }
        
        return false
    }
    
    

    
    //判断显示模式：true:强制更新  false:非强制
    func showStyle(_ forceUpdate:Bool){
        
        if forceUpdate {
            
            updateAlertContentView.addSubview(updateButton)
            updateButton.addTarget(self, action: #selector(AppUpdateView.updateOnClick), for: UIControlEvents.touchUpInside)
            
            updateButton.snp_makeConstraints{ (make) in
                let _ = make.top.equalTo(updateInfoContent.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_30)
                let _ = make.left.equalTo(updateAlertContentView).offset(Constant.ScreenSizeV2.MARGIN_30)
                let _ = make.right.equalTo(updateAlertContentView).offset(-Constant.ScreenSizeV2.MARGIN_30)
                let _ = make.height.equalTo(Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * 80)
                let _ = make.bottom.equalTo(updateAlertContentView).offset(-Constant.ScreenSizeV2.MARGIN_30)
            }
            
        }else{
            
            neverRemind.addTarget(self, action: #selector(AppUpdateView.noRemindOnClick), for: UIControlEvents.touchUpInside)
            noUpdate.addTarget(self, action: #selector(AppUpdateView.noUpdateOnClick), for: UIControlEvents.touchUpInside)
            
            noUpdate.layer.masksToBounds = true
            updateAlertContentView.addSubview(notForceUpdateMenu)
            
            updateButton.addTarget(self, action: #selector(AppUpdateView.updateOnClick), for: UIControlEvents.touchUpInside)
            let _ = [neverRemind, noUpdate, updateButton].map{notForceUpdateMenu.addSubview($0)}
            
            notForceUpdateMenu.snp_makeConstraints{ (make) in
                let _ = make.top.equalTo(updateInfoContent.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_10)
                let _ = make.left.equalTo(updateAlertContentView)
                let _ = make.right.equalTo(updateAlertContentView)
                let _ = make.bottom.equalTo(updateAlertContentView).offset(-Constant.ScreenSizeV2.MARGIN_30)
            }
            
            neverRemind.snp_makeConstraints{ (make) in
                let _ = make.top.equalTo(notForceUpdateMenu).offset(Constant.ScreenSizeV2.MARGIN_20)
                let _ = make.left.equalTo(notForceUpdateMenu).offset(Constant.ScreenSizeV2.MARGIN_44)
                //            make.bottom.equalTo(noUpdate)
            }
            
            noUpdate.snp_makeConstraints{ (make) in
                let _ = make.top.equalTo(neverRemind.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_30)
                let _ = make.left.equalTo(Constant.ScreenSizeV2.MARGIN_30)
                let _ = make.width.equalTo(260 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
                let _ = make.height.equalTo(80 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
                let _ = make.bottom.equalTo(notForceUpdateMenu).offset(-Constant.ScreenSizeV2.MARGIN_10)
            }
            
            updateButton.snp_makeConstraints{ (make) in
                let _ = make.top.equalTo(noUpdate)
                let _ = make.left.equalTo(noUpdate.snp_right).offset(Constant.ScreenSizeV2.MARGIN_20)
                let _ = make.width.equalTo(260 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
                let _ = make.height.equalTo(80 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
            }
        }
    }
    
    
    fileprivate func setupUI(_ frame: CGRect){
        
        self.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        
        
        self.addSubview(updateAlertContentView)
        
        //AutoLayout  创建一个约束    约束对象必须有父视图 -- superView != nil
        updateAlertContentView.snp_makeConstraints { (make) in
            let _ = make.centerX.equalTo(self)
            let _ = make.centerY.equalTo(self)
            let _ = make.width.equalTo(600 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
        }
        
        let _ = [topTitleLabel,editionNumberTitle,editionNumberContent,editionSizeTitle, editionSizeContent, updateInfoContent].map{updateAlertContentView.addSubview($0)}
        
        topTitleLabel.text = "发现新版本"
        editionNumberTitle.text = "最新版本："
        editionSizeTitle.text = "新版本大小："
        topTitleLabel.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(updateAlertContentView)
            let _ = make.left.equalTo(self)
            let _ = make.right.equalTo(self)
            let _ = make.height.equalTo(100 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
        }
        
        editionNumberTitle.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(topTitleLabel.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_36)
            let _ = make.left.equalTo(updateAlertContentView).offset(Constant.ScreenSizeV2.MARGIN_40)
        }
        
        editionNumberContent.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(editionNumberTitle)
            let _ = make.left.equalTo(editionNumberTitle.snp_right)
            let _ = make.right.equalTo(updateAlertContentView)
        }
        
        editionSizeTitle.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(editionNumberTitle.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_30)
            let _ = make.left.equalTo(updateAlertContentView).offset(Constant.ScreenSizeV2.MARGIN_40)
        }
        
        editionSizeContent.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(editionSizeTitle)
            let _ = make.height.equalTo(editionSizeTitle)
            let _ = make.left.equalTo(editionSizeTitle.snp_right)
            let _ = make.right.equalTo(updateAlertContentView)
        }
//        
//        updateInfoTitle.snp_makeConstraints { (make) in
//            make.top.equalTo(editionSizeTitle.snp_bottom).offset(10)
//            make.left.equalTo(updateAlertContentView).offset(Constant.ScreenSizeV2.MARGIN_30)
//            make.right.equalTo(updateAlertContentView)
//        }
//        
        
        updateInfoContent.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(editionSizeTitle.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_30)
            let _ = make.left.equalTo(updateAlertContentView).offset(Constant.ScreenSizeV2.MARGIN_40)
            let _ = make.right.equalTo(updateAlertContentView).offset(-Constant.ScreenSizeV2.MARGIN_40)
        }
        
    }
    
    
    var shouldShowUpdate = true
    
    //不再提醒
    func noRemindOnClick() {
        self.neverRemind.setImage(shouldShowUpdate ? UIImage(named: "choosed") : UIImage(named: "noChoose"), for: UIControlState())
        shouldShowUpdate = !shouldShowUpdate
        
        UserDefaults.standard.set(!shouldShowUpdate, forKey: "isNotShowUpdateNext")
        UserDefaults.standard.set(Constant.versionCode, forKey: "notShowUpdateVersion")
        UserDefaults.standard.synchronize()
        
    }
    
    //暂不更新
    func noUpdateOnClick() {
        self.removeFromSuperview()
    }
    
    
    //去更新
    func updateOnClick() {
        
        let evaluateStr = "https://itunes.apple.com/us/app/xin-yi-dian-dian-sheng-ri/id988254401?l=zh&ls=1&mt=8"
        let url = URL(string: evaluateStr)
        if UIApplication.shared.canOpenURL(url!)
        {
            UIApplication.shared.openURL(url!)
        }
        
        if !SystemInfoRequestManager.shareInstance().version_force{
            self.removeFromSuperview()
        }
    }
}



















