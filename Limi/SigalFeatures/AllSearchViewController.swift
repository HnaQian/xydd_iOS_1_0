//
//  AllSearchViewController.swift
//  Limi
//
//  Created by meimao on 15/11/24.
//  Copyright © 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class AllSearchViewController: BaseViewController,UITextFieldDelegate {
    
    // 判断是 商品列表 还是 攻略列表
    var SearchType: Int = 1
    
    // 搜索词汇
    var keepString: String?
    
    var lastString:String?
    // 搜var框
    var searchBar:SearchTextFieldView!
    fileprivate var shouldReturnUpVC = true
    
    // 搜索热词 搜索历史页面
    fileprivate let historyView = SearchHistoryView()
    // 搜索结果页面
    fileprivate let resultView = SearchResultView()
    
    fileprivate var searchGoodsButton:CommonWidgets.NavigationRightBtn!
    fileprivate var searchArticleButton:CommonWidgets.NavigationRightBtn!
    fileprivate var cancleButton:CommonWidgets.NavigationRightBtn!
    
    var isPoped: Bool = false
    
    var sourceType:Int{return _sourceType}
    fileprivate var _sourceType:Int = 2
    
    //搜索条件
    fileprivate var searchType = "r"
    fileprivate var isSelectButoonClick:Bool = false
    
    fileprivate var giftChoiceScreenView:GiftChoiceScreenView!
    fileprivate let moneyDuesString = ["0","200","500","1000","2000","2000+"]//金额显示
    
    fileprivate let moneyDues = [0,200,500,1000,2000,99990000]//金额区域
    
    class func newSearchViewController(_ sourceType:Int) ->AllSearchViewController{
        let newSearchVC = AllSearchViewController()
        newSearchVC._sourceType = sourceType
        return newSearchVC
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UserDefaults.standard.set(1, forKey: "come_from")
        UserDefaults.standard.synchronize()
        self.navigationBarStyle = 1
        IQKeyboardManager.shared().shouldResignOnTouchOutside = true
        searchBar.textField.delegate = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationBarStyle = 0
        IQKeyboardManager.shared().shouldResignOnTouchOutside = false
        self.navigationController!.setNavigationBarHidden(false, animated: true)
        searchBar.textField.delegate = nil
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.searchBar.textField.resignFirstResponder()
    }
    
    func getPlaceName(_ notification: Notification)
    {
        let user_info:NSDictionary = (notification as NSNotification).userInfo! as NSDictionary
        if let name = user_info.object(forKey: "placeName") as? String{
            searchBar.textField.placeholder = name
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //关闭手势返回
        self.gesturBackEnable = false
        self.view.itop = 20
        
        if self.sourceType == 1{
            searchBar = SearchTextFieldView(frame: CGRect(x: 0,y: 0,width: Constant.ScreenSize.SCREEN_WIDTH - 80,height: 30))
            searchBar.textField.iwidth = searchBar.iwidth
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: searchBar)
            self.navigationItem.title = ""
        }else{
            searchBar = SearchTextFieldView(frame: CGRect(x: 0,y: 0,width: Constant.ScreenSize.SCREEN_WIDTH - 130,height: 30))
            searchBar.textField.iwidth = searchBar.iwidth
            self.navigationItem.titleView = searchBar
        }
        
        searchGoodsButton = CommonWidgets.NavigationRightBtn.newRightBtn("搜礼物", target: self, action: #selector(AllSearchViewController.rightItemAction(_:)),color: Constant.common_C1_color,font:Constant.common_F4_font)
        searchArticleButton = CommonWidgets.NavigationRightBtn.newRightBtn("搜攻略", target: self, action: #selector(AllSearchViewController.rightItemAction(_:)),color: Constant.common_C1_color,font:Constant.common_F4_font)
        
        cancleButton = CommonWidgets.NavigationRightBtn.newRightBtn("取消", target: self, action: #selector(AllSearchViewController.rightItemAction(_:)),color: Constant.common_C6_color,font:Constant.common_F4_font)
        
        self.navigationItem.rightBarButtonItem = sourceType == 2 ? searchArticleButton:cancleButton

        // 添加 搜索历史页面
        self.view.addSubview(historyView)
        historyView.snp_makeConstraints { (make) -> Void in
            let _ = make.left.equalTo(self.view)
            let _ = make.right.equalTo(self.view)
            let _ = make.top.equalTo(self.view)
            let _ = make.bottom.equalTo(self.view)
        }
        
        // 搜索热门 历史页面点击事件
        historyView.saveHandler = {(title: String) -> Void in
            
            Statistics.count(Statistics.SearchAndGiftBasket.search_key_click, andAttributes: ["key" : title])
            
            self.searchBar.textField.text = title
            self.searchBar.changeTitleStyle(title)
            self.keepString = title
            self.lastString = title
            if self.SearchType == 1 {
                self.prepareForSearch(1)
            }else {
                self.prepareForSearch(2)
            }
            
        }
        
        // 添加 搜索结果页面
        resultView.articleTabView.userVC = self
        resultView.goodsColleView.userVC = self
        self.view.addSubview(resultView)
        resultView.delegate = self
        resultView.nav = self.navigationController!
        resultView.isHidden = true
      
        resultView.frame = self.view.bounds
        
        self.view.addSubview(self.resultView.scrollToTopButton)
        
        self.resultView.scrollToTopButton.snp_makeConstraints { (make) in
            let _ = make.right.equalTo(self.view).offset(-40 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
            let _ = make.width.equalTo(100 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
            let _ = make.height.equalTo(100 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
            let _ = make.bottom.equalTo(self.view).offset(-20 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
        }
        
        var screenViewFrame = self.view.bounds
        screenViewFrame.origin.y = self.resultView.goodsTypeView.ibottom
        screenViewFrame.size.height -= self.resultView.goodsTypeView.ibottom
        giftChoiceScreenView = GiftChoiceScreenView.newScreenView(screenViewFrame, moneyDues: moneyDuesString, sexP:nil, didChooseOver: { [weak self]() -> Void in
            
            if let minMoney = self?.moneyDues[(self?.giftChoiceScreenView.moneyDuesIndex.first)!],
                let maxMoney = self?.moneyDues[(self?.giftChoiceScreenView.moneyDuesIndex.last)!]
            {
                if let sex = self?.giftChoiceScreenView.sex{
                    Statistics.count(Statistics.GiftChoice.selectgift_choice_click, andAttributes: ["rang":"\(minMoney)-\(maxMoney)","gender":"\(sex)"])
                }else{
                    Statistics.count(Statistics.GiftChoice.selectgift_choice_click, andAttributes: ["rang":"\(minMoney)-\(maxMoney)","gender":"0"])
                }
            }
            self?.isSelectButoonClick = true
            self?.prepareForSearch(1)
        })
        
        self.view.addSubview(giftChoiceScreenView)
        
        self.resultView.goodsColleView.collectionView.addFooterRefresh { [weak self]() -> Void in
            gcd.async(.default) {
                self?.searchGoodsData(false)
            }
        }
        
        self.resultView.articleTabView.tableView.addFooterRefresh { [weak self]() -> Void in
             gcd.async(.default) {
                self?.searchArticleData(false)
            }
        }
        
        let time: TimeInterval = 1.0
        let delay = DispatchTime.now() + Double(Int64(time * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delay) {
            if self.isPoped == false{
                self.searchBar.textField.becomeFirstResponder()
            }
        }
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(AllSearchViewController.getPlaceName(_:)),
            name: NSNotification.Name(rawValue: Constant.search_get_placeName),
            object: nil)
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // 右上角rightItem触发方法
    func rightItemAction(_ customView:UIView) {
        giftChoiceScreenView.dismiss()
        if customView == searchGoodsButton.customView {
            self.SearchType = 1
            searchType = "r"
            self.isSelectButoonClick = false
            self.giftChoiceScreenView.resetScreenView()
            self.resultView.goodsTypeView.changeFistButtonColor()
            prepareForSearch(1)
            self.navigationItem.rightBarButtonItem = searchArticleButton
        }else if customView == searchArticleButton.customView {
            self.SearchType = 2
            searchType = "r"
            self.giftChoiceScreenView.resetScreenView()
            self.resultView.articleTypeView.changeFistButtonColor()
            prepareForSearch(2)
            self.navigationItem.rightBarButtonItem = searchGoodsButton
        }else {
            if shouldReturnUpVC == false{
                self.searchBar.textField.resignFirstResponder()
                self.searchBar.changeTitleStyle(self.lastString!)
                if self.SearchType == 1 {
                    self.navigationItem.rightBarButtonItem = searchArticleButton
                }else {
                    self.navigationItem.rightBarButtonItem = searchGoodsButton
                }
                return
            }else {
               let _ = self.navigationController!.popViewController(animated: true)
            }
        }
    }
    
    // 搜索框delegate方法
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.searchBar.textField.textColor = UIColor.black
        self.searchBar.textLabel.isHidden = true
        self.navigationItem.rightBarButtonItem = cancleButton
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.searchBar.changeTitleStyle(textField.text!)
        if self.resultView.isHidden == false{
            if self.SearchType == 1 {
                self.navigationItem.rightBarButtonItem = searchArticleButton
            }else {
                self.navigationItem.rightBarButtonItem = searchGoodsButton
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == searchBar.textField {
            if string == "\n" {
                if textField.text?.characters.count != 0 {
                    self.keepString = self.searchBar.textField.text!
                    self.lastString = self.searchBar.textField.text!
                    self.historyView.storageSearchString(str: self.searchBar.textField.text!)
                }
            }
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if searchBar.textField != textField{return false}
        if self.searchBar.textField.text?.characters.count == 0 {return false}
        self.searchBar.changeTitleStyle(self.searchBar.textField.text!)
        searchBar.textField.resignFirstResponder()
        
        self.keepString = self.searchBar.textField.text!
        self.lastString = self.searchBar.textField.text!
        self.historyView.storageSearchString(str: self.searchBar.textField.text!)
        if self.SearchType == 1 {
            self.prepareForSearch(1)
        }else {
            self.prepareForSearch(2)
        }
        
        return true
    }
    
    // 下拉刷新执行 获取数据
    func prepareForSearch(_ mold: Int) {
        
        self.navigationItem.leftBarButtonItem = self.searchCustomBackButton()
        searchBar.iwidth = Constant.ScreenSize.SCREEN_WIDTH - 130
        searchBar.textField.iwidth = searchBar.iwidth
        self.navigationItem.titleView = searchBar
        shouldReturnUpVC = false
        self.historyView.isHidden = true
        self.resultView.isHidden = false
        
        if mold == 1
        {
            //商品
            self.resultView.goodsColleView.collectionView.scrollRectToVisible(CGRect(x: 0, y: 0, width: 1, height: 1), animated: true)
            self.navigationItem.rightBarButtonItem = self.searchArticleButton
            
            self.resultView.goodsPage = 1
            self.resultView.goodsTypeView.isHidden = false
            self.resultView.goodsColleView.isHidden = false
            self.resultView.articleTypeView.isHidden = true
            self.resultView.articleTabView.isHidden = true
             gcd.async(.default) {
                self.searchGoodsData(true)
            }
        }
        if mold == 2
        {
            //文章
            self.resultView.articleTabView.tableView.scrollRectToVisible(CGRect(x: 0, y: 0, width: 1, height: 1), animated: true)
            self.navigationItem.rightBarButtonItem = self.searchGoodsButton
            
            self.resultView.articlePage = 1
            self.resultView.goodsTypeView.isHidden = true
            self.resultView.goodsColleView.isHidden = true
            self.resultView.articleTypeView.isHidden = false
            self.resultView.articleTabView.isHidden = false
             gcd.async(.default) {
                self.searchArticleData(true)
            }
        }
        
    }
    
    func searchCustomBackButton() -> UIBarButtonItem
    {
        let backButton = UIButton(type: UIButtonType.custom)
        backButton.frame = CGRect(x: 0, y: 0, width: 36, height: 36)
        backButton.setBackgroundImage(UIImage(named: "commonBackNoBG"), for: UIControlState())
        backButton.addTarget(self, action: #selector(AllSearchViewController.backButtonClicked), for: UIControlEvents.touchUpInside)
        backButton.clipsToBounds = true
        
        let item = UIBarButtonItem(customView: backButton)
        
        return item
    }
    
    func backButtonClicked() {
        self.isPoped = true
        SearchType = 1
        searchBar.textField.resignFirstResponder()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: Constant.search_get_placeName), object: nil)
        let _ = self.navigationController?.popViewController(animated: true)
    }
}

//MARK: - network
extension AllSearchViewController {
    
    //搜索商品
    //type 综合：r，销量：s，人气：c，价格升序：p_asc，价格降序：p_desc
    func searchGoodsData(_ isRefresh:Bool) {
        
        var params = [String:AnyObject]()
        params["words"] = self.keepString as AnyObject?
        params["gender"] = giftChoiceScreenView.sex as AnyObject?
        if self.isSelectButoonClick {
            params["min_price"] = moneyDues[giftChoiceScreenView.moneyDuesIndex.first!] as AnyObject?
            params["max_price"] = moneyDues[giftChoiceScreenView.moneyDuesIndex.last!] as AnyObject?
        }

        params["type"] = self.searchType as AnyObject?
        params["size"] = self.resultView.goodsSize as AnyObject?
        params["page"] = self.resultView.goodsPage as AnyObject?
    
        let _ = LMRequestManager.requestByDataAnalyses( context: self.view, URLString: Constant.JTAPI.search_goods, parameters: params, isNeedUserTokrn: true, isShowErrorStatuMsg: true,  successHandler: { (data, status, msg) in
            self.endRefresh()
            if status != 200 {return}
            
            if let tempTotal = JSON(data as AnyObject)["data"]["total"].string {
                self.resultView.gnum = Int(tempTotal)!
            }
            
            var count = 0
            if let tempArray = JSON(data as AnyObject)["data"]["list"].array {
                print(tempArray)
                count = tempArray.count
                if isRefresh {
                    self.resultView.goodsList = [JSON]()
                    self.resultView.goodsList = tempArray
                    if self.resultView.gnum != 0 {
                        (self.resultView.goodsColleView.collectionView.collectionViewLayout as! UICollectionViewFlowLayout).headerReferenceSize = CGSize(width: 0, height: 0)
                    }else{
                        (self.resultView.goodsColleView.collectionView.collectionViewLayout as! UICollectionViewFlowLayout).headerReferenceSize = CGSize(width: Constant.ScreenSize.SCREEN_WIDTH, height: EmptyView2.viewHeight)
                        self.resultView.goodsColleView.headerView.frame.size = CGSize(width: Constant.ScreenSize.SCREEN_WIDTH,height: EmptyView2.viewHeight)
                        self.resultView.searchStr = self.keepString!
                        
                    }
                }else {
                    for index in 0 ..< tempArray.count {
                        self.resultView.goodsList.append(tempArray[index])
                    }
                }
                self.resultView.goodsColleView.goodsList = self.resultView.goodsList
            }
            
            if count < self.resultView.goodsSize  {
                self.resultView.goodsColleView.collectionView.endFooterRefreshWithNoMoreData()
            }else {
               self.resultView.goodsPage += 1
            }
            
            },failureHandler:{[weak self] in
                self?.endRefresh()
            })
        
    }
    
    //搜索文章
    //type 综合：r，阅读数：v，分享数：s，发布时间升序：p_asc，发布时间降序：p_desc
    func searchArticleData(_ isRefresh:Bool) {
        
        var params = [String:AnyObject]()
        params["words"] = self.keepString as AnyObject?
        params["type"] = self.searchType as AnyObject?
        params["size"] = self.resultView.articleSize as AnyObject?
        params["page"] = self.resultView.articlePage as AnyObject?
        
        let _ = LMRequestManager.requestByDataAnalyses( context: self.view, URLString: Constant.JTAPI.search_article, parameters: params, isNeedUserTokrn: true, isShowErrorStatuMsg: true,  successHandler: { (data, status, msg) in
            self.endRefresh()
            if status != 200 {return}
            
            if let tempTotal = JSON(data as AnyObject)["data"]["total"].string {
                self.resultView.anum = Int(tempTotal)!
            }
            
            var count = 0
            if let tempArray = JSON(data as AnyObject)["data"]["list"].array {
                count = tempArray.count
                if isRefresh {
                    self.resultView.articleList = [JSON]()
                    self.resultView.articleList = tempArray
                    if self.resultView.anum != 0 {
                        self.resultView.articleTabView.tableView.tableHeaderView = UIView(frame: CGRect(x: 0,y: 0,width: Constant.ScreenSize.SCREEN_WIDTH,height: 0.1))
                    }else{
                        self.resultView.searchStr = self.keepString!
                    }
                }else {
                    for index in 0 ..< tempArray.count {
                        self.resultView.articleList.append(tempArray[index])
                    }
                }
                self.resultView.articleTabView.articleList = self.resultView.articleList
            }
            if count < self.resultView.articleSize {
                self.resultView.articleTabView.tableView.endFooterRefreshWithNoMoreData()
            }else {
                self.resultView.articlePage += 1
            }
            
            },failureHandler:{[weak self] in
                self?.endRefresh()
            })
    }

    
    // 关闭刷新
    func endRefresh()
    {
        
        self.resultView.goodsColleView.collectionView.endFooterRefresh()
        
        self.resultView.articleTabView.tableView.endFooterRefresh()
        
    }
    
    
}

extension AllSearchViewController:SearchResultViewDelegate {
    func searchResultViewclicked(_ value:String,type:Int)
    {
        if value == "select" {
            filterItemClicked()
        }else {
            self.searchType = value
            prepareForSearch(type)
            giftChoiceScreenView.dismiss()
        }
    }
    
    func filterItemClicked() {
        if giftChoiceScreenView.isShowing
        {
            giftChoiceScreenView.dismiss()
        }else {
            
            if self.resultView.itop == 0 {
                giftChoiceScreenView.itop = self.resultView.goodsTypeView.ibottom
            }else {
                giftChoiceScreenView.itop = self.resultView.goodsTypeView.ibottom + 20
            }
            giftChoiceScreenView.show()
        }
    }
}
