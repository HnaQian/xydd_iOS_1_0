//
//  BaseRechargeView.swift
//  Limi
//
//  Created by Richie on 16/5/25.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//充值中心

import UIKit
import CoreData
import ContactsUI
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}




protocol RechargeView:NSObjectProtocol {
    /*
     public var taskIdentifier: Int { get } /* an identifier for this task, assigned by and unique to the owning session */
     @NSCopying public var originalRequest: NSURLRequest? { get } /* may be nil if this is a stream task */
     @NSCopying public var currentRequest: NSURLRequest? { get } /* may differ from originalRequest due to http server redirection */
     @NSCopying public var response: NSURLResponse? { get } /* may be nil if no response has been received */
     
     */
    
    
    //类型 0:话费 1:流量
    var type:Int{get}
    
    //当前视图的View
    var view:UIView{get}
    
    //总花费
    var sumCost:Double{get}
    
    //充值说明
    var rule_url:String{get}
    
    //提交订单连接
    var commitUrl:String{get}
    
    //手机号码
    var phoneNumber:String{get}
    
    //选择的红包
    var couponModel:CouponModel?{get}
    
    //充值面值
    var rechargeModel:RechargeModel?{get}
    
    weak var viewController:UIViewController?{get set}
    
    weak var delegate:BaseRechargeViewDelegate?{get set}
    
    ///刷新红包数据
    func reloadData()
    
    ///刷新手机号
    func reloadDataWithPhoneNumber(_ phoneNumber:String)
}



class BaseRechargeView: UIView,RechargeView {
    
    var type:Int{return self.rechargeTyep}
    
    //手机号码
    var phoneNumber:String{return _phoneNumber}
    //选择的红包
    var couponModel:CouponModel?{return _couponModel}
    //充值面值
    var rechargeModel:RechargeModel?{return _selectedCard}
    //总花费
    var sumCost:Double{
        var cost:Double = 0
        if let model = rechargeModel{
            
            if let coupon = couponModel{
                if coupon.discount >= model.price{
                    cost = 0
                }else{
                    cost = model.price - coupon.discount
                }
            }else{
                cost = model.price
            }
        }
        return cost
    }
    
    //充值说明
    var rule_url:String{return _rule_url}
    
    //流量充值提示
    var tipes:String{return _tipes}
    
    //提交订单连接
    var commitUrl:String{return self.rechargeTyep == 0 ? Constant.JTAPI.fees_order : Constant.JTAPI.flow_order}
    
    var view:UIView{return self}
    
    //当前选择的面值
    fileprivate var selectedCard:RechargeModel?{return _selectedCard}
    fileprivate var _selectedCard:RechargeModel?
    
    weak var delegate:BaseRechargeViewDelegate?
    
    weak var viewController:UIViewController?{
        didSet{
            collectionFooter.viewController = viewController
            collectionHeader.viewController = viewController
        }
    }
    
    //    private var _rechargeModel:RechargeModel?
    fileprivate var _couponModel:CouponModel?
    
    //是否已经把所有的card设为不可选择
    fileprivate var didSetAllCardsDisable :Bool{return _didSetAllCardsDisable}
    fileprivate var _didSetAllCardsDisable = false
    
    //充值规则
    fileprivate var _rule_url:String = ""
    //tipe
    fileprivate var _tipes = ""
    
    //header
    fileprivate var collectionHeader:RechargeHeaderView!
    //footer
    fileprivate var collectionFooter:RechargeFooterView!
    
    fileprivate lazy var collectionView:UICollectionView = {
        
        let collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: Constant.ScreenSizeV1.SCREEN_WIDTH, height: Constant.ScreenSizeV1.SCREEN_HEIGHT - MARGIN_49 + 10), collectionViewLayout: self.flowLayout)
        
        collectionView.bounces = true
        collectionView.alwaysBounceVertical = true
        collectionView.autoresizingMask = [UIViewAutoresizing.flexibleHeight, UIViewAutoresizing.flexibleWidth]
        collectionView.register(RechargeCell.self, forCellWithReuseIdentifier: RechargeCell.reuseIdentifier)
        
        //注册header
        collectionView.register(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: RechargeHeaderView.reuseIdentifier)
        
        //注册footer
        collectionView.register(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: RechargeFooterView.reuseIdentifier)
        
        collectionView.backgroundColor = UIColor.white
        
        return collectionView
    }()
    
    fileprivate lazy var flowLayout:UICollectionViewFlowLayout = {
        
        var flow = UICollectionViewFlowLayout()
        
        flow.minimumLineSpacing = RechargeCell.lineSpacing
        flow.minimumInteritemSpacing = RechargeCell.interitemSpacing
        
        flow.sectionInset = UIEdgeInsetsMake(RechargeCell.lineSpacing, RechargeCell.lineSpacing, RechargeCell.lineSpacing, RechargeCell.lineSpacing)
        flow.headerReferenceSize = CGSize(width: Constant.ScreenSizeV1.SCREEN_WIDTH,height: RechargeHeaderView.height)
        flow.footerReferenceSize = CGSize(width: Constant.ScreenSizeV1.SCREEN_WIDTH,height: RechargeFooterView.height)
        return flow
    }()
    
    //面值数组
    fileprivate var faceValueAry = [RechargeModel]()
    
    //广告数组
    fileprivate var adDataModelAry = [ADMode]()
    
    //0：话费充值  1：流量充值
    fileprivate var rechargeTyep = 0
    
    //    //最后一次选中的面值
    //    private var lastSelectedIndexPath:RechargeModel?{return _lastSelectedIndexPath}
    //    private var _lastSelectedIndexPath:RechargeModel?
    //电话号码
    fileprivate var _phoneNumber = ""
    
    init(frame: CGRect,type:Int) {
        super.init(frame: frame)
        
        self.rechargeTyep = type
        self.setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    //MARK: OUTER: 刷新红包数据
    func reloadData(){
        
        //即将刷新红包数据前，记录当前选择的红包
        self.collectionFooter.setSelectedCoupon(self.couponModel)
        //红包控件 设置为：无可用红包
        self.collectionFooter.reloadCoupon(nil,phoneNumber: "")
        //把当前选择红包置为空
        _couponModel = nil
        
        if self.rechargeModel != nil{
            //通知刷新
            self.delegate?.rechargeViewCardsDidselected(self, rechargeModel: self.rechargeModel!,phoneNumber: self.phoneNumber)
            
            //刷新红包数据
            if self.phoneNumber.characters.count == 11{
                self.collectionFooter.reloadCoupon(self.rechargeModel!,phoneNumber: self.phoneNumber)
            }
        }
    }
    
    func reloadDataWithPhoneNumber(_ phoneNumber:String){
        self.collectionHeader.setPhoneNumber(phoneNumber)
    }
    
    //MARK: Service选中的Card
    fileprivate func serviceSelectedModel(_ dataModel:RechargeModel){
        
        //红包控件
        _couponModel = nil
        //        _rechargeModel = dataModel
        _selectedCard = dataModel
        
        for tempModel in self.faceValueAry{
            if tempModel.id ==  dataModel.id{
                tempModel.didSelected = true
            }else{
                tempModel.didSelected = false
            }
        }
        
        self.collectionView.reloadData()
        
        //card改变之后，红包记录去除
        self.collectionFooter.setSelectedCoupon(nil)
        
        //红包设置为：无可用红包
        self.collectionFooter.reloadCoupon(nil,phoneNumber: "")
        
        //刷新红包数据
        self.collectionFooter.reloadCoupon(dataModel,phoneNumber: phoneNumber)
        
        if dataModel.status{
            //通知delegate已经选择了card
            self.delegate?.rechargeViewCardsDidselected(self, rechargeModel: dataModel,phoneNumber: phoneNumber)
        }
    }
    
    
    //MARK: User选中card
    fileprivate func handUpSelected(_ dataModel:RechargeModel){
        //判断当前选择的card与上次的是否一样，如果一样不做操作
        if let currentModel = rechargeModel{
            if currentModel.id == dataModel.id{
                return
            }
        }
        
        //红包控件
        _couponModel = nil
        //        _rechargeModel = dataModel
        _selectedCard = dataModel
        
        for tempModel in self.faceValueAry{
            if tempModel.id ==  dataModel.id{
                tempModel.didSelected = true
            }else{
                tempModel.didSelected = false
            }
        }
        
        self.collectionView.reloadData()
        
        //card改变之后，红包记录去除
        self.collectionFooter.setSelectedCoupon(nil)
        
        //红包设置为：无可用红包
        self.collectionFooter.reloadCoupon(nil,phoneNumber: "")
        
        if dataModel.status{
            //通知delegate已经选择了card
            self.delegate?.rechargeViewCardsDidselected(self, rechargeModel: dataModel,phoneNumber: phoneNumber)
        }
    }
    
    
    
    //MARK: 选中coupon
    fileprivate func couponChoosed(_ coupon:CouponModel){
        _couponModel = coupon
        self.delegate?.rechargeViewCouponDidselected(self, couponModel: coupon,phoneNumber:phoneNumber)
    }
    
    
    //MARK: 手机号码改变时：把所有cell置为不可点击、立即充值按钮置为不可点击、金额置为 0,并通知红包选择控件
    /*
     手机号码改变时：
     1.把所有cell置为不可点击
     2.立即充值按钮置为不可点击
     3.外部系那是金额置为 0
     4.通知红包选择控件 -> 无可用红包
     */
    
    
    fileprivate func refreshCollectionWhenPhonenNmberChanged(){
        
        _couponModel = nil
        //        _rechargeModel = nil
        _selectedCard = nil
        
        self.collectionFooter.setSelectedCoupon(nil)
        
        //红包控件
        self.collectionFooter.reloadCoupon(nil,phoneNumber: "")
        
        if !self.didSetAllCardsDisable{
            let _ = self.faceValueAry.map{$0.status = false}
            self.collectionView.reloadData()
            _didSetAllCardsDisable = true
        }
        
        //刷新外部工具栏
        self.delegate?.rechargeViewCouponDidchangePhoneNumber(self)
    }
    
    
    //MARK: 广告点击事件
    fileprivate func bannerSelected(_ href:String){
        if let URL = URL(string: href){
            EventDispatcher.dispatch(URL: URL, onNavigationController : viewController?.navigationController)
        }
    }
    
    
    ///header 和 footer的事件监听设置
    fileprivate func setupFunction(){
        
        self.collectionView.delegate  = self
        self.collectionView.dataSource  = self
        
        collectionHeader.setActionOperation({ [weak self](phoneNumber) in
            
            self?._phoneNumber = phoneNumber
            
            if phoneNumber.characters.count == 11{
                self?.loadDataWithPhoneNumber(phoneNumber)
            }else{
                self?.refreshCollectionWhenPhonenNmberChanged()
            }
            
            }, selectedBanner: { [weak self](href) in
                self?.bannerSelected(href)
            },openPhoneBook:{[weak self] in
                self?.openPhoneBook()
            })
        
        self.collectionFooter.setChooseCouponAction({[weak self](coupon) in
            self?.couponChoosed(coupon)
            })
        
        
        self.loadDataWithoutPhoneNumber()
        
        if UserInfoManager.didLogin == true{
            
            var userInfomation : User? = nil
            
            let requestUserInfo: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
            let datasourceUserInfo : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(requestUserInfo)!
            if datasourceUserInfo.count > 0{
                userInfomation = datasourceUserInfo[0] as? User
            }
            
            if  userInfomation?.user_name != "" && userInfomation?.user_name != nil{
                self.collectionHeader.setPhoneNumber(userInfomation!.user_name!)
            }
        }
        
        //检查本地是否有手机号码，如果有就请求数据
    }
}


//MARK: 网络请求
extension BaseRechargeView{
    
    
    //通过手机号获取数据
    func loadDataWithPhoneNumber(_ phoneNumber:String){
        
        /**
         token	false	string	token值
         mobile	true	string	手机号
         id	false	int	产品ID，传此值时，token必须传
         */
        if phoneNumber.characters.count < 11{return}
        
        let url = rechargeTyep == 0 ? Constant.JTAPI.fees_segment : Constant.JTAPI.flow_segment
        
        var param = [String:AnyObject]()
        if _selectedCard != nil{
            param["id"] = _selectedCard!.id as AnyObject?
        }
        
        param["mobile"] = phoneNumber as AnyObject?
        
        let _ = LMRequestManager.requestByDataAnalyses(context:self, URLString: url,parameters: param,isNeedUserTokrn: true,successHandler: {data, status, msg in
            
            if status != 200{
                self.collectionHeader.setPhoneAddressTipeLbl(msg, iswarning: true)
                return
            }
            
            /*
             flow
             
             "address": "上海 联通",
             "list": [
             {
             "Id": 7,
             "Name": "20M",
             "Is_discount": 2,
             "Price": 30
             },
             
             
             fees
             "address":"北京 移动",
             "list": [
             {
             "Id": 1,
             "Name": "30元",
             "Is_discount": 1,
             "Price": 25,
             "Status": false
             },
             
             */
            
            
            self.collectionFooter.setTipes(JSON(data as AnyObject)["data"]["tips"].string)
            
            if let address = JSON(data as AnyObject)["data"]["address"].string{
                self.collectionHeader.setPhoneAddressTipeLbl(address,iswarning: false)
            }
            
            if let list = JSON(data as AnyObject)["data"]["list"].arrayObject{
                
                if list.count > 0{
                    
                    self.faceValueAry = [RechargeModel]()
                    var shouldUseDefaultModel = true
                    var selectedModel:RechargeModel?
                    
                    for index in 0..<list.count{
                        
                        let model = RechargeModel(list[index] as! [String:AnyObject])
                        self.faceValueAry.append(model)
                        
                        //如果最后一次的选择 的model不为空
                        if self.selectedCard != nil && shouldUseDefaultModel{
                            //判断是否与最后一次选择的model相同的model 判断此model是否可选
                            if self.selectedCard!.id == model.id && model.status{
                                selectedModel = model
                                shouldUseDefaultModel = false
                            }
                        }
                    }
                    
                    //如果需要设置默认的
                    if shouldUseDefaultModel{
                        for model in self.faceValueAry{
                            //如果有默认的，查看默认的
                            if model.didSelected && model.status{
                                selectedModel = model
                            }
                        }
                    }
                    
                    if let model = selectedModel{
                        self.serviceSelectedModel(model)
                    }else{
                        self.collectionView.reloadData()
                    }
                    
                    self._didSetAllCardsDisable = false
                }
            }
            
        }){Void in}
    }
    
    
    //获取数据
    func loadDataWithoutPhoneNumber(){
        
        let url = rechargeTyep == 0 ? Constant.JTAPI.fees_list : Constant.JTAPI.flow_list
        
       let _ = LMRequestManager.requestByDataAnalyses(HTTPMethod.get,context:self, URLString: url,isShowErrorStatuMsg:true,isNeedHud : rechargeTyep == 0 ? true : false, successHandler: {data, status, msg in
            
            if status != 200{return}
            
            /*
             flow
             
             "list": [
             {
             "Id": 7,
             "Name": "20M",
             "Is_discount": 2
             },
             
             
             fees
             "list": [
             {
             "Id": 1,
             "Name": "30元",
             "Is_discount": 2
             },
             */
            //            print(data)
            
            //及充值规则
            if let rule_url = JSON(data as AnyObject)["data"]["rule_url"].string{
                self._rule_url = rule_url
            }
            
            //广告数据
            if let list = JSON(data as AnyObject)["data"]["ad_list"].array{
                if list.count > 0{
                    self.adDataModelAry = [ADMode]()
                    
                    list.forEach({ (dic) in
                        self.adDataModelAry.append(ADMode(dataDic: dic))
                    })
                    
                    self.collectionHeader.refreshBanner(self.adDataModelAry)
                }
            }
            
            //如果此时已经有数据，不再加载数据
            if self.faceValueAry.count > 0{return}
            
            //面值表
            if let list = JSON(data as AnyObject)["data"]["list"].arrayObject{
                if list.count > 0{
                    self.faceValueAry = [RechargeModel]()
                    
                    list.forEach({ (dic) in
                        self.faceValueAry.append(RechargeModel(dic as! [String:AnyObject]))
                    })
                    
                    self.collectionView.reloadData()
                    
                    self._didSetAllCardsDisable = true
                }
            }
            
        }){Void in}
    }
}


//MARK: 通讯录
extension BaseRechargeView:ABPeoplePickerNavigationControllerDelegate,CNContactPickerDelegate{
    
    
    func openPhoneBook()
    {
        self.navigationBarAppearance(true)
        if #available(iOS 9.0, *) {
            let newPicker = CNContactPickerViewController()
            newPicker.delegate = self
            self.viewController?.present(newPicker, animated: true, completion: {
                
            })
        } else {
            let picker = ABPeoplePickerNavigationController()
            
            picker.peoplePickerDelegate = self
            
            if ((UIDevice.current.systemVersion as NSString).floatValue >= 8.0)
            {
                picker.predicateForSelectionOfPerson = NSPredicate(value:false)
            }
            
            picker.displayedProperties = [3]
            
            self.viewController?.present(picker, animated: true) { () -> Void in
                
            }

        }

    }
    
    @available(iOS 9.0, *)
    @objc(contactPicker:didSelectContactProperty:)
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contactProperty: CNContactProperty) {
        if contactProperty.value != nil {
            let tempPhoneNumber = contactProperty.value as! CNPhoneNumber
            let phoneNumber = tempPhoneNumber.stringValue
            self.collectionHeader.setPhoneNumber(phoneNumber)
        }
       
    }
    
    @available(iOS 9.0, *)
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        //去除地址选择界面
        picker.dismiss(animated: true, completion: { () -> Void in
            
        })
    }

    
    func peoplePickerNavigationController(_ peoplePicker: ABPeoplePickerNavigationController,
                                          didSelectPerson person: ABRecord) {
        
        /* Get all the phone numbers this user has */
        let unmanagedPhones = ABRecordCopyValue(person, kABPersonPhoneProperty)
        let phones: ABMultiValue =
            Unmanaged.fromOpaque(unmanagedPhones!.toOpaque()).takeUnretainedValue()
                as NSObject as ABMultiValue
        
        let countOfPhones = ABMultiValueGetCount(phones)
        
        for index in 0..<countOfPhones{
            let unmanagedPhone = ABMultiValueCopyValueAtIndex(phones, index)
           let _ = Unmanaged.fromOpaque(
                (unmanagedPhone?.toOpaque())!).takeUnretainedValue() as NSObject as! String
        }
    }
    
    func peoplePickerNavigationController(_ peoplePicker: ABPeoplePickerNavigationController,
                                          didSelectPerson person: ABRecord, property: ABPropertyID,
                                                          identifier: ABMultiValueIdentifier) {
        
        let multiValue: ABMultiValue = ABRecordCopyValue(person, property).takeRetainedValue()
        var index = ABMultiValueGetIndexForIdentifier(multiValue, identifier)
        if index == -1{
            index = 0 as CFIndex
        }
        let phone = ABMultiValueCopyValueAtIndex(multiValue, index).takeRetainedValue() as? String ?? ""
        
        self.collectionHeader.setPhoneNumber(phone)
    }
    
    //取消按钮点击
    func peoplePickerNavigationControllerDidCancel(_ peoplePicker: ABPeoplePickerNavigationController) {
        
        //去除地址选择界面
        
        peoplePicker.dismiss(animated: true, completion: { () -> Void in
            
        })
    }
    
    func peoplePickerNavigationController(_ peoplePicker: ABPeoplePickerNavigationController,
                                          shouldContinueAfterSelectingPerson person: ABRecord) -> Bool {
        return false
    }
    
    func peoplePickerNavigationController(_ peoplePicker: ABPeoplePickerNavigationController,
                                          shouldContinueAfterSelectingPerson person: ABRecord, property: ABPropertyID,
                                                                             identifier: ABMultiValueIdentifier) -> Bool {
        return false
    }
    
    
    func navigationBarAppearance(_ shouldSet:Bool)
    {
        if shouldSet
        {
            let image = UIImage(named: "titlebar")
            
            UINavigationBar.appearance().tintColor = UIColor.black
            
            UINavigationBar.appearance().setBackgroundImage(image, for: UIBarMetrics.default)
        }else
        {
            UINavigationBar.appearance().tintColor = UIColor.white
            
            UINavigationBar.appearance().setBackgroundImage(nil, for: UIBarMetrics.default)
        }
    }
}


//MARK: Collection
extension BaseRechargeView:UICollectionViewDelegate,UICollectionViewDataSource{
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int{
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return self.faceValueAry.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: RechargeCell.reuseIdentifier, for: indexPath) as! RechargeCell
        self.faceValueAry[(indexPath as NSIndexPath).row].indexPath = indexPath
        cell.refreshData(self.faceValueAry[(indexPath as NSIndexPath).row])
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize{
        //384  260
        return CGSize(width: RechargeCell.width, height: RechargeCell.height)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView{
        
        precondition(UICollectionElementKindSectionHeader == kind || UICollectionElementKindSectionFooter == kind ,"Unexpected element kind")
        if UICollectionElementKindSectionHeader == kind{
            //RechargeHeaderView
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: RechargeHeaderView.reuseIdentifier, for: indexPath)
            
            if collectionHeader.superview == nil{headerView.addSubview(collectionHeader)}
            
            return headerView

        }
        let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: RechargeFooterView.reuseIdentifier, for: indexPath)
        
        if collectionFooter.superview == nil{footerView.addSubview(collectionFooter)}
        
        return footerView
//        switch kind {
//            
//        case UICollectionElementKindSectionHeader:
//            //RechargeHeaderView
//            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: RechargeHeaderView.reuseIdentifier, for: indexPath)
//            
//            if collectionHeader.superview == nil{headerView.addSubview(collectionHeader)}
//            
//            return headerView
//            
//        case UICollectionElementKindSectionFooter:
//            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: RechargeFooterView.reuseIdentifier, for: indexPath)
//            
//            if collectionFooter.superview == nil{footerView.addSubview(collectionFooter)}
//            
//            return footerView
//            
//        default:
//            assert(false, "Unexpected element kind")
//        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let model = self.faceValueAry[(indexPath as NSIndexPath).row]
        
        if model.canSelected{
            
            self.handUpSelected(model)
            
            //刷新产品数据
            self.loadDataWithPhoneNumber(phoneNumber)
        }
    }
}


//MARK:布局
extension BaseRechargeView{
    
    
    //布局
    fileprivate func setupUI(){
        
        self.collectionHeader = RechargeHeaderView(frame: CGRect(x: 0,y: 0,width: Constant.ScreenSizeV1.SCREEN_WIDTH,height: RechargeHeaderView.height))
        
        self.collectionFooter = RechargeFooterView(frame: CGRect(x: 0,y: 0,width: Constant.ScreenSizeV1.SCREEN_WIDTH,height: RechargeFooterView.height),type:rechargeTyep)
        
        
        self.addSubview(self.collectionView)
        
        self.collectionView.frame = self.bounds
        self.collectionView.backgroundColor = UIColor.white
        
        //        self.collectionView.backgroundColor = UIColor(rgba: Constant.common_background_color)
        
        setupFunction()
    }
    
    
    //MARK:CollectionView 的 header
    class RechargeHeaderView: UIView,BBannerViewDataSource,BBannerViewDelegate,UITextFieldDelegate {
        
        static let height:CGFloat = (112 + 75 + 5) * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE
        
        weak var viewController:UIViewController?
        
        static let reuseIdentifier = "RechargeHeaderView_reuseIdentifier"
        
        fileprivate var textfieldContent = ""
        
        //wedgites
        //广告
        fileprivate let banner:BBannerView = {
            return BBannerView(frame:CGRect(x: 0, y: 0, width: Constant.ScreenSizeV1.SCREEN_WIDTH, height: 112 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE))
        }()
        
        
        //输入框
        fileprivate let phoneTextField:UITextField = {
            let textField = UITextField()
            textField.placeholder = "请输入手机号"
            textField.font = Constant.Theme.Font_31
            textField.keyboardType = UIKeyboardType.numberPad
            textField.iheight = 60 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE
            textField.iwidth = Constant.ScreenSizeV1.SCREEN_WIDTH - 100
            
            return textField
        }()
        
        
        //通讯录/清除
        fileprivate let btn:UIButton = {
            let btn = UIButton()
            btn.iheight = 26 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE
            btn.iwidth = 26 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE
            btn.imageView?.contentMode = UIViewContentMode.scaleAspectFill
            return btn
        }()
        
        
        //提示语
        fileprivate let tipeLbl:UILabel = {
            let lbl = UILabel()
            lbl.textColor = UIColor(rgba: Constant.common_C1_color)
            lbl.font = Constant.Theme.Font_13
            lbl.iwidth = Constant.ScreenSizeV1.SCREEN_WIDTH - MARGIN_8 * 2
            lbl.iheight = 15 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE
            
            return lbl
        }()
        
        fileprivate var adDataSource = [ADMode]()
        
        fileprivate var bannerUrl = ""
        
        //Block 如果phoneNumber的长度是11位，请求面值数据 否则请求通讯录
        fileprivate var phoneNumberChangeBlock:((_ phoneNumber:String)->Void)!
        
        //广告的连接
        fileprivate var selectedBanner:((_ href:String)->Void)!
        
        //打开通讯录
        fileprivate var openPhoneBook:(()->Void)!
        
        //0：话费充值 1：流量充值
        fileprivate var type = 0
        
        override init(frame: CGRect){
            super.init(frame: frame)
            setupUI()
        }
        
        
        ///OUTER：设置操作
        func setActionOperation(_ putinMobilePhoneOver:@escaping ((_ phoneNumber:String)->Void),selectedBanner:@escaping ((_ href:String)->Void),openPhoneBook:@escaping (()->Void)){
            
            self.openPhoneBook = openPhoneBook
            
            self.phoneNumberChangeBlock = putinMobilePhoneOver
            
            self.selectedBanner = selectedBanner
        }
        
        
        ///OUTER：设置手机号
        func setPhoneNumber(_ number:String){
            
            phoneTextField.text = number
            self.textFieldDidChange(phoneTextField)
        }
        
        ///OUTER：在请求完数据的时候 设置手机号码提示
        func setPhoneAddressTipeLbl(_ tipe:String,iswarning:Bool){
            
            self.tipeLbl.textColor = iswarning ? UIColor(rgba: Constant.common_C1_color) : UIColor(rgba: Constant.common_C8_color)
            
            self.tipeLbl.text = tipe
        }
        
        
        //OUTER：广告数据
        fileprivate func refreshBanner(_ adDataModelAry:[ADMode]){
            
            self.adDataSource = adDataModelAry
            self.banner.reloadData()
        }
        
        
        //textField ------->
        @objc fileprivate func textFieldDidChange(_ textField: UITextField) {
            
            if let _ = textField.text{
                
                textField.text = checkNumber(textField.text!)
                
                //长度控制在11个数字加两个空格
                if textField.text!.characters.count > 13{
                    textField.text = NSString(string: textField.text!).substring(to: 13)
                }
                
                textfieldContent = textField.text!
                
                //如果编辑了，提示语置空
                if textfieldContent.characters.count < 13{
                    self.tipeLbl.text = ""
                }
                
                //如果内容有变化，回调刷新collectionView的cell的状态
                if self.phoneNumberChangeBlock != nil{
                    let newContent = NSString(string: textfieldContent).replacingOccurrences(of: " ", with: "")
                    self.phoneNumberChangeBlock(newContent)
                }
                
            }else{
                textfieldContent = ""
            }
            
            
            if self.phoneTextField.text?.characters.count > 0{
                btn.setImage(UIImage(named: "recharge_delete"), for: UIControlState())
            }else{
                btn.setImage(UIImage(named: "chargeContactIcon"), for: UIControlState())
            }
        }
        
        
        //数字空格
        fileprivate func checkNumber(_ text:String) ->String{
            //去除所有空格，重新添加空格，防止手动删除空格之后造成不必要的问题
            
            if text.characters.count == 0{
                return ""
            }
            
            var newContent = text
            var tempContent = ""
            
            //去除 +86
            newContent = Utils.detectionTelphoneSimple(text)
            
            //筛选出newContent中的数字
            for index in 0..<newContent.characters.count{
                let char = NSString(string:newContent).substring(with: NSMakeRange(index,1))
                if Utils.checkNumber(char){
                    tempContent = tempContent + char
                }
            }
            
            let newText:NSMutableString = NSMutableString(string: tempContent)
            
            if newText.length > 3{
                //在第三位之后添加空格
                newText.insert(" ", at: 3)
            }
            
            if newText.length > 8{
                //在第8位之后添加空格
                newText.insert(" ", at: 8)
            }
            
            return newText as String
        }
        
        
        
        //btn action
        @objc fileprivate func btnOnClick(){
            
            if textfieldContent.characters.count > 0{
                //清除textField
                phoneTextField.text = ""
                self.textFieldDidChange(phoneTextField)
            }else{
                //打开通讯录
                if self.openPhoneBook != nil{
                    self.openPhoneBook()
                }
            }
        }
        
        
        //banner selected
        func didSelectItem(_ index: Int){
            if self.selectedBanner != nil{
                self.selectedBanner(adDataSource[index].href)
            }
        }
        
        
        
        //banner delegate ---------->
        func viewForItem(_ bannerView: BBannerView, index: Int) -> UIView{
            let img = ADImageView(frame: banner.bounds, data: adDataSource[index])
            img.backgroundColor = UIColor.red
            return img
        }
        
        
        func numberOfItems() -> Int{
            return adDataSource.count
        }
        
        
        fileprivate func setupUI(){
            self.backgroundColor = UIColor.white
            self.isUserInteractionEnabled = true
            
            banner.delegate = self
            banner.dataSource = self
            
            self.phoneTextField.addTarget(self, action: #selector(RechargeHeaderView.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
            self.btn.addTarget(self, action: #selector(RechargeHeaderView.btnOnClick), for: UIControlEvents.touchUpInside)
            
            phoneTextField.delegate = self
            
            let lineView = UIView()
            lineView.backgroundColor = UIColor(rgba: Constant.common_separatLine_color)
            
            let grayView = BaseUnitView(frame:CGRect(x: 0,y: banner.ibottom,width: self.iwidth,height: MARGIN_4))
            grayView.backgroundColor = UIColor(rgba: Constant.common_separatLine_color)
            grayView.edgLineModes = [EdgLineMode.topLongLine,EdgLineMode.bottomLongLine]
            self.addSubview(grayView)
            
            let _ = [banner,grayView,phoneTextField,btn,tipeLbl,lineView].map{self.addSubview($0)}
            
            phoneTextField.iy = grayView.ibottom
            phoneTextField.ix = MARGIN_8 * 2
            
            btn.iright = self.iwidth - MARGIN_8 * 2
            btn.icenterY = phoneTextField.icenterY - 2
            
            tipeLbl.ix = phoneTextField.ix
            tipeLbl.iy = phoneTextField.ibottom - MARGIN_4
            
            lineView.frame = CGRect(x: 0, y: self.iheight - LINE_HEIGHT, width: self.iwidth, height: LINE_HEIGHT)
            
            setPhoneNumber("")
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    
    
    //MARK:CollectionView 的 footer
    class RechargeFooterView: UIView,CouponUnitViewDelegate {
        
        static let height:CGFloat =  (44 + 30) * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE
        
        weak var viewController:UIViewController?{didSet{
            unitCoupon.viewController = viewController
            }}
        
        static let reuseIdentifier = "RechargeFooterView_reuseIdentifier"
        
        //0：话费充值 1：流量充值
        fileprivate var type = 0
        
        //红包
        fileprivate let unitCoupon:CouponUnitView = {
            let couponView = CouponUnitView(true, title: "红包")
            couponView.iheight = 44 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE
            couponView.iwidth = Constant.ScreenSizeV1.SCREEN_WIDTH - MARGIN_8 * 2
            couponView.setTipeWithCouponCount(0)
            
            return couponView
        }()
        
        //提示
        fileprivate lazy var tipeLbl:UILabel = {
            let lbl = UILabel()
            lbl.textColor = UIColor(rgba: Constant.common_C8_color)
            lbl.font = Constant.Theme.Font_13
            lbl.frame = CGRect(x: MARGIN_8 * 2, y: 0, width: self.iwidth - MARGIN_8 * 4, height: 20)
            lbl.isHidden = true
            return lbl
        }()
        
        let lineView:UIView = {
            let lineView = UIView()
            lineView.layer.borderWidth = 0.5;
            lineView.backgroundColor = UIColor.white
            lineView.layer.borderColor = UIColor(rgba: Constant.common_separatLine_color).cgColor
            return lineView
        }()
        
        fileprivate var chooseBlock:((_ coupon:CouponModel)->Void)!
        
        init(frame: CGRect,type:Int){
            super.init(frame: frame)
            self.type = type
            setupUI()
        }
        
        
        //OUTER：设置当选中红包时的操作
        func setChooseCouponAction(_ chooseBlock:@escaping ((_ coupon:CouponModel)->Void)){
            self.chooseBlock = chooseBlock
        }
        
        
        //OUTER：传进来数据模型，请求红包数据,如果传进来的空数据，显示无可用红包，非空数据，请求红包数据
        func reloadCoupon(_ chargeModel:RechargeModel?,phoneNumber:String){
            if let model = chargeModel{
                unitCoupon.loadChargeCouponData(phoneNumber, chargeId: model.id)
            }else{
                unitCoupon.setTipeWithCouponCount(0)
            }
        }
        
        //OUTER：设置选择的红包
        func setSelectedCoupon(_ coupon:CouponModel?){
            self.unitCoupon.setSelectedCoupon(coupon)
        }
        
        
        
        //OUTER: 设置提示语
        func setTipes(_ tipe:String?){
            
            lineView.iy = 0
            
            if tipe == nil{
                self.tipeLbl.isHidden = true
                return
            }
            
            if tipe!.characters.count > 0{
                self.tipeLbl.text = tipe
                self.tipeLbl.isHidden = false
                lineView.iy = self.iheight - lineView.iheight
            }
        }
        
        func couponUnitViewDidSelectedCoupon(_ coupon:CouponModel){
            if self.chooseBlock != nil{
                self.chooseBlock(coupon)
            }
        }
        
        
        fileprivate func setupUI(){
            
            self.isUserInteractionEnabled = true
            lineView.frame = CGRect(x: -1, y: 0, width: self.iwidth + 2, height: unitCoupon.iheight + 1)
            
            self.addSubview(lineView)
            lineView.addSubview(unitCoupon)
            
            let tempView = UIView(frame: CGRect(x: 0,y: lineView.iheight,width: self.iwidth,height: Constant.ScreenSizeV1.SCREEN_HEIGHT))
            tempView.backgroundColor = UIColor(rgba: Constant.common_background_color)
            lineView.addSubview(tempView)
            
            unitCoupon.delegate = self
            unitCoupon.ix = MARGIN_8 + 1
            unitCoupon.iy = LINE_HEIGHT
            
            
            self.addSubview(self.tipeLbl)
            
            self.addSubview(self.tipeLbl)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}


//MARK: BaseRechargeView 的 delegate
protocol BaseRechargeViewDelegate:NSObjectProtocol {
    
    //选择了某个面值card
    func rechargeViewCardsDidselected(_ rechargeView:BaseRechargeView,rechargeModel:RechargeModel,phoneNumber:String)
    
    //选择了红包
    func rechargeViewCouponDidselected(_ rechargeView:BaseRechargeView,couponModel:CouponModel,phoneNumber:String)
    
    //改变了手机号码 此时应该把总金额置为0，结算按钮置灰
    func rechargeViewCouponDidchangePhoneNumber(_ rechargeView:BaseRechargeView)
}
