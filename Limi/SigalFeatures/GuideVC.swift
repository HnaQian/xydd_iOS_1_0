//
//  GuideVC.swift
//  Guide+Banner
//
//  Created by Mac OS on 15/10/23.
//  Copyright © 2015年 LJ. All rights reserved.
//

import UIKit
import CoreData

private let navHeight = 64

class GuideVC: BaseViewController ,UIScrollViewDelegate{

    var startClosure:(() -> Void)?
    
    var scrollView: UIScrollView!
    var pageControl: UIPageControl!
    
    var mutableArray: NSMutableArray?
    let navH = CGFloat(navHeight)
    
    fileprivate var _bottomWhiteView: UIView?
    fileprivate var _oldUserButton: UIButton?
    fileprivate var _newUserButton: UIButton?
    fileprivate var _bottomVerView: UIView?

    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        UIApplication.shared.statusBarStyle = .default
        self.navigationController?.navigationBar.isHidden = false
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        UIApplication.shared.statusBarStyle = .lightContent
        
    }
    
    var bottomWhiteView: UIView {
        get {
            if _bottomWhiteView == nil {
                _bottomWhiteView = UIView()
                _bottomWhiteView!.backgroundColor = UIColor.white
                _bottomWhiteView!.alpha = 0.9
            }
            return _bottomWhiteView!
        }
    }
    var oldUserButton: UIButton {
        get {
            if _oldUserButton == nil {
                _oldUserButton = UIButton(type: .custom)
                _oldUserButton!.setTitle("登录/注册", for: UIControlState())
                _oldUserButton!.setTitleColor(Constant.Theme.Color2, for: UIControlState())
                _oldUserButton!.titleLabel?.font = UIFont.systemFont(ofSize: Constant.common_F2_font)
                _oldUserButton!.addTarget(self, action: #selector(GuideVC.didTapOldUser), for: .touchUpInside)
            }
            return _oldUserButton!
        }
    }
    
    var newUserButton: UIButton {
        get {
            if _newUserButton == nil {
                _newUserButton = UIButton(type: .custom)
                _newUserButton?.layer.borderColor = UIColor(rgba: "#42ae3c").cgColor
                _newUserButton?.layer.borderWidth = 1 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
                _newUserButton?.layer.cornerRadius = 5 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
                _newUserButton?.setImage(UIImage(named: "guide_wechat"), for: UIControlState())
                _newUserButton?.titleEdgeInsets = UIEdgeInsetsMake(0,-(_newUserButton?.titleLabel?.frame)!.maxX + 12 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE,0, 0)
                _newUserButton?.imageEdgeInsets = UIEdgeInsetsMake(0, (_newUserButton?.titleLabel?.frame)!.maxX , 0, 0)
                _newUserButton!.setTitle("使用微信登录", for: UIControlState())
                _newUserButton!.setTitleColor(UIColor(rgba:"#42ae3c"), for: UIControlState())
                _newUserButton!.titleLabel?.font = UIFont.systemFont(ofSize: Constant.common_F2_font)
                _newUserButton!.addTarget(self, action: #selector(GuideVC.didTapNewUser), for: .touchUpInside)
            }
            return _newUserButton!
        }
    }

    fileprivate let ignoreButton:UIButton = {
        let btn = UIButton()
        btn.backgroundColor = UIColor.white
        btn.setTitle("跳过", for: UIControlState())
        btn.layer.cornerRadius = 2.0
        btn.setTitleColor(UIColor(rgba:Constant.common_C6_color), for: UIControlState())
        btn.titleLabel?.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        return btn
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DevelopTestManager.refreshDevelopeTipe()
        self.navigationItem.hidesBackButton = true
        
        createInitView()
        ignoreButton.addTarget(self, action: #selector(GuideVC.ignoreBtnOnclicked), for: .touchUpInside)
        self.view.addSubview(ignoreButton)
        self.view.addSubview(self.bottomWhiteView)
        self.bottomWhiteView.addSubview(self.oldUserButton)
        self.bottomWhiteView.addSubview(self.newUserButton)
        
        let scale = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        ignoreButton.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(self.view).offset(50 * scale)
            let _ = make.right.equalTo(self.view).offset(-30 * scale)
            let _ = make.width.equalTo(100 * scale)
            let _ = make.height.equalTo(50 * scale)
        }
        
        pageControl.snp_makeConstraints { (make) -> Void in
            let _ = make.centerX.equalTo(self.view)
            let _ = make.top.equalTo(self.view).offset(25)
           
            let _ = make.width.equalTo(40)
            let _ = make.height.equalTo(20)
        }
        self.bottomWhiteView.snp_makeConstraints { (make) -> Void in
            let _ = make.left.right.bottom.equalTo(self.view)
            let _ = make.height.equalTo(140 * scale)
        }
        self.oldUserButton.snp_makeConstraints { (make) -> Void in
            let _ = make.centerY.equalTo(self.bottomWhiteView)
            let _ = make.height.equalTo(self.bottomWhiteView)
            let _ = make.left.equalTo(self.bottomWhiteView)
            let _ = make.width.equalTo(250 * scale)
        }
        self.newUserButton.snp_makeConstraints { (make) -> Void in
            let _ = make.centerY.equalTo(self.bottomWhiteView)
            let _ = make.right.equalTo(self.bottomWhiteView).offset(-30 * scale)
            let _ = make.width.equalTo(470 * scale)
            let _ = make.height.equalTo(88 * scale)
        }
    }

    func createInitView() {
        
        let arr = ["splash01.jpg","splash02.jpg","splash03.jpg","splash04.jpg"]
        
        if self.scrollView == nil {
            self.scrollView = UIScrollView.init(frame: Constant.ScreenSize.SCREEN_BOUNDS)
            self.scrollView.delegate = self
            self.scrollView.isPagingEnabled = true
            self.scrollView.showsHorizontalScrollIndicator = false
            self.scrollView.showsVerticalScrollIndicator = false
            self.view.addSubview(self.scrollView)
            
            self.scrollView.contentSize = CGSize(width: Constant.ScreenSize.SCREEN_WIDTH * CGFloat(Float(arr.count)), height: Constant.ScreenSize.SCREEN_HEIGHT)
            
            for i in 0  ..< arr.count  {
                let index = CGFloat(i)
                let imgView = UIImageView.init(frame: CGRect(x: index * Constant.ScreenSize.SCREEN_WIDTH, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: Constant.ScreenSize.SCREEN_HEIGHT))
                imgView.image = UIImage(named: arr[i])
                self.scrollView.addSubview(imgView)
            }
        }
        
        if self.pageControl == nil {
            self.pageControl = UIPageControl()
            self.pageControl.numberOfPages = arr.count
            self.view.addSubview(self.pageControl)
            self.pageControl.pageIndicatorTintColor = UIColor(rgba: "#d2d2d2")
            self.pageControl.currentPageIndicatorTintColor = UIColor(rgba: Constant.common_red_color)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let index = scrollView.contentOffset.x / Constant.ScreenSize.SCREEN_WIDTH
        self.pageControl.currentPage = Int(index)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension GuideVC {
    
    func didTapOldUser() {
        UserDefaults.standard.set(true, forKey: "everLaunched")
        UserDefaults.standard.synchronize()
        let login = LoginViewController()
        login.isOutLogin = true
        self.navigationController?.pushViewController(login, animated: true)
    }
    
    func didTapNewUser() {
        UserDefaults.standard.set(true, forKey: "everLaunched")
        UserDefaults.standard.synchronize()
        loginWithWechat()
    }
    
    func ignoreBtnOnclicked() {
        UserDefaults.standard.set(true, forKey: "everLaunched")
        UserDefaults.standard.synchronize()
         NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.show_mainTabBarController), object: nil)
    }
    
    //微信账号登录
    func loginWithWechat()
    {
        let wechatAuth : LDSDKWXServiceImpl = LDSDKManager.getAuthService(LDSDKPlatformType.weChat) as! LDSDKWXServiceImpl
        
        wechatAuth.loginToPlatform (callback: {(oauthInfo,userInfoData,error) in
            if error == nil{
                if (userInfoData != nil && oauthInfo != nil) {
                    let json = (userInfoData as Any) as! NSDictionary
                    
                    // 保存微信内获取到的 用户昵称
                    let request: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
                    let datasource : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(request)!
                    if datasource.count > 0{
                        
                        let userInfo = datasource[0] as! User
                        if let headImageUrl = json.object(forKey: "headimgurl") as? String{
                            userInfo.wx_head_image_url = headImageUrl
                        }
                        if let nickName = json.object(forKey: "nickname") as? String{
                            userInfo.qq_nickName = nickName
                            if userInfo.nick_name == nil || userInfo.nick_name == ""{
                                userInfo.nick_name = nickName
                            }
                        }
                        if let gender = json.object(forKey: "sex") as? Int{
                            if userInfo.gender == 0{
                                userInfo.gender = Int64(gender)
                            }
                        }
                    }
                    else{
                        let entity = NSEntityDescription.entity(forEntityName: "User", in: CoreDataManager.shared.managedObjectContext)
                        request.entity = entity
                        do {
                            try! CoreDataManager.shared.managedObjectContext.fetch(request)
                            let user: User = NSEntityDescription.insertNewObject(forEntityName: "User", into: CoreDataManager.shared.managedObjectContext) as! User
                            if let headImageUrl = json.object(forKey: "headimgurl") as? String{
                                user.wx_head_image_url = headImageUrl
                            }
                            if let nickName = json.object(forKey: "nickname") as? String{
                                user.qq_nickName = nickName
                                if user.nick_name == nil || user.nick_name == ""{
                                    user.nick_name = nickName
                                }
                            }
                            if let gender = json.object(forKey: "sex") as? Int{
                                user.gender = Int64(gender)
                            }
                        }
                    }
                    CoreDataManager.shared.save()
                    let authInfo = (oauthInfo as Any) as! NSDictionary
                    self.getUserInfoWithOpen((authInfo.object(forKey: "openid") as? String)!,sns_token: (authInfo.object(forKey: "access_token"))! as! String, type: 3)
                }
                
            }
        }
      )
    }
    
    func getUserInfoWithOpen(_ openId : String,sns_token:String,type : Int){
        var params = [String: AnyObject]()
        params["open_id"] = openId as AnyObject?
        params["type"] = type as AnyObject?
        params["app_type"] = 1 as AnyObject?
        params["sns_token"] = sns_token as AnyObject?
        let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.login_open, parameters: params, isNeedAppTokrn: true, isShowErrorStatuMsg: true, isNeedHud: true,successHandler: {data, status, msg in
            print(data)
            if status == 201{
//                dew
//                let unvc = OpenBindPhoneViewController()
//                unvc.bindType = type == 2 ? Constant.OpenLoginType.qq.rawValue : Constant.OpenLoginType.wx.rawValue
//                unvc.isOutLogin = false
//                unvc.openId = openId
//                unvc.accessToken = sns_token
//                self.navigationController?.pushViewController(unvc, animated: true)
                
                
                let loginWithWechatVC = LoginWithWechatViewController()
                loginWithWechatVC.hidesBottomBarWhenPushed = true
                loginWithWechatVC.open_id = openId
                loginWithWechatVC.accessToken = sns_token
                loginWithWechatVC.isOutLogin = true
                self.navigationController?.pushViewController(loginWithWechatVC, animated: true)
            }
            else if status == 200{
               NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.show_mainTabBarController), object: nil)
            }
            
        })
        
    }
    
}
