//
//  SearchTermsView.swift
//  Limi
//
//  Created by maohs on 16/7/26.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//搜索条件
import UIKit

@objc protocol SearchTermsViewDelegate:NSObjectProtocol {
    @objc optional func searchTermsViewClicked(_ title:String,type:Int)
}

class SearchTermsView: UIView {
    
    weak var delegate:SearchTermsViewDelegate?
    fileprivate let grayLine:UIView = {
       let line = UIView()
        line.backgroundColor = UIColor(rgba: Constant.common_separatLine_color)
        
        return line
    }()
    
    fileprivate let separateLine:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(rgba: Constant.common_separatLine_color)
        
        return view
    }()
    
    var goodsArrayValues = ["r","s","p_asc","select"]
    var articleArrayValue = ["r","v","s","p_asc"]
    
    let goodsArrayKeys:NSArray = ["综合","销量","价格","筛选"]
    let articleArrayKeys:NSArray = ["综合","阅读数","分享数","发布时间"]
    fileprivate var firstButton:UIButton?
    fileprivate var filterButton:UIButton?
    fileprivate var filterButtonClicked:Bool = false
    
    fileprivate var priceButton = UIButton()
    fileprivate var publishedButton = UIButton()
    
    fileprivate var isPriceAsc:Bool = true
    fileprivate var isPublishAsc:Bool = true
    
    fileprivate var type = 0
    //type: 1商品 2攻略
    init(frame: CGRect,type:Int) {
        super.init(frame: frame)
        self.type = type
        setUpUI()
    }
    
    fileprivate func setUpUI() {
        self.backgroundColor = UIColor.white
        self.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: 50 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE)
        grayLine.frame = CGRect(x: 0, y: self.iheight, width: self.iwidth, height: 1)
        
        self.addSubview(grayLine)
        
        if self.type == 1 {
            resetUI(goodsArrayKeys as! [String])
        }else {
            resetUI(articleArrayKeys as! [String])
        }
    }
    
    fileprivate func resetUI(_ data:[String]) {
        var lastButtonLocation:CGFloat = 0
        var width:CGFloat = 0
        if data.count > 0 {
            width = Constant.ScreenSizeV2.SCREEN_WIDTH / CGFloat(data.count)
        }
        for index in 0 ..< data.count {
            let button = customButton(data[index])
            button.frame = CGRect(x: lastButtonLocation, y: 0, width: width, height: self.iheight)
            lastButtonLocation += button.iwidth
            if index == 0 {
                button.setTitleColor(UIColor(rgba: Constant.common_C1_color), for: UIControlState())
            }
            if index == 0 {
                firstButton = button
            }
            
            if data[index] == "价格" {
                
                priceButton = button
                priceButton.setImage(UIImage(named: "arrow_default"), for: UIControlState())
                priceButton.titleEdgeInsets = UIEdgeInsetsMake(0, -30, 0, 0)
                priceButton.imageEdgeInsets = UIEdgeInsetsMake(0,35 + 20 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE, 0, 0)

            }
            
            if data[index] == "发布时间" {
                
                publishedButton = button
                publishedButton.setImage(UIImage(named: "arrow_default"), for: UIControlState())
                publishedButton.titleEdgeInsets = UIEdgeInsetsMake(0, -30, 0, 0)
                publishedButton.imageEdgeInsets = UIEdgeInsetsMake(0, 60 + Constant.ScreenSizeV2.MARGIN_4, 0, 0)

            }
            
            if data[index] == "筛选" {
                separateLine.frame = CGRect(x: button.ileft, y: 0, width: 1, height: button.iheight/2)
                separateLine.icenterY = button.icenterY
                self.addSubview(separateLine)
                button.setImage(UIImage(named: "filternormal"), for: UIControlState())
                filterButton = button
            }
            
            self.addSubview(button)
        }
    }
    
    fileprivate func customButton(_ title:String) -> UIButton {
        let button = UIButton(type: UIButtonType.custom)
        button.clipsToBounds = true
        
        button.tag = self.type
        button.setTitle(title, for: UIControlState())
        button.setTitleColor(UIColor(rgba:Constant.common_C6_color), for: UIControlState())
        button.setTitleColor(UIColor(rgba:Constant.common_C1_color), for: .selected)
        button.titleLabel?.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        button.addTarget(self, action: #selector(SearchTermsView.generalButtonClicked(_:)), for: .touchUpInside)
        
        return button
    }
    
    func generalButtonClicked(_ sender:UIButton) {
        ///规则：
        //1.除了"筛选"其余按钮都是互斥的
        //2.点击了"筛选"则"筛选"一直高亮，除非切换
        //3."筛选"不影响其余按钮的状态
        
        let title = (sender.titleLabel?.text)!
        
        if title != "筛选" {
            for view in self.subviews {
                if view.tag == self.type {
                    (view as! UIButton).setTitleColor(UIColor(rgba: Constant.common_C6_color), for: UIControlState())
                }
            }
        }
       
        var index = 0
        var value = ""
        if self.type == 1 {
            index = goodsArrayKeys.index(of: title)
            value = goodsArrayValues[index]
        }else {
            index = articleArrayKeys.index(of: title)
            value = articleArrayValue[index]
        }
        if title == "价格" {
            if !isPriceAsc {
                //降序
                value = "p_desc"
                priceButton.setImage(UIImage(named: "arrow_down"), for: UIControlState())
                
            }else {
                priceButton.setImage(UIImage(named: "arrow_up"), for: UIControlState())
            }
            isPriceAsc = !isPriceAsc
        }else if title == "筛选" {
            //不改变状态
        }else {
            isPriceAsc = true
            priceButton.setImage(UIImage(named: "arrow_default"), for: UIControlState())
        }
        
        if title == "发布时间" {
            if !isPublishAsc {
                //降序
                value = "p_desc"
                publishedButton.setImage(UIImage(named: "arrow_down"), for: UIControlState())
            }else {
                publishedButton.setImage(UIImage(named: "arrow_up"), for: UIControlState())
            }
            isPublishAsc = !isPublishAsc
        }else {
            isPublishAsc = true
            publishedButton.setImage(UIImage(named: "arrow_default"), for: UIControlState())
        }
        
        if title == "筛选" {
           filterButtonClicked = true
            sender.setImage(UIImage(named: "filterselected"), for: UIControlState())
    
        }else {
            if filterButtonClicked {
                filterButton?.setTitleColor(UIColor(rgba: Constant.common_C1_color), for: UIControlState())
                filterButton?.setImage(UIImage(named: "filterselected"), for: UIControlState())
            }else {
                filterButton?.setImage(UIImage(named: "filternormal"), for: UIControlState())
            }
            
        }
        sender.setTitleColor(UIColor(rgba: Constant.common_C1_color), for: UIControlState())
    
        delegate?.searchTermsViewClicked!(value, type: self.type)
    }
    
    func changeFistButtonColor() {
        for view in self.subviews {
            if view.tag == self.type {
                (view as! UIButton).setTitleColor(UIColor(rgba: Constant.common_C6_color), for: UIControlState())
            }
        }
        isPublishAsc = true
        isPriceAsc = true
        filterButtonClicked = false
        firstButton!.setTitleColor(UIColor(rgba: Constant.common_C1_color), for: UIControlState())
        
        priceButton.setImage(UIImage(named: "arrow_default"), for: UIControlState())
        publishedButton.setImage(UIImage(named: "arrow_default"), for: UIControlState())
        filterButton?.setImage(UIImage(named: "filternormal"), for: UIControlState())
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
