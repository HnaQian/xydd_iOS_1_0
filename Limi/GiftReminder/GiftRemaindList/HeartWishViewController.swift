//
//  HeartWishViewController.swift
//  Limi
//
//  Created by guo chen on 15/12/8.
//  Copyright © 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//心意祝福首页

import UIKit
import ContactsUI

///送礼提醒类型
/**
Holiday     :节假日送礼提醒

Personal    :个人送礼提醒
*/
///
public enum PresentType:Int
{
    case holiday//节日
    
    case personal//个人
}

private var limitMessageId = 0

private let redColor = UIColor(rgba: Constant.common_red_color)

private let separatLineColor = UIColor(rgba: Constant.common_separatLine_color)

///节日信息 模块内全局变量
private var limitPresentInfo:JSON!

private var limitData:JSON!

///节日类型 模块内全局变量
private var limitPresentType:PresentType!

///发送类型 模块内全局变量
private var limitSendMsgType:SendMsgType!

///当前类对象 模块内全局变量
private var limitHeartWishVC:HeartWishViewController!

class HeartWishViewController: BaseViewController
{
    fileprivate var headView:HeadView!//主题视图
    
    fileprivate var changeSendModeView:ChangeSendModeView!//切换发送渠道视图
    
    fileprivate var sendMessageContentView:SendMessageContentView!//发送的信息内容
    
    fileprivate let lastBtn = CustomButton(type: 1)//上一个
    fileprivate let nextbtn = CustomButton(type: 2)//下一个
    
    fileprivate let contentView = UIView()
    
    fileprivate let sendMessageButton = UIButton(type: .system)//发送信息按钮
    
    fileprivate let sendManager = SendMsgManager()
    
    fileprivate let changeWishWordAlertMsg = "切换到别的祝福语模板，会丢失之前编辑的内容，是否确定要切换？"
    
    fileprivate let existAlertMsg = "退出当前界面，会丢失之前编辑的内容，是否确定要切换"
    
    let titleView = CustomLabel(font: 19, textColorHex: "#ffffff")
    
    
    class func newHeartWishViewController(_ presentInfo:JSON?,presentType:PresentType)-> HeartWishViewController
    {
        let heartVC = HeartWishViewController()
        
        if presentInfo != nil
        {
            limitHeartWishVC = heartVC
            
            limitPresentInfo = presentInfo
            
            limitPresentType = presentType
            
            limitSendMsgType = .weChat
            
        }else
        {
            heartVC.backItemAction()
        }
        return heartVC
    }
    
    
    
    override func loadView()
    {
        super.loadView()
        
        let scrollView = UIScrollView(frame: Constant.ScreenSize.SCREEN_BOUNDS)
        
        scrollView.contentSize = CGSize(width: Constant.ScreenSize.SCREEN_WIDTH,height: Constant.ScreenSize.SCREEN_HEIGHT)
        scrollView.backgroundColor = UIColor.red
        view = scrollView
    }
    
    
    override func viewDidLoad()
    {
        if limitPresentType == PresentType.holiday{

            if let id:Int = limitPresentInfo["Id"].int{
                Statistics.count(Statistics.GiftRemind.remind_wish_click, andAttributes: ["gid":"\(id)"])
            }
        }
        
        super.viewDidLoad()
        //关闭手势返回
        self.gesturBackEnable = false
        
        if limitPresentType == PresentType.personal
        {
            titleView.textAlignment = NSTextAlignment.center
            titleView.frame = CGRect(x: 0, y: 0, width: 120, height: 30)
            titleView.text = "心意祝福"
            
            self.navigationItem.titleView = titleView
        }else
        {
            self.navigationItem.title = ""
        }
        
        self.view.addSubview(contentView)
        contentView.snp_makeConstraints { (make) -> Void in
            let _ = make.left.equalTo(self.view)
            let _ = make.right.equalTo(self.view)
            let _ = make.top.equalTo(self.view)
            let _ = make.bottom.equalTo(self.view)
            let _ = make.width.equalTo(Constant.ScreenSize.SCREEN_WIDTH)
           let _ =  make.height.equalTo(Constant.ScreenSize.SCREEN_HEIGHT)
        }

        self.loadWishList()
    }
    
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        self.navigationController?.navigationBar.lt_setBackgroundColor(UIColor.clear)
        NotificationCenter.default.addObserver(self, selector: #selector(HeartWishViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(HeartWishViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        self.navigationController?.navigationBar.lt_reset()
        
        self.navigationController?.navigationBar.lt_setElementsAlpha(1)
        
        updateNaviBarColor()
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    
    
    func keyboardWillShow(_ notification: Notification?)
    {
        limitHeartWishVC.titleView.textColor = UIColor.black
        
        limitHeartWishVC.navigationController?.navigationBar.lt_reset()
        
        limitHeartWishVC.navigationController?.navigationBar.lt_setElementsAlpha(1)
        
        limitHeartWishVC.updateNaviBarColor()
    }
    
    func keyboardWillHide(_ notification: Notification?)
    {
        limitHeartWishVC.titleView.textColor = UIColor.white
        
        limitHeartWishVC.navigationController?.navigationBar.shadowImage = UIImage()
        
        limitHeartWishVC.navigationController?.navigationBar.lt_setBackgroundColor(UIColor.clear)
    }
    
    
    
    //MARK:加载祝福语
    func loadWishList()
    {
        var param = [String:AnyObject]()
        
        param["type"] = limitPresentType == .personal ? 1 as AnyObject? : 2 as AnyObject?
        
        param["id"] = limitPresentInfo["Id"].intValue as AnyObject?
        
        let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.bless_list, parameters:param, isNeedUserTokrn: true, isShowErrorStatuMsg: true,successHandler: {[weak self](data,status,msg) in
        
            if status != 200
            {
                return
            }
            
            let subData = JSON(data["data"]! as AnyObject)
            
            limitData = subData
            
            if limitPresentType == .personal
            {
                limitPresentInfo = subData["user_relation"]
            }else
            {
                limitPresentInfo = subData["holiday"]
            }
            
            self!.setupUI()
            
            if let list = subData["list"].array
            {
                if list.count > 0
                {
                    self!.sendMessageContentView.wishList = list
                    
                    self!.sendMessageContentView.wishContent = list[0]["Content"].string!
                }
            }
            
            if limitPresentType == .personal
            {
                if let senceName = subData["name"].string
                {
                    self!.headView.personalView.senceNameLabel.text = senceName
                }
            }
            
            self!.headView.headBgImageName = subData["background_img"].string
            })
    }
  
    
    //MARK:切换祝福语
    func changWishWord(_ btn:UIButton)
    {
        sendMessageContentView.changeDirection = btn == lastBtn ? 1 : 2
        
        if sendMessageContentView.wishContentDidChange
        {
            LCActionSheet(title: changeWishWordAlertMsg, buttonTitles: ["放弃编辑"], redButtonIndex: -1, clicked: { (buttonIndex) -> Void in
                
                if buttonIndex == 1
                {
                    self.sendMessageContentView.changeWishWord()
                }
                
            }).show()
        }else
        {
            sendMessageContentView.changeWishWord()
        }
    }
    
    
    
    //MARK:发送信息
    func sendMessageBtnAction()
    {
        //if UserInfoManager.didLogin == false{return}
        
        let tempContent = self.sendMessageContentView.wishContent
        
        if let content = tempContent
        {
            if content.characters.count < 1
            {
                Utils.showError(context: self.view, errorStr: "你还没有填写祝福语哦")
                return
            }
        }else
        {
            Utils.showError(context: self.view, errorStr: "你还没有填写祝福语哦")
            return
        }
        
        
        self.sendManager.heartWishVC = self
        
        let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.bless_send, parameters: ["id":limitMessageId as AnyObject],isNeedUserTokrn: true, isShowErrorStatuMsg:true)
        
        if limitSendMsgType == SendMsgType.weChat
        {
            self.sendManager.sendMessage(.weChat, message: tempContent!)
        }else
        {
            self.sendManager.sendMessage(.message, message: tempContent!, mobile: sendMessageContentView.wishPeopleMobile)
        }
    }
    
    
    //MARK:返回操作
    override func backItemAction()
    {
        
        if sendMessageContentView != nil
        {
            if sendMessageContentView.wechatView.wechatContentField.isFirstResponder{
                sendMessageContentView.wechatView.wechatContentField.resignFirstResponder()
            }
            
            if sendMessageContentView.messageView.messageContentField.isFirstResponder{
                sendMessageContentView.messageView.messageContentField.resignFirstResponder()
            }
            
            if sendMessageContentView.wishContentDidChange
            {
                LCActionSheet(title: existAlertMsg, buttonTitles: ["放弃编辑"], redButtonIndex: -1, clicked: { (buttonIndex) -> Void in
                    
                    if buttonIndex == 1
                    {
                        let _ = self.navigationController?.popViewController(animated: true)
                        
                        self.cusDinit()
                    }
                    
                }).show()
                
            }else
            {
                let _ = self.navigationController?.popViewController(animated: true)
                
                self.cusDinit()
            }
            
        }else
        {
            let _ = self.navigationController?.popViewController(animated: true)
            
            self.cusDinit()
        }
    }
    
    
    
    
    //MARK:界面布局
    func setupUI()
    {
        headView = HeadView()
        
        sendMessageContentView = SendMessageContentView()
        
        changeSendModeView = ChangeSendModeView(sendMessageContentView: sendMessageContentView)
        
        changeSendModeView.sendMessageButton = sendMessageButton
        
        lastBtn.addTarget(self, action: #selector(HeartWishViewController.changWishWord(_:)), for: UIControlEvents.touchUpInside)
        
        nextbtn.addTarget(self, action: #selector(HeartWishViewController.changWishWord(_:)), for: UIControlEvents.touchUpInside)
        
        sendMessageButton.addTarget(self, action: #selector(HeartWishViewController.sendMessageBtnAction), for: UIControlEvents.touchUpInside)
        
        sendMessageButton.setTitle("微信发送", for: UIControlState())
        
        sendMessageButton.setTitleColor(UIColor(rgba: Constant.common_C12_color), for: UIControlState())
        
        sendMessageButton.titleLabel?.font = UIFont.systemFont(ofSize: Constant.common_F2_font)
        
        sendMessageButton.backgroundColor = UIColor(rgba: Constant.common_C1_color)
        
        
        for view in[headView,sendMessageContentView,changeSendModeView,lastBtn,nextbtn,sendMessageButton] as [Any]
        {
            contentView.addSubview(view as! UIView)
        }
        self.headView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.view)
            let _ = make.width.equalTo(Constant.ScreenSize.SCREEN_WIDTH)
            let _ = make.left.equalTo(self.view)
            
            if limitPresentType == .personal
            {
                let _ = make.height.equalTo(175)
            }else
            {
                let _ = make.height.equalTo(Constant.ScreenSize.SCREEN_HEIGHT * 0.28)
            }
        }
        
        
        changeSendModeView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(headView.snp_bottom)
            let _ = make.left.equalTo(self.view)
            let _ = make.width.equalTo(self.view)
        }
        
        self.sendMessageContentView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(changeSendModeView.snp_bottom)
            let _ = make.height.equalTo(self.view.snp_width).multipliedBy(0.8)
            let _ = make.left.equalTo(self.view)
            let _ = make.width.equalTo(self.view).multipliedBy(2)
        }
        
        lastBtn.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.sendMessageContentView.snp_bottom).offset(8)
            let _ = make.left.equalTo(self.sendMessageContentView)
            let _ = make.width.equalTo(80)
            let _ = make.height.equalTo(30)
        }
        
        nextbtn.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(lastBtn)
            let _ = make.right.equalTo(sendMessageContentView.snp_centerX)
            let _ = make.height.equalTo(lastBtn)
            let _ = make.width.equalTo(lastBtn)
        }
        
        sendMessageButton.snp_makeConstraints { (make) -> Void in
            let _ = make.bottom.equalTo(self.view)
            let _ = make.left.equalTo(self.view)
            let _ = make.right.equalTo(self.view)
            let _ = make.height.equalTo(45)
        }
        
    }
    
    
    
    ///销毁全局变量
    func cusDinit()
    {
        limitPresentInfo = nil
        
        limitPresentType = nil
        
        limitSendMsgType = SendMsgType.weChat
        
        limitHeartWishVC = nil
    }
    
    
    override func didReceiveMemoryWarning() {super.didReceiveMemoryWarning()}
}



//MARK:改变发送渠道视图
class ChangeSendModeView:UIView
{
    fileprivate var sendMessageContentView:SendMessageContentView!
    
    fileprivate var sendMessageButton:UIButton!
    
    fileprivate let wechatButton = UIButton()
    
    fileprivate let lineView = UIView()
    
    fileprivate let messageButton = UIButton()
    
    init(sendMessageContentView:SendMessageContentView)
    {
        super.init(frame: CGRect.zero)
        
        self.sendMessageContentView = sendMessageContentView
        
        self.changeSendModeViewSetupUI()
    }
    
    
    fileprivate let blackColor = UIColor(rgba:"#555555")
    
    func changeSendModeViewSetupUI()
    {
        self.backgroundColor = UIColor.white
        
        self.isUserInteractionEnabled = true
        
        for view in [wechatButton,lineView,messageButton]
        {
            self.addSubview(view)
        }
        
        
        wechatButton.setTitle("微信送祝福", for: UIControlState())
        wechatButton.setTitleColor(redColor, for: UIControlState())
        
        messageButton.setTitle("短信送祝福", for: UIControlState())
        messageButton.setTitleColor(blackColor, for: UIControlState())
        
        lineView.backgroundColor = redColor
        
        wechatButton.addTarget(self, action: #selector(ChangeSendModeView.switchSendType(_:)), for: UIControlEvents.touchUpInside)
        messageButton.addTarget(self, action: #selector(ChangeSendModeView.switchSendType(_:)), for: UIControlEvents.touchUpInside)
        
        wechatButton.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self)
            let _ = make.height.equalTo(35)
            let _ = make.left.equalTo(self)
            let _ = make.width.equalTo(self).multipliedBy(0.5)
        }

        messageButton.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(wechatButton)
            let _ = make.height.equalTo(wechatButton)
            let _ = make.left.equalTo(wechatButton.snp_right)
            let _ = make.width.equalTo(self).multipliedBy(0.5)
        }

        lineView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(wechatButton.snp_bottom)
            let _ = make.centerX.equalTo(wechatButton)
            let _ = make.height.equalTo(2)
            let _ = make.width.equalTo(90)
            let _ = make.bottom.equalTo(self)
        }

    }
    
    //MARK:改变发送信息渠道
    func switchSendType(_ btn:UIButton)
    {
        if btn == self.wechatButton
        {
            if limitSendMsgType != SendMsgType.weChat
            {
                self.sendMessageContentView.changeDisplayMode(SendMsgType.weChat)
                
                limitSendMsgType = SendMsgType.weChat
                
                UIView.animate(withDuration: 0.25, animations: { () -> Void in
                    self.lineView.transform = CGAffineTransform(translationX: self.wechatButton.ix, y: 0)
                }) 
                
                sendMessageButton.setTitle("微信发送", for: UIControlState())
                
                self.messageButton.setTitleColor(blackColor, for: UIControlState())
                
                self.wechatButton.setTitleColor(redColor, for: UIControlState())
            }
        }else
        {
            if limitSendMsgType != SendMsgType.message
            {
                self.sendMessageContentView.changeDisplayMode(SendMsgType.message)
                
                UIView.animate(withDuration: 0.25, animations: { () -> Void in
                    self.lineView.transform = CGAffineTransform(translationX: self.messageButton.ix, y: 0)
                }) 
                sendMessageButton.setTitle("短信发送", for: UIControlState())
                
                self.wechatButton.setTitleColor(blackColor, for: UIControlState())
                
                self.messageButton.setTitleColor(redColor, for: UIControlState())
                
                limitSendMsgType = SendMsgType.message
            }
        }
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}



//MARK:祝福的主题视图
class HeadView:UIView
{
    var personalView:PersonalView!
    
    fileprivate var holidayView:HolidayView!
    
    
    
    ///主题背景图片
    var headBgImageName:String?{
        didSet
        {
            if let img = headBgImageName
            {
                if img.characters.count > 0
                {
                    if limitPresentType == PresentType.holiday{
                        
                        let bgimage  = UIImage(named: "defaultBgImg_otherday")
                        
                        let scal_Value = Constant.ScreenSize.SCREEN_WIDTH / (bgimage?.size.width)!
                        
                        let scal_imageUrl = Utils.scaleImage(img, width: Constant.ScreenSize.SCREEN_WIDTH, height: (bgimage?.size.height)! * scal_Value)

                        self.holidayView.bgImageView.af_setImageWithURL(URL(string: scal_imageUrl)!, placeholderImage: UIImage(named: ""))
                    }else
                    {
                        let imageScale : String = img + "?imageView2/0/w/" + "\(Int(Constant.ScreenSize.SCREEN_WIDTH * 2))" + "/h/" + "\(Int(Constant.ScreenSize.SCREEN_WIDTH * 2))"
                        
                        self.personalView.bgImageView.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: ""))
                    }
                }
            }
        }}
    
    
    init ()
    {
        super.init(frame: CGRect.zero)
        
        self.clipsToBounds = true
        
        self.headViewSetupUI()
    }
    
    
    func headViewSetupUI()
    {
        var view:UIImageView!
        
        if limitPresentType == .personal
        {
            self.personalView = PersonalView()
            
            self.personalView.image = UIImage(named: "personal_common")

            view = self.personalView
        }else
        {
            self.holidayView = HolidayView()
            
            self.holidayView.image = UIImage(named: "holiday_common")
            
            view = self.holidayView
        }
        
        self.addSubview(view)
        view.contentMode = UIViewContentMode.scaleToFill
        view.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self)
            let _ = make.left.equalTo(self)
            let _ = make.right.equalTo(self)
            let _ = make.bottom.equalTo(self)
        }

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    //MARK:节假日主题
    class HolidayView:UIImageView
    {
        fileprivate let bgImageView = UIImageView()
        
        fileprivate var presentInfo:JSON?{didSet{}}//保留字段
        
        fileprivate let titleLabel = CustomLabel(font: 35, textColorHex: Constant.common_C12_color)//节日title
        
        fileprivate let dateContentView = UIView()//时间控件组
        
        fileprivate let gregorianLabel = CustomLabel(font: Constant.common_F5_font, textColorHex: Constant.common_C9_color)//公历的日期
        
        fileprivate let gregorianIcon = UIImageView()//公历的icon
        
        fileprivate let luarLabel = CustomLabel(font: Constant.common_F5_font, textColorHex: Constant.common_C9_color)//农历的日期
        
        fileprivate let luarIcon = UIImageView()//农历的icon
        
        fileprivate let restDaysLabel = CustomLabel(font: Constant.common_F1_font, textColorHex: Constant.common_C12_color)//剩余天数
        
        init()
        {
            super.init(frame:CGRect.zero)
            
            self.holidaySetupUI()
        }
        
        
        func holidaySetupUI()
        {
            gregorianIcon.contentMode = UIViewContentMode.scaleAspectFill
            luarIcon.contentMode = UIViewContentMode.scaleAspectFill
            
            for view in [gregorianLabel,gregorianIcon,luarLabel,luarIcon]{dateContentView.addSubview(view)}
            
            for view in [bgImageView,titleLabel,dateContentView,restDaysLabel]{self.addSubview(view)}
            
            let bgimage  = UIImage(named: "defaultBgImg_otherday")
            
            bgImageView.image = bgimage
            
            let scal_Value = Constant.ScreenSize.SCREEN_WIDTH / (bgimage?.size.width)!
            
            bgImageView.snp_makeConstraints { (make) -> Void in
                let _ = make.center.equalTo(self)
                let _ = make.height.equalTo((bgimage?.size.height)! * scal_Value)
                let _ = make.width.equalTo(Constant.ScreenSize.SCREEN_WIDTH)
            }

            titleLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.centerX.equalTo(self)
                let _ = make.centerY.equalTo(self).offset(-8)
                let _ = make.height.equalTo(50)
            }

            dateContentView.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(titleLabel.snp_bottom).offset(16)
                let _ = make.centerX.equalTo(titleLabel)
            }

            gregorianIcon.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(dateContentView)
                let _ = make.height.equalTo(gregorianIcon.snp_width)
                let _ = make.bottom.equalTo(dateContentView)
                let _ = make.width.equalTo(17)
            }

            gregorianLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(gregorianIcon)
                let _ = make.left.equalTo(gregorianIcon.snp_right).offset(8)
                let _ = make.height.equalTo(gregorianIcon)
                let _ = make.right.lessThanOrEqualTo(luarIcon.snp_left).offset(-16)
            }

            luarIcon.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(gregorianIcon)
                let _ = make.height.equalTo(gregorianIcon)
                let _ = make.width.equalTo(gregorianIcon)
                let _ = make.right.equalTo(luarLabel.snp_left).offset(-8)
            }

            luarLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(gregorianIcon)
                let _ = make.height.equalTo(gregorianIcon)
                let _ = make.right.lessThanOrEqualTo(dateContentView)
            }

            restDaysLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(dateContentView.snp_bottom).offset(8)
                let _ = make.centerX.equalTo(titleLabel)
                let _ = make.height.equalTo(30)
            }

            self.refreshData()
        }
        
        
        
        //填充数据
        func refreshData()
        {
            var timeStr = ""
            
            if let str = limitPresentInfo!["Gift_time"].string
            {
                if str.characters.count > 0
                {
                    timeStr = str
                }else
                {
                    timeStr = (limitPresentInfo["Date_time"].string)!
                }
            }else
            {
                timeStr = (limitPresentInfo["Date_time"].string)!
            }
            
            
            let dateType = limitPresentInfo["Date_type"].int
            
            
            let imageNameAry = NSMutableArray()
            
            imageNameAry.add("heartWish_gregorian_unsel")
            
            imageNameAry.add("heartWish_luar_unsel")
            
            if dateType == 2//农历
            {
                gregorianLabel.alpha = 0.5
                
                imageNameAry.replaceObject(at: 1, with: "heartWish_luar_sel")
                
                luarLabel.text = CalendarUtil.alTolunar(timeStr)//当年农历
                
                let gregoiranDate = CalendarUtil.calendarChange(timeStr, andWithType: 2)//今年的公历
                
                
                
                gregorianLabel.text = CalendarUtil.subString(CalendarUtil.dateDict(toString: gregoiranDate,andTargetDateType:0), withRang: NSMakeRange(5, 5))
            }
            else if dateType == 1//公历
            {
                luarLabel.alpha = 0.5
                
                imageNameAry.replaceObject(at: 0, with: "heartWish_gregorian_sel")
                
                gregorianLabel.text = CalendarUtil.subString(timeStr, withFrom: 5)//当年公历
                
                let gregoiranDate = CalendarUtil.calendarChange(timeStr, andWithType: 1)//今年的农历
                
                luarLabel.text = CalendarUtil.subString(CalendarUtil.dateDict(toString: gregoiranDate,andTargetDateType:1), withRang: NSMakeRange(3, 4))
            }
            else//公历 & 农历
            {
                imageNameAry.replaceObject(at: 0, with: "heartWish_gregorian_sel")
                
                imageNameAry.replaceObject(at: 1, with: "heartWish_luar_sel")
                
                
                gregorianLabel.text = CalendarUtil.subString(timeStr, withFrom: 4)
                
                luarLabel.text = CalendarUtil.alTolunar(timeStr)
            }
            
            luarIcon.image = UIImage(named:(imageNameAry[1] as? String)!)
            
            gregorianIcon.image = UIImage(named:(imageNameAry[0] as? String)!)
            
            titleLabel.text  = limitPresentInfo["Name"].string
            
            restDaysLabel.text = CalculateResetDays.returnString()
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    
    
    //MARK:个人主题
    class PersonalView:UIImageView
    {
        fileprivate let bgImageView = UIImageView()
        
        fileprivate var presentInfo:JSON?{didSet{}}//保留字段
        
        fileprivate let headIcon = UIImageView()//用户头像
        
        fileprivate let headIconBgView = UIView()//用户头像
        
        fileprivate let headIconLabel = CustomLabel()//用户头像姓名
        
        fileprivate let nameLabel = CustomLabel(font: Constant.common_F2_font,textColorHex: Constant.common_C12_color)//用户姓名
        
        fileprivate let senceNameLabel = CustomLabel(font:Constant.common_F5_font , textColorHex: Constant.common_C9_color)//场景名称
        
        fileprivate let restDaysLabel = CustomLabel(font: Constant.common_F1_font, textColorHex: Constant.common_C12_color)//剩余天数
        
        fileprivate let gregorianLabel = CustomLabel(font: Constant.common_F5_font, textColorHex: Constant.common_C9_color)//公历的日期
        
        fileprivate let gregorianIcon = UIImageView()//公历的icon
        
        fileprivate let luarLabel = CustomLabel(font: Constant.common_F5_font, textColorHex: Constant.common_C9_color)//农历的日期
        
        fileprivate let luarIcon = UIImageView()//农历的icon
        
        
        init()
        {
            super.init(frame:CGRect.zero)
            
            self.personalSetupUI()
        }
        
        func personalSetupUI()
        {
            gregorianIcon.contentMode = UIViewContentMode.scaleAspectFill

            luarIcon.contentMode = UIViewContentMode.scaleAspectFill
            
            let icon_width:CGFloat = 56
            
            headIcon.layer.cornerRadius = icon_width/2
            
            headIcon.clipsToBounds = true
            
//            headIconBgView.layer.cornerRadius = icon_width/2 + 2
            
//            headIconBgView.backgroundColor = UIColor.whiteColor()
            headIconBgView.alpha = 0
            
            headIconLabel.text = nil
            
            headIconLabel.layer.cornerRadius = icon_width/2
            
            headIconLabel.clipsToBounds = true
            
            headIconLabel.textColor = UIColor.white
            
            headIconLabel.textAlignment = NSTextAlignment.center
            
            headIconLabel.font = UIFont.systemFont(ofSize: 35)

            for view in [bgImageView,headIconBgView,headIcon,headIconLabel,nameLabel,senceNameLabel,restDaysLabel,gregorianLabel,gregorianIcon,luarLabel,luarIcon]
            {
                self.addSubview(view)
            }
            
            let bgimage  = UIImage(named: "defaultBgImg_otherday")
            
            let scal_Value = Constant.ScreenSize.SCREEN_WIDTH / (bgimage?.size.width)!
            
            bgImageView.snp_makeConstraints { (make) -> Void in
                let _ = make.center.equalTo(self)
                let _ = make.height.equalTo((bgimage?.size.height)! * scal_Value)
                let _ = make.width.equalTo(Constant.ScreenSize.SCREEN_WIDTH)
            }

            headIconBgView.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(self).offset(8)
                let _ = make.width.equalTo(self).offset(-8)
                let _ = make.width.equalTo(headIconBgView.snp_height)
                let _ = make.height.equalTo(icon_width + 4)
            }

            headIcon.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(self).offset(10)
                let _ = make.bottom.equalTo(self).offset(-10)
                let _ = make.width.equalTo(headIcon.snp_height)
                let _ = make.height.equalTo(icon_width)
            }

            headIconLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(headIcon)
                let _ = make.top.equalTo(headIcon)
               let _ =  make.width.equalTo(headIcon.snp_height)
                let _ = make.height.equalTo(icon_width)
                
            }
            
            
            nameLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(headIcon.snp_right).offset(10)
                let _ = make.top.equalTo(headIcon).offset(4)
                let _ = make.height.equalTo(20)
            }

            senceNameLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(nameLabel).offset(4)
                let _ = make.left.equalTo(nameLabel.snp_right).offset(8)
                let _ = make.height.equalTo(14)
            }

            
            gregorianIcon.snp_makeConstraints { (make) -> Void in
                let _ = make.bottom.equalTo(headIcon).offset(-4)
                let _ = make.left.equalTo(headIcon.snp_right).offset(10)
               let _ =  make.height.equalTo(gregorianIcon.snp_width)
                let _ = make.width.equalTo(17)
            }

            gregorianLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(gregorianIcon)
                let _ = make.left.equalTo(gregorianIcon.snp_right).offset(2)
                let _ = make.height.equalTo(gregorianIcon)
            }

            
            luarIcon.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(gregorianIcon)
                let _ = make.left.equalTo(gregorianLabel.snp_right).offset(8)
                let _ = make.height.equalTo(gregorianIcon)
                let _ = make.width.equalTo(gregorianIcon)
            }

            luarLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(luarIcon)
                let _ = make.left.equalTo(luarIcon.snp_right).offset(2)
                let _ = make.height.equalTo(luarIcon)
            }

            
            restDaysLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.right.equalTo(self).offset(-8)
                let _ = make.bottom.equalTo(luarLabel)
                let _ = make.height.equalTo(30)
            }
            
            self.refreshData()
        }
        
        
        //填充数据
        func refreshData()
        {
            //测试数据
            
            var timeStr = ""
            
            if let str = limitPresentInfo!["Gift_time"].string
            {
                if str.characters.count > 0
                {
                    timeStr = str
                }else
                {
                    timeStr = (limitPresentInfo["Date_time"].string)!
                }
            }else
            {
                if let date_time = limitPresentInfo["Date_time"].string{
                    timeStr = date_time
                }
            }
            
            
            let dateType = limitPresentInfo["Date_type"].int
            
            
            let imageNameAry = NSMutableArray()
            
            imageNameAry.add("heartWish_gregorian_unsel")
            
            imageNameAry.add("heartWish_luar_unsel")
            
            if dateType == 2//农历
            {
                gregorianLabel.alpha = 0.5
                
                imageNameAry.replaceObject(at: 1, with: "heartWish_luar_sel")
                
                luarLabel.text = CalendarUtil.alTolunar(timeStr)//当年农历
                
                let gregoiranDate = CalendarUtil.calendarChange(timeStr, andWithType: 2)//今年的公历
                
                gregorianLabel.text = CalendarUtil.subString(CalendarUtil.dateDict(toString: gregoiranDate,andTargetDateType:0), withRang: NSMakeRange(5, 5))
            }
            else if dateType == 1//公历
            {
                luarLabel.alpha = 0.5
                
                imageNameAry.replaceObject(at: 0, with: "heartWish_gregorian_sel")
                
                gregorianLabel.text = CalendarUtil.subString(timeStr, withFrom: 5)//当年公历
                
                let gregoiranDate = CalendarUtil.calendarChange(timeStr, andWithType: 1)//今年的农历
                
                luarLabel.text = CalendarUtil.subString(CalendarUtil.dateDict(toString: gregoiranDate,andTargetDateType:1), withRang: NSMakeRange(3, 4))
            }
            else//公历 & 农历
            {
                luarLabel.alpha = 0.5
                
                gregorianLabel.alpha = 0.5
                
                imageNameAry.replaceObject(at: 0, with: "heartWish_gregorian_sel")
                
                imageNameAry.replaceObject(at: 1, with: "heartWish_luar_sel")
                
                gregorianLabel.text = CalendarUtil.subString(timeStr, withFrom: 4)
                
                luarLabel.text = CalendarUtil.alTolunar(timeStr)
            }
            
            luarIcon.image = UIImage(named:(imageNameAry[1] as? String)!)
            
            gregorianIcon.image = UIImage(named:(imageNameAry[0] as? String)!)
            
            nameLabel.text  = limitPresentInfo["Name"].string
            
            restDaysLabel.text = CalculateResetDays.returnString()
            
            
            let data = limitPresentInfo
            
            let sence = data?["Scene"].int
            
            if sence == 1{
                
                if let imageName = data?["Avatar"].string
                {
                    if imageName.characters.count > 0
                    {
                        let imageScale : String = imageName + "?imageView2/0/w/" + "\(Int(94*Constant.ScreenSize.SCALE_SCREEN))" + "/h/" + "\(Int(94*Constant.ScreenSize.SCALE_SCREEN))"
                        
                        headIcon.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "new_default_banner"))
                    }
                    else
                    {
                        let name = Utils.subLastCharacter(data?["Name"].string)
                        
                        if name != NULL_STRING{
                            
                            headIconLabel.text = name
                            
                            let index = (data?["Id"].int)!%10
                            
                            let hex = Constant.randomColor[index]
                            
                            headIconLabel.backgroundColor = UIColor(rgba:hex)
                            
                        }else{
                            headIcon.image = UIImage(named: "gift_reminder_situation_birthday_default")
                        }
                    }
                }
                else
                {
                    let name = Utils.subLastCharacter(data?["Name"].string)
                    
                    if name != "null"{
                        
                        headIconLabel.text = name
                        
                        let index = (data?["Id"].int)!%10
                        
                        let hex = Constant.randomColor[index]
                        
                        headIconLabel.backgroundColor = UIColor(rgba:hex)
                        
                    }else{
                        headIcon.image = UIImage(named: "gift_reminder_situation_birthday_default")
                    }
                }
            }else
            {
                if let imageName = data?["Avatar"].string
                {
                    let imageScale : String = imageName + "?imageView2/0/w/" + "\(Int(94*Constant.ScreenSize.SCALE_SCREEN))" + "/h/" + "\(Int(94*Constant.ScreenSize.SCALE_SCREEN))"
                    
                    headIcon.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "new_default_banner"))
                }
                else
                {
                    headIcon.image =  UIImage(named: "gift_reminder_situation_birthday_default")
                }
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    
    class CalculateResetDays:NSObject
    {
        
        class func returnString()-> String
        {
            var dayStr = ""
            
            let days:Int = limitPresentInfo["Between_day"].int!
            
            if days == 0//今天
            {
                dayStr = "今天"
            }else if days == 1//明天
            {
                dayStr = "明天"
            }else if days == 2//后天
            {
                dayStr = "后天"
            }else
            {
                dayStr = "还有\(days)天"
            }
            
            return dayStr
        }
    }
}



//MARK:发送祝福视图
class  SendMessageContentView :UIView
{
    var wishList:[JSON]!
    
    var wishIndex = 0
    
    var viewMode:SendMsgType = .weChat
    
    var changeDirection = 1//祝福语切换方向：1：前一条 2：换一条
    
    var wishContentDidChange:Bool{
        
        get{
            if let list = wishList {
                
                if list.count > 0{
                    
                    if wishIndex < list.count{
                        
                        return wishContent != list[wishIndex]["Content"].string
                    }
                }
            }
            return false
        }
    }
    
    //祝福内容
    var wishContent:String?{
        get
        {return (viewMode == .weChat ? wechatView.weChatContent : messageView.messageContent)!}
        
        set(newValue)
        {if viewMode == .weChat {wechatView.weChatContent = newValue }else{ messageView.messageContent = newValue}}
    }
    
    
    //接收方姓名
    var wishPeopleMobile:String?{
        get
        {return messageView.messageMobileField.text}
        
        set(newValue)
        {messageView.messageMobileField.text = newValue}
    }
    
    
    //接收方手机号码
    var wishPeopleName:String?{
        get
        {return messageView.messageNameLabel.text}
        
        set(newValue)
        {messageView.messageNameLabel.text = newValue}
    }
    
    
    init()
    {
        super.init(frame: CGRect.zero)
        heartWishSetupUI()
        
    }
    
    
    
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    ///改变现实模式
    /**
    viewMode : 视图模式：.WeChat :微信微信现实模式 .Message :短信发送
    */
    ///
    func changeDisplayMode(_ viewMode:SendMsgType)
    {
        self.viewMode = viewMode
        
        if viewMode == .weChat
        {
            wechatView.weChatContent = messageView.messageContent
        }else
        {
            messageView.messageContent = wechatView.weChatContent
        }
        
        let index:CGFloat = viewMode == .weChat ? 0 : -0.5
        
        UIView.animate(withDuration: 0.25, animations: { () -> Void in
            self.transform = CGAffineTransform(translationX: self.iwidth * index, y: 0)
        }) 
    }
    
    
    
    ///切换wish内容
    /**
    changeType : 1：前一条  2：后一条
    */
    func changeWishWord()
    {
        if let list = wishList
        {
            if list.count != 0
            {
                if changeDirection == 1
                {
                    wishIndex -= 1
                    
                    wishIndex = wishIndex < 0 ? wishList.count - 1 : wishIndex
                }
                else
                {
                    wishIndex += 1
                    
                    wishIndex = wishIndex > wishList.count - 1 ? 0 : wishIndex
                }
                
            }else{return}
            
        }else{return}
        
        UIView.animate(withDuration: 0.0, delay: 0, options: UIViewAnimationOptions(), animations: {Void in
            
            if self.viewMode == .weChat
            {
                self.wechatView.wechatContentField.alpha = 0
            }else
            {
                self.messageView.messageContentField.alpha = 0
            }
            
            
            }, completion: {Void in
                
                self.wishContent = self.wishList[self.wishIndex]["Content"].string!
                
                limitMessageId = self.wishList[self.wishIndex]["Id"].intValue
                
                UIView.animate(withDuration: 0.45, delay: 0, options: UIViewAnimationOptions.curveEaseIn, animations: {Void in
                    
                    if self.viewMode == .weChat
                    {
                        self.wechatView.wechatContentField.alpha = 1
                    }else
                    {
                        self.messageView.messageContentField.alpha = 1
                    }
                    }, completion:nil)
        })
    }
    
    
    
    
    fileprivate let wechatView = WechatView()
    
    fileprivate let messageView = MessageView()
    
    
    func heartWishSetupUI()
    {
        
        //初始化短信发送联系人
        self.messageView.messageNameLabel.text = limitPresentType == PresentType.personal ? limitPresentInfo["Name"].string : ""
        
        self.messageView.messageMobileField.text = limitPresentType == PresentType.personal ? limitPresentInfo["Mobile"].string : nil
        
        
        self.addSubview(wechatView)
        self.addSubview(messageView)
        wechatView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self).offset(8)
            let _ = make.left.equalTo(self).offset(8)
            let _ = make.bottom.equalTo(self)
            let _ = make.width.equalTo(self).multipliedBy(0.5).offset(-16)
        }

        messageView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self)
            let _ = make.left.equalTo(self.snp_centerX).offset(8)
            let _ = make.bottom.equalTo(self)
            let _ = make.width.equalTo(self).multipliedBy(0.5).offset(-16)
        }

    }
    
    
    
    //MARK:微信消息视图
    class WechatView:UIView,UITextViewDelegate
    {
        ///微信消息内容
        var weChatContent:String?{get{return wechatContentField.text}set(newValue){
            
            self.wechatContentField.text = newValue
            
            oldFieldText = newValue
            }
        }
        
        var oldFieldText:String?
        
        fileprivate let wechatContentField = UITextView()
        
        init()
        {
            super.init(frame: CGRect.zero)
            
            self.wechatSetupUI()
        }
        
        fileprivate func wechatSetupUI()
        {
            wechatContentField.delegate = self
            
            wechatContentField.textColor = UIColor(rgba: Constant.common_C7_color)
            
            wechatContentField.font = UIFont.systemFont(ofSize: Constant.common_F2_font)
            
            self.clipsToBounds = true
            self.backgroundColor = UIColor.white
            self.layer.borderColor = separatLineColor.cgColor
            self.layer.cornerRadius = 3
            self.layer.borderWidth = 0.5
            
            self.addSubview(wechatContentField)
            
            wechatContentField.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(self)
                let _ = make.bottom.equalTo(self)
                let _ = make.left.equalTo(self).offset(3)
                let _ = make.right.equalTo(self)
            }

        }
        
        
        
        func textViewDidChange(_ textView: UITextView)
        {
            if textView.text.characters.count <= 180
            {
                oldFieldText = textView.text
            }else
            {
                textView.text = oldFieldText
            }
        }
        
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
    }
    
    
    //MARK:短信消息视图
    class MessageView:UIView,UITextViewDelegate,ABPeoplePickerNavigationControllerDelegate,CNContactPickerDelegate
    {
        var oldFieldText:String?
        
        ///短信消息内容
        var messageContent:String?{get{return messageContentField.text}
            
            set(newValue)
            {
                self.messageContentField.text = newValue
                
                oldFieldText = newValue
                
                self.caluteChartCount(newValue!.characters.count)
            }}
        
        fileprivate let bgView = UIView()//联系人背景条纹
        
        fileprivate let peopleContentView = UIView()//联系人控件组
        
        fileprivate let messageMobileField = UITextField()//联系人电话号码输入框
        
        fileprivate let messageNameLabel = CustomLabel(font: Constant.common_F2_font, textColorHex: Constant.common_C6_color)//
        
        fileprivate let openAddressBookImageView = UIImageView(image: UIImage(named: "heartWish_contactIcon"))//打开通讯录按钮
        
        fileprivate let messageContentTipLabel = CustomLabel(font: 11, textColorHex: "#FF1111")//还可以写xx字的提示
        
        fileprivate let messageContentField = UITextView()//短信内容
        
        fileprivate let messageBgView = UIView()//短信内容输入框和提示文字的背景
        
        fileprivate let mobilLineView = LineView()//输入框底部的黑线
        
        init()
        {
            super.init(frame: CGRect.zero)
            self.messageSetupUI()
        }
        
        fileprivate func messageSetupUI()
        {
            self.isUserInteractionEnabled = true
            peopleContentView.isUserInteractionEnabled = true
            
            messageContentTipLabel.textAlignment = NSTextAlignment.right
            //            messageContentTipLabel.backgroundColor = UIColor.redColor()
            
            messageContentField.delegate = self
            peopleContentView.layer.borderColor = separatLineColor.cgColor
            peopleContentView.backgroundColor = UIColor.white
            peopleContentView.layer.cornerRadius = 3
            peopleContentView.layer.borderWidth = 0.5
            
            messageContentField.textColor = UIColor(rgba: Constant.common_C7_color)
            messageContentField.font = UIFont.systemFont(ofSize: Constant.common_F2_font)
            
            messageBgView.clipsToBounds = true
            messageBgView.layer.borderColor = separatLineColor.cgColor
            messageBgView.backgroundColor = UIColor.white
            messageBgView.layer.cornerRadius = 3
            messageBgView.layer.borderWidth = 0.5
            
            
            peopleContentView.isUserInteractionEnabled = true
            
            openAddressBookImageView.isUserInteractionEnabled = true
            
            openAddressBookImageView.contentMode = UIViewContentMode.center
            
            openAddressBookImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(MessageView.openAddressAction)))
            
            
            for view in [peopleContentView,messageBgView]
            {
                self.addSubview(view)
            }
            
            
            for view in [messageContentField,messageContentTipLabel]
            {
                messageBgView.addSubview(view)
            }
            for view in [messageMobileField,messageNameLabel,openAddressBookImageView,mobilLineView]
            {
                peopleContentView.addSubview(view)
            }
            peopleContentView.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(self).offset(8)
                let _ = make.left.equalTo(self)
                let _ = make.right.equalTo(self)
            }

            messageBgView.snp_makeConstraints { (make) -> Void in
                
                
                if limitPresentType == .personal
                {
                    peopleContentView.isHidden = false
                    let _ = make.top.equalTo(peopleContentView.snp_bottom).offset(8)
                }else
                {
                    peopleContentView.isHidden = true
                    let _ = make.top.equalTo(self).offset(8)
                }
                let _ = make.left.equalTo(peopleContentView)
                let _ = make.right.equalTo(peopleContentView)
                let _ = make.bottom.equalTo(self)
            }

            messageNameLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(peopleContentView)
            }
            
            messageNameLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(peopleContentView).offset(8)
                let _ = make.bottom.equalTo(peopleContentView).offset(-8)
                let _ = make.left.equalTo(peopleContentView).offset(8)
                let _ = make.height.equalTo(30)
            }

            
            messageMobileField.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(messageNameLabel)
                let _ = make.bottom.equalTo(messageNameLabel)
                let _ = make.right.equalTo(openAddressBookImageView.snp_left).offset(-8)
                let _ = make.left.lessThanOrEqualTo(messageNameLabel.snp_right).offset(8)
            }

            mobilLineView.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(messageMobileField.snp_bottom)
                let _ = make.width.equalTo(messageMobileField)
                let _ = make.left.equalTo(messageMobileField)
                let _ = make.height.equalTo(0.5)
            }

            openAddressBookImageView.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(messageNameLabel)
                let _ = make.bottom.equalTo(messageNameLabel)
                let _ = make.right.equalTo(peopleContentView).offset(-8)
                let _ = make.width.equalTo(openAddressBookImageView.snp_height)
            }

            messageContentField.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(messageBgView)
                let _ = make.left.equalTo(messageBgView).offset(3)
               let _ =  make.right.equalTo(messageBgView)
                let _ = make.bottom.equalTo(messageContentTipLabel.snp_top)
            }

            messageContentTipLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.height.equalTo(14)
                let _ = make.bottom.equalTo(messageBgView).offset(-4)
                let _ = make.left.equalTo(messageBgView).offset(8)
                let _ = make.right.equalTo(messageBgView).offset(-10)
            }

        }
        
        
        
        ///打开通讯录
        func openAddressAction()
        {
            limitHeartWishVC.navigationBarAppearance(true)
            if #available(iOS 9.0, *) {
                let newPicker = CNContactPickerViewController()
                newPicker.delegate = self
                limitHeartWishVC.present(newPicker, animated: true, completion: {
                    
                })
            } else {
                let picker = ABPeoplePickerNavigationController()
                picker.displayedProperties = [3]
                picker.peoplePickerDelegate = self
                
                if ((UIDevice.current.systemVersion as NSString).floatValue >= 8.0)
                {
                    picker.predicateForSelectionOfPerson = NSPredicate(value:false)
                }
                
                limitHeartWishVC.present(picker, animated: true) {Void in}

                
            }

        }
        
        @available(iOS 9.0, *)
        @objc(contactPicker:didSelectContactProperty:)
        func contactPicker(_ picker: CNContactPickerViewController, didSelect contactProperty: CNContactProperty) {
            let firstName = contactProperty.contact.givenName
            let name = contactProperty.contact.familyName
            self.messageNameLabel.text = (name + firstName)
            
            if contactProperty.value != nil {
                let tempPhoneNumber = contactProperty.value as! CNPhoneNumber
                let phoneNumber = tempPhoneNumber.stringValue
                self.messageNameLabel.text = Utils.detectionTelphoneSimple(phoneNumber)
            }
            
        }
        
        @available(iOS 9.0, *)
        func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
            //去除地址选择界面
            picker.dismiss(animated: true, completion: { () -> Void in
                
            })
        }

        
        // peoplePickerNavigationController 代理方法
        func peoplePickerNavigationController(_ peoplePicker: ABPeoplePickerNavigationController,
            didSelectPerson person: ABRecord) {}
        
        func peoplePickerNavigationController(_ peoplePicker: ABPeoplePickerNavigationController,
            didSelectPerson person: ABRecord, property: ABPropertyID,
            identifier: ABMultiValueIdentifier) {
                
                let multiValue: ABMultiValue = ABRecordCopyValue(person, property).takeRetainedValue()
                var index = ABMultiValueGetIndexForIdentifier(multiValue, identifier)
                if index == -1{
                    index = 0 as CFIndex
                }
                let phone = ABMultiValueCopyValueAtIndex(multiValue, index).takeRetainedValue() as? String ?? ""
                
                let first = ABRecordCopyValue(person, kABPersonFirstNameProperty)?.takeRetainedValue() as? String ?? ""
                let last  = ABRecordCopyValue(person, kABPersonLastNameProperty)?.takeRetainedValue() as? String ?? ""
                
                self.messageNameLabel.text = last + first
                
                self.messageMobileField.text = Utils.detectionTelphoneSimple(phone)
        }
        
        //取消按钮点击
        func peoplePickerNavigationControllerDidCancel(_ peoplePicker: ABPeoplePickerNavigationController) {
            //去除地址选择界面
            //            UINavigationBar.appearance().tintColor = UIColor(rgba: Constant.titlebar_color)
            
            peoplePicker.dismiss(animated: true, completion: { () -> Void in
                
            })
        }
        
        func peoplePickerNavigationController(_ peoplePicker: ABPeoplePickerNavigationController,
            shouldContinueAfterSelectingPerson person: ABRecord) -> Bool {
                return false
        }
        
        func peoplePickerNavigationController(_ peoplePicker: ABPeoplePickerNavigationController,
            shouldContinueAfterSelectingPerson person: ABRecord, property: ABPropertyID,
            identifier: ABMultiValueIdentifier) -> Bool {
                return false
        }
        
        
        
        func textViewDidChange(_ textView: UITextView)
        {
            if textView.text.characters.count <= 180
            {
                oldFieldText = textView.text
            }else
            {
                textView.text = oldFieldText
            }
            
            self.caluteChartCount(textView.text.characters.count)
        }
        
        
        
        //根据内容长度显示提示语
        func caluteChartCount(_ charLen:Int)
        {
            if charLen <= 70
            {
                let str : String = "还可以写 " + String(70 - charLen) + " 字"
                let attributeString = NSMutableAttributedString(string: str)
                attributeString.setAttributes([NSForegroundColorAttributeName: UIColor(rgba: "#999999"),NSFontAttributeName : UIFont.systemFont(ofSize: 11.0)], range: NSMakeRange(0, 4))
                
                attributeString.setAttributes([NSForegroundColorAttributeName: UIColor(rgba: "#555555"),NSFontAttributeName : UIFont.systemFont(ofSize: 11.0)], range: NSMakeRange(4, str.characters.count - 5))
                
                attributeString.setAttributes([NSForegroundColorAttributeName: UIColor(rgba: "#999999"),NSFontAttributeName : UIFont.systemFont(ofSize: 11.0)], range: NSMakeRange(str.characters.count - 1, 1));
                
                messageContentTipLabel.attributedText = attributeString;
                
            }else if charLen > 70 && charLen < 140
            {
                messageContentTipLabel.text = "已超出70字，将拆分成2条短信发送"
            }else if charLen > 140 && charLen <= 180
            {
                messageContentTipLabel.text = "已超出140字，将拆分成3条短信发送"
            }else
            {
                messageContentTipLabel.text = "已超出180字，请缩减字数"
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}



class CustomLabel:UILabel
{
    
    ///为UILabel添加属性
    /*
    *   font            :label的文字尺寸 可不传    默认17
    *   autoFont        :在iphone5之前的文字与iphone6之后的文字大小 的差
    *   textColorHex    :label的文字颜色 可不传    默认是黑色
    *   textAlignment   :label的内容布局 可不传    默认是居中
    */
    ///
    
    
    class func newLabel(_ font:CGFloat,autoFont:CGFloat! = 0,textColor:UIColor = UIColor.black,textAlignment: NSTextAlignment = NSTextAlignment.center) ->CustomLabel{
        
        let lbl = CustomLabel(frame:CGRect.zero)
        
        lbl.setFont(font - autoFont, laterIph6: font)
        
        lbl.textColor = textColor
        
        lbl.textAlignment = textAlignment
        
        return lbl
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
   
    
    init(font:CGFloat,autoFont:CGFloat! = 0,textColorHex:String = "#000000",textAlignment: NSTextAlignment = NSTextAlignment.center)
    {
        super.init(frame: CGRect.zero)
        
        self.setFont(font - autoFont, laterIph6: font)
        
        self.textColor = UIColor(rgba: textColorHex)
        
        self.textAlignment = textAlignment
    }
    
    
    
    func setFont(_ beforeIph5s:CGFloat! = 0,laterIph6:CGFloat! = 0)
    {
        if Constant.DeviceType.IS_IPHONE_4_OR_LESS == true || Constant.DeviceType.IS_IPHONE_5 == true
        {
            if beforeIph5s != 0
            {
                self.font = UIFont.systemFont(ofSize: beforeIph5s)
            }
            
        }else
        {
            if laterIph6 != 0
            {
                self.font = UIFont.systemFont(ofSize: laterIph6)
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}





class LineView:UIView
{
    
    init()
    {
        super.init(frame: CGRect.zero)
        
        self.iheight = 0.5
        
        self.backgroundColor = UIColor(rgba: Constant.common_separatLine_color)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}



private class CustomButton:UIButton
{
    var type:Int = 0//1：前一条  2：后一条
    
    init(type:Int)
    {
        super.init(frame: CGRect.zero)
        
        self.type = type
        
        self.customButtonSetupUI()
    }
    
    func customButtonSetupUI()
    {
        let icon = UIImageView()
        
        let text = CustomLabel(font: Constant.common_F4_font, textColorHex: Constant.common_C8_color)
        
        icon.contentMode = UIViewContentMode.center
        
        self.addSubview(icon)
        self.addSubview(text)
        
        if type == 1
        {
            icon.image = UIImage(named: "heartWish_left")
            text.text = "上一条"
            icon.snp_makeConstraints(closure: { (make) -> Void in
                let _ = make.left.equalTo(self)
                let _ = make.top.equalTo(self)
                let _ = make.height.equalTo(self)
                let _ = make.width.equalTo(self.snp_height)
            })

            text.snp_makeConstraints(closure: { (make) -> Void in
                let _ = make.left.equalTo(icon.snp_right)
                let _ = make.top.equalTo(self)
                let _ = make.height.equalTo(self)
                let _ = make.right.equalTo(self)
            })

            
        }else
        {
            icon.image = UIImage(named: "heartWish_right")
            text.text = "下一条"
            text.snp_makeConstraints(closure: { (make) -> Void in
                let _ = make.left.equalTo(self)
                let _ = make.top.equalTo(self)
                let _ = make.height.equalTo(self)
                let _ = make.right.equalTo(icon.snp_left)
            })

            icon.snp_makeConstraints(closure: { (make) -> Void in
                let _ = make.right.equalTo(self)
                let _ = make.top.equalTo(self)
                let _ = make.height.equalTo(self)
                let _ = make.width.equalTo(self.snp_height)
            })

        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
