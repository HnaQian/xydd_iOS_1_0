//
//  ScreenContacts.swift
//  Limi
//
//  Created by guo chen on 15/10/21.
//  Copyright © 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

typealias DataCheckOver = (_ dataArray:NSMutableArray,_ isFinish:Bool) -> Void

private var onceCount = 100//一次上传的数据量

var exist_count = 0

var service_count = 0


class ScreenContacts: NSObject 
{   
    fileprivate var dateCheckOver:DataCheckOver?
    
    var sourceArray = NSMutableArray()//源数据
    
    fileprivate var screenedArray = NSMutableArray()//筛选后的数据
    
    fileprivate let should = 100
    var allRound = 0 //记录有多少组  一组100个数据
    let gcdlock = NSLock()
    fileprivate var queue = DispatchQueue(label: "checkData",attributes: [])
    
   let checkQueue = DispatchQueue(label: "checkDataCloud",attributes: [])
    
    
    
    static let sharedInstance = ScreenContacts()
    
    fileprivate override init() {}
    
    /**
    传入联系人OBj数组
    
    - parameter sysContacts: 联系人OBj数组
    - parameter dataCheck:   当本地联系人与服务器对比完成之后的block，其中block会传回过滤后的合法联系人数组，其中数组不为nil，但 count 可为0
    
    - returns: 返回筛选管理对象
    */
    class func newScreenContacts(_ sysContacts:NSArray,andCheckOver dataCheck:@escaping DataCheckOver)->ScreenContacts
    {
        let sigle = ScreenContacts()
        
        sigle.dateCheckOver = dataCheck
        
        //此处调用函数不能放入线程中处理
        sigle.analyzeSysContacts(sysContacts)
        
        return sigle
    }
    
    
    //No.1.1 初步过滤数据，上传服务器check
    fileprivate func analyzeSysContacts(_ sysContacts:NSArray)
    {
        if sysContacts.count < 1
        {
            self.dateCheckOver!(NSMutableArray(),true)
            
            return
        }
        
        //过滤无号码联系人
        for contact in sysContacts
        {
            let dict = AddressBookManager.analiyseConnectCard(contact as ABRecord!)
            
            if  (dict?["Name"] as! String) != "null" && (dict?["PhoneNumbers"] as! NSArray).count > 0 //是否有手机号码
            {
                sourceArray.add(createParems(dict?.copy() as! NSDictionary))//初步筛选数据 //整理数据   让每一个电话号都可以有对应的姓名和生日 分解了一个联系人多个手机号的情况，从而得到了源数据（也就是要上传的数据） sourceArray德玛数量也就是通讯录电话号的个数   而不是联系人的个数（一个联系人课能有多个电话号）
                
                screenedArray.add(dict)
            }
        }
        
        if screenedArray.count == 0 {
            self.dateCheckOver!(NSMutableArray(),true)
            return
        }
        //上传服务器，比对数据
//        dispatch_async(self.queue, {()->Void in
            self.supLoad()
//        })
        
    }
    
    
    //上传服务器，比对数据
    fileprivate func supLoad()
    {
        var ophoneAry:[String]?//电话数组
        
        var onameAry:[String]?//姓名数组
        
        var obirthDayAry:[String]?//生日数组
        
        var obirthdayTypeAry:[String]?//生日类型数组
        
        var index_start = 0//开始位置
        
        var index_end = 0//结束位置
        
        var isfirstUpLoad = true
        
        
        
        allRound = sourceArray.count
        
        if sourceArray.count < onceCount
        {
            allRound = 1
        }else
        {
            if sourceArray.count % onceCount == 0
            {
                allRound = sourceArray.count/onceCount
            }else
            {
                allRound = sourceArray.count/onceCount + 1
            }
        }
        
        for i in 0 ..< sourceArray.count
        {
            if i == 0
            {
                ophoneAry = [String]()//电话数组
                
                onameAry = [String]()//姓名数组
                
                obirthDayAry = [String]()//生日数组
                
                obirthdayTypeAry = [String]()//生日类型数组
            }
            
            
            let userInfo = sourceArray[i] as! [String:AnyObject]
            let phoneAry = userInfo["mobile"] as! [String]//电话数组
            
            let nameAry = userInfo["name"] as! [String]//姓名数组
            
            let birthDayAry = userInfo["birthday"] as! [String]//生日数组
            
            let birthdayTypeAry = userInfo["birthday_type"] as! [String]//生日类型数组
            
            
            for j in 0 ..< phoneAry.count
            {
                ophoneAry?.append(phoneAry[j])//电话数组
                
                onameAry?.append(nameAry[j])//姓名数组
                
                obirthDayAry?.append(birthDayAry[j])//生日数组
                
                obirthdayTypeAry?.append(birthdayTypeAry[j])//生日类型数组
            }
            
            
            //每 onceCount 个数据上传到服务器一次  记录上传的次数，没个现成返回一次，返回的次数等于上传的次数就说明已经完成了
            if i != 0 && (i % onceCount == 0 || i == sourceArray.count - 1)//100个数据一组
            {
                index_start = isfirstUpLoad == true ? 0 : index_end
                
                index_end = i
                
                var params = [String:AnyObject]()
                
                params["birthday_type"] = obirthdayTypeAry as AnyObject?
                
                params["mobile"] = ophoneAry as AnyObject?
                
                params["birthday"] = obirthDayAry as AnyObject?
                
                params["name"] = onameAry as AnyObject?

                let _ = CheckWithCloud.checkCloud(index_start,index_end, andScreenContacts: self, andParameter: params)
                ophoneAry = [String]()//电话数组
                
                onameAry = [String]()//姓名数组
                
                obirthDayAry = [String]()//生日数组
                
                obirthdayTypeAry = [String]()//生日类型数组
                
                isfirstUpLoad = false
            }else if sourceArray.count == 1//只有一个联系人
            {
                index_start = 0
                
                index_end = 0
                
                var params = [String:AnyObject]()
                
                params["birthday_type"] = obirthdayTypeAry as AnyObject?
                
                params["mobile"] = ophoneAry as AnyObject?
                
                params["birthday"] = obirthDayAry as AnyObject?
                
                params["name"] = onameAry as AnyObject?
                
                let _ = CheckWithCloud.checkCloud(index_start,index_end,andScreenContacts: self, andParameter: params)
            }
        }
    }
    
    
    //No.1.2.1 创建数据字典
    fileprivate func createParems(_ userInfo:NSDictionary)->[String:AnyObject]
    {
        var params = [String:AnyObject]()
        
        var phone =  [String]()
        
        for i in 0 ..< (userInfo["PhoneNumbers"] as! NSArray).count
        {
            phone.append(((userInfo["PhoneNumbers"] as! NSArray)[i] as? String)!)
        }
        
        
        params["mobile"] = phone as AnyObject?
        
        params = self.createParameter(userInfo,params:params,count:phone.count)
        
        return params
    }
    
    
    
    //No.1.2.2 创建其它参数字典
    fileprivate func createParameter(_ userInfo:NSDictionary, params:[String: AnyObject],count:Int)->[String: AnyObject]
    {
        var names =  [String]()
        
        var birthday_types =  [String]()
        
        var birthdays =  [String]()
        
        var genders =  [String]()
        
        
        for _ in 0 ..< count
        {
            names.append((userInfo["Name"] as? String)!)
            
            birthdays.append((userInfo["Birthday"] as? String)!)
            
            birthday_types.append("1")
            
            genders.append("0")
        }
        var total_params = params
        total_params["name"] = names as AnyObject?
        
        total_params["birthday_type"] = birthday_types as AnyObject?
        
        total_params["birthday"] = birthdays as AnyObject?
        
        return total_params
    }
}


class CheckWithCloud: NSObject 
{
    fileprivate var screenContacts:ScreenContacts! 

    fileprivate var index_start = 0//开始坐标
     
    fileprivate var index_end = 0    //结束坐标
    

    
    class func checkCloud(_ startIndex:Int,_ endIndex:Int,andScreenContacts screenContacts:ScreenContacts,andParameter params:[String: AnyObject]) ->CheckWithCloud
    {
        let checkC = CheckWithCloud()
        
        checkC.index_start = startIndex
        
        checkC.index_end = endIndex
        
        checkC.screenContacts = screenContacts
        
        checkC.updatePresentRemaind(params)
        
        return checkC
    }
    
    
    
    //No.2 本地数据上传服务器
    fileprivate func updatePresentRemaind(_ params:[String: AnyObject]) 
    {
        let urlStr = Constant.JTAPI.user_relation_upload
        let _ = LMRequestManager.requestByDataAnalyses(context:UIApplication.shared.keyWindow!,URLString: urlStr, parameters: params, isNeedUserTokrn: true, isShowErrorStatuMsg: true, successHandler: {data,status,msg in
            print(msg)
            if status == 200
            {
                
                let mdata = data["data"] as! NSDictionary
                
                var dataArray = NSArray()
                
                if (mdata["birthday_data"] as! NSObject) != NSNull()
                {
                    dataArray = mdata["birthday_data"] as! NSArray
                }
                self.checkWithServicer(dataArray)
            }
        })
    }
    
    
    
    //No.3 当服务器传回数据后，与本地数据对比
    fileprivate func checkWithServicer(_ servicerArray:NSArray)
    {
        let exist:NSString = NSString(string:"exist")
        
        let screenItemArray = NSMutableArray()
        
        if servicerArray.count > 0//判断服务器是否传回数据
        {
            service_count += servicerArray.count
            
            var index = 0;
            
            for i in index_start ..< index_end
            {
                let localDict = screenContacts.sourceArray[i] as! NSDictionary//本地数据
                
                let  localMobileAry = localDict["mobile"] as! [String]//电话号码组
                
                var  localBirthdayAry = localDict["birthday"] as! [String]//生日组
                
                let screenDict = screenContacts.screenedArray[i] as! NSMutableDictionary
                
                var isExist = false
                
                index = index + localMobileAry.count
                
                var currentIndex = 0
                
                //遍历手机数组
                for p in 0 ..< localMobileAry.count
                {
                    currentIndex = index - localMobileAry.count + p
                    
                    let phoneNumber = localMobileAry[p]
                    
                    for j in 0 ..< servicerArray.count
                    {
                        let servDict = servicerArray[j] as! NSDictionary//服务器数据
                        
                        let servID = servDict["Id"] as! Int
                        
                        let serPhoneNumber = servDict["Mobile"] as? String
                        
                        
                        if currentIndex == servID//如果id相同
                        {
                            isExist = (servDict["Is_exist"] as! Bool)
                            
                            if isExist//检验是已否存在
                            {
                                print("Exist====>")
                                print(phoneNumber)
                                
                                screenDict["Birthday"] = exist
                                
                                break
                            }else
                            {
                                print("noExist====>")
                                print(phoneNumber)
                                
                                if phoneNumber == serPhoneNumber
                                {
                                    //比对生日，如果生日有、云数据库有
                                    if localBirthdayAry[p] == "null"
                                    {
                                        screenDict["Birthday"] = NSString(string:servDict["Birthday"] as! String)
                                        
                                        screenDict["isCloud"] = true
                                    }
                                }
                            }
                        }
                    }
                    
                    if isExist == true{break}
                }
                
                if screenDict["Birthday"] as! String != "null" && screenDict["Birthday"] as! String != "exist"//是否有生日
                {
                    print("screenDict=======>")
                    print(screenDict)
                    screenItemArray.add(screenDict)
                }
            }
        }else
        {
            for i in index_start ..< index_end
            {
                
                let screenDict = screenContacts.screenedArray[i] as! NSMutableDictionary
                print("compare before")
                    print(screenDict)
                if screenDict["Birthday"] as! String != "null" && screenDict["Birthday"] as! String != "exist"//是否有生日
                {
                    screenItemArray.add(screenDict)
                }
            }
        }

        
        //allRound  为分批上传的总次数：从1开始。 当allRound减为0时，数据筛选结束
        screenContacts.allRound -= 1
        
        screenContacts.dateCheckOver!(screenItemArray,screenContacts.allRound == 0 ? true:false)
    }
}
