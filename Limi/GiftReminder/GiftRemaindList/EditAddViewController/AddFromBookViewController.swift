//
//  AddFromBookViewController.swift
//  Limi
//
//  Created by guo chen on 15/10/13.
//  Copyright © 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//从通讯录添加联系人

import UIKit

import Foundation

import AddressBook

import AddressBookUI



class AddFromBookViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,UpLoadContactsDelegate
{
    
    fileprivate var error:Unmanaged<CFError>?
    
    fileprivate let precentLabel = UILabel()
    
    fileprivate let tableView = UITableView()
    
    fileprivate var screenContactsArray = NSMutableArray()//筛选过后的合法联系人
    
    fileprivate var sourceContactsArray:NSArray?//系统联系人
    
    fileprivate let resultLabel = UILabel()
    
    fileprivate let radioImageView = UIImageView(frame:CGRect(x: 5,y: 12,width: 26,height: 26))
    
    //removeAllObjects
    fileprivate var selectedAry = NSMutableArray()
    
    fileprivate var selectedContactAry = NSMutableArray()    
    
    fileprivate var headUrlStr:String?
    
//    private let nullView = NullView(frame: CGRectMake(0, 0, Constant.ScreenSize.SCREEN_WIDTH, Constant.ScreenSize.SCREEN_HEIGHT))
    
    fileprivate var chooseView:UIView?
    
    fileprivate let queue = DispatchQueue(label: "getData",attributes: [])
    
    fileprivate let onceCount = 100
    
    fileprivate var allRound = 0
    
    fileprivate var _importButton: UIButton?
    
    var sysContacts : NSArray = []
    
    var updateType:Int = 1//0是新手引导，1是从送礼提醒列表右上角导入联系人生日
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setupUI()
        
        self.title = "导入联系人生日"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 
     func setupUI()
     {
        self.view.backgroundColor = Constant.grayColor
        
        tableView.frame = self.view.bounds
        
        tableView.iheight = tableView.iheight - 64 - 49//50为“导入工具条的高度”
        
        tableView.backgroundColor = UIColor.clear
        
        tableView.delegate = self
        
        tableView.dataSource = self
        
        tableView.separatorColor = UIColor(rgba: Constant.common_separatLine_color)
        
        tableView.showsHorizontalScrollIndicator = false
        
        tableView.showsVerticalScrollIndicator = false
        
        resultLabel.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH,height: 50)
        
        
        resultLabel.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        
        resultLabel.clipsToBounds = true
        
        resultLabel.text = "为您找到0个联系人的生日信息"
        
        self.view.addSubview(resultLabel)
        
        self.view.addSubview(tableView)
        
        
        precentLabel.frame = self.view.bounds
        
        
        precentLabel.textAlignment = NSTextAlignment.center
        
        precentLabel.backgroundColor = UIColor(red: 0 , green: 0, blue: 0, alpha: 0.5)
        
        chooseAllView()
        
        analyzeSysContacts(sysContacts)
        
    }
    
    override func backItemAction() {
//        super.backItemAction()
//        let vc = RemindViewController()
       let _ = self.navigationController?.popToRootViewController(animated: true)
    }
    
    /**
     从通讯录中添加/导入生日
     
     - parameter updateType: 0：上传通讯录，1：导入联系人生日
     
     - returns: 返回对象
     */
    
    class func newAddFromBookViewController(_ updateType:Int)->AddFromBookViewController
    {
        let addVC = AddFromBookViewController()
        
        addVC.setupUI()
        
        addVC.updateType = updateType
        
        return addVC
    }

    
    /**
     过滤联系人
     
     - parameter sysContacts: 联系人数组
     */
    internal func analyzeSysContacts(_ sysContacts:NSArray)
    {
        if sysContacts.count > 0
        {
            let _ = ScreenContacts.newScreenContacts(sysContacts,andCheckOver:{(dataArray:NSMutableArray,isFinish:Bool)->Void in
                
                self.screenArray(dataArray,andIsFinish:isFinish)
            })
        }else
        {
            self.screenArray(self.screenContactsArray,andIsFinish:true)
        }
    }
    
    
    fileprivate func screenArray(_ array:NSMutableArray,andIsFinish isFinish:Bool)
    {
        for i in 0 ..< array.count
        {
            let dict = array[i] as! NSMutableDictionary
            
            screenContactsArray.add(dict)
        }
        
        
//        dispatch_async(dispatch_get_main_queue(), {()->Void in
        
            self.createSelectedAry(self.screenContactsArray.count, andisfFinish:isFinish)
            
//        });
    }

    
    fileprivate func createSelectedAry(_ count:Int,andisfFinish isFinish:Bool)
    {
        if isFinish
        {
         //   nullView.hidden = count > 0 ? true : false
            
            if self.updateType == 0 && count == 0 //如果没有联系人，
            {
                let navController : UINavigationController = self.navigationController!
                let vc = GuideEmptyContactViewController()
                vc.hidesBottomBarWhenPushed = true
//                navController.popViewControllerAnimated(false)
//                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    navController.pushViewController(vc, animated: true)
//                })
            }
        }
    
    
        if count == 0//如果没有数据，返回
        {
            return
        }
        
        chooseView?.isHidden = count > 0 ? false : true
        
        selectedAry.removeAllObjects()
        
        for _ in 0 ..< count
        {
            selectedAry.add(true)
        }
        
        self.tableView.reloadData()
        
        resultLabel.text = "为您找到" + String(count) + "个联系人的生日信息"
    }
    
    
    
    
    //导入用户数据
    func updateButtonAction()
    {
            if screenContactsArray.count > 0
        {
            for i in 0 ..< selectedAry.count
            {
                let statue = selectedAry[i] as! Bool
                
                if statue
                {
                    let dict = screenContactsArray[i] as! NSDictionary
                    
                    selectedContactAry.add(createParems(dict.copy() as! NSDictionary))
                }
            }
        }
        if selectedContactAry.count > 0{
        self.supLoad(selectedContactAry)
        }
        else{
           let _ = self.navigationController?.popToRootViewController(animated: false)

        }
    }
    
    
    //导入数据
    fileprivate func supLoad(_ sourceArray:NSArray)
    {
        Statistics.count(Statistics.GiftRemind.remind_import_finish_click)
        
        var ophoneAry:[String]?//电话数组
        
        var mround = 1
        
        var onameAry:[String]?//姓名数组
        
        var obirthDayAry:[String]?//生日数组
        
        var obirthdayTypeAry:[String]?//生日类型数组
        
        var oimageAry:[String]?//头像类型数组
        
        var ogenderAry:[String]?//性别类型数组
        
        allRound = sourceArray.count
        
        if sourceArray.count < onceCount
        {
            allRound = 1
        }else
        {
            if sourceArray.count % onceCount == 0
            {
                allRound = sourceArray.count/onceCount
            }else
            {
                allRound = sourceArray.count/onceCount + 1
            }
        }
        
        
        
        for i in 0 ..< sourceArray.count
        {
            if i == 0
            {
                ophoneAry = [String]()//电话数组
                
                onameAry = [String]()//姓名数组
                
                obirthDayAry = [String]()//生日数组
                
                obirthdayTypeAry = [String]()//生日类型数组
                
                oimageAry = [String]()
                
                ogenderAry = [String]()
            }
            
            
            let userInfo = sourceArray[i] as! [String:AnyObject]
            let phoneAry = userInfo["mobile"] as! [String]//电话数组
            
            let nameAry = userInfo["name"] as! [String]//姓名数组
            
            let birthDayAry = userInfo["birthday"] as! [String]//生日数组
            
            let birthdayTypeAry = userInfo["birthday_type"] as! [String]//生日类型数组
            
            let genderAry = userInfo["gender"] as! [String]//生日数组
            
            let imageAry = userInfo["avatar"] as! [String]//生日数组
            
            for j in 0 ..< phoneAry.count
            {
                ophoneAry?.append(phoneAry[j])//电话数组
                
                onameAry?.append(nameAry[j])//姓名数组
                
                obirthDayAry?.append(birthDayAry[j])//生日数组
                
                obirthdayTypeAry?.append(birthdayTypeAry[j])//生日类型数组
                
                oimageAry?.append(imageAry[j])//头像数组
                
                ogenderAry?.append(genderAry[j])//性别数组
            }
            
            
            
            //每 onceCount 个数据上传到服务器一次
            if i != 0 && (i % onceCount == 0 || i == sourceArray.count - 1)//onceCount个数据一组
            {
                var params = [String:AnyObject]()
                
                params["birthday_type"] = obirthdayTypeAry as AnyObject?
                
                params["mobile"] = ophoneAry as AnyObject?
                
                params["birthday"] = obirthDayAry as AnyObject?
                
                params["name"] = onameAry as AnyObject?
                
                params["gender"] = ogenderAry as AnyObject?
                
                params["avatar"] = oimageAry as AnyObject?
                
                let upLoad = UpLoadContacts.upLoadContacts(params)
                
                upLoad.delegate = self
                
                ophoneAry = [String]()//电话数组
                
                onameAry = [String]()//姓名数组
                
                obirthDayAry = [String]()//生日数组
                
                obirthdayTypeAry = [String]()//生日类型数组
                
                oimageAry =  [String]()//头像类型数组
                
                ogenderAry = [String]()//性别类型数组
                
                mround += 1
                
            }else if sourceArray.count == 1
            {
                var params = [String:AnyObject]()
                
                params["birthday_type"] = obirthdayTypeAry as AnyObject?
                
                params["mobile"] = ophoneAry as AnyObject?
                
                params["birthday"] = obirthDayAry as AnyObject?
                
                params["name"] = onameAry as AnyObject?
                
                params["gender"] = ogenderAry as AnyObject?
                
                params["avatar"] = oimageAry as AnyObject?
                
                let upLoad = UpLoadContacts.upLoadContacts(params)
                
                upLoad.delegate = self
            }
        }
    }
    
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }

    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        let height:CGFloat = self.screenContactsArray.count > 0 ? 50 : 0
        
        resultLabel.iheight = height
        
        return height
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        resultLabel.textAlignment = NSTextAlignment.center
        
        resultLabel.textColor = UIColor.lightGray
        
        resultLabel.backgroundColor = Constant.grayColor
        
        return resultLabel
    }
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return  self.screenContactsArray.count
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 70
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let identifier = "UpDateConnectCell"
        
        var cell: UpDateConnectCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? UpDateConnectCell
        
        if cell == nil
        {
            tableView.register(UINib(nibName: "UpDateConnectCell", bundle: nil), forCellReuseIdentifier: identifier)
            
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? UpDateConnectCell
        }
        
        cell.setPropertise(self.screenContactsArray[(indexPath as NSIndexPath).row] as! NSMutableDictionary, shouldSelected: selectedAry[(indexPath as NSIndexPath).row] as! Bool)
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        callBack((indexPath as NSIndexPath).row)
    }
    
    
    
    func setAllToStatue(_ statue:Bool)
    {
        for i in 0 ..< selectedAry.count
        {
            selectedAry.replaceObject(at: i, with: statue)
        }
    }
    
    //更新完成之后
    func upLoadContactsDelegateSchedule()
    {
        allRound -= 1
        
        if allRound == 0
        {
            NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.notification_import_contact_success), object: nil)
            //TODO
           let _ = self.navigationController?.popToRootViewController(animated: false)

        }
    }
    
    func callBack(_ index: Int)//筛选被选中的数据
    {
        let temp = selectedAry[index] as! Bool
        
        selectedAry.replaceObject(at: index, with: !temp)
        
        self.tableView.reloadData()
        
        var isAllSelected = true
        var isAllNotSelected = true
        
        for i in 0 ..< selectedAry.count
        {
            if selectedAry[i] as! Bool == false
            {
                isAllSelected = false
            } else {
                isAllNotSelected = false
            }
        }
        
        
        if isAllSelected
        {
            radioImageView.image = UIImage(named:"mchoose")
            
            radioImageView.tag = 120
            
        }else
        {
            radioImageView.image = UIImage(named:"add_gift_reminder_unchoose")
            
            radioImageView.tag = 110
            
        }
        self._importButton?.isEnabled = !isAllNotSelected
    }
    
    
    deinit 
    {
        
    }
}







extension AddFromBookViewController
{
    //全选 + 导入工具条】
    
    
    fileprivate func chooseAllView()
    {
        let height:CGFloat = 50
        
        chooseView = UIView(frame:CGRect(x: 0,y: self.view.iheight - height - 64,width: Constant.ScreenSize.SCREEN_WIDTH,height: height))
        
        chooseView!.isHidden = true
        
        chooseView!.isUserInteractionEnabled = true
        
        radioImageView.image = UIImage(named:"mchoose")
        
        radioImageView.contentMode = UIViewContentMode.center
        
        radioImageView.isUserInteractionEnabled = true
        //        radioButton.imageView!.contentMode = UIViewContentMode.ScaleToFill
        radioImageView.addGestureRecognizer( UITapGestureRecognizer(target:self,action:#selector(AddFromBookViewController.chooseAllBtnAction(_:))))
        
        radioImageView.tag = 120
        
        
        let radiotitleLable = UILabel(frame:CGRect(x: radioImageView.iright,y: 0,width: 40,height: 50))
        
        radiotitleLable.textColor = UIColor(rgba: Constant.common_C2_color)
        
        radiotitleLable.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        
        radiotitleLable.text = "全选"
        
        let updateButton = UIButton(frame:CGRect(x: radiotitleLable.iright + 15,y: 5,width: Constant.ScreenSize.SCREEN_WIDTH - radiotitleLable.iright - 15 - 8,height: 40))
        
        updateButton.addTarget(self, action: #selector(AddFromBookViewController.updateButtonAction), for: UIControlEvents.touchUpInside)
        
        updateButton.backgroundColor = UIColor(rgba: "#ff3a48")
        
        updateButton.setTitle("导入", for:UIControlState())
        
        updateButton.setTitleColor(UIColor.white, for: UIControlState())
        
        updateButton.clipsToBounds = true
        
        updateButton.layer.cornerRadius = 3
        
        self._importButton = updateButton
        
        for view in [radioImageView,radiotitleLable,updateButton]
        {
            chooseView!.addSubview(view)
        }
        
        chooseView!.backgroundColor = UIColor.white
        
        self.view.addSubview(chooseView!)
    }
    
    
    
    func chooseAllBtnAction(_ ges:UITapGestureRecognizer)
    {
        
        let btn = ges.view as! UIImageView
        print(btn.tag)
        if btn.tag == 120
        {
            btn.image = UIImage(named:"datePicker_unselecte")
            
            setAllToStatue(false)
//            self._importButton?.enabled = false
            
            btn.tag = 110
        }else
        {
            btn.image = UIImage(named:"choosed")
            
            setAllToStatue(true)
//            self._importButton?.enabled = true
            
            btn.tag = 120
        }
        
        self.tableView.reloadData()
    }
    
    
    
    
    //创建数据字典
    fileprivate func createParems(_ userInfo:NSDictionary)->[String:AnyObject]
    {
        var params = [String:AnyObject]()
        
        var phone =  [String]()
        
        for i in 0 ..< (userInfo["PhoneNumbers"] as! NSArray).count
        {
            phone.append(((userInfo["PhoneNumbers"] as! NSArray)[i] as? String)!)
        }
        
        
        params["mobile"] = phone as AnyObject?
        
        params = self.createParameter(userInfo,params:params,count:phone.count)
        
        return params
    }
    
    
    fileprivate func createParameter(_ userInfo:NSDictionary, params:[String: AnyObject],count:Int)->[String: AnyObject]
    {
        var names =  [String]()
        
        var avatars = [String]()
        
        var birthday_types =  [String]()
        
        var birthdays =  [String]()
        
        var genders =  [String]()
        
        
        for _ in 0 ..< count
        {
            names.append((userInfo["Name"] as? String)!)
            
            birthdays.append((userInfo["Birthday"] as? String)!)
            
            
            if headUrlStr != nil
            {
                avatars.append(headUrlStr!)
                
            }else
            {
                avatars.append("null")
            }
            
            birthday_types.append("1")
            
            genders.append("0")
        }
        var total_params = params
        
        total_params["name"] = names as AnyObject?
        
        total_params["gender"] = genders as AnyObject?
        
        total_params["avatar"] = avatars as AnyObject?
        
        total_params["birthday_type"] = birthday_types as AnyObject?
        
        total_params["birthday"] = birthdays as AnyObject?
        
        return total_params
    }
    
    
    //创建百分比显示器
    class PercentView:UIView
    {
        let precentLabel = UILabel()
        
        
        override init(frame:CGRect)
        {
            super.init(frame:frame)
            
            setupUI()
        }
        
        
        func setupUI()
        {
            precentLabel.frame = self.bounds
            
            precentLabel.textAlignment = NSTextAlignment.center
            
            precentLabel.backgroundColor = UIColor(red: 0 , green: 0, blue: 0, alpha: 0.5)
            
            self.addSubview(precentLabel)
        }
        
        
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        
    }
    
    
    
}
