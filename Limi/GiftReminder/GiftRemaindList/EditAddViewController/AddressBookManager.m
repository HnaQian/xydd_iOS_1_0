//
//  AddressBookManager.m
//  Limi
//
//  Created by guo chen on 15/10/13.
//  Copyright © 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

#import "AddressBookManager.h"

@implementation AddressBookManager

+ (NSMutableArray *)analiyseConnectCard:(ABRecordRef)people
{
    NSMutableDictionary *mutableDict  = [NSMutableDictionary new];
    
    //    ABRecordRef  people = CFArrayGetValueAtIndex(allLinkPeople, i);
    //获取当前联系人名字
    NSString *firstName=(__bridge NSString *)(ABRecordCopyValue(people, kABPersonFirstNameProperty));
    
    NSString *lastName = (__bridge NSString *)(ABRecordCopyValue(people, kABPersonLastNameProperty));
    
    if (firstName == nil && lastName == nil) 
    {
        firstName = @"null";
    }
    else 
    {
        if (firstName == nil) 
        {
            firstName = lastName;
        }
        else if(lastName != nil)
        {
            firstName = [lastName stringByAppendingString:firstName];
        }
    }
    
    //
    [mutableDict setValue:[NSString stringWithString:firstName ] forKey:@"Name"];
    
    //获取当前联系人的生日
    NSDate *birthday = (__bridge NSDate*)(ABRecordCopyValue(people, kABPersonBirthdayProperty));
    
    if (birthday == nil) 
    {
        [mutableDict setValue:@"null" forKey:@"Birthday"];
        
    }else
    {
        if (birthday.description.length >= 10) 
        {
            NSString *time = [birthday.description substringToIndex:4];
            
            if ([time isEqualToString:@"1604"]) 
            {
                [mutableDict setValue:@"null" forKey:@"Birthday"];                    
            }
            else
            {
                time = [birthday.description substringToIndex:10];
                
                [mutableDict setValue:time forKey:@"Birthday"];
            }
            
        }else
        {
            [mutableDict setValue:@"null" forKey:@"Birthday"];    
        }
    }
    
    //获取当前联系人的电话 数组
    
    ABMultiValueRef phoneNumberProperty = ABRecordCopyValue(people, kABPersonPhoneProperty);  
    
    NSMutableArray* phoneArr = [CFBridgingRelease(ABMultiValueCopyArrayOfAllValues(phoneNumberProperty)) mutableCopy];  

    NSMutableArray* screenedPhoneArr = [NSMutableArray new];
    
    if (phoneArr != nil)
    {
        for (NSInteger j=0; j < phoneArr.count; j++) 
        {
            NSString *mobile = phoneArr[j];

            mobile = [mobile stringByReplacingOccurrencesOfString:@"-" withString:@""];

            mobile = [self detectionTelphoneSimple:mobile];
            
            if (mobile.length == 11 && [[mobile substringToIndex:1] isEqualToString:@"1"])
            {   
                [screenedPhoneArr addObject:mobile];
            }
        }
    }
    
    [mutableDict setValue:screenedPhoneArr forKey:@"PhoneNumbers"];
    
    
    //获取当前联系人头像图片
    NSData *userImage = (__bridge NSData*)(ABPersonCopyImageData(people));
    
    //    if (userImage != nil) 
    //    {
    UIImage *image = [UIImage imageWithData:userImage];
    
    [mutableDict setValue:image forKey:@"HeadImage"];    
    //    }else
    //    {
    //        [mutableDict setValue:nil forKey:@"HeadImage"];    
    //    }
    
    
    int index = (arc4random()) % (9 - 0 + 1) + 0;
    
    [mutableDict setValue:@(index) forKey:@"colorIndex"];
    
    [mutableDict setValue:@(NO) forKey:@"isCloud"];    
    
    
    /*
     
     //获取创建当前联系人的时间 注意是NSDate
     NSDate*creatTime=(__bridge NSDate*)(ABRecordCopyValue(people, kABPersonCreationDateProperty));
     //获取最近修改当前联系人的时间
     NSDate*alterTime=(__bridge NSDate*)(ABRecordCopyValue(people, kABPersonModificationDateProperty));
     //获取地址
     ABMultiValueRef address = ABRecordCopyValue(people, kABPersonAddressProperty);
     for (int j=0; j<ABMultiValueGetCount(address); j++) {
     //地址类型
     NSString * type = (__bridge NSString *)(ABMultiValueCopyLabelAtIndex(address, j));
     NSDictionary * temDic = (__bridge NSDictionary *)(ABMultiValueCopyValueAtIndex(address, j));
     //地址字符串，可以按需求格式化
     NSString * adress = [NSString stringWithFormat:@"国家:%@\n省:%@\n市:%@\n街道:%@\n邮编:%@",[temDic valueForKey:(NSString*)kABPersonAddressCountryKey],[temDic valueForKey:(NSString*)kABPersonAddressStateKey],[temDic valueForKey:(NSString*)kABPersonAddressCityKey],[temDic valueForKey:(NSString*)kABPersonAddressStreetKey],[temDic valueForKey:(NSString*)kABPersonAddressZIPKey]];
     }
     
     
     
     //获取当前联系人姓氏
     NSString*lastName=(__bridge NSString *)(ABRecordCopyValue(people, kABPersonLastNameProperty));
     //获取当前联系人中间名
     NSString*middleName=(__bridge NSString*)(ABRecordCopyValue(people, kABPersonMiddleNameProperty));
     //获取当前联系人的名字前缀
     NSString*prefix=(__bridge NSString*)(ABRecordCopyValue(people, kABPersonPrefixProperty));
     //获取当前联系人的名字后缀
     NSString*suffix=(__bridge NSString*)(ABRecordCopyValue(people, kABPersonSuffixProperty));
     //获取当前联系人的昵称
     NSString*nickName=(__bridge NSString*)(ABRecordCopyValue(people, kABPersonNicknameProperty));
     //获取当前联系人的名字拼音
     NSString*firstNamePhoneic=(__bridge NSString*)(ABRecordCopyValue(people, kABPersonFirstNamePhoneticProperty));
     //获取当前联系人的姓氏拼音
     NSString*lastNamePhoneic=(__bridge NSString*)(ABRecordCopyValue(people, kABPersonLastNamePhoneticProperty));
     //获取当前联系人的中间名拼音
     NSString*middleNamePhoneic=(__bridge NSString*)(ABRecordCopyValue(people, kABPersonMiddleNamePhoneticProperty));
     //获取当前联系人的公司
     NSString*organization=(__bridge NSString*)(ABRecordCopyValue(people, kABPersonOrganizationProperty));
     //获取当前联系人的职位
     NSString*job=(__bridge NSString*)(ABRecordCopyValue(people, kABPersonJobTitleProperty));
     //获取当前联系人的部门
     NSString*department=(__bridge NSString*)(ABRecordCopyValue(people, kABPersonDepartmentProperty));
     
     
     NSMutableArray * emailArr = [[NSMutableArray alloc]init];
     //获取当前联系人的邮箱 注意是数组
     ABMultiValueRef emails= ABRecordCopyValue(people, kABPersonEmailProperty);
     for (NSInteger j=0; j<ABMultiValueGetCount(emails); j++) {
     [emailArr addObject:(__bridge NSString *)(ABMultiValueCopyValueAtIndex(emails, j))];
     }
     //获取当前联系人的备注
     NSString*notes=(__bridge NSString*)(ABRecordCopyValue(people, kABPersonNoteProperty));
     
     
     
     //获取当前联系人纪念日
     
     NSMutableArray * dateArr = [[NSMutableArray alloc]init];
     
     ABMultiValueRef dates= ABRecordCopyValue(people, kABPersonDateProperty);
     
     for (NSInteger j=0; j<ABMultiValueGetCount(dates); j++)
     {
     //获取纪念日日期
     NSDate * data =(__bridge NSDate*)(ABMultiValueCopyValueAtIndex(dates, j));
     //获取纪念日名称
     NSString * str =(__bridge NSString*)(ABMultiValueCopyLabelAtIndex(dates, j));
     }
     
     */
    
    return [mutableDict mutableCopy];
}


+(NSString *)detectionTelphoneSimple:(NSString *)str
{
    str=[str stringByReplacingOccurrencesOfString:@"-" withString:@""];
    str=[str stringByReplacingOccurrencesOfString:@" " withString:@""];
    if([str hasPrefix:@"17951"]){
        str = [str substringFromIndex:5];
    }
    else if ([str hasPrefix:@"0086"]){
        str = [str substringFromIndex:4];
    }
    else if ([str hasPrefix:@"+86"]){
        str = [str substringFromIndex:3];
    }
    else if ([str hasPrefix:@"19389"]){
        str = [str substringFromIndex:5];
    }
    else if ([str hasPrefix:@"12593"]){
        str = [str substringFromIndex:5];
    }
    else if ([str hasPrefix:@"17911"]){
        str = [str substringFromIndex:5];
    }
    else if ([str hasPrefix:@"17901"]){
        str = [str substringFromIndex:5];
    }
    else if ([str hasPrefix:@"10193"]){
        str = [str substringFromIndex:5];
    }
    else if ([str hasPrefix:@"86"]){
        str = [str substringFromIndex:2];
    }
    str =[str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    return str;
}



@end
