//
//  AddressBookManager.h
//  Limi
//
//  Created by guo chen on 15/10/13.
//  Copyright © 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//对联系人数据解析

#import <Foundation/Foundation.h>

#import <AddressBook/AddressBook.h>

#import <AddressBookUI/AddressBookUI.h>

@interface AddressBookManager : NSObject

/**
 * 解析联系人数据,返回的数据中，如果是String类型的数据，如果数据为空以 "null" 字符标记
 *
 *  @param people 联系人Object
 *
 *  @return 返回一个字典：[Name:String, Birthday:String?//2015-10-13, PhoneNumbers:NSArray,HeadImage:UIImage?, colorIndex:int]
 */
+ (NSMutableDictionary *)analiyseConnectCard:(ABRecordRef)people;

@end
