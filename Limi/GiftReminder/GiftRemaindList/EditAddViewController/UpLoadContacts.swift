//
//  UpLoadContacts.swift
//  Limi
//
//  Created by guo chen on 15/10/26.
//  Copyright © 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//导入联系人数组

import UIKit

protocol UpLoadContactsDelegate:NSObjectProtocol
{
    func upLoadContactsDelegateSchedule()
}

class UpLoadContacts: NSObject 
{
    class func upLoadContacts(_ params:[String: AnyObject])->UpLoadContacts
    {
        let uploadVc = UpLoadContacts()
        
        uploadVc.updatePresentRemaind(UIApplication.shared.keyWindow!,params:params)
        
        return uploadVc
    }

    weak var delegate:UpLoadContactsDelegate?
    
    fileprivate func updatePresentRemaind(_ context:UIView,params:[String: AnyObject]) 
    {
        let urlStr = Constant.JTAPI.user_relation_import
        let _ = LMRequestManager.requestByDataAnalyses(context:context,URLString: urlStr, parameters: params, isNeedUserTokrn: true, isShowErrorStatuMsg: true, successHandler: {data,status,msg in
        
            if self.delegate != nil{self.delegate?.upLoadContactsDelegateSchedule()}
            
            if status != 200
            {
                return
            }
            
            
        })
    }
    
 
    
    deinit 
    {
        
    }
    
}
