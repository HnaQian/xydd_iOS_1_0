//
//  UpDateConnectCell.swift
//  Limi
//
//  Created by guo chen on 15/10/13.
//  Copyright © 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class UpDateConnectCell: UITableViewCell
{
    @IBOutlet weak var radioButtom: UIImageView!
    
    @IBOutlet weak var headImageView: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var headLabel: UILabel!

    @IBOutlet weak var icloudImg: UIImageView!//云图标
    
    
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }
    
    
    
    func setPropertise(_ dataDict:NSMutableDictionary,shouldSelected:Bool)
    {
        if let img = dataDict["HeadImage"] as? UIImage
        {   
            self.headImageView.clipsToBounds = true
            
            self.headImageView.layer.cornerRadius = self.headImageView.iwidth/2
            
            self.headImageView.image = img
            
            self.headLabel.isHidden = true
            
        }else
        {
            self.headLabel.isHidden = false
            
            self.headLabel.clipsToBounds = true
            
            self.headLabel.layer.cornerRadius = self.headImageView.iwidth/2
            
            let index = dataDict["colorIndex"] as! Int
            
            headLabel.backgroundColor = UIColor(rgba:Constant.randomColor[index])    
    
            headLabel.text = Utils.subLastCharacter(dataDict["Name"] as? String)
        }
        
        let isCloud = dataDict["isCloud"] as! Bool
        
        icloudImg.isHidden = !(isCloud)

        let content = dataDict["Name"] as? String
        
        let font = UIFont.systemFont(ofSize: 15)//UIFont(descriptor:UIFontDescriptor(name:"", size: 13), size: 0)
        
        let size = NSString(string: content!).boundingRect(with: CGSize(width: CGFloat(MAXFLOAT), height: 20), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil).size
        
        self.nameLabel.text = content
        
        if size.width > 80
        {
            self.nameLabel.iwidth = 80
        }
        else
        {
            self.nameLabel.iwidth = size.width
        }

        
        icloudImg.ix = self.nameLabel.iright
        
        self.dateLabel.text =  dataDict["Birthday"] as? String
        let imageStr = shouldSelected ? "mchoose" : "add_gift_reminder_unchoose"
        radioButtom.frame = CGRect(x: 5, y: 22, width: 26, height: 26)
        radioButtom.image =  UIImage(named:imageStr)
        //radioButtom.contentMode = UIViewContentMode.Center
    }


    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
