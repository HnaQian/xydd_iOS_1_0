//
//  SendMsgManager.swift
//  Limi
//
//  Created by guo chen on 15/12/8.
//  Copyright © 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

import MessageUI


public enum SendMsgType:Int
{
    case weChat
    
    case message
}



class SendMsgManager: NSObject,MFMessageComposeViewControllerDelegate
{
    weak var delegate:UIViewController?
    
    var heartWishVC:HeartWishViewController!
    
    
    ///发送信息管理类
    ///
    /**
    sendMessageType:发送信息的途径：.WeChat:微信方式  .Message:短信方式
    message :       消息内容
    delegate:       代理：接收消息发送的结果
    */
    ///备注
    func sendMessage(_ sendMessageType:SendMsgType,message:String,mobile:String? = nil)
    {
        if sendMessageType == .weChat
        {
            let shareDict:NSDictionary = [
                LDSDKShareContentTextKey : message]
            let wechatShare : LDSDKWXServiceImpl = LDSDKManager.getShareService(LDSDKPlatformType.weChat) as! LDSDKWXServiceImpl
            
            wechatShare.share(withContent: shareDict as! [AnyHashable: Any], shareModule:1, onComplete: ({(success,error) in
                if error != nil{
                    Utils.showError(context: self.heartWishVC.view, errorStr: (error?.localizedDescription)!)
                }
                
            }))
            
        }else
        {
            if MFMessageComposeViewController.canSendText() == true
            {
                let messageVC:MFMessageComposeViewController = MFMessageComposeViewController()
    
                messageVC.body = message;//信息内容
                
                if let phone = mobile
                {
                    if phone.characters.count > 0
                    {
                        messageVC.recipients = [phone]//接收者
                    }
                }
                
                messageVC.messageComposeDelegate = self;
                
                heartWishVC.present(messageVC, animated: false, completion: nil)
                
            }else
            {
                Utils.showError(context: heartWishVC.view, errorStr: "该设备无短信功能")
            }
        }
    }
    
    
    
    
    //  MARK:短信发送结果代理
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult)
    {
        controller.dismiss(animated: true, completion: { () -> Void in
            
        })
    }
}


//AskFriendBirthdayViewController



class AskFriendBirthdaySendMsgManager: NSObject,MFMessageComposeViewControllerDelegate
{
    weak var delegate:UIViewController?
    
    var heartWishVC:AskFriendBirthdayViewController!
    
    ///发送信息管理类
    ///
    /**
     sendMessageType:发送信息的途径：.WeChat:微信方式  .Message:短信方式
     message :       消息内容
     delegate:       代理：接收消息发送的结果
     */
    ///备注
    func sendMessage(_ sendMessageType:SendMsgType,message:String,mobile:String? = nil)
    {
        if sendMessageType == .weChat
        {
            let shareDict:NSDictionary = [
                LDSDKShareContentTextKey : message]
            let wechatShare : LDSDKWXServiceImpl = LDSDKManager.getShareService(LDSDKPlatformType.weChat) as! LDSDKWXServiceImpl
            
            wechatShare.share(withContent: shareDict as! [AnyHashable: Any], shareModule:1, onComplete: ({(success,error) in
                if error != nil{
                    Utils.showError(context: self.heartWishVC.view, errorStr: (error?.localizedDescription)!)
                }
                
            }))
            
        }else
        {
            if MFMessageComposeViewController.canSendText() == true
            {
                let messageVC:MFMessageComposeViewController = MFMessageComposeViewController()
                
                messageVC.body = message;//信息内容
                
                if let phone = mobile
                {
                    if phone.characters.count > 0
                    {
                        messageVC.recipients = [phone]//接收者
                    }
                }
                
                messageVC.messageComposeDelegate = self;
                
                heartWishVC.present(messageVC, animated: false, completion: nil)
                
            }else
            {
                Utils.showError(context: heartWishVC.view, errorStr: "该设备无短信功能")
            }
        }
    }
    
    
    
    
    //  MARK:短信发送结果代理
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult)
    {
        controller.dismiss(animated: true, completion: { () -> Void in
            
        })
    }
}

