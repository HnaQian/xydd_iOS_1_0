//
//  StartGuidSettingView.swift
//  Limi
//
//  Created by guo chen on 15/11/17.
//  Copyright © 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit
import CoreData

public enum UpLoadInfoType : Int {
    
    case sex  = 1// 性别
    
    case birthday // 生日
    
    case nickname // 昵称
}



private var staticStartGuidSetting:StartGuidSettingView!

class StartGuidSettingView: UIView 
{    
    static var shareObject:StartGuidSettingView
    {
        if staticStartGuidSetting != nil
        {
            return staticStartGuidSetting
        }
        else 
        {
            let guidView = StartGuidSettingView()
            
            staticStartGuidSetting = guidView
            
            return guidView
        }
    }
    
  
    
    
    
    var gender: Int = 0       // 性别
    
    var birthday: String?   // 日期
    
    var birthdayType: Int = 0  // 日期类型
    
    var nickName: String?   // 昵称
    
    var avatar: String?     // 头像地址
    
    var beginner_guide_url:String?//设置完成Url
    
    var sexMarket: String?   // 设置性别引导字段
    
    var birthdayMarket: String?  // 设置生日引导字段
    
    var nickNameMarket: String?  // 设置昵称引导字段
    
    var addressMarket: String?   // 通讯录授权引导字段
    
    var noContactMarket: String? // 没有联系人引导字段
      
    
  
}


extension StartGuidSettingView {

    //上传用户信息
    func uploadInfo(_ infoType: UpLoadInfoType,andCompletionHandler completionHandler:@escaping (_ result:Bool)->Void)
    {   
        var postString: String?
        
        var parameters = [String: AnyObject]()

        switch infoType
        {
        case UpLoadInfoType.sex:
        
        postString = Constant.JTAPI.user_gender
        parameters["gender"] = self.gender as AnyObject?
            
        case UpLoadInfoType.birthday:
        
        postString = Constant.JTAPI.user_birthday
        parameters["birthday"] = String(self.birthday!) + " 00:00:00" as AnyObject?
        parameters["birthday_type"] = self.birthdayType as AnyObject?
            
        case UpLoadInfoType.nickname:
        
        postString = Constant.JTAPI.user_nick_name
        parameters["nick_name"] = self.nickName as AnyObject?
            
        }
        
        
        // 保存信息
       let _ = LMRequestManager.requestByDataAnalyses(context:self,URLString: postString!, parameters: parameters, isNeedUserTokrn: true, isShowErrorStatuMsg: true, isShowSuccessStatuMsg:true,isNeedHud: true,successHandler: {data, status, msg in
            if status != 200
            {
                completionHandler(false)
                return
            }  
                // 本地保存相关信息
                switch infoType
                {
                case UpLoadInfoType.sex:
                    let request: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
                    let datasource : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(request)!
                    if datasource.count > 0{
                        
                        let userInfo = datasource[0] as! User
                        userInfo.gender = Int64(self.gender)

                    }
                    else{
                        let entity = NSEntityDescription.entity(forEntityName: "User", in: CoreDataManager.shared.managedObjectContext)
                        request.entity = entity
                        do {
                            try! CoreDataManager.shared.managedObjectContext.fetch(request)
                            let user: User = NSEntityDescription.insertNewObject(forEntityName: "User", into: CoreDataManager.shared.managedObjectContext) as! User
                            user.gender = Int64(self.gender)
                        }
                    }
                    CoreDataManager.shared.save({Void in
                        completionHandler(true)
                        },failureHandler: {Void in
                            completionHandler(false)
                            
                    })
                    
                case UpLoadInfoType.birthday:
                    
                    let request: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
                    let datasource : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(request)!
                    if datasource.count > 0{
                        
                        let userInfo = datasource[0] as! User
                        userInfo.birthday = self.birthday
                        if self.birthdayType != 0 {
                            userInfo.birthday_type = Int64(self.birthdayType)
                        }
                    }
                    else{
                        let entity = NSEntityDescription.entity(forEntityName: "User", in: CoreDataManager.shared.managedObjectContext)
                        request.entity = entity
                        do {
                            try! CoreDataManager.shared.managedObjectContext.fetch(request)
                            let user: User = NSEntityDescription.insertNewObject(forEntityName: "User", into: CoreDataManager.shared.managedObjectContext) as! User
                            user.birthday = self.birthday
                            if self.birthdayType != 0 {
                                user.birthday_type = Int64(self.birthdayType)
                            }
                        }
                    }
                    CoreDataManager.shared.save({Void in
                        completionHandler(true)
                        },failureHandler: {Void in
                            completionHandler(false)
                            
                    })
                    
                case UpLoadInfoType.nickname:
                    
                    let request: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
                    let datasource : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(request)!
                    if datasource.count > 0{
                        
                        let userInfo = datasource[0] as! User
                        userInfo.nick_name = self.nickName
                    }
                    else{
                        let entity = NSEntityDescription.entity(forEntityName: "User", in: CoreDataManager.shared.managedObjectContext)
                        request.entity = entity
                        do {
                            try! CoreDataManager.shared.managedObjectContext.fetch(request)
                            let user: User = NSEntityDescription.insertNewObject(forEntityName: "User", into: CoreDataManager.shared.managedObjectContext) as! User
                            user.nick_name = self.nickName
                        }
                    }
                    CoreDataManager.shared.save({Void in
                        completionHandler(true)
                        },failureHandler: {Void in
                            completionHandler(false)
                            
                    })
                    
                }
            
            
        })
        
        
    }
    
    
}

