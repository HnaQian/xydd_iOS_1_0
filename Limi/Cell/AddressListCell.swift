//
//  AddressListCell.swift
//  Limi
//
//  Created by ZhiQiangMi on 15/12/18.
//  Copyright © 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class AddressListCell: UITableViewCell {

    @IBOutlet weak var checkView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addRessLabel: UILabel!

    func setData(_ item: AddressMode,isSelected : Bool){
        
        nameLabel.text = item.name + "      " + item.mobile
        
        let address1 = item.province + " " + item.city + " "
        let address2 = item.area + " " + item.address
        
        addRessLabel.lineBreakMode = NSLineBreakMode.byCharWrapping
        addRessLabel.text = address1 + address2
        
        if isSelected{
            checkView.image = UIImage(named: "choosed")
        }
        else{
            checkView.image = UIImage(named: "noChoose")
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
