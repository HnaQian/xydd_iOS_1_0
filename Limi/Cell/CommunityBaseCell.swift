//
//  CommunityBaseCell.swift
//  Limi
//
//  Created by maohs on 16/8/4.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//问答社区 基本cell
import UIKit

@objc protocol CommunityBaseCellDelegate:NSObjectProtocol {
    @objc optional func communityBaseCellPraiseButtonChanged(_ data:CommunityCommonModel,index:Int)
    @objc optional func communityBaseCellSkimButtonChanged(_ id:Int,type:Int,index:Int)
    @objc optional func communityBaseCellGoodsClicked(_ id:Int)
}

class CommunityBaseCell: UITableViewCell {
    internal let questionLabelFont =  Constant.common_F3_font
    internal let responseLabelFont = Constant.common_F5_font
    
    internal let questionLabelWidth = Constant.ScreenSizeV2.SCREEN_WIDTH - 60 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
    internal let responseLabelWidth = Constant.ScreenSizeV2.SCREEN_WIDTH - 150 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    weak var delegate:CommunityBaseCellDelegate?
    class var reuseIdentifier:String {return getReuseIdentifier()}
    
    //need child class override it
    class func getReuseIdentifier()->String {return "CommunityBaseCell"}
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = UITableViewCellSelectionStyle.none
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        self.clipsToBounds = true
        setUpUI()
    }
    
    //need child class override it
    func setUpUI() {}
    
    //need child class override it
    func refreshWithData(_ data:CommunityCommonModel,index:Int) {}
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
