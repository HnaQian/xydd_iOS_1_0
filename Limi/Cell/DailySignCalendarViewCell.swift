//
//  DailySignCalendarViewCell.swift
//  Limi
//
//  Created by maohs on 16/5/26.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}


public protocol DailySignCalendarFooterDelegate:NSObjectProtocol {
    func DailySignCalendarFooterClicked()
}

class DailySignCalendarViewCell: UICollectionViewCell {

    
//    static let width:CGFloat = (DailySignCalendarHeader.width - lineSpacing * 8)/7
    
    static let width:CGFloat = 38 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE

    static let height:CGFloat = width

    static let lineSpacing:CGFloat = 0
    static let interitemSpacing:CGFloat = (DailySignCalendarHeader.width - 7 * width)/9
    
    
    static let reuseIdentifier = "mdailySignCalendarViewCell"
    
    
    var contentLabel = UILabel()
    var bgImageView = UIImageView()
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setUpUI()
    }
    
    func setUpUI() {
        
//        self.clipsToBounds = true
//        self.backgroundColor = UIColor.brownColor()
        
        self.addSubview(bgImageView)
        self.addSubview(contentLabel)
        bgImageView.frame = CGRect(x: 0, y: 0, width: 38 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, height: 38 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE)
        
        contentLabel.frame = bgImageView.bounds
        contentLabel.font = Constant.Theme.Font_19
        contentLabel.textColor = UIColor(rgba: Constant.common_C8_color)
        contentLabel.textAlignment = .center
        contentLabel.text = ""
    }
    
    func reloadData(_ data:DailySignDate_listModel,currentDay:String) {
        contentLabel.text =  String(data.day)
        bgImageView.image = nil
        //天数是否大于当前天
        if data.day >= Int(currentDay) {
            //判断是否有特殊奖品
            if data.gift_tip != 0 {
                //有奖品 未签到
                contentLabel.textColor = UIColor(rgba: Constant.common_C12_color)
                bgImageView.image = UIImage(named: "signSpetial")
            }else {
                //无奖品 未签到
                contentLabel.textColor = UIColor(rgba: Constant.common_C2_color)
            }
            
            if data.day == Int(currentDay) {
                
                if data.gift_tip != 0 {
                    //有奖品 未签到
                    bgImageView.image = UIImage(named: "currentDaySpetial")
                }else {
                    //无奖品 未签到
                    bgImageView.image = UIImage(named: "red-dot")
                }
                
                if data.checkin_times != 0 {
                   //已签到
                    contentLabel.textColor = UIColor(rgba: Constant.common_C1_color)
                    bgImageView.image = UIImage(named: "signed")
                }
            }
            
        }else {
            //判断是否签到
            if data.checkin_status != 0 {
                //已签到
                contentLabel.textColor = UIColor(rgba: Constant.common_C1_color)
                bgImageView.image = UIImage(named: "signed")
            }else {
                //未签到
                contentLabel.textColor = UIColor(rgba: Constant.common_C8_color)
            }
            
        }
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class DailySignCalendarHeader: UICollectionReusableView {
    
    static let height:CGFloat = 98 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE + DailySignCalendarViewCell.interitemSpacing * 2
    
    static let width:CGFloat = Constant.ScreenSizeV1.SCREEN_WIDTH - 40 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE
    
    var titleView = UIView()
    
    var currentTimeLabel = UILabel()
    
    var creditLabel = UILabel()
    
    var weekView = CustomWeekView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setUpUI()
    }
    
    func setUpUI() {
        
        self.addSubview(titleView)
        titleView.backgroundColor = UIColor(rgba: "#ff5b5b")
        titleView.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV1.SCREEN_WIDTH  - 40 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, height: 60 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE)
        
        titleView.addSubview(currentTimeLabel)
        titleView.addSubview(creditLabel)
        
        currentTimeLabel.frame = CGRect(x: 18 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, y: 20 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, width: 145 + 20 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE , height: 20)
        currentTimeLabel.font = Constant.Theme.Font_19
        currentTimeLabel.textColor = UIColor(rgba: Constant.common_C12_color)
        
        creditLabel.frame = CGRect(x: 160 , y: 20 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, width: titleView.iwidth - 160 - 18 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, height: 20)
        creditLabel.icenterY = currentTimeLabel.icenterY
        creditLabel.font = Constant.Theme.Font_13
        creditLabel.textColor = UIColor(rgba: Constant.common_C12_color)
        creditLabel.textAlignment = .right
        creditLabel.adjustsFontSizeToFitWidth = true
        
        self.addSubview(weekView)
        weekView.frame = CGRect(x: 0, y: titleView.ibottom, width: 335 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, height: 38 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE)
        
        let seperateLineView = UIView()
        seperateLineView.frame = CGRect(x: 0, y: 97 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, width: titleView.iwidth, height: 1)
        seperateLineView.backgroundColor = UIColor(rgba: Constant.common_C10_color)
        
        self.addSubview(seperateLineView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    final class CustomWeekView:UIView {
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            setUpUI()
        }
        
        func setUpUI() {
            
            let titleAry = ["日","一","二","三","四","五","六"]

            for index in 0..<titleAry.count{
                
                let title = titleAry[index]
                
                let lbl = UILabel(frame:CGRect(x: 0,y: 0,width: DailySignCalendarViewCell.width,height: DailySignCalendarViewCell.height))
                
                lbl.text = title
                lbl.textAlignment = NSTextAlignment.center
                lbl.textColor = UIColor(rgba: Constant.common_C6_color)
                lbl.font = Constant.Theme.Font_19
                
                lbl.icenterY = (38 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE)/2
                lbl.ix = DailySignCalendarViewCell.interitemSpacing + 8 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE + CGFloat(index) * (lbl.iwidth + DailySignCalendarViewCell.interitemSpacing)
  
                self.addSubview(lbl)
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    
}

class DailySignCalendarFooter: UICollectionReusableView {
    
    static let height:CGFloat = 84 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE + DailySignCalendarViewCell.interitemSpacing * 2
    static let width:CGFloat = Constant.ScreenSizeV1.SCREEN_WIDTH  - 40 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE
    
    fileprivate var signButton = UIButton(type: UIButtonType.custom)
    fileprivate var hintLabel = UILabel()
    weak var delegate:DailySignCalendarFooterDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setUpUI()
    }
    
    func setUpUI() {
        
        let seperateLineView = UIView()
        self.addSubview(seperateLineView)
        seperateLineView.frame = CGRect(x: 0, y: DailySignCalendarViewCell.interitemSpacing * 2, width: Constant.ScreenSizeV1.SCREEN_WIDTH  - 40 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, height: 1)
        seperateLineView.backgroundColor = UIColor(rgba: Constant.common_C10_color)
        
        self.addSubview(hintLabel)
        hintLabel.frame = CGRect(x: 0, y: seperateLineView.ibottom + 10 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, width: seperateLineView.iwidth, height: 14 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE)
        hintLabel.textAlignment = .center
        hintLabel.font = Constant.Theme.Font_13
        hintLabel.textColor = UIColor(rgba: Constant.common_C7_color)
        hintLabel.isHidden = true
        
        self.addSubview(signButton)
        signButton.frame = CGRect(x: 10, y: seperateLineView.ibottom +  29 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, width: 150 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, height: 40 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE)
        signButton.icenterX = seperateLineView.icenterX
        signButton.setTitle("立即签到", for: UIControlState())
        signButton.setTitleColor(UIColor(rgba: Constant.common_C12_color), for: UIControlState())
        signButton.titleLabel?.font = Constant.Theme.Font_19
        signButton.backgroundColor = UIColor(rgba: Constant.common_C1_color)
        signButton.layer.cornerRadius = 5.0
        signButton.layer.masksToBounds = true
        signButton.addTarget(self, action: #selector(DailySignCalendarFooter.signButtonClicked(_:)), for: .touchUpInside)
        
    }
    
    @objc fileprivate func signButtonClicked(_ sender:AnyObject) {
        delegate?.DailySignCalendarFooterClicked()
    }
    
    func resetSignButton(_ data:DailySignModel,currentDay:String) {
        let today = Int(currentDay)
        if data.date_list![today! - 1].checkin_status != 0 {
            //已签到
            signButton.itop =  DailySignCalendarViewCell.interitemSpacing * 2 + 39 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE
            signButton.setTitle("已签到", for: UIControlState())
            signButton.setTitleColor(UIColor(rgba: Constant.common_C1_color), for: UIControlState())
            signButton.backgroundColor = UIColor(rgba: Constant.common_C9_color)
            signButton.isEnabled = false
            
            hintLabel.isHidden = false
            
            let checkinTimes = data.checkin_times
            let next_day = data.next_day
            let next_point = data.next_point
            
            let tempString = "已连续签到\(checkinTimes)天  再签到\(next_day)天可获得额外\(next_point)心意币"
            let attributedString1 = NSMutableAttributedString(string: tempString as String)
            let firstAttributes = [NSForegroundColorAttributeName: UIColor(rgba: Constant.common_C1_color)]
            attributedString1.addAttributes(firstAttributes, range: NSMakeRange(String(checkinTimes).characters.count + 11, String(next_day).characters.count))
            attributedString1.addAttributes(firstAttributes, range: NSMakeRange(attributedString1.length - 3 - String(next_point).characters.count, String(next_point).characters.count))
            hintLabel.attributedText = attributedString1
            
        }else {
            
            //未签到
            signButton.itop =  DailySignCalendarViewCell.interitemSpacing * 2 + 29 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE
            signButton.setTitle("立即签到", for: UIControlState())
            signButton.setTitleColor(UIColor(rgba: Constant.common_C12_color), for: UIControlState())
            signButton.backgroundColor = UIColor(rgba: Constant.common_C1_color)
            signButton.isEnabled = true
           
            hintLabel.isHidden = true
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
