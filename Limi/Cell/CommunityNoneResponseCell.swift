//
//  CommunityNoneResponseCell.swift
//  Limi
//
//  Created by maohs on 16/8/4.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//问答社区  没有回复的cell
import UIKit

class CommunityNoneResponseCell: CommunityBaseCell {

    fileprivate let contextView:UIView = {
       let view = UIView()
        view.backgroundColor = UIColor.white
        
        return view
    }()
    
    fileprivate let questionLabel:UILabel = {
       let lbl = UILabel()
        lbl.textColor = UIColor(rgba: Constant.common_C2_color)
        lbl.font = UIFont.systemFont(ofSize: Constant.common_F2_font)
        lbl.numberOfLines = 0
        return lbl
    }()
    
    fileprivate let noneReponseLabel:UILabel = {
       let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: Constant.common_F5_font)
        lbl.textColor = UIColor(rgba: Constant.common_C7_color)
        
        return lbl
    }()
    
    override class func getReuseIdentifier() ->String {return "CommunityNoneResponseCell"}
    
    override func setUpUI() {
        
        self.contentView.addSubview(contextView)
        for view in [questionLabel,noneReponseLabel]{
            contextView.addSubview(view)
        }
        questionLabel.font = UIFont.systemFont(ofSize: self.questionLabelFont)
        
        let scale = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        contextView.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(self)
            let _ = make.left.equalTo(self)
            let _ = make.right.equalTo(self)
            let _ = make.bottom.equalTo(self).offset(-20 * scale)
        }
        
        questionLabel.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(self).offset(40 * scale)
            let _ = make.left.equalTo(self).offset(30 * scale)
            let _ = make.right.equalTo(self).offset(-30 * scale)
            let _ = make.height.greaterThanOrEqualTo(20)
        }
        
        noneReponseLabel.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(questionLabel.snp_bottom).offset(40 * scale)
            let _ = make.left.equalTo(self).offset(30 * scale)
            let _ = make.right.equalTo(self).offset(-30 * scale)
            let _ = make.height.equalTo(15)
        }
    }
    
    override func refreshWithData(_ data:CommunityCommonModel,index:Int) {
        if data.is_nice == 2 {
            //加精
            questionLabel.text = " " + data.content
            let attachment = NSTextAttachment()
            attachment.image = UIImage(named: "essenceIcon")
            attachment.bounds = CGRect(x: 0, y: -3, width: 35, height: 18)
            let attStr = NSAttributedString(attachment: attachment)
            let mutableStr = NSMutableAttributedString(attributedString: questionLabel.attributedText!)
            mutableStr.insert(attStr, at: 0)
            questionLabel.attributedText = mutableStr
            
        }else {
            //未加精
            questionLabel.text = data.content
        }
        
//        let attributedString = NSMutableAttributedString(string: questionLabel.text! as String)
//        let paragraphStyle = NSMutableParagraphStyle()
//        paragraphStyle.lineSpacing = Constant.ScreenSizeV2.MARGIN_15
//        attributedString.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: NSMakeRange(0, (questionLabel.text?.characters.count)!))
//        questionLabel.attributedText = attributedString
        
        //1正常2删除
        let statusNum = (data.status as NSString).integerValue
       if statusNum == 2{
            noneReponseLabel.text = "抱歉，该问题隐含违规话题，已被删除"
        }else {
            noneReponseLabel.text = "等待送礼达人回复"
        }
        
    }

}
