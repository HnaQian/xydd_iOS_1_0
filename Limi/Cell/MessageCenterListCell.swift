//
//  MessageCenterListCell.swift
//  Limi
//
//  Created by 程巍巍 on 5/20/15.
//  Copyright (c) 2015 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class MessageCenterListCell: UITableViewCell {

    @IBOutlet var iconImageView: UIImageView!
    @IBOutlet var typeLabel: UILabel!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var contentLabel: UILabel!
    
    var isRead: Bool = false {
        didSet{
            let tintColor = isRead ? UIColor.lightGray : UIColor.darkText
            typeLabel.textColor = tintColor
            contentLabel.textColor = tintColor
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        for label in [typeLabel, timeLabel, contentLabel] {
            label?.font = Constant.CustomFont.Default(size: (label?.font.pointSize)!)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        iconImageView.layer.cornerRadius = iconImageView.frame.width/2
    }
}
