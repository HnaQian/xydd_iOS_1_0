//
//  DailySignCollectionViewCell.swift
//  Limi
//
//  Created by maohs on 16/5/25.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class DailySignCollectionViewCell: UICollectionViewCell {
    
    fileprivate var titleImage = UIImageView()
    
    fileprivate var bottomView = UIView()
    
    fileprivate var titleLabel = UILabel()
    
    fileprivate var contentLabel = UILabel()
    
    fileprivate var convertButton = UIButton(type: UIButtonType.custom)
    
    weak var delegate:DailySignCollectionViewCellDelegate?
    
    fileprivate var cellData:DetailListMode?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setUpUI()
    }
    
    func setUpUI() {
        
        self.addSubview(titleImage)
        titleImage.frame = CGRect(x: 0, y: 0, width: 172 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, height: 172 * Constant.ScreenSizeV1.SCREEN_HEIGHT_SCALE)
        
        self.addSubview(bottomView)
        bottomView.frame = CGRect(x: 0, y: titleImage.ibottom, width: 172 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, height: self.iheight - titleImage.ibottom)
        bottomView.backgroundColor = UIColor.white
        
        bottomView.addSubview(titleLabel)
        titleLabel.frame = CGRect(x: 11 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, y: 8 * Constant.ScreenSizeV1.SCREEN_HEIGHT_SCALE, width: 151 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, height: 10 )
        titleLabel.textAlignment = .left
        titleLabel.font = Constant.Theme.Font_13
        titleLabel.textColor = UIColor(rgba: Constant.common_C6_color)
        
        bottomView.addSubview(contentLabel)
        contentLabel.frame = CGRect(x: titleLabel.ileft, y: titleLabel.ibottom + 12 * Constant.ScreenSizeV1.SCREEN_HEIGHT_SCALE, width: titleLabel.iwidth, height: titleLabel.iheight)
        contentLabel.textAlignment = .left
        contentLabel.font = Constant.Theme.Font_13
        contentLabel.textColor = UIColor(rgba: Constant.common_C6_color)
        
        bottomView.addSubview(convertButton)
        convertButton.frame = CGRect(x: 110 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, y: 28 * Constant.ScreenSizeV1.SCREEN_HEIGHT_SCALE, width: 50 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, height: 24 * Constant.ScreenSizeV1.SCREEN_HEIGHT_SCALE)
        convertButton.setTitle("兑换", for: UIControlState())
        convertButton.setTitleColor(UIColor(rgba: Constant.common_C1_color), for: UIControlState())
        convertButton.titleLabel?.font = Constant.Theme.Font_13
        convertButton.layer.borderColor = UIColor(rgba: Constant.common_C1_color).cgColor
        convertButton.layer.borderWidth = 1.0
        convertButton.layer.cornerRadius = 5.0
        convertButton.layer.masksToBounds = true
        convertButton.addTarget(self, action: #selector(DailySignCollectionViewCell.convertButtonClicked(_:)), for: .touchUpInside)
        convertButton.isHidden = true
        
    }
    
    func refreshData(_ outData:DetailListMode,type:Int = 0) {
        self.cellData = outData
        
        let width:CGFloat = titleImage.iwidth;
        let height:CGFloat = titleImage.iheight;
        
        let imageScale : String = outData.img + "?imageView2/0/w/" + "\(Int(width * Constant.ScreenSizeV1.SCALE_SCREEN))" + "/h/" + "\(Int(height * Constant.ScreenSizeV1.SCALE_SCREEN))"
        print(imageScale)
        titleImage.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "Default_400_400"))
        titleLabel.text = outData.name
        let attributedString1 = NSMutableAttributedString(string: (String(outData.point) + "心意币") as String)
        let firstAttributes = [NSForegroundColorAttributeName: UIColor(rgba: Constant.common_C1_color)]
         attributedString1.addAttributes(firstAttributes, range: NSMakeRange(0, attributedString1.length - 3))
        contentLabel.attributedText = attributedString1
        
        if type == 1 {
            convertButton.isHidden = false
        }
        
    }
    
    @objc fileprivate func convertButtonClicked(_ sender:AnyObject) {
        let tempUserInfo = ["id":self.cellData!.id,"name": self.cellData!.name,"point":self.cellData!.point] as [String : Any]
      delegate?.dailySignCollectionViewCellClicked!(tempUserInfo as [String : AnyObject])
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}

class DailySignCollectionViewHeader: UICollectionReusableView,DailySignCalendarViewDelegate {
    
    let headerView = DailySignCalendarView()
    fileprivate let headerLabel:UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = Constant.Theme.Font_15
        label.textColor = UIColor(rgba: Constant.common_C2_color)
        label.text = "积分兑换"
        return label
    }()
    
    fileprivate let leftLine = UIView()
    fileprivate let rightLine = UIView()
    weak var delegate:DailySignCollectionViewHeaderDelegate?
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setUpUI()
    }
    
    func setUpUI() {
        
        self.addSubview(headerView)
        headerView.delegate = self
        headerView.frame = self.bounds
        
        headerView.addSubview(headerLabel)
        headerLabel.frame = CGRect(x: 10, y: 10, width: 80 , height: 15 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE)
        headerLabel.icenterX = headerView.icenterX
        headerLabel.itop = headerView.ibottom - 20 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE
        
        headerView.addSubview(leftLine)
        leftLine.backgroundColor = UIColor(rgba: Constant.common_C8_color)
        leftLine.frame = CGRect(x: 10, y: 10, width: 40, height: 1)
        leftLine.icenterY = headerLabel.icenterY
        leftLine.iright = headerLabel.ileft - 5
        
        headerView.addSubview(rightLine)
        rightLine.backgroundColor = UIColor(rgba: Constant.common_C8_color)
        rightLine.frame = CGRect(x: 10, y: 10, width: 40, height: 1)
        rightLine.icenterY = headerLabel.icenterY
        rightLine.ileft = headerLabel.iright + 5
    }
    
    func resetUI() {
        headerView.iheight = 590 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE
        headerLabel.itop = headerView.ibottom - 20 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE
        leftLine.icenterY = headerLabel.icenterY
        rightLine.icenterY = headerLabel.icenterY
    }
    
    func DailySignCalendarViewClicked() {
        delegate?.dailySignCollectionViewHeaderClicked()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class DailySignCollectionViewFooter: UICollectionReusableView {
    
    fileprivate let mailButton = UIButton(type: UIButtonType.custom)
    weak var delegate:DailySignCollectionViewFooterDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setUpUI()
    }
    
    func setUpUI() {
        self.addSubview(mailButton)
        mailButton.frame = CGRect(x: 10, y: 10 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, width: 150 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, height: 40 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE)
        mailButton.icenterX = self.icenterX
        mailButton.setTitle("去积分商城", for: UIControlState())
        mailButton.layer.cornerRadius = 5.0
        mailButton.layer.masksToBounds = true
        mailButton.backgroundColor = UIColor(rgba: Constant.common_C1_color)
        mailButton.titleLabel?.font = Constant.Theme.Font_19
        mailButton.setTitleColor(UIColor(rgba: Constant.common_C12_color), for: UIControlState())
        mailButton.addTarget(self, action: #selector(DailySignCollectionViewFooter.mailButtonClicked(_:)), for: .touchUpInside)
    }
    
    @objc fileprivate func mailButtonClicked(_ sender:AnyObject) {
        delegate?.dailySignCollectionViewFooterClicked()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class DetailListMode: BaseDataMode {
    /*
     {
     "id": 7,
     "name": "500元话费",
     "img": "http://up.xinyidiandian.com/150716120218542725.png",
     "point": 400
     }
 */
    
    override func customAnalyis(_ data: [String : AnyObject]) {
    
        if let id = JSON(data as AnyObject)["id"].int{
            self.id = id
        }
        
        if let name = JSON(data as AnyObject)["name"].string{
            self.name = name
        }
        
        if let img = JSON(data as AnyObject)["img"].string{
            self.img = img
        }
        
        if let point = JSON(data as AnyObject)["point"].int{
            self.point = point
        }
    }
    
    var id = NULL_INT
    var name = NULL_STRING
    var img = NULL_STRING
    var point = NULL_INT
}

@objc public protocol DailySignCollectionViewCellDelegate:NSObjectProtocol {
    @objc optional func dailySignCollectionViewCellClicked(_ userInfo:[String:AnyObject])
}

public protocol DailySignCollectionViewHeaderDelegate:NSObjectProtocol {
    func dailySignCollectionViewHeaderClicked()
}

public protocol DailySignCollectionViewFooterDelegate:NSObjectProtocol {
    func dailySignCollectionViewFooterClicked()
}
