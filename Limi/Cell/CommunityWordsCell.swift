//
//  CommunityCommonGoodsCell.swift
//  Limi
//
//  Created by maohs on 16/8/4.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//问答社区 带有文字+商品cell
import UIKit

class CommunityWordsCell: CommunityBaseCell {

    fileprivate let scale = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
    let bannerViewHeight = (316 + 30 + 40) * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE + 14
    fileprivate let authorImageWidth = 80 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
    
    fileprivate var goodsAry = [GoodsModel]()
    fileprivate var wordsModel:CommunityWordsModel!
    fileprivate var index = 0
    
    fileprivate let contextView:UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = UIColor.white
        
        return view
    }()
    
    fileprivate let questionLabel:UILabel = {
        let lbl = UILabel()
        lbl.numberOfLines = 0
        lbl.textColor = UIColor(rgba: Constant.common_C2_color)
        lbl.font = UIFont.systemFont(ofSize: Constant.common_F2_font)
        
        return lbl
    }()
    
    //达人信息
    fileprivate let authorInfoLabel:UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: Constant.common_F5_font)
        lbl.textColor = UIColor(rgba: Constant.common_C7_color)
        
        return lbl
    }()
    
    fileprivate let authorImage:UIImageView = {
        let image = UIImageView()
        image.layer.cornerRadius = 40 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        image.layer.masksToBounds = true
        image.image = UIImage(named: "accountIcon")
        
        return image
    }()
    
    //达人回复
    fileprivate let authorResponseLabel:UILabel = {
        let lbl = UILabel()
        lbl.numberOfLines = 0
        lbl.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        lbl.textColor = Constant.Theme.Color14
        
        return lbl
    }()
   
    fileprivate let foldLabel:UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor(rgba: "#7ecefd")
        lbl.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        lbl.isUserInteractionEnabled = true
        
        return lbl
    }()
    
    //浏览 点赞
    fileprivate let skimLabel:UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .right
        lbl.font = UIFont.systemFont(ofSize: 12)
        lbl.textColor = UIColor(rgba: Constant.common_C7_color)
        
        return lbl
    }()
    
    fileprivate let praiseButton:UIButton = {
        let btn = UIButton(type: .custom)
        return btn
    }()
    
    fileprivate let praiseImage:UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "praiseNormal")
        return image
    }()
    
    fileprivate let praiseLabel:UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: 12)
        lbl.textColor = UIColor(rgba: Constant.common_C7_color)
        lbl.textAlignment = .right
        
        return lbl
    }()
    
    //产品列表
    fileprivate let bannerView = UIView()
    fileprivate let bannerLine:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(rgba: Constant.common_separatLine_color)
        
        return view
    }()
    
    fileprivate let bannerTitleLabel:UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .left
        lbl.textColor = Constant.Theme.Color14
        lbl.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        
        return lbl
    }()
    
    fileprivate var banner:LMHorizontalTableView!
    override class func getReuseIdentifier() ->String {return "CommunityWordsCell"}
    
    override func setUpUI() {
        
        self.contentView.addSubview(contextView)
        contextView.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(self)
            let _ = make.left.equalTo(self)
            let _ = make.right.equalTo(self)
            let _ = make.bottom.equalTo(self).offset(-20 * scale)
        }
        
        for view in [questionLabel,authorInfoLabel,authorImage,authorResponseLabel,foldLabel,skimLabel,praiseButton] {
            contextView.addSubview(view)
        }
        
        questionLabel.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(self).offset(40 * scale)
            let _ = make.left.equalTo(self).offset(30 * scale)
            let _ = make.right.equalTo(self).offset(-30 * scale)
            let _ = make.height.greaterThanOrEqualTo(20)
        }
        
        authorInfoLabel.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(questionLabel.snp_bottom).offset(30 * scale)
            let _ = make.left.equalTo(self).offset(30 * scale)
            let _ = make.right.equalTo(self).offset(-30 * scale)
            let _ = make.height.equalTo(12)
        }
        
        authorImage.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(authorInfoLabel.snp_bottom).offset(30 * scale)
            let _ = make.left.equalTo(self).offset(30 * scale)
            let _ = make.width.equalTo(80 * scale)
            let _ = make.height.equalTo(80 * scale)
        }
        
        authorResponseLabel.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(authorInfoLabel.snp_bottom).offset(25 * scale)
            let _ = make.left.equalTo(authorImage.snp_right).offset(20 * scale)
            let _ = make.right.equalTo(self).offset(-30 * scale)
            let _ = make.height.lessThanOrEqualTo(40)
        }
        
        foldLabel.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(authorResponseLabel.snp_bottom).offset(5 * scale)
            let _ = make.left.equalTo(authorImage.snp_right).offset(20 * scale)
            let _ = make.width.equalTo(80)
            let _ = make.height.equalTo(13)
        }
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(CommunityWordsCell.foldLabelTaped))
        foldLabel.addGestureRecognizer(tapGesture)

        //浏览 点赞
        praiseButton.addSubview(praiseLabel)
        praiseButton.addSubview(praiseImage)
        praiseButton.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(foldLabel.snp_bottom).offset(35 * scale)
            let _ = make.right.equalTo(self)
            let _ = make.width.equalTo(80)
            let _ = make.height.equalTo(40 * scale)
        }
        
        praiseLabel.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(praiseButton).offset(4 * scale)
            let _ = make.right.equalTo(praiseButton).offset(-30 * scale)
            let _ = make.height.equalTo(15)
        }
        
        praiseImage.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(praiseButton)
            let _ = make.right.equalTo(praiseLabel.snp_left).offset(-4 * scale)
            let _ = make.width.equalTo(15)
            let _ = make.height.equalTo(15)
        }

        skimLabel.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(foldLabel.snp_bottom).offset(40 * scale)
            let _ = make.right.equalTo(praiseButton.snp_left).offset(-20 * scale)
            let _ = make.left.equalTo(self).offset(30 * scale)
            let _ = make.height.equalTo(12)
        }
        
        praiseButton.addTarget(self, action: #selector(CommunityWordsCell.praiseButtonClicked), for: .touchUpInside)
        
        //产品
        contextView.addSubview(bannerView)
        bannerView.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(skimLabel.snp_bottom).offset(20 * scale)
            let _ = make.left.equalTo(self)
            let _ = make.right.equalTo(self)
            let _ = make.height.equalTo(bannerViewHeight)
        }
        
        let tempFrame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH - 60 * scale, height: SubCell.cell_height)
        banner = LMHorizontalTableView(frame: tempFrame)
        banner.dataSource = self
        banner.delegate = self
        for view in [bannerLine,bannerTitleLabel,banner] {
            bannerView.addSubview(view)
        }
        
        bannerLine.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(bannerView)
            let _ = make.left.equalTo(self).offset(30 * scale)
            let _ = make.right.equalTo(self).offset(-30 * scale)
            let _ = make.height.equalTo(1)
        }
        
        bannerTitleLabel.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(bannerView).offset(30 * scale)
            let _ = make.left.equalTo(self).offset(30 * scale)
            let _ = make.right.equalTo(self).offset(-30 * scale)
            let _ = make.height.equalTo(14)
        }
        
        banner.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(bannerTitleLabel.snp_bottom).offset(40 * scale)
            let _ = make.left.equalTo(self).offset(30 * scale)
            let _ = make.width.equalTo(Constant.ScreenSizeV2.SCREEN_WIDTH - 60 * scale)
            let _ = make.height.equalTo(SubCell.cell_height)
        }
        
        bannerView.isHidden = true
    }

    func foldLabelTaped() {
        foldLabel.text =  wordsModel.isRetract ? "收起" : "显示全部"
        if wordsModel.isRetract {
            //显示收起，表示想查看全部
            Statistics.count(Statistics.Answers.answers_openanswer_click)
        }else {
            Statistics.count(Statistics.Answers.answers_offanswer_click)
        }
        delegate?.communityBaseCellSkimButtonChanged!(wordsModel.id,type:1,index:self.index)
    }
    
    func praiseButtonClicked() {
        if UserInfoManager.didLogin {
            if wordsModel.is_like == 1 {
                //已经点赞，再点击表示取消
                wordsModel.total_like_num -= 1
                wordsModel.is_like = 2
                praiseLabel.text = " " + String(wordsModel.total_like_num)
                praiseImage.image = UIImage(named: "praiseNormal")
                praiseLabel.textColor = UIColor(rgba: Constant.common_C7_color)
            }else {
                //没有点赞，再点击表示点赞
                wordsModel.total_like_num += 1
                wordsModel.is_like = 1
                praiseLabel.text = " " + String(wordsModel.total_like_num)
                praiseImage.image = UIImage(named: "praiseSelected")
                praiseLabel.textColor = UIColor(rgba: Constant.common_C1_color)
            }
        }
        Statistics.count(Statistics.Answers.answers_like_click)
        delegate?.communityBaseCellPraiseButtonChanged!(wordsModel,index:index)
    }
    
    override func refreshWithData(_ data:CommunityCommonModel,index:Int) {
        self.index = index
        if let tempWordsData = data as? CommunityWordsModel {
            wordsModel = tempWordsData
        }else {
            return
        }
        
        if wordsModel.is_nice == 2 {
            //加精
            questionLabel.text =  " " + wordsModel.content
            let attachment = NSTextAttachment()
            attachment.image = UIImage(named: "essenceIcon")
            attachment.bounds = CGRect(x: 0, y: -3, width: 35, height: 18)
            let attStr = NSAttributedString(attachment: attachment)
            let mutableStr = NSMutableAttributedString(attributedString: questionLabel.attributedText!)
            mutableStr.insert(attStr, at: 0)
            questionLabel.attributedText = mutableStr
            
        }else {
            //未加精
            questionLabel.text = data.content
        }
        
        //更改行距
//        let attributedString = NSMutableAttributedString(string: questionLabel.text! as String)
//        let paragraphStyle = NSMutableParagraphStyle()
//        paragraphStyle.lineSpacing = Constant.ScreenSizeV2.MARGIN_15
//        attributedString.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: NSMakeRange(0, (questionLabel.text?.characters.count)!))
//        questionLabel.attributedText = attributedString
        
        if let name = wordsModel.master?.name {
            authorInfoLabel.text = name
        }
        
        if let intro = wordsModel.master?.intro {
            if intro.characters.count > 0 {
                authorInfoLabel.text = authorInfoLabel.text! + "，" + intro
            }
        }
        
        if let url = URL(string: Utils.scaleImage((wordsModel.master?.avatar)!, width:authorImageWidth * Constant.ScreenSizeV2.SCALE_SCREEN, height: authorImageWidth * Constant.ScreenSizeV2.SCALE_SCREEN)){
            authorImage.af_setImageWithURL(url, placeholderImage: UIImage(named: "accountIcon"))
        }

        authorResponseLabel.text = wordsModel.sns_reply?.content
        
//        let attributedString1 = NSMutableAttributedString(string: authorResponseLabel.text! as String)
//        let paragraphStyle1 = NSMutableParagraphStyle()
//        paragraphStyle1.lineSpacing = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * 10
//        attributedString1.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle1, range: NSMakeRange(0, (authorResponseLabel.text?.characters.count)!))
//        authorResponseLabel.attributedText = attributedString1
        
        foldLabel.text = wordsModel.isRetract ? "显示全部" : "收起"
        refreshPosition(wordsModel)
        skimLabel.text = "浏览数 " + "\(wordsModel.total_view_num)"
        
        praiseLabel.text = " " +  String(wordsModel.total_like_num)
        if wordsModel.is_like == 1 {
            praiseImage.image = UIImage(named: "praiseSelected")
            praiseLabel.textColor = UIColor(rgba: Constant.common_C1_color)
        }else {
            praiseImage.image = UIImage(named: "praiseNormal")
            praiseLabel.textColor = UIColor(rgba: Constant.common_C7_color)
        }
        
        if wordsModel.goods_ids.count > 0 {
            bannerView.isHidden = false
            self.bannerTitleLabel.text = "回答中提到的礼物"
            self.goodsAry = [GoodsModel]()
            self.goodsAry = wordsModel.goods_ids
            self.banner.reloadData()
        }else {
            bannerView.isHidden = true
        }

    }
    
    func refreshPosition(_ data:CommunityCommonModel) {
        var tempRect = CGRect.zero
        if data.isRetract {
            let tempAttributedText = NSAttributedString(string: (data.sns_reply?.content)!, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: Constant.common_F4_font)])
            tempRect = tempAttributedText.boundingRect(with: CGSize(width: Constant.ScreenSizeV2.SCREEN_WIDTH - 160 * scale, height: 40), options: NSStringDrawingOptions.usesLineFragmentOrigin, context: nil)
        }else {
            let tempAttributedText = NSAttributedString(string: (data.sns_reply?.content)!, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: Constant.common_F4_font)])
            tempRect = tempAttributedText.boundingRect(with: CGSize(width: Constant.ScreenSizeV2.SCREEN_WIDTH - 160 * scale, height: CGFloat.greatestFiniteMagnitude), options: NSStringDrawingOptions.usesLineFragmentOrigin, context: nil)
        }
        authorResponseLabel.snp_updateConstraints { (make) in
            let _ = make.height.equalTo(tempRect.size.height + MARGIN_8)
        }
        foldLabel.snp_updateConstraints { (make) in
            let _ = make.top.equalTo(authorResponseLabel.snp_bottom).offset(5 * scale)
        }
        
        praiseButton.snp_updateConstraints { (make) in
            let _ = make.top.equalTo(foldLabel.snp_bottom).offset(35 * scale)
        }
        skimLabel.snp_updateConstraints { (make) in
            let _ = make.top.equalTo(foldLabel.snp_bottom).offset(40 * scale)
        }
        bannerView.snp_updateConstraints { (make) in
            let _ = make.top.equalTo(skimLabel.snp_bottom).offset(20 * scale)
        }
    }
}

extension CommunityWordsCell:LMHorizontalTableViewDataSource,LMHorizontalTableViewDelegate {
    func numberOfRows(_ horizontalTableView: LMHorizontalTableView) -> Int {
        return self.goodsAry.count
    }
    
    func horizontalTableView(_ horizontalTableView: LMHorizontalTableView, cellForRow row: Int, reuseCell: ((_ reuseInfentifer: String) -> LMHorizontalTableViewCell?)) -> LMHorizontalTableViewCell {
        var cell:SubCell? = reuseCell(SubCell.identifier) as? SubCell
        if cell == nil {
            cell = SubCell(reuseIdentifier:SubCell.identifier)
        }
        cell!.reloadData(goodsAry[row])
        return cell!
    }
    
    func horizontalTableView(_ horizontalTableView: LMHorizontalTableView, widthForRow row: Int) -> CGFloat {
        return SubCell.cell_width
    }
    
    func horizontalTableView(_ horizontalTableView: LMHorizontalTableView, heightForRow row: Int) -> CGFloat {
        return SubCell.cell_height
    }
    
    func horizontalTableView(_ horizontalTableView: LMHorizontalTableView, didSelectRow row: Int) {
        Statistics.count(Statistics.Answers.answers_giftlist_click, andAttributes: ["gid":String(self.goodsAry[row].gid)])
        delegate?.communityBaseCellGoodsClicked!(self.goodsAry[row].gid)
    }
    
}

class SubCell:LMHorizontalTableViewCell {
    static let identifier = "SubCell"
    static let cell_width = 250 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
    static let cell_height = 316 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
    
    //商品图片
    let goodsImageView:UIImageView = {
        let imgView = UIImageView()
        imgView.image = UIImage(named: "Default_370_340")
        imgView.clipsToBounds = true
        
        return imgView
    }()
    
    //心意价
    let priceLabel:UILabel = {
        let label = UILabel()
        label.textColor = UIColor(rgba: "#EE5151")
        label.textAlignment = .right
        label.font = Constant.Theme.Font_11
        return label
    }()
    
    //市场价
    let marketPriceLabel:StrikeThroughLabel = {
        let label = StrikeThroughLabel()
        label.textColor = UIColor(rgba: Constant.common_C7_color)
        label.textAlignment = .left
        label.font = Constant.Theme.Font_10
        label.strikeThroughEnabled = true
        return label
    }()
    
    //商品名称
    let goodsLabel:UILabel = {
        let label = UILabel()
        label.textColor = UIColor(rgba: "#666666")
        label.textAlignment = .center
        label.font = Constant.Theme.Font_13
        return label
    }()
    
    var goods_url:String?
    
    override init(reuseIdentifier: String) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        setupUI()
    }
    
    
    func reloadData(_ data:GoodsModel){
        if let url = URL(string: Utils.scaleImage((data.goods_image), width:goodsImageView.iwidth * Constant.ScreenSizeV2.SCALE_SCREEN, height: goodsImageView.iheight * Constant.ScreenSizeV2.SCALE_SCREEN)){
            self.goodsImageView.af_setImageWithURL(url, placeholderImage: UIImage(named: "Default_370_340"))
        }
        
        self.priceLabel.text = "￥" + "\(data.discount_price)"
        if data.market_price == 0 {
            self.marketPriceLabel.removeFromSuperview()
            priceLabel.center.x = goodsImageView.center.x
        }else{
            self.marketPriceLabel.text = "\(data.price)"
        }
        
        self.goodsLabel.text = data.goods_name
        
    }
    func setupUI() {
        
        
        for view in [goodsImageView,priceLabel,marketPriceLabel,goodsLabel] {
            self.addSubview(view)
            view.frame = CGRect.zero
        }
        
        goodsImageView.iwidth = 195 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        goodsImageView.iheight = goodsImageView.iwidth
        
        priceLabel.iy = goodsImageView.ibottom + 10 * Constant.ScreenSize.SCREEN_WIDTH_SCALE
        priceLabel.iheight = 16 * Constant.ScreenSize.SCREEN_WIDTH_SCALE
        priceLabel.iwidth = goodsImageView.iwidth / 2 - 2
        
        marketPriceLabel.iy = priceLabel.iy
        marketPriceLabel.iheight = priceLabel.iheight
        marketPriceLabel.ileft = priceLabel.iright + 4
        marketPriceLabel.iwidth = priceLabel.iwidth
        
        goodsLabel.iy = marketPriceLabel.ibottom
        goodsLabel.iheight = 16 * Constant.ScreenSize.SCREEN_WIDTH_SCALE
        goodsLabel.iwidth = goodsImageView.iwidth
        
        self.iwidth = goodsImageView.iwidth
        self.iheight = goodsLabel.ibottom + 18 * Constant.ScreenSize.SCREEN_WIDTH_SCALE
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }


}
