//
//  RechargeCell.swift
//  Limi
//
//  Created by Richie on 16/5/25.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class RechargeCell: UICollectionViewCell {
    
    static let width:CGFloat = (Constant.ScreenSizeV1.SCREEN_WIDTH - lineSpacing * 4)/3.0;
    
    static let height:CGFloat = width * 64/104;
    
    static let lineSpacing:CGFloat = MARGIN_8
    static let interitemSpacing:CGFloat = MARGIN_8
    
    static let reuseIdentifier = "RechargeCell_reuseIdentifier"
    
    fileprivate let subContentView = UIView()
    
    //面值
    fileprivate let faceValueLbl:UILabel = {

        let lbl = UILabel()
        lbl.font = Constant.Theme.Font_21
        lbl.textColor = Constant.Theme.Color13
        lbl.textAlignment = NSTextAlignment.center
        lbl.frame = CGRect(x: 0, y: 0, width: RechargeCell.width, height: 30)
        return lbl
    }()
    
    //实际售价
    fileprivate let sellPriceLbl:UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = NSTextAlignment.center
        lbl.font = Constant.Theme.Font_13
        lbl.frame = CGRect(x: 0, y: 0, width: RechargeCell.width, height: 25)
        
        return lbl
    }()
    
    //特惠图标
    fileprivate let icon:UIImageView = {
        let img = UIImageView()
        
        return img
    }()
    
    
    //次为：灰色，黑色，红色
    fileprivate let colors = [UIColor(rgba:Constant.common_C8_color),UIColor(rgba:Constant.common_C6_color),UIColor(rgba:Constant.common_C1_color)]
    
    fileprivate let discIcons = ["recharge_disc_gray","recharge_disc_black","recharge_disc"]
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    //刷新数据
    func refreshData(_ chargeModel:RechargeModel){
        
        faceValueLbl.text = chargeModel.name
        
        if chargeModel.price != 0{
            faceValueLbl.icenterY = RechargeCell.height/2 - MARGIN_8
            sellPriceLbl.iy = faceValueLbl.ibottom - MARGIN_4
            sellPriceLbl.text = "仅售:\((NSString(format: "%.2f", chargeModel.price) as String))元"
        }else{
            sellPriceLbl.text = ""
            faceValueLbl.icenterY = RechargeCell.height/2
        }
        
        self.icon.isHidden = chargeModel.is_discount == 2
        
        if chargeModel.canSelected{
            //选中
            if chargeModel.didSelected{
                refreshColor(2,isDiscount: chargeModel.is_discount == 1)
                //未选中
            }else{
                refreshColor(1,isDiscount: chargeModel.is_discount == 1)
            }
        }else{
            //不可选
            refreshColor(0,isDiscount: chargeModel.is_discount == 1)
        }
    }
    
    
    //刷新颜色
    fileprivate func refreshColor(_ colorType:Int,isDiscount:Bool){
        
        subContentView.layer.borderWidth = 0.5
        subContentView.layer.borderColor = colors[colorType].cgColor
        
        faceValueLbl.textColor = colors[colorType]
        sellPriceLbl.textColor = colors[colorType]
        
        icon.isHidden = !isDiscount
        
        if isDiscount{icon.image = UIImage(named: discIcons[colorType])}
    }
    
    
    
    fileprivate func setupUI(){
        
        self.addSubview(subContentView)
        
        subContentView.frame = CGRect(x: 0, y: 0, width: RechargeCell.width, height: RechargeCell.height)
        subContentView.layer.cornerRadius = 3
        
        [icon,faceValueLbl,sellPriceLbl].forEach { (view) in
            subContentView.addSubview(view)
        }
        
        icon.frame = CGRect(x: 0, y: 0, width: 28 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE, height: 14 * Constant.ScreenSizeV1.SCREEN_WIDTH_SCALE)
        
        faceValueLbl.center = CGPoint(x: RechargeCell.width/2, y: RechargeCell.height/2 - MARGIN_8)
        
        sellPriceLbl.center = CGPoint(x: RechargeCell.width/2, y: sellPriceLbl.ibottom + sellPriceLbl.iheight/2)
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
