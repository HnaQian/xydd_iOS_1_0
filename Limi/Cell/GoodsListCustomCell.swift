//
//  GoodsListCustomCell.swift
//  Limi
//
//  Created by ZhiQiangMi on 15/11/24.
//  Copyright © 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class GoodsListCustomCell: UICollectionViewCell {
    
    static let reuseIdentifier = "GoodsListCustomCell"
    fileprivate let width = 366 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
    
    let data : JSON = []
    fileprivate let titleImage:UIImageView = {
        let image = UIImageView()
        image.contentMode = UIViewContentMode.scaleAspectFill
        return image
    }()
    
    fileprivate let contentLabel:UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: 14)
        lbl.textColor = UIColor(rgba: Constant.common_C6_color)
        lbl.textAlignment = .center
        return lbl
    }()
    
    fileprivate let priceLabel:UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.boldSystemFont(ofSize: Constant.common_F4_font)
        lbl.textColor = UIColor(rgba: Constant.common_C2_color)
        lbl.textAlignment = .right
        return lbl
    }()
    fileprivate let marketPriceLabel: StrikeThroughLabel = {
        let lbl = StrikeThroughLabel()
        lbl.strikeThroughEnabled = true
        lbl.font = UIFont.systemFont(ofSize: 12)
        lbl.textColor = UIColor(rgba: Constant.common_C8_color)
        lbl.textAlignment = .left
        return lbl
    }()

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setUpUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUpUI() {
        self.backgroundColor = UIColor.white
        self.clipsToBounds = true
        self.contentView.addSubview(titleImage)
        self.contentView.addSubview(contentLabel)
        self.contentView.addSubview(priceLabel)
        self.contentView.addSubview(marketPriceLabel)
        
        let scale = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        titleImage.frame = CGRect(x: 0, y: 0, width: self.width, height: self.width)
        contentLabel.frame = CGRect(x: 20 * scale, y: titleImage.ibottom + 20 * scale, width: self.width - 40 * scale, height: 15)
        priceLabel.frame = CGRect(x: 0, y: contentLabel.ibottom + 20 * scale, width: self.width / 2 - 7 * scale, height: 17)
        marketPriceLabel.frame = CGRect(x: priceLabel.iright + 14 * scale, y: contentLabel.ibottom + 20 * scale, width: self.width / 2, height: 17)
        marketPriceLabel.icenterY = priceLabel.icenterY
        
    }
    
    func reloadData(_ data: GoodsModel?) {
        
        if let name = data?.goods_name {
            contentLabel.text = name
        }
        
        if let imageUrl = data?.goods_image {
            let imageScale : String = imageUrl + "?imageView2/0/w/" + "\(Int(titleImage.iwidth * Constant.ScreenSize.SCALE_SCREEN))" + "/h/" + "\(Int(titleImage.iwidth * Constant.ScreenSize.SCALE_SCREEN))"
            titleImage.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "Default_400_400"))
        }
       
        if let price = data?.discount_price {
            if price > 0{
                let floatPrice = roundf(Float(price))
                if floatPrice == Float(price) {
                    priceLabel.text = String(format: "￥%.0f", price)
                }
                else{
                    let priceStr1 : NSString = String(format: "%.2f", price) as NSString
                    let priceStr2 : NSString = String(format: "%.1f", price) as NSString
                    let floatPrice1 : Float = priceStr1.floatValue
                    let floatPrice2 : Float = priceStr2.floatValue
                    if floatPrice1 == floatPrice2{
                        priceLabel.text = String(format: "￥%.1f", price)
                    }
                    else{
                        priceLabel.text = String(format: "￥%.2f", price)
                    }
                    
                }
            }else{
                priceLabel.text = "￥0"
            }
        }
            
        if let marketPrice = data?.price {
            if marketPrice != data?.discount_price && marketPrice > 0{
                marketPriceLabel.isHidden = false
                let floatPrice = roundf(Float(marketPrice))
                if floatPrice == Float(marketPrice) {
                    marketPriceLabel.text = String(format: "￥%.0f", marketPrice)
                }
                else{
                    let priceStr1 : NSString = String(format: "%.2f", marketPrice) as NSString
                    let priceStr2 : NSString = String(format: "%.1f", marketPrice) as NSString
                    let floatPrice1 : Float = priceStr1.floatValue
                    let floatPrice2 : Float = priceStr2.floatValue
                    if floatPrice1 == floatPrice2{
                        marketPriceLabel.text = String(format: "￥%.1f", marketPrice)
                    }
                    else{
                        marketPriceLabel.text = String(format: "￥%.2f", marketPrice)
                    }
                    
                }
               priceLabel.iwidth = self.width / 2 - 7 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
               marketPriceLabel.ileft = priceLabel.iwidth + 14 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
                
                priceLabel.textAlignment = .right
                marketPriceLabel.textAlignment = .left
            }
            else{
                priceLabel.iwidth = self.width
                priceLabel.textAlignment = .center
                marketPriceLabel.isHidden = true
            }

        }
        
    }

}
