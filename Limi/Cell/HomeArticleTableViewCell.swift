//
//  HomeArticleTableViewCell.swift
//  Limi
//
//  Created by Richie on 16/6/12.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//攻略列表的普通cell
import UIKit

class HomeArticleTableViewCell: HomeBaseTableViewCell {
    
    
    //商品图片
    fileprivate let goodsImageView:UIImageView = {
        let img = UIImageView()
        img.contentMode = UIViewContentMode.scaleAspectFill
        img.clipsToBounds  = true
        return img
    }()
    
    //标签 位于攻略图的左上角的“NEW”标签
    fileprivate let articleTagLbl:UILabel = {
        let lbl = UILabel()
        lbl.font = Constant.Theme.Font_15
        lbl.textColor = Constant.Theme.Color12
        lbl.textAlignment = NSTextAlignment.center
        lbl.backgroundColor = Constant.Theme.Color13
        lbl.text = "NEW"
        lbl.isHidden = true
        return lbl
    }()
    
    //副标题
    fileprivate let subTitleLbl:UILabel = {
        let lbl = UILabel()
        lbl.font = Constant.Theme.Font_13
        lbl.textColor = Constant.Theme.Color7
        return lbl
    }()
    
    //喜欢的数量
    fileprivate let favCountLbl:UILabel = {
        let lbl = UILabel()
        lbl.font = Constant.Theme.Font_13
        lbl.textColor = Constant.Theme.Color7
        return lbl
    }()
    //标题
    fileprivate let titleLbl:UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont(name: "Helvetica-Bold", size: Constant.Theme.FontSize_15)
        lbl.textColor = Constant.Theme.Color13
        return lbl
    }()
    
    //喜欢的btn
    fileprivate let favBtn:UIButton = {
        let btn = UIButton()
        return btn
    }()
    
    static let subColor:UIColor = UIColor(red: 102, green: 102, blue: 102, alpha: 1)
    
    //标签label
    fileprivate let tagLbl:UILabel = {
        
        let lbl = UILabel()
        lbl.font = Constant.Theme.Font_11
        lbl.textColor = Constant.Theme.Color7
        lbl.layer.borderColor = Constant.Theme.Color7.cgColor
        lbl.layer.borderWidth = 0.5
        
        return lbl
    }()

    //30 + 400 + 30 + 30 + 8 + 26 + 30
    override class func getheight() ->CGFloat{return 554 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE + Constant.ScreenSizeV2.MARGIN_15}
    
    override class func getReuseIdentifier()->String{return "HomeArticleTableViewCell"}
    
    override func refreshWithData(_ dataModel: BaseDataMode) {
        
        print(Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
        
        if let model =  dataModel as? HomeCommonArticleModel {

            if let url = URL(string: Utils.scaleImage(model.cover, width: Constant.ScreenSizeV2.SCREEN_WIDTH * 3, height: Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * 400 * 3)){
                goodsImageView.af_setImageWithURL(url, placeholderImage: UIImage(named: "new_default_banner"))
            }
            
            titleLbl.text = model.title
            
            if model.cover_title.characters.count != 0{
                tagLbl.isHidden = false
                tagLbl.text = " \(model.cover_title) "
                subTitleLbl.text = "  \(model.sub_title)"
                
            }else{
                tagLbl.isHidden = true
                tagLbl.text = ""
                subTitleLbl.text = model.sub_title
            }
            
            articleTagLbl.isHidden = model.Is_new == 0
            
            favCountLbl.text = "\(model.collect_num)"
            favBtn.setImage(UIImage(named:model.collect_status == 1 ? "goods_general_toolbar_heart_press" : "goods_general_toolbar_heart"), for: UIControlState())
            favBtn.imageView?.contentMode = UIViewContentMode.scaleToFill
            
        }else{
            assert(false, "\(#file)\(#line)  ===> 传进的Model有误")
        }
    }
    
    override func getCellImage() ->UIImageView {
        return goodsImageView
    }
    
    
    override func setupUI() {
        
        let _ = [goodsImageView,tagLbl,titleLbl,subTitleLbl,favBtn,favCountLbl].map{self.addSubview($0)}
        
        goodsImageView.addSubview(articleTagLbl)
        
        goodsImageView.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(self).offset(0)
            let _ = make.left.equalTo(self)
            let _ = make.right.equalTo(self)
            let _ = make.height.equalTo(400 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
        }
        
        articleTagLbl.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(goodsImageView)
            let _ = make.left.equalTo(goodsImageView).offset(Constant.ScreenSizeV2.MARGIN_30)
            let _ = make.height.equalTo(50 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
            let _ = make.width.equalTo(100 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
        }
        
        
        titleLbl.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(goodsImageView.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_30)
            let _ = make.left.equalTo(self).offset(Constant.ScreenSizeV2.MARGIN_30)
            let _ = make.height.equalTo(Constant.ScreenSizeV2.MARGIN_30 + Constant.ScreenSizeV2.MARGIN_8)
            let _ = make.width.lessThanOrEqualTo(Constant.ScreenSizeV2.SCREEN_WIDTH * 0.7)
        }
        
        tagLbl.snp_makeConstraints { (make) in
            let _ = make.left.equalTo(titleLbl)
            let _ = make.top.equalTo(titleLbl.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_15)
            let _ = make.height.equalTo(Constant.ScreenSizeV2.MARGIN_30)
        }
        
        subTitleLbl.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(tagLbl)
            let _ = make.left.equalTo(tagLbl.snp_right)
            let _ = make.height.equalTo(Constant.ScreenSizeV2.MARGIN_30)
        }
        
        
        favCountLbl.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(titleLbl)
            let _ = make.right.equalTo(self).offset(-Constant.ScreenSizeV2.MARGIN_30)
            let _ = make.height.equalTo(titleLbl)
        }
        
        favBtn.snp_makeConstraints { (make) in
            let _ = make.right.equalTo(favCountLbl.snp_left).offset(-Constant.ScreenSizeV2.MARGIN_4)
            let _ = make.centerY.equalTo(favCountLbl)
            let _ = make.height.equalTo(Constant.ScreenSizeV2.MARGIN_30)
            let _ = make.width.equalTo(Constant.ScreenSizeV2.MARGIN_30)
        }
        goodsImageView.image = UIImage(named: "new_default_banner")
    }
}
