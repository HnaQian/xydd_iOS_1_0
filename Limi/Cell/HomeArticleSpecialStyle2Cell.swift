//
//  HomeArticleSpecialStyle2Cell.swift
//  Limi
//
//  Created by Richie on 16/6/12.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//攻略列表里面的第二种形式 达人攻略：有：头像、名称、title、礼物表 --- 对应的model ： HomeStyle2ArticleModel
import UIKit

class HomeArticleSpecialStyle2Cell: HomeBaseTableViewCell {
    
    
    //    #if false
    fileprivate let margin_30:CGFloat = 30 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
    
    fileprivate let icon_height:CGFloat = 160 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
    
    fileprivate var goodsAry = [GoodsModel]()
    
    //头像
    fileprivate let icon:UIImageView = {
        let icon = UIImageView()
        icon.clipsToBounds  = true
        icon.layer.cornerRadius = ((160 - MARGIN_4) * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)/2
        icon.contentMode = UIViewContentMode.scaleAspectFill
        return icon
    }()

    
    //头像
    fileprivate let iconBgView:UIView = {
        let icon = UIView()
        icon.clipsToBounds  = true
        icon.backgroundColor = UIColor.white
        icon.layer.cornerRadius = (160 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)/2
        return icon
    }()
    
    //标题
    fileprivate let nameLbl:UILabel = {
        let lbl = UILabel()
        lbl.font = Constant.Theme.Font_17
        lbl.textColor = Constant.Theme.Color13
        lbl.textAlignment = NSTextAlignment.center
        return lbl;
    }()
    
    //子标题
    fileprivate let subTitleLbl:UILabel = {
        let lbl = UILabel()
        lbl.font = Constant.Theme.Font_17
        lbl.textColor = Constant.Theme.Color13
        lbl.textAlignment = NSTextAlignment.center
        return lbl;
    }()
    
    //灰色背景
    fileprivate let grayBgView:UIView = {
        let view = UIView()
        view.layer.borderColor = Constant.Theme.ColorSeparatLine.cgColor
        view.layer.borderWidth = 0.5
        
        return view
    }()
    
    fileprivate let cell_height:CGFloat = 160 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
    fileprivate let cell_width:CGFloat = 200 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
    
    
    //产品列表：横向
    fileprivate var banner:LMHorizontalTableView!
    
    override class func getheight() ->CGFloat{return 530 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE}
    
    override class func getReuseIdentifier()->String{return "HomeArticleSpecialStyle1Cell"}

    
    override func refreshWithData(_ dataModel: HomCellBaseDataModel) {
        
        if let model =  dataModel as? HomeStyle2ArticleModel {

            if let author = model.author,
            let url = URL(string: Utils.scaleImage(author.avatar, width: icon_height * 3, height: icon_height * 3)){
                icon.af_setImageWithURL(url, placeholderImage: UIImage(named: "new_default_banner"))
                nameLbl.text = author.name
            }
            
            subTitleLbl.text = model.title
            
            //礼物数组
            if let ary = model.goods_list{
                self.goodsAry = ary
                self.banner.reloadData()
            }
            
            
        }else{
            assert(false, "\(#file)\(#line)  ===> 传进的Model有误")
        }
    }
    

    override func setupUI() {
        
        banner = LMHorizontalTableView(frame: CGRect(x: 0,y: HomeArticleSpecialStyle2Cell.height - Constant.ScreenSizeV2.MARGIN_30 - cell_height,width: Constant.ScreenSizeV2.SCREEN_WIDTH - Constant.ScreenSizeV2.MARGIN_30 * 2,height: cell_height))
        
        banner.dataSource = self
        banner.delegate = self
        
        let _ = [grayBgView,iconBgView,nameLbl,subTitleLbl,banner].map{self.addSubview($0)}
        iconBgView.addSubview(icon)
//        grayBgView.backgroundColor = UIColor.blackColor()
        grayBgView.snp_makeConstraints { (make) in
            let _ = make.left.equalTo(self).offset(-MARGIN_4)
            let _ = make.top.equalTo(icon.snp_centerY)
            let _ = make.right.equalTo(self).offset(MARGIN_4)
            let _ = make.bottom.equalTo(self)
        }
        
        iconBgView.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(self).offset(margin_30)
            let _ = make.centerX.equalTo(self)
            let _ = make.width.equalTo(icon_height)
            let _ = make.height.equalTo(icon_height)
        }
        
        icon.snp_makeConstraints { (make) in
            let _ = make.centerY.equalTo(iconBgView)
            let _ = make.centerX.equalTo(iconBgView)
            let _ = make.width.equalTo(icon_height - Constant.ScreenSizeV2.MARGIN_10)
            let _ = make.height.equalTo(icon_height - Constant.ScreenSizeV2.MARGIN_10)
        }
        
        nameLbl.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(icon.snp_bottom).offset(margin_30)
            let _ = make.centerX.equalTo(icon)
            let _ = make.height.equalTo(margin_30)
        }
        
        subTitleLbl.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(nameLbl.snp_bottom).offset(margin_30)
            let _ = make.centerX.equalTo(nameLbl)
            let _ = make.height.equalTo(margin_30)
        }
        
        banner.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(subTitleLbl.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_30)
            let _ = make.left.equalTo(self).offset(Constant.ScreenSizeV2.MARGIN_30)
            let _ = make.right.equalTo(self).offset(-Constant.ScreenSizeV2.MARGIN_30)
            let _ = make.height.equalTo(cell_height)
        }
        
        icon.image = UIImage(named: "new_default_banner")
    }
    //    #endif
    
    
}


extension HomeArticleSpecialStyle2Cell:LMHorizontalTableViewDataSource,LMHorizontalTableViewDelegate{
    
        func numberOfRows(_ horizontalTableView: LMHorizontalTableView) -> Int {
            return goodsAry.count
        }
        
        
        func horizontalTableView(_ horizontalTableView: LMHorizontalTableView, cellForRow row: Int ,reuseCell:((_ reuseInfentifer:String) ->LMHorizontalTableViewCell?)) -> LMHorizontalTableViewCell {
            
            var cell:SubCell? = reuseCell("indetifer") as? SubCell
            
            if cell == nil{
                cell = SubCell(reuseIdentifier:"indetifer")
            }
            
            cell!.reloadData(goodsAry[row])
            
            return cell!
        }
        
        
        func horizontalTableView(_ horizontalTableView: LMHorizontalTableView, heightForRow row: Int) -> CGFloat{
            return cell_height
        }
        
        func horizontalTableView(_ horizontalTableView: LMHorizontalTableView, widthForRow row: Int) -> CGFloat{
            return cell_width
        }
        
        
        func horizontalTableView(_ horizontalTableView: LMHorizontalTableView, didSelectRow row: Int) {
            self.delegate?.homeTableViewCellGoodsSelected(goodsAry[row].gid)
        }
}


extension HomeArticleSpecialStyle2Cell{
    
    fileprivate class SubCell:LMHorizontalTableViewCell{
        
        fileprivate var itemView = UIImageView()
        fileprivate let itemView_size:CGFloat = 160 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        
        override init(reuseIdentifier: String) {
            super.init(reuseIdentifier: reuseIdentifier)
            
            setupUI()
        }
        
        func reloadData(_ dataModel:GoodsModel){
            if let url = URL(string: Utils.scaleImage(dataModel.goods_image, width: itemView_size * 3, height: itemView_size * 3)){
                itemView.af_setImageWithURL(url, placeholderImage: UIImage(named: "loadingDefault"))
            }
            
        }
        
        fileprivate func setupUI(){
            self.backgroundColor = UIColor.clear
            itemView.frame = CGRect(x: Constant.ScreenSizeV2.MARGIN_20,y: 0, width: itemView_size, height: itemView_size)
            self.addSubview(itemView)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}
