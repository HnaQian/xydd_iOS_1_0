//
//  HomeArticleSpecialStyle1Cell.swift
//  Limi
//
//  Created by Richie on 16/6/12.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//攻略列表里面的第一种形式:自选礼盒：攻略图片+礼盒列表 -- 对应的model - HomeStyle1ArticleModel
import UIKit

class HomeArticleSpecialStyle1Cell: HomeBaseTableViewCell {
    
    static var nullListHeight:CGFloat {return (354+30) * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE}
    
    fileprivate let imageView_height:CGFloat = 354 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
    
    fileprivate let cell_height:CGFloat = 210 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
    fileprivate let cell_width:CGFloat = 190 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
    
    fileprivate var horTableView:LMHorizontalTableView!
    
    fileprivate let goodsImage:UIImageView = {
        let imageView = UIImageView()
        imageView.clipsToBounds  = true
        imageView.contentMode = UIViewContentMode.scaleAspectFill
        return imageView
    }()
    
    
    fileprivate var goodsAry = [HomeStyle1ArticleGoods]()
    
    override class func getheight() ->CGFloat{return (30+354+30+210+30) * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE}
    
    override class func getReuseIdentifier()->String{return "HomeArticleSpecialStyle2Cell"}
    
    fileprivate var dataModel:HomeStyle1ArticleModel?
    
    override func refreshWithData(_ dataModel: HomCellBaseDataModel) {
        
        if let model =  dataModel as? HomeStyle1ArticleModel {
            self.dataModel = model
            if let attr = model.attributes,
                let url = URL(string: Utils.scaleImage(attr.img, width: Constant.ScreenSizeV2.SCREEN_WIDTH * 3, height: Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * 354 * 3)){
                    goodsImage.af_setImageWithURL(url, placeholderImage: UIImage(named: "Default_300_160"))
            }
            
            //刷新banner
            if let ary = model.children{
                self.goodsAry = ary
                horTableView.reloadData()
            }
            
        }else{
            assert(false, "\(#file)\(#line)  ===> 传进的Model有误")
        }
    }
    
    override func getCellImage() -> UIImageView {
        return goodsImage
    }
    
    override func setupUI() {
        
        goodsImage.frame = CGRect(x: 0, y: Constant.ScreenSizeV2.MARGIN_30, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: imageView_height)
        goodsImage.image = UIImage(named: "Default_300_160")
        
        horTableView = LMHorizontalTableView(frame: CGRect(x: Constant.ScreenSizeV2.MARGIN_30,y: goodsImage.ibottom + Constant.ScreenSizeV2.MARGIN_30,width: Constant.ScreenSizeV2.SCREEN_WIDTH - Constant.ScreenSizeV2.MARGIN_30 * 2,height: cell_height))
        
        horTableView.dataSource = self
        horTableView.delegate = self
        
        let _ = [goodsImage,horTableView].map{self.addSubview($0)}
    }
}



//MARK: tableView的代理
extension HomeArticleSpecialStyle1Cell:LMHorizontalTableViewDataSource,LMHorizontalTableViewDelegate{

    func numberOfRows(_ horizontalTableView: LMHorizontalTableView) -> Int {
        return goodsAry.count
    }
    
    
    func horizontalTableView(_ horizontalTableView: LMHorizontalTableView, cellForRow row: Int ,reuseCell:((_ reuseInfentifer:String) ->LMHorizontalTableViewCell?)) -> LMHorizontalTableViewCell {
        
        var cell:SubCell? = reuseCell("indetifer") as? SubCell
        
        if cell == nil{
            cell = SubCell(reuseIdentifier:"indetifer")
        }
        
        cell!.reloadData(goodsAry[row])
        
        return cell!
    }
    
    
    func horizontalTableView(_ horizontalTableView: LMHorizontalTableView, heightForRow row: Int) -> CGFloat{
        return cell_height
    }
    
    func horizontalTableView(_ horizontalTableView: LMHorizontalTableView, widthForRow row: Int) -> CGFloat{
        return cell_width
    }
    
    
    func horizontalTableView(_ horizontalTableView: LMHorizontalTableView, didSelectRow row: Int) {
        if let model = dataModel{
            if let href = model.attributes?.href{
                self.delegate?.homeTableViewCellPackageSelected(href)
            }
        }
    }

}

extension HomeArticleSpecialStyle1Cell{
    
    
    fileprivate class SubCell:LMHorizontalTableViewCell{
        
        fileprivate var itemView:HomeArticleSpecialStyle2Cell_goodsBannerItemView!
        
        override init(reuseIdentifier: String) {
            super.init(reuseIdentifier: reuseIdentifier)
            setupUI()
        }

        func reloadData(_ dataModel:HomeStyle1ArticleGoods){
            itemView.reloadData(dataModel)
        }
        
        fileprivate func setupUI(){
            itemView = HomeArticleSpecialStyle2Cell_goodsBannerItemView(frame:CGRect(x: Constant.ScreenSizeV2.MARGIN_15,y: 0,width: 160 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE,height: 210 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE))
            self.addSubview(itemView)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    
    fileprivate class HomeArticleSpecialStyle2Cell_goodsBannerItemView: UIView {
        
        fileprivate let goodsImageView = UIImageView()
        
        fileprivate let titleLbl:UILabel = {
            let lbl = UILabel()
            lbl.font = Constant.Theme.Font_11
            lbl.textColor = Constant.Theme.Color13
            return lbl
        }()
        
        fileprivate let icon_size:CGFloat = 160 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            self.setupUI()
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        
        func reloadData(_ dataModel:HomeStyle1ArticleGoods){

            if let url = URL(string: Utils.scaleImage(dataModel.img, width: Constant.ScreenSizeV2.SCREEN_WIDTH * 3, height:Constant.ScreenSizeV2.SCREEN_WIDTH * 3)){
                goodsImageView.af_setImageWithURL(url, placeholderImage: UIImage(named: "Default_750_750"))
            }
            
            titleLbl.text = dataModel.name
        }
        
        
        fileprivate func setupUI(){
            let _ = [goodsImageView,titleLbl].map{self.addSubview($0)}
            
            goodsImageView.frame = CGRect(x: 0, y: 0, width: icon_size, height: icon_size)

            titleLbl.frame = CGRect(x: 0, y: goodsImageView.ibottom + Constant.ScreenSizeV2.MARGIN_30, width: goodsImageView.iwidth, height: Constant.ScreenSizeV2.MARGIN_20)
        }
    }
}
