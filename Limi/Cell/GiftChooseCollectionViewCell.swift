//
//  GiftChooseCollectionViewCell.swift
//  Limi
//
//  Created by ZhiQiangMi on 16/3/31.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class GiftChooseCollectionViewCell: UICollectionViewCell {
    
    let topicImageView = UIImageView()
    
    //    let selectStatusImageView = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        
    }
    
    func setupUI()
    {
        
//        self.layer.borderWidth = 0.5
        self.layer.borderColor = UIColor(rgba: Constant.common_separatLine_color).cgColor
        self.layer.cornerRadius = 4
        self.backgroundColor = UIColor.white
        
        topicImageView.frame = self.bounds
        
        self.addSubview(topicImageView)
        
        topicImageView.image = UIImage(named: "Default_300_160")
        topicImageView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self)
            let _ = make.left.equalTo(self)
            let _ = make.right.equalTo(self)
            let _ = make.bottom.equalTo(self)
        }
        
        
        topicImageView.layer.cornerRadius = 3
        
        topicImageView.clipsToBounds = true
        
        topicImageView.contentMode = UIViewContentMode.scaleAspectFill
        
    }
    
    func reloadData(_ data:GiftChooseChildModel)
    {
        
        
        if let name = data.attributes?.src
        {
            if name.characters.count > 0
            {
                let width:CGFloat = (Constant.ScreenSize.SCREEN_WIDTH - MARGIN_10 * 3)/2.0;
                let height:CGFloat = width * 260.0/384;
                
                let imageScale : String = name + "?imageView2/0/w/" + "\(Int(width*Constant.ScreenSize.SCALE_SCREEN))" + "/h/" + "\(Int(height*Constant.ScreenSize.SCALE_SCREEN))"
                
                topicImageView.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "Default_300_160"))
            }else {
                topicImageView.image = UIImage(named: "Default_300_160")
            }
        }
        
    }
}

class GiftChooseCollectionViewHeader: UICollectionReusableView {
    
    let headerLabel:UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor(rgba:"#FF3A48")
        return lbl
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.white;
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupUI() {
        
        headerLabel.font = UIFont.boldSystemFont(ofSize: Constant.Theme.FontSize_15)
        self.addSubview(headerLabel)
        
        headerLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.left.equalTo(self).offset(MARGIN_8)
            let _ = make.width.equalTo(self)
            let _ = make.bottom.equalTo(self)
            let _ = make.height.equalTo(20)
        }
    }
    
    
}



class GiftChooseCollectionViewFooter: UICollectionReusableView {
    
    
    weak var delegate:GiftChooseCollectionViewFooterDelegate?
    
    var didSpread = false
    
    var section:Int = 0
    
    var footerLabel:UILabel = {
        
        let lbl = UILabel()
        lbl.font = Constant.Theme.Font_15
        lbl.textColor = Constant.Theme.Color13
        lbl.textAlignment = NSTextAlignment.center
        return lbl
    }()
    var footerImageView = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        self.backgroundColor = UIColor(rgba: Constant.common_separatLine_color)
        self.isUserInteractionEnabled = true
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupUI() {
        
        let footerSelectView : UIView = UIView()
        
        self.addSubview(footerSelectView)
        
        footerSelectView.backgroundColor = UIColor.white
        
        footerSelectView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self).offset(0.5)
            let _ = make.width.equalTo(self)
            let _ = make.left.equalTo(self)
            let _ = make.height.equalTo(50)
            
        }
        
        //        footerLabel.font = UIFont.systemFontOfSize(Constant.common_F4_font)
        footerSelectView.addSubview(footerLabel)
        footerLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(footerSelectView).offset(10)
            let _ = make.width.equalTo(footerSelectView)
            let _ = make.left.equalTo(footerSelectView)
           let _ =  make.height.equalTo(20)
            
        }
        
        footerSelectView.addSubview(footerImageView)
        footerImageView.snp_makeConstraints { (make) -> Void in
            let _ = make.width.equalTo(10)
            let _ = make.height.equalTo(5)
            let _ = make.top.equalTo(footerLabel.snp_bottom).offset(MARGIN_4)
            let _ = make.centerX.equalTo(self.snp_centerX)
        }
        
        self.footerLabel.text = "查看更多"
        self.footerImageView.image = UIImage(named: "spreadarrow")
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let del = delegate{
            del.giftChooseCollectionViewFooterDidSelected(self.section)
        }
    }
    
    
    
    func refreshUI(_ section:Int,delegate:GiftChooseCollectionViewFooterDelegate?,didSpread:Bool){
        
        self.didSpread = didSpread
        self.delegate = delegate
        self.section = section
        
        if !didSpread{
            //展开动画
            self.footerLabel.text = "查看更多"
            self.footerImageView.image = UIImage(named: "spreadarrow")
            
        }else{
            //shou qi donghua
            
            self.footerLabel.text = "收起"
            self.footerImageView.image = UIImage(named: "retractarrow")
        }
    }
    
}

protocol GiftChooseCollectionViewFooterDelegate:NSObjectProtocol {
    func giftChooseCollectionViewFooterDidSelected(_ section:Int)
}
