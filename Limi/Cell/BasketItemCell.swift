//
//  BasketItemCell.swift
//  Limi
//
//  Created by ZhiQiangMi on 16/3/7.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class BasketItemCell: UITableViewCell {
    
    
    
    //商品图片
    @IBOutlet weak var goodImage: UIImageView!
    //商品名称
    @IBOutlet weak var goodNameLabel: UILabel!
    //商品属性
    @IBOutlet weak var goodSubLabel: UILabel!
    //价格
    @IBOutlet weak var priceLabel: UILabel!
    //数量操作按钮
    @IBOutlet weak var reductionPriceLabel: UILabel!
    //数量提示
    @IBOutlet weak var numTipLabel: UILabel!
    
    //失效图片
    @IBOutlet weak var invalidStateLabel: UILabel!

    fileprivate let invalidView = UIView()
    
    fileprivate var limit_count:Int = 0
    
    fileprivate var row = 0
    
    fileprivate var section = 0
    
    fileprivate let buyView = BuyView()
    
    fileprivate let  selectImageView = OperationAbleImageView()
    
    fileprivate let bottomLine = UIView()

    fileprivate var goodsNum = 0
    
    fileprivate lazy var surplusLabel: UILabel = {
        let surplusLabel = UILabel()
        surplusLabel.textColor = UIColor(rgba: Constant.common_red_color)
        surplusLabel.font = UIFont.systemFont(ofSize: 13)
        surplusLabel.textAlignment = .right
        return surplusLabel
    }()
    
    fileprivate let collectView = ImageLabel(type: 1)
    fileprivate let deleteView = ImageLabel(type: 2)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = UITableViewCellSelectionStyle.none
        self.setupUI()
        self.initFunction()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    fileprivate var dataModel:GoodsOfBasketModel!
    
    fileprivate func initFunction(){
      
    }
    
    
    
    func reloadWithData(_ model:GoodsOfBasketModel){
        dataModel = model
        self.section = model.section
        self.row = model.row
        
        if model.selectedGoodsAttrNames.count != 0{
            var attribute = ""
            for index in 0...model.selectedGoodsAttrNames.count - 1{
                if index == model.selectedGoodsAttrNames.count - 1{
                    attribute = attribute + model.selectedGoodsAttrNames[index]
                }else{
                    attribute = attribute + model.selectedGoodsAttrNames[index] + ";"
                }
            }
            self.goodSubLabel.text = attribute
        }
        
        if model.goods.goods_image != NULL_STRING{
            let imageName = model.goods.goods_image
            let imageScale : String = imageName + "?imageView2/0/w/" + "\(Int(self.goodImage.frame.size.width*3))" + "/h/" + "\(Int(self.goodImage.frame.size.height*3))"
            
            self.goodImage.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "loadingDefault"))
        }
        
        self.goodNameLabel.text = model.goods.goodsName
        
        self.priceLabel.text = String(format: "￥%.2f",model.price)
        
        self.reductionPriceLabel.text = model.price_tip
        if model.num_tip == "仅剩0件" {
            self.numTipLabel.text = ""
        }else {
            self.numTipLabel.text = model.num_tip
        }
        
        
        buyView.refreshData(model)
        
        if model.goods_status == false{
            self.invalidStateLabel.isHidden = false
            invalidView.alpha = 0.7
            self.priceLabel.textColor = UIColor(rgba: Constant.common_C2_color)
            self.reductionPriceLabel.textColor = UIColor(rgba: Constant.common_C2_color)
        }
        else{
            self.invalidStateLabel.isHidden = true
            invalidView.alpha = 0
            self.priceLabel.textColor = UIColor(rgba: Constant.common_C1_color)
            self.reductionPriceLabel.textColor = UIColor(rgba: Constant.common_C1_color)
        }
        
        selectImageView.refreshData(model)
    }
   
    
    fileprivate func setupUI()
    {
        let height:CGFloat = 140 * Constant.ScreenSize.SCREEN_HEIGHT_SCALE
        
        let lbl_height:CGFloat = (140 * Constant.ScreenSize.SCREEN_HEIGHT_SCALE - MARGIN_8 * 2)/4
        
        let _ = [selectImageView,surplusLabel,buyView,bottomLine,invalidView,collectView,deleteView].map{self.addSubview($0)}
        
        var invalidLabelIndex = 0
        var invalidViewIndex = 0
        
        for index in 0...(self.subviews.count - 1){
            
            if self.subviews[index] == invalidStateLabel{
                invalidLabelIndex = index
            }else if self.subviews[index] == invalidView{
                invalidViewIndex = index
            }
        }
        
        self.exchangeSubview(at: invalidViewIndex, withSubviewAt: invalidLabelIndex)
        self.exchangeSubview(at: invalidLabelIndex, withSubviewAt: invalidViewIndex - 1)
        
        self.invalidStateLabel.textColor = UIColor(rgba: Constant.common_C6_color)
        invalidView.backgroundColor = UIColor.white
        invalidView.isUserInteractionEnabled = false
        invalidView.alpha = 0
        
        goodImage.layer.borderColor = UIColor(rgba: Constant.common_separatLine_color).cgColor
        goodImage.layer.borderWidth = 0.5
        
        self.selectImageView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self)
            let _ = make.bottom.equalTo(self)
            let _ = make.left.equalTo(self)
            let _ = make.width.equalTo(49)
        }
        
        self.goodImage.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self).offset(MARGIN_8)
            let _ = make.bottom.equalTo(self).offset(-MARGIN_8)
            let _ = make.left.equalTo(self.selectImageView.snp_right)
            let _ = make.width.equalTo(self.goodImage.snp_height)
        }
        
        self.invalidStateLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.left.equalTo(self.goodImage)
            let _ = make.right.equalTo(self.goodImage)
            let _ = make.height.equalTo(self.goodImage)
            let _ = make.height.equalTo(self.goodImage)
        }
        
        self.goodNameLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.goodImage)
            let _ = make.left.equalTo(self.goodImage.snp_right).offset(MARGIN_8)
            let _ = make.right.equalTo(self).offset(-MARGIN_8)
            let _ = make.height.equalTo(lbl_height)
        }
        
        self.goodSubLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.goodNameLabel.snp_bottom)
            let _ = make.left.equalTo(self.goodNameLabel)
            let _ = make.height.equalTo(self.goodNameLabel)
            let _ = make.right.equalTo(self.goodNameLabel)
        }
        
        self.reductionPriceLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.goodSubLabel.snp_bottom)
            let _ = make.left.equalTo(self.goodNameLabel)
            let _ = make.height.equalTo(self.goodNameLabel)
            let _ = make.right.equalTo(self.goodNameLabel.snp_centerX)
        }
        
        self.numTipLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.goodSubLabel.snp_bottom)
            let _ = make.right.equalTo(self).offset(-MARGIN_8)
            let _ = make.left.equalTo(self.reductionPriceLabel.snp_right)
            let _ = make.height.equalTo(self.goodNameLabel)
        }
        
        self.priceLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.reductionPriceLabel.snp_bottom)
            let _ = make.left.equalTo(self.goodNameLabel)
            let _ = make.height.equalTo(self.goodNameLabel)
            let _ = make.width.equalTo(100)
        }
        
        
        buyView.snp_remakeConstraints { (make) -> Void in
            let _ = make.right.equalTo(self).offset(20)
            let _ = make.width.equalTo(125)
            let _ = make.height.equalTo(50)
            let _ = make.centerY.equalTo(self.priceLabel)
        }
       
        surplusLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.right.equalTo(buyView).offset(-MARGIN_8)
            let _ = make.bottom.equalTo(buyView.snp_top)
        }
        
        bottomLine.backgroundColor = UIColor(rgba: Constant.common_separatLine_color)
        bottomLine.snp_makeConstraints { (make) in
            let _ = make.bottom.equalTo(self)
            let _ = make.height.equalTo(0.5)
            let _ = make.left.equalTo(self)
            let _ = make.right.equalTo(self)
        }
        
        invalidView.snp_makeConstraints { (make) in
            let _ = make.edges.equalTo(self)
        }
        
        
        collectView.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(self)
            let _ = make.left.equalTo(self.snp_right)
            let _ = make.height.equalTo(height)
            let _ = make.width.equalTo(height * 74/130)
        }
        
        deleteView.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(self)
            let _ = make.left.equalTo(collectView.snp_right)
            let _ = make.height.equalTo(height)
            let _ = make.width.equalTo(height * 74/130)
        }
        
    }
    
}



extension BasketItemCell{

    class OperationAbleImageView:UIImageView{
    
        init() {
            super.init(frame: CGRect.zero)
            self.contentMode = UIViewContentMode.center
            self.isUserInteractionEnabled = true
            self.image = UIImage(named: "basket_no_selected")
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        var section = 0
        var row = 0
        
        func refreshData(_ goodsModel:GoodsOfBasketModel){
            
            self.row  = goodsModel.row
            
            self.section = goodsModel.section
//            self.chooseImage.image = UIImage(named: "choosed")
//        }else
//        {
//        self.chooseImage.image = UIImage(named: "noChoose")
            self.image = UIImage(named: goodsModel.didSelected ? "choosed" : "noChoose")
        }
        
        override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {

            let userInfo: Dictionary<String,AnyObject>! = [
                "row": self.row as AnyObject,
                "section":self.section as AnyObject,
                ]
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.cart_selected), object: nil,userInfo: userInfo)
        }
    }

    
    class ImageLabel:UIView{
        
        fileprivate let icon = UIImageView()
        fileprivate let lbl = UILabel()
        

        
        init(type:Int){
            super.init(frame: CGRect.zero)
            
            self.addSubview(icon)
            self.addSubview(lbl)
            
            self.setupUI(type)
        }
        
        fileprivate func setupUI(_ type:Int){
            
            let _ = [icon,lbl,self].map{$0.isUserInteractionEnabled = false}
            
            icon.contentMode = UIViewContentMode.scaleAspectFill
            
            lbl.textAlignment = NSTextAlignment.center
            lbl.font = UIFont.systemFont(ofSize: Constant.common_F5_font)
            
            let width:CGFloat = 20
            
            icon.snp_makeConstraints { (make) in
                let _ = make.centerX.equalTo(self)
                let _ = make.bottom.equalTo(self.snp_centerY).offset(-MARGIN_4)
                let _ = make.width.equalTo(width)
                let _ = make.height.equalTo(width)
            }
            
            lbl.snp_makeConstraints { (make) in
                let _ = make.left.equalTo(self)
                let _ = make.right.equalTo(self)
                let _ = make.centerX.equalTo(self)
                let _ = make.top.equalTo(self.snp_centerY).offset(MARGIN_4)
                let _ = make.height.equalTo(20)
            }
            
            if type == 1{
                self.backgroundColor = UIColor(rgba: Constant.common_C9_color)
                lbl.textColor = UIColor(rgba: Constant.common_C3_color)
                lbl.text = "移至喜欢"
                
                icon.image = UIImage(named: "basket_collect")
                
            }else{
                self.backgroundColor = UIColor(rgba: Constant.common_red_color)
                lbl.textColor = UIColor(rgba: Constant.common_C11_color)
                lbl.text = "删除"
                
                icon.image = UIImage(named: "basket_icon_delete")
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
    }
    
    
    class ChangeCountButton:UIButton{
        
        init(type:Int){
            super.init(frame:CGRect.zero)
            
            self.setupUI(type)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        
        fileprivate let titleLable = UILabel()
        
        fileprivate func setupUI(_ type:Int){
            
            self.addSubview(titleLable)
            
            titleLable.snp_makeConstraints { (make) -> Void in
                let _ = make.centerY.equalTo(self)
                
                if type == 1{
                    let _ = make.left.equalTo(self).offset(2)
                }else{
                    let _ = make.right.equalTo(self).offset(-2)
                }
                let _ = make.width.equalTo(20)
               let _ = make.height.equalTo(20)
            }
            
            
            titleLable.font = UIFont.systemFont(ofSize: Constant.common_F3_font)
            titleLable.text = type == 1 ? "+" : "-"
            titleLable.textAlignment = NSTextAlignment.center
            titleLable.textColor =  UIColor.black
            titleLable.layer.borderColor = UIColor.lightGray.cgColor
            titleLable.layer.borderWidth = 0.5
        }
    }
    
    
}
