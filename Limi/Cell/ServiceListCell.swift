//
//  ServiceListCell.swift
//  Limi
//
//  Created by maohs on 16/11/23.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class ServiceListCell: UITableViewCell {

    static let reuseIdentifier = "ServiceListCell"
    static let height = (356 + 20) * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
    
    
    fileprivate let bgView:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    fileprivate let leftImageView:UIImageView = {
        let image = UIImageView()
        image.contentMode = UIViewContentMode.scaleAspectFill
        return image
    }()
    
    fileprivate let middleImageView:UIImageView = {
        let image = UIImageView()
        image.contentMode = UIViewContentMode.scaleAspectFill
        return image
    }()
    
    fileprivate let titleLabel:UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.boldSystemFont(ofSize: Constant.Theme.FontSize_19)
        lbl.textColor = UIColor(rgba: Constant.common_C2_color)
        return lbl
    }()
    
    fileprivate let contentLabel:UILabel = {
       let lbl = UILabel()
        lbl.font = Constant.Theme.Font_13
        lbl.textColor = UIColor(rgba: Constant.common_C2_color)
        return lbl
    }()
    
    fileprivate let markImageView:UIImageView = {
        let image = UIImageView()
        image.contentMode = UIViewContentMode.scaleAspectFill
        image.image = UIImage(named: "service_mark")
        return image
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = UITableViewCellSelectionStyle.none
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        self.clipsToBounds = true
        setUpUI()
    }
    
    func setUpUI() {
        self.contentView.addSubview(bgView)
        
        bgView.addSubview(leftImageView)
        bgView.addSubview(middleImageView)
        bgView.addSubview(titleLabel)
        bgView.addSubview(contentLabel)
        bgView.addSubview(markImageView)
        
        let scale = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        bgView.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: ServiceListCell.height - 20 * scale)
        leftImageView.frame = CGRect(x: 0, y: 0, width: bgView.iheight, height: bgView.iheight)
        middleImageView.frame = CGRect(x: leftImageView.iright - 20 * scale, y: 0, width: 80 * scale, height: 80 * scale)
        middleImageView.icenterY = leftImageView.icenterY
        
        titleLabel.frame = CGRect(x: middleImageView.iright + 16 * scale, y: middleImageView.itop + Constant.ScreenSizeV1.MARGIN_4, width: bgView.iwidth - middleImageView.iright - 36 * scale, height: 15 + Constant.ScreenSizeV2.MARGIN_4)
        contentLabel.frame = CGRect(x: middleImageView.iright + 16 * scale, y: titleLabel.ibottom + Constant.ScreenSizeV1.MARGIN_4, width: titleLabel.iwidth, height: 12 + Constant.ScreenSizeV2.MARGIN_4)
        markImageView.frame = CGRect(x: bgView.iwidth - 90 * scale, y: 0, width: 80 * scale, height: 80 * scale)
        markImageView.icenterY = middleImageView.icenterY
    }
    
    func reloadData(data:[String]) {
        leftImageView.image = UIImage(named: data[0])
        middleImageView.image = UIImage(named: data[1])
        titleLabel.text = data[2]
        contentLabel.text = data[3]
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
