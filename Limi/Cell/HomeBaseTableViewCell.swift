//
//  HomeBaseTableViewCell.swift
//  Limi
//
//  Created by Richie on 16/6/13.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class HomeBaseTableViewCell: UITableViewCell {
    
    let sizeScal:CGFloat = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    weak var delegate:HomeBaseTableViewCellDelegate?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = UITableViewCellSelectionStyle.none
        self.backgroundColor = UIColor.white
        self.contentView.backgroundColor = UIColor.white
        self.clipsToBounds = true
        self.setupUI()
    }
    
    
    class var height:CGFloat{return self.getheight()}
    class var reuseIdentifier:String{return self.getReuseIdentifier()}
    
    //need children override
    class func getheight() ->CGFloat{return 0}
    
    //need children override
    class func getReuseIdentifier()->String{return "HomeBaseTableViewCell"}
    
    //need children override
    func refreshWithData(_ dataModel: HomCellBaseDataModel) {}
    
    //need children override
    func setupUI(){}
    
    func getCellImage() ->UIImageView {return UIImageView()}
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}


protocol HomeBaseTableViewCellDelegate:NSObjectProtocol {
    ///攻略
    func homeTableViewCellSelected(_ articleID:Int)
    ///商品
    func homeTableViewCellGoodsSelected(_ goodsID:Int)
    ///自选礼盒
    func homeTableViewCellPackageSelected(_ href:String)
}
