//
//  RecommendCell.swift
//  Limi
//
//  Created by ZhiQiangMi on 15/8/27.
//  Copyright (c) 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class RecommendCell: UITableViewCell {

    @IBOutlet weak var personIntroduceLabel: UILabel!
    @IBOutlet weak var personView: UIView!
    @IBOutlet weak var personNameLabel: UILabel!
    @IBOutlet weak var portraitImageView: UIImageView!
    @IBOutlet weak var summaryLabel: UILabel!
    @IBOutlet weak var coverImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
    var data: JSON? {
        didSet {
            self.portraitImageView.clipsToBounds = true
            self.portraitImageView.layer.cornerRadius = portraitImageView.frame.width/2
            if let coverUrl = data?["Img"].string{
                 let imageScale : String = coverUrl + "?imageView2/0/w/" + "\(Int((Constant.ScreenSize.SCREEN_WIDTH-10)*Constant.ScreenSize.SCALE_SCREEN))" + "/h/" + "\(Int(145*Constant.ScreenSize.SCALE_SCREEN))"
                
                self.coverImageView.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "loadingDefault"))
            }
            if let Intro = data?["Master"]["Intro"].string{
                self.personIntroduceLabel.text = Intro
            }
            if let personName = data?["Master"]["Name"].string{
                self.personNameLabel.text = personName
            }
            if let portraitUrl = data?["Master"]["Avatar"].string{
                
                let imageScale : String = portraitUrl + "?imageView2/0/w/" + "\(Int(50*Constant.ScreenSize.SCALE_SCREEN))" + "/h/" + "\(Int(50*Constant.ScreenSize.SCALE_SCREEN))"
                self.portraitImageView.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "loadingDefault"))
            }
            if let summary = data?["Content"].string{
                self.summaryLabel.text = summary
            }

        }
    }
    

}
