//
//  DeliveryAddressListCell.swift
//  Limi
//
//  Created by 程巍巍 on 5/19/15.
//  Copyright (c) 2015 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class AccountDeliveryAddressListCell: BaseTableViewCell {

    
    @IBOutlet var containerview: UIView!

    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var phoneLabel: UILabel!
    @IBOutlet var addressLabel: UILabel!
    @IBOutlet var acctionView: UIView!
    
    var radioButton = RadioButton()
    
    var editButton = ActionButton()
    var delButton = ActionButton()
    
    override func defaultInit() {
        
        // 设置字体
        for label in [nameLabel, phoneLabel, addressLabel] {
            label?.font = Constant.CustomFont.Default(size: (label?.font.pointSize)!)
        }
        
        self.acctionView.addSubview(radioButton)
        radioButton.translatesAutoresizingMaskIntoConstraints = false
        self.acctionView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[radio]-0-|", options: [], metrics: nil, views: ["radio": self.radioButton]))
        self.acctionView.addConstraint(NSLayoutConstraint(item: self.radioButton, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: self.acctionView, attribute: NSLayoutAttribute.leading, multiplier: 1, constant: 0))
        
        self.radioButton.isOn = false
        self.radioButton.addTarget(self, action: #selector(AccountDeliveryAddressListCell.radioButtonClick), for: UIControlEvents.touchUpInside)
        
        self.editButton.setTitle("编辑", for: UIControlState())
        self.delButton.setTitle("删除", for: UIControlState())
        self.acctionView.addSubview(self.editButton)
        self.acctionView.addSubview(self.delButton)
        
        self.editButton.translatesAutoresizingMaskIntoConstraints = false
        self.delButton.translatesAutoresizingMaskIntoConstraints = false
        self.acctionView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[del]-0-|", options: [], metrics: nil, views: ["del": self.delButton]))
        self.acctionView.addConstraint(NSLayoutConstraint(item: self.delButton, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: self.acctionView, attribute: NSLayoutAttribute.trailing, multiplier: 1, constant: -8))
        
        self.acctionView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[edit]-0-|", options: [], metrics: nil, views: ["edit": self.editButton]))
        self.acctionView.addConstraint(NSLayoutConstraint(item: self.editButton, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: self.delButton, attribute: NSLayoutAttribute.leading, multiplier: 1, constant: -8))
        
        
        //
        containerview.layer.borderColor = UIColor.lightGray.cgColor
        containerview.layer.borderWidth = 0.5
        
    }
    
    func radioButtonClick(){
        self.radioButton.isOn = !self.radioButton.isOn
    }
    
    override class func nibName()->String! {
        return "AccountDeliveryAddressListCell"
    }
    
    override class func heightWithData(_ data: [String: AnyObject], width: CGFloat)-> CGFloat{
        return 88
    }

}

extension AccountDeliveryAddressListCell {
    
    class ActionButton: UIButton {
        override init(frame: CGRect) {
            super.init(frame: frame)
            self.setTitleColor(UIColor.black, for: UIControlState())
            self.setTitleColor(UIColor.darkGray, for: UIControlState.highlighted)
            self.titleLabel?.font = Constant.CustomFont.Default(size: 15)
        }
        
        required init(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            
            self.imageView?.frame = CGRect(x: 0, y: 0, width: self.titleLabel!.frame.height, height: self.titleLabel!.frame.height)
            let width = self.imageView!.frame.width + 3 + self.titleLabel!.frame.width
            self.imageView?.center = CGPoint(x: self.frame.width/2 - width/2 + self.imageView!.frame.width/2, y: self.frame.height/2)
            self.titleLabel?.center = CGPoint(x: self.frame.width/2 + width/2 - self.titleLabel!.frame.width/2, y: self.frame.height/2)
        }
    }
    class RadioButton: ActionButton {
        var isOn: Bool = false {
            didSet {
                let imageName = isOn ? "choosed" : "noChoose"
                self.setImage(UIImage(named: imageName), for: UIControlState())
                self.setTitle("默认", for: UIControlState())
            }
        }
    }
}
