//
//  CommunityCommonCell.swift
//  Limi
//
//  Created by maohs on 16/8/4.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//问答社区 语音+商品cell
import UIKit

class CommunitySpeechCell: CommunityBaseCell {
    
    fileprivate let scale = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
    let bannerViewHeight = (316 + 30 + 40) * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE + 14
    fileprivate let authorImageWidth = 80 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
    
    fileprivate var speechModel:CommunitySpeechModel!
    fileprivate var index = 0
    
    fileprivate var goodsAry = [GoodsModel]()
    
    fileprivate let contextView:UIView = {
       let view = UIView()
        view.backgroundColor = UIColor.white
        
        return view
    }()
    
    fileprivate let questionLabel:UILabel = {
       let lbl = UILabel()
        lbl.numberOfLines = 0
        lbl.textColor = UIColor(rgba: Constant.common_C2_color)
        lbl.font = UIFont.systemFont(ofSize: Constant.common_F2_font)
        
        return lbl
    }()
    
    //达人信息
    fileprivate let authorInfoLabel:UILabel = {
       let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: Constant.common_F5_font)
        lbl.textColor = UIColor(rgba: Constant.common_C7_color)
        
        return lbl
    }()
    
    fileprivate let authorImage:UIImageView = {
        let image = UIImageView()
        image.layer.cornerRadius = 40 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        image.layer.masksToBounds = true
        image.image = UIImage(named: "accountIcon")
        
        return image
    }()
    
    //达人语音回复
    fileprivate let speechButton:UIButton = {
        let btn = UIButton(type: .custom)
        
        btn.setImage(UIImage(named: "speechBackImage"), for: UIControlState())
        return btn
    }()
    
    fileprivate let speechImage:UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "speechPlayImage")
//        image.userInteractionEnabled = true
        return image
    }()
    
    fileprivate let speechLabel:UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .right
        lbl.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        lbl.textColor = UIColor(rgba: Constant.common_C12_color)
//        lbl.userInteractionEnabled = true
        
        return lbl
    }()
    
    //浏览 点赞
    fileprivate let skimLabel:UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .right
        lbl.font = UIFont.systemFont(ofSize: 12)
        lbl.textColor = UIColor(rgba: Constant.common_C7_color)
        
        return lbl
    }()
    
    fileprivate let praiseButton:UIButton = {
       let btn = UIButton(type: .custom)
        return btn
    }()
    
    fileprivate let praiseImage:UIImageView = {
       let image = UIImageView()
        image.image = UIImage(named: "praiseNormal")
        return image
    }()
    
    fileprivate let praiseLabel:UILabel = {
       let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: 12)
        lbl.textColor = UIColor(rgba: Constant.common_C7_color)
        lbl.textAlignment = .right
        
        return lbl
    }()
    
    
    //产品列表
    fileprivate let bannerView = UIView()
    fileprivate let bannerLine:UIView = {
       let view = UIView()
        view.backgroundColor = UIColor(rgba: Constant.common_separatLine_color)
        
        return view
    }()
    
    fileprivate let bannerTitleLabel:UILabel = {
       let lbl = UILabel()
        lbl.textAlignment = .left
        lbl.textColor = Constant.Theme.Color14
        lbl.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        
        return lbl
    }()
    
    fileprivate var banner:LMHorizontalTableView!
    
    override class func getReuseIdentifier() ->String {return "CommunitySpeechCell"}
    
    override func setUpUI() {
        
        self.contentView.addSubview(contextView)
        contextView.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(self)
            let _ = make.left.equalTo(self)
            let _ = make.right.equalTo(self)
            let _ = make.bottom.equalTo(self).offset(-20 * scale)
        }
        
        
        for view in [questionLabel,authorInfoLabel,authorImage,speechButton,skimLabel,praiseButton] {
            contextView.addSubview(view)
        }
        
        questionLabel.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(self).offset(40 * scale)
            let _ = make.left.equalTo(self).offset(30 * scale)
            let _ = make.right.equalTo(self).offset(-30 * scale)
            let _ = make.height.greaterThanOrEqualTo(20)
        }
        
        authorInfoLabel.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(questionLabel.snp_bottom).offset(30 * scale)
            let _ = make.left.equalTo(self).offset(30 * scale)
            let _ = make.right.equalTo(self).offset(-30 * scale)
            let _ = make.height.equalTo(12)
        }
        
        authorImage.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(authorInfoLabel.snp_bottom).offset(30 * scale)
            let _ = make.left.equalTo(self).offset(30 * scale)
            let _ = make.width.equalTo(80 * scale)
            let _ = make.height.equalTo(80 * scale)
        }
        
        speechButton.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(authorInfoLabel.snp_bottom).offset(30 * scale)
            let _ = make.left.equalTo(authorImage.snp_right).offset(10 * scale)
            let _ = make.width.equalTo(504 * scale)
            let _ = make.height.equalTo(80 * scale)
        }
        
        speechButton.addSubview(speechImage)
        speechButton.addSubview(speechLabel)
        
        speechImage.snp_makeConstraints { (make) in
            let _ = make.left.equalTo(50 * scale)
            let _ = make.centerY.equalTo(speechButton.iheight / 2)
            let _ = make.width.equalTo(40 * scale)
            let _ = make.height.equalTo(40 * scale)
        }
        
        speechLabel.snp_makeConstraints { (make) in
            let _ = make.left.equalTo(speechImage.snp_right).offset(30 * scale)
            let _ = make.right.equalTo(speechButton).offset(-40 * scale)
            let _ = make.centerY.equalTo(speechButton.iheight / 2)
            let _ = make.height.equalTo(15)
        }
        
        speechButton.addTarget(self, action: #selector(CommunitySpeechCell.speechButtonClicked), for: .touchUpInside)
        
        //浏览 点赞
        praiseButton.addSubview(praiseLabel)
        praiseButton.addSubview(praiseImage)
        praiseButton.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(speechButton.snp_bottom).offset(35 * scale)
            let _ = make.right.equalTo(self)
            let _ = make.width.equalTo(80)
            let _ = make.height.equalTo(40 * scale)
        }
        
        praiseLabel.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(praiseButton).offset(4 * scale)
            let _ = make.right.equalTo(praiseButton).offset(-30 * scale)
            let _ = make.height.equalTo(15)
        }
        
        praiseImage.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(praiseButton)
            let _ = make.right.equalTo(praiseLabel.snp_left).offset(-4 * scale)
            let _ = make.width.equalTo(15)
            let _ = make.height.equalTo(15)
        }
        
        skimLabel.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(speechButton.snp_bottom).offset(40 * scale)
            let _ = make.right.equalTo(praiseButton.snp_left).offset(-20 * scale)
            let _ = make.left.equalTo(self).offset(30 * scale)
            let _ = make.height.equalTo(12)
        }
        
        praiseButton.addTarget(self, action: #selector(CommunitySpeechCell.praiseButtonClicked), for: .touchUpInside)
        
        //产品
        contextView.addSubview(bannerView)
        bannerView.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(skimLabel.snp_bottom).offset(20 * scale)
            let _ = make.left.equalTo(self)
            let _ = make.right.equalTo(self)
            let _ = make.height.equalTo(bannerViewHeight)
        }
        
        let tempFrame = CGRect(x: 0, y: 0,width: Constant.ScreenSizeV2.SCREEN_WIDTH - 60 * scale, height: SubCell.cell_height)
        banner = LMHorizontalTableView(frame: tempFrame)
        banner.dataSource = self
        banner.delegate = self
        for view in [bannerLine,bannerTitleLabel,banner] {
            bannerView.addSubview(view)
        }
        
        bannerLine.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(bannerView)
            let _ = make.left.equalTo(self).offset(30 * scale)
            let _ = make.right.equalTo(self).offset(-30 * scale)
            let _ = make.height.equalTo(1)
        }
        
        bannerTitleLabel.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(bannerView).offset(30 * scale)
            let _ = make.left.equalTo(self).offset(30 * scale)
            let _ = make.right.equalTo(self).offset(-30 * scale)
            let _ = make.height.equalTo(14)
        }
        
        banner.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(bannerTitleLabel.snp_bottom).offset(40 * scale)
            let _ = make.left.equalTo(self).offset(30 * scale)
            let _ = make.width.equalTo(Constant.ScreenSizeV2.SCREEN_WIDTH - 60 * scale)
            let _ = make.height.equalTo(SubCell.cell_height)
        }
        
        bannerView.isHidden = true
    }

    override func refreshWithData(_ data:CommunityCommonModel,index:Int) {
        self.index = index
        if let tempSpeechData = data as? CommunitySpeechModel {
            speechModel = tempSpeechData
        }else {
            return
        }
        
        if speechModel.is_nice == 2 {
            //加精
            questionLabel.text = " " + speechModel.content
            let attachment = NSTextAttachment()
            attachment.image = UIImage(named: "essenceIcon")
            attachment.bounds = CGRect(x: 0, y: -3, width: 35, height: 18)
            let attStr = NSAttributedString(attachment: attachment)
            let mutableStr = NSMutableAttributedString(attributedString: questionLabel.attributedText!)
            mutableStr.insert(attStr, at: 0)
            questionLabel.attributedText = mutableStr
            
        }else {
            //未加精
            questionLabel.text = data.content
        }

//        let attributedString = NSMutableAttributedString(string: questionLabel.text! as String)
//        let paragraphStyle = NSMutableParagraphStyle()
//        paragraphStyle.lineSpacing = Constant.ScreenSizeV2.MARGIN_15
//        attributedString.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: NSMakeRange(0, (questionLabel.text?.characters.count)!))
//        questionLabel.attributedText = attributedString
        
        if let name = speechModel.master?.name {
            authorInfoLabel.text = name
        }
        
        if let intro = speechModel.master?.intro {
            if intro.characters.count > 0 {
                authorInfoLabel.text = authorInfoLabel.text! + "，" + intro
            }
        }
        
        if let url = URL(string: Utils.scaleImage((speechModel.master?.avatar)!, width:authorImageWidth * Constant.ScreenSizeV2.SCALE_SCREEN, height: authorImageWidth * Constant.ScreenSizeV2.SCALE_SCREEN)){
            authorImage.af_setImageWithURL(url, placeholderImage: UIImage(named: "accountIcon"))
        }
        
        speechLabel.text = speechModel.sns_reply?.second
        if speechModel.is_play {
            speechButton.isSelected = true
            speechImage.image = UIImage(named: "speechPauseImage")
        }else {
            speechButton.isSelected = false
            speechImage.image = UIImage(named: "speechPlayImage")
        }
        
        
        
        skimLabel.text = "浏览数 " + "\(speechModel.total_view_num)"
        
        praiseLabel.text = " " +  String(speechModel.total_like_num)
        if speechModel.is_like == 1 {
            praiseImage.image = UIImage(named: "praiseSelected")
            praiseLabel.textColor = UIColor(rgba: Constant.common_C1_color)
        }else {
            praiseImage.image = UIImage(named: "praiseNormal")
            praiseLabel.textColor = UIColor(rgba: Constant.common_C7_color)
        }
        
        if speechModel.goods_ids.count > 0 {
            bannerView.isHidden = false
            self.bannerTitleLabel.text = "回答中提到的礼物"
            self.goodsAry = [GoodsModel]()
            self.goodsAry = speechModel.goods_ids
            self.banner.reloadData()
        }else {
            bannerView.isHidden = true
        }
    }
    
    //播放语音按钮
    func speechButtonClicked() {
        //默认selected为false
        if speechButton.isSelected {
            //停止
            speechImage.image = UIImage(named: "speechPlayImage")
            LMOnlineAudio.shareInstance.stopAudio()
        }else {
            //开始播放
            speechImage.image = UIImage(named: "speechPauseImage")
            LMOnlineAudio.shareInstance.playAudio((speechModel!.sns_reply?.content)!)
        }
        speechButton.isSelected = !speechButton.isSelected
        delegate?.communityBaseCellSkimButtonChanged!(speechModel!.id,type:2,index:self.index)
    }
    
    //点赞按钮
    func praiseButtonClicked() {
        if UserInfoManager.didLogin {
            if speechModel.is_like == 1 {
                //已经点赞，再点击表示取消
                speechModel.total_like_num -= 1
                speechModel.is_like = 2
                praiseLabel.text = " " + String(speechModel.total_like_num)
                praiseImage.image = UIImage(named: "praiseNormal")
                praiseLabel.textColor = UIColor(rgba: Constant.common_C7_color)
            }else {
                //没有点赞，再点击表示点赞
                speechModel.total_like_num += 1
                speechModel.is_like = 1
                praiseLabel.text = " " + String(speechModel.total_like_num)
                praiseImage.image = UIImage(named: "praiseSelected")
                praiseLabel.textColor = UIColor(rgba: Constant.common_C1_color)
            }
        }
        
        delegate?.communityBaseCellPraiseButtonChanged!(speechModel,index:index)
    }
    
}

extension CommunitySpeechCell:LMHorizontalTableViewDataSource,LMHorizontalTableViewDelegate {
    func numberOfRows(_ horizontalTableView: LMHorizontalTableView) -> Int {
        return self.goodsAry.count
    }
    
    func horizontalTableView(_ horizontalTableView: LMHorizontalTableView, cellForRow row: Int, reuseCell: ((_ reuseInfentifer: String) -> LMHorizontalTableViewCell?)) -> LMHorizontalTableViewCell {
        var cell:SubCell? = reuseCell(SubCell.identifier) as? SubCell
        if cell == nil {
            cell = SubCell(reuseIdentifier:SubCell.identifier)
        }
        cell!.reloadData(goodsAry[row])
        return cell!
    }
    
    func horizontalTableView(_ horizontalTableView: LMHorizontalTableView, widthForRow row: Int) -> CGFloat {
        return SubCell.cell_width
    }
    
    func horizontalTableView(_ horizontalTableView: LMHorizontalTableView, heightForRow row: Int) -> CGFloat {
        return SubCell.cell_height
    }
    
    func horizontalTableView(_ horizontalTableView: LMHorizontalTableView, didSelectRow row: Int) {
        delegate?.communityBaseCellGoodsClicked!(self.goodsAry[row].gid)
    }
    
}


