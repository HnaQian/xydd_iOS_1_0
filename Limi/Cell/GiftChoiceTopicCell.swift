//
//  GiftChoiceTopicCell.swift
//  Limi
//
//  Created by guo chen on 15/12/28.
//  Copyright © 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class GiftChoiceTopicCell: UICollectionViewCell
{
    let topicImageView = UIImageView()
    
    let titleLabel = CustomLabel(font: Constant.common_F2_font, textColorHex: "#ffffff", textAlignment: NSTextAlignment.center)
    
    let width:CGFloat = (Constant.ScreenSize.SCREEN_WIDTH/2) - 15
    
    override init(frame:CGRect)
    {
        super.init(frame:CGRect.zero)
        
        self.setupUI()
    }
    
    
    
    func setupUI()
    {
        self.frame = CGRect(x: 0, y: 0, width: width, height: width)
        
        topicImageView.frame = self.bounds
        

        self.addSubview(topicImageView)
        self.addSubview(titleLabel)
        
        topicImageView.backgroundColor = UIColor.clear
        topicImageView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self)
            let _ = make.left.equalTo(self)
            let _ = make.right.equalTo(self)
            let _ = make.bottom.equalTo(self)
        }
        
        titleLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.left.equalTo(self)
            let _ = make.width.equalTo(self)
            let _ = make.bottom.equalTo(self).offset(-10)
            let _ = make.height.equalTo(20)
        }
        
        topicImageView.layer.cornerRadius = 3
        
        topicImageView.clipsToBounds = true

        topicImageView.contentMode = UIViewContentMode.scaleAspectFill
        
        
        
    }
    
    
    
    func reloadData(_ data:JSON)
    {
        if let name = data["attributes"]["src"].string
        {
            if name.characters.count > 0
            {
                let imageScale : String = name + "?imageView2/0/w/" + "\(Int(80*Constant.ScreenSize.SCALE_SCREEN))" + "/h/" + "\(Int(80*Constant.ScreenSize.SCALE_SCREEN))"
                
                topicImageView.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "loadingDefault"))
            }
        }
        
        //title
        if let name = data["attributes"]["title"].string
        {
            if name.characters.count > 0
            {
                titleLabel.text = name
            }
        }
        
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
