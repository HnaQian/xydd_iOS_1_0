//
//  CouponTableViewCell.swift
//  Limi
//
//  Created by guo chen on 15/9/11.
//  Copyright (c) 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

@objc protocol CouponTableViewCellDelegate:NSObjectProtocol {
    @objc optional func couponCellStatusChanged(isselected:Bool,status:Int,index:Int)
}

class CouponTableViewCell: UITableViewCell
{
    static let identifier = "CouponTableViewCell"
    //价格
    let Par_value:UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor(rgba: "#cfa972")
        lbl.font = UIFont.systemFont(ofSize: Constant.Theme.getFontSizeForScreenSizeByScal(45))
        return lbl
    }()
    
    let Name:UILabel = {
        let lbl = UILabel()
        lbl.textColor = Constant.Theme.Color14
        lbl.font = Constant.Theme.Font_17
        lbl.textAlignment = .right
        return lbl
    }()
    
    let Min_title:UILabel = {
        let lbl = UILabel()
        lbl.textColor = Constant.Theme.Color17
        lbl.font = UIFont.systemFont(ofSize: Constant.Theme.getFontSizeForScreenSizeByScal(12))
        lbl.textAlignment = .right
        return lbl
    }()
    
    let Scope_name:UILabel = {
        let lbl = UILabel()
        lbl.textColor = Constant.Theme.Color17
        lbl.font = UIFont.systemFont(ofSize: Constant.Theme.getFontSizeForScreenSizeByScal(12))
        lbl.textAlignment = .right
        return lbl
    }()
    
    //时间
    let timeDue:UILabel = {
        let lbl = UILabel()
        lbl.textColor = Constant.Theme.Color17
        lbl.font = UIFont.systemFont(ofSize: Constant.Theme.getFontSizeForScreenSizeByScal(12))
        lbl.textAlignment = .right
        return lbl
    }()
    
    let TypeLabel:UILabel = {
        let lbl = UILabel()
        lbl.textColor = Constant.Theme.Color1
        lbl.font = UIFont.systemFont(ofSize: Constant.Theme.getFontSizeForScreenSizeByScal(12))
        return lbl
    }()
    
    let instructionLabel:UILabel = {
        let lbl = UILabel()
        lbl.textColor = Constant.Theme.Color17
        lbl.font = Constant.Theme.Font_14
        lbl.isUserInteractionEnabled = true
        return lbl
    }()
    
    let directionImage:UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "coupon_downMark")
        return image
    }()
    
    let instructionContentLabel:UILabel = {
        let lbl = UILabel()
        lbl.textColor = Constant.Theme.Color17
        lbl.font = UIFont.systemFont(ofSize: Constant.Theme.getFontSizeForScreenSizeByScal(12))
        lbl.numberOfLines = 0
        return lbl
    }()
    
    let instructionContentView = UIView()
    let bgContentView = UIView()
    var bgImagView1 = UIImageView()
    var bgImagView2 = UIImageView()
    
    var cellIndex = 0
    weak var delegate:CouponTableViewCellDelegate?
    var cellStatus = 0
    var dataDict:CouponModel!
    
    func setPropertise(_ dataDict: CouponModel, status: Int,index:Int)
    {
        self.dataDict = dataDict
        self.cellStatus = status
        self.cellIndex = index
        self.Name.text = dataDict.name
        
        self.Min_title.text = dataDict.min_title
        
        self.Scope_name.text = dataDict.scope_name

        let parValue:Float = Float(dataDict.par_value)
        if parValue > 0{
            let floatPrice = roundf(parValue)
            if floatPrice == parValue{
                self.Par_value.text = String(format: "￥%.0f", parValue)
            }
            else{
                let priceStr1 : NSString = String(format: "%.2f", parValue) as NSString
                let priceStr2 : NSString = String(format: "%.1f", parValue) as NSString
                let floatPrice1 : Float = priceStr1.floatValue
                let floatPrice2 : Float = priceStr2.floatValue
                if floatPrice1 == floatPrice2{
                    self.Par_value.text = String(format: "￥%.1f", parValue)
                }
                else{
                    self.Par_value.text = String(format: "￥%.2f", parValue)
                }
                
            }
            
            //对价格之前的人民币符号大小设置
            let attributedString = NSMutableAttributedString(string: self.Par_value.text! as String)
            let attributes = [NSFontAttributeName:Constant.Theme.Font_15]
            attributedString.addAttributes(attributes, range: NSRange(location: 0, length: 1))
            
            self.Par_value.attributedText = attributedString
        }

        self.timeDue.text = dataDict.time_title
        let width = CalculateHeightOrWidth.getLabOrBtnWidth(self.timeDue.text! as NSString, font: self.timeDue.font, height: 60 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
        timeDue.iwidth = width
        
        self.bgImagView1.image = UIImage(named: "coupon_unuserble1")
        self.bgImagView2.image = UIImage(named: "coupon_unuserble2")
        // 未使用
        if status == 0 {
            // 判断优惠券是否即将过期（7天内过期）
            let endTime = dataDict.end_time
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            if let endDate: Date = formatter.date(from: endTime) {
            
                let time: TimeInterval = endDate.timeIntervalSinceNow
                self.bgImagView1.image = UIImage(named: "coupon_userble1")
                self.bgImagView2.image = UIImage(named: "coupon_userble2")
                if time - 28800 <= 604800 {
                    TypeLabel.text = "即将过期"
                    TypeLabel.textColor = UIColor(rgba: "#E13636")
                    let width = CalculateHeightOrWidth.getLabOrBtnWidth(self.TypeLabel.text! as NSString, font: self.TypeLabel.font, height: 60 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
                    TypeLabel.iwidth = width
                }else
                {
                    TypeLabel.iwidth = 0
                    TypeLabel.isHidden = true
                }
            }
            
        }
        // 已过期 // 已使用
        else
        {
            TypeLabel.iwidth = 0
            self.TypeLabel.isHidden = true
            
        }
        TypeLabel.iright = Name.iright
        timeDue.iright = TypeLabel.ileft - 2
        
        if dataDict.isSelected {
            self.directionImage.image = UIImage(named:"coupon_upMark")
            self.instructionContentView.iheight = dataDict.externCellHeight - 270 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
            self.instructionContentLabel.iheight = self.instructionContentView.iheight
            self.instructionContentLabel.text = dataDict.content
        }else {
            self.directionImage.image = UIImage(named: "coupon_downMark")
            if dataDict.content == "" {
                self.instructionLabel.isHidden = true
                self.instructionLabel.isUserInteractionEnabled = false
                self.directionImage.isHidden = true
            }
            self.instructionContentLabel.text = ""
            self.instructionContentView.iheight = 0
        }

    }
    
    
    /*
    
    Id: 3,
    Cid: 5,
    Name: "国庆节礼包",
    Par_value: 1001,
    Min_value: 10,
    Min_title: "单笔订单满10可用",
    Scope: 1,
    Scope_name: "国庆专区",
    Url: "http://wechat.giftyou.me/index?app=1",
    Start_time: "2015-08-06 18:40:41",
    End_time: "2015-08-15 19:40:41",
    Time_title: "2015.08.06 -- 2015.08.15"
    
    */
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setUpUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUpUI() {
        
        self.backgroundColor =  UIColor(red: 0.96, green: 0.96, blue: 0.96, alpha: 1)
        self.selectionStyle = .none
        self.contentView.addSubview(bgContentView)
        
        bgContentView.addSubview(bgImagView1)
        bgContentView.addSubview(bgImagView2)
        bgContentView.addSubview(Name)
        bgContentView.addSubview(timeDue)
        bgContentView.addSubview(Min_title)
        bgContentView.addSubview(Scope_name)
        bgContentView.addSubview(TypeLabel)
        
        bgContentView.addSubview(Par_value)
        bgContentView.addSubview(instructionLabel)
        bgContentView.addSubview(directionImage)
        
        bgContentView.addSubview(instructionContentView)
        instructionContentView.addSubview(instructionContentLabel)
        instructionContentView.backgroundColor = UIColor.white
        let scale = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        
        bgContentView.frame = CGRect(x: 20 * scale, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH - 40 * scale, height: 240 * scale)
        bgImagView1.frame = CGRect(x: 0, y: 0, width: bgContentView.iwidth, height: 180 * scale)
        bgImagView2.frame = CGRect(x: 0, y: bgImagView1.ibottom, width: bgImagView1.iwidth, height: 60 * scale)
        
        //right
        Name.frame = CGRect(x: Constant.ScreenSize.SCREEN_WIDTH / 2 - 80 * scale, y: 40 * scale, width:Constant.ScreenSize.SCREEN_WIDTH / 2, height: 21)
        
        Min_title.frame = CGRect(x: Name.ileft, y: Name.ibottom + 10 * scale,width: Name.iwidth, height: 15)
        
        Scope_name.frame = CGRect( x: Min_title.ileft, y: Min_title.ibottom + 10 * scale, width: Name.iwidth, height: 15)
        
        timeDue.frame = CGRect(x: Name.ileft - 80 * scale, y: 180 * scale, width: 211 * Constant.ScreenSize.SCREEN_WIDTH_SCALE, height: 60 * scale)
        
        TypeLabel.frame = CGRect(x: timeDue.iright + 8 * scale, y: timeDue.itop, width: 64.5 * Constant.ScreenSize.SCREEN_WIDTH_SCALE, height: 60 * scale)
        
        //left
        Par_value.frame = CGRect(x:40 * scale, y: 56 * scale, width: Constant.ScreenSize.SCREEN_WIDTH / 2 - 120 * scale, height: 53)
        instructionLabel.frame = CGRect(x: 40 * scale, y: 180 * scale, width: 80, height: 60 * scale)
        directionImage.frame = CGRect(x: instructionLabel.iright, y: 197 * scale, width:26 * scale, height: 26 * scale)
        
        //扩展的部分
        instructionContentView.frame = CGRect(x: 0, y: bgImagView2.ibottom, width: bgImagView2.iwidth, height: 0)
        instructionContentLabel.frame = CGRect(x: 40 * scale, y: 0, width: instructionContentView.iwidth - 80 * scale, height: 0)
        self.instructionLabel.text = "详细信息"
        let width = CalculateHeightOrWidth.getLabOrBtnWidth(self.instructionLabel.text! as NSString, font: self.instructionLabel.font, height: 60 * scale)
        instructionLabel.iwidth = width
        directionImage.ileft = instructionLabel.iright
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(CouponTableViewCell.instructionLabelClicked))
        instructionLabel.addGestureRecognizer(tapGesture)
        
    }
    
    func instructionLabelClicked() {
        //isDelected==true 展开 isDelected==false 收起
        self.dataDict.isSelected = !self.dataDict.isSelected
        self.delegate?.couponCellStatusChanged!(isselected: self.dataDict.isSelected,status: self.cellStatus, index: self.cellIndex)
        if self.dataDict.isSelected {
            self.directionImage.image = UIImage(named:"coupon_upMark")
        }else {
            self.directionImage.image = UIImage(named: "coupon_downMark")
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
