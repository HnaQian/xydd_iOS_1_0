//
//  ConvertNotesCell.swift
//  Limi
//
//  Created by Richie on 16/6/1.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//

import UIKit

class ConvertNotesCell: UITableViewCell {

    static let reuseIdentifier = "convertNotesCell"
    static let height:CGFloat = 40
    
    
    fileprivate let titleLbl:UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor(rgba: Constant.common_C6_color)
        lbl.font  = UIFont.systemFont(ofSize: Constant.common_F5_font)
        lbl.frame = CGRect(x: 0, y: 0, width: 120, height: 20)
        return lbl
    }()
    
    fileprivate let valueLbl:UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor(rgba: Constant.common_C1_color)
        lbl.font  = UIFont.systemFont(ofSize: Constant.common_F5_font)
        lbl.frame = CGRect(x: 0, y: 0, width: 60, height: 20)
        return lbl
    }()
    
    fileprivate let timeLbl:UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = NSTextAlignment.right
        lbl.textColor = UIColor(rgba: Constant.common_C7_color)
        lbl.font  = UIFont.systemFont(ofSize: Constant.common_F5_font)
        lbl.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH * 3/5, height: 20)
        return lbl
    }()
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = UITableViewCellSelectionStyle.none
        self.setupUI()
    }
    
    
    func refreshData(_ dataModel:ConvertNotesDataModel){
        //dataModel.type == 2:连续签到
        if dataModel.type < 6{
            valueLbl.text = "+\(dataModel.point)"
        }else{
            //dataModel.type == 6
            valueLbl.text = "-\(dataModel.point)"
        }
        
        titleLbl.text = dataModel.type_name
        timeLbl.text = dataModel.add_time
    }
    
    
    
    fileprivate func setupUI(){
        
        let _ = [titleLbl,valueLbl,timeLbl].map{self.addSubview($0)}
        
        titleLbl.ix = MARGIN_20
        titleLbl.icenterY = ConvertNotesCell.height/2
        
        valueLbl.ix = Constant.ScreenSize.SCREEN_WIDTH/3
        valueLbl.icenterY = titleLbl.icenterY
        
        timeLbl.iright = Constant.ScreenSize.SCREEN_WIDTH - MARGIN_20
        timeLbl.icenterY = titleLbl.icenterY
        
        let line = UIView(frame: CGRect(x: 0,y: ConvertNotesCell.height - LINE_HEIGHT,width: Constant.ScreenSize.SCREEN_WIDTH,height: LINE_HEIGHT))
        line.backgroundColor = UIColor(rgba: Constant.common_separatLine_color)
        self.addSubview(line)
    }
    

    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    
}




class ConvertNotesDataModel:BaseDataMode{
    /*
     "add_time" = "2016-05-27 00:00:00";
     id = 51;
     point = 1;
     type = 1;
     "type_name" = "\U7b7e\U5230";
     */
    
    var id = NULL_INT
    var type = NULL_INT
    var point = NULL_INT
    var add_time = NULL_STRING
    var type_name = NULL_STRING
}
