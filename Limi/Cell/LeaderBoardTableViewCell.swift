//
//  LeaderBoardTableViewCell.swift
//  Limi
//
//  Created by Richie on 16/5/16.
//  Copyright © LeaderBoardTableViewCell.gap16年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//心意榜单的cell

import UIKit

class LeaderBoardTableViewCell: UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    static var reuseIdentifier:String{return "LeaderBoardTableViewCell"}
    
    static let gap:CGFloat = 20 * Constant.ScreenSize.SCREEN_WIDTH_SCALE
    
    static let GoodActionNotification = "LeaderBoardTableViewCell_GoodActionNotification"
    
    static let SendActionNotification = "LeaderBoardTableViewCell_SendActionNotification"
    
    
    fileprivate let top_gap:CGFloat = 15 * Constant.ScreenSize.SCREEN_WIDTH_SCALE
    
    fileprivate var model:LeaderBoardModel!
    
    static func cellHeight(_ row:Int) ->CGFloat{
        
        if row < 3{
            return 200 * Constant.ScreenSize.SCREEN_WIDTH_SCALE
        }else{
            return 149 * Constant.ScreenSize.SCREEN_WIDTH_SCALE
        }
    }
    
    fileprivate let iconAry  = ["Leaderboard_rant1","Leaderboard_rant2","Leaderboard_rant3"]
    fileprivate let goodsImage = UIImageView(image: UIImage(named: "loadingDefault"))
    //goodsImage.image = UIImage(named: "loadingDefault")
    fileprivate let icon = UIImageView()
    
    fileprivate let titleLbl:UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        lbl.textColor = UIColor(rgba: Constant.common_C2_color)
        lbl.textAlignment = NSTextAlignment.right
        return lbl
    }()
    
    
    fileprivate let contentLbl:UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor(rgba:Constant.common_C7_color)
        lbl.font = UIFont.systemFont(ofSize: Constant.common_F5_font)
        lbl.numberOfLines = 0
        return lbl
    }()
    
    fileprivate let subContentView:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    
    fileprivate let menuContentView:UIView = {
        let view = UIView()
        view.layer.cornerRadius = 10
        view.layer.borderWidth = 0.5
        view.layer.borderColor = UIColor(rgba:Constant.common_C1_color).cgColor
        view.backgroundColor = UIColor.white
        
        return view
    }()
    
    
    fileprivate let sendBtn:UIButton = {
        
        let btn = UIButton()
        btn.setTitle("我也送", for: UIControlState())
        
        if Constant.DeviceType.IS_IPHONE_5 || Constant.DeviceType.IS_IPHONE_4_OR_LESS{
            btn.titleLabel?.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        }else{
            btn.titleLabel?.font = UIFont.systemFont(ofSize: Constant.common_F2_font)
        }
        
        btn.setTitleColor(UIColor(rgba:Constant.common_C1_color), for: UIControlState())
        
        return btn
    }()

    fileprivate let goodImage:UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named:"Leaderboard_good_no")
        return img
    }()
    
    
    fileprivate let goodBtn:UIButton = {
        let btn = UIButton()
        return btn
    }()
    
    fileprivate let goodLbl:UILabel = {
        let lbl = UILabel()
        if Constant.DeviceType.IS_IPHONE_5 || Constant.DeviceType.IS_IPHONE_4_OR_LESS{
            lbl.font  = UIFont.systemFont(ofSize: Constant.common_F7_font)
        }else{
            lbl.font  = UIFont.systemFont(ofSize: Constant.common_F6_font)
        }
        
        lbl.textColor = UIColor(rgba:Constant.common_C1_color)
        
        lbl.text = "0"
        
        return lbl
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.clipsToBounds = true
        self.selectionStyle = UITableViewCellSelectionStyle.none
        self.contentView.backgroundColor = UIColor(rgba: Constant.common_C1_color)
        
        self.setupUI()
    }
    
    
    @objc fileprivate func goodBtnAction(){
        if self.model.ORIGINAL_DATA == nil{return}
        NotificationCenter.default.post(name: Notification.Name(rawValue: LeaderBoardTableViewCell.GoodActionNotification), object: nil, userInfo:NSDictionary(dictionary:  ["model":model]) as? [AnyHashable: Any])
    }
    
    @objc fileprivate func sendBtnAction(){
        if self.model.ORIGINAL_DATA == nil{return}
        NotificationCenter.default.post(name: Notification.Name(rawValue: LeaderBoardTableViewCell.SendActionNotification), object: nil, userInfo: NSDictionary(dictionary:  ["model":model]) as? [AnyHashable: Any])
    }
    
    
    //MARK: refreshData
    func refreshData(_ model:LeaderBoardModel,forIndexPath indexPath:IndexPath){
        
        self.model = model
        self.model.indexPath = indexPath
        
        if (indexPath as NSIndexPath).row < 3{
            icon.image = UIImage(named: iconAry[(indexPath as NSIndexPath).row])
        }else{
            icon.image = nil
        }
        
        if self.model.ORIGINAL_DATA == nil{
            
            goodLbl.text = ""
            titleLbl.text = ""
            contentLbl.text = ""
            goodsImage.image = UIImage(named: "Leaderboard_fixImg")
            
            return
        }
        
        contentLbl.text = model.Description
        
        goodImage.image = UIImage(named:model.collect_status ? "Leaderboard_good" : "Leaderboard_good_no")
        
        if let mgoods = model.goods{
            titleLbl.text = mgoods.goods_name
            let edg:CGFloat = LeaderBoardTableViewCell.cellHeight((indexPath as NSIndexPath).row) - top_gap * 2
            
            let scal = Utils.scaleImage(mgoods.goods_image, width: edg * 3, height: edg * 3)
            goodsImage.af_setImageWithURL((URL(string: scal))!, placeholderImage: UIImage(named: "loadingDefault"))
        }
        
        goodLbl.text = "\(model.collect_num)"
        if model.collect_num >= 9999{
            goodLbl.text = "9999+"
        }
        
        if model.goodBtnShouldAnimated{
            
            UIView.animate(withDuration: 0.25, animations: {
                self.goodImage.transform = CGAffineTransform(scaleX: 1.7, y: 1.7)
                }, completion: { (over) in
                    UIView.animate(withDuration: 0.25, animations: {
                        self.goodImage.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
                        }, completion: { (over) in
                            UIView.animate(withDuration: 0.25, animations: {
                                self.goodImage.transform = CGAffineTransform(scaleX: 1, y: 1)
                                }, completion: { (over) in
                                    
                            })
                    })
            })
            
            model.goodBtnShouldAnimated = false
        }
    }
    
    
    
    fileprivate func setupUI(){
        
        self.addSubview(subContentView)
        let _ = [goodsImage,icon,titleLbl,contentLbl,menuContentView].map{subContentView.addSubview($0)}
        
        subContentView.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(self)
            let _ = make.left.equalTo(LeaderBoardTableViewCell.gap)
            let _ = make.right.equalTo(-LeaderBoardTableViewCell.gap)
            let _ = make.bottom.equalTo(self).offset(1)
        }
        
        
        goodsImage.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(subContentView).offset(top_gap)
            let _ = make.left.equalTo(LeaderBoardTableViewCell.gap)
            let _ = make.bottom.equalTo(subContentView).offset(-top_gap)
            let _ = make.width.equalTo(goodsImage.snp_height)
        }
        
        icon.snp_makeConstraints { (make) in
            let _ = make.left.equalTo(goodsImage).offset(-3)
            let _ = make.top.equalTo(goodsImage).offset(-3)
            let _ = make.height.equalTo(50 * Constant.ScreenSize.SCREEN_WIDTH_SCALE)
            let _ = make.width.equalTo(50 * Constant.ScreenSize.SCREEN_WIDTH_SCALE)
        }
        
        titleLbl.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(goodsImage)
            let _ = make.left.equalTo(goodsImage.snp_right).offset(LeaderBoardTableViewCell.gap)
            let _ = make.right.equalTo(subContentView).offset(-LeaderBoardTableViewCell.gap)
            let _ = make.height.equalTo(LeaderBoardTableViewCell.gap)
        }
        
        
        contentLbl.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(titleLbl.snp_bottom).offset(MARGIN_4)
            let _ = make.left.equalTo(titleLbl)
            let _ = make.right.equalTo(titleLbl)
            let _ = make.bottom.lessThanOrEqualTo(menuContentView.snp_top).offset(-MARGIN_4)
        }
        
        menuContentView.snp_makeConstraints { (make) in
            let _ = make.right.equalTo(titleLbl)
            let _ = make.bottom.equalTo(goodsImage)
            let _ = make.width.equalTo(140 * Constant.ScreenSize.SCREEN_WIDTH_SCALE)
            let _ = make.height.equalTo(40 * Constant.ScreenSize.SCREEN_WIDTH_SCALE)
        }
        
        
        
        let line = UIView()
        line.backgroundColor  = UIColor(rgba: Constant.common_C1_color)
        
        goodBtn.addTarget(self, action: #selector(LeaderBoardTableViewCell.goodBtnAction), for: UIControlEvents.touchUpInside)
        
        sendBtn.addTarget(self, action: #selector(LeaderBoardTableViewCell.sendBtnAction), for: UIControlEvents.touchUpInside)
        
        let _ = [goodImage,goodLbl,sendBtn,line,goodBtn].map{menuContentView.addSubview($0)}
        goodImage.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(menuContentView.snp_centerY).offset(-MARGIN_13)
            let _ = make.left.equalTo(menuContentView).offset(MARGIN_13)
            let _ = make.size.equalTo(16)
        }
        
        goodBtn.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(menuContentView)
            let _ = make.bottom.equalTo(menuContentView)
            let _ = make.left.equalTo(menuContentView)
            let _ = make.right.equalTo(line.snp_right)
        }
        
        goodLbl.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(goodImage.snp_bottom)
            let _ = make.centerX.equalTo(goodImage)
            let _ = make.height.equalTo(13)
        }
        
        sendBtn.snp_makeConstraints { (make) in
            let _ = make.right.equalTo(menuContentView)
            let _ = make.left.equalTo(line.snp_right)
            let _ = make.centerY.equalTo(menuContentView)
            let _ = make.height.equalTo(20)
        }
        
        line.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(menuContentView).offset(MARGIN_4)
            let _ = make.bottom.equalTo(menuContentView).offset(-MARGIN_4)
            let _ = make.left.equalTo(goodImage.snp_right).offset(MARGIN_8)
            let _ = make.width.equalTo(0.5)
        }
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}


class LaederBoardLastCell: UITableViewCell {
    
    static let reuseIdentifier = "LaederBoardLastCell"
    
    static let cellHeight:CGFloat = 78 * Constant.ScreenSize.SCREEN_WIDTH_SCALE
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.clipsToBounds = true
        self.selectionStyle = UITableViewCellSelectionStyle.none
        self.contentView.backgroundColor = UIColor(rgba: Constant.common_C1_color)
        
        self.setupUI()
    }
    
    fileprivate let whiteView = UIView()
    
    fileprivate func setupUI(){
        
        self.addSubview(whiteView)
        
        whiteView.backgroundColor = UIColor.white
        
        whiteView.layer.cornerRadius = 20 * Constant.ScreenSize.SCREEN_WIDTH_SCALE
        
        whiteView.frame = CGRect(x: LeaderBoardTableViewCell.gap, y: -LaederBoardLastCell.cellHeight/3, width: Constant.ScreenSize.SCREEN_WIDTH - LeaderBoardTableViewCell.gap * 2, height: LaederBoardLastCell.cellHeight * 2/3)
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

