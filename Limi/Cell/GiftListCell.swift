//
//  GiftListCell.swift
//  Limi
//
//  Created by ZhiQiangMi on 15/9/9.
//  Copyright (c) 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

protocol GiftListCellDelegate{
    func rightBtnClick(_ item: JSON)
    func leftBtnClick(_ item: JSON)
}

class GiftListCell: UITableViewCell {
    var orderStatus : Int = 0
    var item: JSON = []
    var delegate: GiftListCellDelegate?
    @IBOutlet weak var rightBtn: UIButton!
    @IBOutlet weak var leftBtn: UIButton!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var goodSubtitleLabel: UILabel!
    @IBOutlet weak var goodNumLabel: UILabel!
    @IBOutlet weak var goodTitleLabel: UILabel!
    @IBOutlet weak var goodImageView: UIImageView!
    @IBOutlet weak var orderNumLabel: UILabel!
    @IBOutlet weak var orderStatusLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setData(_ item: JSON,type : Int){
        self.item = item
        self.orderNumLabel.text = "订单号  " + String(item["Oid"].int!)
        if let status = item["Order_status"].int{
            orderStatus = status
            if status == 1{
                self.orderStatusLabel.text = "待付款"
            }else if status == 2{
                self.orderStatusLabel.text = "待发货"
            }
            else if status == 3{
                self.orderStatusLabel.text = "待收货"
            }
            else if status == 4{
                self.orderStatusLabel.text = "已完成"
            }
            else if status == 5{
                self.orderStatusLabel.text = "售后中"
            }
        }
        
        if item["Order_type"].intValue == 2{
            if let img = item["Order_image"].string{
            let imageScale : String = img + "?imageView2/0/w/" + "\(Int(self.goodImageView.frame.size.width*Constant.ScreenSize.SCALE_SCREEN))" + "/h/" + "\(Int(self.goodImageView.frame.size.height*Constant.ScreenSize.SCALE_SCREEN))"
            
            self.goodImageView.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "loadingDefault"))
            }
            if item["Order_status"].int != 4 && item["Order_status"].int != 5{
                let price = String(format:"%.2f", item["Actual_price"].float ?? 0)
                
//                let text = "<t color='#ACACAC' size='13'>共<n color='black'>\(1)</n>件礼品  实付:</t><t color='black' size='13'>￥\(price)</t>"
//                do {
//                    let attribText : NSAttributedString = try NSAttributedString(data: text.dataUsingEncoding(NSUnicodeStringEncoding, allowLossyConversion: true)!, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil)
//                    self.totalLabel.attributedText = attribText
//                } catch {
//                    print(error)
//                }
                
                let attributedString1 = NSMutableAttributedString(string: String(format: "共 %d 件礼品", 1) as String)
                
                let firstAttributes = [NSForegroundColorAttributeName: UIColor.lightGray, NSFontAttributeName: UIFont.systemFont(ofSize: 13)]
                let secondAttributes = [NSForegroundColorAttributeName: UIColor.black, NSFontAttributeName: UIFont.systemFont(ofSize: 13)]
                
                attributedString1.addAttributes(firstAttributes, range: NSMakeRange(0, 1))
                attributedString1.addAttributes(secondAttributes, range: NSMakeRange(2, attributedString1.length - 6))
                attributedString1.addAttributes(firstAttributes, range: NSMakeRange(attributedString1.length - 3, 3))
                self.totalLabel.attributedText = attributedString1
                
                let attributedString2 = NSMutableAttributedString(string: String(format: "实付: ¥%@", price) as String)
                attributedString2.addAttributes(firstAttributes, range: NSMakeRange(0, 4))
                attributedString2.addAttributes(secondAttributes, range: NSMakeRange(4, attributedString2.length - 4))
                priceLabel.attributedText = attributedString2

                self.priceLabel.isHidden = false
            }
            else{
                self.totalLabel.text = item["Update_time"].string
                self.priceLabel.isHidden = true
            }
            
            self.goodTitleLabel.text = item["Order_name"].string!
            self.goodNumLabel.text = "x1"
            if let actualPrice = item["Actual_price"].float {
                self.goodSubtitleLabel.text = String(format: "￥%.2f", actualPrice)
                
            }
            
        }
        else{
        if let goodsArray = item["Goods"].array
        {
            print(goodsArray)
            let goods = goodsArray[0]
            let number = goods["Goods_num"].int ?? 0
            
            let imageScale : String = goods["Goods_image"].string! + "?imageView2/0/w/" + "\(Int(self.goodImageView.frame.size.width*Constant.ScreenSize.SCALE_SCREEN))" + "/h/" + "\(Int(self.goodImageView.frame.size.height*Constant.ScreenSize.SCALE_SCREEN))"
            
            
            self.goodImageView.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "loadingDefault"))
            let price = String(format:"%.2f", item["Actual_price"].float ?? 0)
            if item["Order_status"].int != 4 && item["Order_status"].int != 5{
                
//                let text = "<t color='#ACACAC' size='13'>共<n color='black'>\(number)</n>件礼品  实付:</t><t color='black' size='13'>￥\(price)</t>"
//                do {
//                    let attribText : NSAttributedString = try NSAttributedString(data: text.dataUsingEncoding(NSUnicodeStringEncoding, allowLossyConversion: true)!, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil)
//                    self.totalLabel.attributedText = attribText
//                } catch {
//                    print(error)
//                }
//                
                let attributedString1 = NSMutableAttributedString(string: String(format: "共 %d 件礼品", number) as String)
                
                let firstAttributes = [NSForegroundColorAttributeName: UIColor.lightGray, NSFontAttributeName: UIFont.systemFont(ofSize: 13)]
                let secondAttributes = [NSForegroundColorAttributeName: UIColor.black, NSFontAttributeName: UIFont.systemFont(ofSize: 13)]
                
                attributedString1.addAttributes(firstAttributes, range: NSMakeRange(0, 1))
                attributedString1.addAttributes(secondAttributes, range: NSMakeRange(2, attributedString1.length - 6))
                attributedString1.addAttributes(firstAttributes, range: NSMakeRange(attributedString1.length - 3, 3))
                self.totalLabel.attributedText = attributedString1
                
                let attributedString2 = NSMutableAttributedString(string: String(format: "实付: ¥%@", price) as String)
                attributedString2.addAttributes(firstAttributes, range: NSMakeRange(0, 4))
                attributedString2.addAttributes(secondAttributes, range: NSMakeRange(4, attributedString2.length - 4))
                priceLabel.attributedText = attributedString2
                

                
                self.priceLabel.isHidden = false
            }
            else{
                self.totalLabel.text = item["Update_time"].string
                self.priceLabel.isHidden = true
            }
            self.goodTitleLabel.text = goods["Goods_name"].string!
            
            if let goodsAttrArr = goods["Goods_attr"].array{
                var goodsAttrStr = ""
                for index in 0 ..< goodsAttrArr.count{
                    if let name = goodsAttrArr[index]["Attr_value_name"].string{
                        goodsAttrStr = goodsAttrStr + name + " "
                    }
                }
                self.goodSubtitleLabel.text = goodsAttrStr
            }
            
            self.goodNumLabel.text = "x" + String(number)
            
        }
        }
        
        if item["Order_status"].intValue == 1
        {
            self.leftBtn.isHidden = true
            self.rightBtn.setTitle("去支付", for: UIControlState())
            self.rightBtn.setTitleColor(UIColor.white, for: UIControlState())
            self.rightBtn.titleLabel?.font = UIFont.systemFont(ofSize: 13)
            self.rightBtn.backgroundColor = UIColor(rgba: Constant.common_red_color)
            self.rightBtn.layer.cornerRadius = 2.5
        }
        else if item["Order_status"].intValue == 2 || item["Order_status"].intValue == 3 || item["Order_status"].intValue == 5
        {
            self.leftBtn.isHidden = true
            
            self.rightBtn.setTitle("联系客服", for: UIControlState())
            self.rightBtn.titleLabel?.font = UIFont.systemFont(ofSize: 13)
            self.rightBtn.setTitleColor(UIColor(rgba: "#DE383C"), for: UIControlState())
            self.rightBtn.layer.borderColor = UIColor(rgba: "#DE383C").cgColor
            self.rightBtn.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0)
            self.rightBtn.layer.borderWidth = 0.5
            self.rightBtn.layer.cornerRadius = 2.5
        }
        else if item["Order_status"].intValue == 4
        {
            self.rightBtn.setTitle("联系客服", for: UIControlState())
            self.rightBtn.titleLabel?.font = UIFont.systemFont(ofSize: 13)
            self.rightBtn.setTitleColor(UIColor(rgba: "#DE383C"), for: UIControlState())
            self.rightBtn.layer.borderColor = UIColor(rgba: "#DE383C").cgColor
            self.rightBtn.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0)
            self.rightBtn.layer.borderWidth = 0.5
            self.rightBtn.layer.cornerRadius = 2.5
         
            
            if item["Order_type"].intValue == 2{
            self.leftBtn.isHidden = true
            }
            else{
                self.leftBtn.isHidden = false
                self.leftBtn.setTitle("申请售后", for: UIControlState())
                self.leftBtn.setTitleColor(UIColor(rgba: "#656565"), for: UIControlState())
                self.leftBtn.titleLabel?.font = UIFont.systemFont(ofSize: 13)
                self.leftBtn.layer.borderColor = UIColor(rgba: "#656565").cgColor
                self.rightBtn.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0)
                self.leftBtn.layer.borderWidth = 0.5
                self.leftBtn.layer.cornerRadius = 2.5
            }

        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func rightBtnClick(_ sender: AnyObject) {
        delegate?.rightBtnClick(self.item)
        }
    @IBAction func leftBtnClick(_ sender: AnyObject) {
        delegate?.leftBtnClick(self.item)
    }
}
