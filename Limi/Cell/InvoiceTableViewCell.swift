//
//  InvoiceTableViewCell.swift
//  Limi
//
//  Created by Richie on 16/3/17.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class InvoiceTableViewCell: UITableViewCell {
    
    fileprivate let chooseImage = UIImageView(image: UIImage(named: "noChoose"))
    fileprivate let deleteImage = UIImageView(image: UIImage(named: "basket_close"))
    fileprivate let deleteBtn = UIButton()
    fileprivate let content = UILabel()
    
    weak var delegate:InvoiceTableViewCellDelegate?
    
    fileprivate var invoiceModel:InvoiceModel!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    func deleteAction(){
    
        let _ = Delegate_Selector(delegate, #selector(InvoiceTableViewCellDelegate.invoiceDeleteHeader(_:))){Void in self.delegate!.invoiceDeleteHeader(self.invoiceModel)}
    
    }
    func tapGesAction(_ tapGes:UITapGestureRecognizer)
    {
        let _ = Delegate_Selector(delegate, #selector(InvoiceTableViewCellDelegate.invoiceSelectedHeader(_:))){Void in self.delegate!.invoiceSelectedHeader(self.invoiceModel)}

    }
    
    func reloadData(_ data: InvoiceModel,selectedID:Int)
    {
        self.invoiceModel = data
        
        self.content.text = data.invoice
        
        if data.id == selectedID
        {
            self.chooseImage.image = UIImage(named: "choosed")
        }else
        {
            self.chooseImage.image = UIImage(named: "noChoose")
        }
    }
    
    
    fileprivate func setupUI()
    {
        self.selectionStyle = UITableViewCellSelectionStyle.none
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        
        let whiteBgView = UIView()
        whiteBgView.isUserInteractionEnabled  = true
        self.addSubview(whiteBgView)
        
        let MARGIN:CGFloat = 9
        let _ = [chooseImage,content,deleteImage,deleteBtn].map{whiteBgView.addSubview($0)}
        
        content.textColor = UIColor(rgba: Constant.common_C6_color)
        content.font = UIFont.systemFont(ofSize: Constant.common_F2_font)
        
        whiteBgView.layer.borderWidth = 1
        whiteBgView.layer.borderColor = UIColor(rgba: Constant.common_separatLine_color).cgColor
        whiteBgView.backgroundColor = UIColor.white
        
        chooseImage.isUserInteractionEnabled = true
        deleteImage.isUserInteractionEnabled = true
        deleteImage.contentMode = UIViewContentMode.scaleAspectFill
        chooseImage.contentMode = UIViewContentMode.scaleAspectFill
        
        
        whiteBgView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(InvoiceTableViewCell.tapGesAction(_:))))
        deleteBtn.addTarget(self, action: #selector(InvoiceTableViewCell.deleteAction), for: UIControlEvents.touchUpInside)
        
        let img_width:CGFloat = 20
        
        whiteBgView.snp_makeConstraints { (make) -> Void in
            let _ = make.left.equalTo(self).offset(-1)
            let _ = make.right.equalTo(self).offset(1)
            let _ = make.top.equalTo(self).offset(8)
            let _ = make.bottom.equalTo(self).offset(-8)
        }
        
        chooseImage.snp_makeConstraints { (make) -> Void in
            let _ = make.left.equalTo(whiteBgView).offset(MARGIN)
            let _ = make.centerY.equalTo(whiteBgView)
            let _ = make.height.equalTo(img_width)
            let _ = make.width.equalTo(img_width)
        }
        
        content.snp_makeConstraints { (make) -> Void in
            let _ = make.left.equalTo(chooseImage.snp_right).offset(MARGIN/2)
            let _ = make.centerY.equalTo(whiteBgView)
            let _ = make.height.equalTo(img_width)
            let _ = make.right.equalTo(deleteImage.snp_left)
        }
        
        deleteImage.snp_makeConstraints { (make) -> Void in
            let _ = make.right.equalTo(whiteBgView).offset(-MARGIN)
            let _ = make.centerY.equalTo(whiteBgView)
            let _ = make.height.equalTo(13)
            let _ = make.width.equalTo(13)
        }
        
        deleteBtn.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(whiteBgView)
            let _ = make.bottom.equalTo(whiteBgView)
            let _ = make.right.equalTo(whiteBgView)
            let _ = make.width.equalTo(deleteBtn.snp_height)
        }
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}




@objc protocol InvoiceTableViewCellDelegate:LMNetWorkDelegate
{
    func invoiceDeleteHeader(_ mode:InvoiceModel)
    
    func invoiceSelectedHeader(_ mode:InvoiceModel)
}


class InvoiceModel:BaseDataMode {
 
    override func customAnalyis(_ data: [String : AnyObject]) {
        if let title = data["Invoice_head"] as? String{
            self.invoice = title
        }
        
        if let id = data["id"] as? Int{
            self.id = id
        }
    }

    var id = NULL_INT
    
    var invoice = ""
    
    //extension: 发票内容
    var invoice_type:Int = 10086
}
