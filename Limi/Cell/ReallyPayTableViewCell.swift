//
//  ReallyPayTableViewCell.swift
//  Limi
//
//  Created by guo chen on 15/11/24.
//  Copyright © 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class ReallyPayTableViewCell: UITableViewCell {

    @IBOutlet weak var payIcon: UIImageView!
    
    
    @IBOutlet weak var payTitle: UILabel!
    
    fileprivate let uppayIcon:UIImageView = {
       let image = UIImageView()
        image.contentMode = .scaleAspectFill
        return image
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.addSubview(applePayView)
        self.addSubview(uppayIcon)
        uppayIcon.isHidden = true
        applePayView.isHidden = true
        uppayIcon.snp_makeConstraints { (make) in
            let _ = make.centerY.equalTo(self)
            let _ = make.left.equalTo(110)
            let _ = make.width.equalTo(65)
            let _ = make.height.equalTo(18)
        }
        
        applePayView.snp_makeConstraints { (make) in
            let _ = make.centerY.equalTo(self)
            let _ = make.left.equalTo(self).offset(MARGIN_8 * 2)
            let _ = make.width.equalTo(applePayView.iwidth)
            let _ = make.height.equalTo(applePayView.iheight)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    fileprivate let applePayView = PayViewController.ApplePayView()
    
    
    
    func refreshData(_ data:[String:String])
    {
        applePayView.isHidden = true
        uppayIcon.isHidden = true
//      payIcon.frame = CGRectMake(15, 12, 24, 24)
        if data["icon"] == "pay_union" {
            uppayIcon.isHidden = false
            uppayIcon.image = UIImage(named:data["icon"]!)
            payTitle.text = data["title"]
        }else {
            payIcon.image = UIImage(named:data["icon"]!)
            payTitle.text = data["title"]
        }
        
    }
    
    
}
