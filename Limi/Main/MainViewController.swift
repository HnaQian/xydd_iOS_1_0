//
//  MainViewController.swift
//  Limi
//
//  Created by 程巍巍 on 5/7/15.
//  Copyright (c) 2015 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit
import CoreData


class MainViewController: UITabBarController,CheckMessageDelegate {
    let userCenter = UserCenterViewController()
    let userCenterbarItem = UITabBarItem(title: "我", image: UIImage(named: "main_tabbar_userCenter")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal), selectedImage: UIImage(named: "main_tabbar_userCenter_selected")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal))
    var isHaveMessahe : Bool = false
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.navigationBar.isTranslucent = true
    }
    
    func gotoHandle(_ notification: Notification)
    {
        let user_info:NSDictionary = (notification as NSNotification).userInfo! as NSDictionary
        if let aps = user_info.object(forKey: "aps") as? NSDictionary{
            if let alert = aps.object(forKey: "alert") as? NSDictionary{
                if let body = alert.object(forKey: "body") as? String{
                    let alertView = UIAlertController(title: "通知", message: body, preferredStyle: .alert)
                    if let payload = user_info.object(forKey: "payload") as? String{
                    alertView.addAction(UIAlertAction(title: "去看看", style: .default, handler: { (alertAction) -> Void in
                            if let url : URL = URL(string: payload){
                                let eventURL = EventDispatcher.preParserURLWithNotInMap(url)
                                let host = eventURL.host
                                var id : String = ""
                                if let param : NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: url.query) as NSDictionary?{
                                    if let app_id: AnyObject = param.object(forKey: "id") as AnyObject?{
                                        id = String(describing: app_id)
                                    }
                                }
                                
                                Statistics.count(Statistics.Other.tuisong_ios, andAttributes: ["message_id": host! + "    " + id])
                            }
                            if let NAV = self.selectedViewController as? UINavigationController{
                                EventDispatcher.dispatch(payload, onNavigationController: NAV)
                            }
                    }))
                    alertView.addAction(UIAlertAction(title: "取消", style: .cancel, handler: nil))
                    }
                    else{
                       alertView.addAction(UIAlertAction(title: "确定", style: .cancel, handler: nil))
                    }
                    self.present(alertView, animated: true, completion: nil)
                }
            }
        }

    }
    
    func touchHandle(_ notification: Notification){
        let user_info:NSDictionary = (notification as NSNotification).userInfo! as NSDictionary
        if  let touchType = user_info.object(forKey: "touchType") as? String{
            if let NAV = self.selectedViewController as? UINavigationController{
                if touchType == "Search"{
                    EventDispatcher.dispatch("local://search?app=1", onNavigationController: NAV)
                }
                else if touchType == "AddReminder"{
                    if AccountManager.shared.isLogin(){
                        EventDispatcher.dispatch("local://relation_add?app=1", onNavigationController: NAV)
                        
                    }
                    else{
                        let alertView = UIAlertController(title: "提示", message: "需要登录账号后才可以添加送礼提醒", preferredStyle: .alert)
                        alertView.addAction(UIAlertAction(title: "确定", style: .cancel, handler: nil))
                        self.present(alertView, animated: true, completion: nil)
                    }
                }
                else if touchType == "FlashSale"{
                    EventDispatcher.dispatch("http://wechat.giftyou.me/goods/flashSale", onNavigationController: NAV)
                }
                else if touchType == "NewTactic"{
                    EventDispatcher.dispatch("local://wechat.giftyou.me/article_list?app=1&sort=0", onNavigationController: NAV)
                    
                }
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.navigationController?.interactivePopGestureRecognizer!.isEnabled = false
        self.view.backgroundColor = UIColor.white
        self.navigationItem.hidesBackButton = true
        
        // home
        let homeVC = HomeViewControllerV1()
        let nc1 : UINavigationController = UINavigationController(rootViewController: homeVC)
        homeVC.tabBarItem = self.tabBarItem(PageName: "home")
        
        //service
        let service = ServiceListViewController()
        let nc2 : UINavigationController = UINavigationController(rootViewController: service)
        service.tabBarItem = self.tabBarItem(PageName: "service")
        
        //gift
        let giftVC = GiftChoiceRootViewController()
        let nc3 : UINavigationController = UINavigationController(rootViewController: giftVC)
        giftVC.tabBarItem = self.tabBarItem(PageName: "gift")
        
        
        // userCenter
        userCenter.delegate = self
        let nc4 : UINavigationController = UINavigationController(rootViewController: userCenter)
        userCenterbarItem.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor(rgba: "#ff3a48"),  NSFontAttributeName: Constant.CustomFont.Default(size: 11)], for: UIControlState.selected)
        userCenterbarItem.setTitleTextAttributes([NSFontAttributeName: Constant.CustomFont.Default(size: 11)], for: UIControlState())
        userCenter.tabBarItem = userCenterbarItem
        
        self.viewControllers = [nc1, nc2, nc3, nc4]
        
        self.queryUnreadMsgCount()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(MainViewController.gotoHandle(_:)),
            name: NSNotification.Name(rawValue: Constant.notifationMessageReceiveHandle),
            object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(MainViewController.touchHandle(_:)),
            name: NSNotification.Name(rawValue: Constant.notifationTouchReceiveHandle),
            object: nil)
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: Constant.show_rootController), object: nil, queue: OperationQueue.main) { (notification) -> Void in
           let index = (notification as NSNotification).userInfo!["index"] as! Int
            self.selectedIndex = index
            
        }
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: Constant.usercenter_changed), object: nil, queue: OperationQueue.main) { (Notification) -> Void in
            self.isHaveMessahe = true
        }
        
        ShoppingBasketDataManager.shareManager().reloadData()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    func reloadMainTabMessage(_ isHaveMessage : Bool){
        self.isHaveMessahe = isHaveMessage

    }
    
    func queryUnreadMsgCount() {
        if UserInfoManager.didLogin == true{
            let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.message_unread_count, isNeedUserTokrn: true, successHandler: {data, status, msg in
                if status == 200 {
                    if JSON(data as AnyObject)["data"]["num"].intValue > 0{
                         self.isHaveMessahe = true
                    }
                    else{
                         self.isHaveMessahe = false
                    }
                }
                else{
                    self.isHaveMessahe = false
                }
            })
            
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension MainViewController {
    
    fileprivate func tabBarItem(PageName name: String)->UITabBarItem
    {
        let titleMap = ["home": "首页", "service": "服务", "gift": "选礼","userCenter": "我"]
        let title = titleMap[name]
        let image = UIImage(named: "main_tabbar_\(name)")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        let selectedImage = UIImage(named: "main_tabbar_\(name)_selected")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        
        let barItem = UITabBarItem(title: title, image: image, selectedImage: selectedImage)
        barItem.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor(rgba: "#ff3a48"),  NSFontAttributeName: Constant.CustomFont.Default(size: 11)], for: UIControlState.selected)
        barItem.setTitleTextAttributes([NSFontAttributeName: Constant.CustomFont.Default(size: 11)], for: UIControlState())
        
        return barItem
    }
}

extension MainViewController: UITabBarControllerDelegate {
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController){
        let tabTitleAry = ["首页","服务","选礼","我"]
        if tabBarController.selectedIndex >= 0 && tabBarController.selectedIndex < tabTitleAry.count{
            Statistics.count(Statistics.Dcok.dock_click, andAttributes: ["tab":tabTitleAry[tabBarController.selectedIndex]])
        }
        
        if tabBarController.selectedIndex != 2 {
            UserDefaults.standard.set(3, forKey: "come_from")
            UserDefaults.standard.synchronize()
        }
       
        if tabBarController.selectedIndex != 3{
            if self.isHaveMessahe{
                self.userCenterbarItem.image = UIImage(named: "main_tabbar_userCenterWithMessage")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
            }
            else{
                self.userCenterbarItem.image = UIImage(named: "main_tabbar_userCenter")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
            }
        }
    }
    
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        return true
    }
    

    
}
