//
//  HomeGiftRemindBannerUnitView.swift
//  Limi
//
//  Created by guo chen on 15/12/22.
//  Copyright © 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//滚动视图

import UIKit

private weak var limitHomeGiftRemindBannerUnitView:HomeGiftRemindBannerUnitView!

class HomeGiftRemindBannerUnitView: HomeBaseUnitView,BBannerViewDelegate,BBannerViewDataSource {
    
    /*
     // Only override drawRect: if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func drawRect(rect: CGRect) {
     // Drawing code
     }
     */
    
    fileprivate var adDataSource = [ADMode]()
    
    fileprivate var giftRemindData = [[String:AnyObject]]()
    
    fileprivate var giftRemindViews = [GiftRemainViewV1]()
    
    fileprivate var adViews = [ADImageView]()
    
    
    fileprivate let bannerView = BBannerView(frame: CGRect(x: 0,y: 0,width: Constant.ScreenSize.SCREEN_WIDTH,height: 240 * Constant.ScreenSize.SCREEN_WIDTH_SCALE))
    
    override func setupUI()
    {
        limitHomeGiftRemindBannerUnitView = self
        
        self.addSubview(bannerView)
        
        let scal:CGFloat =  Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        
        self.selfHeight = scal * 360
        
        self.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: self.selfHeight)
        
        if let height = JSON(self.unitData as AnyObject)["attributes"]["height"].string,
            let width = JSON(self.unitData as AnyObject)["attributes"]["width"].string
        {
            if NSString(string: width).intValue != 0 && NSString(string: height).intValue != 0
            {
                let scal:CGFloat =  Constant.ScreenSize.SCREEN_WIDTH/CGFloat(NSString(string: width).intValue)
                
                self.selfHeight = scal * CGFloat(NSString(string: height).intValue)
                
                self.iheight = self.selfHeight
            }
        }
        
        
        bannerView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self)
            let _ = make.left.equalTo(self)
            let _ = make.size.equalTo(self)
        }
        
        bannerView.delegate = self
        bannerView.dataSource = self
        
        bannerView.currentPageIndicatorTintColor = UIColor(rgba: "#b5b5b5")
        bannerView.pageIndicatorTintColor = UIColor(rgba: "#eeeeee")
        
        self.analysesData()
    }
    
    
    
    //返回View
    func viewForItem(_ bannerView: BBannerView, index: Int) -> UIView
    {
        if index < self.adDataSource.count
        {
            return ADImageView(frame: CGRect(x: 0, y: 0, width: self.iwidth, height: self.iheight), data: self.adDataSource[index])
        }else
        {
            let mindex = index - self.adDataSource.count
            
            let giftRemindView = GiftRemainViewV1.newRemindView(JSON(self.giftRemindData[mindex] as AnyObject),size:CGSize(width: self.iwidth, height: self.iheight))
            
            return giftRemindView
        }
    }
    
    
    //选择
    func numberOfItems() -> Int
    {
        return self.giftRemindData.count + self.adDataSource.count
    }
    
    
    
    func didSelectItem(_ index: Int)
    {
        if index < self.adDataSource.count
        {
            let imageView = self.adViews[index]
            
            if imageView.data.href.characters.count > 0
            {
                print(imageView.data.href)
                
                if Utils.containsString("article", inString: imageView.data.href){
                    Statistics.count(Statistics.Home.home_banner_article_click, andAttributes: ["href": imageView.data.href])
                    
                }else if Utils.containsString("package_item", inString: imageView.data.href){
                    Statistics.count(Statistics.Home.home_banner_giftbox_click, andAttributes: ["href": imageView.data.href])
                }else {
                    Statistics.count(Statistics.Home.home_banner_goods_click, andAttributes: ["href": imageView.data.href])
                }
                
                
                if let URL = URL(string: imageView.data.href)
                {
                    self.delegate?.homeUnitViewTouchOnClick({(homeVC) in
                        EventDispatcher.dispatch(URL: URL, onNavigationController : homeVC.navigationController)
                    })
                }
            }
        }else
        {
            let mindex = index - self.adDataSource.count
            
            let imageView = self.giftRemindViews[mindex]
            
            if let url : String = imageView.data!["Url"].string
            {
                if url != ""{

                    let webVC = WebViewController(URL: URL(string: url)!)
                    
                    print(imageView.data.dictionaryObject)
                    
                    if let Uid : Int = imageView.data!["Uid"].int{
                        if Uid != 0{
                        
                            
                            if let scene : Int = imageView.data!["Scene"].int{
                                if scene == 1{
                                    webVC.isHaveBirthdayWiki = true
                                }
                                else{
                                    webVC.isHaveBirthdayWiki = false
                                }
                                
                                if let id : Int = imageView.data!["Id"].int{
                                    webVC.RemindDetailId = id
                                }
                                
                                webVC.type = Constant.WebViewType.remindDetailType.rawValue
                            }
                        }
                    }
                    
                    if let scene : Int = imageView.data!["Scene"].int,
                        let id : Int = imageView.data!["Id"].int{
                        if scene != 1{
                            Statistics.count(Statistics.Home.home_banner_remind_click, andAttributes: ["remindId":"\(id)"])
                        }
                        Statistics.count(Statistics.Home.home_notice_holiday_click, andAttributes: ["remindId":"\(id)"])
                    }
                    
                    webVC.hidesBottomBarWhenPushed = true
                    webVC.shareType = Constant.ShareContentType.imageType.rawValue
                    self.delegate?.homeUnitViewTouchOnClick({(homeVC) in
                        homeVC.navigationController?.pushViewController(webVC, animated: true)
                    })
                    
                }
            }
            
        }
    }
    
    var shouldAddGiftRemind = false
    
    fileprivate func analysesData()
    {
        print(self.unitData)
        
        if  let ary = JSON(self.unitData as AnyObject)["children"].array
        {
            if ary.count > 0
            {
                for dict in ary
                {
                    if let name = dict["name"].string
                    {
                        //广告
                        if name == "img"
                        {
                            if UserInfoManager.didLogin {
                                if let temp = dict["attributes"]["switch_btn"].string,temp == "0" {
                                    continue
                                }
                            }
                            self.adDataSource.append(ADMode(dataDic: dict))
                            //拥有送礼提醒的标记
                        }else if name == "gift_remind"
                        {
                            shouldAddGiftRemind = true
                        }
                    }
                }
            }
        }
        
        self.refreshBanaer()
        
        if shouldAddGiftRemind == true
        {
            self.loadRelationList()
        }
    }
    
    
    //MARK:获取送礼提醒数据
    func loadRelationList()
    {
        var url:String!
        
        if UserInfoManager.didLogin == true
        {
            url = Constant.JTAPI.user_relation_index
        }else
        {
            url = Constant.JTAPI.user_relation_index_v2
        }
        let _ = LMRequestManager.requestByDataAnalyses(context:self,URLString: url,isNeedUserTokrn: true,isShowErrorStatuMsg:true, successHandler: {data, status, msg in
            if status == 200
            {
               // print(data)
                
                self.giftRemindData = [[String:AnyObject]]()
                
                if let list = JSON(data as AnyObject)["data"]["list"].array
                {
                    if list.count > 0{
                        
                        //                        self.giftRemindData = self.sortGiftRmindList(JSON(data)["data"]["list"].arrayObject as! [[String:AnyObject]])
                        self.giftRemindData = JSON(data as AnyObject)["data"]["list"].arrayObject as! [[String:AnyObject]]
                        
                        self.refreshBanaer()
                    }
                }
            }
            
        })
    }
    
    
    fileprivate func sortGiftRmindList(_ giftRemindList:[[String:AnyObject]])-> [[String:AnyObject]]
    {
        var newList = [[String:AnyObject]]()
        
        if giftRemindList.count > 1
        {
            newList = giftRemindList.sorted{JSON($0["Between_day"]! as AnyObject).int! < JSON($1["Between_day"]! as AnyObject).int!}
        }else
        {
            return giftRemindList
        }
        
        return newList
    }
    
    
    
    //MARK:
    func refreshBanaer()
    {
        //创建提醒
        if giftRemindData.count > 0
        {
            self.giftRemindViews = [GiftRemainViewV1]()
            
            for data in giftRemindData
            {
                self.giftRemindViews.append(GiftRemainViewV1.newRemindView(JSON(data as AnyObject),size:CGSize(width: self.iwidth, height: self.iheight)))
            }
        }
        
        //创建广告
        if adViews.count == 0
        {
            if adDataSource.count > 0
            {
                for data in adDataSource
                {
                    self.adViews.append(ADImageView(frame: CGRect.zero, data: data))
                }
            }
        }
        
        self.bannerView.reloadData()
        
        self.bannerView.startAutoScroll(4)
    }
}

extension HomeGiftRemindBannerUnitView{
    
    class GiftRemainViewV1:UIView{
        
        var data:JSON!{return _data}
        fileprivate var _data:JSON!
        
        fileprivate let bgImageView = UIImageView()
        
        fileprivate let contentView = UIView()//内容视图
        fileprivate let remindIcon = UIImageView()//送礼提醒名称
        fileprivate let remindIconLabel = CustomLabel(font: 20,textColorHex: "#FFFFFF")//送礼提醒名称
        
        //姓名label
        fileprivate let remindTitleLbl:UILabel = {
           let lbl = UILabel()
            lbl.font = Constant.Theme.Font_17
            lbl.textColor = UIColor(rgba: Constant.common_C2_color)
            lbl.textAlignment = .center
            return lbl
        }()
        
        //场景label
        fileprivate let remindSenceLbl:UILabel = {
            let lbl = UILabel()
            lbl.font = Constant.Theme.Font_17
            lbl.textColor = UIColor(rgba: Constant.common_C2_color)
            lbl.textAlignment = .center
            return lbl
        }()

        
        //倒计时时间
        fileprivate let restDaysLbl :UILabel = {
            let lbl = UILabel()
            lbl.font = Constant.Theme.Font_17
            lbl.textColor = UIColor(rgba: Constant.common_C2_color)
            lbl.textAlignment = .center
            return lbl
        }()

        
        fileprivate let detailIcon = UIImageView()
        
        class func newRemindView(_ data:JSON,size:CGSize) ->GiftRemainViewV1
        {
            let newRemind = GiftRemainViewV1()
            
            newRemind.iheight = size.height
            newRemind.iwidth = size.width
            
            newRemind._data = data
            
            newRemind.setupUI()
            
            return newRemind
        }
        
        
        fileprivate func setupUI()
        {
            self.addSubview(bgImageView)
            bgImageView.addSubview(contentView)
            bgImageView.clipsToBounds = true
            
            let _ = [bgImageView,remindIcon].map{$0.contentMode = UIViewContentMode.scaleAspectFill}
            detailIcon.contentMode = UIViewContentMode.scaleToFill
            
            let _ = [remindIcon,remindIconLabel,remindTitleLbl,remindSenceLbl,restDaysLbl,detailIcon].map{
                contentView.addSubview($0)}
            
            contentView.layer.cornerRadius = 3
            contentView.clipsToBounds = true
            contentView.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.8)
            
            remindIconLabel.layer.cornerRadius = 19
            remindIconLabel.clipsToBounds = true
            
            remindIcon.layer.cornerRadius = 19
            remindIcon.clipsToBounds = true
            
            restDaysLbl.textColor = UIColor(rgba: Constant.common_C1_color)
            
            let contentView_height:CGFloat  = 54
            
            bgImageView.frame = self.bounds
            
            contentView.frame = CGRect(x: MARGIN_10, y: self.iheight - MARGIN_20 - MARGIN_4 - contentView_height, width: Constant.ScreenSize.SCREEN_WIDTH - MARGIN_10 * 2, height: contentView_height)
            
            bgImageView.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(self)
                let _ = make.left.equalTo(self)
                let _ = make.right.equalTo(self)
                let _ = make.bottom.equalTo(self)
            }
            
            contentView.snp_makeConstraints { (make) -> Void in
                let _ = make.bottom.equalTo(bgImageView).offset(-MARGIN_20 )
                let _ = make.left.equalTo(bgImageView).offset(MARGIN_10)
                let _ = make.right.equalTo(bgImageView).offset(-MARGIN_10)
                let _ = make.height.equalTo(contentView_height)
            }
            
            
            remindIcon.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(contentView).offset(MARGIN_8)
                let _ = make.top.equalTo(contentView).offset(MARGIN_8)
                let _ = make.bottom.equalTo(contentView).offset(-MARGIN_8)
                let _ = make.width.equalTo(remindIcon.snp_height)
            }
            
            remindIconLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.center.equalTo(remindIcon)
                let _ = make.size.equalTo(remindIcon)
            }
            
            remindTitleLbl.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(remindIcon.snp_right).offset(MARGIN_8)
                let _ = make.centerY.equalTo(contentView)
            }
            
            remindSenceLbl.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(remindTitleLbl.snp_right)
                let _ = make.centerY.equalTo(remindTitleLbl)
            }
            
            detailIcon.snp_makeConstraints { (make) -> Void in
                let _ = make.right.equalTo(contentView).offset(-MARGIN_8)
                let _ = make.centerY.equalTo(contentView)
                let _ = make.height.equalTo(14)
                let _ = make.width.equalTo(7.33)
            }
            
            restDaysLbl.snp_makeConstraints { (make) -> Void in
                let _ = make.right.equalTo(detailIcon.snp_left).offset(-MARGIN_8)
                let _ = make.centerY.equalTo(contentView).offset(-1)
            }
            
            self.setProperty()
        }
        
        
        fileprivate func setProperty(){
            
            if let day = data["Between_day"].int
            {
                if day == 0{
                    restDaysLbl.text = "今天"
                }else if day == 1{
                    restDaysLbl.text = "明天"
                }else{
                    
                   let font = Constant.Theme.Font_17
                    
                    
                    let attributedString1 = NSMutableAttributedString(string: String(format: "%d天后",day) as String)
                    
                    var firstAttributes:[String:AnyObject]!
                    
                    var secondAttributes:[String:AnyObject]!
                    
                    firstAttributes = [NSForegroundColorAttributeName: UIColor.black, NSFontAttributeName: font]
                    
                    secondAttributes = [NSForegroundColorAttributeName: UIColor(rgba: Constant.common_red_color), NSFontAttributeName: font]
                    
                    attributedString1.addAttributes(secondAttributes, range: NSMakeRange(0, attributedString1.length))
                    attributedString1.addAttributes(firstAttributes, range: NSMakeRange(attributedString1.length - 2, 2))
                    
                    restDaysLbl.attributedText = attributedString1
                }
            }
            
            detailIcon.image = UIImage(named: "detail_icon")
            
            //姓名
            if let name = data["Name"].string{
                self.remindTitleLbl.text = name
            }
            
            if let sence =  data["Scene_name"].string{
                self.remindSenceLbl.text = sence
            }
            
            bgImageView.image = UIImage(named: "new_default_banner")
            
            //Index_img
            if let bgimage = data["Index_img"].string
            {
                if bgimage.characters.count > 0
                {
                    let tempWidth = "\(Int(bgImageView.iwidth * Constant.ScreenSizeV1.SCALE_SCREEN))"
                    let tempHeight = "\(Int(bgImageView.iheight * Constant.ScreenSizeV1.SCALE_SCREEN))"
                    let tempSuffix = "?imageView2/0/w/" + tempWidth + "/h/" + tempHeight
                    let imageScale : String = String(bgimage + tempSuffix)
                    bgImageView.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "new_default_banner"))
                }
            }
            
            let sence = data["Scene"].int
            
            if sence == 1{
                
                if let imageName = data["Avatar"].string
                {
                    if imageName.characters.count > 0
                    {
                        let imageScale : String = imageName + "?imageView2/0/w/" + "\(Int(94*Constant.ScreenSize.SCALE_SCREEN))" + "/h/" + "\(Int(94*Constant.ScreenSize.SCALE_SCREEN))"
                        
                        remindIcon.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "new_default_banner"))
                    }
                    else
                    {
                        let name = Utils.subLastCharacter(data["Name"].string)

                        if name != NULL_STRING{
                            
                            remindIconLabel.text = name
                            
                            let index = (data["Id"].int)!%10
                            
                            let hex = Constant.randomColor[index]
                            
                            remindIconLabel.backgroundColor = UIColor(rgba:hex)
                            
                        }else{
                            remindIcon.image = UIImage(named: "gift_reminder_situation_birthday_default")
                        }
                    }
                }
                else
                {
                    let name = Utils.subLastCharacter(data["Name"].string)
                    
                    if name != "null"{
                        
                        remindIconLabel.text = name
                        
                        let index = (data["Id"].int)!%10
                        
                        let hex = Constant.randomColor[index]
                        
                        remindIconLabel.backgroundColor = UIColor(rgba:hex)
                        
                    }else{
                        remindIcon.image = UIImage(named: "gift_reminder_situation_birthday_default")
                    }
                }
            }else
            {
                if let imageName = data["Avatar"].string
                {
                    let imageScale : String = imageName + "?imageView2/0/w/" + "\(Int(94*Constant.ScreenSize.SCALE_SCREEN))" + "/h/" + "\(Int(94*Constant.ScreenSize.SCALE_SCREEN))"
                    
                    remindIcon.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "new_default_banner"))
                }
                else
                {
                    remindIcon.image =  UIImage(named: "gift_reminder_situation_birthday_default")
                }
            }
        }
    }
    
    
}



class ADImageView: UIImageView {
    
    var data:ADMode!
    
    init(frame: CGRect,data:ADMode)
    {
        super.init(frame: frame)
        
        self.isUserInteractionEnabled = true
        
        self.data = data
        let imageScale : String = data.src + "?imageView2/0/w/" + "\(Int(self.frame.size.width * Constant.ScreenSize.SCALE_SCREEN))" + "/h/" + "\(Int(self.frame.size.height * Constant.ScreenSize.SCALE_SCREEN))"
        self.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "new_default_banner"))
    }
    
    init(frame: CGRect,imgSrc:String)
    {
        super.init(frame: frame)
        self.isUserInteractionEnabled = true
         let imageScale : String = imgSrc + "?imageView2/0/w/" + "\(Int(self.frame.size.width * Constant.ScreenSize.SCALE_SCREEN))" + "/h/" + "\(Int(self.frame.size.height * Constant.ScreenSize.SCALE_SCREEN))"
        self.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "new_default_banner"))
    }
    
    
    func setupUI()
    {
        let imageScale : String = data.src + "?imageView2/0/w/" + "\(Int(self.frame.size.width * Constant.ScreenSize.SCALE_SCREEN))" + "/h/" + "\(Int(self.frame.size.height * Constant.ScreenSize.SCALE_SCREEN))"
        self.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "new_default_banner"))
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

