//
//  HomeActivityUnitView.swift
//  Limi
//
//  Created by guo chen on 15/12/22.
//  Copyright © 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//首页活动视图的基类 : 心意充值、拜年红包、心意宝箱

/*
 children =             (
 {
 attributes =                     {
 href = "http://wechat.giftyou.me/fees?app=1";
 src = "http://up.xydd.co/14581866291405.png";
 title = "\U5fc3\U610f\U5145\U503c";
 };
 },
 */

import UIKit

class HomeActivityUnitView: HomeBaseUnitView {
    
    fileprivate var dataSource = [JSON]()
    
    // 加载内容
    func loadMatter(){
        
        if let children = JSON(self.unitData as AnyObject)["children"].array{
            
            if children.count == 0{return}
            dataSource = children
            var widthGap:CGFloat = 0
            if let tempBoundsAttribute = JSON(self.unitData as AnyObject)["attributes"].dictionaryObject {
                if let tempHeight = tempBoundsAttribute["height"] as? String {
                    
                    self.selfHeight = CGFloat(NSString(string:tempHeight).integerValue) * serviceWidthScal
                    self.iheight = self.selfHeight
                    
                    self.snp_makeConstraints { (make) -> Void in
                        let _ = make.height.equalTo(self.selfHeight)
                    }
                    
                    self.iwidth = Constant.ScreenSize.SCREEN_WIDTH
                }
                
                if let tempWidth = tempBoundsAttribute["width"]  as? String {
                    if CGFloat(NSString(string:tempWidth).integerValue) < serviceWidth {
                        widthGap = (serviceWidth - CGFloat(NSString(string:tempWidth).integerValue)) * serviceWidthScal / 2
                    }
                }
            }
            
            var leftDistance:CGFloat = widthGap
            for index in 0 ..< children.count{
                
                let attributesMode = HomeUnitActivityModel(children[index]["attributes"].dictionaryObject)
                var frame = CGRect(x: CGFloat(attributesMode.left) * serviceWidthScal,  y: CGFloat(attributesMode.top) * serviceWidthScal, width: CGFloat(attributesMode.width) * serviceWidthScal, height: CGFloat(attributesMode.height) * serviceWidthScal)
                
                frame.origin.x += leftDistance
                let imageView = self.getImageView(children[index]["attributes"]["src"].string, index: index, frame: frame)
               leftDistance += frame.size.width + CGFloat(attributesMode.right + attributesMode.left) * serviceWidthScal

                self.addSubview(imageView)
            }
        }
    }
    
    
    fileprivate func getImageView(_ imgSrc:String?,index:Int,frame:CGRect)->UIImageView{
        
        let imageView = UIImageView()
        imageView.tag = 100 + index
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = true
        imageView.contentMode = UIViewContentMode.scaleAspectFill
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(HomeADbannerUnitView.tagGestAction(_:))))
        
        imageView.frame = frame
        imageView.image = UIImage(named: "loadingDefault")
        
        if let src = imgSrc{
            if let url = URL(string: Utils.scaleImage(src, width: imageView.iwidth * 3, height: imageView.iheight * 3)){
                imageView.af_setImageWithURL(url, placeholderImage: UIImage(named: "loadingDefault"))
            }
        }
        
        return imageView
    }
    
    
    @objc fileprivate func tagGestAction(_ tapGest:UITapGestureRecognizer)
    {
        if let view  = tapGest.view
        {
            if let href = dataSource[view.tag - 100]["attributes"]["href"].string{
                
                switch view.tag {
                case 100:
                    //ICON 1
                    Statistics.count(Statistics.Home.home_icon1_click, andAttributes: ["href": href])
                case 101:
                    //ICON 2
                    Statistics.count(Statistics.Home.home_icon2_click, andAttributes: ["href": href])
                case 102:
                    //ICON 3
                    Statistics.count(Statistics.Home.home_icon3_click, andAttributes: ["href": href])
                case 103:
                    //ICON 4
                    Statistics.count(Statistics.Home.home_icon4_click, andAttributes: ["href": href])
                    
                default:
                    break
                }
                
//                Statistics.count(Statistics.Home.home_ad_add_click, andAttributes: ["href": href])
                
                self.delegate?.homeUnitViewTouchOnClick({ (homeVC) in
                    EventDispatcher.dispatch(href, onNavigationController: homeVC.navigationController)
                    })
            }
        }
    }
    
    
    
    override func setupUI()
    {
        self.backgroundColor = UIColor(rgba: Constant.common_C12_color)
        
        self.loadMatter()
    }
    
}
