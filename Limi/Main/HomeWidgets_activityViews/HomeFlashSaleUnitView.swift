//
//  HomeFlashSaleUnitView.swift
//  Limi
//
//  Created by guo chen on 15/12/22.
//  Copyright © 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//推荐攻略模块

import UIKit

/*  左边礼盒
 
 attributes = {
 height = 260;
 "left_size" = 1;
 "right_size" = 2;
 width = 960;
 };
 
 "left_children" =             (
 {
 href = "http://wechat.giftyou.me/article_item?id=745&app=1";
 src = "http://up.xydd.co/14647960761431.jpg";
 }
 );
 
 "right_children" =             (
 {
 href = "http://wechat.giftyou.me/top?app=1";
 src = "http://up.xydd.co/14645669134910.jpg";
 },
 {
 href = "http://wechat.giftyou.me/article_item?id=825&app=1";
 src = "http://up.xydd.co/14648316995844.jpg";
 }
 );
 
 name = block;
 
*/

class HomeFlashSaleUnitView: HomeBaseUnitView {
    
    fileprivate var dataSource = [HomeRecommandTracticModel]()
    
    //左边的列表
    fileprivate var leftChildren = [HomeRecommandTracticModel]()
    //右边的列表
    fileprivate var rightChildren = [HomeRecommandTracticModel]()
    
    override func setupUI() {

        self.iheight = Constant.ScreenSize.SCREEN_WIDTH_SCALE * 194
        self.iwidth = Constant.ScreenSize.SCREEN_WIDTH
        
        if let width = JSON(self.unitData as AnyObject)["attributes"]["width"].string,
            let height = JSON(self.unitData as AnyObject)["attributes"]["height"].string
        {
            if NSString(string: width).intValue != 0 && NSString(string: height).intValue != 0
            {
                let tempHeight:CGFloat = CGFloat(NSString(string: height).intValue)
                self.iheight = tempHeight * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
            }
        }
        
        self.selfHeight = self.iheight

        
        if let ary = self.unitData["left_children"] as? [AnyObject]{
            for dic in ary{
                leftChildren.append(HomeRecommandTracticModel(dic as! [String:AnyObject]))
            }
        }

        if let ary = self.unitData["right_children"] as? [AnyObject]{
            for dic in ary{
                rightChildren.append(HomeRecommandTracticModel(dic as! [String:AnyObject]))
            }
        }
        
        
        //left
        var leftHeight:CGFloat = 0
        let scale = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        for index  in 0..<leftChildren.count{
            let model  =  leftChildren[index]
            let imageFrame = CGRect(x: 0, y: leftHeight, width: CGFloat(leftChildren[index].width) * scale, height: CGFloat(leftChildren[index].height) * scale)
            let imageView = HomeFlashSaleUnitView_Image(dataModel: model, frame: imageFrame)
            imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(HomeFlashSaleUnitView.imageTaped(_:))))
            leftHeight += imageFrame.size.height
            
            self.addSubview(imageView)
        }
        
        
        //right
        var rightHeight:CGFloat = 0
        for index  in 0..<rightChildren.count{
            let model  =  rightChildren[index]
            
            let imageFrame =  CGRect(x: Constant.ScreenSizeV2.SCREEN_WIDTH / 2, y: rightHeight, width: CGFloat(rightChildren[index].width ) * scale, height: CGFloat(rightChildren[index].height) * scale)
            let imageView = HomeFlashSaleUnitView_Image(dataModel: model, frame:imageFrame)
            
            imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(HomeFlashSaleUnitView.imageTaped(_:))))
            rightHeight += imageFrame.size.height
            
            self.addSubview(imageView)
        }
    }
    
    
    @objc fileprivate func imageTaped(_ sender: UITapGestureRecognizer){

        if let img = sender.view as? HomeFlashSaleUnitView_Image{
            
            Statistics.count(Statistics.Home.home_area_click, andAttributes: ["href":img.dataModel.href])
            
            self.delegate?.homeUnitViewTouchOnClick({(homeVC) in
                EventDispatcher.dispatch(img.dataModel.href, onNavigationController: homeVC.navigationController)
            })
        }
    }
}


extension HomeFlashSaleUnitView{

    fileprivate class HomeFlashSaleUnitView_Image:UIImageView{
        
        var dataModel:HomeRecommandTracticModel!
        
        init(dataModel:HomeRecommandTracticModel,frame: CGRect) {
            super.init(frame: frame)
            
            self.dataModel = dataModel
            
            self.setupUI()
        }
        
        
        fileprivate func setupUI(){
            
            self.clipsToBounds = true
            /*
             href = "http://wechat.giftyou.me/article_item?id=745&app=1";
             src = "http://up.xinyidiandian.com/14620715026574.jpg";
             text = "\U72ec\U5bb6\U793c\U76d211";
             "text_color" = "#7AAE79";
             title = "\U5927\U793c\U5305";
             "title_color" = "#12171F";
             */
            
            self.isUserInteractionEnabled = true
            
            self.contentMode = UIViewContentMode.scaleAspectFill
            
            if let url = URL(string: Utils.scaleImage(dataModel.src, width: self.iwidth * 3, height: self.iheight * 3)){
                self.af_setImageWithURL(url, placeholderImage: UIImage(named: "loadingDefault"))
            }

        }
        
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    
    
    
}
