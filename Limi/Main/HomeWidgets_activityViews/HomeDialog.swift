//
//  HomeDialog.swift
//  Limi
//
//  Created by Richie on 16/5/3.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//首页的弹窗

import UIKit

class HomeDialog: UIView {
    
    /*
     // Only override drawRect: if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func drawRect(rect: CGRect) {
     // Drawing code
     }
     */
    
    fileprivate let adImageView = UIImageView()
    
    fileprivate let closeBtn = UIButton()
    
    weak var delegate:HomeDialogDelegate?

    var href = ""

    fileprivate var _adImage:UIImage?
    
    var adImage:UIImage?{return _adImage}
    
    
    
    init(){
        super.init(frame: CGRect.zero)
        self.setupUI()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc fileprivate func closeBtnOnClick(){
        delegate?.homeDialogOnClick(1)
        
        dismiss()
    }
    
    
    @objc fileprivate func imageOnClick(){
        
        delegate?.homeDialogOnClick(0)
        
        dismiss()
    }
    
    
    var imageUrl:String{return _imageUrl}
    
    fileprivate var _imageUrl = ""
    
    func loadAdImage(_ imageUrl:String,loadSuccess:@escaping (()->Void)){
        _imageUrl = imageUrl
        _adImage = nil
        
        OriginalDownLoad().downloadWithUrlString(context: self, urlString: imageUrl) {[weak self] (data) in
            if let mdata = data{
                self?._adImage = UIImage(data: mdata as Data)
                self?.adImageView.image = self?.adImage
                loadSuccess()
            }
        }
    }
    
    
    //显示用户领取红包
    func show(){

        //显示用去去更新
        if AppUpdateView.checkUpdateShow() {return}
        
        if self.superview == nil{
            if let window = UIApplication.shared.keyWindow{
                self.frame =  window.bounds
                window.addSubview(self)
            }
        }
        
        
        UIView.animate(withDuration: 0.25, animations: {
            self.adImageView.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.alpha = 1
        }, completion: { (done) in
            
        }) 
    }
    
    
    func dismiss(){
        
        UIView.animate(withDuration: 0.25, animations: {
            self.alpha = 0
        }, completion: { (done) in
            self.adImageView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        }) 
    }
    
    
    fileprivate func setupUI(){
        self.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        self.alpha = 0
        
        self.addSubview(adImageView)
        //        adImageView.image = UIImage(named: "dialogAdPicture")
        adImageView.isUserInteractionEnabled = true
        
        adImageView.contentMode = UIViewContentMode.scaleAspectFill
        adImageView.clipsToBounds = true
        
        adImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(HomeDialog.imageOnClick)))
        
        self.addSubview(closeBtn)
        closeBtn.setImage(UIImage(named: "dialogClose_icon"), for: UIControlState())
        closeBtn.setImage(UIImage(named: "dialogClose_icon"), for: UIControlState.highlighted)
        
        //Highlighted
        closeBtn.addTarget(self, action: #selector(HomeDialog.closeBtnOnClick), for: UIControlEvents.touchUpInside)
        let width:CGFloat = Constant.ScreenSize.SCREEN_WIDTH * 0.85
        let height:CGFloat = width * 450/350
        let top:CGFloat = (Constant.ScreenSize.SCREEN_HEIGHT - (42 * Constant.ScreenSize.SCREEN_HEIGHT_SCALE + 34 + height))/2
        
        adImageView.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(top)
            let _ = make.left.equalTo((Constant.ScreenSize.SCREEN_WIDTH - width)/2)
            let _ = make.width.equalTo(width)
            let _ = make.height.equalTo(height)
        }
        
        closeBtn.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(self.adImageView.snp_bottom).offset(34)
            let _ = make.centerX.equalTo(self.adImageView)
            let _ = make.size.equalTo(42 * Constant.ScreenSize.SCREEN_HEIGHT_SCALE)
        }
        
        self.adImageView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
    }
    
    
    
}



protocol HomeDialogDelegate:NSObjectProtocol {
    ///type: 0:立马领取 1:关闭
    func homeDialogOnClick(_ type:Int)
}
