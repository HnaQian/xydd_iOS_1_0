//
//  HomeHotGiftListUnitView.swift
//  Limi
//
//  Created by guo chen on 15/12/22.
//  Copyright © 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//热卖商品榜单

import UIKit

class HomeHotGiftListUnitView: HomeBaseUnitView {
    
    var topTitle = UILabel()
    var scrollView = UIScrollView()

    var buttons = [Button]() {
        didSet
        {
            var lastView: UIView?
            for i in 0 ..< buttons.count
            {
                scrollView.addSubview(buttons[i])
                
                buttons[i].snp_makeConstraints(closure: { (make) -> Void in
                    
                    let _ = make.top.equalTo(scrollView)
                    
                    let _ = make.height.equalTo(scrollView)

                    let _ = make.left.equalTo(lastView == nil ? scrollView.snp_left : lastView!.snp_right).offset(10)
                })
                
                lastView = buttons[i]
            }

            let total_width = CGFloat(buttons.count) * ((lastView?.iwidth)! + 10) + 10
            
            if total_width > Constant.ScreenSize.SCREEN_WIDTH
            {
                scrollView.contentSize = CGSize(width: total_width, height: 0)
            }
        }
    }
    
    
    // 页面布局
    override func setupUI() {
        
        self.backgroundColor = UIColor.white
        
        for view in [topTitle, scrollView] as [Any]
        {
            self.addSubview(view as! UIView)
        }

        // 设置属性
        topTitle.text = "礼物榜单"
        topTitle.textColor = UIColor(rgba: Constant.common_C7_color)
        
        topTitle.font = UIFont.systemFont(ofSize: Constant.common_F5_font)
        
        
        
        scrollView.scrollsToTop = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        
        self.selfHeight = 236 * Constant.ScreenSize.SCREEN_WIDTH_SCALE
        self.snp_makeConstraints { (make) -> Void in
            let _ = make.height.equalTo(self.selfHeight)
        }
        
        // 页面布局
        topTitle.snp_makeConstraints { (make) -> Void in
            let _ = make.left.equalTo(self).offset(10)
            let _ = make.top.equalTo(self).offset(16)
            let _ = make.height.equalTo(16)
            
        }
        
        scrollView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(topTitle.snp_bottom).offset(10)
            let _ = make.bottom.equalTo(self).offset(-10)
            let _ = make.left.equalTo(self)
            let _ = make.right.equalTo(self)
        }
        
        // 加载礼物榜单内容
        self.loadGoodsMatter()
    }
    
    // 加载内容
    func loadGoodsMatter()
    {
        if let title = JSON(self.unitData as AnyObject)["attributes"]["text"].string
        {
            topTitle.text = title
        }
        
        var buttonArray = [Button]()
        
        if let children = JSON(self.unitData as AnyObject)["children"].array {
            
            for i in 0 ..< children.count {
                let button = Button()
                let child = children[i]
                
                if let text = child["attributes"]["text"].string
                {
                    //延迟一秒显示 不然会黑屏 原因未找到
                    let time: TimeInterval = 1.0
                    let delay = DispatchTime.now() + Double(Int64(time * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                    DispatchQueue.main.asyncAfter(deadline: delay) {
                        do {
                            let attribText : NSAttributedString = try NSAttributedString(data: text.data(using: String.Encoding.unicode, allowLossyConversion: true)!, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil)
                            button.numlabel.attributedText = attribText
                        } catch {
                            print(error)
                        }
                    }
                    
                }
                
                if let src = child["attributes"]["src"].string {
                    let imageScale : String = src + "?imageView2/0/w/" + "\(Int(120*Constant.ScreenSize.SCALE_SCREEN))" + "/h/" + "\(Int(120*Constant.ScreenSize.SCALE_SCREEN))"
                    
                    button.imageView.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "loadingDefault"))
                }
                
                if let title = child["attributes"]["title"].string {
                    button.shopNameLabel.text = title
                }
                
                if let price = child["attributes"]["price"].string {
                    
                    let count = Utils.decimalCount(price)

                    let priceFloat : Float = NSString(string: price).floatValue

                    button.priceLabel.text = "￥" + String(format: "%.\(count)f", priceFloat)
                }
                
                if let marketPrice = child["attributes"]["market_price"].string {
                    let marketpriceFloat = NSString(string: marketPrice).floatValue
                    if marketpriceFloat != 0
                    {
                        let count = Utils.decimalCount(marketPrice)
                        let marketPriceString = String(format: "￥%.\(count)f", marketpriceFloat)
                        button.marketpriceLabel.text = marketPriceString
                    }
                }
                
                if let href = child["attributes"]["href"].string {
                    button.href = href
                    button.isUserInteractionEnabled = true
                    button.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(HomeHotGiftListUnitView.buttonClicked(_:))))
                    
                }
                buttonArray.append(button)
            }
            
        }
        self.buttons = buttonArray
    }
    
    @objc func buttonClicked(_ sender: UITapGestureRecognizer){
        
        if let href = (sender.view as? Button)?.href
        {
            Statistics.count(Statistics.Home.home_area_right1_click,andAttributes: ["href": href])
            
            self.delegate?.homeUnitViewTouchOnClick({(homeVC) in
                EventDispatcher.dispatch(href, onNavigationController: homeVC.navigationController)
            })
        }
    }
    
    // 自定义 单个button
    class Button: UIView {
        
        let MARGON:CGFloat = 10
        
        var href: String?
        
        var imageView = UIImageView()  // 商品图片
        var rankingImageView = UIImageView(image: UIImage(named: "home_paihang_bg"))  // “送出XX” 图片
        var numlabel = CustomLabel(font: Constant.common_F7_font, textColorHex: Constant.common_C12_color,textAlignment: NSTextAlignment.left)   // “送出XX”
        var shopNameLabel = UILabel()  // 商品名称
        var priceLabel = UILabel()     // 心意价
        var marketpriceLabel = StrikeThroughLabel()  // 市场价
        var contentView = UIView()
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            self.iwidth = 120 * Constant.ScreenSize.SCREEN_WIDTH_SCALE
            self.layer.borderColor = UIColor(rgba: Constant.common_C10_color).cgColor
            self.layer.borderWidth = 0.5
            
            // 属性设置
            imageView.clipsToBounds = true
            imageView.contentMode = UIViewContentMode.scaleAspectFill
            
            rankingImageView.clipsToBounds = true
            rankingImageView.contentMode = UIViewContentMode.scaleAspectFill
            
            numlabel.numberOfLines = 1
            
            shopNameLabel.addAttribute(font: Constant.common_F5_font, textColor: Constant.common_C6_color)
            
            shopNameLabel.numberOfLines = 1
            shopNameLabel.lineBreakMode = NSLineBreakMode.byTruncatingTail
            
            priceLabel.addAttribute(font: Constant.common_F6_font, textColor: Constant.common_C1_color)
            
            marketpriceLabel.addAttribute(font: Constant.common_F6_font, textColor: Constant.common_C8_color)
            
            if Constant.DeviceType.IS_IPHONE_5 == true || Constant.DeviceType.IS_IPHONE_4_OR_LESS  == true{
                
                shopNameLabel.font = UIFont.systemFont(ofSize: Constant.common_F6_font)
                priceLabel.font = UIFont.systemFont(ofSize: Constant.common_F7_font)
                marketpriceLabel.font = UIFont.systemFont(ofSize: Constant.common_F7_font)
            }else
            {
                shopNameLabel.font = UIFont.systemFont(ofSize: Constant.common_F5_font)
                priceLabel.font = UIFont.systemFont(ofSize: Constant.common_F6_font)
                marketpriceLabel.font = UIFont.systemFont(ofSize: Constant.common_F6_font)
            }
            
            
            
            marketpriceLabel.strikeThroughEnabled = true
            
            
            // 布局
            self.addSubview(contentView)
            for view in [imageView, rankingImageView, numlabel, shopNameLabel, priceLabel, marketpriceLabel] as [Any]
            {
                contentView.addSubview(view as! UIView)
            }
            contentView.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(self)
                let _ = make.top.equalTo(self)
                let _ = make.bottom.equalTo(self)
                let _ = make.right.equalTo(self)
            }
            
            rankingImageView.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(contentView).offset(8)
                let _ = make.left.equalTo(contentView)
                let _ = make.width.equalTo(50)
                let _ = make.height.equalTo(16)
                
            }
            
            numlabel.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(contentView).offset(4)
                let _ = make.centerY.equalTo(rankingImageView)
                let _ = make.right.lessThanOrEqualTo(rankingImageView).offset(-5)
            }
            
            imageView.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(contentView)
                let _ = make.left.equalTo(contentView)
                let _ = make.right.equalTo(contentView)
                let _ = make.width.equalTo(120 * Constant.ScreenSize.SCREEN_WIDTH_SCALE)
                let _ = make.height.equalTo(imageView.snp_width)
            }
            
            shopNameLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(imageView).offset(MARGON)
                let _ = make.right.equalTo(imageView)
                let _ = make.top.equalTo(imageView.snp_bottom).offset(10)
                let _ = make.height.equalTo(16 * Constant.ScreenSize.SCREEN_WIDTH_SCALE)
                
                if Constant.DeviceType.IS_IPHONE_4_OR_LESS == true || Constant.DeviceType.IS_IPHONE_5 == true
                {
                    let _ = make.top.equalTo(imageView.snp_bottom).offset(6)
                }
            }
            
            priceLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(shopNameLabel)
                let _ = make.top.equalTo(shopNameLabel.snp_bottom).offset(4)
                let _ = make.height.equalTo(14 * Constant.ScreenSize.SCREEN_WIDTH_SCALE)
            }
            
            marketpriceLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(priceLabel.snp_right).offset(2)
                let _ = make.centerY.equalTo(priceLabel)
                let _ = make.height.equalTo(14)
            }
            
        }
        
        required init(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
    }
}
