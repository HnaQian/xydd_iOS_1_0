//
//  HomeActivityView.swift
//  Limi
//
//  Created by Richie on 16/4/20.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//首页活动版图


import UIKit

class HomeActivityView: UIView,HomeBaseUnitViewDelegate {
    
    //本地视图名与服务器数据视图名映射表
    fileprivate let data_viewName =
        [
            "carousel"  : "HomeGiftRemindBannerUnitView",
            
            "grid"      : "HomeActivityUnitView",
            
            "flash_sale": "HomeTodayVitalityUnitView",
            
            "top_goods" : "HomeHotGiftListUnitView",
            
            "ad"        : "HomeADbannerUnitView",
            
            "space"     : "HomeSpaceUnitView",
            
            "fourpic"     : "HomeFlashSaleUnitView",
            
            "title"     : "HomeTitleUnitView"
    ]
    
    
    //unitView map
    fileprivate let unitViewMap: [String: HomeBaseUnitView.Type] =
        [
            "HomeGiftRemindBannerUnitView": HomeGiftRemindBannerUnitView.self,//滚动视图(送礼提醒)
            
            "HomeActivityUnitView": HomeActivityUnitView.self,//三个活动入口
            
            "HomeTodayVitalityUnitView" : HomeTodayVitalityUnitView.self,//限时抢购的视图
            
            "HomeHotGiftListUnitView" : HomeHotGiftListUnitView.self,//礼物榜单
            
            "HomeADbannerUnitView" : HomeADbannerUnitView.self,//平铺广告栏
            
            "HomeSpaceUnitView" : HomeSpaceUnitView.self,//模块之间的间隔
            
            "HomeFlashSaleUnitView" : HomeFlashSaleUnitView.self,//三个攻略
            
            "HomeTitleUnitView" : HomeTitleUnitView.self//文字
    ]
    
    
    weak var delegate:HomeActivityViewDelegate?
    
    //列表数据
    fileprivate var tableDataSource:[JSON]!
    fileprivate var unitViewClassNames:[String]!
    fileprivate var valueKeys:[String]!
    
    //布局数据
    fileprivate var pageSource:[JSON]!
    
    fileprivate var currentIndex:Int = 0
    
    fileprivate var unitViews:[HomeBaseUnitView]!
    
    var activityHeight:CGFloat {return _activityHeight}
    
    var segmentHeight:CGFloat {return _segmentHeight}
    
    fileprivate var _activityHeight:CGFloat = 0
    fileprivate var _segmentHeight:CGFloat = 0
    
    func refreshWithData(_ pageSource:[JSON]){
        self.pageSource = pageSource
        self.refreshReflectionAry()
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    //MARK: 刷新模块数组
    fileprivate func refreshReflectionAry()
    {
        /**
         清除旧的视图排序，建立新的视图排序
         */
        unitViews = [HomeBaseUnitView]()
        self.unitViewClassNames = []
        self.valueKeys = [String]()
        
        let _ = self.pageSource.map{
            
            if let name = $0["name"].string{
                print(name)
                self.valueKeys.append(name)
                
                if let view_name = self.data_viewName[name],
                    let CLASS = unitViewMap[view_name],
                    let munitView = CLASS.newUnitView($0,dataKey: name)
                {
                    munitView.delegate = self
                    
                    self.unitViews.append(munitView)
                }
            }
        }
        
        self.refreshUI()
    }
    
    
    
    //MARK:刷新界面数据
    fileprivate func refreshUI()
    {
        if unitViews.count > 0{
            
            let _ = self.subviews.map{$0.removeFromSuperview()}
            let _ = unitViews.map{self.addSubview($0)}
            
            var tempView:UIView?
            var height:CGFloat = 0
            
            for view in unitViews{
                
                view.snp_makeConstraints(closure: { (make) -> Void in
                    
                    let _ = make.left.equalTo(self)
                    let _ = make.right.equalTo(self)
                    let _ = make.height.equalTo(view.selfHeight)
                    let _ = make.top.equalTo(tempView == nil ? self.snp_top : (tempView!.snp_bottom))
                })
                
                height += view.selfHeight
                tempView = view
            }
            
            self.iwidth = Constant.ScreenSize.SCREEN_WIDTH
            self.iheight = height
        }
        
        _activityHeight = self.iheight
        
        self.iheight = self.iheight + segmentHeight
    }
    
    
    
    fileprivate func setupUI(){
        self.backgroundColor = UIColor.white
    }
    
    
    func  homeUnitViewTouchOnClick(_ moduleSelectedHandler: ((_ homeVC:HomeViewControllerV1)->Void)){
        delegate?.homeActivityViewDidSelected({ (homeVC) in
            moduleSelectedHandler(homeVC)
        })
    }
}


protocol HomeActivityViewDelegate:NSObjectProtocol {
    
    //点击某个板块的动作
    func  homeActivityViewDidSelected(_ moduleSelectedHandler:((_ homeVC:HomeViewControllerV1)->Void))
}
