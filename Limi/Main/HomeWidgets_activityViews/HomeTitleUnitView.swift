//
//  HomeTitleUnitView.swift
//  Limi
//
//  Created by Richie on 16/6/14.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//首页标题模块

import UIKit

class HomeTitleUnitView: HomeBaseUnitView {

    /*
     {
     attributes =             {
     "font_size" = 18;
     title = "\U4eba\U6c14\U7cbe\U9009";
     "title_color" = "#12171F";
     };
     na
    */
    
    fileprivate let titleLbl = UILabel()
    
    
    override func setupUI() {
        
        self.iwidth = Constant.ScreenSizeV1.SCREEN_WIDTH
        self.iheight = Constant.ScreenSizeV1.MARGIN_44
        self.selfHeight = self.iheight
        self.addSubview(titleLbl)
        
        titleLbl.frame = CGRect(x: 0, y: (self.iheight-Constant.ScreenSizeV1.MARGIN_30)/2, width: self.iwidth, height: Constant.ScreenSizeV1.MARGIN_30)
        
        titleLbl.textAlignment = NSTextAlignment.center
        
        //
        if let attr = self.unitData["attributes"] as? [String:AnyObject]{
            
            let mattr = JSON(attr as AnyObject)
            if let font = mattr["font"].int{
                titleLbl.font = UIFont.systemFont(ofSize: CGFloat(font))
            }
            
            if let title = mattr["title"].string{
                titleLbl.text = title
            }
            
            if let color = mattr["title_color"].string{
                if color.characters.count > 6{
                    titleLbl.textColor = UIColor(rgba: color)
                }
            }

        }
    }
    

}
