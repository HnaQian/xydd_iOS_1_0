
//
//  HomeADbannerUnitView.swift
//  Limi
//
//  Created by guo chen on 15/12/22.
//  Copyright © 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//广告banner 平铺的视图

import UIKit

class HomeADbannerUnitView: HomeBaseUnitView {
    
    /*
     
     {
     attributes =             {
     height = 260;
     width = 960;
     };
     children =             (
     {
     href = "http://wechat.giftyou.me/article_item?id=815&app=1";
     src = "http://up.xydd.co/14648317238929.jpg";
     }
     );
     
     */
    
    let height:CGFloat = 89 * Constant.ScreenSize.SCREEN_WIDTH/414
    
    var dataSource = [ADMode]()
    
    var  edgGap:CGFloat = 0
    
    override func setupUI() {
        self.selfHeight = height
        self.iheight = height
        self.iwidth = Constant.ScreenSizeV2.SCREEN_WIDTH
        
        if let width = JSON(self.unitData as AnyObject)["attributes"]["width"].string,
            let height = JSON(self.unitData as AnyObject)["attributes"]["height"].string
        {
            
            
            if NSString(string: width).intValue != 0 && NSString(string: height).intValue != 0
            {
                
                let mWidth = CGFloat(NSString(string: width).intValue)
                let mHeight = CGFloat(NSString(string: height).intValue)
                
                edgGap = ((serviceWidth - mWidth) * serviceWidthScal)/2
                
                self.selfHeight = mHeight * serviceWidthScal
                
                self.iheight = self.selfHeight
            }
        }
        
        if let children = JSON(self.unitData as AnyObject)["children"].array {
            for child in children {
                dataSource.append(ADMode(dataDic: child))
            }
        }
        
        self.createSingle()
    }
    
    
    func createSingle()
    {
        if dataSource.count == 0 {
            return
        }

        var leftDistance:CGFloat = edgGap
        for index in 0...(dataSource.count - 1)
        {
            let imageView = UIImageView()
            imageView.backgroundColor = UIColor.red
            imageView.clipsToBounds = true
            imageView.isUserInteractionEnabled = true
            imageView.contentMode = UIViewContentMode.scaleAspectFill
            
            var frame = CGRect(x: CGFloat(dataSource[index].left) * serviceWidthScal, y: CGFloat(dataSource[index].top) * serviceWidthScal, width: CGFloat(dataSource[index].width) * serviceWidthScal, height: CGFloat(dataSource[index].height) * serviceWidthScal)
            frame.origin.x += leftDistance
            imageView.frame = frame
             leftDistance += frame.size.width + CGFloat(dataSource[index].right + dataSource[index].left) * serviceWidthScal
            if let url = URL(string: Utils.scaleImage(dataSource[index].src, width: imageView.iwidth * 3, height: imageView.iheight * 3)){
                imageView.af_setImageWithURL(url, placeholderImage: UIImage(named: "Default_750_216"))
            }
            
            self.addSubview(imageView)
            imageView.tag = 100 + index
            imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(HomeADbannerUnitView.tagGestAction(_:))))
        }
    }
    
    
    func tagGestAction(_ tapGest:UITapGestureRecognizer)
    {
        if let view  = tapGest.view
        {
            let href = dataSource[view.tag - 100].href
            Statistics.count(Statistics.Home.home_ad_add_click, andAttributes: ["href": href])
            
            self.delegate?.homeUnitViewTouchOnClick({ (homeVC) in
                EventDispatcher.dispatch(href, onNavigationController: homeVC.navigationController)
                })
        }
    }
    
}
