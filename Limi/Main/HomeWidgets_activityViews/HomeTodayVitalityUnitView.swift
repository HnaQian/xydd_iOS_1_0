//
//  HomeTodayVitalityUnitView.swift
//  Limi
//
//  Created by maohs on 16/5/11.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

//争分夺礼
import UIKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


class HomeTodayVitalityUnitView: HomeBaseUnitView {
    fileprivate let leftTitleLabel:UILabel = {
        let tmpLabel = UILabel()
        tmpLabel.textColor = UIColor(rgba: Constant.common_C2_color)
//        tmpLabel.font = Constant.Theme.Font_15
        tmpLabel.font = UIFont.boldSystemFont(ofSize: Constant.Theme.FontSize_16)
        return tmpLabel
    }()
    
    //倒计时
    fileprivate let timeView:TimeView = {
        let tmpView = TimeView()
        
        return tmpView
    }()
    
    //查看全部
    fileprivate let rightButton:UIButton = {
        let tmpButton = UIButton(type:.custom)
        tmpButton.setTitle("更多", for: UIControlState())
        tmpButton.setTitleColor(UIColor(rgba: Constant.common_C7_color), for: UIControlState())
        tmpButton.titleLabel?.font = Constant.Theme.Font_15
        
        tmpButton.setImage(UIImage(named: "home_module_topic_rightarrow"), for: UIControlState())
        tmpButton.titleEdgeInsets = UIEdgeInsetsMake(0, -17 - Constant.ScreenSizeV2.MARGIN_4, 0, 0)
        tmpButton.imageEdgeInsets = UIEdgeInsetsMake(0, (tmpButton.titleLabel?.frame)!.maxX + 30, 0, 0)
        
        return tmpButton
    }()

    //查看所有的URL
    fileprivate var rightButtonUrl:String?
    
    fileprivate var endDate:Date?
    
    fileprivate var showNextChild:Bool = false
    
    fileprivate var horTableView:LMHorizontalTableView!

    //cell的高度
    fileprivate let cell_height:CGFloat = 220 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
    //cell的宽度
    fileprivate let cell_width:CGFloat = 520 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
    
    override class func shouldReturnNewView(_ data: JSON,dataKey: String)-> [String:AnyObject]?
    {
        
        return shouldShowNextChildren(data) ? data.dictionaryObject : nil

    }
    
    fileprivate class func shouldShowNextChildren(_ data:JSON) ->Bool {

        if let time = data["attributes"]["end_time"].string {

            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let tempEndDate = dateFormatter.date(from: time)

            if tempEndDate == nil {
                return false
            }
            let zone:TimeZone = TimeZone.current
            let interval:Int = zone.secondsFromGMT(for: tempEndDate!)
            let intervalCurrent :Int = zone.secondsFromGMT(for: Date())
            let tmpDate = (tempEndDate!.addingTimeInterval(TimeInterval(interval)))
            let nowDate = (Date().addingTimeInterval(TimeInterval(intervalCurrent)))
            let interval2 = tmpDate.timeIntervalSince(nowDate)

            //应该显示下一次的
            if interval2 < 0 {
                //nextChildren > 0 return true
                if let children = data["next_children"].array {
                    if children.count > 0{
                        return true
                    }
                }
                
            }else{
                //显示当前的
                //children > 0 return true
                if let children = data["children"].array {
                    if children.count > 0{
                        return true
                    }
                }
            }
        }
        
        return false
    }
    
    override func setupUI() {
        
        let widthScale:CGFloat = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        self.backgroundColor = UIColor(rgba: "#eeeeee")
        self.selfHeight = 300 * widthScale
        self.iheight = self.selfHeight
        
        leftTitleLabel.frame = CGRect(x: 30 * widthScale, y: 0, width: 80 * widthScale, height: 16)
        timeView.ileft = leftTitleLabel.iright
        timeView.ibottom = leftTitleLabel.ibottom
        
        rightButton.addTarget(self, action:#selector(HomeTodayVitalityUnitView.rightButtonClicked), for: .touchUpInside)
        rightButton.frame = CGRect(x: Constant.ScreenSize.SCREEN_WIDTH - 50, y: leftTitleLabel.itop, width: 40, height: leftTitleLabel.iheight)
        
        horTableView = LMHorizontalTableView(frame: CGRect(x: 30 * widthScale,y: leftTitleLabel.ibottom + 26 * widthScale,width: Constant.ScreenSize.SCREEN_WIDTH - 30 * widthScale,height: cell_height))
        self.selfHeight = horTableView.ibottom
        horTableView.dataSource = self
        horTableView.delegate = self
        
        for view in [leftTitleLabel,timeView,rightButton,horTableView] {
            self.addSubview(view)
        }
  
        loadGoodsData()
    }
    
    
    func rightButtonClicked() {
        if let href = rightButtonUrl {

            self.delegate?.homeUnitViewTouchOnClick({(homeVC) in
                EventDispatcher.dispatch(href, onNavigationController: homeVC.navigationController)
            })
        }
    }
    
    
    func loadGoodsData() {
        
        var isOverTime = false
        if let leftTitle = JSON(self.unitData as AnyObject)["attributes"]["title"]["name"].string {
            leftTitleLabel.text = leftTitle
            let constraint = CGSize(width: 120, height: 16);
            let attribute = NSDictionary(object: leftTitleLabel.font, forKey: NSFontAttributeName as NSCopying)
            let size = (leftTitle as NSString).boundingRect(with: constraint, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes:attribute as? [String : AnyObject], context: nil)
            leftTitleLabel.iwidth = size.width
            timeView.ileft = leftTitleLabel.iright + MARGIN_8
            
        }
        
//        if let titleColor = JSON(self.unitData as AnyObject)["attributes"]["title"]["color"].string {
//            leftTitleLabel.textColor = UIColor(rgba: titleColor)
//        }
        
        if let href = JSON(self.unitData as AnyObject)["attributes"]["href"].string {
            rightButtonUrl = href
        }
        
        if let dateString = JSON(self.unitData as AnyObject)["attributes"]["end_time"].string {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
           let tmpDate = formatter.date(from: dateString)
            
            if tmpDate != nil {
                
                let zone : TimeZone = TimeZone.current
                
                let interval : Int = zone.secondsFromGMT(for: tmpDate!)
                let intervalCurrent : Int = zone.secondsFromGMT(for: Date())
                
                endDate = (tmpDate!.addingTimeInterval(TimeInterval(interval)))
                let nowDate = (Date().addingTimeInterval(TimeInterval(intervalCurrent)))
                
                let interval2 = endDate!.timeIntervalSince(nowDate)
                if interval2 < 0 {
                    isOverTime = true
                }
            }
        }
        
        if isOverTime {
            
            if let children = JSON(self.unitData as AnyObject)["next_children"].array {
                showNextChild = true
                loadSpecialGoodsData(children)
            }
        }else {
            if let children = JSON(self.unitData as AnyObject)["children"].array {
                
                loadSpecialGoodsData(children)
            }
        }
        
        if let time = JSON(self.unitData as AnyObject)["attributes"]["countdown"].string {
            let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
            let year = (calendar as NSCalendar).component(.year, from: Date())
            let month = (calendar as NSCalendar).component(.month, from: Date())
            let day = (calendar as NSCalendar).component(.day, from: Date())
            var dateString = "\(year)-\(month)-\(day)"
            dateString = dateString + " " + time
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
           let countdown = dateFormatter.date(from: dateString)
            
            repeateMinuSecondLabel(9)
            //倒计时
            countDown(countdown)
        }
        
    }
    
    fileprivate var goodsAry = [JSON]()
    
    
    func loadSpecialGoodsData(_ children:[JSON]) {
        goodsAry = children
        
        self.horTableView.reloadData()
        
    }
    
    // 倒计时
    func countDown(_ refrenceDate:Date?) {
        if refrenceDate != nil{
            let zone : TimeZone = TimeZone.current
            
            let interval : Int = zone.secondsFromGMT(for: refrenceDate!)
            let intervalCurrent : Int = zone.secondsFromGMT(for: Date())
            
            let tmpDate = (refrenceDate!.addingTimeInterval(TimeInterval(interval)))
            let nowDate = (Date().addingTimeInterval(TimeInterval(intervalCurrent)))
            
            var interval2 = tmpDate.timeIntervalSince(nowDate)
            if interval2 <= 0 {
                let tempInterval = endDate?.timeIntervalSince(nowDate)
                if tempInterval < 0 && !showNextChild{
                    if let children = JSON(self.unitData as AnyObject)["next_children"].array {
                        if children.count > 0 {
                            showNextChild = true
                            loadSpecialGoodsData(children)
                        }else {
                           NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.flash_saleDataReload), object: nil)
                        }
                    }
                }
                repeat {
                    interval2 = interval2 + 24 * 3600
                }while(interval2 < 0)
               
            }
            let hour = String(format:"%.2i", Int(interval2/3600))
            let minute = String(format: "%.2i", Int(interval2.truncatingRemainder(dividingBy: 3600)/60))
            let second = String(format:"%.2i", Int(interval2.truncatingRemainder(dividingBy: 60)))
            self.timeView.hourLabel.text = hour
            self.timeView.minuteLabel.text = minute
            self.timeView.secondLabel.text = second
            
            gcd.async(.main, delay: 1.0) {
                
                self.countDown(refrenceDate)
            }
        }
    }
    
    func repeateMinuSecondLabel(_ outCount:Int) {
        var count = outCount
        
        weak var weakSelf = self as HomeTodayVitalityUnitView
        
        
        gcd.async(.main, delay: 0.1) {
            count -= 1
            if count == -1{count = 9}
            self.timeView.minuSecondLabel.text = "\(count)"
            self.repeateMinuSecondLabel(count)
        }
        
        #if false
        gcd.async(.Background, delay: 0.1) {
            count -= 1
            if count == -1{count = 9}
            
            dispatch_async(dispatch_get_main_queue(), {
                weakSelf?.timeView.minuSecondLabel.text = "\(count)"
            })
            weakSelf?.repeateMinuSecondLabel(count)
        }
        #endif
    }

    
}



//MARK: tableView的代理
extension HomeTodayVitalityUnitView:LMHorizontalTableViewDataSource,LMHorizontalTableViewDelegate{
    
    func numberOfRows(_ horizontalTableView: LMHorizontalTableView) -> Int {
        return goodsAry.count
    }
    
    
    func horizontalTableView(_ horizontalTableView: LMHorizontalTableView, cellForRow row: Int ,reuseCell:((_ reuseInfentifer:String) ->LMHorizontalTableViewCell?)) -> LMHorizontalTableViewCell {
        
        var cell:SubCell? = reuseCell("indetifer") as? SubCell
        
        if cell == nil{
            cell = SubCell(reuseIdentifier:"indetifer")
        }
        
        cell!.reloadData(goodsAry[row])
        
        return cell!
    }
    
    
    func horizontalTableView(_ horizontalTableView: LMHorizontalTableView, heightForRow row: Int) -> CGFloat{
        return cell_height
    }
    
    func horizontalTableView(_ horizontalTableView: LMHorizontalTableView, widthForRow row: Int) -> CGFloat{
        return cell_width
    }
    
    
    func horizontalTableView(_ horizontalTableView: LMHorizontalTableView, didSelectRow row: Int) {
        
        if let src = goodsAry[row]["goods_url"].string{
            Statistics.count(Statistics.Home.home_sales_click,andAttributes: ["href":src])
            self.delegate?.homeUnitViewTouchOnClick({(homeVC) in
                EventDispatcher.dispatch(src, onNavigationController: homeVC.navigationController)
            })
        }
    }
    
}

extension HomeTodayVitalityUnitView{
    
    
    fileprivate class SubCell:LMHorizontalTableViewCell{
        
        fileprivate var itemView:CustItemView!
        
        override init(reuseIdentifier: String) {
            super.init(reuseIdentifier: reuseIdentifier)
            setupUI()
        }
        
        func reloadData(_ data:JSON){
            itemView.reloadData(data)
        }
        
        fileprivate func setupUI(){
            self.clipsToBounds = true
            itemView = CustItemView()
            self.addSubview(itemView)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    
    
    
    
    //MARK: - CustItemView
    class CustItemView: UIView {
        //商品图片
        let goodsImageView:UIImageView = {
            let imgView = UIImageView()
            imgView.image = UIImage(named: "Default_300_160")
            imgView.clipsToBounds = true
            imgView.contentMode = UIViewContentMode.center
            
            return imgView
        }()
        
        //心意价
        let priceLabel:UILabel = {
            let label = UILabel()
            label.textColor = Constant.Theme.Color2
            label.textAlignment = .right
            label.font = Constant.Theme.Font_13
            return label
        }()
        
        //市场价
        let marketPriceLabel:StrikeThroughLabel = {
            let label = StrikeThroughLabel()
            label.textColor = UIColor(rgba: Constant.common_C7_color)
            label.textAlignment = .left
            label.font = Constant.Theme.Font_11
            label.strikeThroughEnabled = true
            return label
        }()
        
        //商品名称
        let goodsLabel:UILabel = {
            let label = UILabel()
            label.textColor = UIColor(rgba: "#666666")
            label.textAlignment = .center
            label.font = Constant.Theme.Font_13
            label.textColor = Constant.Theme.Color2
            return label
        }()
        
        let seperateView:UIView = {
            let view = UIView()
            view.backgroundColor = UIColor(rgba: "#eeeeee")
            
            return view
        }()
        
        var goods_url:String?
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            self.frame = CGRect.zero
            setUpUI()
        }
        
        
        func reloadData(_ data:JSON){
            print(data)
            if let imageSrc = data["goods_image"].string {
                let imageScale:String = imageSrc + "?imageView2/0/w/" + "\(Int(goodsImageView.iwidth*Constant.ScreenSize.SCALE_SCREEN))" + "/h/" + "\(Int(goodsImageView.iheight*Constant.ScreenSize.SCALE_SCREEN))"
                self.goodsImageView.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "Default_300_160"))
            }
            
            if let price = data["event_price"].string {
                
                let count = Utils.decimalCount(price)
                
                let priceFloat : Float = NSString(string: price).floatValue
                
                self.priceLabel.text = "￥" + String(format: "%.\(count)f", priceFloat)
            }
            
            if let marketPrice = data["price"].string {
                let marketpriceFloat = NSString(string: marketPrice).floatValue
                if marketpriceFloat != 0
                {
                    let count = Utils.decimalCount(marketPrice)
                    let marketPriceString = String(format: "￥%.\(count)f", marketpriceFloat)
                    self.marketPriceLabel.text = marketPriceString
                    
                    self.priceLabel.textAlignment = .right
                    self.priceLabel.iwidth = 131.5 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
                }else {
                    self.priceLabel.textAlignment = .center
                    self.priceLabel.iwidth = 275 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
                }
            }
            
            if let goodsName = data["product_name"].string {
                self.goodsLabel.text = goodsName
            }
        }
        
        func setUpUI() {
            self.backgroundColor = UIColor.white
            
            for view in [goodsImageView,priceLabel,marketPriceLabel,goodsLabel,seperateView] {
                self.addSubview(view)
            }
            let scale = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
            goodsImageView.frame = CGRect(x: 10 * scale, y: 12 * scale, width: 195 * scale, height: 195 * scale)
            goodsLabel.frame = CGRect(x: goodsImageView.iright + 10 * scale, y: 84 * scale, width: 275 * scale, height: 16)
            
            priceLabel.frame = CGRect(x: goodsLabel.ileft, y: goodsLabel.ibottom + 20 * scale, width: 131.5 * scale, height: 15)
            marketPriceLabel.frame = CGRect(x: priceLabel.iright + 6 * scale, y: goodsLabel.ibottom + 20 * scale, width: priceLabel.iwidth, height: 15)
            seperateView.frame = CGRect(x: 500 * scale, y: 0, width: 20 * scale, height: 220 * scale)
            
            self.iwidth = 520 * scale
            self.iheight = 220 * scale
            
            
        }
        
        required init(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
    }
    
    //MARK: - TimeView
    class TimeView: UIView {
        var hourLabel = UILabel()
        var minuteLabel = UILabel()
        var secondLabel = UILabel()
        var minuSecondLabel = UILabel()
        
        fileprivate let stateLabel:UILabel = {
            let label = UILabel()
            label.textAlignment = .center
            label.font = Constant.Theme.Font_13
            label.textColor = UIColor(rgba: Constant.common_C7_color)
            label.text = ""
            
            return label
        }()
        
        fileprivate let leftColonLabel = UILabel()
        fileprivate let middleColonLabel = UILabel()
        fileprivate let rightColonLabel = UILabel()
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            
            setUpUI()
        }
        
        func setUpUI() {
            let widthScale:CGFloat = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
            for view in [stateLabel,hourLabel,leftColonLabel,minuteLabel,middleColonLabel,secondLabel,rightColonLabel,minuSecondLabel] {
                self.addSubview(view)
                view.frame = CGRect(x: 0, y: 0, width: 31 * widthScale, height: 31 * widthScale)
            }
            
            timeLabelAttribute(hourLabel)
            timeLabelAttribute(minuteLabel)
            timeLabelAttribute(secondLabel)
            timeLabelAttribute(minuSecondLabel)
            minuSecondLabel.backgroundColor = UIColor(rgba: "#EE5151")
            
            colonLabelAttribute(leftColonLabel)
            colonLabelAttribute(middleColonLabel)
            colonLabelAttribute(rightColonLabel)
            
            stateLabel.frame = CGRect(x: 0, y: 0, width: 0, height: 31 * widthScale)
            
            hourLabel.ileft = 0
            
            leftColonLabel.ileft = hourLabel.iright
            leftColonLabel.iwidth = MARGIN_8
            
            minuteLabel.ileft = leftColonLabel.iright
            
            middleColonLabel.ileft = minuteLabel.iright
            middleColonLabel.iwidth = MARGIN_8
            
            secondLabel.ileft = middleColonLabel.iright
            
            rightColonLabel.ileft = secondLabel.iright
            rightColonLabel.iwidth = MARGIN_8
            
            minuSecondLabel.iwidth =  21 * widthScale
            minuSecondLabel.ileft = rightColonLabel.iright
            minuSecondLabel.layer.cornerRadius = 3
            minuSecondLabel.layer.masksToBounds = true
            minuSecondLabel.text = "0"
            
            self.iwidth = minuSecondLabel.iright
            self.iheight = 30 * widthScale
            
        }
        
        func timeLabelAttribute(_ timeLabel:UILabel) {
            timeLabel.textColor = UIColor.white
            timeLabel.font = Constant.Theme.Font_11
            timeLabel.layer.cornerRadius = 4
            timeLabel.layer.masksToBounds = true
            timeLabel.textAlignment = .center
            timeLabel.backgroundColor = UIColor(rgba: "#515151")
            timeLabel.text = "00"
        }
        
        func colonLabelAttribute(_ colonLabel:UILabel) {
            colonLabel.textColor = UIColor(rgba: "#515151")
            colonLabel.font = Constant.Theme.Font_11
            colonLabel.textAlignment = .center
            colonLabel.text = ":"
        }
        
        required init(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
    }

}


