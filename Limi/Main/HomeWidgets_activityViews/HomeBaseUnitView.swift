//
//  HomeBaseUnitView.swift
//  Limi
//
//  Created by guo chen on 15/12/22.
//  Copyright © 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//首页各种单元视图的基类

import UIKit


class HomeBaseUnitView: UIView
{
    /*
     // Only override drawRect: if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func drawRect(rect: CGRect) {
     // Drawing code
     }
     */
    
    //首页尺寸缩放比例
    let serviceWidthScal:CGFloat = Constant.ScreenSizeV2.SCREEN_WIDTH/960
    //服务器的宽度
    let serviceWidth:CGFloat = 960
    
    //以下的间距按需求使用
    //顶部和底部的内间距
    let verticalMargin:CGFloat = Constant.ScreenSizeV2.MARGIN_15
    //左右两边的内间距
    let horizontalMargin:CGFloat = Constant.ScreenSizeV2.MARGIN_30
    
    weak var delegate:HomeBaseUnitViewDelegate?
    
    var selfHeight:CGFloat = 0//每个unitView自己的高度
    
    var unitData:[String:AnyObject]!
    
    class func newUnitView(_ data: JSON, dataKey: String) ->HomeBaseUnitView?
    {
        if let unitData =  self.shouldReturnNewView(data,dataKey: dataKey)
        {
            let unitView = self.init()
            
            unitView.isUserInteractionEnabled = true
            unitView.clipsToBounds = true
            
            unitView.unitData = unitData
            
            unitView.setupUI()
            
            return unitView
        }
        
        return nil
    }
    
    
    /**
     是否应该创建此函数当前的调用者：HomeBaseUnitView的子类
     返回nil：不创建此模块
     如果此函数不满足调用者的判断逻辑，可以在此调用者中复写此函数，重写内部判断逻辑即可
     */
    class func shouldReturnNewView(_ data: JSON,dataKey: String)-> [String:AnyObject]?
    {
        if ["event","ad","subject","grid","carousel","flash_sale"].contains(dataKey)
        {
            if let array = data["children"].array
            {
                return array.count > 0 ? data.dictionaryObject : nil
            }
            
            return nil
        }
        
        return data.dictionaryObject
    }
    
    
    ///子类需要重写
    func setupUI()
    {
        fatalError("方法setupUI()必须在子类中被重写")
    }
    
    
    ///刷新数据
    func refreshData()
    {
        fatalError("方法refreshData()必须在子类中被重写")
    }
}


protocol HomeBaseUnitViewDelegate:NSObjectProtocol {
    
    //点击某个板块的动作
    func  homeUnitViewTouchOnClick(_ operationHandler:((_ homeVC:HomeViewControllerV1)->Void))
}
