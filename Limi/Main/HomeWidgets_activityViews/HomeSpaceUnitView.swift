//
//  HomeSpaceUnitView.swift
//  Limi
//
//  Created by Richie on 16/2/23.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//每个模块之间的间距

import UIKit

class HomeSpaceUnitView: HomeBaseUnitView {

    /*
    "attributes": {
    "height": "13"
    }
    */
    
    override func setupUI() {
        
        self.iwidth = Constant.ScreenSize.SCREEN_WIDTH
        
        let scal:CGFloat = self.iwidth/960
        
        self.iheight = 0
        
        if let height =  JSON(self.unitData as AnyObject)["attributes"]["height"].string
        {
            self.iheight = CGFloat(NSString(string:height).intValue) * scal
        }
        
        self.selfHeight = self.iheight
        self.backgroundColor = UIColor(rgba: "#eeeeee")
    }
}
