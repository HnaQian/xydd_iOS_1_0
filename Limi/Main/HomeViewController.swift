//
//  HomeViewControllerV1.swift
//  Limi
//
//  Created by guo chen on 15/12/22.
//  Copyright © 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//首页

import UIKit
import CoreData

class HomeViewControllerV1: BaseViewController,UIScrollViewDelegate,QYConversationManagerDelegate {
    fileprivate static let articleListKey = "homeArticleListKey"
    //首页布局数据
    fileprivate var pageSource:[JSON]!
    //礼盒攻略
    fileprivate var packageArticleAry = [HomeStyle1ArticleModel]()
    //攻略数组：普通攻略+达人攻略
    fileprivate var commonArticleAry = [HomeCommonArticleModel]()
    //攻略数组：普通攻略+达人攻略+礼盒攻略
    fileprivate var allArticleAry = [HomCellBaseDataModel]()
    
    //头部活动视图（除了列表之外的所有模块的父视图）
    fileprivate let activityView = HomeActivityView()
    //rightTableView
    fileprivate let rightTableView = UITableView()
    
    //是否显示弹窗的标记
    fileprivate var dialogShouldShow = false
    
    //弹窗广告
    fileprivate var dialogView:HomeDialog = {return HomeDialog()}()
    fileprivate var isCheckingDialog:Bool{return _isCheckingDialog}
    fileprivate var _isCheckingDialog = false
    fileprivate let loginDialog = LoginDialog()
   
    fileprivate var pageScrollView = LazyPageScrollView(verticalDistance: 2, tabItemType: TabItemType.customView)
    fileprivate let leftScrollView = UIScrollView()
    fileprivate var isFirstIn = true
    
    
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(animated)
        
        UserDefaults.standard.set(3, forKey: "come_from")
        UserDefaults.standard.synchronize()
        
        
        if (GeTuiSdk.status() == SdkStatusStoped) {
            let delegate = UIApplication.shared.delegate as! AppDelegate
            GeTuiSdk.start(withAppId: Constant.kGtAppId, appKey: Constant.kGtAppKey, appSecret: Constant.kGtAppSecret, delegate: delegate);
            NSLog("\n\n>>>[GeTui]:%@\n\n","启动APP");
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if self.dialogShouldShow && self.dialogView.adImage != nil{
            self.dialogView.show()
            self.dialogShouldShow = false
        }
        
        if !UserInfoManager.didLogin{
            //检测是否第一次打开首页
            if UserDefaults.standard.string(forKey: "first_start") == nil{
                UserDefaults.standard.setValue("first_start", forKey: "first_start")
                UserDefaults.standard.setValue(true, forKey: "is_reg")
                UserDefaults.standard.synchronize()
            }
        }
        
        if let url = UserDefaults.standard.url(forKey: "appRouteMLink"){
            let parserUrl = EventDispatcher.preParserURLWithNotInMap(url)
            if parserUrl.host == "coupon"{
                if UserInfoManager.didLogin == true{
                    EventDispatcher.dispatch("\(url)", onNavigationController: self.navigationController)
                    UserDefaults.standard.removeObject(forKey: "appRouteMLink")
                    UserDefaults.standard.synchronize()
                }
                else{
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.loginType = Constant.InLoginType.couponType.rawValue
                    self.loginDialog.controller = self
                    self.loginDialog.show()
                }
            }
            else if parserUrl.host == "package_item"{
                EventDispatcher.dispatch("\(url)", onNavigationController: self.navigationController)
                UserDefaults.standard.removeObject(forKey: "appRouteMLink")
                UserDefaults.standard.synchronize()
            }
            else if parserUrl.host == "package_guess"{
                EventDispatcher.dispatch("\(url)", onNavigationController: self.navigationController)
                UserDefaults.standard.removeObject(forKey: "appRouteMLink")
                UserDefaults.standard.synchronize()
            }
            else{
                EventDispatcher.dispatch("\(url)", onNavigationController: self.navigationController)
                UserDefaults.standard.removeObject(forKey: "appRouteMLink")
                UserDefaults.standard.synchronize()
            }
        }
        else{
            self.checkDialog()
        }
    }
    
    override func viewDidLoad(){

        super.viewDidLoad()
        
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.title = "心意点点"
        self.gesturBackEnable = false
        
        let conversation = QYSDK.shared().conversationManager()
        Constant.UNREAD = (conversation?.allUnreadCount())!
       
        conversation?.setDelegate(self)
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: Constant.notification_logout_success), object: nil, queue: OperationQueue.main) { (notification) -> Void in
            self.loadPageData()
        }
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: Constant.notification_login_success), object: nil, queue: OperationQueue.main) { (notification) -> Void in
            self.loadPageData()
        }
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: Constant.flash_saleDataReload), object: nil, queue: OperationQueue.main) { (notification) -> Void in
            self.loadPageData()
        }
        
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: Constant.APP_LCA.WillResignActive), object: nil, queue: OperationQueue.main) { (notification) -> Void in
            
            //存储布局数据
            if let homeDB = CoreDataManager.shared.getDBManager(Constant.CoreDataType.homeAAGiftChoiceType),
                let db = homeDB as? HomeAAGiftChoice{
                var ary = [AnyObject]()
                
                //把首页布局数据存入数据库
                if self.pageSource != nil{
                    let _ = self.pageSource.map{ary.append($0.dictionaryObject! as AnyObject)}
                }
                
                //把攻略列表存入数据库 只存前10条数据
                var tempArticleList = [AnyObject]()
                for index in 0..<(self.commonArticleAry.count >= 10 ? 10 : self.commonArticleAry.count){
                    tempArticleList.append(self.commonArticleAry[index].ORIGINAL_DATA! as AnyObject)
                }
                ary.append(["name":HomeViewControllerV1.articleListKey,"list":tempArticleList] as AnyObject)
                
                db.homeLayout = ary as NSObject?
                CoreDataManager.shared.save()
            }
        }
        DevelopTestManager.refreshDevelopeTipe()
        self.setupUI()
        
        self.checkScanningRule()
    }
   
    //获取上传统计规则
    func checkScanningRule() {
        //第一次请求
        let goods_rule = UserDefaults.standard.object(forKey: "goods_rule")
        let article_rule = UserDefaults.standard.object(forKey: "article_rule")
        if goods_rule == nil || article_rule == nil {
            let _ = LMRequestManager.requestByDataAnalyses(context:self.view, URLString: Constant.JTAPI.submitScanning,isNeedUserTokrn :true,successHandler: {data, status, msg in
                if status != 200 {return}
                if let tempGoodsRule = JSON(data as AnyObject)["data"]["request_rule"].arrayObject {
                    if tempGoodsRule.count >= 2 {
                         UserDefaults.standard.set(tempGoodsRule[0].object(forKey: "requetst_frequency"), forKey: "goods_rule")
                         UserDefaults.standard.set(tempGoodsRule[1].object(forKey: "requetst_frequency"), forKey: "article_rule")
                        UserDefaults.standard.synchronize()
                    }
                   
                }
            })
            return
        }
        
        //打开App上传的情况
        if (goods_rule as! Int) == 4 {
            if let temp = UserDefaults.standard.array(forKey: "goods_scanning") {
                if temp.count > 0 {
                    var params = [String:AnyObject]()
                    params["type"] = 1 as AnyObject?
                    params["requetst_frequency"] = goods_rule as AnyObject?
                    let jsonData = try! JSONSerialization.data(withJSONObject: temp, options: JSONSerialization.WritingOptions.prettyPrinted)
                    let jsonString: String = String(data: jsonData, encoding: String.Encoding.utf8)!
                    params["request_json"] = jsonString as AnyObject?
                    let _ = LMRequestManager.requestByDataAnalyses(context:self.view, URLString: Constant.JTAPI.submitScanning,parameters: params,isNeedUserTokrn :true,successHandler: {data, status, msg in
                        if status != 200 {return}
                        if let tempGoodsRule = JSON(data as AnyObject)["data"]["request_rule"].arrayObject {
                            if tempGoodsRule.count >= 2 {
                                UserDefaults.standard.set(tempGoodsRule[0].object(forKey: "requetst_frequency"), forKey: "goods_rule")
                                UserDefaults.standard.set(tempGoodsRule[1].object(forKey: "requetst_frequency"), forKey: "article_rule")
                                UserDefaults.standard.synchronize()
                            }
                            UserDefaults.standard.removeObject(forKey: "goods_scanning")
                            
                        }
                    })
                }
                
            }
        }
        
        if (article_rule as! Int) == 4 {
            if let temp = UserDefaults.standard.array(forKey: "article_scanning") {
                if temp.count > 0 {
                    var params = [String:AnyObject]()
                    params["type"] = 1 as AnyObject?
                    params["requetst_frequency"] = goods_rule as AnyObject?
                    
                    let jsonData = try! JSONSerialization.data(withJSONObject: temp, options: JSONSerialization.WritingOptions.prettyPrinted)
                    let jsonString: String = String(data: jsonData, encoding: String.Encoding.utf8)!
                    params["request_json"] = jsonString as AnyObject?
                    let _ = LMRequestManager.requestByDataAnalyses(context:self.view, URLString: Constant.JTAPI.submitScanning,parameters: params,isNeedUserTokrn :true,successHandler: {data, status, msg in
                        if status != 200 {return}
                        if let tempGoodsRule = JSON(data as AnyObject)["data"]["request_rule"].arrayObject {
                            if tempGoodsRule.count >= 2 {
                                UserDefaults.standard.set(tempGoodsRule[0].object(forKey: "requetst_frequency"), forKey: "goods_rule")
                                UserDefaults.standard.set(tempGoodsRule[1].object(forKey: "requetst_frequency"), forKey: "article_rule")
                                UserDefaults.standard.synchronize()
                            }
                            UserDefaults.standard.removeObject(forKey: "article_scanning")
                            
                        }
                    })

                }
            }
        }
    }
    
    fileprivate func setupUI(){
        
        self.view.addSubview(pageScrollView)
        
        var frame = self.view.bounds
        frame.origin.y = 0
        frame.size.height = frame.size.height - MARGIN_49 - MARGIN_64
        pageScrollView.segment(["精选","推荐"], views: [leftScrollView,rightTableView], frame: frame)
        pageScrollView.delegate = self
        
        
        leftScrollView.addSubview(activityView)
        leftScrollView.showsHorizontalScrollIndicator = false
        leftScrollView.showsVerticalScrollIndicator = false
        leftScrollView.addHeaderRefresh {
            [weak self] in
            self?.loadPageData(false)
        }
        
        rightTableView.separatorStyle = UITableViewCellSeparatorStyle.none
        rightTableView.delegate = self
        rightTableView.dataSource = self
        
        rightTableView.addHeaderRefresh {
            [weak self] in
            self?.loadArticleList(false)
        }
        
        rightTableView.addFooterRefresh {
            [weak self] in
            self?.loadArticleList(true)
        }
        
        rightTableView.register(HomeArticleTableViewCell.self, forCellReuseIdentifier: HomeArticleTableViewCell.reuseIdentifier)
        rightTableView.register(HomeArticleSpecialStyle1Cell.self, forCellReuseIdentifier: HomeArticleSpecialStyle1Cell.reuseIdentifier)
        rightTableView.register(HomeArticleSpecialStyle2Cell.self, forCellReuseIdentifier: HomeArticleSpecialStyle2Cell.reuseIdentifier)
        
        activityView.iheight = 20
        activityView.delegate = self
        
        //添加导航按钮
        self.navigationItem.rightBarButtonItem = CommonWidgets.NavigationRightBtn.newRightBtn(bgImgName: "search_searchIcon", target: self, action: #selector(HomeViewControllerV1.serarchBtnOnClick))
        
        //添加签到按钮
        self.navigationItem.leftBarButtonItem = CommonWidgets.NavigationRightBtn.newRightBtn(bgImgName: "dailysignIcon", target: self, action: #selector(HomeViewControllerV1.signupBtnOnClick))
        
        //添加title
        let titleLbl = CustomLabel.newLabel(17, autoFont: 0, textColor: UIColor.black, textAlignment: NSTextAlignment.center)
        titleLbl.frame = CGRect(x: 0, y: 0, width: 150, height: 44)
        titleLbl.text = "心意点点"
        self.navigationItem.titleView = titleLbl
        
        //控件构建完成之后填充数据
        self.leftScrollView.beginHeaderRefresh()
    }
    
    
    func onUnreadCountChanged(_ count:NSInteger){
      Constant.UNREAD = count
    }
    
    //MARK: 搜索
    @objc fileprivate func serarchBtnOnClick(){
        Statistics.count(Statistics.Home.home_search_click)

        let searchVC = AllSearchViewController.newSearchViewController(1)
        searchVC.hidesBottomBarWhenPushed = true
        self.navigationController!.pushViewController(searchVC, animated: true)
    }
    
    //MARK: 签到
    @objc fileprivate func signupBtnOnClick() {
        Statistics.count(Statistics.Home.home_sign_click)
       let dailySignVC = DailySignViewController()
        dailySignVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(dailySignVC, animated: true)
    }
    
    
    //MARK: 刷新攻略列表
    fileprivate func refreshArticleList(_ isLoadMore:Bool,articleAry:[HomeCommonArticleModel]){
        
        //如果是不是加载更多数据，把原来的数据清空 加入新数据
        if !isLoadMore { self.commonArticleAry.removeAll() }
        let _ = articleAry.map{self.commonArticleAry.append($0)}
        
        //每次都清空组合后的数据，然后重新组合
        self.allArticleAry.removeAll()
        let _ = self.commonArticleAry.map{self.allArticleAry.append($0)}
        
        //组合数据 把package数据 packageArticleAry 插入 allArticleAry中
        if self.packageArticleAry.count > 0 && self.allArticleAry.count > 0{
            for index in 0..<self.packageArticleAry.count{
                let model = self.packageArticleAry[index]
                let position = model.attributes!.position
                
                if position + index < self.allArticleAry.count{
                    print(position)
                    self.allArticleAry.insert(model, at: position + index)
                }else{
                    break
                }
            }
        }
        
        self.rightTableView.reloadData()
        self.rightTableView.closeAllRefresh()
    }
    
    
    //MARK: 刷新活动视图
    fileprivate func refreshActivityView(){
        //头部数据：
        if self.pageSource.count > 0 {
            if let tabArray = self.pageSource[0]["children"].array {
                var tempArray = [String]()
                for dic in tabArray {
                    if let tempString = dic["attributes"]["title"].string {
                        tempArray.append(tempString)
                    }
                }
                (pageScrollView.tabItem as! LMSegmentedControl).titles = tempArray
            }
            activityView.refreshWithData(self.pageSource)
        }
        self.leftScrollView.contentH = activityView.iheight
        //取出礼盒攻略
        //list /package
        var package:JSON?
        for dic in self.pageSource{
            if let name = dic["name"].string{
                if name == "package"{
                    package = dic
                    break
                }
            }
        }
        
        //礼盒数据装入数组
        if let mpackage = package{
            if let list = mpackage["list"].arrayObject{
                self.packageArticleAry.removeAll()
                for dic in list {
                    self.packageArticleAry.append(HomeStyle1ArticleModel(dic as! [String:AnyObject]))
                }
            }
        }
    }
    
}

//MARK: 网络请求
extension HomeViewControllerV1{
    
    //MARK: 请求布局数据
    fileprivate func loadPageData(_ showHud:Bool = false)
    {
        
      let _ = LMRequestManager.requestByDataAnalyses(context:self.view, URLString: Constant.JTAPI.home_page,isShowErrorStatuMsg:true,isNeedHud :showHud, successHandler: {data, status, msg in
            self.leftScrollView.endHeaderRefresh()
            if status != 200{return}
            if let dataDic: [String: AnyObject] = data["data"] as? [String: AnyObject]{
                if let dataSource = dataDic["layout"] as? [[String: AnyObject]]{
                    self.pageSource = JSON(dataSource as AnyObject).array
                }
            }
            self.refreshActivityView()
        }){[weak self] in self?.leftScrollView.endHeaderRefresh()}
    }

    //MARK: 请求攻略数据
    fileprivate func loadArticleList(_ isLoadMore:Bool){
        /*
         token	false	string	登录的token值，有必须传
         last_id	false	int	上一次列表的最后一个文章ID，不传默认为0
         size	false	int	不传默认为20
         */
        
        var param = [String:AnyObject]()
        param["size"] = 10 as AnyObject?
        param["code"] = "bestcolumn" as AnyObject?
        if isLoadMore {
            if let model = self.commonArticleAry.last{
                param["last_id"] = model.id as AnyObject?
            }
        }
        
        
       let _ = LMRequestManager.requestByDataAnalyses(context:self.view, URLString: Constant.JTAPI.article_list,parameters:param,isNeedUserTokrn:true,isShowErrorStatuMsg:true, successHandler: {data, status, msg in
            var articleAry = [HomeCommonArticleModel]()
            self.rightTableView.endHeaderRefresh()
            if status != 200{
                //登录Token过期，引导user重新登录
                if status == 400{
                    UserDefaults.standard.removeObject(forKey: "UserToken")
                    UserDefaults.standard.removeObject(forKey: "UserUid")
                    UserDefaults.standard.synchronize()
                    
                    let user = CoreDataManager.shared.getDBManager(Constant.CoreDataType.userInfoType) as? User
                    
                    CoreDataManager.shared.deleteEntity(user!)
                    
                    CoreDataManager.shared.save({Void in
                        },failureHandler: {Void in
                    })
                    
                    //清除送礼提醒数据
                    let homeAAGiftChoice = CoreDataManager.shared.getDBManager(Constant.CoreDataType.homeAAGiftChoiceType) as? HomeAAGiftChoice
                    
                    homeAAGiftChoice?.homeRmind = nil
                    
                    CoreDataManager.shared.save()
                    AccountManager.shared.removeParentReminder()//还原父母设置信息

                    //刷新数据
                    self.refreshArticleList(isLoadMore,articleAry: articleAry)
                    //提示
                    if msg != ""{
                        Utils.showError(context: UIApplication.shared.keyWindow!, errorStr: msg)
                    }
                    
                    self.loadArticleList(false)
                    //去登录
                    let login = LoginViewController()
                    login.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(login, animated: true)
                }
                
                self.rightTableView.closeAllRefresh();
                return
            }
            
            var isCommonArticle = true
            if let dataDic: [String: AnyObject] = data["data"] as? [String: AnyObject],
                let dataSource = dataDic["list"] as? [[String: AnyObject]]{
                if dataSource.count > 0{
                    for dic in dataSource{
                        
                        if let array = JSON(dic as AnyObject)["Goods_list"].arrayObject{
                            if array.count > 0{
                                isCommonArticle = false
                            }
                        }
                        
                        if isCommonArticle{
                            articleAry.append(HomeCommonArticleModel(dic))
                        }else{
                            articleAry.append(HomeStyle2ArticleModel(dic))
                        }
                        
                        isCommonArticle = true
                    }
                }
            }
            
            self.refreshArticleList(isLoadMore,articleAry: articleAry)
            
        }){[weak self] in self?.rightTableView.closeAllRefresh()}
    }
    
    
    
    //MARK: 检查是否弹出窗
    fileprivate func checkDialog(){
        
        //判断距离上次时间是否大于24小时
        if let lastDate =  UserDefaults.standard.object(forKey: "last_dialogTime") as? Date{
            if Date().timeIntervalSince(lastDate)/3600 < 24{
                return
            }
        }else{
            //存储当前时间
            UserDefaults.standard.setValue(Date(), forKey: "last_dialogTime")
        }
        
        if self.isCheckingDialog{return}
        
        _isCheckingDialog = true
        
        var param = [String:AnyObject]()
        
        var is_reg:Bool = false
        var last_id:Int = 0
        
        let did:Bool = UserDefaults.standard.bool(forKey: "is_reg")
        is_reg = did
        
        let id:Int = UserDefaults.standard.integer(forKey: "homeAd_lastId")
        last_id = id
        
        param["is_reg"] = is_reg as AnyObject?
        param["last_id"] = last_id as AnyObject?
        
        let _ = LMRequestManager.requestByDataAnalyses(context: self.view, URLString: Constant.JTAPI.pop_ad, parameters: param,successHandler: { (data, status, msg) in
            
            
            if status != 200{return}
            
//            let jsData = JSON(data as AnyObject)["data"].dictionaryObject
            
            if !is_reg{
                if let id = JSON(data as AnyObject)["data"]["ad"]["Id"].int{
                    UserDefaults.standard.setValue(id, forKey: "homeAd_lastId")
                    UserDefaults.standard.synchronize()
                }
            }else{
                UserDefaults.standard.setValue(false, forKey: "is_reg")
                UserDefaults.standard.synchronize()
            }
            
            if let sta = JSON(data as AnyObject)["data"]["ad_status"].int{
                
                if sta == 1{
                    
                    self.dialogView.delegate = self
                    
                    if let url = JSON(data as AnyObject)["data"]["ad"]["Url"].string{
                        self.dialogView.href = url
                    }
                    
                    self.dialogShouldShow = true
                    
                    if let imageName = JSON(data as AnyObject)["data"]["ad"]["Img"].string{
                        
                        self.dialogView.loadAdImage(imageName, loadSuccess: {
                            if self.view.window != nil && self.dialogView.adImage != nil{
                                self.dialogShouldShow = false
                                self.dialogView.show()
                            }
                        })
                    }
                }
            }
            
            self._isCheckingDialog = false
            
        }){Void in self._isCheckingDialog = false
            
        }
    }
}


//MARK: DELEGATE:  UITableViewDelegate, UITableViewDataSource, HomeBaseTableViewCellDelegate
extension HomeViewControllerV1:UITableViewDelegate,UITableViewDataSource,HomeBaseTableViewCellDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.allArticleAry.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.allArticleAry[(indexPath as NSIndexPath).row].getCellHeight()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:HomeBaseTableViewCell = tableView.dequeueReusableCell(withIdentifier: self.allArticleAry[(indexPath as NSIndexPath).row].classMap().reuseIdentifier) as! HomeBaseTableViewCell
        
        cell.refreshWithData(self.allArticleAry[(indexPath as NSIndexPath).row])
        cell.delegate = self
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = allArticleAry[(indexPath as NSIndexPath).row]
        
        
        
        switch model.getModelType() {

        case 1,2:
            
            //攻略
            if let mmodel = model as? HomeCommonArticleModel{
                
                //攻略达人
                if model.getModelType() == 2{
                    //home_article_master_click
                    Statistics.count(Statistics.Home.home_article_master_click, andAttributes: ["articleID":"\(mmodel.id)"])
                }
                
                Statistics.count(Statistics.Home.home_article_click, andAttributes: ["articleID":"\(mmodel.id)"])
                
                let tacticDetailVC = TacticDetailViewController()
                tacticDetailVC.article_id = mmodel.id
                tacticDetailVC.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(tacticDetailVC, animated: true)
            }
            break
        case 3:
            //自选礼盒
            if let mmodel = model as? HomeStyle1ArticleModel{
                
                
                if let href = mmodel.attributes?.href{
                    Statistics.count(Statistics.Home.home_article_box_click, andAttributes: ["href":href])
                    
                    EventDispatcher.dispatch(href, onNavigationController: self.navigationController)
                }
            }
            break
        default:
            break
        }
        
    }
    
    
    //攻略
    func homeTableViewCellSelected(_ articleID:Int){
        let webVC = WebViewController(URL: nil)
        webVC.itemId =  articleID
        webVC.type = Constant.WebViewType.tacticDetailType.rawValue
        webVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(webVC, animated: true)
    }
    
    //自选礼盒
    func homeTableViewCellPackageSelected(_ href: String) {
        EventDispatcher.dispatch(href, onNavigationController: self.navigationController)
    }
    
    //商品
    func homeTableViewCellGoodsSelected(_ goodsID:Int){
        let goodsdetail = GoodsDetailViewController.newGoodsDetailWithID(goodsID)
        goodsdetail.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(goodsdetail, animated: true)
    }
    
}


//MARK: DELEGATE: HomeTracticListUnitViewDelegate,HomeActivityViewDelegate,HomeDialogDelegate

extension HomeViewControllerV1:HomeActivityViewDelegate,HomeDialogDelegate,LazyPageScrollViewDelegate{
    
    //点击某个板块的动作
    func  homeActivityViewDidSelected(_ moduleSelectedHandler:((_ homeVC:HomeViewControllerV1)->Void)){
        moduleSelectedHandler(self)
    }
    
    //弹窗代理函数
    func homeDialogOnClick(_ type:Int){
        
        if type == 0{
            //去领取
            print("去领取")
            if self.dialogView.href != ""{
                Statistics.count(Statistics.Home.home_window_click, andAttributes: ["link":self.dialogView.href])
                EventDispatcher.dispatch(URL: URL(string: self.dialogView.href)!, onNavigationController: self.navigationController)
            }
            
        }else{
        
        }
        
    }
    
    //页面滑动代理
    func lazyPageScrollView(_ pageScrollView: LazyPageScrollView, changeIndex index: Int) {
        Statistics.count(Statistics.Home.home_tab_click)
        if index == 1 {
            if self.isFirstIn {
                self.rightTableView.beginHeaderRefresh()
                isFirstIn = false
            }
        }
    }
}

