//
//  LeaderBoardViewController.swift
//  Limi
//
//  Created by Richie on 16/5/16.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//心意榜单VC

import UIKit

class LeaderBoardViewController: BaseViewController {
    fileprivate var statueBarStyle:UIStatusBarStyle = .lightContent
    fileprivate let tableViwe = UITableView()
    fileprivate let headView = UIView()
    fileprivate var currentIndex = 1{didSet{
        self.tableViwe.reloadData()
        }}
    
    fileprivate let dataManager = LeaderBoardLoaddataManager()
    
    fileprivate var btnAry:[LeaderBoardButton] = []
    fileprivate let titleAry = ["INS热榜","总榜","跨境Buy"]
    fileprivate let imageAry = ["Leaderboard_emoution_gay-1","Leaderboard_hot","Leaderboard_workSpace"]
    fileprivate let disSelectedImageAry = ["Leaderboard_emoution_gay","Leaderboard_hot_gay","Leaderboard_workSpace_gay"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()
        
        self.view.backgroundColor = UIColor(rgba: Constant.common_C1_color)
        
        NotificationCenter.default.addObserver(self, selector: #selector(LeaderBoardViewController.goodAction(_:)), name: NSNotification.Name(rawValue: LeaderBoardTableViewCell.GoodActionNotification), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(LeaderBoardViewController.sendAction(_:)), name: NSNotification.Name(rawValue: LeaderBoardTableViewCell.SendActionNotification), object: nil)
        
        dataManager.delegate  = self
        dataManager.loadDataForIndex(currentIndex)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationBarBgImageAlpha(0)
        
        UIApplication.shared.statusBarStyle = statueBarStyle
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.statusBarStyle = .default
        navigationBarBgImageAlpha(1)
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    
    fileprivate func navigationBarBgImageAlpha(_ alpha:CGFloat){
        
        self.navigationController?.navigationBar.isTranslucent = true
        if alpha == 0{
            self.navigationController?.navigationBar.shadowImage = UIImage()
            self.navigationController?.navigationBar.lt_setBackgroundColor(UIColor.clear)
            self.navigationController?.navigationBar.lt_setElementsAlpha(0)
            self.navigationController?.navigationBar.barTintColor = UIColor.clear
            self.navigationItem.title = ""
        }else{
            
            self.navigationController?.navigationBar.lt_reset()
            self.navigationController?.navigationBar.lt_setElementsAlpha(1)
            self.navigationItem.title = ""
            
            let image = UIImage(named: "titlebar")
            self.navigationController?.navigationBar.setBackgroundImage(image, for: UIBarMetrics.default)
        }
        
    }
    
    
    
    @objc fileprivate func goodAction(_ notification:Notification){
        
        if !UserInfoManager.didLogin{
            self.jumpToLogin(Constant.InLoginType.defaultType, tipe: nil)
            return
        }
        
        if let userinfo = (notification as NSNotification).userInfo,
            let model = userinfo["model"] as? LeaderBoardModel{
            
            let url = model.collect_status ? Constant.JTAPI.collect_del : Constant.JTAPI.collect_add
            
            let _ = LMRequestManager.requestByDataAnalyses(HTTPMethod.post, context: self.view, URLString: url, parameters: ["collect_type":3 as AnyObject,"obj_id":(model.collect_status ? "\(model.id)" as AnyObject : [model.id] as AnyObject) ], isNeedUserTokrn: true, successHandler: { (data, status, msg) in
                
                if status == 200{
                    if model.indexPath.row < self.dataManager.dataForIndex(self.currentIndex).count{
                        
                        self.dataManager.dataForIndex(self.currentIndex)[model.indexPath.row].collect_status = !model.collect_status
                        self.dataManager.dataForIndex(self.currentIndex)[model.indexPath.row].collect_num = model.collect_num + (!model.collect_status ? -1 : 1)
                        
                        self.dataManager.dataForIndex(self.currentIndex)[model.indexPath.row].goodBtnShouldAnimated = true
                        
                        self.tableViwe.reloadRows(at: [model.indexPath], with: UITableViewRowAnimation.none)
                    }
                    
                }
                
                }, failureHandler: {
                    
            })
        }
    }
    
    
    @objc fileprivate func sendAction(_ notification:Notification){
        
        if let userinfo = (notification as NSNotification).userInfo,
            let model = userinfo["model"] as? LeaderBoardModel,
            let mgoods = model.goods{
            EventDispatcher.dispatch("local://goods_item", params: ["id": mgoods.gid as AnyObject], onNavigationController: self.navigationController)
        }
    }
    
    
    fileprivate func setupUI(){
        
        self.backButton.setImage(UIImage(named: "Leaderboard_white_backBtnIcon"), for: UIControlState())
        self.backButton.setBackgroundImage(nil, for: UIControlState())
        
        self.view.addSubview(tableViwe)
        tableViwe.frame  = self.view.bounds
        
        tableViwe.showsVerticalScrollIndicator = false
        
        tableViwe.separatorStyle = UITableViewCellSeparatorStyle.none
        
        tableViwe.backgroundColor = UIColor(rgba:Constant.common_C1_color)
        
        tableViwe.addHeaderRefresh {[weak self] in
            self?.dataManager.loadDataForIndex((self?.currentIndex)!)
        }
        
        tableViwe.delegate = self
        tableViwe.dataSource = self
        
        tableViwe.register(LeaderBoardTableViewCell.self, forCellReuseIdentifier: LeaderBoardTableViewCell.reuseIdentifier)
        tableViwe.register(LaederBoardLastCell.self, forCellReuseIdentifier: LaederBoardLastCell.reuseIdentifier)
        
        self.setupHeaderViewUI()
    }
    
    
    
    fileprivate func setupHeaderViewUI(){
        
        
        let headImg_height:CGFloat = 236 * Constant.ScreenSize.SCREEN_WIDTH_SCALE
        let seg_height:CGFloat = 130 * Constant.ScreenSize.SCREEN_WIDTH_SCALE
        
        let gap:CGFloat = 20 * Constant.ScreenSize.SCREEN_WIDTH_SCALE
        
        let btn_width:CGFloat = Constant.ScreenSize.SCREEN_WIDTH_SCALE * 60
        let btn_gap:CGFloat = (Constant.ScreenSize.SCREEN_WIDTH - btn_width * 3 - gap * 2)/4
        
        headView.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: headImg_height + seg_height)
        
        
        let whitBgView = UIView(frame: CGRect(x: gap,y: 0,width: headView.iwidth - gap * 2,height: headView.iheight))
        whitBgView.backgroundColor = UIColor.white
        headView.addSubview(whitBgView)
        
        
        let headerImage = UIImageView(image: UIImage(named: "Leaderboard_banner"))
        
        headerImage.contentMode = UIViewContentMode.scaleAspectFill
        
        headerImage.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: headImg_height)
        headView.addSubview(headerImage)
        
        
        for index in 0...2{
            
            let btn = LeaderBoardButton(title: titleAry[index], imageNormal: disSelectedImageAry[index], imageSelecte: imageAry[index])
            
            if index == currentIndex{btn.didSelected = true}
            
            btn.tag = 100 + index
            
            btn.addTarget(self, action: #selector(LeaderBoardViewController.btnAction(_:)), for: UIControlEvents.touchUpInside)
            
            btn.frame = CGRect(x: gap + btn_gap + CGFloat(index) * (btn_gap + btn_width), y: headerImage.ibottom + 26 * Constant.ScreenSize.SCREEN_WIDTH_SCALE ,width: btn_width, height: btn_width + 20)
            
            btnAry.append(btn)
            headView.addSubview(btn)
        }
        
        headView.backgroundColor = UIColor(rgba: Constant.common_C1_color)
        tableViwe.tableHeaderView = headView
    }
    
    
    
    @objc fileprivate func btnAction(_ btn:LeaderBoardButton){
        
        currentIndex = btn.tag - 100
        
        for mbtn in btnAry{
            mbtn.didSelected = mbtn == btn
        }
    }
    
    
    deinit{
        
        for name in [LeaderBoardTableViewCell.SendActionNotification,LeaderBoardTableViewCell.GoodActionNotification]{
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: name), object: nil)
        }
    }
}


extension LeaderBoardViewController:UITableViewDelegate,UITableViewDataSource,LeaderBoardLoaddataManagerDelegate{
    
    func leaderBoardLoaddataManagerDidLoadFinish(_ index: Int, data: [LeaderBoardModel]) {
        tableViwe.endHeaderRefresh()
        if currentIndex == index{
            self.tableViwe.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataManager.dataForIndex(currentIndex).count + 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (indexPath as NSIndexPath).row == dataManager.dataForIndex(currentIndex).count{
            let cell:LaederBoardLastCell = tableView.dequeueReusableCell(withIdentifier: LaederBoardLastCell.reuseIdentifier) as! LaederBoardLastCell
            return cell
        }
        
        
        let cell:LeaderBoardTableViewCell = tableView.dequeueReusableCell(withIdentifier: LeaderBoardTableViewCell.reuseIdentifier) as! LeaderBoardTableViewCell
        
        cell.refreshData(dataManager.dataForIndex(currentIndex)[(indexPath as NSIndexPath).row],forIndexPath: indexPath)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (indexPath as NSIndexPath).row == dataManager.dataForIndex(currentIndex).count{
            return LaederBoardLastCell.cellHeight
        }
        
        return LeaderBoardTableViewCell.cellHeight((indexPath as NSIndexPath).row)
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let mgoods = dataManager.dataForIndex(currentIndex)[(indexPath as NSIndexPath).row].goods{
            
            EventDispatcher.dispatch("local://goods_item", params: ["id": mgoods.gid as AnyObject], onNavigationController: self.navigationController)
        }
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //        navigationBarBgImageAlpha(scrollView.contentOffset.y/scrollView.iheight)
        changeTitleColor(scrollView)
    }
    
    
    func changeTitleColor(_ scrollView:UIScrollView) {
        let color : UIColor = UIColor(colorLiteralRed: 252/255.0, green: 252/255.0, blue: 252/255.0, alpha: 1)
        let offsetY : CGFloat = scrollView.contentOffset.y
        
        if (offsetY > 0) {
            
            
            let alpha : CGFloat = min(1, scrollView.contentOffset.y/Constant.NAVBAR_CHANGE_POINT)
            self.navigationItem.title = "排行榜"
            self.navigationController?.navigationBar.lt_setBackgroundColor(color.withAlphaComponent(alpha))
            self.navigationController?.navigationBar.lt_setElementsAlpha(alpha)
            
        } else {
            self.navigationController?.navigationBar.lt_setBackgroundColor(color.withAlphaComponent(0))
            self.navigationController?.navigationBar.lt_setElementsAlpha(0)
        }
        
        
        if scrollView.contentOffset.y <= 60{
            UIApplication.shared.statusBarStyle = .lightContent
            statueBarStyle = .lightContent
        }else{
            UIApplication.shared.statusBarStyle = .default
            statueBarStyle = .default
        }
        
        
        let during:CGFloat = 100
        
        if (offsetY < during){
            
            self.backButton.setImage(UIImage(named: "Leaderboard_white_backBtnIcon"), for: UIControlState())
            self.backButton.alpha = (during - offsetY)/during
        }else{
            self.backButton.setImage(UIImage(named: "common_back_icon"), for: UIControlState())
            self.backButton.alpha = (offsetY - during)/during
        }
    }
}


extension LeaderBoardViewController{
    
    
    class LeaderBoardButton:UIButton{
        
        var didSelected = false{didSet{
            
            mtitleLabl.textColor = didSelected ? UIColor(rgba:Constant.common_C1_color) : UIColor(rgba:Constant.common_C8_color)
            
            mimageView.image = UIImage(named: didSelected ? imageSelecte : imageNormal)
            
            }}
        
        fileprivate var imageNormal:String = ""
        fileprivate var imageSelecte:String = ""
        
        init(title:String,imageNormal:String,imageSelecte:String){
            super.init(frame:CGRect.zero)
            
            mtitleLabl.text = title
            mimageView.image = UIImage(named: imageNormal)
            
            self.imageNormal = imageNormal
            self.imageSelecte = imageSelecte
            
            self.setupUI()
        }
        
        
        fileprivate let mimageView = UIImageView()
        fileprivate let mtitleLabl = UILabel()
        
        fileprivate func setupUI(){
            mimageView.isUserInteractionEnabled = false
            mimageView.contentMode = UIViewContentMode.scaleAspectFill
            self.addSubview(mimageView)
            
            self.addSubview(mtitleLabl)
            mtitleLabl.font = UIFont.systemFont(ofSize: Constant.common_F5_font)
            mtitleLabl.textColor = UIColor(rgba:Constant.common_C8_color)
            mtitleLabl.textAlignment = NSTextAlignment.center
            
            mimageView.snp_makeConstraints { (make) in
                let _ = make.top.equalTo(self)
                let _ = make.left.equalTo(self)
                let _ = make.right.equalTo(self)
                let _ = make.height.equalTo(mimageView.snp_width)
            }
            
            mtitleLabl.snp_makeConstraints { (make) in
                let _ = make.top.equalTo(mimageView.snp_bottom)
                //                make.left.equalTo(self)
                //                make.right.equalTo(self)
                let _ = make.centerX.equalTo(self)
                let _ = make.bottom.equalTo(self)
            }
        }
        
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}



class LeaderBoardLoaddataManager:NSObject{
    
    
    fileprivate var allData:[Int:[LeaderBoardModel]] = [0:[LeaderBoardModel]()]
    
    fileprivate let tempData:[LeaderBoardModel] = {
        var ary = [LeaderBoardModel]()
        for index in 0...9{
            ary.append(LeaderBoardModel())
        }
        return ary
    }()
    
    weak var delegate:LeaderBoardLoaddataManagerDelegate?
    
    fileprivate var refreshStatusAry = [false,false,false]
    
    
    func dataForIndex(_ index:Int) ->[LeaderBoardModel]{
        
        if let ary = allData[index]{
            if ary.count > 0{
                return ary
            }
        }
        
        self.loadDataForIndex(index)
        
        return tempData
    }
    
    
    func loadDataForIndex(_ index:Int) {
        self.fetchData(index)
    }
    
    
    
    fileprivate func fetchData(_ index:Int){
        
        if index >= 3{return}
        if refreshStatusAry[index]{return}
        
        refreshStatusAry[index] = true
        let _ = LMRequestManager.requestByDataAnalyses(HTTPMethod.post, context: UIApplication.shared.keyWindow!, URLString: Constant.JTAPI.top_goods_list, parameters: ["type":(index + 1) as AnyObject], isNeedUserTokrn: true, successHandler: { (data, status, msg) in
            self.refreshStatusAry[index] = false
            if status != 200{return}
            var tempAry = [LeaderBoardModel]()
            
            if let list = JSON(data as AnyObject)["data"]["goods_list"].arrayObject{
                if list.count > 0{
                    for dic in list{
                        tempAry.append(LeaderBoardModel(dic as! [String:AnyObject]))
                    }
                }
            }
            
            self.allData[index] = tempAry
            self.delegate?.leaderBoardLoaddataManagerDidLoadFinish(index, data: tempAry)
            
        }) {
            self.refreshStatusAry[index] = false
        }
    }
}


protocol LeaderBoardLoaddataManagerDelegate:NSObjectProtocol {
    func leaderBoardLoaddataManagerDidLoadFinish(_ index:Int,data:[LeaderBoardModel])
}
