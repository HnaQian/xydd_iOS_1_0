//
//  HeaderOfOrderDetail.swift
//  Limi
//
//  Created by Richie on 16/3/23.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class HeaderOfOrderDetail: BaseUnitView {
    
    fileprivate let unitOrderNumber = UnitView(title: "订单号")
    
    fileprivate var receiveAddressView:ReceiveAddressView!{
        if _receiveAddressView == nil{_receiveAddressView = ReceiveAddressView()}
        return _receiveAddressView}
    
    fileprivate var _receiveAddressView:ReceiveAddressView!
    
    fileprivate var tariffeAddressView:TariffeAddressView!{
        if _tariffeAddressView == nil {
            _tariffeAddressView = TariffeAddressView()
        }
        return _tariffeAddressView
    }
    
    fileprivate var _tariffeAddressView:TariffeAddressView!
    fileprivate var orderDetailModel:OrderDetailModel?
    
    
    init(detailModel:OrderDetailModel?) {
        super.init(frame: CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: 0))
        
        self.orderDetailModel = detailModel
        setupUI(detailModel)
    }
    
    
    //MARK: headerUI
    func setupUI(_ orderDetailModel:OrderDetailModel?){
        
        self.addSubview(unitOrderNumber)
        self.addSubview(receiveAddressView)
        self.addSubview(tariffeAddressView)
        
        unitOrderNumber.setContentLblAttrubutes(color:UIColor(rgba: Constant.common_C8_color))
        
        unitOrderNumber.snp_makeConstraints { (make) -> Void in
            let _ = make.left.equalTo(self)
            let _ = make.right.equalTo(self)
            let _ = make.top.equalTo(self)
            let _ = make.height.equalTo(unitOrderNumber.iheight)
        }
        
        receiveAddressView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(unitOrderNumber.snp_bottom).offset(MARGIN_8)
            let _ = make.left.equalTo(self)
            let _ = make.right.equalTo(self)
            let _ = make.height.equalTo(receiveAddressView.iheight)
        }
        
        tariffeAddressView.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(receiveAddressView.snp_bottom)
            let _ = make.left.equalTo(self)
            let _ = make.right.equalTo(self)
            let _ = make.height.equalTo(tariffeAddressView.iheight)
        }
        
        self.iheight = unitOrderNumber.iheight + receiveAddressView.iheight + MARGIN_8 * 2
        
        if orderDetailModel != nil{
            self.setProperty(orderDetailModel!)
        }
    }
    

    fileprivate var layoutChangedHandler:(()->Void)?
    
   
    
    func setProperty(_ orderDetailModel:OrderDetailModel)
    {
        unitOrderNumber.title = "订单号: \(orderDetailModel.oid)"
        
        unitOrderNumber.content = orderDetailModel.add_time
        
        if orderDetailModel.isHaveAddress
        {
            let addressModel = AddressMode()
            addressModel.name = orderDetailModel.receiver_name
            addressModel.mobile = orderDetailModel.receiver_mobile
            addressModel.address = orderDetailModel.address
            addressModel.province = orderDetailModel.province_name
            addressModel.city = orderDetailModel.city_name
            addressModel.area = orderDetailModel.area_name
            
            receiveAddressView.reloadData(addressModel)
        }else{
            receiveAddressView.snp_updateConstraints(closure: { (make) -> Void in
                let _ = make.height.equalTo(0)
            })
            
            self.iheight  = self.iheight - (receiveAddressView.iheight + MARGIN_8 * 2)
        }
        
        if orderDetailModel.order_type == 2 {
            tariffeAddressView.isHidden = false
            receiveAddressView.isHidden = true
            
            let tariffeAddressMode = TariffeAddressMode()
            tariffeAddressMode.rechargeNumber = orderDetailModel.receiver_mobile
            tariffeAddressMode.belongAddress = orderDetailModel.order_remark
            tariffeAddressView.reloadData(tariffeAddressMode)
            
            self.iheight = self.iheight + tariffeAddressView.iheight + MARGIN_8 * 2
            
            receiveAddressView.snp_updateConstraints(closure: { (make) in
                let _ = make.height.equalTo(0)
            })
        }else {
            tariffeAddressView.isHidden = true
            receiveAddressView.isHidden = false
        }
    }
    
    
    
    //设置数据
    func setData(_ dataManager:OrderDataManager,layoutChangedHandler:@escaping (()->Void)){
        
        reloadData(dataManager, layoutChangedHandler: layoutChangedHandler)
    }
    
    
    //刷新数据
    func reloadData(_ dataManager:OrderDataManager,layoutChangedHandler:(()->Void)?){
        //Layout
        if layoutChangedHandler != nil{
            self.layoutChangedHandler = layoutChangedHandler
        }
        
        self.iwidth = Constant.ScreenSize.SCREEN_WIDTH
        
        if self.layoutChangedHandler != nil{
            self.layoutChangedHandler!()
        }
    }
    
    
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
