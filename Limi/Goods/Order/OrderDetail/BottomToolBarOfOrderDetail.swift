//
//  BottomToolBarOfOrderDetail.swift
//  Limi
//
//  Created by Richie on 16/3/23.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class BottomToolBarOfOrderDetail: BaseUnitView {
    
    /*
     // Only override drawRect: if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func drawRect(rect: CGRect) {
     // Drawing code
     }
     */
    
    init()
    {
        super.init(frame:CGRect.zero)
        
        self.customInit()
    }
    
    
    override func customInit() {
        self.backgroundColor = UIColor.white
        
        self.edgLineModes = [EdgLineMode.topLongLine]
    }
    
    
    enum ActionType:String{
        case Pay = "去支付"
        case Cancle = "取消订单"
        case RePay = "再次购买"
        case Closed = "该订单已关闭"
    }
    
    fileprivate var buttons = [LMButton]()
    fileprivate var currentStatus = 10086
    var consultButton = CustomConsultButton()
    
    fileprivate var _delegate:BottomToolBarOfOrderDetailDelegate?{return (self.delegate as? BottomToolBarOfOrderDetailDelegate)}
    
    
    func refreshUI(_ orderDetailModel:OrderDetailModel){
        
        //        if orderDetailModel.order_type == 2{return}
        
        currentStatus = orderDetailModel.order_status
        
        if currentStatus < 1 || currentStatus > 6{return}
        
        
        let _ = buttons.map{$0.removeFromSuperview()}
        buttons = [LMButton]()
        
        /*
         订单状态：普通订单：1待付款 2待发货 3待收货 4已完成 5售后中 6已关闭；
         话费订单：1待付款 2待充值 4已完成 6已关闭
         */
        
        var buttonTypes = [LMButtonType]()
        var actionTypes = [ActionType]()
        
        switch currentStatus{
        case 1:
            buttonTypes = [LMButtonType.titleWhite_bgRed,LMButtonType.title_bord_red]
            actionTypes = [ActionType.Pay,ActionType.Cancle]
        case 2,3,4,5:
            if orderDetailModel.order_type == 1 {
                buttonTypes = [LMButtonType.titleWhite_bgRed]
                actionTypes = [ActionType.RePay]
            }
           break
        case 6:
            buttonTypes = [LMButtonType.title_gray]
            actionTypes = [ActionType.Closed]
            
        default :break
        }
        self.addSubview(consultButton)
        consultButton.itop = 4
        consultButton.addTarget(self, action: #selector(BottomToolBarOfOrderDetail.consultButtonClicked), for: .touchUpInside)
        let btn_width:CGFloat = (Constant.ScreenSize.SCREEN_WIDTH - MARGIN_8 * 4)/3
        
        var tempBtn:LMButton!
        
        //从右到左一次排列
        for index in 0 ..< buttonTypes.count
        {
            let btn = LMButton(type: buttonTypes[index])
            
            btn.title = actionTypes[index].rawValue
            
            btn.indetifer = actionTypes[index].rawValue
            
            btn.addTarget(self, action: #selector(BottomToolBarOfOrderDetail.buttonAction(_:)), for: UIControlEvents.touchUpInside)
            
            self.addSubview(btn)
            
            btn.snp_makeConstraints(closure: { (make) -> Void in
                
                let _ = make.top.equalTo(self).offset(MARGIN_8)
                
                let _ = make.bottom.equalTo(self).offset(-MARGIN_8)
                
                let _ = make.width.equalTo(btn_width)
                
                if self.currentStatus == 6 {
                    let _ = make.centerX.equalTo(self)
                }else {
                    if tempBtn == nil{
                        let _ = make.right.equalTo(self).offset(-MARGIN_8)
                    }
                    else{
                        let _ = make.right.equalTo(tempBtn.snp_left).offset(-MARGIN_8)
                    }
                }
            })
           
            tempBtn = btn
            
            buttons.append(btn)
        }
    }
    
    func consultButtonClicked() {
        if let dele = _delegate {
            dele.toolOfOrderDetailOnClickContanctBtn()
        }
    }
    
    func buttonAction(_ btn:LMButton)
    {
        if let dele = _delegate{
            
            if btn.indetifer == ActionType.Pay.rawValue
            {
                dele.toolOfOrderDetailOnClickPayBtn()
            }
            else if btn.indetifer == ActionType.Cancle.rawValue
            {
                dele.toolOfOrderDetailOnClickCancleBtn()
            }
            else if btn.indetifer == ActionType.RePay.rawValue
            {
                dele.toolOfOrderDetailOnClickRepayBtn()
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


protocol BottomToolBarOfOrderDetailDelegate:OrderUnitViewDelegate{
    
    /**
     取消订单
     */
    func toolOfOrderDetailOnClickCancleBtn()
    
    /**
     支付订单
     */
    func toolOfOrderDetailOnClickPayBtn()
    
    /**
     联系客服
     */
    func toolOfOrderDetailOnClickContanctBtn()
    
    /**
     再次购买
     */
    func toolOfOrderDetailOnClickRepayBtn()
}
