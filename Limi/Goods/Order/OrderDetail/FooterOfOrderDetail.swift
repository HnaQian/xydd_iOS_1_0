//
//  FooterOfOrderDetail.swift
//  Limi
//
//  Created by Richie on 16/3/23.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class FooterOfOrderDetail: BaseUnitView,OrderUnitViewDelegate,UnitViewDelegate {
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
    // Drawing code
    }
    */
    fileprivate let unitCoupon = UnitView(title:"红包")
    fileprivate let unitPayType = PayTypeUnitView(title:"支付方式")
    fileprivate let unitWishSend = UnitView(true,title:"心意服务")
    fileprivate let unitReceiptContent = UnitView(title:"发票内容")
    fileprivate let unitRemarkContent = UnitView(title:"留言:")
    fileprivate let unitTotalMoney = UnitView(title:"共0件")
    fileprivate let expensesSummaryView = ExpensesSummaryView()
    
    fileprivate var orderDetailModel:OrderDetailModel?
    
    init(detailModel:OrderDetailModel?) {
        super.init(frame: CGRect.zero)
        self.orderDetailModel = detailModel
        self.setupUI(detailModel)
    }
    
    
    func setupUI(_ orderDetailModel:OrderDetailModel?)
    {
        let _ = [unitRemarkContent,unitCoupon,unitPayType,unitWishSend,unitReceiptContent,expensesSummaryView,unitTotalMoney].map{self.addSubview($0)}
        
        unitRemarkContent.setTitleLblAttrubutes("留言:", font: 0, color: UIColor(rgba: Constant.common_C8_color))
        unitRemarkContent.setContentLblAttrubutes("", font: 0, color: UIColor(rgba: Constant.common_C2_color), textAlignment: NSTextAlignment.left)
        unitRemarkContent.contentLbl.snp_updateConstraints { (make) in
            let _ = make.left.equalTo(unitRemarkContent.titleLbl.snp_right).offset(MARGIN_4)
        }
        unitRemarkContent.edgLineModes = [EdgLineMode.topShortLine]
        
        unitCoupon.edgLineModes = [EdgLineMode.topLongLine,EdgLineMode.bottomShortLine]
        unitPayType.edgLineModes = [EdgLineMode.bottomShortLine]
        unitWishSend.edgLineModes = [EdgLineMode.bottomLongLine]
        
        unitReceiptContent.edgLineModes = [EdgLineMode.bottomLongLine]
        expensesSummaryView.edgLineModes = [EdgLineMode.bottomShortLine,EdgLineMode.topLongLine]
        
        unitTotalMoney.edgLineModes = [EdgLineMode.bottomLongLine]
        unitTotalMoney.setTitleLblAttrubutes( color: UIColor(rgba: Constant.common_C8_color))
        unitTotalMoney.setContentLblAttrubutes(color: UIColor(rgba: Constant.common_C2_color))
        
        unitWishSend.delegate = self
        
        var tempView:BaseUnitView!
        
        for view in [unitRemarkContent,unitCoupon,unitPayType,unitWishSend,unitReceiptContent,expensesSummaryView,unitTotalMoney]
        {
            view.snp_makeConstraints(closure: { (make) -> Void in
                let _ = make.height.equalTo(view.iheight)
                let _ = make.left.equalTo(self)
                let _ = make.right.equalTo(self)
                
                if tempView == nil
                {
                    let _ = make.top.equalTo(self)
                }else
                {
                    if view == unitCoupon || view == expensesSummaryView
                    {
                        let _ = make.top.equalTo(tempView.snp_bottom).offset(MARGIN_8)
                    }else
                    {
                        let _ = make.top.equalTo(tempView.snp_bottom)
                    }
                }
            })
            self.iheight += view.iheight
            tempView = view
        }
        
        self.iheight += MARGIN_8 * 3
        
        if orderDetailModel != nil
        {
            self.setProperty(orderDetailModel!)
        }
    }
    
    
    
    func setProperty(_ orderDetailModel:OrderDetailModel)
    {
        unitTotalMoney.title = "共\(orderDetailModel.goods.count)件礼品"
        unitTotalMoney.content = String(format:"共计实付: ￥%.2lf", orderDetailModel.actual_price)
        
        //价格总结
        expensesSummaryView.goodsCost = orderDetailModel.sell_price
        expensesSummaryView.wishSendCost = 0
//        expensesSummaryView.disCountCost = 0
        expensesSummaryView.disCountCost = orderDetailModel.discount_price
        //        expensesSummaryView.transportCost = String(format: "￥%.2lf", orderDetailModel.actual_price)
        
        if orderDetailModel.order_type == 2 {

            expensesSummaryView.wishSendLbl.snp_updateConstraints(closure: { (make) in
                let _ = make.height.equalTo(0)
            })
            
            expensesSummaryView.transportLbl.snp_updateConstraints(closure: { (make) in
                let _ = make.height.equalTo(0)
            })
            
            expensesSummaryView.iheight = expensesSummaryView.iheight - expensesSummaryView.transportLbl.iheight - expensesSummaryView.wishSendLbl.iheight
            
            expensesSummaryView.snp_updateConstraints(closure: { (make) in
                let _ = make.top.equalTo(unitWishSend.snp_bottom)
                let _ = make.height.equalTo(expensesSummaryView.iheight)
            })
            
            self.iheight = self.iheight - expensesSummaryView.transportLbl.iheight - expensesSummaryView.wishSendLbl.iheight

        }else{
//            expensesSummaryView.disCountCost = orderDetailModel.discount_price
            expensesSummaryView.transportCost = 0
        }
        
        if orderDetailModel.order_remark != "" && orderDetailModel.order_type != 2{
            self.unitRemarkContent.content = orderDetailModel.order_remark
            if self.unitRemarkContent.content?.characters.count > 20 {
               self.unitRemarkContent.snp_updateConstraints(closure: { (make) in
                let _ = make.height.equalTo(self.unitRemarkContent.iheight + 20)
               })
                self.iheight = self.iheight + 20
            }
        }else{
            self.unitRemarkContent.snp_updateConstraints(closure: { (make) in
                let _ = make.height.equalTo(0)
            })
            self.iheight = self.iheight - unitRemarkContent.iheight
        }
        
        //没有红包：没有使用红包、未付款的
        if orderDetailModel.isHaveCoupon
        {
            unitCoupon.content = orderDetailModel.couponOfOrderModel!.name + " \(orderDetailModel.couponOfOrderModel!.par_value)"
        }else
        {
            unitCoupon.content = "未使用红包"
        }
        
        
        //没有支付方式:未付款的
        if orderDetailModel.isHavePayType{
            
            unitPayType.reloadPayType(orderDetailModel.pay_type)
            
            unitPayType.snp_updateConstraints(closure: { (make) -> Void in
                let _ = make.height.equalTo(unitPayType.iheight)
            })
        }
        else{
            unitPayType.snp_updateConstraints(closure: { (make) -> Void in
                let _ = make.height.equalTo(0)
                self.iheight = self.iheight - unitPayType.iheight
            })
        }
        
        
        
        //没有心意服务
        if orderDetailModel.isHaveWishSendServices{
            unitWishSend.content = orderDetailModel.service_name
            unitWishSend.contentLbl.textColor = UIColor(rgba: Constant.common_C2_color)
            expensesSummaryView.wishSendCost = orderDetailModel.service_price
            unitWishSend.detailIcon.isHidden = false
            unitWishSend.detailIcon.snp_makeConstraints(closure: { (make) -> Void in
                let _ = make.width.equalTo(unitWishSend.detailIcon.snp_height)
            })
        }else
        {
            unitWishSend.content = "未选择心意服务"
            unitWishSend.contentLbl.textColor = UIColor(rgba: Constant.common_C8_color)
            unitWishSend.detailIcon.isHidden = true
            unitWishSend.detailIcon.snp_makeConstraints(closure: { (make) -> Void in
                let _ = make.width.equalTo(0)
            })
            
            if orderDetailModel.order_type == 2 {
                unitWishSend.snp_updateConstraints(closure: { (make) in
                    let _ = make.height.equalTo(0)
                    self.iheight = self.iheight - unitWishSend.iheight
                })
            }
        }
        
        
        if orderDetailModel.isHaveInvoice
        {
            
            if (orderDetailModel.invoice_text.intValue - 1) >= 0 && (orderDetailModel.invoice_text.intValue - 1) <= 2{
                unitReceiptContent.content = InvoiceViewController.InvoiceContentAry[orderDetailModel.invoice_text.intValue - 1]
            }
            
        }else
        {
            
            unitReceiptContent.snp_updateConstraints(closure: { (make) -> Void in
                let _ = make.height.equalTo(0)
            })
            
            self.iheight = self.iheight - unitReceiptContent.iheight
            
        }

    }
    
    
    func orderUnitViewTapAction(_ unitView: BaseUnitView) {
        
        if let model = orderDetailModel{
            
            if unitView == unitWishSend && model.isHaveWishSendServices == true
            {
                let _ = Delegate_Selector(delegate, #selector(OrderUnitViewDelegate.orderUnitViewTapAction(_:))){Void in self.delegate!.orderUnitViewTapAction!(unitView)}
            }
        }
    }
    
    func addBill() {
        let _ = Delegate_Selector(delegate, #selector(OrderUnitViewDelegate.orderUnitViewBillButtonClicked)){Void in self.delegate!.orderUnitViewBillButtonClicked!()}
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

