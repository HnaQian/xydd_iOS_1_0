//
//  OrderDetailTableCell.swift
//  Limi
//
//  Created by Richie on 16/3/18.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class OrderDetailTableCell: OrderListTableViewCell {
    
    fileprivate var _delegate:OrderDetailTableCellDelegate?{return self.delegate as? OrderDetailTableCellDelegate}
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
    // Drawing code
    }
    */
    
    let actionBtn = UIButton()
    let logisticBtn = UIButton()
    
    
    override func setupUI() {
        
        self.addSubview(self.orderStautsLbl)
        self.orderStautsLbl.textColor = UIColor(rgba: Constant.common_C2_color)
        self.orderStautsLbl.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        self.orderStautsLbl.textAlignment = NSTextAlignment.right
        
        self.orderStautsLbl.snp_makeConstraints { (make) -> Void in
            let _ = make.right.equalTo(self).offset(-MARGIN_8)
            let _ = make.width.equalTo(50)
            let _ = make.top.equalTo(self).offset(MARGIN_8)
            let _ = make.height.equalTo(20)
        }
        //
        //        titleLbl.snp_updateConstraints { (make) in
        //
        //        }
        
        titleLbl.snp_remakeConstraints { (make) -> Void in
            let _ = make.top.equalTo(goodsImg)
            let _ = make.left.equalTo(goodsImg.snp_right).offset(MARGIN_8)
            let _ = make.right.equalTo(self.orderStautsLbl.snp_left).offset(-MARGIN_4)
            let _ = make.height.equalTo(20)
        }
        
        self.addSubview(actionBtn)
        actionBtn.setTitleColor(UIColor(rgba: Constant.common_red_color), for: UIControlState())
        actionBtn.titleLabel?.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        
        actionBtn.snp_makeConstraints { (make) -> Void in
            let _ = make.right.equalTo(self).offset(-MARGIN_8)
            let _ = make.bottom.equalTo(self).offset(-MARGIN_8)
            let _ = make.height.equalTo(30)
        }
        
        actionBtn.addTarget(self, action: #selector(OrderDetailTableCell.actionBtnOnClick), for: UIControlEvents.touchUpInside)
        
        self.addSubview(logisticBtn)
        logisticBtn.isHidden = true
        logisticBtn.setTitleColor(UIColor(rgba: Constant.common_red_color), for: UIControlState())
        logisticBtn.titleLabel?.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        
        logisticBtn.snp_makeConstraints { (make) -> Void in
            let _ = make.right.equalTo(actionBtn.snp_left).offset(-MARGIN_8)
            let _ = make.bottom.equalTo(self).offset(-MARGIN_8)
            let _ = make.height.equalTo(30)
        }
        
        logisticBtn.addTarget(self, action: #selector(OrderDetailTableCell.logisticBtnOnClick), for: UIControlEvents.touchUpInside)
        
        self.edgLineModes = [EdgLineMode.topShortLine]
    }
    
    
//    data.order.Order_status	订单状态：普通订单：1待付款 2待发货 3待收货 4已完成 5售后中 6已关闭
    override func reload(_ data: GoodsGenericModel) {
        super.reload(data)
        
        logisticBtn.setTitle("", for: UIControlState())
        logisticBtn.isEnabled = false
        
        actionBtn.setTitle("", for: UIControlState())
        actionBtn.isEnabled = false

        //如果是话费订单,没有物流和申请售后
        if data.goodsOfOrderType == 2{return}
        
        if let status = data.goodsOfOrderStatus{

            if status == 3{
                
                if data.goodsTransport != NULL_STRING && data.goodsTransport != ""
                {
                    actionBtn.isEnabled = true
                    actionBtn.setTitle("查看物流", for: UIControlState())
                }
                
            }else if status == 4{
                
                if shouldShowLogisticBtn(data) {
                    logisticBtn.isHidden = false
                    logisticBtn.isEnabled = true
                    logisticBtn.setTitle("查看物流", for: UIControlState())
                }
               
                actionBtn.isEnabled = true
                actionBtn.setTitle("申请售后", for: UIControlState())
            }
        }
        
    }
    
    //判断时间是否超过7天
    fileprivate func shouldShowLogisticBtn(_ outData:GoodsGenericModel) ->Bool{
        var shouldShow = true
        if let serverTime = outData.logisticUpdate_time {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            if let serverDate = formatter.date(from: serverTime) {
                let zone = TimeZone.current
                let tempInterval :Int = zone.secondsFromGMT(for: serverDate)
                let tempServerDate = serverDate.addingTimeInterval(TimeInterval(tempInterval))
                
                let tempDate = Date()
                let tempInterval2 :Int = zone.secondsFromGMT(for: tempDate)
                let currentDate = tempDate.addingTimeInterval(TimeInterval(tempInterval2))
                
                let interval = currentDate.timeIntervalSince(tempServerDate)
                if interval > 24 * 7 * 3600 {
                    shouldShow = false
                }else {
                    shouldShow = true
                }
            }
            
        }
        return shouldShow
    }
    
    @objc func actionBtnOnClick()
    {
        if let model = goodsModel,
            let orderStatus = model.goodsOfOrderStatus,
            let del = _delegate
        {
            if model.goodsOfOrderType == 1{
                
                if orderStatus == 3{
                    //查看物流
                    del.orderDetailTableCellOnClickShowTransport(model)
                }else if orderStatus == 4{
                    //联系客服
                    del.orderDetailTableCellOnClickSlod(model)
                }
            }
        }
    }
    
    @objc func logisticBtnOnClick() {
        if let model = goodsModel,
            let del = _delegate
        {
            if model.goodsOfOrderType == 1{
                //查看物流
                del.orderDetailTableCellOnClickShowTransport(model)
            }
        }
    }
}

protocol OrderDetailTableCellDelegate:BaseOrderTableViewCellDelegate{
    
    func orderDetailTableCellOnClickSlod(_ goodsGenericModel:GoodsGenericModel)
    func orderDetailTableCellOnClickShowTransport(_ goodsGenericModel:GoodsGenericModel)
}
