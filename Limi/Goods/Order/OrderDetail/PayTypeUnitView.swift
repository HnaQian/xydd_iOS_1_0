//
//  PayTypeUnitView.swift
//  Limi
//
//  Created by Richie on 16/3/24.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class PayTypeUnitView: UnitView {
    
    /*
     // Only override drawRect: if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func drawRect(rect: CGRect) {
     // Drawing code
     }
     */
    
    let payTypeIcons = ["pay_wechat","pay_ali","pay_union"]
    let payTypeNames = ["微信支付","支付宝支付","银联支付"]
    
    
    func reloadPayType(_ type:Int)
    {
        //1微信 2支付宝 3银联 4ApplePay
        if type > payTypeIcons.count{return}
        
        if type == 4{
            let appleView = PayViewController.ApplePayView()
            self.addSubview(appleView)
            appleView.snp_makeConstraints(closure: { (make) in
                let _ = make.right.equalTo(self).offset(-MARGIN_8)
                let _ = make.centerY.equalTo(self)
                let _ = make.height.equalTo(appleView.iheight)
                let _ = make.width.equalTo(appleView.iwidth)
            })
        }else{
            self.content = payTypeNames[type - 1]
//            let constraint = CGSize(width: Constant.ScreenSize.SCREEN_WIDTH, height: 40)
//            let attribute = NSDictionary(object: contentLbl.font, forKey: NSFontAttributeName as NSCopying)
//            let size = (contentLbl.text! as NSString).boundingRect(with: constraint, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes:attribute as? [String : AnyObject], context: nil)
//            self.contentLbl.snp_updateConstraints(closure: { (make) in
//                let _ = make.width.equalTo(size.width)
//            })
            self.contentIcon.image = UIImage(named: payTypeIcons[type - 1])
        }
    }
    
    
}
