//
//  OrderDetailViewControllerV1.swift
//  Limi
//
//  Created by Richie on 16/3/8.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//订单详情

/*
订单类型 1为普通订单，2为话费订单（新增Order_img,Order_name,Order_remark）

data.order.Order_status	订单状态：普通订单：1待付款 2待发货 3待收货 4已完成 5售后中 6已关闭
*/

import UIKit

private let OrderDetailVC_CellReuseInde = "OrderDetailVC_CellReuseInde"

class OrderDetailViewControllerV1: BaseOrderInfoViewController,OrderUnitViewDelegate,BottomToolBarOfOrderDetailDelegate,OrderRequestManagerDelegate,GiftBasketRequestManagerDelegate
{
    //UI
    fileprivate let toolBar = BottomToolBarOfOrderDetail()
    
    fileprivate let requestManager = OrderRequestManager()
    fileprivate var orderDetailModel:OrderDetailModel!
    var oid:Int!
    
    fileprivate var isFirst = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        requestManager.delegate = self
        
        requestManager.orderDetail(self.view,oid: oid,hudContent:"")
        
        Utils.updateViewControllersOfNavViewController(self.navigationController, exceptVCs: [PayViewController.self,OrderGeneratorViewControllerV1.self])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isFirst{isFirst = false}else{
            requestManager.orderDetail(self.view,oid: oid)}
        if UserInfoManager.didLogin {
           gcd.async(.default) {
                //用户信息
                let qyUserInfo = QYUserInfo()
                qyUserInfo.userId = UserDefaults.standard.string(forKey: "UserToken")
                qyUserInfo.data = QYLocalUserInfo.shareInstance.getLocalUserInfo()
                QYSDK.shared().setUserInfo(qyUserInfo)
            }
        }
        self.toolBar.consultButton.refreshBageValue(Constant.UNREAD)
    }
    
    //MARK: 创建一个新的DetailVC
    class func newDetailVC(_ oid:Int) ->OrderDetailViewControllerV1 {
        let VC = OrderDetailViewControllerV1()
        VC.oid = oid
        return VC
    }
    
    //MARK: 加载详情回调
    func orderRequestManagerDidLoadDetailFinish(_ manager: LMRequestManager, result: LMResultMode, detail: OrderDetailModel?) {
        
        print(result.rootData)
        if let detailModel = detail{
            print(detailModel.ORIGINAL_DATA)
            self.orderDetailModel = detailModel
            self.refreshUI()
        }
    }
    
    
    
    //MARK: 取消订单
    func toolOfOrderDetailOnClickCancleBtn() {
        
        LCActionSheet(title: "您确定要取消订单吗？", buttonTitles: ["确定"], cancelTitle: "取消", redButtonIndex: 0) { (buttonIndex) -> Void in
            
            if buttonIndex == 1{
                self.requestManager.orderdelete(self.view,oid: self.orderDetailModel.oid)
            }
            
            }.show()
    }
    //MARK: 取消订单回调
    func orderRequestManagerDidDeleteFinish(_ manager: LMRequestManager, result: LMResultMode) {
        if result.status == 200{
            NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.orderList_changed), object: nil)
           let _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    //MARK:  去支付
    func toolOfOrderDetailOnClickPayBtn() {
        self.navigationController?.pushViewController(PayViewController.newPayViewController(orderDetailModel.oid, canclePay: CancleAction.goBack), animated: true)
    }
    
    
    //MARK: 联系客服
    func toolOfOrderDetailOnClickContanctBtn() {
        if !UserInfoManager.didLogin {
            self.jumpToLogin(Constant.InLoginType.consultType, tipe: nil)
        }else {
            let source = QYSource()
            source.title = "订单详情"
            if let link = self.orderDetailModel.goods[0].onlyId {
                 source.urlString = Constant.JTAPI.logistic_link + "?goods_oid=\(link)"
            }
           
            let commodityInfo = QYCommodityInfo()
            commodityInfo.title = "ID:" + String(self.orderDetailModel.oid)
            commodityInfo.pictureUrlString = self.orderDetailModel.order_image
            commodityInfo.desc = qyOrderInfo(self.orderDetailModel.goods)
            commodityInfo.note = self.orderDetailModel.order_status_name
            commodityInfo.urlString = source.urlString
            
            
            let qyvc = QYSDK.shared().sessionViewController()
            qyvc?.navigationItem.leftBarButtonItem = qyCustomBackButton()
            qyvc?.sessionTitle = ""
            qyvc?.commodityInfo = commodityInfo
            qyvc?.source = source
            qyvc?.groupId = 74126
            qyvc?.hidesBottomBarWhenPushed = true
            QYSDK.shared().customActionConfig().linkClickBlock = {
                [weak self](QYLinkClickBlock) -> Void in
                let temp = QYLinkClickBlock! as String
                EventDispatcher.dispatch(temp, onNavigationController: self?.navigationController)
            }
            self.navigationController?.pushViewController(qyvc!, animated: true)
        }
    }
    
    func qyOrderInfo(_ outGoods:[GoodsGenericModel]?) ->String {
        var orderInfo = ""
        for index in 0 ..< outGoods!.count {
            let price = outGoods![index].currentPrice
            let temp = outGoods![index].selectedGoodsAttrNames
            orderInfo += "￥\(price) "
            for index1 in 0 ..< temp.count {
                if index1 < temp.count - 1 {
                    orderInfo += temp[index1] + ","
                }else {
                    orderInfo += temp[index1]
                }
            }
            orderInfo += ";"
        }
        return orderInfo
    }
    
    func qyCustomBackButton() -> UIBarButtonItem
    {
        let backButton = UIButton(type: UIButtonType.custom)
        backButton.frame = CGRect(x: 0, y: 0, width: 36, height: 36)
        backButton.setBackgroundImage(UIImage(named: "common_back_icon"), for: UIControlState())
        backButton.addTarget(self, action: #selector(OrderDetailViewControllerV1.backButtonClicked), for: UIControlEvents.touchUpInside)
        backButton.clipsToBounds = true
        backButton.tintColor = UIColor.red
        
        let item = UIBarButtonItem(customView: backButton)
        
        return item
    }
    
    func backButtonClicked() {
       let _ = self.navigationController?.popViewController(animated: true)
    }
    
    //再次购买
    func toolOfOrderDetailOnClickRepayBtn() {
        
        var gids = [Int]()
        var subIds = [Int]()
        var num = [Int]()
        if self.orderDetailModel.goods.count > 0 {
            for index in 0 ..< orderDetailModel.goods.count {
                if let temp0 = orderDetailModel.goods[index].goodsId {
                    gids.append(temp0)
                }
                
                if let temp1 = orderDetailModel.goods[index].goodsSubId {
                    subIds.append(temp1)
                }else {
                    subIds.append(0)
                }
                num.append(orderDetailModel.goods[index].selectedGoodsSubModelCount)
            }
        }
        let giftBasketManager = GiftBasketRequestManager()
        giftBasketManager.delegate = self
        giftBasketManager.basketAdd(self.view,gids: gids,subIds: subIds,num: num,showError: true,isNeedHud: true)
        
    }
    
    func giftBasketRequestManagerDidAdd(_ manager: LMRequestManager, result: LMResultMode) {
        let shoppingBasketVc = ShoppingBasketViewController()
        shoppingBasketVc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(shoppingBasketVc, animated: true)
    }
    
    //MARK: 补开发票
    func orderUnitViewBillButtonClicked() {
        self.navigationController?.pushViewController(InvoiceViewController.newReceipt(orderDetailModel.oid), animated: true)
    }
    
    //MARK: 心意服务
    func orderUnitViewTapAction(_ unitView:BaseUnitView){
        
        if let url = URL(string: orderDetailModel.service_url){
            EventDispatcher.dispatch(URL: url, onNavigationController: self.navigationController)
        }
    }
    
    
    //cell的函数监听
    //申请售后
    override func orderDetailTableCellOnClickSlod(_ goodsGenericModel: GoodsGenericModel) {
        
        print((goodsGenericModel as! BaseDataMode).ORIGINAL_DATA!)
        
        self.navigationController?.pushViewController(OrderServiceViewController.newOrderServiceViewController(JSON(orderDetailModel.ORIGINAL_DATA! as AnyObject),selectedGoodsInfo: JSON((goodsGenericModel as! BaseDataMode).ORIGINAL_DATA! as AnyObject)), animated: true)
    }
    
    
    //可查看物流
    override func orderDetailTableCellOnClickShowTransport(_ goodsGenericModel: GoodsGenericModel) {
        //物流
        if goodsGenericModel.goodsOfOrderType == 1{
            let link = (goodsGenericModel as! GoodsOfOrderModel).express_url
            
            if link.characters.count > 1 ,let url = URL(string: link){
                    EventDispatcher.dispatch(URL: url, onNavigationController: self.navigationController)
                return ;
            }
            Utils.showError(context: self.view, errorStr: "暂无物流信息")
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath)
    {
        let data = self.goodsDataList[(indexPath as NSIndexPath).row]
        
        if let id = data.goodsId{
            let goods = GoodsDetailViewController.newGoodsDetailWithID(id)
            goods.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(goods, animated: true)
        }
    }
    
    
    //MARK: 刷新UI
    func refreshUI()
    {
        self.header = HeaderOfOrderDetail(detailModel: orderDetailModel)
        
        let tempFooter = FooterOfOrderDetail(detailModel: orderDetailModel)
        tempFooter.delegate = self
        self.footer = tempFooter
        
        if orderDetailModel != nil
        {
            toolBar.refreshUI(orderDetailModel)
            self.goodsDataList = orderDetailModel.goods
        }
    }
    
    
    //MARK: refreshUI
    override func setupUI()
    {
        self.registerClass(OrderDetailTableCell.self, forCellReuseIdentifier: OrderDetailVC_CellReuseInde)
        
        self.navigationItem.title = "订单详情"
        
        self.view.addSubview(toolBar)
        toolBar.delegate = self
        toolBar.snp_makeConstraints { (make) -> Void in
            let _ = make.bottom.equalTo(self.view)
            let _ = make.left.equalTo(self.view)
            let _ = make.right.equalTo(self.view)
            let _ = make.height.equalTo(50)
        }
        
        tableView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.view)
            let _ = make.left.equalTo(self.view)
            let _ = make.right.equalTo(self.view)
            let _ = make.bottom.equalTo(toolBar.snp_top)
        }
        
        refreshUI()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
