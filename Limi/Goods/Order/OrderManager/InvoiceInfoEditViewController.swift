//
//  InvoiceInfoEditViewController.swift
//  Limi
//
//  Created by 程巍巍 on 5/24/15.
//  Copyright (c) 2015 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class InvoiceInfoEditViewController: BaseViewController,UITextFieldDelegate {
    
    var editView: EditView {
        return self.view as! EditView
    }
    
    var invoiceInfoHandler: ((_ head: String, _ type: Int)->Void)?
    var faPiaoString: String?
    
    override func loadView() {
        super.loadView()
        self.view = EditView()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
//        self.edgesForExtendedLayout = UIRectEdge()
        self.editView.titleField.delegate = self
        self.navigationItem.title = "发票信息"
        
        editView.commitButton.addTarget(self, action: #selector(InvoiceInfoEditViewController.commit), for: UIControlEvents.touchUpInside)
        editView.commitButton.addTarget(self, action: #selector(InvoiceInfoEditViewController.changeBackColor(_:)), for: UIControlEvents.touchDown)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        editView.titleField.text = faPiaoString

    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func changeBackColor(_ sender: UIButton)
    {
        sender.backgroundColor = UIColor(rgba: Constant.common_C100_color)
    }
    
    @objc func commit() {
        
        editView.commitButton.backgroundColor = UIColor(rgba: Constant.common_C1_color)
        if editView.titleField.text?.characters.count < 2 && self.faPiaoString?.characters.count == 0 {
            Utils.showError(context: self.view, errorStr: "发票抬头不可少于2个字符")
            return
        }else if editView.titleField.text?.characters.count > 100 {
            Utils.showError(context: self.view, errorStr: "发票抬头不可超过100个字符")
            return
        }
        self.faPiaoString = editView.titleField.text
        editView.titleField.resignFirstResponder()
        let _ = self.navigationController?.popViewController(animated: true)
        
        var head = editView.titleField.text
        if head == nil {head = ""}
        invoiceInfoHandler?(head!, editView.selectedButton.type)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        if textField == self.editView.titleField
        {
            if string.characters.count > 0 {
                if textField.text?.characters.count > 99{
                    Utils.showError(context: self.view, errorStr: "发票抬头不可超过100个字符")
                    return false
                }
            }
        }
        return true
    }

}

extension InvoiceInfoEditViewController {
    class EditView: BaseView {
        var titleView = UIView()
        var titleLabel = UILabel()
        var titleField = UITextField()
        
        var contentView = UIView()
        var contentTitleLabel = UILabel()
        
        var selectedButton: RadioButton!
        
        var commitButton = UIButton()
        var commitButtonBackgroundView = UIView()
        
        override func defaultInit() {
            self.backgroundColor = UIColor(rgba: Constant.common_background_color)
            
            self.titleView.backgroundColor = UIColor.white
            self.contentView.backgroundColor = UIColor.white
            
            self.titleLabel.text = "发票抬头"
            self.contentTitleLabel.text = "发票内容"
            self.titleLabel.font = Constant.CustomFont.Default(size: 15)
            self.contentTitleLabel.font = Constant.CustomFont.Default(size: 15)
            self.titleField.font = Constant.CustomFont.Default(size: 15)
            
            self.titleField.clearButtonMode = UITextFieldViewMode.whileEditing
            
            self.titleField.layer.borderColor = UIColor(rgba: "#F1F1F1").cgColor
            self.titleField.layer.borderWidth = 0.5
            self.titleField.backgroundColor = UIColor(rgba: Constant.common_background_color)
            
            self.titleField.placeholder = "可以输入个人/单位名称"
            
            commitButton.backgroundColor = UIColor(rgba: Constant.common_red_color)

            commitButton.titleLabel?.font = UIFont.systemFont(ofSize: 15)
            commitButton.setTitleColor(UIColor.white, for: UIControlState())
            commitButton.setTitle("确定", for: UIControlState())
            
            commitButtonBackgroundView.backgroundColor = UIColor.white
            
            self.addSubview(titleView)
            self.addSubview(contentView)
            
            self.titleView.addSubview(titleLabel)
            self.titleView.addSubview(titleField)
            
            self.contentView.addSubview(contentTitleLabel)
            
            // layout constraint
            self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "|-(-1)-[title]-(-1)-|", options: [], metrics: nil, views: ["title": titleView]))

            titleView.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(self).offset(8)
            }
            titleLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(titleView).offset(10)
                let _ = make.left.equalTo(titleView).offset(10)
            }

            titleField.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(titleLabel.snp_bottom).offset(15)
                let _ = make.left.equalTo(titleView).offset(10)
                let _ = make.right.equalTo(titleView).offset(-10)
                let _ = make.height.equalTo(32)
            }

            titleView.snp_makeConstraints { (make) -> Void in
                let _ = make.bottom.equalTo(titleField).offset(10)
            }
            

            self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "|-(-1)-[content]-(-1)-|", options: [], metrics: nil, views: ["content": contentView]))
            
            contentView.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(titleView.snp_bottom).offset(8)
            }
            contentTitleLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.centerY.equalTo(contentView.snp_top).offset(24)
                let _ = make.left.equalTo(contentView).offset(10)
            }

            // 添加 separatorline 
            var separator = UIView()
            separator.backgroundColor = UIColor(rgba: Constant.common_separatLine_color)
            self.contentView.addSubview(separator)
            
            separator.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(contentView).offset(10)
                let _ = make.right.equalTo(contentView).offset(10)
                let _ = make.top.equalTo(contentView).offset(48)
                let _ = make.height.equalTo(0.5)
            }
            
            _ = contentTitleLabel
            var types = [1: "明细", 2: "礼品", 3: "办公用品"]
            for index in 1...3 {
                let tag = index
                let title = types[index]
                let button = RadioButton()
                button.tag = tag
                button.selected(false)
                button.title = title
                button.addTarget(self, action: #selector(EditView.buttonClick(_:)), for: UIControlEvents.touchUpInside)

                
                contentView.addSubview(button)
                
                // constraint
                
                button.snp_makeConstraints(closure: { (make) -> Void in
                    let _ = make.centerY.equalTo(separator.snp_bottom).offset(24)
                    let _ = make.left.equalTo(contentTitleLabel)
                    let _ = make.width.equalTo(contentView).offset(-20)
                })
                
                let separatorLine = UIView()
                separatorLine.backgroundColor = UIColor(rgba: Constant.common_separatLine_color)
                self.contentView.addSubview(separatorLine)

                separatorLine.snp_makeConstraints(closure: { (make) -> Void in
                    let _ = make.left.equalTo(contentView).offset(10)
                    let _ = make.right.equalTo(contentView).offset(10)
                    let _ = make.top.equalTo(separator).offset(48)
                    let _ = make.height.equalTo(0.5)
                })
                
                separator = separatorLine
                
                if selectedButton == nil {
                    selectedButton = button
                    button.selected(true)
                }
            }
            

            contentView.snp_makeConstraints { (make) -> Void in
                let _ = make.bottom.equalTo(separator)
            }
            self.addSubview(commitButtonBackgroundView)
            commitButtonBackgroundView.addSubview(commitButton)
            
            commitButtonBackgroundView.snp_makeConstraints { (make) -> Void in
                let _ = make.bottom.equalTo(self)
                let _ = make.centerX.equalTo(self)
                let _ = make.width.equalTo(self)
                let _ = make.height.equalTo(44)
            }
            

            commitButton.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(commitButtonBackgroundView)
                let _ = make.bottom.equalTo(commitButtonBackgroundView)
                let _ = make.centerX.equalTo(commitButtonBackgroundView)
                let _ = make.width.equalTo(commitButtonBackgroundView)
            }
        }
        
        func buttonClick(_ sender: RadioButton){
            self.selectedButton.selected(false)
            self.selectedButton = sender 
            self.selectedButton.selected(true)
        }
        
    }
    
    class RadioButton: UIControl {
        var imageview = UIImageView()
        var titleLabel = UILabel()
        
        var title: String! {
            didSet{
                titleLabel.text = title
            }
        }
        
        var type: Int! {
            return self.tag & 0xf0ff
        }
        
        func selected(_ sel: Bool) {
            if sel {
                self.tag = self.tag | 0x0f00
                imageview.image = UIImage(named: "choosed")
            }else{
                self.tag = self.tag & 0xf0ff
                imageview.image = UIImage(named: "noChoose")
            }
            self.superview?.layoutIfNeeded()
        }
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            
            self.addSubview(imageview)
            self.addSubview(titleLabel)
            
            imageview.contentMode = UIViewContentMode.center
            
            imageview.image = UIImage(named: "unChoose")
            titleLabel.font = Constant.CustomFont.Default(size: 15)
            titleLabel.textColor = UIColor.black
            

            imageview.snp_makeConstraints { (make) -> Void in
                let _ = make.centerY.equalTo(self)
            }
            
            titleLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(self).offset(4)
                let _ = make.bottom.equalTo(self).offset(4)
                let _ = make.left.equalTo(imageview.snp_right).offset(2)
                let _ = make.right.equalTo(self).offset(-2)
            }
            

            imageview.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(self)
                let _ = make.height.equalTo(18)
                let _ = make.width.equalTo(18)
            }
        }

        required init(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}
