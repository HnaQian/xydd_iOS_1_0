//
//  OrderServiceReasonViewController.swift
//  Limi
//
//  Created by 程巍巍 on 7/28/15.
//  Copyright (c) 2015 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class OrderServiceReasonViewController: BaseViewController
{
    // 售后原因 1. 2. 3. 4.
    fileprivate var typeTitles: [Int: [String]] = [
        1: ["我买错了", "收到商品破损", "收到商品不符", "商品质量问题", "七天无理由退换货"],
        2: ["我不想要了", "收到商品破损", "收到商品不符", "商品质量问题", "七天无理由退换货"],
        3: ["我不小心弄坏了", "商品质量问题", "保修期内申请维修"],
        4: ["商品未发", "未收到货"],
    ]
    
    var reasonType: Int = 1{didSet{
            dataSource = typeTitles[reasonType]!
            self.tableView.reloadData()
        }}
    
    var didSelectReason: ((_ reason: String)->())?
    
    var tableView: UITableView
        {
        return self.view as! UITableView
    }
    
    var dataSource: [String] = []
    
    override func loadView()
    {
        super.loadView()
        self.view = UITableView(frame: CGRect.zero, style: UITableViewStyle.grouped)
    }

    override func viewDidLoad()
    {
        super.viewDidLoad()
    
//        self.edgesForExtendedLayout = UIRectEdge()
        
        self.navigationItem.title = "申请售后"
        
        dataSource = typeTitles[reasonType]!
        
        tableView.register(TableViewCell.self, forCellReuseIdentifier: "TableViewCell")
        
        tableView.delegate = self
        
        tableView.dataSource = self
        
        tableView.separatorColor = UIColor(rgba: Constant.common_separatLine_color)
        
        tableView.showsHorizontalScrollIndicator = false
        
        tableView.showsVerticalScrollIndicator = false
        
        // 默认选中第一项
        tableView.selectRow(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: UITableViewScrollPosition.middle)
        
        didSelectReason?(dataSource.first!)
    }
    
    
    
//    override func viewWillAppear(animated: Bool) {
//        super.viewWillAppear(animated)
//    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension OrderServiceReasonViewController: UITableViewDelegate, UITableViewDataSource
{
    fileprivate final class TableViewCell: UITableViewCell
    {
        var checkButton = UIButton()
        
        var desLabel = UILabel()
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?)
        {
            super.init(style: style, reuseIdentifier: reuseIdentifier)
        
            self.selectionStyle = UITableViewCellSelectionStyle.none
            
            checkButton.setBackgroundImage(UIImage(named: "noChoose"), for: UIControlState())
            
            checkButton.setBackgroundImage(UIImage(named: "choosed"), for: UIControlState.disabled)
            
            desLabel.font = UIFont.systemFont(ofSize: 13)
            
            desLabel.textColor = UIColor.black
            
            // 布局
            contentView.addSubview(checkButton)
            
            contentView.addSubview(desLabel)
            checkButton.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(self).offset(13)
                let _ = make.centerY.equalTo(self)
                let _ = make.height.equalTo(self).multipliedBy(0.5)
                let _ = make.width.equalTo(checkButton.snp_height)
            }
            
            desLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(checkButton.snp_right).offset(13)
                let _ = make.centerY.equalTo(checkButton)
            }

        }
        
        override func setSelected(_ selected: Bool, animated: Bool)
        {
            super.setSelected(selected, animated: animated)
        
            checkButton.isEnabled = !selected
        }

        required init(coder aDecoder: NSCoder)
        {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return dataSource.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
  
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath) as! TableViewCell
        
        cell.desLabel.text = dataSource[(indexPath as NSIndexPath).row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 8
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        didSelectReason?(dataSource[(indexPath as NSIndexPath).row])
        
        let _ = self.navigationController?.popViewController(animated: true)
    }
}
