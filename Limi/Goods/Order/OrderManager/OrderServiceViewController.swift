//
//  OrderServiceViewController.swift
//  Limi
//
//  Created by 程巍巍 on 7/27/15.
//  Copyright (c) 2015 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//  售后服务

import UIKit
import AVFoundation
import Photos

class OrderServiceViewController: BaseViewController,OrderRequestManagerDelegate {
    fileprivate var serviceView: ServiceView {
        return self.view as! ServiceView
    }
    
    var orderInfo: JSON?
    var selectedGoodsInfo:JSON?
    
    override func loadView() {
        super.loadView()
        self.view =  ServiceView()
    }
    
    
    fileprivate var serviceType = 1
    
    fileprivate var  OrderServiceReasonVC:OrderServiceReasonViewController!
    
    
    class func newOrderServiceViewController(_ orderInfo:JSON,selectedGoodsInfo:JSON) ->OrderServiceViewController
    {
        let orderServiceVC = OrderServiceViewController()
        
        orderServiceVC.orderInfo = orderInfo
        
        orderServiceVC.selectedGoodsInfo = selectedGoodsInfo
        
        return orderServiceVC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.edgesForExtendedLayout = UIRectEdge()
        self.navigationItem.title = "申请售后"
        
        serviceView.goodInfoView.reloadData(orderInfo, selectedGoodsInfo: selectedGoodsInfo)
        
        // 选择原因
        serviceView.contentView.reasonView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(OrderServiceViewController.selectReason)))
        
        // 提交
        serviceView.actionButton.addTarget(self, action: #selector(OrderServiceViewController.sendRequest), for: .touchUpInside)
        
        serviceView.vc = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(OrderServiceViewController.onBtnClick1(_:)), name: NSNotification.Name(rawValue: "xiangji"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(OrderServiceViewController.onBtnClick2(_:)), name: NSNotification.Name(rawValue: "xiangce"), object: nil)
        
    }
    
    func onBtnClick1(_ notification:Notification){
        
        if let data = notification.object as? UIAlertController{
            self.present(data, animated: true, completion: { 
                
            })
            
        }
    }
    
    func onBtnClick2(_ notification:Notification){
        
        if let data = notification.object as? UIAlertController{
            self.present(data, animated: true, completion: {
                
            })
            
        }

    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // 监听 键盘
        NotificationCenter.default.addObserver(self, selector: #selector(OrderServiceViewController.keyboardFrameChanged(_:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    @objc func selectReason() {
        self.serviceView.contentView.desTextView.resignFirstResponder()
        
        if self.serviceType == serviceView.contentView.serviceType
        {
            if self.OrderServiceReasonVC != nil
            {
                self.navigationController?.pushViewController(self.OrderServiceReasonVC, animated: true)
                
                return
            }
            
        }else{
            
            self.serviceType = serviceView.contentView.serviceType
        }
        
        if self.OrderServiceReasonVC == nil
        {
            self.OrderServiceReasonVC = OrderServiceReasonViewController()
        }
        
        self.OrderServiceReasonVC.reasonType = self.serviceType
        
        self.OrderServiceReasonVC.didSelectReason = { [weak self](reason: String)->() in
            self?.serviceView.contentView.reasonView.reasonField.text = reason
        }
        
        self.navigationController?.pushViewController(self.OrderServiceReasonVC, animated: true)
    }
    
    @objc func keyboardFrameChanged(_ noti: Notification) {
        var frame = self.view.frame
        let endFrame = ((noti as NSNotification).userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let duration = (noti as NSNotification).userInfo![UIKeyboardAnimationDurationUserInfoKey] as! NSNumber
        
        frame.size.height = endFrame.origin.y - 64
        frame.origin.y = 64
        
        UIView.animate(withDuration: duration.doubleValue, animations: { () -> Void in
            self.view.frame = frame
        })
    }
    
    // MARK: - 请求接口
    @objc func sendRequest() {
        
        /*
         
         token	true	string	登录token值
         oid	true	int	订单ID
         goods_oid	true	int	子订单ID
         type	true	int	售后类型: 1换货 2退货 3维修 4退款
         reason	true	string	售后原因
         detail	false	string	问题描述
         sold_img	false	[]string	售后图片，有的话必须传，格式为数组
         
         */
        
        
        var params = [String: AnyObject]()
        if let oid = orderInfo?["Oid"].int {
            params["oid"] = oid as AnyObject
        }
        
        if let oid = selectedGoodsInfo?["Id"].int {
            params["goods_oid"] = oid as AnyObject?
        }
        
        print(serviceView.contentView.serviceType)
        params["type"] = serviceView.contentView.serviceType as AnyObject?
        let reason = serviceView.contentView.reasonView.reasonField.text
        if reason == nil || reason!.isEmpty {
            Utils.showError(context: self.view, errorStr: "请选择售后原因")
            return
        }
        params["reason"] = reason as AnyObject?
        let detail = serviceView.contentView.desTextView.text
        if detail != nil && !(detail?.isEmpty)! {
            params["detail"] = detail as AnyObject?
        }
        let sold_img = serviceView.contentView.imageSelectView.images
        if !sold_img.isEmpty {
            params["sold_img"] = sold_img as AnyObject?
        }

        OrderRequestManager(delegate:self).orderCommitSlod(self.view,params: params)
    }
    
    func orderRequestManagerDidCommitOfSoldFinish(_ manager: LMRequestManager, result: LMResultMode) {
        
        if result.status == 200
        {
            // 显示提交成功界面
            if let view  = self.view {
                
                let csv = CommitSuccessView()
                
                if let oid = orderInfo?["Oid"].int {
                    csv.orderId = oid
                }
                
                view.addSubview(csv)
                csv.snp_makeConstraints(closure: { (make) in
                   let _ = make.edges.equalTo(self.view)
                })
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}


private let TITLESize: CGFloat = 13

extension OrderServiceViewController
{
    fileprivate final class ServiceView: UIView
    {
        var scrollView = UIScrollView()
        
        var goodInfoView = GoodInfoView()//物品规格信息
        
        var contentView = ActionView()
        
        var actionButton = UIButton()
        
        var vc:UIViewController?
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            
            actionButton.backgroundColor = UIColor(rgba: "#DE383C")
            actionButton.layer.cornerRadius = 2.5
            actionButton.setTitle("提交", for: UIControlState())
            
            let actionView = UIView()
            actionView.addSubview(actionButton)
            actionButton.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(actionView).offset(4)
                let _ = make.bottom.equalTo(actionView).offset(-4)
                let _ = make.left.equalTo(actionView).offset(MARGIN_13)
                let _ = make.right.equalTo(actionView).offset(-MARGIN_13)
            }
            
            self.backgroundColor = UIColor(rgba: Constant.common_background_color)
            goodInfoView.backgroundColor = UIColor.white
            contentView.backgroundColor = UIColor.white
            actionView.backgroundColor = UIColor.white
            
            contentView.vc = self.vc
            scrollView.showsHorizontalScrollIndicator = false
            scrollView.showsVerticalScrollIndicator = false
            
            self.addSubview(scrollView)
            self.addSubview(actionView)
            scrollView.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(self)
                let _ = make.left.equalTo(self)
                let _ = make.right.equalTo(self)
            }
            
            actionView.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(scrollView.snp_bottom)
                let _ = make.left.equalTo(self)
                let _ = make.right.equalTo(self)
                let _ = make.bottom.equalTo(self)
                let _ = make.height.equalTo(36)
            }
            
            scrollView.addSubview(goodInfoView)
            scrollView.addSubview(contentView)
            goodInfoView.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(scrollView).offset(8)
                let _ = make.left.equalTo(scrollView)
                let _ = make.right.equalTo(scrollView)
                let _ = make.width.equalTo(scrollView)
            }
            
            contentView.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(goodInfoView.snp_bottom).offset(8)
                let _ = make.left.equalTo(scrollView)
                let _ = make.right.equalTo(scrollView)
                let _ = make.bottom.lessThanOrEqualTo(scrollView)
            }
            
        }
        
        required init(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    
    fileprivate final class GoodInfoView: UIView {
        
        func reloadData(_ orderInfo: JSON?,selectedGoodsInfo:JSON?)
        {
            if let orderId = orderInfo?["Oid"].stringValue
            {
                orderIdLabel.text = "礼单号" + String(orderId)
            }

            if let url = selectedGoodsInfo?["Goods_image"].string
            {
                let imageScale : String = url + "?imageView2/0/w/" + "\(Int(48*Constant.ScreenSize.SCALE_SCREEN))" + "/h/" + "\(Int(48*Constant.ScreenSize.SCALE_SCREEN))"
                
                goodImage.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "loadingDefault"))
            }
            
            
            if let title = selectedGoodsInfo?["Goods_name"].string
            {
                titleLabel.text = title
            }
            
            
            var subt = ""
            
            if let attrs = selectedGoodsInfo?["Goods_attr"].array
            {
                for attr in attrs
                {
                    subt += " " + attr["Attr_value_name"].stringValue
                }
            }
            
            subTitleLabel.text = subt
            
            let number = selectedGoodsInfo?["Goods_num"].int ?? 0
            
            countLabel.text = String(format: "x%d", number)
            
            
            let price = String(format:"%.2f", orderInfo?["Actual_price"].float ?? 0)
            let attributedString1 = NSMutableAttributedString(string: String(format: "共%d件礼品", number) as String)
            
            let firstAttributes = [NSForegroundColorAttributeName: UIColor.lightGray, NSFontAttributeName: UIFont.systemFont(ofSize: 13)]
            let secondAttributes = [NSForegroundColorAttributeName: UIColor.black, NSFontAttributeName: UIFont.systemFont(ofSize: 13)]
            
            attributedString1.addAttributes(firstAttributes, range: NSMakeRange(0, 1))
            attributedString1.addAttributes(secondAttributes, range: NSMakeRange(1, attributedString1.length - 4))
            attributedString1.addAttributes(firstAttributes, range: NSMakeRange(attributedString1.length - 3, 3))
            totallLabel.attributedText = attributedString1
            
            let attributedString2 = NSMutableAttributedString(string: String(format: "实付: ¥%@", price) as String)
            attributedString2.addAttributes(firstAttributes, range: NSMakeRange(0, 4))
            attributedString2.addAttributes(secondAttributes, range: NSMakeRange(4, attributedString2.length - 4))
            priceLabel.attributedText = attributedString2
        }
        
        
        
        var orderIdLabel = UILabel()
        var goodImage = UIImageView(image: UIImage(named: "loadingDefault"))
        var titleLabel = UILabel()
        var subTitleLabel = UILabel()
        var countLabel = UILabel()
        var totallLabel = UILabel()
        var priceLabel = UILabel()
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            
            goodImage.backgroundColor = UIColor.lightGray
            goodImage.layer.cornerRadius = 2.5
            
            let sep0 = separatorLine()
            let sep1 = separatorLine()
            
            for view in [sep0, sep1, orderIdLabel, goodImage, titleLabel, subTitleLabel, countLabel, totallLabel,priceLabel] {
                self.addSubview(view)
            }
            orderIdLabel.font = UIFont.systemFont(ofSize: 13)
            orderIdLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(self).offset(MARGIN_13)
                let _ = make.left.equalTo(self).offset(MARGIN_13)
            }
            
            sep0.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(orderIdLabel.snp_bottom).offset(MARGIN_13)
                let _ = make.left.equalTo(self).offset(MARGIN_13)
                let _ = make.right.equalTo(self).offset(-MARGIN_13)
            }
            
            
            goodImage.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(self).offset(MARGIN_13)
                let _ = make.top.equalTo(sep0.snp_bottom).offset(MARGIN_13)
                let _ = make.width.equalTo(48)
                let _ = make.height.equalTo(goodImage.snp_width)
            }
            
            countLabel.font = UIFont.systemFont(ofSize: 13)
            countLabel.textAlignment = .right
            countLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.right.equalTo(self).offset(-MARGIN_13)
                let _ = make.centerY.equalTo(goodImage.snp_centerY)
                let _ = make.height.equalTo(20)
                let _ = make.width.equalTo(60)
            }
            
            titleLabel.font = UIFont.systemFont(ofSize: 13)
            titleLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(goodImage)
                let _ = make.left.equalTo(goodImage.snp_right).offset(8)
                let _ = make.right.lessThanOrEqualTo(self).offset(-8)
            }
            
            subTitleLabel.font = UIFont.systemFont(ofSize: 12)
            subTitleLabel.textColor = UIColor.lightGray
            subTitleLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(titleLabel.snp_bottom).offset(MARGIN_13)
                let _ = make.left.equalTo(titleLabel)
                let _ = make.right.lessThanOrEqualTo(countLabel.snp_left).offset(-8)
                //make.centerY.equalTo(goodImage)
                let _ = make.right.equalTo(self).offset(-MARGIN_13)
            }
            
            sep1.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(goodImage.snp_bottom).offset(MARGIN_13)
                let _ = make.left.equalTo(goodImage)
                let _ = make.right.equalTo(self).offset(-8)
            }
            
            totallLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(sep1.snp_bottom).offset(MARGIN_13)
                let _ = make.left.equalTo(sep1)
                let _ = make.bottom.equalTo(self).offset(-MARGIN_13)
            }
            priceLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.centerY.equalTo(totallLabel.snp_centerY)
                let _ = make.left.equalTo(totallLabel.snp_right).offset(20)
            }
        }
        
        required init(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func separatorLine()->UIView {
            let view = UIView()
            view.backgroundColor = UIColor(rgba: Constant.common_separatLine_color)
            view.snp_makeConstraints { (make) -> Void in
                let _ = make.height.equalTo(0.5)
            }
            
            return view
        }
    }
    
    fileprivate final class ActionView: UIView,UITextViewDelegate {
        
        var serviceType: Int = 1 {
            didSet {
                if serviceType != oldValue {
                    reasonView.reasonField.text = nil
                }
            }
        }
        var vc:UIViewController?
        var soldImage = [String]()
        
        let reasonView = ReasonCell()
        let desTextView = UITextView()
        let imageSelectView = ImageSelectView()
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            
            let typeLabel = UILabel()
            typeLabel.font = UIFont.systemFont(ofSize: TITLESize)
            typeLabel.textColor = UIColor.black
            typeLabel.text = "申请售后类型"
            
            self.addSubview(typeLabel)
            
            typeLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(self).offset(MARGIN_13)
                let _ = make.left.equalTo(self).offset(MARGIN_13)
            }
            
            // buttons
            let typeButtons = NSMutableArray()
            var buttonTable = [String: UIButton]()
            // 售后原因 1. 2. 3.
            var typeTitles = ["换货","退货","仅退款"]
            for index in 1...3 {
                let button = UIButton()
                if index == 3
                {
                    button.tag = 4
                }else
                {
                    button.tag = index
                }
                button.setTitle(typeTitles[index-1], for: UIControlState())
                button.titleLabel?.font = UIFont.systemFont(ofSize: 13)
                let _ = button.handle(events: .touchUpInside, withBlock: {[weak self] (sender, event) -> Void in
                    for index in 1...3 {
                        if let button: UIButton = typeButtons.object(at: index-1) as? UIButton {
                            button.setTitleColor(UIColor.lightGray, for: UIControlState())
                            button.setBackgroundImage(UIImage(named: "goods_order_info_attribute_button"), for: UIControlState())
                        }
                    }
                    
                    (sender as! UIButton).setTitleColor(UIColor(rgba: "#DE383C"), for: UIControlState())
                    (sender as! UIButton).setBackgroundImage(UIImage(named: "goods_order_info_attribute_button_selected"), for: UIControlState())
                    
                    self?.serviceType = sender.tag
                    })
                
                typeButtons.add(button)
                buttonTable["bb\(index)"] = button
                
                self.addSubview(button)
                
                button.snp_makeConstraints(closure: { (make) -> Void in
                    let _ = make.top.equalTo(typeLabel.snp_bottom).offset(MARGIN_13)
                    let _ = make.height.equalTo(32)
                })
            }
            
            buttonTable["bb1"]?.snp_makeConstraints(closure: { (make) -> Void in
                let _ = make.left.equalTo(self).offset(MARGIN_13)
                let _ = make.width.equalTo(buttonTable["bb2"]!.snp_width)
            })
            
            buttonTable["bb2"]?.snp_makeConstraints(closure: { (make) -> Void in
                let _ = make.left.equalTo(buttonTable["bb1"]!.snp_right).offset(MARGIN_13)
                let _ = make.width.equalTo(buttonTable["bb3"]!.snp_width)
            })
            
            buttonTable["bb3"]?.snp_makeConstraints(closure: { (make) -> Void in
                let _ = make.left.equalTo(buttonTable["bb2"]!.snp_right).offset(MARGIN_13)
                let _ = make.width.equalTo(buttonTable["bb1"]!.snp_width)
                let _ = make.right.equalTo(self).offset(-MARGIN_13)
            })
            
            buttonTable["bb1"]?.sendActions(for: .touchUpInside)
            
            let sep0 = separatorLine()
            self.addSubview(sep0)
            sep0.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(buttonTable["bb1"]!.snp_bottom).offset(MARGIN_13)
                let _ = make.left.equalTo(self).offset(MARGIN_13)
                let _ = make.right.equalTo(self).offset(-MARGIN_13)
            }
            
            
            // 原因
            self.addSubview(reasonView)
            reasonView.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(sep0.snp_bottom)
                let _ = make.left.equalTo(sep0)
                let _ = make.right.equalTo(sep0)
                let _ = make.height.equalTo(44)
            }
            
            let sep1 = separatorLine()
            self.addSubview(sep1)
            sep1.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(reasonView.snp_bottom)
                let _ = make.left.equalTo(self).offset(MARGIN_13)
                let _ = make.right.equalTo(self).offset(-MARGIN_13)
            }
            
            // 问题描述
            
            let desLabel = UILabel()
            
            desLabel.font = UIFont.systemFont(ofSize: TITLESize)
            desLabel.textColor = UIColor.black
            desLabel.text = "问题描述"
            
            desTextView.layer.borderColor = UIColor(rgba: "#E8E8E8").cgColor
            desTextView.layer.borderWidth = 0.5
            desTextView.backgroundColor = UIColor(rgba: "#F4F4F4")
            desTextView.isScrollEnabled = false
            desTextView.delegate = self
            
            self.addSubview(desLabel)
            self.addSubview(desTextView)
            desLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(self).offset(MARGIN_13)
                let _ = make.top.equalTo(sep1.snp_bottom).offset(MARGIN_13)
            }
            
            desTextView.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(desLabel.snp_bottom).offset(MARGIN_13)
                let _ = make.left.equalTo(self).offset(MARGIN_13)
                let _ = make.right.equalTo(self).offset(-MARGIN_13)
                let _ = make.height.greaterThanOrEqualTo(48)
            }
            
            let sep2 = separatorLine()
            self.addSubview(sep2)
            sep2.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(desTextView.snp_bottom).offset(MARGIN_13)
                let _ = make.left.equalTo(self).offset(MARGIN_13)
                let _ = make.right.equalTo(self).offset(-MARGIN_13)
            }
            
            // 上传照片
            let upImageLabel = UILabel()
            upImageLabel.font = UIFont.systemFont(ofSize: TITLESize)
            upImageLabel.textColor = UIColor.black
            upImageLabel.text = "问题描述"
            
            self.addSubview(upImageLabel)
            self.addSubview(imageSelectView)
            imageSelectView.vc = vc
            
            upImageLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(sep2.snp_bottom).offset(MARGIN_13)
                let _ = make.left.equalTo(self).offset(MARGIN_13)
            }
            
            imageSelectView.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(upImageLabel.snp_bottom).offset(MARGIN_13)
                let _ = make.left.equalTo(self).offset(MARGIN_13)
                let _ = make.right.equalTo(self).offset(-MARGIN_13)
                let _ = make.bottom.equalTo(self)
            }
            
            
        }
        
        
        required init(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func separatorLine()->UIView {
            let view = UIView()
            view.backgroundColor = UIColor(rgba: Constant.common_separatLine_color)
            
            view.snp_makeConstraints { (make) -> Void in
                let _ = make.height.equalTo(0.5)
            }
            
            return view
        }
    }
    
    fileprivate class ReasonCell: UIView {
        var reasonField = UITextField()
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            
            let label = UILabel()
            label.font = UIFont.systemFont(ofSize: TITLESize)
            label.textColor = UIColor.black
            
            label.text = "申请售后的原因"
            
            reasonField.font = UIFont.systemFont(ofSize: TITLESize)
            reasonField.textColor = UIColor.darkText
            reasonField.isEnabled = false
            
            reasonField.clearButtonMode = UITextFieldViewMode.whileEditing
            
            reasonField.placeholder = "请选择"
            
            let image = UIImage(named: "home_module_topic_rightarrow")!
            let accessorView = UIImageView(image: image)
            
            self.addSubview(label)
            self.addSubview(reasonField)
            self.addSubview(accessorView)
            label.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(self)
                let _ = make.centerY.equalTo(self)
            }
            
            accessorView.snp_makeConstraints { (make) -> Void in
                let _ = make.right.equalTo(self)
                let _ = make.centerY.equalTo(self)
                let _ = make.height.equalTo(16)
                let _ = make.width.equalTo(accessorView.snp_height).multipliedBy(image.size.width / image.size.height)
            }
            
            reasonField.snp_makeConstraints { (make) -> Void in
                let _ = make.centerY.equalTo(self)
                let _ = make.right.equalTo(accessorView.snp_left).offset(-8)
            }
            
        }
        
        
        required init(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    // MARK: - 图片上传选择界面，选择后，每张图片单独上传，上传成功后添加到 images 中，如果 images.count 与 imageButtons.count 不相等，则说明还有图片正在上传
    fileprivate final class ImageSelectView: UIView,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIPopoverControllerDelegate {
        var picker:UIImagePickerController?=UIImagePickerController()
        var imageButtons = NSMutableArray()
        var vc:UIViewController?
        var images = [String]()
        var uploadDone: Bool {
            return images.count == imageButtons.count
        }
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            // 默认的按钮
            let imageButton = UIImageView(image: UIImage(named: "upload_image_button"))
            imageButtons.add(imageButton)
            imageButton.isUserInteractionEnabled = true
            imageButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ImageSelectView.selectImage)))
        }
        
        required init(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        override func updateConstraints() {
            super.updateConstraints()
            for view in self.subviews {
                view.removeFromSuperview()
            }
            
            var lastView: UIView?
            for index in 0..<self.imageButtons.count {
                if let button = self.imageButtons.object(at: index) as? UIView {
                    
                    self.addSubview(button)
                    
                    button.snp_makeConstraints(closure: { (make) -> Void in
                        let _ = make.height.equalTo(button.snp_width)
                        let _ = make.width.equalTo(self).multipliedBy(0.2).offset(-4)
                        let _ = make.centerX.equalTo(self).multipliedBy(2 * (CGFloat(index%5) * 0.2 + 0.1))
                        
                        if lastView == nil
                        {
                            let _ = make.top.equalTo(self)
                        }else{
                            let _ = make.top.equalTo(lastView!.snp_bottom).offset(MARGIN_13)
                        }
                    })
                    
                    if index%5 == 4 {
                        lastView = button
                    }
                }
            }
            if let lastView = self.imageButtons.lastObject as? UIView {
                
                lastView.snp_makeConstraints(closure: { (make) -> Void in
                    let _ = make.bottom.equalTo(self).offset(-MARGIN_13)
                })
            }
        }
        
        // 选择并上传图片
        @objc func selectImage() {
            picker?.delegate = self
            picker!.allowsEditing = true
            LCActionSheet(title: nil, buttonTitles: ["拍照","我的相册"], redButtonIndex: -1) { (buttonIndex) -> Void in
                
                if buttonIndex == 1
                {
                    let authStatus:AVAuthorizationStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
                    
                    if(authStatus == AVAuthorizationStatus.denied || authStatus == AVAuthorizationStatus.restricted) {
                        let alertController = UIAlertController(title: "温馨提示",
                                                                message: "请到“设置-隐私-相机”中打开心意点点获取手机相机的授权才能使用此功能", preferredStyle: .alert)
                        let okAction = UIAlertAction(title: "好的", style: .default,
                                                     handler: {
                                                        action in
                                                        return
                        })
                        
                        alertController.addAction(okAction)
                         NotificationCenter.default.post(name: Notification.Name(rawValue: "xiangji"), object: alertController)
                    }else {
                        self.openCamera()
                    }
                }
                if buttonIndex == 2
                {
                    let library:PHAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
                    if(library == PHAuthorizationStatus.denied || library == PHAuthorizationStatus.restricted){
                        let alertController = UIAlertController(title: "温馨提示",
                                                                message: "请到“设置-隐私-照片”中打开心意点点读取手机相册的授权才能使用此功能", preferredStyle: .alert)
                        let okAction = UIAlertAction(title: "好的", style: .default,
                                                     handler: {
                                                        action in
                                                        return
                        })
                        
                        alertController.addAction(okAction)
                        NotificationCenter.default.post(name: Notification.Name(rawValue: "xiangce"), object: alertController)

                    }else {
                        
                        self.openGallary()
                    }
                }
                
                }.show()
            
        }
        
        func openCamera()
        {
            if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
            {
                BaseViewController.staticNavigationBarAppearance(true)
                picker!.sourceType = UIImagePickerControllerSourceType.camera
                picker!.cameraViewTransform = CGAffineTransform(scaleX: 1.5, y: 1.5)//缩放控制
                UIApplication.shared.keyWindow?.rootViewController?.present(picker!, animated: true, completion: nil)
            }
            else
            {
                openGallary()
            }
        }
        func openGallary()
        {
            picker!.sourceType = UIImagePickerControllerSourceType.photoLibrary
            BaseViewController.staticNavigationBarAppearance(true)
            UIApplication.shared.keyWindow?.rootViewController?.present(picker!, animated: true, completion: nil)
            
        }
        @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
        {
            UINavigationBar.appearance().tintColor = UIColor(rgba: Constant.titlebar_color)
            picker .dismiss(animated: true, completion: nil)
            if let imageOri : UIImage = info[UIImagePickerControllerEditedImage] as? UIImage{
                DispatchQueue.global(qos: .default).async{
                    let newSize : CGSize = CGSize(width: Constant.ScreenSize.SCREEN_WIDTH, height: Constant.ScreenSize.SCREEN_WIDTH)
                    UIGraphicsBeginImageContext(newSize)
                    imageOri.draw(in: CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: Constant.ScreenSize.SCREEN_WIDTH))
                    // Get the new image from the context
                    let image : UIImage = UIGraphicsGetImageFromCurrentImageContext()!;
                    // End the context
                    UIGraphicsEndImageContext();
                    DispatchQueue.main.async { // 2
                        self.uploadHeaderImage(image)
                    }
                }
                
            }
            else
            {
                return
            }
            
            
            
        }
        @objc func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
        {
            UINavigationBar.appearance().tintColor = UIColor(rgba: Constant.titlebar_color)
            picker .dismiss(animated: true, completion: nil)
        }
        
        
        
        func uploadHeaderImage(_ image: UIImage?){
            if image == nil {return}
            
            // 添加待上传图片
            let imageView = UIImageView(image: image!)
            imageView.clipsToBounds = true
            imageView.contentMode = UIViewContentMode.scaleAspectFill
            imageButtons.insert(imageView, at: 0)
            self.setNeedsUpdateConstraints()
            let data = UIImageJPEGRepresentation(image!, 0.8)
            ImageUploader.uploadimage(context:self,data: data!) { [weak self](id,domain) -> Void in
                DispatchQueue.main.async(execute: { () -> Void in
                    if let imageid = id?.lastPathComponent {
                        self?.images.append(imageid)
                    }else{
                        // 上传失败，则不再显示该图片
                        self?.imageButtons.remove(imageView)
                        self?.setNeedsUpdateConstraints()
                    }
                })
            }
        }
    }
}

extension OrderServiceViewController {
    fileprivate final class CommitSuccessView: UIView {
        
        var messageLabel = UILabel()
        
        var orderId: Int = 0 {
            didSet {
                messageLabel.text = String(format: "您的订单 %d 售后信息已成功提交，点心将在72小时内对售后订单做出处理，敬请耐心等待！谢谢！",orderId)
            }
        }
        override init(frame: CGRect) {
            super.init(frame: frame)
            self.backgroundColor = UIColor(rgba: Constant.common_background_color)
            
            let statusImage = UIImageView(image: UIImage(named: "choosed"))
            let statusLabel = UILabel()
            statusLabel.text = "提交成功"
            statusLabel.font = UIFont.systemFont(ofSize: 17)
            statusLabel.textColor = UIColor(rgba: "#DE383C")
            
            messageLabel.numberOfLines = 0
            messageLabel.font = UIFont.systemFont(ofSize: 15)
            self.addSubview(statusImage)
            self.addSubview(statusLabel)
            self.addSubview(messageLabel)
            // 布局
            statusImage.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(self).offset(18)
                let _ = make.centerX.equalTo(self)
                let _ = make.height.equalTo(statusImage.snp_width)
                let _ = make.width.equalTo(self).multipliedBy(0.1)
            }
            
            statusLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(statusImage.snp_bottom).offset(8)
                let _ = make.centerX.equalTo(self)
            }
            
            messageLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(statusLabel.snp_bottom).offset(18)
                let _ = make.left.equalTo(self).offset(MARGIN_13)
                let _ = make.right.equalTo(self).offset(-MARGIN_13)
            }
            
        }
        
        required init(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
    }
}
