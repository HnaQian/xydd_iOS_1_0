//
//  BaseOrderInfoViewController.swift
//  Limi
//
//  Created by Richie on 16/3/8.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//填写订单和订单详情的父视图

import UIKit

class BaseOrderInfoViewController: BaseViewController,OrderDetailTableCellDelegate {

    var tableView:UITableView{return _tableView}
    
    fileprivate let _tableView = UITableView()
    
    var cellReuseIdentifier = "baseOrderTableViewCell"

    var goodsDataList = [GoodsGenericModel](){didSet{_tableView.reloadData()}}
    
    var header:UIView?{didSet{self._tableView.tableHeaderView = header}}
    
    var footer:UIView?{didSet{self._tableView.tableFooterView = footer}}
    
    
    func refreshTableView(){
        _tableView.reloadData()
    }
    
    
    func setupUI()
    {
        _tableView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.view)
            let _ = make.bottom.equalTo(self.view)
            let _ = make.left.equalTo(self.view)
            let _ = make.right.equalTo(self.view)
        }
    }
    
    func registerClass(_ cell:BaseOrderTableViewCell.Type, forCellReuseIdentifier: String)
    {
        self.cellReuseIdentifier = forCellReuseIdentifier
        
        _tableView.register(cell, forCellReuseIdentifier: forCellReuseIdentifier)
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
       
        self.superSetupUI()
        // Do any additional setup after loading the view.
    }

    func orderDetailTableCellOnClickSlod(_ goodsGenericModel:GoodsGenericModel){}
    func orderDetailTableCellOnClickShowTransport(_ goodsGenericModel:GoodsGenericModel){}
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}


extension BaseOrderInfoViewController:UITableViewDelegate,UITableViewDataSource{

    fileprivate func superSetupUI()
    {
        self.view.addSubview(_tableView)
        
        self._tableView.backgroundColor = UIColor.clear
        
        _tableView.showsHorizontalScrollIndicator = false
        _tableView.showsVerticalScrollIndicator = false
        _tableView.register(BaseOrderTableViewCell.self, forCellReuseIdentifier: self.cellReuseIdentifier)
        
        _tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        _tableView.backgroundColor = UIColor.clear
        
        _tableView.dataSource = self
        _tableView.delegate = self
        
        self.setupUI()
    }

    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.goodsDataList.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellReuseIdentifier) as! BaseOrderTableViewCell
        
        cell.reload(self.goodsDataList[(indexPath as NSIndexPath).row])
        
        cell.delegate = self
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    
    //header
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = UIColor.white
        if self.goodsDataList.count > 0 {
            view.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: 50)
            let label = UILabel()
            label.frame = CGRect(x: 20 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH - 40, height: 50)
            label.font = UIFont.systemFont(ofSize: Constant.common_F3_font)
            label.textColor = Constant.Theme.Color17
            label.text = "您选购的礼物"
            view.addSubview(label)
        }
        
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if self.goodsDataList.count > 0 {
            return 50
        }
        return 0
    }
    
    
    //footer
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
}
