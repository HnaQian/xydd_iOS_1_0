//
//  BaseGoodsListView.swift
//  Limi
//
//  Created by Richie on 16/3/8.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

//价格清单 :礼品金额、心意服务、优惠、运费
class ExpensesSummaryView: BaseUnitView
{
    //private
    let goodsCostLbl = UnitView()
    let wishSendLbl = UnitView()
    let disCountLbl = UnitView()
    let transportLbl = UnitView()
    
    //pulic
    var goodsCost = NULL_DOUBLE{didSet{self.goodsCostLbl.content = "￥\(String(format: "%.2lf", goodsCost))"}}
    var wishSendCost = NULL_DOUBLE{didSet{self.wishSendLbl.content = "+￥\(String(format: "%.2lf", wishSendCost))"}}
    var disCountCost = NULL_DOUBLE{didSet{self.disCountLbl.content = "-￥\(String(format: "%.2lf", disCountCost))"}}
    var transportCost = NULL_DOUBLE{didSet{self.transportLbl.content = "+￥\(String(format: "%.2lf", transportCost))"}}
    
    convenience init()
    {
        self.init(frame:CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: 120))
        
        self.setupUI()
    }
    
    
    func reloadData(_ data:OrderDataManager)
    {
        self.goodsCost = data.totalCost
        self.wishSendCost = data.wishSendCost
        self.disCountCost = data.discountCost
        self.transportCost = data.transportCost
    }
    
    
    
    override func setupUI()
    {
        self.backgroundColor = UIColor.white
        
        let titles = ["礼品金额","心意服务","优惠","运费"]
        
        let contentLbls = [goodsCostLbl,wishSendLbl,disCountLbl,transportLbl]
        let _ =  contentLbls.map{$0.clipsToBounds = true}
        
        var tempLbl:UnitView!
        
        for index in 0...3
        {
            let contentLbl = contentLbls[index]
            contentLbl.iheight = 20
            
            self.addSubview(contentLbl)
            
            contentLbl.title = titles[index]
            contentLbl.setTitleLblAttrubutes(isBolderFont:true)
            contentLbl.setContentLblAttrubutes("", font: Constant.common_F4_font, color: UIColor(rgba: Constant.common_red_color))
            
            contentLbl.snp_makeConstraints(closure: { (make) -> Void in
                let _ = make.left.equalTo(self)
                let _ = make.right.equalTo(self)
                let _ = make.height.equalTo(contentLbl.iheight)
                
                if index == 0
                {
                    let _ = make.top.equalTo(self).offset(MARGIN_8)
                }else
                {
                    let _ = make.top.equalTo(tempLbl.snp_bottom).offset(MARGIN_8)
                }
            })
            
            tempLbl = contentLbl
        }
        
        tempLbl.snp_updateConstraints { (make) -> Void in
            let _ = make.bottom.equalTo(self).offset(-MARGIN_8)
        }
    }
}

//tableview的两端View
class EndsViewOfTable:BaseUnitView {
    
    init(height:CGFloat)
    {
        super.init(frame: CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: height))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

@objc protocol OrderUnitViewDelegate:NSObjectProtocol
{
    @objc optional func orderReceiveAddressViewTapAction(_ receiveView:ReceiveAddressView)
    
    @objc optional func orderUnitViewTapAction(_ unitView:BaseUnitView)
    
    @objc optional func orderBottomToolBarAction(_ tooBar:BottomToolsBar)
    
    @objc optional func orderUnitViewBillButtonClicked()
}
