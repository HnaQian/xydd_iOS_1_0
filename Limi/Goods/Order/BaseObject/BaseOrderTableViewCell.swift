//
//  BaseOrderTableViewCell.swift
//  Limi
//
//  Created by Richie on 16/3/8.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//展示订单信息cell的基类

import UIKit

class BaseOrderTableViewCell: UITableViewCell {

    weak var delegate:BaseOrderTableViewCellDelegate?
    
    var edgLineModes = [EdgLineMode](){didSet
    {
        if edgLineModes.count == 0{return}
        
        for mode in edgLineModes
        {
            let line = UIView()
            line.backgroundColor = UIColor(rgba: Constant.common_separatLine_color)
            self.addSubview(line)
            
            line.snp_makeConstraints(closure: { (make) -> Void in
                let _ = make.height.equalTo(0.5)
                
                switch (mode)
                {
                case .topLongLine:
                    let _ = make.top.equalTo(self)
                    let _ = make.right.equalTo(self)
                    let _ = make.left.equalTo(self)
                    
                case .topShortLine:
                    let _ = make.top.equalTo(self)
                    let _ = make.right.equalTo(self).offset(-MARGIN_8)
                    let _ = make.left.equalTo(self).offset(MARGIN_8)
                    
                case .bottomLongLine:
                    let _ = make.bottom.equalTo(self)
                    let _ = make.right.equalTo(self)
                    let _ = make.left.equalTo(self)
                    
                case .bottomShortLine:
                    let _ = make.bottom.equalTo(self)
                    let _ = make.right.equalTo(self).offset(-MARGIN_8)
                    let _ = make.left.equalTo(self).offset(MARGIN_8)
                }
            })
        }
        
        }
    }
    
    
    
    //MARK:private
    let goodsImg = UIImageView()
    let titleLbl = UILabel()
    let priceLbl = UILabel()
    let countLbl = UILabel()
    //规格
    let attributeLbl = UILabel()
    
    var goodsModel:GoodsGenericModel?{return _goodsModel}
    
    var _goodsModel:GoodsGenericModel?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.superSetupUI()
    }
    
    
    fileprivate func superSetupUI()
    {
        let _ = [goodsImg,titleLbl,attributeLbl,priceLbl,countLbl].map{self.addSubview($0)}
        
        self.selectionStyle = UITableViewCellSelectionStyle.none
        
        titleLbl.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        titleLbl.textColor = UIColor(rgba: Constant.common_C2_color)
        
        priceLbl.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        priceLbl.textColor = UIColor(rgba: Constant.common_C2_color)
        
        countLbl.font = UIFont.systemFont(ofSize: Constant.common_F5_font)
        countLbl.textColor = UIColor(rgba: Constant.common_C6_color)
        
        attributeLbl.font = UIFont.systemFont(ofSize: Constant.common_F5_font)
        attributeLbl.textColor = UIColor(rgba: Constant.common_C6_color)
        
        goodsImg.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self).offset(MARGIN_8)
            let _ = make.left.equalTo(self).offset(MARGIN_8)
            let _ = make.bottom.equalTo(self).offset(-MARGIN_8)
            let _ = make.width.equalTo(goodsImg.snp_height)
        }
        
        titleLbl.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(goodsImg)
            let _ = make.left.equalTo(goodsImg.snp_right).offset(MARGIN_8)
            let _ = make.right.equalTo(self.snp_right).offset(-MARGIN_8)
            let _ = make.height.equalTo(20)
        }
        
        attributeLbl.snp_makeConstraints { (make) -> Void in
            let _ = make.centerY.equalTo(goodsImg)
            let _ = make.left.equalTo(titleLbl)
            let _ = make.height.equalTo(20)
        }
        
        priceLbl.snp_makeConstraints { (make) -> Void in
            let _ = make.bottom.equalTo(goodsImg)
            let _ = make.height.equalTo(20)
            let _ = make.left.equalTo(titleLbl)
        }
        
        countLbl.snp_makeConstraints { (make) -> Void in
            let _ = make.left.equalTo(attributeLbl.snp_right).offset(MARGIN_4)
            let _ = make.centerY.equalTo(attributeLbl)
            let _ = make.height.equalTo(20)
        }
        
        self.setupUI()
    }
    
    func setupUI(){}
    
    func reload(_ data: GoodsGenericModel){

        _goodsModel = data
        
        let imageScale : String = data.goodsImage as String + "?imageView2/0/w/" + "\(Int(80*Constant.ScreenSize.SCALE_SCREEN))" + "/h/" + "\(Int(80*Constant.ScreenSize.SCALE_SCREEN))"
        
        self.goodsImg.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "loadingDefault"))
        
        titleLbl.text = data.goodsName
        priceLbl.text = String(format: "￥%.2lf", data.selelctedGoodsSubModelPrice * Double(data.selectedGoodsSubModelCount))
        
        countLbl.text = "x\(data.selectedGoodsSubModelCount)"
        
        var nameString = ""
        if data.selectedGoodsAttrNames.count > 0{
            var index = 0
            for name in data.selectedGoodsAttrNames{
                if index == 0{
                    nameString = name
                }else{
                    nameString = nameString + "；" + name
                }
                index = index + 1
            }
        }
        
        self.attributeLbl.text = nameString
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


@objc protocol BaseOrderTableViewCellDelegate:NSObjectProtocol{

}
