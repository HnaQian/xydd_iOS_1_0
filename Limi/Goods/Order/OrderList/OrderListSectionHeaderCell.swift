//
//  OrderListSectionHeaderCell.swift
//  Limi
//
//  Created by Richie on 16/3/18.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class OrderListSectionHeaderCell: UITableViewCell {
    
    static let SECTION_HEADER_HEIGHT:CGFloat = 28 + SECTION_GAP
    
    static let SECTION_GAP:CGFloat = 8
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.setupUI()
    }
    
    
    fileprivate let lbl = UILabel()
    fileprivate let timelbl = UILabel()
    fileprivate let gapView = UIView()
    
    func setupUI() {
        self.iheight = OrderListSectionHeaderCell.SECTION_HEADER_HEIGHT
        self.selectionStyle = UITableViewCellSelectionStyle.none
        self.addSubview(lbl)
        self.addSubview(timelbl)
        self.addSubview(gapView)
        
        lbl.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        timelbl.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        
        lbl.textColor = UIColor(rgba: Constant.common_C6_color)
        timelbl.textColor = UIColor(rgba: Constant.common_C6_color)
        
        gapView.layer.borderWidth = 0.5
        gapView.layer.borderColor = UIColor(rgba: Constant.common_separatLine_color).cgColor
        gapView.backgroundColor = UIColor(rgba: Constant.common_background_color)
        
        lbl.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(gapView.snp_bottom)
            let _ = make.left.equalTo(self).offset(MARGIN_8)
            let _ = make.height.equalTo(OrderListSectionHeaderCell.SECTION_HEADER_HEIGHT - OrderListSectionHeaderCell.SECTION_GAP)
        }
        
        timelbl.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(gapView.snp_bottom)
            let _ = make.left.equalTo(self.lbl.snp_right)
            let _ = make.right.equalTo(self).offset(-MARGIN_8)
            let _ = make.height.equalTo(OrderListSectionHeaderCell.SECTION_HEADER_HEIGHT - OrderListSectionHeaderCell.SECTION_GAP)
        }
        
        gapView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self)
            let _ = make.left.equalTo(self).offset(-1)
            let _ = make.right.equalTo(self).offset(1)
            let _ = make.height.equalTo(OrderListSectionHeaderCell.SECTION_GAP)
        }
    }
    
    func reload(_ data: OrderModel) {
        lbl.text = "订单 \(data.oid)"
        timelbl.text = data.add_time
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
