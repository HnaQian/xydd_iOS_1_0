//
//  OrderListSectionFooterCell.swift
//  Limi
//
//  Created by Richie on 16/3/18.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class OrderListSectionFooterCell: UITableViewCell,OrderUnitViewDelegate {
    
    
    fileprivate var orderModel: OrderModel!
    
    weak var delegate:OrderListSectionFooterCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    static let SECTION_FOOTER_HEIGHT:CGFloat = 44
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    fileprivate let bar = BottomToolsBar()
    
    func setupUI() {
        
        self.addSubview(bar)
        self.selectionStyle = UITableViewCellSelectionStyle.none
        bar.delegate = self
        bar.snp_makeConstraints { (make) -> Void in
            let _ = make.right.equalTo(self)
            let _ = make.left.equalTo(self)
            let _ = make.centerY.equalTo(self)
            let _ = make.height.equalTo(OrderListSectionFooterCell.SECTION_FOOTER_HEIGHT)
        }
        
        bar.setTitle(UIColor(rgba:Constant.common_C6_color), textFont: Constant.common_F3_font)
        bar.actionBtnStyle = 1
    }
    
    
    func reload(_ data: OrderModel) {
        
        self.orderModel = data
        
        bar.title = "共\(data.goods.count)件礼物" + " 实付￥" + "\(data.actual_price)"
        
        if data.order_status == 1{
            
            bar.actionBtnStyle = 1
            bar.actionBtnTitle = "去支付"
        }else if data.order_status > 1 && data.order_status < 6{
            
            if data.order_type == 1 {
                bar.actionBtnStyle = 1
                bar.actionBtnTitle = "再次购买"
            }else {
                bar.actionBtnStyle = 3
            }
            
        }else{
            bar.actionBtnStyle = 3
            
        }
    }
    
    
    func orderBottomToolBarAction(_ tooBar: BottomToolsBar) {
        
        if tooBar.actionType == .actionBtnTouch{
            if let del = delegate{
                del.orderListSectionFooterCellAction(orderModel)
            }
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
}

protocol OrderListSectionFooterCellDelegate:NSObjectProtocol{
    func orderListSectionFooterCellAction(_ orderModel: OrderModel)
}
