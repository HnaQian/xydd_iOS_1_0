//
//  OrderListTableViewCell.swift
//  Limi
//
//  Created by Richie on 16/3/18.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class OrderListTableViewCell: BaseOrderTableViewCell {
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
    // Drawing code
    }
    */
    
    let orderStautsLbl = UILabel()
    
    override func setupUI() {
        self.addSubview(orderStautsLbl)
        orderStautsLbl.textColor = UIColor(rgba: Constant.common_C2_color)
        orderStautsLbl.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        orderStautsLbl.textAlignment = NSTextAlignment.right
        
        orderStautsLbl.snp_makeConstraints { (make) -> Void in
            let _ = make.right.equalTo(self).offset(-MARGIN_8)
            let _ = make.width.equalTo(50)
            let _ = make.top.equalTo(self).offset(MARGIN_8)
            let _ = make.height.equalTo(20)
        }

        titleLbl.snp_remakeConstraints { (make) -> Void in
            let _ = make.top.equalTo(goodsImg)
            let _ = make.left.equalTo(goodsImg.snp_right).offset(MARGIN_8)
            let _ = make.right.equalTo(orderStautsLbl.snp_left).offset(-MARGIN_4)
            let _ = make.height.equalTo(20)
        }
        
        self.edgLineModes = [EdgLineMode.topShortLine,EdgLineMode.bottomShortLine]
    }
    
    //0全部订单 1未付款订单 2待收货订单 3已完成订单
    //1待付款 2待发货 3待收货 4已完成 5售后中 6已关闭；话费订单：1待付款 2待充值 4已完成 6已关闭
    fileprivate let goodsStatus_name_map = [1:"待付款",2:"待发货",3:"待收货",4:"已完成",5:"售后中",6:"已关闭"]
    
    fileprivate let status_name_map = [1:"待付款",2:"待充值",3:"待收货",4:"已完成",5:"售后中",6:"已关闭"]
    
    override func reload(_ data: GoodsGenericModel) {
        
        super.reload(data)
        
        if data.isKind(of: GoodsOfOrderModel.self){
            priceLbl.text = String(format: "￥%.2lf",(data as! GoodsOfOrderModel).totalCost)
        }else{
            priceLbl.text = String(format: "￥%.2lf", data.selelctedGoodsSubModelPrice)
        }
        
        
        if data.goodsOfOrderStatus > 0 && data.goodsOfOrderStatus < 7{
            
            if data.goodsOfOrderType == 1{
                self.orderStautsLbl.text = goodsStatus_name_map[data.goodsOfOrderStatus!]
            }else{
                self.orderStautsLbl.text = status_name_map[data.goodsOfOrderStatus!]
            }
        }
    }
    
}
