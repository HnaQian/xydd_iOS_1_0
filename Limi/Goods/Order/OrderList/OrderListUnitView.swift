//
//  OrderListUnitView.swift
//  Limi
//
//  Created by Richie on 16/3/17.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//我的订单列表

import UIKit

enum OrderListType:Int{
    case all
    case obligations
    case receipt
    case done
}

private let ReusableCELLIdentifier = "orderListTableViewCell"

private let ReusableFOOTERIdentifier = "orderListTableViewCellFooter"

private let ReusableHEADERIdentifier = "orderListTableViewCellHeader"

private let CELL_HEIGHT:CGFloat = 75

private let OrderTypeNameAry = ["全部","待付款","待收货","已完成"]


class OrderListUnitView: UIView,UITableViewDelegate,UITableViewDataSource,OrderRequestManagerDelegate,OrderListSectionFooterCellDelegate {
    
    fileprivate let tableView = UITableView()
    
    fileprivate var listType:OrderListType!
    
    fileprivate let requestManager = OrderRequestManager()
    
    fileprivate var orderList = [OrderModel]()
    
//    fileprivate var didAcutoLoad = false
    
    weak var delegate:OrderListUnitViewDelegate?
    
    init(orderType:OrderListType) {
        super.init(frame: CGRect.zero)
        self.listType = orderType
        
        self.setupUI()
    }
    
    func selected(_ index:Int)
    {
        if index == listType.rawValue{
//            if didAcutoLoad == false{
                self.tableView.beginHeaderRefresh()
//                didAcutoLoad = true
//            }
        }
    }
    
    func reloadData()
    {
        self.requestManager.orderList(self,status: self.listType.rawValue)
    }
    
    func orderRequestManagerDidLoadListFinish(_ manager: LMRequestManager, result: LMResultMode, list: [OrderModel]) {
        self.orderList = list
        self.tableView.reloadData()
        if self.orderList.count  == 0{
            self.tableView.showTipe(UIScrollTipeMode.nullData).tipeContent("您还没有\(OrderTypeNameAry[listType.rawValue])订单")
        }else{
            if list.count < 10 {
                tableView.showTipe(UIScrollTipeMode.noMuchMore)
            }
            tableView.endHeaderRefresh()
        }
    }
    
    func orderRequestManagerDidLoadMoreListFinish(_ manager: LMRequestManager, result: LMResultMode, list: [OrderModel]) {
        
        
        if list.count > 0{
            let _ = list.map{self.orderList.append($0)}
            tableView.reloadData()
            tableView.endFooterRefresh()
            if list.count < 10 {
                tableView.showTipe(UIScrollTipeMode.noMuchMore)
            }
            
        }else{
            tableView.showTipe(UIScrollTipeMode.noMuchMore)
        }
    }
    
    
    func networkTaskDidFail(_ networkTask: LMRequestManager, error: NSError?, msg: String?) {
        self.tableView.showTipe(UIScrollTipeMode.nullData).tipeContent("网络错误")
        self.tableView.endFooterRefresh()
        self.tableView.endHeaderRefresh()
        
//        self.tableView.showTipe(UIScrollTipeMode.LostConnect).tipeContent("网络错误")
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return orderList.count
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return orderList[section].goods.count + 2
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell:UITableViewCell!
        
        if (indexPath as NSIndexPath).row == orderList[(indexPath as NSIndexPath).section].goods.count + 1
        {
            cell = tableView.dequeueReusableCell(withIdentifier: ReusableFOOTERIdentifier) as! OrderListSectionFooterCell
            (cell as! OrderListSectionFooterCell).reload(orderList[(indexPath as NSIndexPath).section])
            (cell as! OrderListSectionFooterCell).delegate = self
        }else if (indexPath as NSIndexPath).row == 0
        {
            cell = tableView.dequeueReusableCell(withIdentifier: ReusableHEADERIdentifier) as! OrderListSectionHeaderCell
            (cell as! OrderListSectionHeaderCell).reload(orderList[(indexPath as NSIndexPath).section])
        }
        else
        {
            cell = tableView.dequeueReusableCell(withIdentifier: ReusableCELLIdentifier) as! OrderListTableViewCell
            (cell as! OrderListTableViewCell).reload(orderList[(indexPath as NSIndexPath).section].goods[(indexPath as NSIndexPath).row - 1])
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (indexPath as NSIndexPath).row == orderList[(indexPath as NSIndexPath).section].goods.count + 1{
            return OrderListSectionFooterCell.SECTION_FOOTER_HEIGHT
        }
        else if (indexPath as NSIndexPath).row == 0{
            return OrderListSectionHeaderCell.SECTION_HEADER_HEIGHT
        }
        else{
            return CELL_HEIGHT
        }
    }
    
    
    func orderListSectionFooterCellAction(_ orderModel: OrderModel){
        
        if let del =  delegate{
            del.orderListSectionFooterCellAction(orderModel)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let _ = Delegate_Selector(delegate, Selector(("orderListUnitViewDidSelected:"))){Void in self.delegate!.orderListUnitViewDidSelected(self.orderList[(indexPath as NSIndexPath).section])}
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}



extension OrderListUnitView{
    
    func setupUI(){
        
        self.addSubview(tableView)
        
        tableView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self)
            let _ = make.bottom.equalTo(self)
            let _ = make.left.equalTo(self)
            let _ = make.right.equalTo(self)
        }
        
        tableView.register(OrderListTableViewCell.self, forCellReuseIdentifier: ReusableCELLIdentifier)
        tableView.register(OrderListSectionFooterCell.self, forCellReuseIdentifier: ReusableFOOTERIdentifier)
        tableView.register(OrderListSectionHeaderCell.self, forCellReuseIdentifier: ReusableHEADERIdentifier)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        tableView.backgroundColor = UIColor.clear
        
        requestManager.delegate = self
        tableView.addHeaderRefresh { [weak self]() -> Void in
            self?.reloadData()
        }
        
        tableView.addFooterRefresh { [weak self]() -> Void in
            self?.requestManager.moreOrderList(self!,lastId: 1, status: (self?.listType.rawValue)!)
        }
    }
}

protocol OrderListUnitViewDelegate:OrderListSectionFooterCellDelegate
{
    func orderListUnitViewDidSelected(_ orderModel:OrderModel)
}


