//
//  OrderListViewControllerV1.swift
//  Limi
//
//  Created by Richie on 16/3/17.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//我的订单列表

import UIKit

class OrderListViewControllerV1: BaseViewController,OrderListUnitViewDelegate,LazyPageScrollViewDelegate,GiftBasketRequestManagerDelegate {
    
    var outIndex = 0
    fileprivate var bageLabel = CustomLabel(font: Constant.common_F7_font, autoFont: 2, textColorHex: Constant.common_C12_color)
    fileprivate var scrollView = UIScrollView()
    fileprivate var pageScrollView = LazyPageScrollView(verticalDistance: 0, tabItemType: TabItemType.customView)
    
    fileprivate var currentIndex = 0{didSet{
        let _ = tableViewArray.map{$0.selected(currentIndex)}
        }}
    
    fileprivate var tableViewArray = [OrderListUnitView]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "我的订单"
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: Constant.orderList_changed), object: nil, queue: OperationQueue.main) {  (notification) -> Void in
            self.tableViewArray[self.currentIndex].selected(self.currentIndex)
        }
        self.setupUI()
    }
    
    
    func orderListUnitViewDidSelected(_ orderModel: OrderModel) {
        
        self.navigationController?.pushViewController(OrderDetailViewControllerV1.newDetailVC(orderModel.oid), animated: true)
    }
    
    
    func orderListSectionFooterCellAction(_ orderModel: OrderModel) {
        
        if orderModel.order_status == 1{
            self.navigationController?.pushViewController(PayViewController.newPayViewController(orderModel.oid, canclePay: CancleAction.forward), animated: true)
        }else{
           //再次购买
            var gids = [Int]()
            var subIds = [Int]()
            var num = [Int]()
            if orderModel.goods.count > 0 {
                for index in 0 ..< orderModel.goods.count {
                    if let temp0 = orderModel.goods[index].goodsId {
                        gids.append(temp0)
                    }
                    
                    if let temp1 = orderModel.goods[index].goodsSubId {
                        subIds.append(temp1)
                    }else {
                        subIds.append(0)
                    }
                    num.append(orderModel.goods[index].selectedGoodsSubModelCount)
                }
            }
            let giftBasketManager = GiftBasketRequestManager()
            giftBasketManager.delegate = self
            giftBasketManager.basketAdd(self.view,gids: gids,subIds: subIds,num: num,showError: true,isNeedHud: true)
        }
    }
    
    func giftBasketRequestManagerDidAdd(_ manager: LMRequestManager, result: LMResultMode) {
        let shoppingBasketVc = ShoppingBasketViewController()
        shoppingBasketVc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(shoppingBasketVc, animated: true)
    }
    
    func setupUI(){
        
        let types = [OrderListType.all,OrderListType.obligations,OrderListType.receipt,OrderListType.done]
        for index in 0...3
        {
            let unitView = OrderListUnitView(orderType:types[index])
            unitView.delegate = self
            tableViewArray.append(unitView)
        }
        self.view.addSubview(pageScrollView)
        var frame = self.view.bounds
        frame.size.height = frame.size.height - 64
        
        pageScrollView.segment(["全部", "待付款", "待收货", "已完成"], views: tableViewArray, frame: frame)
        
        bageLabel.isHidden = true
        bageLabel.iwidth = 20
        bageLabel.iheight = bageLabel.iwidth
        bageLabel.layer.cornerRadius = bageLabel.iwidth/2
        bageLabel.backgroundColor = UIColor(rgba:Constant.common_red_color)
        bageLabel.clipsToBounds = true
        
        pageScrollView.tabItem.addSubview(bageLabel)
        
        bageLabel.frame = CGRect(x: pageScrollView.tabItem.iwidth / 4, y: 5, width: bageLabel.iwidth, height: bageLabel.iheight)
        pageScrollView.delegate = self
        
        self.currentIndex = 0
//        (pageScrollView.tabItem as! LMSegmentedControl).currentIndex = outIndex
    }
    
    func lazyPageScrollView(_ pageScrollView: LazyPageScrollView, changeIndex index: Int) {
        currentIndex = index
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
