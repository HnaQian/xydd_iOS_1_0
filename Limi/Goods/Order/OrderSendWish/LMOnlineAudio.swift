//
//  LMOnlineAudio.swift
//  Limi
//
//  Created by maohs on 16/8/16.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit
import AVFoundation
class LMOnlineAudio: NSObject {
    static let shareInstance = LMOnlineAudio()
    fileprivate override init() {}
    
    var player:AVQueuePlayer?
    var songItem:AVPlayerItem?
    var onlineAudioFinished:(()->())?
    
    func playAudio(_ urlString:String) {
        let url = URL(string: urlString)
        songItem = AVPlayerItem(url: url!)
        if player != nil {
            player?.pause()
            player?.removeAllItems()
        }
        NotificationCenter.default.removeObserver(self)
        
        player = AVQueuePlayer(playerItem: songItem!)
        player?.play()
        NotificationCenter.default.addObserver(self, selector: #selector(LMOnlineAudio.playFinished), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: songItem!)

    }
    
    func stopAudio() {
        if let tempPlayer = self.player {
            tempPlayer.pause()
        }
    }
    
    func playFinished() {
        onlineAudioFinished?()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
