//
//  VoiceWishViewController.swift
//  Limi
//
//  Created by Richie on 16/3/1.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//语音祝福入口

import UIKit
import AVFoundation

let RECORD_DURATION:Int = 60

class VoiceWishViewController: BaseViewController,OrderRequestManagerDelegate {
    

    //录音管理器
    private let recoderManager:LMRecorder = LMRecorder.newRecorder()
    
    //引导语视图
    private let guid_Content = GuidView()
    
    //录音视图
    private let recordView_Content = RecordView()
    
    //录音按钮上方的提示语
    private let topTipeLbl = UILabel()
    
    private var _recordButton_Content:RecordButton!

    var timer:NSTimer!
    
    var count = 0
    
    //录音按钮
    private var recordButton_Content:RecordButton!{
        
        if _recordButton_Content == nil{
            
            _recordButton_Content = RecordButton(startRecord: {[weak self] in
                    //开始录音
                    self?.startRecord()
                }, end: { [weak self] in
                    //结束录音
                    self?.endRecord()
            })
        }

        return _recordButton_Content
    }
     
    //录音按钮下方的提示语
    private let bottomTipeLbl = UILabel()
    
    //录音完成按钮
    private var recordOverButton:UIButton = {
    
        let recordOverButton = UIButton(frame:CGRectMake(0,0,264 * Constant.ScreenSize.SCREEN_HEIGHT_SCALE,40 * Constant.ScreenSize.SCREEN_HEIGHT_SCALE))

        recordOverButton.hidden = true
        recordOverButton.layer.cornerRadius = 3
        recordOverButton.setTitle("录制完成", forState: UIControlState.Normal)
        recordOverButton.backgroundColor = UIColor(rgba: Constant.common_C12_color)
        
        recordOverButton.titleLabel?.font = UIFont.systemFontOfSize(Constant.common_F1_font)
        recordOverButton.setTitleColor(UIColor(rgba:Constant.common_C1_color), forState: UIControlState.Normal)
        
        return recordOverButton
    }()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationItem.title = "语音祝福"
        self.setupUI()
    }
    
    
    /**
     开始录音
     */
    private func startRecord()
    {
        print("buttonAction---》开始录音")
        if self.timer != nil{self.timer.invalidate();self.timer = nil}
        self.timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(VoiceWishViewController.clock), userInfo: nil, repeats: true)
        self.recoderManager.start()
        count = 0
        
        
        
        //刷新UI
//        self.recordView_Content.show()
//        self.guid_Content.dismiss()
        
        self.guid_Content.disPlay(false)
        
    }
    
    
    /**
     结束录音
     */
    private func endRecord()
    {
        print("buttonAction---》结束录音")
        self.recoderManager.end()
        if self.timer != nil{self.timer.invalidate();self.timer = nil}
        
        
        
        //刷新UI
        self.guid_Content.disPlay(count == 0)
        self.recordOverButton.hidden = count == 0
    }
    
    
    /**
     上传语音文件
     */
    @objc func uploadVoiceFile()
    {
        HudOfWaitingShow("正在上传")

        ImageUploader.uploadFile(Constant.JTAPI.voice_upload,data: NSData(contentsOfURL: self.recoderManager.fileUrl)!) { (id, domain) -> Void in
            if id == nil
            {
                HudOfWaitingDismiss()
                HudShow("录音上传失败", dismissDelay: 2)
            }
            else{
                OrderRequestManager(delegate:self).orderUpLoadVoice(id, second: self.count)
            }
        }
    }
    
    
    func orderRequestManagerDidVoiceAdd(manager: LMRequestManager, result: LMResultMode) {
        
        HudOfWaitingDismiss()

        if result.status == 200{
            HudShow("录音上传成功", dismissDelay: 2)
            print(result.data!["id"] as! Int)
            
            WishSendUnitView.updateVoiceId(result.data!["id"] as! Int)
            
            if let nav = self.navigationController{
                if nav.viewControllers.count > 1{
                    if let webVC = nav.viewControllers[nav.viewControllers.count - 2] as? WebViewController
                    {
                        webVC.webView.loadRequest(NSURLRequest(URL: WishSendUnitView.tempWishSendUrl))
                    }
                }
                
                nav.popViewControllerAnimated(true)
            }
            
        }else
        {
            HudShow("录音上传失败", dismissDelay: 2)
        }
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
    }

    
    @objc func clock(){count = count + 1}
    

    //试听录音
    @objc func recordViewTagAction()
    {
//        let _ = self.recoderManager.fileSize
//        self.recoderManager.play()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}


extension VoiceWishViewController:LMAudioPlayerDelegate
{
    
    private func setupUI()
    {
        self.view.backgroundColor = UIColor(rgba: Constant.common_red_color)
      
        for view in [recordOverButton,guid_Content,recordView_Content,topTipeLbl,recordButton_Content,bottomTipeLbl]{
            self.view.addSubview(view)
        }

        recordOverButton.addTarget(self, action: "uploadVoiceFile", forControlEvents: UIControlEvents.TouchUpInside)
        
        let _ = [topTipeLbl,bottomTipeLbl].map{$0.textAlignment = NSTextAlignment.Center;$0.textColor = UIColor(rgba:Constant.common_C12_color)}
        
        bottomTipeLbl.text = "按住开始录音"
        
        topTipeLbl.font = UIFont.systemFontOfSize(Constant.common_F4_font)
        bottomTipeLbl.font = UIFont.systemFontOfSize(Constant.common_F1_font)
        
        self.guid_Content.snp_makeConstraints { (make) -> Void in
            
            make.top.equalTo(self.view)
            make.centerX.equalTo(self.view)
            make.width.equalTo(self.guid_Content.iwidth)
            make.height.equalTo(self.guid_Content.iheight)
        }

        topTipeLbl.snp_makeConstraints { (make) in
            make.bottom.equalTo(self.recordButton_Content.snp_top).offset(-MARGIN_4)
            make.left.equalTo(self.view).offset(MARGIN_10)
            make.right.equalTo(self.view).offset(-MARGIN_10)
            make.height.equalTo(20)
        }

        self.recordButton_Content.snp_makeConstraints { (make) -> Void in
            make.centerX.equalTo(self.view)
            make.bottom.equalTo(bottomTipeLbl.snp_top).offset(-MARGIN_4)
            make.width.equalTo(self.recordButton_Content.iwidth)
            make.height.equalTo(self.recordButton_Content.iheight)
        }
        
        bottomTipeLbl.snp_makeConstraints { (make) in
            make.bottom.equalTo(self.recordOverButton.snp_top).offset(-MARGIN_4)
            make.left.equalTo(self.view).offset(MARGIN_10)
            make.right.equalTo(self.view).offset(-MARGIN_10)
            make.height.equalTo(20)
        }
        
        
//        self.recordView_Content.snp_makeConstraints { (make) -> Void in
//            make.centerY.equalTo(self.view)
//            make.centerX.equalTo(self.view)
//            make.width.equalTo(self.view).offset(-40)
//            make.height.equalTo(50)
//        }
        
        self.recordOverButton.snp_makeConstraints { (make) -> Void in
            make.bottom.equalTo(self.view).offset(-MARGIN_20)
            make.centerX.equalTo(self.view)
            make.width.equalTo(recordOverButton.iwidth)
            make.height.equalTo(recordOverButton.iheight)
        }
        
//        self.recordView_Content.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(VoiceWishViewController.recordViewTagAction)))
    }
    
    
    //MARK: 广告语
    private class GuidView:UIView
    {
        //图片
        private let topImageView = UIImageView(image: UIImage(named: "wishSend_picture"))
        private let bottomImageView = UIImageView(image: UIImage(named: "wishSend_words"))
        
        var words       = ""{didSet{}}
        var icon_local  = ""{didSet{self.topImageView.image = UIImage(named: icon_local)}}
        var icon_url    = ""{didSet{self.topImageView.af_setImageWithURL(NSURL(string: icon_url)!, placeholderImage: UIImage(named: "loadingDefault"))}}
        
        init()
        {
            super.init(frame:CGRectZero)
            self.setupUI()
        }
        
        
        private func setupUI()
        {
            print(Constant.ScreenSize.SCREEN_WIDTH_SCALE)
            print(Constant.ScreenSize.SCREEN_HEIGHT_SCALE)
            
            let top_edg:CGFloat = 320 * Constant.ScreenSize.SCREEN_HEIGHT_SCALE
            let bottom_edg:CGFloat = 315 * Constant.ScreenSize.SCREEN_HEIGHT_SCALE
            
            self.iheight = (top_edg + bottom_edg) * Constant.ScreenSize.SCREEN_WIDTH_SCALE
            
            self.iwidth = Constant.ScreenSize.SCREEN_WIDTH

            self.topImageView.contentMode = UIViewContentMode.ScaleAspectFill
            self.bottomImageView.contentMode = UIViewContentMode.ScaleAspectFill
            
            self.addSubview(self.bottomImageView)
            self.addSubview(self.topImageView)
            
            self.topImageView.snp_makeConstraints { (make) -> Void in
                make.top.equalTo(self)
                make.centerX.equalTo(self)
                make.width.equalTo(bottom_edg * Constant.ScreenSize.SCREEN_WIDTH_SCALE)
                make.height.equalTo(bottom_edg * Constant.ScreenSize.SCREEN_WIDTH_SCALE)
            }
            
            self.bottomImageView.snp_makeConstraints { (make) -> Void in
                make.top.equalTo(self.topImageView.snp_bottom).offset(-MARGIN_20 - MARGIN_13)
                make.centerX.equalTo(self)
                make.width.equalTo(top_edg * Constant.ScreenSize.SCREEN_WIDTH_SCALE)
                make.height.equalTo(top_edg * Constant.ScreenSize.SCREEN_WIDTH_SCALE)
            }
        }
       
        
        func disPlay(shouldDisplay:Bool){
            
            UIView.animateWithDuration(0.25) {
                    self.alpha = shouldDisplay ? 1 : 0
                }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    
    
    //MARK: 录音按钮
    private class RecordButton:UIButton
    {
        private var startRecord:(()->Void)!
        private var endRecord:(()->Void)!
        
        init(startRecord:(()->Void),end endRecord:(()->Void))
        {
            let width:CGFloat = 100 * Constant.ScreenSize.SCREEN_HEIGHT_SCALE
            
            super.init(frame:CGRectMake(0, 0, width,width))
            
            self.startRecord = startRecord
            self.endRecord = endRecord
            
            self.setImage(UIImage(named: "wishSend_mic_white"), forState: UIControlState.Normal)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        
        override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
            self.startRecord()
            self.setImage(UIImage(named: "wishSend_mic_red"), forState: UIControlState.Normal)
            
//            recordOver = false
        }
        
        
        override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
            self.setImage(UIImage(named: "wishSend_mic_white"), forState: UIControlState.Normal)
//            if recordOver == false
//            {
                self.endRecord()
//            }
        }
    }
    
    //MARK: 录音条
    private class RecordView:UIView
    {
      
    }
}