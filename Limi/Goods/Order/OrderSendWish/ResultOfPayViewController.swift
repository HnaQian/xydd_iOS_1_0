//
//  ResultOfPayViewController.swift
//  Limi
//
//  Created by Richie on 16/3/7.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//支付结果

import UIKit

class ResultOfPayViewController: BaseResultViewController,OrderRequestManagerDelegate {
    
    fileprivate let keepShoppingBtn = UIButton()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.type = 1
        
        self.navTitle = "支付成功"
        
        Utils.updateViewControllersOfNavViewController(self.navigationController, exceptVCs: [PayViewController.self,OrderGeneratorViewControllerV1.self])
        
        self.loadGoodsData("pay")
        // Do any additional setup after loading the view.
    }
    
    var oid:Int = 100
    
    class func newResultVC(_ oid:Int) ->ResultOfPayViewController
    {
        let result = ResultOfPayViewController()
        
        result.oid = oid
        OrderRequestManager(delegate:result).orderDetail(UIApplication.shared.keyWindow!,oid:oid)
        
        return result
    }
    
    
    func orderRequestManagerDidLoadDetailFinish(_ manager: LMRequestManager, result: LMResultMode, detail: OrderDetailModel?) {
        if let model  = detail{
            self.refreshUI(model.order_type)
        }else{
            Utils.showError(context: self.view, errorStr: result.msg)
        }
    }
    
    
    
    @objc func btnAction(_ btn:UIButton)
    {
        
        if btn == keepShoppingBtn
        {
            Statistics.count(Statistics.Pay.pay_goon_click)

            self.navigationController?.tabBarController?.selectedIndex = 0
            let _ = self.navigationController?.popToRootViewController(animated: false)
        }else{
            Statistics.count(Statistics.Pay.pay_takereceipt_click)

            self.navigationController?.pushViewController(InvoiceViewController.newReceipt(self.oid), animated: true)
        }
    }
    
    
    
    
    func refreshUI(_ orderType:Int)
    {
        self.resultTipe = "支付成功"
        
        self.view.addSubview(keepShoppingBtn)
        keepShoppingBtn.setTitle("继续逛逛", for: UIControlState())
        keepShoppingBtn.setTitleColor(UIColor(rgba: Constant.common_C1_color), for: UIControlState())
        keepShoppingBtn.addTarget(self, action: #selector(ResultOfPayViewController.btnAction(_:)), for: UIControlEvents.touchUpInside)
        keepShoppingBtn.titleLabel?.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        
        keepShoppingBtn.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.resultTipeLbl.snp_bottom).offset(MARGIN_8)
            let _ = make.centerX.equalTo(self.view)
            let _ = make.height.equalTo(30)
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
