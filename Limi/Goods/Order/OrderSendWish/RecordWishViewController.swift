//
//  RecordWishViewController.swift
//  Limi
//
//  Created by ZhiQiangMi on 16/4/13.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit
import AVFoundation

class RecordWishViewController: BaseViewController,OrderRequestManagerDelegate{
    
    enum RecordState :Int{
        case prepareRecoredType = 0
        case recordingType
        case recoedEndType
    }

    
    let RECORD_DURATION:Int = 60
    internal var packageId: String = ""
    
    fileprivate var guideView: GuideView!
    
    fileprivate var recordView: RecordView!
    
    fileprivate var playView : PlayView!
    
    fileprivate var dashBoardView : DashBoardView!
    
    fileprivate var recordState : Int = 0
    
    var recordTimer = Timer()
    
    var playTimer = Timer()
    
    var counter = 0
    
    var playCounter = 0
    
    var playTotal = 0
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AVAudioSession.sharedInstance().requestRecordPermission({ (grant) in
            if !grant {
                let alertController = UIAlertController(title: "温馨提示",
                                                        message: "请到“设置-隐私-麦克风”中打开心意点点读取手机麦克风的授权才能使用此功能", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "好的", style: .default,
                                             handler: {
                                                action in
                        let _ = self.navigationController?.popViewController(animated: true)
                })

                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
            }
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        LMRecorder.shareInstance.player?.stop()

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.gesturBackEnable = false
        self.navigationItem.title = "语音祝福"
        
        self.view.backgroundColor = UIColor(rgba: Constant.common_C1_color)
        self.guideView = GuideView(frame: CGRect(x: (Constant.ScreenSize.SCREEN_WIDTH - Constant.ScreenSize.SCREEN_HEIGHT_SCALE * 240)/2 , y: 0, width: Constant.ScreenSize.SCREEN_HEIGHT_SCALE * 240, height: Constant.ScreenSize.SCREEN_HEIGHT_SCALE * 450))
        self.view.addSubview(self.guideView)
        
        self.recordView = RecordView(frame: CGRect(x: 0 , y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: (Constant.ScreenSize.SCREEN_HEIGHT - 64)/3*2))
        
        self.playView = PlayView(frame: CGRect(x: 0 , y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: (Constant.ScreenSize.SCREEN_HEIGHT - 64)/3*2))
        
        if Constant.ScreenSize.SCREEN_MAX_LENGTH < 568.0{
            self.dashBoardView = DashBoardView(frame: CGRect(x: 0 , y: Constant.ScreenSize.SCREEN_HEIGHT_SCALE * 360 , width: Constant.ScreenSize.SCREEN_WIDTH, height: Constant.ScreenSize.SCREEN_HEIGHT - (Constant.ScreenSize.SCREEN_HEIGHT_SCALE * 400)))
        }
        else{
            self.dashBoardView = DashBoardView(frame: CGRect(x: 0 , y: Constant.ScreenSize.SCREEN_HEIGHT_SCALE * 400 , width: Constant.ScreenSize.SCREEN_WIDTH, height: Constant.ScreenSize.SCREEN_HEIGHT - (Constant.ScreenSize.SCREEN_HEIGHT_SCALE * 400)))
        }
        
        self.view.addSubview(self.dashBoardView)
        self.dashBoardView.topLabel.isHidden = true
        self.dashBoardView.commitButton.isHidden = true
        

        let _ = self.dashBoardView.recordButton.handle(events: .touchDown){ (sender, event) -> Void in
            
            LMRecorder.shareInstance.startRecord()
            
            self.recordTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(RecordWishViewController.timerAction), userInfo: nil, repeats: true)
            if self.recordState == RecordState.prepareRecoredType.rawValue{
                self.guideView.removeFromSuperview()
            }
            else{
                self.playView.removeFromSuperview()
            }
            self.view.addSubview(self.recordView)
            self.recordState = RecordState.recordingType.rawValue
            self.dashBoardView.topLabel.isHidden = false
            self.dashBoardView.topLabel.text = "松开手指停止录音"
            self.dashBoardView.bottomLabel.isHidden = true
            self.dashBoardView.commitButton.isHidden = true
            
            self.counter = 0
            self.playCounter = 0
            self.playTotal = 0
            self.playView.playImageView.image = UIImage(named: "wishSend_voice")
            self.playView.playStateLabel.text = "试听"
            if self.playTimer.isValid{
                self.playTimer.invalidate()
            }
            
            weak var weakWaver = self.recordView.waveformView;
            
            self.recordView.waveformView!.setWaverLevelCallback( { () -> () in
                LMRecorder.shareInstance.recorder?.updateMeters()
                let normalizedValue = pow (10, LMRecorder.shareInstance.recorder!.averagePower(forChannel: 0) / 60);
                weakWaver!.level    = CGFloat(normalizedValue)
            })

        
        }

        let _ = self.dashBoardView.recordButton.handle(events: .touchUpInside){ (sender, event) -> Void in
            self.recordTimer.invalidate()
            
            LMRecorder.shareInstance.stopRecord()
            LMRecorder.shareInstance.saveRecord()
            
            self.recordState = RecordState.recoedEndType.rawValue
            self.recordView.removeFromSuperview()
            self.view.addSubview(self.playView)
            self.dashBoardView.topLabel.isHidden = false
            self.dashBoardView.topLabel.text = "按住重新录音"
            self.dashBoardView.bottomLabel.isHidden = true
            self.dashBoardView.commitButton.isHidden = false
            self.playCounter = 0
            self.playTotal = self.counter
            self.counter = 0
            self.playView.recordTotalLabel.text = String(format: "录音共%d秒,可点击试听", self.playTotal)
            if self.playTotal < 10{
                self.playView.countDownLabel.text = String(format: "00:0%d/00:0%d", self.playCounter,self.playTotal)
            }
            else{
                self.playView.countDownLabel.text = String(format: "00:0%d/00:%d", self.playCounter,self.playTotal)
            }
            
            self.recordView.recordLabel.text = "00:00"
            
            
            
            print("456")
        }

        let _ = self.playView.playButton.handle(events: .touchUpInside){ (sender, event) -> Void in
            let userdefault = UserDefaults.standard
            var arr = [[String : String]]()
            if let a = userdefault.object(forKey: "audio")
            {
                arr = a as! [[String : String]]
                //获取最后一首录音
                var dict = arr[arr.count - 1]
                LMRecorder.shareInstance.playRecord(dict["path"]!)
            }

            
            
            self.playCounter = 0
            if self.playTotal < 10{
                self.playView.countDownLabel.text = String(format: "00:0%d/00:0%d", self.playCounter,self.playTotal)
            }
            else{
                self.playView.countDownLabel.text = String(format: "00:0%d/00:%d", self.playCounter,self.playTotal)
            }
            let jeremyGif = UIImage.gifWithName("playingRecord")
            self.playView.playImageView.image = jeremyGif
            self.playTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(RecordWishViewController.playAction), userInfo: nil, repeats: true)
            
        }
        
        let _ = self.dashBoardView.commitButton.handle(events: .touchUpInside){(sender,event) -> Void in
            if LMRecorder.shareInstance.isPlaying{
                self.playView.playImageView.image = UIImage(named: "wishSend_voice")
                self.playView.playStateLabel.text = "试听"
                self.playTimer.invalidate()
                let userdefault = UserDefaults.standard
                var arr = [[String : String]]()
                if let a = userdefault.object(forKey: "audio")
                {
                    arr = a as! [[String : String]]
                    //获取最后一首录音
                    var dict = arr[arr.count - 1]
                    LMRecorder.shareInstance.stopPlayRecord(dict["path"]!);
                }

            }
            let docDir: AnyObject = NSSearchPathForDirectoriesInDomains(.documentDirectory,
                                                                        .userDomainMask, true)[0] as AnyObject
            //组合录音文件路径
            let aacPath = (docDir as! String) + "/record.aac"
            ImageUploader.uploadFile(context:self.view,url: Constant.JTAPI.voice_upload,data: try! Data(contentsOf: URL(fileURLWithPath: aacPath))) { (id, domain) -> Void in
                if id != nil
                {
                    OrderRequestManager(delegate:self).orderUpLoadVoice(self.view,audio: id!, second: self.playTotal)
                }

            }
        }
        // Do any additional setup after loading the view.
    }
    
    override func backItemAction()
    {
        if self.playTimer.isValid{
            self.playTimer.invalidate()
        }
        let _ = self.navigationController?.popViewController(animated: true)
        
    }
    
    func playAction() {
         self.playCounter += 1
        if self.playCounter <= self.playTotal{

            self.playView.playStateLabel.text = "试听中"
        if self.playCounter < 10{
            if self.playTotal < 10{
                self.playView.countDownLabel.text = String(format: "00:0%d/00:0%d", self.playCounter,self.playTotal)
            }
            else{
                self.playView.countDownLabel.text = String(format: "00:0%d/00:%d", self.playCounter,self.playTotal)
            }
            
        }
        else{
            if self.playTotal < 10{
            self.playView.countDownLabel.text = String(format: "00:%d/00:0%d", self.playCounter,self.playTotal)
            }
            else{
                self.playView.countDownLabel.text = String(format: "00:%d/00:%d", self.playCounter,self.playTotal)
            }
        }
        }
        else{
            self.playView.playImageView.image = UIImage(named: "wishSend_voice")
            self.playView.playStateLabel.text = "试听"
            self.playTimer.invalidate()
            let userdefault = UserDefaults.standard
            var arr = [[String : String]]()
            if let a = userdefault.object(forKey: "audio")
            {
                arr = a as! [[String : String]]
                //获取最后一首录音
                var dict = arr[arr.count - 1]
                LMRecorder.shareInstance.stopPlayRecord(dict["path"]!);
            }

        }
    }
    
    func timerAction() {
        if counter == 60{
            recordTimer.invalidate()
        }
        else{
            counter += 1
        }
        
        if self.counter == 60{
            self.recordView.recordLabel.text = "01:00"
        }
        else if self.counter < 10{
            self.recordView.recordLabel.text = String(format: "00:0%d", counter)
        }
        else{
            self.recordView.recordLabel.text = String(format: "00:%d", counter)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func orderRequestManagerDidVoiceAdd(_ manager: LMRequestManager, result: LMResultMode) {
        
        if result.status == 200{
            Utils.showError(context: self.view, errorStr: "录音上传成功")
            print(result.data!["id"] as! Int)
            
            WishSendUnitView.updateVoiceId(result.data!["id"] as! Int)
            
            if let nav = self.navigationController{
                if nav.viewControllers.count > 1{
                    if let webVC = nav.viewControllers[nav.viewControllers.count - 2] as? WebViewController{
                        var tempStr = WishSendUnitView.tempWishSendUrl
                        if self.packageId != ""{
                           tempStr = tempStr + "&package_id=\(self.packageId)"
                        }
                        if let url = URL(string: tempStr){
                            print(tempStr)
                            webVC.webView.load(Foundation.URLRequest(url: url))
                        }else{
                            print("==error!!!===\(#file)--\(#line)")
                        }
                    }
                }
                
                let _ = nav.popViewController(animated: true)
            }
            
        }else
        {
            Utils.showError(context: self.view, errorStr: "录音上传失败")
        }
    }
    
    

    class GuideView: UIView {
        
        let topImageView = ImageView()
        let bottomImageView = ImageView()

        override init(frame: CGRect) {
            super.init(frame: frame)
            
            self.bottomImageView.frame = CGRect(x: 10*Constant.ScreenSize.SCREEN_WIDTH_SCALE,y: self.frame.size.height - (Constant.ScreenSize.SCREEN_HEIGHT_SCALE * 210),width: Constant.ScreenSize.SCREEN_HEIGHT_SCALE * 210,height: Constant.ScreenSize.SCREEN_HEIGHT_SCALE * 210)
            self.bottomImageView.image = UIImage(named:"wishSend_words")
            self.addSubview(self.bottomImageView)
            
            self.topImageView.frame = CGRect(x: 0,y: self.frame.size.height - self.bottomImageView.frame.size.height - (Constant.ScreenSize.SCREEN_HEIGHT_SCALE * 240) + 20,width: (Constant.ScreenSize.SCREEN_HEIGHT_SCALE * 240),height: (Constant.ScreenSize.SCREEN_HEIGHT_SCALE * 240))
            self.topImageView.image = UIImage(named:"wishSend_picture")
            self.addSubview(self.topImageView)
            
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        
    }
    
    class RecordView: UIView {
        
        let recordLabel = UILabel()
        let recordMaxSecondLabel = UILabel()
        var waveformView: Wave?
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            self.waveformView = Wave(frame: CGRect(x: 0, y: self.frame.size.height/2, width: self.frame.size
                      .width, height: 40))
            self.addSubview(self.waveformView!)
            
            self.recordLabel.frame = CGRect(x: 0, y: 60*Constant.ScreenSize.SCREEN_HEIGHT_SCALE, width: self.frame.size
                .width, height: 40)
            self.recordLabel.textColor = UIColor.white
            self.recordLabel.textAlignment = .center
            self.recordLabel.font = UIFont.systemFont(ofSize: 50)
            self.recordLabel.text = "00:00"
            self.addSubview(self.recordLabel)
            
            self.recordMaxSecondLabel.frame = CGRect(x: 0, y: self.recordLabel.frame.origin.y + self.recordLabel.frame.size.height + 10, width: self.frame.size.width, height: 20)
            self.recordMaxSecondLabel.textColor = UIColor.white
            self.recordMaxSecondLabel.textAlignment = .center
            self.recordMaxSecondLabel.font = UIFont.systemFont(ofSize: 13)
            self.recordMaxSecondLabel.text = "最多支持60秒录音"
            self.addSubview(self.recordMaxSecondLabel)
            
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    class PlayView: UIView {
        
        let recordTotalLabel = UILabel()
        let countDownLabel = UILabel()
        let playButton = UIButton()
        let playImageView = UIImageView()
        let playStateLabel = UILabel()
        
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            
            self.playImageView.image = UIImage(named: "wishSend_voice")
            self.playStateLabel.text = "试听"
            self.playStateLabel.font = UIFont.systemFont(ofSize: 15)
            self.playStateLabel.textColor = UIColor.white
            
            self.playButton.frame = CGRect(x: (self.frame.size.width * 0.38)/2, y: self.frame.size.height/2, width: self.frame.size.width * 0.62, height: (Constant.ScreenSize.SCREEN_HEIGHT_SCALE * 56))
            self.playButton.backgroundColor = UIColor.clear
            if Constant.ScreenSize.SCREEN_WIDTH == 320{
                self.playButton.layer.cornerRadius = 20
            }
            else{
            self.playButton.layer.cornerRadius = 25
            }
            self.playButton.layer.masksToBounds = true
            self.playButton.layer.borderColor = UIColor.white.cgColor
            self.playButton.layer.borderWidth = 3
            self.addSubview(self.playButton)
            
            
            if Constant.ScreenSize.SCREEN_WIDTH == 320{
                self.playImageView.frame = CGRect(x: self.playButton.frame.origin.x + (self.playButton.frame.size.width/2) - 25 - 2, y: self.frame.size.height/2 + ((Constant.ScreenSize.SCREEN_HEIGHT_SCALE * 56) - 22)/2, width: 22, height: 22)
            }
            else{
            self.playImageView.frame = CGRect(x: self.playButton.frame.origin.x + (self.playButton.frame.size.width/2) - 30 - 2, y: self.frame.size.height/2 + ((Constant.ScreenSize.SCREEN_HEIGHT_SCALE * 56) - 30)/2, width: 30, height: 30)
            }
            self.insertSubview(self.playImageView, aboveSubview: self.playButton)
            
            self.playStateLabel.frame = CGRect(x: self.playButton.frame.origin.x + (self.playButton.frame.size.width/2) + 2, y: self.frame.size.height/2 + ((Constant.ScreenSize.SCREEN_HEIGHT_SCALE * 56) - 30)/2, width: 80, height: 30)
            self.insertSubview(self.playStateLabel, aboveSubview: self.playButton)
            
            
            
            self.countDownLabel.frame = CGRect(x: 0, y: self.frame.size.height/2 - 30, width: self.frame.size.width, height: 20)
            self.countDownLabel.textAlignment = .center
            self.countDownLabel.textColor = UIColor.white
            self.countDownLabel.font = UIFont.systemFont(ofSize: 13)
            self.countDownLabel.text = "00:00/00:00"
            self.addSubview(self.countDownLabel)
            
            self.recordTotalLabel.frame = CGRect(x: 0, y: self.frame.size.height/2 - 70, width: self.frame.size.width, height: 20)
            self.recordTotalLabel.textAlignment = .center
            self.recordTotalLabel.textColor = UIColor.white
            self.recordTotalLabel.font = UIFont.systemFont(ofSize: 13)
            self.recordTotalLabel.text = "录音共0秒，可点击试听"
            self.addSubview(self.recordTotalLabel)

        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
 
    
    class DashBoardView: UIView {
        
        let topLabel = UILabel()
        let recordButton = UIButton()
        let bottomLabel = UILabel()
        let commitButton = UIButton()
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            
            self.topLabel.frame = CGRect(x: 0, y: (Constant.ScreenSize.SCREEN_HEIGHT_SCALE * 20), width: self.frame.size.width, height: 20)
            self.topLabel.textColor = UIColor.white
            self.topLabel.font = UIFont.systemFont(ofSize: 15)
            self.topLabel.textAlignment = .center
            self.topLabel.text = "松开手指停止录音"
            self.addSubview(self.topLabel)
            
            self.recordButton.frame = CGRect(x: (Constant.ScreenSize.SCREEN_WIDTH - (self.frame.size.width * 0.21))/2, y: 40 + (15*Constant.ScreenSize.SCREEN_HEIGHT_SCALE), width: self.frame.size.width * 0.21, height: self.frame.size.width * 0.21)
            self.recordButton.setImage(UIImage(named: "wishSend_mic_white"), for: UIControlState())
            self.recordButton.setImage(UIImage(named: "wishSend_mic_red"), for: .highlighted)
            
            self.recordButton.backgroundColor = UIColor.clear
            self.addSubview(self.recordButton)
            
            self.bottomLabel.frame = CGRect(x: 0, y: self.recordButton.frame.origin.y + self.recordButton.frame.size.height + 10, width: self.frame.size.width, height: 20)
            self.bottomLabel.textColor = UIColor.white
            self.bottomLabel.font = UIFont.systemFont(ofSize: 15)
            self.bottomLabel.textAlignment = .center
            self.bottomLabel.text = "按住开始录音"
            self.addSubview(self.bottomLabel)
            
            if Constant.ScreenSize.SCREEN_MAX_LENGTH <= 568.0{
                self.commitButton.frame = CGRect(x: (self.frame.size.width * 0.38)/2, y: self.recordButton.frame.origin.y + self.recordButton.frame.size.height + (Constant.ScreenSize.SCREEN_HEIGHT_SCALE * 56) - 20 , width: self.frame.size.width * 0.62, height: Constant.ScreenSize.SCREEN_HEIGHT_SCALE * 40)
            }
            else{
            self.commitButton.frame = CGRect(x: (self.frame.size.width * 0.38)/2, y: self.recordButton.frame.origin.y + self.recordButton.frame.size.height + (Constant.ScreenSize.SCREEN_HEIGHT_SCALE * 56) - (15*Constant.ScreenSize.SCREEN_HEIGHT_SCALE) , width: self.frame.size.width * 0.62, height: Constant.ScreenSize.SCREEN_HEIGHT_SCALE * 40)
            }
            self.commitButton.setTitle("录制完成", for: UIControlState())
            self.commitButton.setTitleColor(UIColor(rgba: Constant.common_C1_color), for: UIControlState())
            self.commitButton.titleLabel?.font = UIFont.systemFont(ofSize: 17)
            
            self.commitButton.backgroundColor = UIColor.white
            self.commitButton.layer.cornerRadius = 5
            self.commitButton.layer.masksToBounds = true
            self.addSubview(self.commitButton)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }



}
