//
//  BaseResultViewController.swift
//  Limi
//
//  Created by Richie on 16/3/7.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//订单支付结果页面以及填写完订单结果页面的父类

import UIKit

class BaseResultViewController: BaseViewController,RecommendGoodsBannerDelegate {

    ///结果图片名称
    var resultImgName = ""{didSet{self.resultImg.image = UIImage(named: resultImgName)}}
    
    var type = 0;
    
    ///结果提示
    var resultTipe = ""{didSet{
        
        if self.isKind(of: ResultOfPayViewController.self) == true
        {
            self.resultTipeLbl.font = UIFont.systemFont(ofSize: Constant.common_F1_font)
            self.resultTipeLbl.textColor = UIColor(rgba: Constant.common_C2_color)
        }
        
        self.resultTipeLbl.text = resultTipe
        
        }}
    /// 标题
    var navTitle = ""{didSet{self.navigationItem.title = navTitle}}
    /// 推荐标题:他们还买了
    var recommend = ""{didSet
    {self.moreGoodsListBanner.recommendTipe = recommend}}
    
    //UI
    let moreGoodsListBanner = RecommendGoodsBanner(style: 0, frame: CGRect.zero)
    let resultImg = UIImageView(image: UIImage(named: "operation_done"))
    let resultTipeLbl = UILabel()
//    let moreTipeLbl = UILabel()
//    let moreTipeLine = UIView()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.superSetupUI()
        // Do any additional setup after loading the view.
    }

    override func customBackButton() {
        super.customBackButton()
        backButton.setBackgroundImage(UIImage(named: "webview_cancel"), for: UIControlState())
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    
    /**
     请求网路数据
     
     - parameter urlStr:     url
     - parameter parameters: parameters
     注:当传入一次参数之后,第二次如果传入的参数没有变化，可以不传任何数据
     */
    func loadGoodsData(_ code:String)
    {
        moreGoodsListBanner.loadRecommendGoodsList(code)
    }

    
    func setupUI(){fatalError("需要子类实现")}

    //delegate func
    func recommendGoodsBannerdidSelectedItem(_ data: GoodsModel) {

        if data.gid != 0
        {
            if type == 1{
                Statistics.count(Statistics.Pay.pay_recommendgift_click, andAttributes: ["gid":"\(data.gid)"])
            }
            
            EventDispatcher.dispatch("local://goods_item", params: ["id": data.gid as AnyObject], onNavigationController: self.navigationController)
        }
    }
    
    
    fileprivate func superSetupUI()
    {
//        let _ = [resultImg,resultTipeLbl,moreTipeLine,moreTipeLbl,moreGoodsListBanner].map{self.view.addSubview($0)}
        let _ = [resultImg,resultTipeLbl].map{self.view.addSubview($0)}
        moreGoodsListBanner.delegate = self
        resultTipeLbl.textAlignment = .center
        resultTipeLbl.font = UIFont.systemFont(ofSize: 15)
        resultTipeLbl.numberOfLines = 0
        
//        moreTipeLbl.textAlignment = .Center
//        moreTipeLbl.font = UIFont.systemFontOfSize(15)
//        moreTipeLbl.textColor = UIColor.blackColor()
//            
//        moreTipeLine.backgroundColor = UIColor(rgba: Constant.common_separatLine_color)
//        moreTipeLbl.backgroundColor = UIColor(rgba: Constant.common_background_color)
        
        let img_top:CGFloat = Constant.ScreenSize.SCREEN_HEIGHT * 0.1
        
        resultImg.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.view).offset(img_top)
            let _ = make.centerX.equalTo(self.view)
            let _ = make.width.equalTo(60)
            let _ = make.height.equalTo(60)
        }

        resultTipeLbl.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(resultImg.snp_bottom)
            let _ = make.centerX.equalTo(resultImg)
            let _ = make.width.equalTo(self.view)
//            make.height.equalTo(60)
        }
//        
//        moreTipeLbl.snp_makeConstraints { (make) -> Void in
//            make.bottom.equalTo(moreGoodsListBanner.snp_top)
//            make.centerX.equalTo(self.view)
//            make.height.equalTo(40)
//            make.width.equalTo(100)
//        }
//        
//        moreTipeLine.snp_makeConstraints { (make) -> Void in
//            make.centerY.equalTo(moreTipeLbl)
//            make.left.equalTo(self.view).offset(MARGIN_8)
//            make.right.equalTo(self.view).offset(-MARGIN_8)
//            make.height.equalTo(0.5)
//        }
        
//        moreGoodsListBanner.snp_makeConstraints { (make) -> Void in
//            let _ = make.bottom.equalTo(self.view)
//            let _ = make.left.equalTo(self.view)
//            let _ = make.right.equalTo(self.view)
//            let _ = make.height.equalTo(moreGoodsListBanner.iheight)
//        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
