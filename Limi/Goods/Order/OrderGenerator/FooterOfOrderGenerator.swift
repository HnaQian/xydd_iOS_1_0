//
//  FooterOfOrderGenerator.swift
//  Limi
//
//  Created by Richie on 16/4/15.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class FooterOfOrderGenerator: BaseUnitView,UITextViewDelegate {
    
    //留言
    var remark:String?{return self.remarkField.text}
    
    fileprivate var commentTextViewContent = ""
    
    //留言
    let remarkField = UITextView()
    //价格表
    fileprivate let expensesSummaryView = ExpensesSummaryView()
    //备注表
    fileprivate let remarkBgView = UIView()
    //物流提示
    fileprivate let expressLbl = UILabel()
    fileprivate let expressLblBgView = UIView()
    
    fileprivate var layoutChangedHandler:(()->Void)?
    
    init(){
     super.init(frame: CGRect(x: 0,y: 0,width: Constant.ScreenSize.SCREEN_WIDTH,height: 0))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //设置数据
    func setData(_ dataManager:OrderDataManager,layoutChangedHandler:@escaping (()->Void)){
        
        let _ =  [expressLblBgView,expressLbl,remarkBgView,remarkField,expensesSummaryView].map{self.addSubview($0)}
        
        expressLblBgView.backgroundColor = UIColor.white
        expressLbl.textColor = UIColor(rgba: Constant.common_C1_color)
        expressLbl.font = UIFont.systemFont(ofSize: Constant.common_F5_font)
        remarkField.contentInset = UIEdgeInsetsMake(5, 0, 0, 0)
        
        expressLbl.numberOfLines = 2
        
        remarkField.delegate = self
        remarkField.placeHolder = "您还有什么要叮嘱我们的呢 ？"
        
        remarkField.isEditable = true
        remarkField.layer.cornerRadius = 3
        remarkField.layer.borderWidth = 0.5
        remarkBgView.backgroundColor = UIColor.white
        remarkField.layer.borderColor = UIColor(rgba: Constant.common_separatLine_color).cgColor
        remarkField.backgroundColor = UIColor(rgba: Constant.common_C11_color)
        
        reloadData(dataManager, layoutChangedHandler: layoutChangedHandler)
    }
    
    
    //刷新数据
    func reloadData(_ dataManager:OrderDataManager,layoutChangedHandler:(()->Void)?){
        //Layout
        if layoutChangedHandler != nil{
            self.layoutChangedHandler = layoutChangedHandler
        }
        
        self.iwidth = Constant.ScreenSize.SCREEN_WIDTH
        
        expensesSummaryView.reloadData(dataManager)
        
        //物流信息
        expressLbl.frame = CGRect(x: MARGIN_8, y: 0, width: self.iwidth - MARGIN_8*2, height: 0)
        expressLblBgView.frame = CGRect(x: 0, y: 0, width: self.iwidth, height: 0)
        
        expressLbl.iheight = dataManager.expressTipe == "" ? 0 :35
        expressLblBgView.iheight = expressLbl.iheight
        
        expressLbl.text = dataManager.expressTipe
        
        //推荐
        remarkBgView.frame = CGRect(x: 0, y: expressLbl.ibottom  + MARGIN_8, width: self.iwidth, height: 50)
        
        //备注
        remarkField.frame = CGRect(x: MARGIN_8, y: remarkBgView.itop + 5, width: self.iwidth - MARGIN_8*2, height: 40)
        
        //价格表
        expensesSummaryView.frame = CGRect(x: 0, y: remarkBgView.ibottom, width: self.iwidth, height: expensesSummaryView.iheight)
        
        self.iheight = expensesSummaryView.ibottom
        
        
        if self.layoutChangedHandler != nil{
            self.layoutChangedHandler!()
        }
    }
    
    
    func textViewDidChange(_ textView: UITextView)
    {
        remarkField.contentInset = UIEdgeInsets.zero
        
        if textView.text.characters.count > 49
        {
            textView.text = commentTextViewContent
        }
        
        commentTextViewContent = textView.text
    }
    
}
