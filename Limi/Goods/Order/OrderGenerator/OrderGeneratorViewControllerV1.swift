//
//  OrderGeneratorViewControllerV1.swift
//  Limi
//
//  Created by Richie on 16/3/8.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//填写订单主页面

import UIKit

class OrderGeneratorViewControllerV1: BaseOrderInfoViewController {
    
    fileprivate let toolBar = BottomToolsBar()
    
    var orderDataManager:OrderDataManager!{return _orderDataManager}
    fileprivate var _orderDataManager:OrderDataManager!
    //sourceType:  1:来自商品详情，2:礼物栏 3:自选礼盒
    fileprivate var sourceType:Int = 0
    
    //额外商品：主要用于自选礼盒
    fileprivate var giftServiceList:[GoodsGenericModel]{return _giftServiceList}
    fileprivate var _giftServiceList = [GoodsGenericModel]()
    
    //packageID
    fileprivate var packageID:Int?{return _packageID}
    fileprivate var _packageID:Int?
    
    //ids :"0,0,0" 三个ID
    fileprivate var ids:String?{return _ids}
    fileprivate var _ids:String?
    fileprivate let loginDialog = LoginDialog()
    
    
    /**
     sourceType:
     1: 来自商品详情
     2: 礼物篮
     3: 自选礼盒
     */
    class  func  orderGenerator(_ goodsList:[GoodsGenericModel],sourceType:Int) ->OrderGeneratorViewControllerV1{
        let orderGeneratorVC = OrderGeneratorViewControllerV1()
        orderGeneratorVC.sourceType = sourceType
        orderGeneratorVC.goodsDataList = goodsList
        orderGeneratorVC._orderDataManager = OrderDataManager.newManager(goodsList,sourceType:sourceType)
        return orderGeneratorVC
    }
    
    
    func setJspParameters(_ parameter:[String:AnyObject]){
        
        /**
         "package_id": 1, 
         "callback_event": guess_box_order_status, 
         "discount_price": 0, 
         "ids": 70,0,0, 
         "gift_service": http://test.wechat.giftyou.me/giftserver/list?need_token=1&ids=70,0,0&package_id=1,
         
         "goods_list": <__NSArrayM 0xb532380>(
         {
             attr = "\U989c\U8272:\U7070\U8272;\U957f\U5ea6:240mm";
             "discount_price" = 100;
             gid = 4100;
             "goods_sub_id" = 4544;
             img = "http://up.xydd.co/14447184538185.jpg";
             name = "\U62cd\U7acb\U5f97\U94bb\U77f3\U83f1\U683c\U8ff7\U4f60\U76f8\U518c";
             price = "120.5";
         },
         {
             attr = "\U989c\U8272:\U7070\U8272;\U957f\U5ea6:240mm";
             "discount_price" = 100;
             gid = 7839;
             "goods_sub_id" = 4544;
             img = "http://up.xydd.co/14447184538185.jpg";
             name = "\U62cd\U7acb\U5f97\U94bb\U77f3\U83f1\U683c\U8ff7\U4f60\U76f8\U518c";
             price = "120.5";
         }
         )
         */
        
        var service_dis = [Int]()
        if let ids  = parameter["ids"] as? String,
            let package_id      = parameter["package_id"] as? Int,
            let discount_price  = parameter["discount_price"] as? Double,
            let callback_event  = parameter["callback_event"] as? String{
            
            for id in NSString(string: ids).components(separatedBy: ","){
                service_dis.append(id.intValue)
            }
            _orderDataManager.packageID = package_id
            _orderDataManager.setDiscountPrice(discount_price)
            _orderDataManager.callback_event = callback_event
        }
        
        WishSendUnitView.updateServiceids(service_dis)
    }
  
    fileprivate var headerOfOrderGenerator =  HeaderOfOrderGenerator()
    fileprivate var footerOfOrderGenerator =  FooterOfOrderGenerator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "填写订单"
        self.gesturBackEnable = false
    }
    
    
    fileprivate var isFirst = true
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if isFirst{
            isFirst = false
        }else{
            self.reload()
        }
    }
    
    
    //刷新数据
    func reload()
    {
        headerOfOrderGenerator.reloadData(orderDataManager, layoutChangedHandler: nil)
    
        footerOfOrderGenerator.reloadData(orderDataManager, layoutChangedHandler: nil)
        
        refreshTotalCost()
    }
    
    //refresh total cost
    func refreshTotalCost(){
        if self._orderDataManager.actureCost <= 0{
            self.toolBar.content = "￥ 0.00"
        }else{
            self.toolBar.content = "\(String(format: "￥%.2lf", self._orderDataManager.actureCost))"
        }
    }
    
    
    //返回按钮
    override func backItemAction() {
        if footerOfOrderGenerator.remarkField.isFirstResponder{
            footerOfOrderGenerator.remarkField.resignFirstResponder()
        }
        
        LCActionSheet(title: "陛下真的要放弃填写订单?", buttonTitles: ["朕去意已决"], cancelTitle: "待朕想想", redButtonIndex: 0) { (buttonIndex) -> Void in
            if buttonIndex == 1{
               let _ = self.navigationController?.popViewController(animated: true)}
            }.show()
    }
    
    
    
    override func setupUI() {
        
        orderDataManager.delegate = self
        
        self.view.addSubview(toolBar)
        self.toolBar.snp_makeConstraints { (make) -> Void in
            let _ = make.bottom.equalTo(self.view)
            let _ = make.left.equalTo(self.view)
            let _ = make.right.equalTo(self.view)
            let _ = make.height.equalTo(45)
        }
        
        self.tableView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.view)
            let _ = make.left.equalTo(self.view)
            let _ = make.right.equalTo(self.view)
            let _ = make.bottom.equalTo(toolBar.snp_top)
        }
        
        self.registerClass(GoodsGeneratorTableCell.self, forCellReuseIdentifier: "goodsGeneratorTableCell")
        toolBar.delegate = self
        toolBar.edgLineModes = [EdgLineMode.topLongLine]
        
        toolBar.setTitle(isBolderFont: true)
        toolBar.setContent(UIColor(rgba: Constant.common_red_color),isBolderFont: true)
        toolBar.title = "共计实付: "
        toolBar.content = "￥ 0.00"
        toolBar.actionBtnTitle = "提交订单"
        
        headerOfOrderGenerator.viewController = self
        weak var weakSelf = self as OrderGeneratorViewControllerV1
        
        headerOfOrderGenerator.setData(orderDataManager) { [weak self] in
            
            weakSelf?.header = self?.headerOfOrderGenerator
        }
        
        footerOfOrderGenerator.viewController = self
        footerOfOrderGenerator.setData(orderDataManager) { [weak self] in
            self?.footer = self?.footerOfOrderGenerator
        }
        
        refreshTotalCost()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

//MARK: Delegate
extension OrderGeneratorViewControllerV1:OrderUnitViewDelegate,OrderRequestManagerDelegate,OrderDataManagerDelegate,UIAlertViewDelegate{
    
    //------Request Delegate------
    //商品详情提交订单的回调
    func orderRequestManagerDidCommitOfFormGoodsDetail(_ manager: LMRequestManager, result: LMResultMode,oid:Int) {
        self.resultAnaliys(result,oid: oid)
    }
    
    //礼物蓝提交订单的回调
    func orderRequestManagerDidCommitOfFormGoodsBasket(_ manager: LMRequestManager, result: LMResultMode,oid:Int) {
        self.resultAnaliys(result,oid: oid)
    }
    
    
    //回调结果解析
    func resultAnaliys(_ result: LMResultMode,oid:Int){
        if result.status == 200
        {
            if sourceType == 2{
                //删除礼物篮已经提交的
                Statistics.count(Statistics.SearchAndGiftBasket.basket_delete_all_click)
                
                ShoppingBasketDataManager.shareManager().deleteSome()
            }
            
            //前往结果页面
            if self._orderDataManager.actureCost <= 0//如果支付金额为0
            {
                self.navigationController?.pushViewController(ResultOfPayViewController.newResultVC(oid), animated: true)
                return
            }
            
            //前往支付页面
            self.navigationController?.pushViewController(PayViewController.newPayViewController(JSON(result.data! as AnyObject)["id"].int!,canclePay:.forward), animated: true)
        }else{
            
            if result.status == 1001{
                UIAlertView(title: nil, message: result.msg, delegate: self, cancelButtonTitle: "知道了").show()
            }else{
                Utils.showError(context: self.view, errorStr: result.msg)
            }
        }
    }
    
    
    
    
    //MARK:-----底部工具栏delegate-----
    func orderBottomToolBarAction(_ tooBar:BottomToolsBar)
    {
        if tooBar.actionType == .actionBtnTouch
        {
            if UserInfoManager.didLogin == false
            {
                Utils.showError(context: self.view, errorStr: "请登录后购买")
                
                let time: TimeInterval = 1.0
                let delay = DispatchTime.now() + Double(Int64(time * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                
                DispatchQueue.main.asyncAfter(deadline: delay) {
                    
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.loginType = Constant.InLoginType.orderType.rawValue
                    self.loginDialog.controller = self
                    self.loginDialog.show()
                }
            }else
            {
                Statistics.count(Statistics.Order.order_submit_click)
                
                if headerOfOrderGenerator.addressModel == nil{
                    Utils.showError(context: self.view, errorStr: "请选择收货地址")
                    return
                }
                
                orderDataManager.remark =  footerOfOrderGenerator.remark
                
                if sourceType > 1
                {
                    OrderRequestManager(delegate:self).orderCommitFromBasket(self.view,dataManager: _orderDataManager,sourceType:sourceType, isShowErrorStatuMsg: true, isNeedHud: true)
                }else
                {
                    OrderRequestManager(delegate:self).orderCommitFromGoodsDetail(self.view,dataManager: _orderDataManager, isShowErrorStatuMsg: true, isNeedHud: true)
                }
            }
        }
    }
    
    
    
    func alertView(_ alertView: UIAlertView, didDismissWithButtonIndex buttonIndex: Int) {
        if sourceType == 1{
            //来自商品详情---通知商品详情页面刷新页面
            NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.goodsGenerShouldReloadData), object: nil)
        }else{
            NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.shopBasketShouldShowInvalidGoods), object: nil)
        }
       let _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: -----orderDataManager Delegate-----
    func orderDataManagerInformRefreshFooterData(){
        footerOfOrderGenerator.reloadData(orderDataManager, layoutChangedHandler: nil)
        refreshTotalCost()
    }
    
    
    func orderDataManagerInformRefreshHeaderData(){
        headerOfOrderGenerator.reloadData(orderDataManager, layoutChangedHandler: nil)
    }
    
    
    //获取到心意服务列表
    func orderDataManagerInformRefreshTableViewWithExtraGoodsList(_ giftServiceList:[GiftServiceModel]){
        self._giftServiceList = giftServiceList
        self.refreshTableView()
    }
    
    
    
    //MARK: -----tableView delegate-----
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.goodsDataList.count + self.giftServiceList.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellReuseIdentifier) as! BaseOrderTableViewCell
        //self.goodsDataList.count == 2    indexPath.row = 1
        if (indexPath as NSIndexPath).row >= self.goodsDataList.count{
            cell.reload(self.giftServiceList[(indexPath as NSIndexPath).row - self.goodsDataList.count])
        }else{
            cell.reload(self.goodsDataList[(indexPath as NSIndexPath).row])
        }
        
        cell.delegate = self
        
        return cell
    }
}
