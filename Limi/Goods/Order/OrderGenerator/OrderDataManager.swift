//
//  OrderDataManager.swift
//  Limi
//
//  Created by Richie on 16/3/14.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//管理ordergener 的所有数据。 更具传进来的数据，计算当前的总价格

import UIKit

class OrderDataManager: NSObject {
    
    /*
    管理：
    使用的红包
    使用的心意服务
    目前的商品组
    
    订单的提交
    计算：
    礼品金额
    心意服务金额
    优惠的金额
    运费
     
     
     
     
     "package_id": 1,
     "callback_event": guess_box_order_status,
     "discount_price": 0,
     "ids": 70,0,0,
     "gift_service": http://test.wechat.giftyou.me/giftserver/list?need_token=1&ids=70,0,0&package_id=1,
     
     "goods_list": <__NSArrayM 0xb532380>(
     {
     attr = "\U989c\U8272:\U7070\U8272;\U957f\U5ea6:240mm";
     "discount_price" = 100;
     gid = 4100;
     "goods_sub_id" = 4544;
     img = "http://up.xydd.co/14447184538185.jpg";
     name = "\U62cd\U7acb\U5f97\U94bb\U77f3\U83f1\U683c\U8ff7\U4f60\U76f8\U518c";
     price = "120.5";
     },
     {
     attr = "\U989c\U8272:\U7070\U8272;\U957f\U5ea6:240mm";
     "discount_price" = 100;
     gid = 7839;
     "goods_sub_id" = 4544;
     img = "http://up.xydd.co/14447184538185.jpg";
     name = "\U62cd\U7acb\U5f97\U94bb\U77f3\U83f1\U683c\U8ff7\U4f60\U76f8\U518c";
     price = "120.5";
     }
     )
     
     
     if let ids  = configJsonData["ids"] as? String,
     let package_id      = configJsonData["package_id"] as? Int,
     let goods_list      = configJsonData["goods_list"] as? [AnyObject],
     let gift_service    = configJsonData["gift_service"] as? String,
     let discount_price  = configJsonData["discount_price"] as? String,
     let callback_event  = configJsonData["callback_event"] as? String{
    */
    
    //备注
    var remark:String?
    
    //物流提示语
    var expressTipe = ""
    
    var packageID:Int?
    
    //自选礼盒的折扣价
    var disCountOfPackage:Double?{return _disCountPrice}
    
    var callback_event:String?
    
    //运费
    var transportCost = 0.0
    
    //商品列表
    var goodsList:[GoodsGenericModel]!
    
    //心意服务价格数组
    fileprivate var _wishServiceCosts:[Double]?
    
    //代理
    weak var delegate:OrderDataManagerDelegate?
    
    //使用的红包
    var couponModel:CouponModel?{return _couponModel}
    fileprivate var _couponModel:CouponModel?
    
    //使用的心意服务的dis
    var serviceIds:[Int]?{return _serviceIds}
    fileprivate var _serviceIds:[Int]?
    
    //选择的地址
    var address:AddressMode?{return _address}
    fileprivate var _address:AddressMode?
    
    //总的礼物话费金额（不包括心意服务价格）
    var totalCost:Double!{
        var price = 0.0
        let _ = goodsList.map{price += ($0.selelctedGoodsSubModelPrice * Double($0.selectedGoodsSubModelCount))}
        return price
    }
    
    
    //优惠金额
    var discountCost:Double!{
        
        if _disCountPrice != nil{
            return _disCountPrice!
        }
        
        if let model = couponModel{
            let discount = model.discount > totalCost ? totalCost : model.discount
            return discount
        }
        return 0.0
    }
    
    
    //心意服务价格
    var wishSendCost:Double{
        var price:Double = 0.0
        if let serviceCost = _wishServiceCosts{
            let _ = serviceCost.map{price = price + $0}
        }
        return price
    }
    
    
    //最终支付价格
    var actureCost:Double!{
        var cost = 0.0
        //计算红包的服务费
        cost =  wishSendCost + transportCost + totalCost - discountCost
        return cost
    }
    
    
    var sourceType:Int{return _sourceType}
    fileprivate var _sourceType:Int = 0
    //MARK: NEW
    class func newManager(_ goodsList:[GoodsGenericModel],sourceType:Int) ->OrderDataManager{
        let manager = OrderDataManager()
        manager.goodsList = goodsList
        manager._sourceType = sourceType
        return manager
    }
    
    
    fileprivate var _disCountPrice:Double?
    
    func setDiscountPrice(_ price:Double){
        _disCountPrice = totalCost - price
    }
}



extension OrderDataManager:WishSendUnitViewDelegate,CouponUnitViewDelegate,OperationableReceiveAddressViewDelegate,OrderRequestManagerDelegate{
    
    //红包选择代理
    func couponUnitViewDidSelectedCoupon(_ coupon: CouponModel) {
        _couponModel = coupon
        delegate?.orderDataManagerInformRefreshFooterData()
        
    }
    
    //心意服务  如果收到用户选择心意服务事件，去加载物流提示
    func wishSendChooseOver(_ context:UIView,serviceids:[Int],prices:[Double]){
        _serviceIds = serviceids
        _wishServiceCosts = prices
        
        /*
         记录 serviceids中元素为0的个数
         如果0-2之间的所有元素都为0则没有心意祝福数据
         */
        var count = 3
        //如果在0-2之间发现不为0的，使count减一
        if serviceids.count > 2{
            for index in 0...2{
                if serviceids[index] != 0{
                    count = count - 1
                    break
                }
            }
        }
        
        if count != 3{
            //加载物流提示
            OrderRequestManager(delegate: self).orderGetExpressTipe(context,dataManager: self)
            
            //拉取心意祝福商品信息
            var idsStr = ""
            for index  in 0..<serviceids.count{
                let id = serviceids[index]
                if index == serviceids.count - 1{
                    idsStr += "\(id)"
                }else{
                    idsStr += "\(id),"
                }
            }
            print(idsStr)
            OrderRequestManager(delegate: self).orderRequestGiftServiceList(context, ids: idsStr, package_id: self.packageID)
            
        }else{
            self.expressTipe = ""
            self.delegate?.orderDataManagerInformRefreshTableViewWithExtraGoodsList([GiftServiceModel]())
        }
    }
    
    
    //心意服务列表
    func orderRequestManagerDidLoadGiftServiceList(_ manager:LMRequestManager,result:LMResultMode,giftServiceList:[GiftServiceModel]){
        
        self.delegate?.orderDataManagerInformRefreshTableViewWithExtraGoodsList(giftServiceList)
    }
    
    
    //地址
    func operationableReceiveAddressViewDidSelectedAddress(_ address:AddressMode){
        _address = address
    }
    
    
    func orderRequestManagerDidGetExpressTipe(_ manager:LMRequestManager,result:LMResultMode){
        
        if result.status == 200{
            if let data = result.data,
            let tipe = data["tip"] as? String{
                if tipe.characters.count > 0{
                    self.expressTipe = tipe
                    self.delegate?.orderDataManagerInformRefreshFooterData()
                }
            }
        }
    }
}



protocol OrderDataManagerDelegate:NSObjectProtocol {
    ///刷新footerView
    func orderDataManagerInformRefreshFooterData()
    ///刷新HeaderView
    func orderDataManagerInformRefreshHeaderData()
    
    ///当加载完心意服务的商品时 刷新tableview
    func orderDataManagerInformRefreshTableViewWithExtraGoodsList(_ giftServiceList:[GiftServiceModel])
}
