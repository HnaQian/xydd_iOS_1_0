//
//  HeaderOfOrderGenerator.swift
//  Limi
//
//  Created by Richie on 16/4/15.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class HeaderOfOrderGenerator: BaseUnitView {
    
    fileprivate let addressView = OperationableReceiveAddressView()
    
    fileprivate let unitSendWish = WishSendUnitView(true,title: "")
    
    fileprivate let unitCoupon = CouponUnitView(true, title: "红包")
    
    var addressModel:AddressMode?{return addressView.addressModel}
    
    init(){
        super.init(frame: CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: 0))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate var layoutChangedHandler:(()->Void)?
    fileprivate weak var dataManager:OrderDataManager?
    
    
    func setData(_ dataManager:OrderDataManager,layoutChangedHandler:@escaping (()->Void)){
        
        self.layoutChangedHandler = layoutChangedHandler
        unitSendWish.packageID = dataManager.packageID
        
        let _ = [addressView,unitSendWish,unitCoupon].map{
            self.addSubview($0)
            $0.viewController = self.viewController
            $0.delegate = dataManager
        }
        
        unitSendWish.edgLineModes = [EdgLineMode.bottomShortLine]
        
        //获取红包数据 如果是从“自选礼盒页面来的商品，不请求红包数据”
        if dataManager.sourceType != 3{
            unitCoupon.prepareData(dataManager.goodsList)
        }else{
            unitCoupon.setTipeWithCouponCount(0)
        }
        
        self.reloadData(dataManager, layoutChangedHandler: layoutChangedHandler)
    }
    
    
    func reloadData(_ dataManager:OrderDataManager,layoutChangedHandler:(()->Void)?){
        
        if layoutChangedHandler != nil{
            self.layoutChangedHandler = layoutChangedHandler
        }
        
        self.iwidth = Constant.ScreenSize.SCREEN_WIDTH
        self.dataManager = dataManager
        
        //刷新心意服务数据
        unitSendWish.reloadData()
        
        addressView.frame = CGRect(x: 0, y: 0, width: self.iwidth, height: addressView.iheight)
        
        unitCoupon.frame = CGRect(x: 0, y: addressView.ibottom + MARGIN_8, width: self.iwidth, height: unitCoupon.iheight)
        
          unitSendWish.frame = CGRect(x: 0, y: unitCoupon.ibottom + MARGIN_8, width: self.iwidth, height: unitSendWish.iheight)

        self.iheight = unitSendWish.ibottom + MARGIN_8
        
        unitCoupon.edgLineModes = [EdgLineMode.bottomLongLine]
        
        self.edgLineModes = [EdgLineMode.bottomLongLine]
        
        if self.layoutChangedHandler != nil{
            self.layoutChangedHandler!()
        }
    }
}
