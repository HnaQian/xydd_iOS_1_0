//
//  InvoiceDoneViewController.swift
//  Limi
//
//  Created by Richie on 16/3/17.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class InvoiceDoneViewController: BaseResultViewController {
    
    fileprivate let keepShoppingBtn  = UIButton()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.navTitle = "填写完成"
        
        self.setupUI()
        
        self.loadGoodsData("done")
        
        Utils.updateViewControllersOfNavViewController(self.navigationController, exceptVCs: [PayViewController.self,OrderGeneratorViewControllerV1.self,ResultOfPayViewController.self,InvoiceViewController.self])
    }
    
    
    @objc func keepShopping()
    {
        self.navigationController?.tabBarController?.selectedIndex = 0
        let _ = self.navigationController?.popToRootViewController(animated: false)
    }
    
    
    override func setupUI()
    {
        self.view.addSubview(keepShoppingBtn)
        
        self.resultTipe = "发票信息填写完成\n我们将在10个工作日内为您寄出发票\n请耐心等待，谢谢"
        
        keepShoppingBtn.setTitleColor(UIColor(rgba: Constant.common_red_color), for: UIControlState())
        keepShoppingBtn.addTarget(self, action: #selector(InvoiceDoneViewController.keepShopping), for: UIControlEvents.touchUpInside)
        keepShoppingBtn.setTitle("继续逛逛", for: UIControlState())
        
        keepShoppingBtn.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.resultTipeLbl.snp_bottom)
            let _ = make.centerX.equalTo(self.view)
            let _ = make.height.equalTo(30)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
    
    
    
}


extension InvoiceDoneViewController
{
    
}
