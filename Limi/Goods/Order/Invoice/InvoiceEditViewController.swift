//
//  InvoiceEditViewController.swift
//  Limi
//
//  Created by Richie on 16/3/17.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class InvoiceEditViewController: BaseViewController,InvoiceRequestManagerDelegate {
    
    
    fileprivate weak var headerLBL:UILabel!
    
    fileprivate let textView = UITextView()
    
    fileprivate var commentTextViewContent = ""
    
    fileprivate var commitTitle = ""
    
    weak var delegate:InvoiceEditViewControllerDelegate?
    
    
    class func newVC(_ headerLBL:UILabel) ->InvoiceEditViewController
    {
        let VC =  InvoiceEditViewController()
        
        headerLBL.text = ""
        
        VC.headerLBL = headerLBL
        
        return VC
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "新增发票抬头"
        
        setupUI()
        // Do any additional setup after loading the view.
    }
    
    /**
     上传数据
     */
    
    @objc func addNewHeader(){
        
        if let text = textView.text{
            if text.characters.count > 0{
                commitTitle = text
                InvoiceRequestManager(delegate: self).invoiceAddnew(self.view,title: text)
                return
            }
        }
        Utils.showError(context: self.view, errorStr: "请填写发票抬头")
    }
    
    func invoiceRequestManagerDidAddnewFinish(_ manager: LMRequestManager, result: LMResultMode) {
        
        if result.status == 200{
            Utils.showError(context: self.view, errorStr: "添加成功")
            let invoiceModel = InvoiceModel()
            if let id = result.data?["id"] as? Int{
                invoiceModel.id = id
            }
            
            invoiceModel.invoice = self.commitTitle
            let _ = Delegate_Selector(delegate, #selector(InvoiceEditViewControllerDelegate.invoiceEditVCdidAddSuccess(_:))){Void in self.delegate!.invoiceEditVCdidAddSuccess(invoiceModel)}
            let _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
       
    
    func setupUI()
    {
        self.view.addSubview(textView)
        textView.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        textView.placeHolderFont = UIFont.systemFont(ofSize: Constant.common_F4_font)
        textView.placeHolder = "请输入个人或公司全称"
        textView.backgroundColor = UIColor.white
        textView.layer.borderColor = UIColor(rgba: Constant.common_separatLine_color).cgColor
        textView.layer.cornerRadius = 3
        textView.layer.borderWidth = 0.5
        textView.isScrollEnabled = false
        textView.showsVerticalScrollIndicator = false
        textView.contentInset = UIEdgeInsetsMake(7, 0, 0, 0);
        
        textView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.view).offset(8)
            let _ = make.left.equalTo(self.view)
            let _ = make.right.equalTo(self.view)
            let _ = make.height.equalTo(48)
        }
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UITextViewTextDidChange, object: nil, queue: OperationQueue.main) { (notification) in
            
            if let text = self.textView.text
            {
                if text.characters.count > 99
                {
                    self.textView.text = (self.textView.text as NSString).substring(to: 99)
                }
                
                self.textView.snp_remakeConstraints(closure: { (make) in
                    let _ = make.top.equalTo(self.view).offset(8)
                    let _ = make.left.equalTo(self.view)
                    let _ = make.right.equalTo(self.view)
                    let textHeight = self.textView.sizeThatFits(CGSize(width: self.view.frame.size.width - 20, height: 130)).height
                    
                    if textHeight < 48 {
                        let _ = make.height.equalTo(48)
                    }else {
                        let _ = make.height.equalTo(textHeight)
                    }
                    
                })
                self.commentTextViewContent = self.textView.text
            }
        }
        
        self.navigationItem.rightBarButtonItem = CommonWidgets.NavigationRightBtn.newRightBtn("确定", target: self, action: #selector(InvoiceEditViewController.addNewHeader))
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
    
}

@objc protocol InvoiceEditViewControllerDelegate:NSObjectProtocol{
    
 func invoiceEditVCdidAddSuccess(_ invoiceModel:InvoiceModel)
}
