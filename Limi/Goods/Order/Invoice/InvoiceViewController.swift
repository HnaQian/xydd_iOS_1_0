//
//  InvoiceViewController.swift
//  Limi
//
//  Created by Richie on 16/3/17.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

private let DETAIL_ICON = "cellInfo"

class InvoiceViewController: BaseViewController,InvoiceListDelegate,InvoiceRequestManagerDelegate,InvoiceEditViewControllerDelegate {


    static let InvoiceContentAry = ["明细","礼品","办公用品"]
    
    fileprivate var address_Content = OperationableReceiveAddressViewOfInvoice(setDefaultAddress:false)
    fileprivate var receipt_Content = Receipt_Content()
    fileprivate var header_Content = Header_Content()
    
    fileprivate var contentSelectedIndex:Int{return receipt_Content.selectedIndex}
    
    fileprivate var invoiceMode:InvoiceModel?
    
    fileprivate let receiptHeaderListVC = InvoiceListViewController.newInvoiceListViewController()

    fileprivate lazy var invoiceEditVC:InvoiceEditViewController = {
        
//        if _invoiceEditViewController == nil{
            let VC = InvoiceEditViewController()
            
            VC.delegate = self
//        }
        return VC
    }()
    
    //                        _invoiceEditViewController.delegate = self
    
    fileprivate var oid:Int!
    
    class func newReceipt(_ oid:Int) ->InvoiceViewController
    {
        let newReceiptVC = InvoiceViewController()
        
        newReceiptVC.oid = oid
        
        return newReceiptVC
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.receiptHeaderListVC.reloadData()
        
        self.navigationItem.title = "填写发票信息"
        
        self.setupUI()
        
        //过略掉：填写订单，选择支付方式页面，支付结果页面
        Utils.updateViewControllersOfNavViewController(self.navigationController, exceptVCs: [OrderGeneratorViewControllerV1.self,PayViewController.self,ResultOfPayViewController.self])
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.reloadData()
    }
    
    
    
    func invoiceListSelected(_ mode:InvoiceModel?)
    {
        self.invoiceMode = mode
    }
    
    func invoiceListdidDelete(_ id:Int)
    {
        if let model = self.invoiceMode{
            if model.id == id{
                self.invoiceMode = nil
            }
        }
    }
    
    //刷新界面数据
    func reloadData()
    {
        if let invoiceModel = self.invoiceMode
        {
            self.header_Content.contentLBL.text = invoiceModel.invoice
        }else
        {
            self.header_Content.contentLBL.text = "请选择发票抬头"
        }
    }
    
    
    
    @objc func tagGesAction(_ tapGes:UITapGestureRecognizer)
    {
        if let view  = tapGes.view
        {
            switch (view.tag - 100)
            {
            case 1:
                if receiptHeaderListVC.invoiceList.count > 0{
                
                    self.navigationController?.pushViewController(receiptHeaderListVC, animated: true)
                }else{
                    self.navigationController?.pushViewController(invoiceEditVC, animated: true)
                }
                
            default:break
            }
        }
    }
    
    
    //MARK:edit delegate:
    func invoiceEditVCdidAddSuccess(_ invoiceModel:InvoiceModel){
        self.receiptHeaderListVC.reloadData()
            self.invoiceMode = invoiceModel
    }
    
    
    //确认按钮
    @objc func confirmAction()
    {
        
        Statistics.count(Statistics.Pay.pay_takereceipt_finish_click)
        
        if invoiceMode == nil {Utils.showError(context: self.view, errorStr: "请选择发票抬头");return}
        if contentSelectedIndex == 10086{Utils.showError(context: self.view, errorStr: "您还没有选择发票内容");return}
        if address_Content.addressModel == nil{Utils.showError(context: self.view, errorStr: "您还没有选择寄送地址");return}
        
        invoiceMode!.invoice_type = contentSelectedIndex + 1

        InvoiceRequestManager(delegate:self).invoiceMake(self.view,oid: oid, invoiceModel: invoiceMode!, addressId: address_Content.addressModel!.id)
    }
    
    
    func invoiceRequestManagerDidMakeFinish(_ manager: LMRequestManager, result: LMResultMode) {
        if result.status == 200{
            self.navigationController?.pushViewController(InvoiceDoneViewController(), animated: true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    fileprivate var confirmBtn:CommonWidgets.ConfirmButton!
}

extension InvoiceViewController
{
    //    #if false
    fileprivate func setupUI()
    {
        var index = 0
        
        receiptHeaderListVC.delegate = self
        
        confirmBtn = CommonWidgets.ConfirmButton.newButton("确 定", target: self, action: #selector(InvoiceViewController.confirmAction))
        
        for view  in [address_Content,header_Content,receipt_Content,confirmBtn] as [UIView]
        {
            self.view.addSubview(view)
            
            if index < 2{(view as AnyObject).addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(InvoiceViewController.tagGesAction(_:))))}
            
            view.tag  = 100 + index
            index += 1
        }
        
        address_Content.viewController  = self
        
        address_Content.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.view).offset(MARGIN_8)
            let _ = make.left.equalTo(self.view).offset(1)
            let _ = make.right.equalTo(self.view).offset(-1)
            let _ = make.height.equalTo(60)
        }
        
        header_Content.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(address_Content.snp_bottom).offset(MARGIN_8)
            let _ = make.left.equalTo(self.view).offset(1)
            let _ = make.right.equalTo(self.view).offset(-1)
            let _ = make.height.equalTo(48)
        }
        
        receipt_Content.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(header_Content.snp_bottom).offset(MARGIN_8)
            let _ = make.left.equalTo(self.view).offset(1)
            let _ = make.right.equalTo(self.view).offset(-1)
            let _ = make.height.equalTo(104)
        }
        
        confirmBtn.snp_makeConstraints { (make) -> Void in
            let _ = make.bottom.equalTo(self.view)
            let _ = make.left.equalTo(self.view).offset(-3)
            let _ = make.right.equalTo(self.view).offset(3)
            let _ = make.height.equalTo(44)
        }
    }
    
    
    //MARK: 发票抬头块
    class Header_Content:BaseReceiptView
    {
        let titleLBL = CustomLabel(font: Constant.common_F2_font, autoFont: 2, textColorHex: Constant.common_C2_color, textAlignment: .left)
        
        var receiptHeader = ""
        
        let contentLBL = CustomLabel(font: Constant.common_F2_font, autoFont: 2, textColorHex: Constant.common_C8_color, textAlignment: .left)
        
        let detailIconImg = UIImageView(image: UIImage(named: DETAIL_ICON))
        
        override func receipt_setupUI() {
            self.isUserInteractionEnabled = true
            self.addSubview(titleLBL)
            self.addSubview(detailIconImg)
            self.addSubview(contentLBL)
            
            titleLBL.text = "发票抬头"
            titleLBL.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(self)
                let _ = make.left.equalTo(self).offset(MARGIN_8)
                let _ = make.bottom.equalTo(self)
            }
            
            
            detailIconImg.snp_makeConstraints { (make) -> Void in
                let _ = make.centerY.equalTo(titleLBL)
                let _ = make.right.equalTo(self).offset(-MARGIN_8)
                let _ = make.height.equalTo(20)
                let _ = make.width.equalTo(20)
            }
            
            contentLBL.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(titleLBL)
                let _ = make.right.equalTo(detailIconImg.snp_left)
                let _ = make.bottom.equalTo(titleLBL)
            }
        }
    }
    
    
    //MARK: 发票内容块
    class Receipt_Content:BaseReceiptView
    {
        let titleLBL = CustomLabel(font: Constant.common_F2_font, autoFont: 2, textColorHex: Constant.common_C2_color, textAlignment: NSTextAlignment.left)
        
        var selectedIndex = 0
        
        let detailIconImg = UIImageView(image: UIImage(named: DETAIL_ICON))
        
        fileprivate var buttons = [LMButton]()
        
        override func receipt_setupUI() {
            
            titleLBL.text = "发票内容"
            self.addSubview(titleLBL)
            
            titleLBL.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(self).offset(MARGIN_8)
                let _ = make.left.equalTo(self).offset(MARGIN_8)
                let _ = make.height.equalTo(30)
            }
            
            
            let vertical_margin:CGFloat = 16
            let width:CGFloat = (Constant.ScreenSize.SCREEN_WIDTH - MARGIN_8 * 4)/3
            
            var tempBtn:LMButton!
            
            for index in 0...2
            {
                let btn = index == 0 ? LMButton(type: LMButtonType.selectedAviable_title_bord_red) : LMButton(type: LMButtonType.selectedAviable_title_bord_gray)
                self.addSubview(btn)
                btn.snp_makeConstraints(closure: { (make) -> Void in
                    let _ = make.top.equalTo(titleLBL.snp_bottom).offset(vertical_margin)
                    let _ = make.bottom.equalTo(self).offset(-vertical_margin - 2)
                    let _ = make.width.equalTo(width)
                    if index == 0
                    {
                        let _ = make.left.equalTo(self).offset(MARGIN_8)
                    }else
                    {
                        let _ = make.left.equalTo(tempBtn.snp_right).offset(MARGIN_8)
                    }
                })
                
                btn.addTarget(self, action: #selector(Receipt_Content.typeAction(_:)), for: UIControlEvents.touchUpInside)
                
                btn.title = InvoiceViewController.InvoiceContentAry[index]
                
                btn.tag = index + 100
                buttons.append(btn)
                tempBtn = btn
            }
        }
        
        
        func typeAction(_ btn:LMButton)
        {
            selectedIndex = btn.tag - 100
            
            btn.refreshButtonType(LMButtonType.selectedAviable_title_bord_red)
            
            for b in buttons{
                if b != btn{
                    b.refreshButtonType(LMButtonType.selectedAviable_title_bord_gray)
                }
            }
        }
    }
    
    
    class BaseReceiptView:UIView
    {
        init()
        {
            super.init(frame:CGRect.zero)
            self.layer.borderWidth = 0.5
            self.isUserInteractionEnabled = true
            self.layer.borderColor = UIColor(rgba: Constant.common_separatLine_color).cgColor
            self.backgroundColor = UIColor.white
            
            self.receipt_setupUI()
        }
        
        func receipt_setupUI(){}
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    //    #endif
}
