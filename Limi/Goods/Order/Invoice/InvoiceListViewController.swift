//
//  InvoiceListViewController.swift
//  Limi
//
//  Created by Richie on 16/3/17.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

private let CELL_IDENTIFER = "receiptHeadTableViewCell"

class InvoiceListViewController: BaseViewController,InvoiceTableViewCellDelegate,InvoiceRequestManagerDelegate,InvoiceEditViewControllerDelegate {
    
    fileprivate weak var delHeaderLBL:UILabel!
    fileprivate let tableView = UITableView()
    fileprivate var addNewBtn:CommonWidgets.ConfirmButton!
    
    fileprivate var selectedID = 0
    fileprivate var deleteID = 0
    
    var invoiceList:[InvoiceModel]{return _invoiceList}
    
    fileprivate var _invoiceList = [InvoiceModel]()
    weak var delegate:InvoiceListDelegate?
    
    fileprivate var invoiceRequestManager = InvoiceRequestManager()
    
    fileprivate var _invoiceEditViewController:InvoiceEditViewController!
    
    fileprivate var invoiceEditVC:InvoiceEditViewController{
        
        if _invoiceEditViewController == nil{
            _invoiceEditViewController = InvoiceEditViewController()
            
            _invoiceEditViewController.delegate = self
        }
        return _invoiceEditViewController
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.setupUI()
        // Do any additional setup after loading the view.
    }
    
    func reloadData(){
        InvoiceRequestManager(delegate:self).invoiceList(self.view)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    class func new(_ headerLBL:UILabel) ->InvoiceListViewController
    {
        let VC = InvoiceListViewController()
        
        VC.delHeaderLBL = headerLBL
        
        return VC
    }
    
    class func newInvoiceListViewController() ->InvoiceListViewController
    {
        let VC = InvoiceListViewController()
        
        VC.setupUI()
        
        return VC
    }
    
    func invoiceRequestManagerDidLoadListFinish(_ manager: LMRequestManager, result: LMResultMode, list: [InvoiceModel]) {
        
        self.tableView.endHeaderRefresh()
        if result.status == 200{
            if list.count  == 0{
                self.tableView.showTipe(UIScrollTipeMode.nullData).tipeContent("您还没有发票，去添加")}
        }
        
        _invoiceList = list
        
        tableView.reloadData()
        
        if _invoiceList.count > 0{
            self.tableView.closeTipe()
        }
        
        self.addNewBtn.isHidden = _invoiceList.count >= 5
    }
    
    
    /**
     删除header
     */
    func invoiceDeleteHeader(_ mode: InvoiceModel) {
        
        LCActionSheet(title: "确定要删除该发票抬头吗？", buttonTitles: ["确定"], cancelTitle: "取消", redButtonIndex: 0) { buttonIndex  in
            if buttonIndex == 1{
                self.deleteID = mode.id
                self.invoiceRequestManager.invoiceDelete(self.view,id: "\(mode.id)")
            }
            }.show()
    }
    
    
    func invoiceRequestManagerDidDeleteFinish(_ manager: LMRequestManager, result: LMResultMode) {
        if result.status == 200{
            Utils.showError(context: self.view, errorStr: "删除成功")
            let _ = Delegate_Selector(self.delegate, #selector(InvoiceListDelegate.invoiceListdidDelete(_:))){Void in self.delegate!.invoiceListdidDelete!(self.deleteID)}
            self.tableView.beginHeaderRefresh()
        }
    }
    
    
    
    /**
     选择header
     */
    func invoiceSelectedHeader(_ mode: InvoiceModel) {
        
        selectedID = mode.id
        
        tableView.reloadData()
        
        let _ = Delegate_Selector(delegate, #selector(InvoiceListDelegate.invoiceListSelected(_:))){Void in self.delegate!.invoiceListSelected!(mode)}
        
        let _ = self.navigationController!.popViewController(animated: true)
    }
    
    
    /**
     新增发票抬头
     */
    func addNewAction()
    {
        self.navigationController!.pushViewController(self.invoiceEditVC, animated: true)
    }
    
    
    
    
    func networkTaskDidFail(_ networkTask: LMRequestManager, error: NSError?, msg: String?) {
        self.tableView.showTipe(UIScrollTipeMode.lostConnect).tipeContent("卧槽！断网了")
        self.tableView.endHeaderRefresh()
    }
    
    
    
    @objc func invoiceEditVCdidAddSuccess(_ invoiceModel:InvoiceModel)
    {
        self.tableView.beginHeaderRefresh()
    }
    
    func setupUI()
    {
        self.invoiceEditVC.delegate = self
        
        self.navigationItem.title = "选择发票抬头"
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(InvoiceTableViewCell.self, forCellReuseIdentifier: CELL_IDENTIFER)
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        self.tableView.addHeaderRefresh { [weak self]() -> Void in
            InvoiceRequestManager(delegate:self!).invoiceList((self?.view)!)
        }
        self.tableView.beginHeaderRefresh()
        
        self.invoiceRequestManager.delegate = self
        
        addNewBtn = CommonWidgets.ConfirmButton.newButton("新增发票抬头", target: self, action: #selector(InvoiceListViewController.addNewAction))
        self.view.addSubview(addNewBtn)
        self.view.addSubview(tableView)
        tableView.backgroundColor = UIColor.clear
        
        addNewBtn.snp_makeConstraints { (make) -> Void in
            let _ = make.height.equalTo(44)
            let _ = make.left.equalTo(self.view).offset(-3)
            let _ = make.right.equalTo(self.view).offset(3)
           let _ =  make.bottom.equalTo(self.view)
        }
        
        tableView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.view)
            let _ = make.left.equalTo(self.view)
            let _ = make.right.equalTo(self.view)
            let _ = make.bottom.equalTo(self.view).offset(-50)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


extension InvoiceListViewController:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return _invoiceList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFER) as! InvoiceTableViewCell
        cell.reloadData(self._invoiceList[(indexPath as NSIndexPath).row],selectedID:selectedID)
        cell.delegate = self
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

@objc protocol InvoiceListDelegate:NSObjectProtocol
{
    @objc optional func invoiceListSelected(_ mode:InvoiceModel?)
    
    @objc optional func invoiceListdidDelete(_ id:Int)
    
}
