//
//  PayViewController.swift
//  Limi
//
//  Created by guo chen on 15/11/24.
//  Copyright © 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

//取消支付之后应该push一个订单详情/返回订单详情
public enum CancleAction
{
    case forward//push订单详情
    
    case goBack//返回订单详情
}


class PayViewController: BaseViewController,LCActionSheetDelegate,OrderRequestManagerDelegate
{
    let moneyText = UILabel()
    
    var isPaying : Bool = false
    fileprivate var payType:Int!
    
    fileprivate var orderID:Int!
    
    fileprivate var actureMoeny:Float!
    
    fileprivate var orderInfo:JSON!
    
    fileprivate let tableView = UITableView()
    
    fileprivate let tableHeadView = UIView()
    
    fileprivate var canclePay:CancleAction!
    fileprivate var payTitleAry = [[String:String]]()
    
    class func newPayViewController(_ orderID:Int,canclePay:CancleAction)-> PayViewController
    {
        let payVC = PayViewController()
        
        payVC.orderID = orderID
        
        payVC.canclePay = canclePay
        
        payVC.getOrderInfo()
        
        payVC.setupUI()
        
        return payVC
    }
    
    fileprivate var isWaittingPayResult = false
    //当程序进入前台时：即从支付页面返回回来时，刷新订单状态
    func refreshOrderStatus(){
        OrderRequestManager(delegate:self).orderDetail(self.view,oid: orderID)
    }
    
    func orderRequestManagerDidLoadDetailFinish(_ manager:LMRequestManager,result:LMResultMode,detail:OrderDetailModel?){
        
        if !isWaittingPayResult{return}
        isWaittingPayResult = false
        
        if let model = detail
        {
            var didSuccess = false
            
            if  model.order_status != NULL_INT{
                //商品订单
                if model.order_type == 1{
                    
                    if model.order_status > 1{
                        didSuccess = true
                        //支付成功
                    }
                    
                    //话费订单
                }else if model.order_type == 2{
                    if model.order_status > 2{
                        didSuccess = true
                        //支付成功
                    }
                }
            }
            
            if didSuccess{
                NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.orderList_changed), object: nil)
                self.goTOResult()
            }else{
                Utils.showError(context: self.view, errorStr: "支付失败")
            }
        }
    }
    
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //程序即将进入后台时保存数据
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: Constant.APP_LCA.DidBecomeActive), object: nil, queue: OperationQueue.main) { [weak self](notification) -> Void in
            self?.refreshOrderStatus()
        }
        
        self.navigationItem.title = "支付方式"
        
        //关闭手势返回
        self.gesturBackEnable = false
        
        self.payTitleAry = [["title":"支付宝支付","icon":"chose_alipay"],["title":"微信支付","icon":"chose_wechat"],["title":"银联支付","icon":"pay_union"]]
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //返回
    override func backItemAction()
    {
        if !isPaying{
            let sheet: LCActionSheet = LCActionSheet(title: "未支付订单将在48小时后被取消噢！真的放弃付款吗?", buttonTitles: ["放弃付款"], cancelTitle: "再想想", redButtonIndex: 0, delegate: self)
            sheet.show()
        }
    }
    
    func actionSheet(_ actionSheet: LCActionSheet!, didClickedButtonAt buttonIndex: Int) {
        
        if buttonIndex == 1
        {
            if self.canclePay == .forward
            {
                navigationController?.pushViewController(OrderDetailViewControllerV1.newDetailVC(self.orderID), animated: true)
            }
            else{
               let _ = self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    
    func convertStringToDictionary(_ text: String) -> [String:AnyObject]? {
        if let data = text.data(using: String.Encoding.utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
            } catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
    
    
    func requestPay()
    {
        isWaittingPayResult = true
        
        isPaying = true
    let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.order_prepay, parameters: ["oid":orderID as AnyObject,"channel": payType as AnyObject], isNeedUserTokrn: true,isShowErrorStatuMsg: true, isNeedHud: true, successHandler: {data, status, msg in
            self.isPaying = false
            if status != 200
            {
                return
            }
            let dataDictionary = data as NSDictionary
            
            let dataPay : NSDictionary = dataDictionary.object(forKey: "data") as! [AnyHashable: Any] as NSDictionary
            if self.payType == 24{
                //微信
                let wxPay : LDSDKWXServiceImpl = LDSDKManager.getPayService(LDSDKPlatformType.weChat) as! LDSDKWXServiceImpl
                
                wxPay.payOrder(dataPay.object(forKey: "data") as! [AnyHashable: Any], callback: ({(signString,error) in
                    if error == nil{
                        self.goTOResult()
                    }
                    else{
                        Utils.showError(context: self.view, errorStr: (error?.localizedDescription)!)
                        
                    }
                    }
                ))
            }
            else if self.payType == 12{
                //支付宝
                let aliPay : LDSDKAliPayServiceImpl = LDSDKManager.getPayService(LDSDKPlatformType.aliPay) as! LDSDKAliPayServiceImpl
                aliPay.payOrder(dataPay.object(forKey: "data") as! [AnyHashable: Any], callback: ({(signString,error) in
                    if error == nil{
                        self.goTOResult()
                    }
                    else{
                        Utils.showError(context: self.view, errorStr: (error?.localizedDescription)!)
                    }
                    }
                ))
            }
            else if self.payType == 26{
                //Apple pay
//                self.createOrder(dataPay.object(forKey: "data") as! [AnyHashable: Any])
            }else if self.payType == 6{
                //银联
               let tempDict = dataPay.object(forKey: "data") as! [AnyHashable: Any]
               //let _ = UPPaymentControl.default().startPay(tempDict["param"] as! String!, fromScheme: "Limi", mode: "00", viewController: self)
               let _ = OIUPPayControl.shareControl.startPay(tempDict["param"] as! String!, fromScheme: "LimiUPPay", mode: "00", viewController: self)
                OIUPPayControl.shareControl.OIUPPaymentResultBlock = {
                    [weak self](code:String?,dict:[AnyHashable:Any]?) -> Void in
                    //结果code为成功时
                    if code == "success" {
                        self?.goTOResult()
                    }else if code == "fail" {
                         Utils.showError(context: (self?.view)!, errorStr: "支付失败")
                    }else if code == "cancel" {
                        Utils.showError(context: (self?.view)!, errorStr: "支付取消")
                    }
                }
            }
            },failureHandler:{
                self.isPaying = false
            }
        )
        
    }
    
    func goTOResult(){
        if ((self.navigationController?.viewControllers.last?.isKind(of: ResultOfPayViewController.self)) == false)
        {
            self.navigationController?.pushViewController(ResultOfPayViewController.newResultVC(self.orderID), animated: true)
        }
    }
    
    
    func getOrderInfo()
    {
       let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.order_item, parameters: ["oid":self.orderID as AnyObject], isNeedUserTokrn: true,isShowErrorStatuMsg: true, isNeedHud: true, successHandler: {data, status, msg in
            if status == 200
            {
                self.orderInfo = JSON(data as AnyObject)["data"]["order"]
                
                self.actureMoeny =  self.orderInfo["Actual_price"].floatValue
                
                self.moneyText.text = String(format: "￥ %.2f元",self.actureMoeny)
            }
            
        })
    }
    
    
    deinit{
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: Constant.APP_LCA.DidBecomeActive), object: nil)
    }
    
}



extension PayViewController:UITableViewDataSource,UITableViewDelegate
{
    //tableView delegate ---->
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return payTitleAry.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        return tableHeadView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 108
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let  indefier = "payVCCell"
        
        var cell = tableView.dequeueReusableCell(withIdentifier: indefier) as? ReallyPayTableViewCell
        
        if cell == nil
        {
            tableView.register(UINib(nibName: "ReallyPayTableViewCell", bundle: nil), forCellReuseIdentifier: indefier)
            
            cell = tableView.dequeueReusableCell(withIdentifier: indefier) as? ReallyPayTableViewCell
        }
        
        cell!.refreshData(payTitleAry[(indexPath as NSIndexPath).row] )
        
        return cell!
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 50
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        var payTypeName = ""
            
        if (indexPath as NSIndexPath).row == 0
        {
            payTypeName = "支付宝"
            self.payType = 12
            
        }else if (indexPath as NSIndexPath).row == 1
        {
            payTypeName = "微信"
            self.payType = 24
        }else if (indexPath as NSIndexPath).row == 2
        {
            payTypeName = "银联"
            self.payType = 6
        }
        requestPay()
        
        Statistics.count(Statistics.Pay.pay_payment_click, andAttributes: ["type":payTypeName])
    }
    
    
    
    
}



extension PayViewController
{
    fileprivate func setupUI()
    {
        self.view.backgroundColor = UIColor(rgba:"#F0F0F0")
        
        tableView.delegate = self
        
        tableView.dataSource = self
        
        self.view.addSubview(tableView)
        
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        
        tableView.showsHorizontalScrollIndicator = false
        
        tableView.showsVerticalScrollIndicator = false
        tableView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.view)
            let _ = make.bottom.equalTo(self.view)
            let _ = make.left.equalTo(self.view)
            let _ = make.right.equalTo(self.view)
        }
        
        
        let moneyContent = UIView()
        let payTitleContent = UIView()
        let gayView = UIView()
        
        let moneyTitile = UILabel()
        let payTitle = UILabel()
        
        tableHeadView.addSubview(moneyContent)
        tableHeadView.addSubview(payTitleContent)
        tableHeadView.addSubview(gayView)
        
        gayView.backgroundColor = UIColor(rgba:"#F0F0F0")
        
        moneyContent.addSubview(moneyTitile)
        moneyContent.addSubview(moneyText)
        payTitleContent.addSubview(payTitle)
        
        
        moneyTitile.font = UIFont.systemFont(ofSize: 15)
        
        payTitle.font = UIFont.systemFont(ofSize: 13)
        
        moneyTitile.textColor = UIColor(rgba:"#222222")
        
        payTitle.textColor = UIColor(rgba:"#999999")
        
        moneyText.textAlignment = NSTextAlignment.right
        
        moneyText.textColor = UIColor(rgba: Constant.common_red_color)
        
        moneyTitile.text = "订单金额"
        
        payTitle.text = "请选择支付方式"
        
        moneyContent.layer.borderWidth = 0.5
        payTitleContent.layer.borderWidth = 0.5
        moneyContent.layer.borderColor = UIColor(rgba:Constant.common_separatLine_color).cgColor
        payTitleContent.layer.borderColor = UIColor(rgba:Constant.common_separatLine_color).cgColor
        
        moneyTitile.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(moneyContent)
            let _ = make.bottom.equalTo(moneyContent)
            let _ = make.width.equalTo(200)
            let _ = make.left.equalTo(moneyContent).offset(8)
        }
        
        moneyText.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(moneyContent)
            let _ = make.bottom.equalTo(moneyContent)
            let _ = make.width.equalTo(120)
            let _ = make.right.equalTo(moneyContent).offset(-8)
        }
        
        payTitle.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(payTitleContent)
            let _ = make.bottom.equalTo(payTitleContent)
            let _ = make.width.equalTo(200)
            let _ = make.left.equalTo(payTitleContent).offset(8)
        }
        
        moneyContent.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(tableHeadView)
            let _ = make.height.equalTo(50)
            let _ = make.right.equalTo(tableHeadView)
            let _ = make.left.equalTo(tableHeadView)
        }
        
        payTitleContent.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(moneyContent.snp_bottom).offset(8)
            let _ = make.height.equalTo(moneyContent)
            let _ = make.right.equalTo(tableHeadView)
            let _ = make.left.equalTo(tableHeadView)
        }
        
        gayView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(moneyContent.snp_bottom)
            let _ = make.height.equalTo(8)
            let _ = make.right.equalTo(tableHeadView)
            let _ = make.left.equalTo(tableHeadView)
        }
        
        tableHeadView.backgroundColor = UIColor.white
    }
    
    
    class ApplePayView:UIView{
        
        fileprivate let applePayLbl:UILabel = {
            let lbl = UILabel()
            lbl.frame = CGRect(x: 0, y: 0, width: 90, height: 20)
            lbl.font = UIFont.systemFont(ofSize: 17)
            lbl.text = "Apple Pay"
            return lbl
        }()
        
        fileprivate let applePayIcon:UIImageView = {
            let icon = UIImageView(image: UIImage(named: "Apple_Pay_mark_normal_"))
            icon.contentMode = UIViewContentMode.scaleAspectFill
            icon.frame = CGRect(x: 0, y: 0, width: 31, height: 20)
            return icon
        }()
        
        fileprivate let unionPayIcon:UIImageView = {
            let icon = UIImageView(image: UIImage(named: ""))
            //icon.contentMode = UIViewContentMode.ScaleAspectFill
            icon.frame = CGRect(x: 0, y: 2, width: 65, height: 18)
            return icon
        }()
        
        
        convenience init() {
            self.init(frame: CGRect(x: 0, y: 0, width: 240, height: 20))
            self.setupUI()
        }
        
        
        fileprivate func setupUI(){
            self.backgroundColor = UIColor.white
            
            let _ = [applePayLbl,applePayIcon,unionPayIcon].map{self.addSubview($0)}
            
            applePayIcon.ix = applePayLbl.iright
            unionPayIcon.ix = self.iright - unionPayIcon.iwidth
        }
        
    }
}
