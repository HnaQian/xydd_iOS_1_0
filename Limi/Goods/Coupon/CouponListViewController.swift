//
//  CouponListViewController2.swift
//  Limi
//
//  Created by meimao on 15/10/8.
//  Copyright © 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class CouponListViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate,CouponRequestManagerDelegate,UITextFieldDelegate,CouponListViewDelegate,CouponTableViewCellDelegate
{
    fileprivate var pageSize = 20
    
    fileprivate var datatype:Int = 0{didSet{
        if self.listData[datatype].isEmpty{
            self.couponListView.tableViewAry[datatype].beginHeaderRefresh()
        }
        }}
    
    var couponListView: CouponListView
    {
        return self.view as! CouponListView
    }
    
    
    override func loadView()
    {
        super.loadView()
        self.view = CouponListView()
        (self.view as! CouponListView).delegate = self
    }
    
    var listData = [[CouponModel]](repeating: [CouponModel](), count: 3)
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.navigationItem.title = "我的红包"
        
        
        weak var weakSelf = self as CouponListViewController
        
        for index in  0...(self.couponListView.tableViewAry.count - 1){
            
            let tablView = self.couponListView.tableViewAry[index]
            
            tablView.delegate = self
            tablView.dataSource = self
            tablView.register(CouponTableViewCell.self, forCellReuseIdentifier: CouponTableViewCell.identifier)
            tablView.addHeaderRefresh({
                DispatchQueue.global().async(execute: { () -> Void in
                    weakSelf?.loadListData(index)
                })
            })
            
            tablView.addFooterRefresh({
                DispatchQueue.global().async(execute: { () -> Void in
                    weakSelf?.loadMoreCoupon(index)
                })
            })
        }
        
        self.couponListView.convertButtonClickedHandler = { [unowned self](sender: UIButton) -> Void in
            
            var param = [String:AnyObject]()
            param["code"] = self.couponListView.convertTextField.text as AnyObject?
            let _ = LMRequestManager.requestByDataAnalyses(context: self.view, URLString: Constant.JTAPI.exchange_coupon,parameters: param,isNeedUserTokrn:true, isShowErrorStatuMsg:false,successHandler: { (data, status, msg) in
                
                if status != 200 {
                    Utils.showError(context: self.view, errorStr: "兑换码无效，请检查后重试")
                    return
                }
                self.couponListView.convertTextField.text = String()
                self.couponListView.convertButton.setTitleColor(UIColor(rgba: Constant.common_C8_color), for: UIControlState())
                self.couponListView.convertButton.layer.borderColor = Constant.Theme.Color8.cgColor
                self.couponListView.convertButton.isEnabled = false
                
                Utils.showError(context: self.view, errorStr: "兑换成功")
                self.couponListView.tableViewL.beginHeaderRefresh()
            })
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool){
        
        super.viewWillAppear(animated)
        
        self.couponListView.tableViewAry[datatype].beginHeaderRefresh()
    }
    
    
    func loadListData(_ type: Int){
        var tempType = type
        if type == 1 {
            tempType = 2
        }else if type == 2 {
            tempType = 1
        }
       let _ = CouponRequestManager(delegate: self).couponLoadList(self.view,type:tempType + 1)
    }
    
    
    func loadMoreCoupon(_ type:Int){
        var tempType = type
        if type == 1 {
            tempType = 2
        }else if type == 2 {
            tempType = 1
        }
        if self.listData[type].count > 0{
           let id = self.listData[type].last!.id
           let _ = CouponRequestManager(delegate: self).couponLoadMore(self.view, type: tempType + 1, lastId: id)
        }
    }
    
    
    func couponRequestManagerDidLoadListFinish(_ requestManager: LMRequestManager, result: LMResultMode, couponList: [[String : AnyObject]]) {
        
        if let sing = requestManager.taskSign{
            var type = 0
            if sing.intValue == 3 {
                type = sing.intValue - 2
            }else if sing.intValue == 2 {
                type = sing.intValue
            }else {
                type = sing.intValue - 1
            }
            
            if result.status != 200{
                
                self.closeRefresh(2)
                self.listData[type] = []
                return
            }
            
            let currentTableView = self.couponListView.tableViewAry[type]
            let refresh = currentTableView.isHeaderRefreshing
            
            if couponList.count > 0{
                let list = JSON(couponList as AnyObject).arrayObject
                print(list)
                if refresh{
                    self.listData[type] = [CouponModel]()
                }
                
                for coupon in list! {
                    self.listData[type].append(CouponModel(coupon as! [String:AnyObject]))
                }
                currentTableView.closeAllRefresh()
//                list.forEach({ (coupon) in
//                    
//                })
                
                if (list?.count)! < 20{
                    currentTableView.showTipe(UIScrollTipeMode.noMuchMore)
                }
            }
            else{
                
                //第一次加载
                if refresh{
                    self.listData[type] = []
                    //没有数据

                    currentTableView.closeAllRefresh()
                    currentTableView.showTipe(UIScrollTipeMode.nullData).tipeContent(self.tipes[type]).tipeIcon(Constant.TipeIcon.myCoupon_nullData)
                    
                }else{
                    
                    currentTableView.closeAllRefresh()
                    currentTableView.showTipe(UIScrollTipeMode.noMuchMore)
                }
            }
            currentTableView.reloadData()
        }
    }
    
    fileprivate let tipes = [Constant.TipeWords.myCoupon_useable_nullData,Constant.TipeWords.myCoupon_overDue_nullData,Constant.TipeWords.myCoupon_used_nullData]
    
    func networkTaskDidFail(_ networkTask: LMRequestManager, error: NSError?, msg: String?) {
        self.closeRefresh(2)
    }
    
    fileprivate func closeRefresh(_ type:Int)
    {
        if type == 0{
            self.couponListView.tableViewAry.forEach({ (tableView) in
                tableView.endHeaderRefresh()
            })
        }
        else if type == 1{
            self.couponListView.tableViewAry.forEach({ (tableView) in
                tableView.endFooterRefresh()
            })
        }
        else if type == 2{
            self.couponListView.tableViewAry.forEach({ (tableView) in
                tableView.closeAllRefresh()
            })
        }
    }
    
    
    //UITabeView delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return listData[tableView.tag].count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let tempData = self.listData[tableView.tag][indexPath.row]
        if tempData.isSelected {
            return tempData.externCellHeight
        }
        return tempData.cellHeight
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell: CouponTableViewCell! = tableView.dequeueReusableCell(withIdentifier: CouponTableViewCell.identifier) as? CouponTableViewCell
        cell.delegate = self
        
        let dataary: [CouponModel] = self.listData[tableView.tag]
        if dataary.count < (indexPath as NSIndexPath).row {
            return cell
        }

        let datadic = dataary[(indexPath as NSIndexPath).row]
        cell.setPropertise(datadic,status: tableView.tag,index:indexPath.row)
        
        return cell
    }
    
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
//    {
//        tableView.deselectRow(at: indexPath, animated: true)
//        // 跳转至优惠券详情界面
//        let dataary: [JSON] = listData[tableView.tag]
//        let datadic = dataary[(indexPath as NSIndexPath).row]
//        let couponDetail = CouponDetailViewController()
//        couponDetail.couponId = datadic["Id"].int
//        self.navigationController?.pushViewController(couponDetail, animated: true)
//    }
    
    func couponListViewChangeIndex(_ index: Int) {
        self.datatype = index
    }
    
    func couponCellStatusChanged(isselected: Bool,status: Int, index: Int) {
        self.listData[status][index].isSelected = isselected
        let indexPath = IndexPath(row: index, section: 0)
        self.couponListView.tableViewAry[status].reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
    }
    
}


