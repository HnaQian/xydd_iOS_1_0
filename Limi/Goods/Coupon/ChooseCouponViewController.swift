//
//  ChooseCouponViewController.swift
//  Limi
//
//  Created by guo chen on 15/9/12.
//  Copyright (c) 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

typealias ChooseOver = (_ describe: String,_ dataDict: NSDictionary?) -> Void

let USEABLE_DISP = "个可用红包"

let UNUSEABLE_COUPON_DISP = "无可用红包"

class ChooseCouponViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate,CouponChooseTableViewCellDelegate
{
    fileprivate var allPrice:Float?//购买商品的总价格
    
    fileprivate var goods_id:Int?//购买商品的ID
    
    fileprivate var chooseOver: ChooseOver?
    
    fileprivate var selectedAry = NSMutableArray()

    fileprivate var status:Int = 1 //加载数据的类型：1:未使用的优惠券  2:快过期的优惠券
    
    fileprivate var couponInfo:NSDictionary?
    
    fileprivate var selectedIndex:Int = 10086
    
    let tableView = UITableView()
    let headView = UIView()
    let footView = UIView()
    let cancelButton = UIButton()
    var convertTextField = UITextField()
    var convertButton = UIButton(type: .custom)
    
    fileprivate var dataAry = [CouponModel]()
    fileprivate var goodsData = [GoodsGenericModel]()
    
    weak var delegate:ChooseCouponViewControllerDelegate?
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        self.tableView.reloadData()
    }
    
    
    //OUTER:设置被选中的红包
    func setSelectedCoupon(_ couponModel:CouponModel){
            if dataAry.count > 0{
                //遍历ary找到对应的
                for index in 0..<dataAry.count{
                    let id = dataAry[index].id
                    if id == couponModel.id{
                        self.tableView(self.tableView, didSelectRowAt: IndexPath(row:index,section: 0))
                        break
                    }
                }
        }
    }
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.title = "选择可用红包"
        
        self.tableView.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: self.view.iheight - 88 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE - MARGIN_64)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.separatorStyle = .none
        self.tableView.backgroundView = nil
        self.tableView.backgroundColor = UIColor.clear
        self.tableView.register(CouponChooseTableViewCell.self, forCellReuseIdentifier: CouponChooseTableViewCell.identifier)
        self.view.addSubview(self.tableView)
        tableViewHeader()
        tableViewFooter()
        
        self.cancelButton.frame = CGRect(x: 0, y: self.tableView.ibottom, width: self.tableView.iwidth, height: 88 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
        self.cancelButton.backgroundColor = UIColor.white
        self.cancelButton.addTarget(self, action: #selector(ChooseCouponViewController.cancelButonClicked), for: .touchUpInside)
        self.cancelButton.setTitle("不使用红包", for: UIControlState())
        self.cancelButton.setTitleColor(Constant.Theme.Color17, for: UIControlState())
        self.cancelButton.titleLabel?.font = Constant.Theme.Font_17
        self.view.addSubview(self.cancelButton)
    }
    
    func tableViewHeader(){
        
        let scale = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        self.headView.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: 140 * scale)
        self.convertTextField.frame = CGRect(x: 20 * scale, y: 30 * scale, width: 460 * scale, height: 80 * scale)
        self.convertButton.frame = CGRect(x: 500 * scale, y: convertTextField.itop, width: 230 * scale, height: 80 * scale)
        
        self.convertTextField.attributedPlaceholder = NSAttributedString(string: "  请输入兑换码", attributes: [NSForegroundColorAttributeName:UIColor(rgba: Constant.common_C9_color),NSFontAttributeName:Constant.Theme.Font_15])
        self.convertTextField.font = Constant.Theme.Font_15
        convertTextField.clearButtonMode = .whileEditing
        convertTextField.leftViewMode = .always
        let leftView = UIView(frame: CGRect(x: 0, y: 0, width: 40 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE, height: 5))
        convertTextField.leftView = leftView
        self.convertTextField.textColor = UIColor(rgba: Constant.common_C6_color)
        self.convertTextField.addTarget(self, action: #selector(ChooseCouponViewController.convertTextFieldAction(_:)), for: UIControlEvents.allEditingEvents)
        self.convertTextField.backgroundColor = UIColor.white
        self.convertTextField.layer.cornerRadius = 5.0
        self.convertTextField.layer.borderWidth = 0.5
        self.convertTextField.layer.borderColor = UIColor(rgba: Constant.common_C10_color).cgColor
        self.convertTextField.layer.masksToBounds = true
        self.headView.addSubview(convertTextField)
        
        self.convertButton.setTitle("兑换红包", for: UIControlState())
        self.convertButton.setTitleColor(UIColor(rgba: Constant.common_C8_color), for: UIControlState())
        self.convertButton.titleLabel?.font = Constant.Theme.Font_15
        self.convertButton.addTarget(self, action: #selector(ChooseCouponViewController.convertButtonClicked), for: UIControlEvents.touchUpInside)
        self.convertButton.layer.borderWidth = 1 * scale
        self.convertButton.layer.borderColor = Constant.Theme.Color8.cgColor
        self.convertButton.layer.cornerRadius = 10 * scale
        self.convertButton.isEnabled = false
        self.headView.addSubview(convertButton)
        
        self.tableView.tableHeaderView = self.headView
    }
    
    func tableViewFooter() {
        let scale = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        self.footView.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: self.view.iheight - 88 * scale - MARGIN_64)
        
        let bgImageView = UIImageView()
        bgImageView.frame = CGRect(x: 0, y: 210 * scale, width: 120 * scale, height: 120 * scale)
        bgImageView.icenterX = self.footView.iwidth / 2
        bgImageView.image = UIImage(named: "iconStyle0")
        self.footView.addSubview(bgImageView)
        
        let label = UILabel()
        label.frame = CGRect(x: 20 * scale, y: bgImageView.ibottom + 10 * scale, width: self.footView.iwidth - 40 * scale, height: 16)
        label.textAlignment = .center
        label.textColor = Constant.Theme.Color6
        label.font = Constant.Theme.Font_15
        label.text = "无可用红包"
        self.footView.addSubview(label)
        
    }
    
    func convertTextFieldAction(_ textField:UITextField){
        
        if (textField.text?.characters.count)! > 0 {
            convertButton.setTitleColor(UIColor(rgba: Constant.common_C1_color), for: UIControlState())
            convertButton.layer.borderColor = Constant.Theme.Color1.cgColor
            self.convertButton.isEnabled = true
        }else {
            convertButton.setTitleColor(UIColor(rgba: Constant.common_C8_color), for: UIControlState())
            convertButton.layer.borderColor = Constant.Theme.Color8.cgColor
            self.convertButton.isEnabled = false
        }
    }
    
    func convertButtonClicked() {
        self.convertTextField.resignFirstResponder()
        requestConvertData()
    }
    
    func requestConvertData() {
        var param = [String:AnyObject]()
        param["code"] = self.convertTextField.text as AnyObject?
        let _ = LMRequestManager.requestByDataAnalyses(context: self.view, URLString: Constant.JTAPI.exchange_coupon,parameters: param,isNeedUserTokrn:true, isShowErrorStatuMsg:true,successHandler: { (data, status, msg) in
            
            if status != 200 {
                Utils.showError(context: self.view, errorStr: "兑换码无效，请检查后重试")
                return
            }
            self.convertTextField.text = ""
            self.convertButton.setTitleColor(UIColor(rgba: Constant.common_C8_color), for: UIControlState())
            self.convertButton.layer.borderColor = Constant.Theme.Color8.cgColor
            self.convertButton.isEnabled = false
            
            Utils.showError(context: self.view, errorStr: "兑换成功")
            self.reloadCouponData()
        })
    }
    
    func reloadCouponData() {
        var ids = [Int]()
        var goods_sub_ids = [Int]()
        var goods_num = [Int]()
        
        for goods in self.goodsData{
            
            ids.append(goods.goodsId!)
            
            if let subModel = goods.selelctedGoodsSubModel,
                let model = subModel{
                goods_sub_ids.append(model.id)
                
            }else{
                goods_sub_ids.append(0)
            }
            
            goods_num.append(goods.selectedGoodsSubModelCount)
        }
        
        var para = [String:AnyObject]()
        
        para["goods_id"] = ids as AnyObject?
        para["goods_num"] = goods_num as AnyObject?
        para["status"] = 1 as AnyObject?
        
        if goods_sub_ids.count == 0{
            var sunids = [Int]()
            for _ in 0..<ids.count{
                sunids.append(0)
            }
            para["goods_sub_id"] = sunids as AnyObject?
            
        }else{
            para["goods_sub_id"] = goods_sub_ids as AnyObject?
        }
        
        let _ = LMRequestManager.requestByDataAnalyses(context: self.view, URLString: Constant.JTAPI.order_coupon_list,parameters: para,isNeedUserTokrn:true, isShowErrorStatuMsg:true,successHandler: { (data, status, msg) in
            
            if status != 200 {
                return
            }
            var couponList = [[String:AnyObject]]()
            
            if let list = data["list"] as? [[String:AnyObject]]
            {
                couponList = list
            }
            
            self.dataAry = [CouponModel]()
            if couponList.count == 0 {
               self.cancelButton.isHidden = true
               self.tableView.tableFooterView = self.footView
            }else {
                self.cancelButton.isHidden = false
                self.tableView.tableFooterView = nil
            }
            for item in couponList {
                self.dataAry.append(CouponModel(item))
            }
            
            self.tableView.reloadData()
            
            self.delegate?.reloadCouponData(data: couponList)
        })
    }
    
    func cancelButonClicked() {
         selectedIndex = 10086
        self.tableView.reloadData()
        self.delegate?.chooseCouponCancel()
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    func rereLoadData(_ list:[[String:AnyObject]],goodsData:[GoodsGenericModel]){
        selectedIndex = 10086
        self.goodsData = goodsData
        self.dataAry = [CouponModel]()
        if list.count == 0 {
            self.cancelButton.isHidden = true
            self.tableView.tableFooterView = self.footView
        }else {
            self.cancelButton.isHidden = false
            self.tableView.tableFooterView = nil
        }
        for item in list {
            self.dataAry.append(CouponModel(item))
        }
        
        self.tableView.reloadData()
    }
    
    
    
    //UITabeView delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return  self.dataAry.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let tempData = self.dataAry[indexPath.row]
        if tempData.isSelected {
            return tempData.externCellHeight
        }
        return tempData.cellHeight
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell: CouponChooseTableViewCell! = tableView.dequeueReusableCell(withIdentifier: CouponChooseTableViewCell.identifier) as? CouponChooseTableViewCell
        
        
        cell.setPropertise(dataAry[(indexPath as NSIndexPath).row],index:indexPath.row, shouldSelected: (indexPath as NSIndexPath).row == selectedIndex)
        
        cell.delegate = self
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        self.selectedIndex = (indexPath as NSIndexPath).row

        let _ = Delegate_Selector(delegate, #selector(ChooseCouponViewControllerDelegate.chooseCouponDidSelected(_:))){Void in self.delegate!.chooseCouponDidSelected((indexPath as NSIndexPath).row)}
        
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func couponChooseCellStatusChanged(isselected: Bool, index: Int) {
        self.dataAry[index].isSelected = isselected
        let indexPath = IndexPath(row: index, section: 0)
        self.tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
    }
  
}


@objc protocol ChooseCouponViewControllerDelegate:NSObjectProtocol{

    func chooseCouponDidSelected(_ index:Int)
    func chooseCouponCancel()
    func reloadCouponData(data:[[String:AnyObject]])
}
