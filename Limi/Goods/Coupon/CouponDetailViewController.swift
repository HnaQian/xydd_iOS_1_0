//
//  CouponDetailViewController.swift
//  Limi
//
//  Created by meimao on 15/9/29.
//  Copyright © 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class CouponDetailViewController: BaseViewController,CouponRequestManagerDelegate {
    
    // 高度、宽度比例
    let heightScale = Constant.ScreenSize.SCREEN_HEIGHT_SCALE
    let widthScale = Constant.ScreenSize.SCREEN_WIDTH_SCALE
    
    // 页面控件
    var contentView = UIView()
    var imageView1 = UIImageView(image: UIImage(named: "coupon_JuChi"))
    var imageView2 = UIImageView(image: UIImage(named: "coupon_bigLiBao"))
    var imageView3 = UIImageView(image: UIImage(named: "coupon_XuXian"))
    var leftImage = UIImageView(image: UIImage(named: "coupon_LeftCircle"))
    var rightImage = UIImageView(image: UIImage(named: "coupon_RightCircle"))
    var backView = UIView()
    var numLab = UILabel()
    var nameLab = UILabel()
    var commitBtn = UIButton()
    var timeLab1 = UILabel()
    var timeLab2 = UILabel()
    var scopeLab1 = UILabel()
    var scopeLab2 = UILabel()
    var minLab1 = UILabel()
    var minLab2 = UILabel()
    var descLab1 = UILabel()
    var descLab2 = UILabel()
    
    // 某张优惠券id
    var couponId: Int!
    
    // 获取某张优惠券详情
    var oneCoupon: JSON? {
        didSet {
            self.navigationItem.title = oneCoupon?["Name"].string
            if let parValue = oneCoupon?["Par_value"].float{
                if parValue > 0{
                    let floatPrice = roundf(parValue)
                    if floatPrice == parValue{
                        numLab.text = String(format: "￥%.0f", parValue)
                    }
                    else{
                        let priceStr1 : NSString = String(format: "%.2f", parValue) as NSString
                        let priceStr2 : NSString = String(format: "%.1f", parValue) as NSString
                        let floatPrice1 : Float = priceStr1.floatValue
                        let floatPrice2 : Float = priceStr2.floatValue
                        if floatPrice1 == floatPrice2{
                            numLab.text = String(format: "￥%.1f", parValue)
                        }
                        else{
                            numLab.text = String(format: "￥%.2f", parValue)
                        }
                        
                    }
                }
                
            }
            nameLab.text = oneCoupon?["Name"].string
            timeLab2.text = oneCoupon?["Time_title"].string
            scopeLab2.text = oneCoupon?["Scope_name"].string
            minLab2.text = oneCoupon?["Min_title"].string
            descLab2.text = oneCoupon?["Content"].string
            descLab1.isHidden = oneCoupon?["Content"].string?.characters.count > 0 ? false : true
            descLab2.isHidden = oneCoupon?["Content"].string?.characters.count > 0 ? false : true
            if  oneCoupon?["Status_name"].string == "已过期" {
                commitBtn.isEnabled = false
                commitBtn.backgroundColor = UIColor(rgba: "#CCCCCC")
                commitBtn.setTitle("已过期", for: UIControlState())
                commitBtn.setTitleColor(UIColor.white, for: UIControlState())
            }
            else if oneCoupon?["Status_name"].string == "已使用" {
                commitBtn.isEnabled = false
                commitBtn.backgroundColor = UIColor(rgba: "#CCCCCC")
                commitBtn.setTitle("已使用", for: UIControlState())
                commitBtn.setTitleColor(UIColor.white, for: UIControlState())
            }
            else
            {
                commitBtn.isEnabled = true
                commitBtn.backgroundColor = UIColor(rgba: "#FFE550")
                commitBtn.setTitle("去使用", for: UIControlState())
                commitBtn.setTitleColor(UIColor(rgba: "#E13636"), for: UIControlState())
                
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let params : NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: eventURL?.query) as NSDictionary?{
            if let id : AnyObject  = params.object(forKey: "id") as AnyObject?{
                self.couponId = id.intValue
            }
        }
        self.addView()
        self.loadData()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // 加载数据
    func loadData()
    {
       let _ = CouponRequestManager.shareInstance(self).couponDetail(self.view,couponId: self.couponId)
    }
    
    func couponRequestManagerDidLoadDetailFinish(_ requestManager: LMRequestManager, result: LMResultMode) {
        if result.status == 200{self.oneCoupon = JSON(result.data! as AnyObject)["coupon"]}
    }
    
    // 点击“去使用”执行
    func commitAction(_ sender: UIButton) {
        // 根据请求得到的url进入不同界面使用
        let href = oneCoupon?["Url"].stringValue
        if !href!.isEmpty && (URL(string: href!) != nil) {
            let eventURL = EventDispatcher.preParserURLWithNotInMap(URL(string: href!)!)
            let host = eventURL.host
            
            if host == "index"
            {
                let userInfo: Dictionary<String,Int>! = [
                    "index": 0,
                    ]
                NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.show_rootController), object: nil, userInfo: userInfo)
               let _ = self.navigationController?.popToRootViewController(animated: true)
                
            }
            else{
                EventDispatcher.dispatch(href!, onNavigationController: self.navigationController)
            }
        }
    }
    
    func addView() {
        // 属性
        contentView.backgroundColor = UIColor(rgba: "#F2F2F2")
        
        backView.backgroundColor = UIColor(rgba: "#E13636")
        backView.layer.cornerRadius = 5.0
        
        numLab.addAttribute(font: 58 * widthScale, textAlignment: NSTextAlignment.center, textColor: "#E13636", text: nil)
        
        nameLab.addAttribute(font: 18 * widthScale, textAlignment: NSTextAlignment.center, textColor: "#E13636", text: nil)
        
        // 使用红包的button按钮
        commitBtn.layer.cornerRadius = 2.5
        commitBtn.titleLabel!.font = UIFont.systemFont(ofSize: 16 * widthScale)
        commitBtn.titleLabel!.textAlignment = NSTextAlignment.center
        commitBtn.addTarget(self, action: #selector(CouponDetailViewController.commitAction(_:)), for: UIControlEvents.touchUpInside)
        
        
        // 有效期
        timeLab1.addAttribute(font: 14 * widthScale, textAlignment: NSTextAlignment.center, textColor: "#FFFFFF", text: "有效期:")
        timeLab2.addAttribute(font: 14 * widthScale, textAlignment: NSTextAlignment.center, textColor: "#FFFFFF", text: nil)
        // 使用范围
        scopeLab1.addAttribute(font: 14 * widthScale, textAlignment: NSTextAlignment.center, textColor: "#FFFFFF", text: "适用范围:")
        scopeLab2.addAttribute(font: 14 * widthScale, textAlignment: NSTextAlignment.center, textColor: "#FFFFFF", text: nil)
        
        // 消费金额
        minLab1.addAttribute(font: 14 * widthScale, textAlignment: NSTextAlignment.center, textColor: "#FFFFFF", text: "消费金额:")
        minLab2.addAttribute(font: 14 * widthScale, textAlignment: NSTextAlignment.center, textColor: "#FFFFFF", text: nil)
        
        // 使用说明
        descLab1.addAttribute(font: 16 * widthScale, textAlignment: NSTextAlignment.center, textColor: "#FFFFFF", text: "使用说明:")
        descLab2.addAttribute(font: 14 * widthScale, textAlignment: NSTextAlignment.center, textColor: "#FFFFFF", text: nil)
        descLab2.numberOfLines = 0
        
        // 加载控件
        self.view.addSubview(contentView)
        contentView.addSubview(backView)
        for view in [imageView1,imageView2,imageView3,leftImage,rightImage,numLab,nameLab,commitBtn,timeLab1,timeLab2,scopeLab1,scopeLab2,minLab1,minLab2,descLab1,descLab2] as [Any] {
            backView.addSubview(view as! UIView)
        }
        
        // 添加约束
        contentView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.view)
            let _ = make.left.equalTo(self.view)
            let _ = make.right.equalTo(self.view)
            let _ = make.bottom.equalTo(self.view)
        }
        
        // 红色背景
        
        backView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(contentView).offset(8)
            let _ = make.left.equalTo(contentView).offset(8)
            let _ = make.right.equalTo(contentView).offset(-8)
            let _ = make.height.equalTo(616 * heightScale)
        }
        
        imageView1.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(backView)
            let _ = make.left.equalTo(backView)
            let _ = make.right.equalTo(backView)
            let _ = make.height.equalTo(6)
        }
        
        // 大礼包图片
        imageView2.snp_makeConstraints { (make) -> Void in
            let _ = make.centerX.equalTo(contentView)
            let _ = make.top.equalTo(backView).offset(56 * heightScale)
            let _ = make.width.equalTo(216 * widthScale)
            let _ = make.height.equalTo(210 * widthScale)
        }
        
        
        numLab.snp_makeConstraints { (make) -> Void in
            let _ = make.centerX.equalTo(contentView)
            let _ = make.centerY.equalTo(imageView2)
        }
        
        nameLab.snp_makeConstraints { (make) -> Void in
            let _ = make.centerX.equalTo(contentView)
            let _ = make.top.equalTo(numLab.snp_bottom).offset(10 * heightScale)
        }
        
        commitBtn.snp_makeConstraints { (make) -> Void in
            let _ = make.centerX.equalTo(contentView)
            let _ = make.top.equalTo(imageView2.snp_bottom).offset(58 * heightScale)
            let _ = make.height.equalTo(36 * heightScale)
            let _ = make.left.equalTo(imageView2)
        }

        leftImage.snp_makeConstraints { (make) -> Void in
            let _ = make.left.equalTo(backView)
            let _ = make.centerY.equalTo(imageView3)
        }

        rightImage.snp_makeConstraints { (make) -> Void in
            let _ = make.right.equalTo(backView)
            let _ = make.centerY.equalTo(imageView3)
        }

        imageView3.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(backView).offset(392 * heightScale)
            let _ = make.left.equalTo(leftImage.snp_right)
            let _ = make.right.equalTo(rightImage.snp_left)
            let _ = make.height.equalTo(4 * heightScale)
        }
        
        timeLab1.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(imageView3.snp_bottom).offset(20 * heightScale)
            let _ = make.left.equalTo(backView).offset(15 * heightScale)
        }
        
        timeLab2.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(timeLab1)
            let _ = make.left.equalTo(scopeLab2)
        }
        
        
        scopeLab1.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(timeLab1.snp_bottom).offset(5 * heightScale)
            let _ = make.left.equalTo(timeLab1)
        }
        
        scopeLab2.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(scopeLab1)
            let _ = make.left.equalTo(scopeLab1.snp_right).offset(10 * heightScale)
        }
        
        minLab1.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(scopeLab1.snp_bottom).offset(5 * heightScale)
            let _ = make.left.equalTo(timeLab1)
        }
        
        minLab2.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(minLab1)
            let _ = make.left.equalTo(scopeLab2)
        }
        
        descLab1.snp_makeConstraints { (make) -> Void in
           let _ = make.top.equalTo(minLab1.snp_bottom).offset(25 * heightScale)
            let _ = make.left.equalTo(timeLab1)
        }
        
        descLab2.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(descLab1.snp_bottom).offset(5 * heightScale)
            let _ = make.left.equalTo(timeLab1)
        }
        
    }
    
}
