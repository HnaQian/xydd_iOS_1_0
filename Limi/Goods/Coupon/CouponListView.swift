//
//  CouponListView.swift
//  Limi
//
//  Created by ZhiQiangMi on 15/11/16.
//  Copyright © 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


@objc public protocol CouponListViewDelegate {
    @objc optional func couponListViewChangeIndex(_ index: Int)
}

class CouponListView: BaseView,LazyPageScrollViewDelegate
{
    var delegate:CouponListViewDelegate?
    var isMycouponView = true
    var pageScrollView:LazyPageScrollView = LazyPageScrollView(verticalDistance: 0, tabItemType: .customView)
    var convertTextField = UITextField()
    var convertButton = UIButton(type: .custom)
    
    var tableViewL = BaseTableView()
    var tableViewM = BaseTableView()
    var tableViewR = BaseTableView()
    
    var tableViewAry = [BaseTableView]()
    
    var convertButtonClickedHandler: ((_ sender: UIButton)->Void)?
    let toolBarItemTitleFontSize:CGFloat = 15
    var tableBackViewAction: (()->())?
    
    override func defaultInit() {
        tableViewAry = [tableViewL,tableViewM,tableViewR]

        pageScrollView.backgroundColor = UIColor(rgba: Constant.common_background_color)
        pageScrollView.delegate = self
        self.addSubview(pageScrollView)
        
        pageScrollView.segment(["未使用","已使用","已过期"], views: tableViewAry,frame: CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: Constant.ScreenSize.SCREEN_HEIGHT - 64))
        
        
        let headView = UIView()
        
        convertTextField.attributedPlaceholder = NSAttributedString(string: "请输入兑换码", attributes: [NSForegroundColorAttributeName:UIColor(rgba: Constant.common_C9_color),NSFontAttributeName:Constant.Theme.Font_15])
        convertTextField.font = Constant.Theme.Font_15
        convertTextField.clearButtonMode = .whileEditing
        convertTextField.leftViewMode = .always
        let leftView = UIView(frame: CGRect(x: 0, y: 0, width: 40 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE, height: 5))
        convertTextField.leftView = leftView
        convertTextField.textColor = UIColor(rgba: Constant.common_C6_color)
        convertTextField.addTarget(self, action: #selector(CouponListView.convertTextFieldAction(_:)), for: UIControlEvents.allEditingEvents)
        convertTextField.backgroundColor = UIColor.white
        convertTextField.layer.cornerRadius = 5.0
        convertTextField.layer.borderWidth = 0.5
        convertTextField.layer.borderColor = UIColor(rgba: Constant.common_C10_color).cgColor
        convertTextField.layer.masksToBounds = true
        headView.addSubview(convertTextField)
        
        convertButton.setTitle("兑换红包", for: UIControlState())
        convertButton.setTitleColor(UIColor(rgba: Constant.common_C8_color), for: UIControlState())
        convertButton.titleLabel?.font = Constant.Theme.Font_15
        convertButton.addTarget(self, action: #selector(CouponListView.convertButtonClicked(_:)), for: UIControlEvents.touchUpInside)
        convertButton.layer.borderWidth = 1 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        convertButton.layer.borderColor = Constant.Theme.Color8.cgColor
        convertButton.layer.cornerRadius = 10 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        convertButton.isEnabled = false
        headView.addSubview(convertButton)
        
        
//        pageScrollView.frame = self.bounds
        let scale = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        pageScrollView.tabItem.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: 80 * scale)
         headView.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: 140 * scale)
        convertTextField.frame = CGRect(x: 20 * scale, y: 30 * scale, width: 460 * scale, height: 80 * scale)
        convertButton.frame = CGRect(x: 500 * scale, y: convertTextField.itop, width: 230 * scale, height: 80 * scale)
        
        let tempView1 = UIView()
        tempView1.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: 30 * scale)
        for index in 0...(tableViewAry.count - 1){
            let tableView = tableViewAry[index]
            if index == 0 && isMycouponView {
                tableView.tableHeaderView = headView
            }else {
                tableView.tableHeaderView = tempView1
            }
            tableView.tag = index
            tableView.separatorStyle = UITableViewCellSeparatorStyle.none
            tableView.backgroundColor = UIColor(rgba: Constant.common_background_color)
        }
    }
    
    override func layoutSubviews()
    {
        super.layoutSubviews()
//        pageScrollView.frame = self.bounds
//        let scale = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
//        pageScrollView.tabItem.frame = CGRect(x: 0, y: 0, width: self.iwidth, height: 80 * scale)
//        convertTextField.frame = CGRect(x: 20 * scale, y: 30 * scale, width: 460 * scale, height: 80 * scale)
//        convertButton.frame = CGRect(x: 500 * scale, y: convertTextField.itop, width: 230 * scale, height: 80 * scale)
//        
//        if !isMycouponView {
//            convertTextField.removeFromSuperview()
//            convertButton.removeFromSuperview()
//            pageScrollView.updateGapHeight(110 * scale)
//        }else {
//             pageScrollView.updateGapHeight(-3)
//        }
        
    }
    
    @objc fileprivate func convertTextFieldAction(_ textField:UITextField){
        
        if textField.text?.characters.count > 0 {
            convertButton.setTitleColor(UIColor(rgba: Constant.common_C1_color), for: UIControlState())
            convertButton.layer.borderColor = Constant.Theme.Color1.cgColor
            self.convertButton.isEnabled = true
        }else {
            convertButton.setTitleColor(UIColor(rgba: Constant.common_C8_color), for: UIControlState())
            convertButton.layer.borderColor = Constant.Theme.Color8.cgColor
            self.convertButton.isEnabled = false
        }
    }
    
    func convertButtonClicked(_ sender:AnyObject) {
        self.convertTextField.resignFirstResponder()
        convertButtonClickedHandler?(convertButton)
    }
    
    func lazyPageScrollView(_ pageScrollView: LazyPageScrollView, changeIndex index: Int) {
        delegate?.couponListViewChangeIndex!(index)
        
    }
    
    func lazyPageScrollViewEdgeSwipeLeft(_ isLeft:Bool) {
        
    }
    
}
