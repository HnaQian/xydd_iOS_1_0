//
//  GoodsListByTagViewController.swift
//  Limi
//
//  Created by 程巍巍 on 7/27/15.
//  Copyright (c) 2015 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class GoodsListByTagViewController: GoodsListViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func loadGoodsList(_ refresh: Bool = true) {
        var params = [String: AnyObject]()
        if !refresh {
            if let id = goodsList.last?["Gid"].int {
                params["last_id"] = id as AnyObject?
            }
        }
        if let param : NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: eventURL?.query) as NSDictionary?{
            if let tag_id  = param.object(forKey: "tag_id") as? String{
                params["tag_id"] = tag_id as AnyObject?
                params["sort"] = self.sortType as AnyObject?

           let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.goods_list_bytag, parameters: params, isNeedUserTokrn: true, isShowErrorStatuMsg: true, successHandler: {data, status, msg in
            
                self.colleView.collectionView.closeAllRefresh()
                
                if status != 200 {
                    return
                }
                
                // 设置导航栏 title
                var title: String = "心意点点"
                if let string = (data["data"] as? [String: AnyObject])?["title"] as? String
                {
                    if string != ""
                    {
                        title = string
                    }
                }
                
                self.navigationItem.title = title
                
                if let list = JSON(data as AnyObject)["data"]["list"].array
                {
                    if refresh
                    {
                        self.goodsList = list
                    }
                    else
                    {
                        for item in list
                        {
                            self.goodsList.append(item)
                        }
                    }
                    
                    self.colleView.goodsList = self.goodsList
                }
                else
                {
                    if !refresh
                    {
                        self.colleView.collectionView.showTipe(UIScrollTipeMode.noMuchMore)
                    }
                    else
                    {
                        self.colleView.goodsList = []
                    }
                }
            })
            
        }
    }
    }
}
