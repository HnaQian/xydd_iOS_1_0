//
//  GiftViewController.swift
//  Limi
//
//  Created by 程巍巍 on 5/23/15.
//  Copyright (c) 2015 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class GiftViewController: BaseViewController {
    var refreshHeader: LMRefreshHeader!
    var giftView: GiftView {
        return self.view as! GiftView
    }
    
    override func loadView() {
        super.loadView()
        self.view = GiftView()
    }
    
    var pageSource = [[String: AnyObject]]() {
        didSet {
            // 加载数据到界面原素
            var children = [UIView]()
            let hrefHandler = {[weak self] (href: String, params: [String: AnyObject]?) ->() in
                EventDispatcher.dispatch(href, params: params, onNavigationController: self?.navigationController)
            }
            
            for item in pageSource 
            {
                if let name = JSON(item as AnyObject)["attributes"]["class"].string
                {
                    switch name 
                    {
                    case "topic":children.append(TopicView().hrefHandler(hrefHandler).reloadData(JSON(item as AnyObject)))
                    case "grid":  children.append(GridView().hrefHandler(hrefHandler).reloadData(JSON(item as AnyObject)))

                    case "subject": children.append(SubjectView().hrefHandler(hrefHandler).reloadData((item as AnyObject) as! [String : AnyObject]))
                    case "occasion", "target", "style", "genre":
                        children.append(GeneralChild().hrefHandler(hrefHandler).reloadData(item))
                    default:
                        print("not suport type \(name)")
                    }
                }else if let _ = JSON(item as AnyObject)["attributes"]["title"].array
                {
                    children.append(GeneralChild().hrefHandler(hrefHandler).reloadData(item))
                }
            }
            giftView.children = children
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "选礼"
        self.navigationItem.titleView = nil
        self.navigationItem.leftBarButtonItem = nil

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

    }

    override func viewDidLoad() {
        super.viewDidLoad()
        weak var weakSelf = self as GiftViewController
        
        giftView.scrollView.addHeaderRefresh { () -> Void in
            
            DispatchQueue.global().async(execute: { () -> Void in
                weakSelf?.loadPageData()
            })
        }

        giftView.scrollView.beginHeaderRefresh()
        loadTempData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func loadPageData(){

        let _ = LMRequestManager.requestByDataAnalyses(HTTPMethod.get, context:self.view,URLString: Constant.JTAPI.gift_page,isShowErrorStatuMsg:true, successHandler: {data, status, msg in
            if self.giftView.scrollView.isHeaderRefreshing == true{
                self.giftView.scrollView.endHeaderRefresh()
            }

        
                if status == 200 {
                    var dataDic: [String: AnyObject] = data["data"] as! [String: AnyObject]
                    self.pageSource = dataDic["layout"] as! [[String: AnyObject]]
                    
                    // 缓存数据
                    let tempName = Crypto.MD5(data: Constant.JTAPI.gift_page.data(using: String.Encoding.utf8, allowLossyConversion: true)!)
                    do {
                        let data = try JSONSerialization.data(withJSONObject: self.pageSource, options: JSONSerialization.WritingOptions())
                        try? data.write(to: URL(fileURLWithPath: NSTemporaryDirectory().stringByAppendingPathComponent(tempName)), options: [.atomic])
                    } catch let error as NSError {
                        print(error)
                    }
                }
        })
        
    }
    func loadTempData(){
        let tempName = Crypto.MD5(data: Constant.JTAPI.gift_page.data(using: String.Encoding.utf8, allowLossyConversion: true)!)
        let data = try? Data(contentsOf: URL(fileURLWithPath: NSTemporaryDirectory().stringByAppendingPathComponent(tempName)))
        if data == nil {return}
        do {
            let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
            self.pageSource = jsonResult as! [[String: AnyObject]]
        } catch let error as NSError {
            print(error)
        }
        
    }
}


extension GiftViewController {
    class GiftView: UIView {
        
        var scrollView = UIScrollView()
        var contentView = UIView()
        
        var children = [UIView]() {
            didSet {
                for subview in contentView.subviews {
                    subview.removeFromSuperview()
                }
                
                var lastView: UIView?
                for child in children {
                    contentView.addSubview(child)
                    
                    child.snp_makeConstraints { (make) -> Void in
                        let _ = make.left.equalTo(contentView)
                        let _ = make.right.equalTo(contentView)
                    }
                    if lastView == nil {
                        
                        child.snp_makeConstraints(closure: { (make) -> Void in
                            let _ = make.top.equalTo(contentView)
                        })

                    }else{
                        child.snp_makeConstraints(closure: { (make) -> Void in
                            let _ = make.top.equalTo(lastView!.snp_bottom).offset(8)
                        })
                    }
                    
                    lastView = child
                }
                
                if lastView != nil {
                    
                    lastView?.snp_makeConstraints(closure: { (make) -> Void in
                        let _ = make.bottom.equalTo(contentView)
                    })
                }
            }
        }
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            
            self.backgroundColor = UIColor(rgba: Constant.common_background_color)
            
            self.addSubview(scrollView)
            
            scrollView.showsHorizontalScrollIndicator = false
            scrollView.showsVerticalScrollIndicator = false
            
            scrollView.translatesAutoresizingMaskIntoConstraints = false

            scrollView.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(self)
                let _ = make.right.equalTo(self)
                let _ = make.top.equalTo(self)
                let _ = make.bottom.equalTo(self).offset(-49)
            }


            
            scrollView.addSubview(contentView)
            contentView.translatesAutoresizingMaskIntoConstraints = false

            contentView.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(scrollView)
                let _ = make.right.equalTo(scrollView)
                let _ = make.top.equalTo(scrollView)
                let _ = make.bottom.equalTo(scrollView)
                let _ = make.width.equalTo(scrollView)
                let _ = make.height.equalTo(scrollView).offset(1)
            }

        }
        
        required init(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    class GiftViewChildren: UIView {
        func reloadData(_ data: JSON)->UIView{
            return self
        }
        var hrefHandler: ((_ href: String, _ params: [String: AnyObject]?)->())?
        func hrefHandler(_ handler: ((_ href: String, _ params: [String: AnyObject]?)->())?) ->Self{
            self.hrefHandler = handler
            return self
        }
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            defaultInit()
        }

        required init(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func defaultInit() {
            self.backgroundColor = UIColor(rgba: Constant.common_white_color)
        }
    }
    
    // 专区
    class TopicView: GiftViewChildren {
        override func defaultInit() {
            self.backgroundColor = UIColor(rgba: Constant.common_background_color)
        }
        override func reloadData(_ data: JSON) -> UIView {
            if let children = data["children"].array {
                if children.count != 2 {
                    print("选礼配置文件发生严重错误，专区仅支持两个栏目")
                }else{
                    var views = [ImageView]()
                    for child in children {
                        let imageview = ImageView()
                        if let src = child["attributes"]["src"].string {
                            let imageScale : String = src + "?imageView2/0/w/" + "\(Int((Constant.ScreenSize.SCREEN_WIDTH-12)/2*Constant.ScreenSize.SCALE_SCREEN))" + "/h/" + "\(Int(((Constant.ScreenSize.SCREEN_WIDTH-12)/2)*0.6*Constant.ScreenSize.SCALE_SCREEN))"
                            imageview.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "loadingDefault"))
                        }
                        if let href = child["attributes"]["href"].string {
                            imageview.isUserInteractionEnabled = true
                            imageview.href = href
                            imageview.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(TopicView.imageViewTaped(_:))))
                        }
                        views.append(imageview)
                    }
                    
                    let childLeft = views[0]
                    let childRight = views[1]
                    
                    self.addSubview(childLeft)
                    self.addSubview(childRight)
                    

                    childLeft.snp_makeConstraints(closure: { (make) -> Void in
                        let _ = make.top.equalTo(self).offset(8)
                        let _ = make.bottom.equalTo(self)
                        let _ = make.left.equalTo(self).offset(MARGIN_10)
                        let _ = make.right.equalTo(self).offset(-MARGIN_10)
                    })
                    
                    childRight.snp_makeConstraints(closure: { (make) -> Void in
                        let _ = make.top.equalTo(self).offset(8)
                        let _ = make.bottom.equalTo(self)
                        let _ = make.left.equalTo(childLeft.snp_right).offset(8)
                        let _ = make.width.equalTo(childLeft)
                    })
                    
                    childLeft.snp_makeConstraints(closure: { (make) -> Void in
                        let _ = make.height.equalTo(childLeft.snp_width).multipliedBy(0.6)
                    })
                }
            }
            return self
        }
        
       func imageViewTaped(_ sender: UITapGestureRecognizer) {
            if let href = (sender.view as? ImageView)?.href {
                
                self.hrefHandler?(href, nil)
            }
        }
        
        class ImageView: UIImageView {
            var href: String?
        }
    }
    
    class GridView: GiftViewChildren {
        
        override func reloadData(_ data: JSON) -> UIView {
            if let children = data["children"].array {
                if children.count != 6 {
                    print("选礼配置文件发生严重错误，分区仅支持 6 个栏目")
                }else{
                    var views = [Button]()
                    for child in children {
                        let button = Button()
                        if let src = child["attributes"]["src"].string {
                            let imageScale : String = src + "?imageView2/0/w/" + "\(Int(0.6 * Constant.ScreenSizeV2.SCREEN_WIDTH * Constant.ScreenSize.SCALE_SCREEN))" + "/h/" + "\(Int(0.6 * Constant.ScreenSizeV2.SCREEN_WIDTH * Constant.ScreenSize.SCALE_SCREEN))"
                            button.imageView.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "loadingDefault"))
                        }
                        if let title = child["attributes"]["title"].string {
                            button.titleLabel.font = UIFont.systemFont(ofSize: 15)
                            button.titleLabel.textColor = UIColor(rgba: "#a1a1a1")
                            button.titleLabel.text = title
                        }
                        if let href = child["attributes"]["href"].string {
                            button.isUserInteractionEnabled = true
                            button.href = href
                            button.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(GridView.buttonClickd(_:))))
                        }
                        views.append(button)
                    }
                    self.buttons = views
                }
            }
            
            return self
        }
        
        var buttons = [Button]() {
            didSet {
                for button  in self.subviews {
                    button.removeFromSuperview()
                }
                for button in buttons {
                    self.addSubview(button)
                }
                self.setNeedsLayout()
            }
        }
        
        
      func buttonClickd(_ sender: UITapGestureRecognizer){
            if let href = (sender.view as? Button)?.href {
                self.hrefHandler?(href, nil)
            }
        }
        
        override func defaultInit() {
            super.defaultInit()
            self.snp_makeConstraints { (make) -> Void in
                let _ = make.height.equalTo(self.snp_width).multipliedBy(0.48)
            }
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            let frame = CGRect(x: 0, y: 0, width: self.frame.width/3, height: self.frame.height/2)
            for index in 0..<6 {
                let button = buttons[index]
                button.frame = frame
                button.center = CGPoint(x: frame.width * (0.5 + CGFloat(index%3)), y: frame.height * (0.5 + CGFloat(index/3)))
            }
        }
        
        class Button: UIView {
            
            class ImageView: UIImageView {
                
                override func layoutSubviews() {
                    super.layoutSubviews()
                    self.clipsToBounds = true
                    self.layer.cornerRadius = self.frame.width/2
                }
            }
            
            var href: String?
            var imageView = ImageView()
            var titleLabel = UILabel()
            var contentView = UIView()
            
            override init(frame: CGRect) {
                super.init(frame: frame)
                
                self.addSubview(contentView)
                contentView.snp_makeConstraints { (make) -> Void in
                    let _ = make.centerX.equalTo(self)
                    let _ = make.centerY.equalTo(self)
                }
                
                contentView.addSubview(imageView)
                contentView.addSubview(titleLabel)
                imageView.snp_makeConstraints { (make) -> Void in
                    let _ = make.top.equalTo(contentView)
                    let _ = make.width.equalTo(self).multipliedBy(0.6)
                    let _ = make.height.equalTo(imageView.snp_width)
                }

                
                titleLabel.snp_makeConstraints { (make) -> Void in
                    let _ = make.left.equalTo(contentView)
                    let _ = make.right.equalTo(contentView)
                    let _ = make.top.equalTo(imageView.snp_bottom).offset(4)
                    let _ = make.bottom.equalTo(contentView)
                    let _ = make.centerX.equalTo(imageView)
                }
            }
            
            required init(coder aDecoder: NSCoder) {
                fatalError("init(coder:) has not been implemented")
            }
            
        }
    }
    
    //MARK:  subject
    class SubjectView: GiftViewChildren {
        fileprivate func reloadData(_ data: [String : AnyObject]) -> UIView {
            
            var imageviews = [ImageView]()
            if let children = JSON(data as AnyObject)["children"].array {
                for child in children {
                    let imageview = ImageView()
                    if let src = child["attributes"]["src"].string {
                        let tempWidth = "\(Int(imageview.iwidth * Constant.ScreenSizeV1.SCALE_SCREEN))"
                        let tempHeight = "\(Int(imageview.iheight * Constant.ScreenSizeV1.SCALE_SCREEN))"
                        let tempSuffix = "?imageView2/0/w/" + tempWidth + "/h/" + tempHeight
                        let imageScale : String = String(src) + tempSuffix
                        imageview.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "loadingDefault"))
                    }
                    if let href = child["attributes"]["href"].string { 
                        imageview.href = href
                        imageview.isUserInteractionEnabled = true
                        imageview.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(SubjectView.imageViewTaped(_:))))
                    }
                    imageviews.append(imageview)
                }
                self.children = imageviews
            }
            return self
        }
        
        
    func imageViewTaped(_ sender: UITapGestureRecognizer) {
            if let href = (sender.view as? ImageView)?.href {
                hrefHandler?(href, nil)
            }
        }
        
        class ImageView: UIImageView {
            var href: String?
            var heightConstraint: NSLayoutConstraint?
            override var image: UIImage? {
                didSet {
                    var rate: CGFloat = 0.0001
                    if let size = image?.size {
                        rate = size.height/size.width
                    }
                    if heightConstraint != nil {
                        self.removeConstraint(heightConstraint!)
                    }
                    
                    self.snp_makeConstraints { (make) -> Void in
                        let _ = make.height.equalTo(self.snp_width).multipliedBy(rate)
                    }
                }
            }
        }
        
        var baker = UIView()
        var titleLabel = UILabel()
        var contentView = UIView()
        var children = [UIView]() {
            didSet {
                for subview in contentView.subviews {
                    subview.removeFromSuperview()
                }
                
                var lastView: UIView?
                for child in children {
                    contentView.addSubview(child)
                    

                    child.snp_makeConstraints(closure: { (make) -> Void in
                        
                        let _ = make.left.equalTo(contentView)
                        let _ = make.right.equalTo(contentView)

                    if lastView == nil {
                        let _ = make.top.equalTo(contentView)
                    }else{
                        let _ = make.top.equalTo(lastView!.snp_bottom).offset(6)
                    }
                    })
                    lastView = child
                }
                
                if lastView != nil {
                    lastView!.snp_makeConstraints(closure: { (make) -> Void in
                        let _ = make.bottom.equalTo(contentView)
                    })
                }
            }
        }
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            
            self.backgroundColor = UIColor.clear
            
            baker.backgroundColor = UIColor.black
            titleLabel.font = UIFont.systemFont(ofSize: 17)
            titleLabel.text = "专题"
            
            self.addSubview(baker)
            baker.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(self).offset(MARGIN_10)
                let _ = make.width.equalTo(3)
            }
            
            self.addSubview(titleLabel)
            titleLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(baker.snp_right).offset(4)
                let _ = make.centerY.equalTo(baker.snp_top)
                let _ = make.top.equalTo(self).offset(4)
                let _ = make.height.equalTo(baker).offset(2)
            }
            
            self.addSubview(contentView)
            
            contentView.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(self).offset(MARGIN_10)
                let _ = make.right.equalTo(self).offset(MARGIN_10)
                let _ = make.top.equalTo(titleLabel.snp_bottom).offset(8)
                let _ = make.bottom.equalTo(self).offset(-4)
            }
        }
        
        required init(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        
    }
    
    //MARK:  GeneralChild
    class GeneralChild: GiftViewChildren {
        var baker = UIView()
        var titleLabel = UILabel()
        
        var contentView = UIView()
        
        fileprivate func reloadData(_ data: [String : AnyObject]) -> UIView {
            var json = JSON(data as AnyObject)
            let name = json["attributes"]["title"][0]["name"].string ?? ""
            let nameEN = json["attributes"]["title"][0]["English_name"].string ?? ""
            titleLabel.attributedText = NSAttributedString(string: "<t color='black' size='15'>\(name) </t><t color='red' size='15'>\(nameEN)</t>")
            var subviews = [Button]()
            for view in contentView.subviews as! [Button]{
                view.removeFromSuperview()
                subviews.append(view)
            }
            
            var childrenViews = [Button]()
            if let children = json["children"].array {
                for child in children {
                    let button = (!subviews.isEmpty ? subviews.removeLast() : nil) ?? {()-> Button in
                        let button = Button()
                        button.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(GeneralChild.buttonTapped(_:))))
                        return button
                    }()
                    
                    childrenViews.append(button)
                    
                    button.titleLabel.text = child["attributes"]["title"].string
                    button.imageView.image = nil
                    if let url = child["attributes"]["src"].string {
                        let imageScale : String = url + "?imageView2/0/w/" + "\(Int((Constant.ScreenSize.SCREEN_WIDTH)/0.15*Constant.ScreenSize.SCALE_SCREEN))" + "/h/" + "\(Int((Constant.ScreenSize.SCREEN_WIDTH)/0.15*Constant.ScreenSize.SCALE_SCREEN))"
                        button.imageView.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "loadingDefault"))
                    }
                    button.href = child["attributes"]["href"].string
                    
                }
            }
            
            var lastView: UIView!
            for index in 0..<childrenViews.count {
                let child = childrenViews[index]
                contentView.addSubview(child)
                child.snp_makeConstraints(closure: { (make) -> Void in
                    if lastView == nil
                    {
                        let _ = make.top.equalTo(contentView)
                    }else{
                        let _ = make.top.equalTo(lastView.snp_bottom).offset(4)
                    }
                    
                    let _ = make.width.equalTo(contentView).multipliedBy(0.25)
                    let _ = make.width.equalTo(child.snp_width)
                    
                    let mlp = 0.25 + CGFloat(index%4)*0.5
                    
                    let _ = make.centerX.equalTo(contentView).multipliedBy(mlp)
                })
                
                
                if index%4 == 3 {
                    lastView = child
                }
            }
            
            if let lastView = childrenViews.last {
                lastView.snp_makeConstraints(closure: { (make) -> Void in
                    let _ = make.bottom.equalTo(contentView)
                })
            }
            
            return self
        }
        
        func buttonTapped(_ sender: UITapGestureRecognizer) {
            if let href = (sender.view as? Button)?.href {
                var params: [String: AnyObject] = [:]
                if let title = (sender.view as? Button)?.titleLabel.text {
                    if !title.isEmpty {
                        params["_title"] = title as AnyObject?
                    }
                }

                self.hrefHandler?(href, params)
            }
        }
        
        override init(frame: CGRect)
        {
            super.init(frame: frame)
            self.layer.borderColor = UIColor(rgba: Constant.common_borderColor_color).cgColor
            self.layer.borderWidth = 0.5
            
            baker.backgroundColor = UIColor.black
            
            self.addSubview(baker)
            self.addSubview(titleLabel)
            self.addSubview(contentView)
            
            baker.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(self).offset(8)
                let _ = make.top.equalTo(self).offset(8)
                let _ = make.width.equalTo(baker.snp_height).multipliedBy(0.2)
            }
            
            titleLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(baker.snp_right).offset(4)
                let _ = make.centerY.equalTo(baker)
                let _ = make.height.equalTo(baker)
            }
            
            contentView.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(self).offset(8)
                let _ = make.right.equalTo(self).offset(-8)
                let _ = make.top.equalTo(baker.snp_bottom).offset(4)
                let _ = make.bottom.equalTo(self).offset(-4)
            }
            
        }

        required init(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        class Button: UIView {
            
            class ImageView: UIImageView {
                
                override func layoutSubviews() {
                    super.layoutSubviews()
                    self.clipsToBounds = true
                    self.layer.cornerRadius = self.frame.width/2
                }
            }
            
            var href: String?
            
            var imageView = ImageView()
            
            var titleLabel = UILabel()
            
            var contentView = UIView()
            
            override init(frame: CGRect)
            {
                super.init(frame: frame)
                
                titleLabel.textColor = UIColor.black
            
                titleLabel.font = UIFont.systemFont(ofSize: 13)
                
                self.addSubview(contentView)
                
                contentView.snp_makeConstraints { (make) -> Void in
                    let _ = make.centerX.equalTo(self)
                    let _ = make.centerY.equalTo(self)
                }

                
                contentView.addSubview(imageView)
                
                contentView.addSubview(titleLabel)
                imageView.snp_makeConstraints { (make) -> Void in
                    let _ = make.top.equalTo(contentView)
                    let _ = make.width.equalTo(self).multipliedBy(0.6)
                    let _ = make.height.equalTo(imageView.snp_width)
                }

                imageView.snp_makeConstraints { (make) -> Void in
                    let _ = make.left.equalTo(contentView)
                    let _ = make.right.equalTo(contentView)
                }
                
                titleLabel.snp_makeConstraints { (make) -> Void in
                    let _ = make.top.equalTo(imageView.snp_bottom).offset(4)
                    let _ = make.bottom.equalTo(contentView)
                    let _ = make.centerX.equalTo(imageView)
                }
                
            }
            
            required init(coder aDecoder: NSCoder) {
                fatalError("init(coder:) has not been implemented")
            }
            
        }
    }
}
