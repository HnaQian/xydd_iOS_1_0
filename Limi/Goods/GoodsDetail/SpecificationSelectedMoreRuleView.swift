//
//  SpecificationSelectedMoreRuleView.swift
//  Limi
//
//  Created by maohs on 16/4/21.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

private var allButtonArray : [UIButton] = [UIButton]()
private var goodsAttr: [GoodsAttributeModel] = []
private var singIdArray = [String]()
class SpecificationSelectedMoreRuleView: UIView,SingleRuleModuleViewDelegate {

    //规格
    fileprivate let scrollview : UIScrollView = UIScrollView()
    fileprivate let numLabel = UILabel()
    fileprivate var tagLabel = UILabel()
    var valueTextField = UITextField()
    fileprivate var increaseButton = UIButton()
    fileprivate var decreaseButton = UIButton()
    
    var totalNum : Int = 0
    var currentNum : Int = 1
    var goodInfo: GoodsModel!
    
    var delegate:SpecificationSelectedMoreRuleViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setUpUI()
    }
    
    func setUpUI() {
        
        for view in [numLabel, increaseButton, decreaseButton, valueTextField] as [Any] {
            self.addSubview(view as! UIView)
        }
        numLabel.text = "数量"
        numLabel.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        numLabel.textColor = UIColor(rgba: Constant.common_C6_color)
        numLabel.textAlignment = .center
        
        valueTextField.font = UIFont.systemFont(ofSize: Constant.common_F2_font)
        valueTextField.textColor = UIColor(rgba: Constant.common_C6_color)
        valueTextField.text = "1"
        valueTextField.textAlignment = .center
        valueTextField.keyboardType = UIKeyboardType.numberPad
        valueTextField.addTarget(self, action: #selector(SpecificationSelectedMoreRuleView.buyCountChangeAction(_:)), for: UIControlEvents.allEditingEvents)
        valueTextField.addDoneOnKeyboard(withTarget: self, action:#selector(SpecificationSelectedMoreRuleView.doneAction))
        
        increaseButton.setImage(UIImage(named: "goods_order_info_attribute_button_NoAdd"), for: .disabled)
        increaseButton.setImage(UIImage(named: "goods_order_info_attribute_button_DoAdd"), for: UIControlState())
        decreaseButton.setImage(UIImage(named: "goods_order_info_attribute_button_NoReduce"), for: .disabled)
        decreaseButton.setImage(UIImage(named: "goods_order_info_attribute_button_DoReduce"), for: UIControlState())
        
        increaseButton.isEnabled = false
        decreaseButton.isEnabled = false
        
        increaseButton.tag = 1
        decreaseButton.tag = -1
        
        increaseButton.addTarget(self, action: #selector(SpecificationSelectedMoreRuleView.increasebuttonClicked), for: .touchUpInside)
        decreaseButton.addTarget(self, action: #selector(SpecificationSelectedMoreRuleView.decreasebuttonClicked), for: .touchUpInside)
    }

    override func layoutSubviews() {
        self.layoutIfNeeded()
    }
    
    
    override func layoutIfNeeded() {
        increaseButton.frame = CGRect(x: self.frame.size.width - MARGIN_10 - 40 , y: self.frame.size.height - 75 , width: 40, height: 40)
        valueTextField.frame = CGRect(x: self.frame.size.width - MARGIN_10 - 40 - 40 , y: self.frame.size.height - 75 , width: 40, height: 40)
        decreaseButton.frame = CGRect(x: self.frame.size.width - MARGIN_10 - 40 - 40 - 40 , y: self.frame.size.height - 75 , width: 40, height: 40)
        numLabel.frame = CGRect(x: MARGIN_10 , y: self.frame.size.height - 75 , width: 40, height: 40)
    }
    
    @objc fileprivate func buyCountChangeAction(_ textField: UITextField){
        
        if let content = textField.text{
            if content.characters.count > 0{
                
                if content.characters.count > 2{
                    if totalNum > 99 {
                        textField.text = "99"
                    }else {
                        textField.text = String(totalNum)
                    }
                }
                
                if content.intValue < 1{
                    textField.text = "1"
                }
                let temp = textField.text!
                currentNum = temp.intValue
                if currentNum > totalNum{
                    currentNum = totalNum
                    textField.text = "\(totalNum)"
                }
                
                if currentNum == 1 {
                    decreaseButton.isEnabled = false
                }else {
                    decreaseButton.isEnabled = true
                }
                
                if currentNum < totalNum {
                    increaseButton.isEnabled = true
                }else {
                    increaseButton.isEnabled = false
                }
                
                delegate?.didChangeGoodsNumber(currentNum)
            }else {
                decreaseButton.isEnabled = false
                increaseButton.isEnabled = false
            }
        }
    }
    
    //编辑完成
    @objc fileprivate func doneAction(){
        if let content = valueTextField.text{
            if content.intValue > 0 && content.intValue <= totalNum{
               
            }else{
                //不合法就换还原
                if content.isEmpty {
                    valueTextField.text = "1"
                }else {
                    valueTextField.text = "\(totalNum)"
                }
            }
            
            if content.intValue == 1 {
                decreaseButton.isEnabled = false
            }else {
                decreaseButton.isEnabled = true
            }
            
            if content.intValue < totalNum {
                increaseButton.isEnabled = true
            }else {
                increaseButton.isEnabled = false
            }
            
        }
        
        if let tmpString = valueTextField.text {
            currentNum = tmpString.intValue
            delegate?.didChangeGoodsNumber(currentNum)
        }
        
        valueTextField.resignFirstResponder()
    }

    //商品规格的可用组合
    fileprivate var goodsSubModeAry = [GoodsSubModel]()
    
//    //商品的规格数组
//    private var goodsAttributeModeAry = [GoodsAttributeModel]()

    
    func updateUI(_ goodInfo : JSON) ->[String] {
        
        self.goodInfo = GoodsModel(goodInfo.dictionaryObject)
        
        self.addSubview(scrollview)
        var contsizeHeight : CGFloat = 0
        var tmpSingIdArr = [String]()
        
        if let attrs = self.goodInfo.goods_attr{
            if let Goods_sub  = self.goodInfo.goods_sub{
                self.goodsSubModeAry = Goods_sub
                goodsAttr = attrs

                //重新组合成新的数组
                var tmpGoodsSubArr = Goods_sub
                
                for tmpIndex in 0 ..< attrs.count {
                    tmpGoodsSubArr = reorderGoodsArray(tmpGoodsSubArr, outAttr: attrs[attrs.count - 1 - tmpIndex])
                }
                
                self.goodsSubModeAry = tmpGoodsSubArr
                
                goodsAttr = attrs
                
                //查找第一个可用组合
                for index in 0 ..< self.goodsSubModeAry.count
                {
                    let sub = self.goodsSubModeAry[index]
                    if sub.Goods_num > 0{
                        
                        //可用组合拿出来
                        let idArr : [String] = sub.attr_item_idsAry
                        
                        singIdArray = idArr
                        contsizeHeight = self.addRules(attrs,id: idArr)
                        tmpSingIdArr = idArr
                        
                        let discount_price : Double = self.goodInfo.discount_price
                        let floatPrice : Double =  sub.price
                        let convertPrice = discount_price + floatPrice
                        var convertmPrice = sub.oriPrice
                        if convertmPrice == 0.0 {
                            convertmPrice = self.goodInfo.market_price + sub.price
                        }
                                delegate?.didSelectedSpecialRuleWithPrice(Float(convertPrice),marketPrice: Float(convertmPrice))
                        
                        let num : Int = sub.Goods_num
                        self.totalNum = num
                        if num > 1{
                            self.totalNum = num
                            increaseButton.isEnabled = true
                            decreaseButton.isEnabled = false
                            
                        }
                        
                        break
                    }
                    
                }
                self.scrollview.contentSize = CGSize(width: self.frame.size.width, height: contsizeHeight)
            }
            
        }
        else{
            
            let num : Int = self.goodInfo.goods_num
            self.totalNum = num
            if num > 1{
                increaseButton.isEnabled = true
                decreaseButton.isEnabled = false
                
            }
        }
        if contsizeHeight >= 180{
            scrollview.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: 180 )
        }
        else{
            scrollview.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: contsizeHeight )
        }
        
        return tmpSingIdArr

    }
    
    func reorderGoodsArray(_ outSub:[GoodsSubModel],outAttr:GoodsAttributeModel) -> [GoodsSubModel] {
        var orderSub = [GoodsSubModel]()
        var tmpSubArr = [String]()
        for index in outAttr.attr_value! {
            for sub in outSub {
                tmpSubArr = sub.attr_item_ids.components(separatedBy: ",")
                if tmpSubArr.contains(String(index.id)) {
                    orderSub.append(sub)
                }
            }
        }
        
        return orderSub
    }

    
    func increasebuttonClicked(){
        currentNum = currentNum + 1
        valueTextField.text = String(currentNum)
        if currentNum > 1{
            decreaseButton.isEnabled = true
        }
        if currentNum  >= totalNum{
            currentNum = totalNum
            increaseButton.isEnabled = false
        }
        
        delegate?.didChangeGoodsNumber(currentNum)
    }
    
    func decreasebuttonClicked(){
        currentNum = currentNum - 1
        valueTextField.text = String(currentNum)
        if currentNum == 1{
            decreaseButton.isEnabled = false
        }
        if currentNum < totalNum{
            increaseButton.isEnabled = true
        }
        
        delegate?.didChangeGoodsNumber(currentNum)
    }

    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func ModuleViewHeight(_ array : [GoodsAttrValueModel]) -> CGFloat{
        let xGap : CGFloat = 5
        let height : CGFloat = 30
        var row : Int = 0
        let number : Int = array.count
        if number%3 == 0{
            row = number/3
        }
        else
        {
            row = number/3+1;
        }
        return 25 + CGFloat(row) * (height+xGap)
        
    }
    
    
    func addRules(_ array : [GoodsAttributeModel],id : [String]) -> CGFloat{
        var y : CGFloat = 0
        for index in 0 ..< array.count
        {
            
            let singView = SingleRuleModuleView(frame: CGRect(x: 0, y: y, width: Constant.ScreenSize.SCREEN_WIDTH, height: ModuleViewHeight(array[index].attr_value!)), Attr_value: array[index],id : id[index],index : index,idArr :id as NSArray,Goods_sub : self.goodsSubModeAry)
            
            singView.delegate = self
            singView.tag = index
            self.scrollview.addSubview(singView)
            y = y + ModuleViewHeight(array[index].attr_value!)
        }
        return y
    }

    func returnTheRule(_ idArray : [String]){
        self.delegate?.moreRuleChangedSingIdArr(idArray)
        self.valueTextField.text = "1"
        self.currentNum = 1
        for index in 0 ..< goodsSubModeAry.count
        {
            let sub = goodsSubModeAry[index]
            if sub.Goods_num > 0{
                let idArr : [String] = sub.attr_item_idsAry
                if idArr == idArray{
                    let discount_price : Double = goodInfo.discount_price
                    let floatPrice : Double =  sub.price
                    let convertPrice = discount_price + floatPrice
                    var convertmPrice = sub.oriPrice
                    if convertmPrice == 0.0 {
                        convertmPrice = self.goodInfo.market_price + sub.price
                    }
                            delegate?.didSelectedSpecialRuleWithPrice(Float(convertPrice),marketPrice: Float(convertmPrice))
                    
                    let num : Int = sub.Goods_num
                    self.totalNum = num
                    self.increaseButton.isEnabled = true
                    self.decreaseButton.isEnabled = false
                }
            }
            
        }
    }
    
    ///规格单元视图
    final class SingleRuleModuleView: UIView {
        let xGap : CGFloat = 20.0
        let kHeight : CGFloat = 30.0
        var kWidth : CGFloat = 0
        var id : String = ""
        var index : Int = 0
        
        ///所有规格组合
        var Goods_sub : [GoodsSubModel] = []
        ///所有规格
        var AttrArray : [GoodsAttrValueModel] = []
        var delegate: SingleRuleModuleViewDelegate?
        
        //Attr_value：当前显示的规格组  id:选择的规格  index:当前位置 idArr：选择的ID组 Goods_sub：所有规格组合
        init(frame: CGRect , Attr_value : GoodsAttributeModel,id : String,index : Int,idArr : NSArray,Goods_sub : [GoodsSubModel]) {
            super.init(frame: frame)
            self.index = index
            
            self.Goods_sub = Goods_sub
            let titleLabel = UILabel()
            titleLabel.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
            titleLabel.textColor = UIColor(rgba: Constant.common_C6_color)
            titleLabel.frame = CGRect(x: 10, y: 5, width: self.frame.size.width - 20, height: 15)
            titleLabel.text = Attr_value.attr_name
            self.addSubview(titleLabel)
            if let array : [GoodsAttrValueModel] = Attr_value.attr_value{
                self.addTheButtonSubViews(array,id: id)
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func addTheButtonSubViews(_ AttrArray : [GoodsAttrValueModel],id : String){
            
//            var otherIds = [String]()
//            
//             singIdArray.forEach { (otherId) in
//                if otherId != id{
//                    otherIds.append(otherId)
//                }
//            }
//            
//            //判断ids的数组个数是否为0
//            var tempAry = Goods_sub
//            for id in otherIds{
//                tempAry = getUseableCombinationWithTargetNorms(id, containAry: tempAry)
//            }
//            
//            let useAbleIds = getUseableAryWithTargetNormsIndex(singIdArray.indexOf(id)!, containAry: tempAry)
            
            
            self.id = id
            self.AttrArray = AttrArray
            var height : CGFloat = 0.0
            kWidth = (Constant.ScreenSize.SCREEN_WIDTH - xGap*4)/3.0
            for index in 0 ..< AttrArray.count
            {
                let button = UIButton(type: .custom)
                button.frame = CGRect(x: 10+(kWidth+xGap)*(CGFloat(index).truncatingRemainder(dividingBy: 3)), y: 25+((5+kHeight)*(CGFloat(index/3))), width: kWidth, height: kHeight)
                button.setTitle(AttrArray[index].name, for: UIControlState())
                
                button.layer.borderWidth = 0.5
                button.layer.cornerRadius = 3
                button.tag = AttrArray[index].id
                
//                if useAbleIds.count > 0 && useAbleIds.contains("\(AttrArray[index].id)"){
//                    
//                    if  AttrArray[index].id == Int(id) {
//                        button.layer.borderColor = UIColor(rgba: Constant.common_C1_color).CGColor
//                        button.setTitleColor(UIColor(rgba: Constant.common_C1_color), forState: .Normal)
//                    }else{
//                        
//                        //这个btn可点击
//                        button.setTitleColor(UIColor(rgba: Constant.common_C6_color), forState: .Normal)
//                        button.setTitleColor(UIColor(rgba: Constant.common_C1_color), forState: .Highlighted)
//                        button.layer.borderColor = UIColor(rgba: Constant.common_C7_color).CGColor
//                        button.enabled = true
//                    }
//                    
//                }else{
//                    button.layer.borderColor = UIColor.lightGrayColor().CGColor
//                    button.setTitleColor(UIColor.lightGrayColor(), forState: .Normal)
//                    button.enabled = false
//                }
                
//                #if false
                if  AttrArray[index].id == Int(id) {
                    button.layer.borderColor = UIColor(rgba: Constant.common_C1_color).cgColor
                    button.setTitleColor(UIColor(rgba: Constant.common_C1_color), for: UIControlState())
        
                }else {
                    if buttonCanClicked(AttrArray[index].id, refrenceArr: singIdArray){
                        //这个btn可点击
                        button.setTitleColor(UIColor(rgba: Constant.common_C6_color), for: UIControlState())
                        button.setTitleColor(UIColor(rgba: Constant.common_C1_color), for: .highlighted)
                        button.layer.borderColor = UIColor(rgba: Constant.common_C7_color).cgColor
                        
                    }else{
                        button.layer.borderColor = UIColor.lightGray.cgColor
                        button.setTitleColor(UIColor.lightGray, for: UIControlState())
                        button.isEnabled = false
                    }
                }
                
//                #endif
                
                button.titleLabel?.font = UIFont.systemFont(ofSize: Constant.common_F5_font)
                button.addTarget(self, action: #selector(SingleRuleModuleView.selcetTheRule(_:)), for: .touchUpInside)
                
                self.addSubview(button)
                allButtonArray.append(button)
                
                height = button.frame.origin.y + button.frame.size.height
            }
            
            let sep = UIView()
            sep.backgroundColor = UIColor(rgba: Constant.common_separatLine_color)
            sep.frame = CGRect(x: MARGIN_10, y: height + 4.5, width: self.frame.size.width - 2*MARGIN_10, height: 0.5)
            self.addSubview(sep)
        }
        
        
        func buttonCanClicked(_ tag:Int,refrenceArr:[String]) ->Bool {
            for sub in Goods_sub
            {
                if sub.attr_item_idsAry.contains(String(tag)) {
                    var tmpIndex: Int = sub.attr_item_idsAry.index(of: String(tag))!
                    
                    var contain = true
                    
                    //513,515,517
                    while tmpIndex > 0 {
                        if !sub.attr_item_idsAry.contains(refrenceArr[tmpIndex - 1]){
                            contain = false
                            break
                        }
                        tmpIndex -= 1
                    }
                    
                    if contain && sub.Goods_num > 0{return true}
                }
                
                continue
                
            }
        
            return false
        }
        
        
        func selcetTheRule(_ sender: UIButton){
            let idTag: Int = sender.tag
            var idArr = [String]()
            idArr = singIdArray
            idArr[self.tag] = String(idTag)
            
            for index in 0 ..< Goods_sub.count
            {
                let sub = Goods_sub[index]
                let idArray : [String] = sub.attr_item_idsAry
                    
                    //优先找上次选中的其他规格
                if idArr == idArray{
                    if sub.Goods_num > 0{
                        
                        resetButton(idArr)
                        singIdArray[self.tag] = String(idTag)
                        delegate?.returnTheRule(idArr)
                        return
                    }else {
                        break
                    }
                }
            }
            
            //如果选中的规格没货则找相关的
            for tmpIndex in 0 ..< Goods_sub.count {
                let tmpSub = Goods_sub[tmpIndex]
                let tmpIdArray : [String] = tmpSub.attr_item_idsAry
                
                if tmpIdArray.contains(String(idTag)) {
                    var arrayIndex: Int = idArr.index(of: String(idTag))!
                    var contain = true
                    while arrayIndex > 0 {
                        if !tmpIdArray.contains(idArr[arrayIndex - 1]){
                            contain = false
                            break
                        }
                        arrayIndex -= 1
                    }
                    
                    if contain && tmpSub.Goods_num > 0 {
                        resetButton(tmpIdArray)
                        singIdArray = tmpIdArray
                        delegate?.returnTheRule(tmpIdArray)
                        return
                    }
                }
            }
            
        }
        
        func resetButton(_ refrenceArr:[String]) {
            
            var tmpTag = 0
            for btn in allButtonArray {
                tmpTag = Int(btn.tag)
                if !refrenceArr.contains(String(tmpTag)) {
                    if  buttonCanClicked(tmpTag, refrenceArr: refrenceArr) {
                        btn.setTitleColor(UIColor(rgba: Constant.common_C6_color), for: UIControlState())
                        btn.layer.borderColor = UIColor(rgba: Constant.common_C7_color).cgColor
                        btn.isEnabled = true
                    }else {
                        btn.layer.borderColor = UIColor.lightGray.cgColor
                        btn.setTitleColor(UIColor.lightGray, for: UIControlState())
                        btn.setTitleColor(UIColor(rgba: Constant.common_C1_color), for: .highlighted)
                        btn.isEnabled = false
                    }
                }else {
                    btn.layer.borderColor = UIColor(rgba: Constant.common_C1_color).cgColor
                    btn.setTitleColor(UIColor(rgba: Constant.common_C1_color), for: UIControlState())
                    btn.setTitleColor(UIColor(rgba: Constant.common_C1_color), for: .highlighted)
                    btn.isEnabled = true
                }
                    
        }
            
    }
    
    }
}


//在containAry中查找包含targetNorms规格的组合
//如果返回值的元素个数为0：没有包含此数组
func getUseableCombinationWithTargetNorms(_ targetNorms:String,containAry:[GoodsSubModel])->[GoodsSubModel]{
    
    var targetAry = [GoodsSubModel]()
    
    if containAry.count > 0{
        containAry.forEach({ (subModel) in
            if subModel.attr_item_idsAry.contains(targetNorms){
                if subModel.Goods_num > 0{
                    targetAry.append(subModel)
                }
            }
        })
    }
    
    return targetAry
}


//获取当前规格组中可用规格
func getUseableAryWithTargetAttribute(_ goodsAttribute:GoodsAttributeModel,containAry:[GoodsSubModel])->[String]{
    
    var targetAry = [String]()
    
    if containAry.count > 0 && goodsAttribute.attr_value != nil{
        if goodsAttribute.attr_value!.count > 0{
            
            for combination in containAry{
                for attr in goodsAttribute.attr_value!{
                    if combination.attr_item_idsAry.contains("\(attr.attr_id)"){
                        targetAry.append("\(attr.attr_id)")
                    }
                }
            }
        }
    }
    
    return targetAry
}


//获取当前规格组中可用规格
func getUseableAryWithTargetNormsIndex(_ normsIndex:Int,containAry:[GoodsSubModel])->[String]{
    
    var targetAry = [String]()
    
    containAry.forEach { (subModel) in
        if !targetAry.contains(subModel.attr_item_idsAry[normsIndex]){
            targetAry.append(subModel.attr_item_idsAry[normsIndex])
        }
    }
    
    return targetAry
}



protocol SpecificationSelectedMoreRuleViewDelegate {
    
    func didSelectedSpecialRuleWithPrice(_ price:Float,marketPrice:Float)
    func didChangeGoodsNumber(_ goodsNum:Int)
    func moreRuleChangedSingIdArr(_ singIdArray:[String])
}

protocol SingleRuleModuleViewDelegate {
    func returnTheRule(_ idArray : [String])
}


