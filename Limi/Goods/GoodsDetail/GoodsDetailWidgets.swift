//
//  GoodsDetailWidgets.swift
//  Limi
//
//  Created by 千云锋 on 16/7/21.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

/**
 商品详情页面用到的一些控件
 */

import Foundation


private let SCREEN = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE


extension GoodsDetailViewController{
    
    /**
     1.商品预览
     2.限时抢购
     3.标题和子标题
     4.价格
     5.文字描述
     6.礼品参数
     7.商品特性
     */
    //MARK: 0 - Content - 头部视图
    class GoodsDetailHeadView:BaseGoodsDetailUnitView{
        
        /**商品预览图*/
        let goodsBannerView:GoodsBannerView = GoodsBannerView()
        
        /**限时抢购*/
        let limitSaleView:LimitSaleView = LimitSaleView()
        
        //标题和子标题
        let goodsTitleInfo:GoodsTitleInfo = GoodsTitleInfo()
        
        //商品价格
        let goodsPriceInfo:GoodsPriceInfo = GoodsPriceInfo()
        
        //心意说
        let storyView:StoryView = StoryView()
        
        //礼品参数
        let goodsAttributeView:GoodsAttributeView = GoodsAttributeView()
        
        //商品特权
        let goodsFeatureView:GoodsFeatureView = GoodsFeatureView()
        
        //商品推荐
        let goodsRecommandView:GoodsRecommandView = GoodsRecommandView()
        
        //图文详情跟商品参数的title
        
        //图文详情
        let detailInfoView:DetailInfoView = DetailInfoView()
        
        fileprivate var viewAry = [BaseGoodsDetailUnitView]()
        
        override func reloadData(_ dataModel: GoodsModel) {
            
            for view in viewAry{
                view.delegate = self.delegate
                view.reloadData(dataModel)
            }
            
            refreshUI()
        }
        
        override func reloadDataArr(_ dataModelArr: [GoodsRelationModel]) {
            
//            goodsRecommandView.reloadDataArr(dataModelArr)
            
//            refreshUI()
        }
        
        
        override func reloadDataArrOfSpce(_ dataModelArr: [GoodsSpceInfoModel]) {
//            self.goodsAttributeView.reloadDataArrOfSpce(dataModelArr)
        }
        
        override func setupUI() {
            
            viewAry.append(goodsBannerView)
            viewAry.append(limitSaleView)
            viewAry.append(goodsTitleInfo)
            viewAry.append(goodsPriceInfo)
            viewAry.append(storyView)
            viewAry.append(goodsFeatureView)
            viewAry.append(detailInfoView)
            viewAry.append(goodsAttributeView)
//            viewAry.append(goodsRecommandView)
            for view in viewAry{
                self.addSubview(view)
            }
            
            refreshUI()
            
            self.iwidth = Constant.ScreenSizeV2.SCREEN_WIDTH
        }
        
        
       func refreshUI(){
            
            var tempView:BaseGoodsDetailUnitView!
            var allHeight:CGFloat = 0
            
            for view in viewAry{
                
                view.iwidth = Constant.ScreenSizeV2.SCREEN_WIDTH
                
                if tempView != nil {
                    view.itop = tempView.ibottom
                }else{
                    view.itop = 0
                }
                
                tempView = view
                
                allHeight += view.iheight
                
            }
            
            self.iheight = allHeight
        }
        
    }
    
    
    
    
    //MARK: 1 - UnitView - 商品预览图
    class GoodsBannerView:BaseGoodsDetailUnitView, BBannerViewDelegate, BBannerViewDataSource{
        
        var imageSource: [GoodsModel]?//图片的url
        var tapActionBlock:TapActionBlock?
        
        fileprivate let banner = BBannerView(frame: CGRect(x: 0,y: 0,width: Constant.ScreenSize.SCREEN_WIDTH,height: Constant.ScreenSize.SCREEN_WIDTH))
        
        fileprivate let imageView = UIImageView()
        
        override func setupUI() {
            
            self.addSubview(banner)
            banner.delegate = self
            banner.dataSource = self
            
            banner.pageIndicatorTintColor = Constant.Theme.Color9
            banner.currentPageIndicatorTintColor = Constant.Theme.Color1
            
            self.iheight = banner.iheight
            self.iwidth = Constant.ScreenSizeV2.SCREEN_WIDTH
            
            self.addSubview(imageView)
            imageView.frame = CGRect(x: 0, y: 0, width: banner.iheight, height: banner.iheight)
            imageView.image = UIImage(named: "Default_800_800")
        }
        
        
        //MARK-:BBannerViewDelegate
        func didSelectItem(_ index: Int) {
            
            if let mdoel = self.goodsModel{
                if let imageAry = mdoel.goods_images{
                    var nameAry = [String]()
                    let _ = imageAry.map{nameAry.append($0.image)}
                    self.delegate?.baseGoodsDetailUnitViewTouchdAction(GoodsDetailUnitViewActionType.goodsPictureSelected, content: ["imageAry" : nameAry as AnyObject,"currentIndex":index as AnyObject])
                }
            }
        }
        
        //MARK-:√BBannerViewDataSource
        func viewForItem(_ bannerView: BBannerView, index: Int) -> UIView {
            let tempImageView = UIImageView()
            tempImageView.isUserInteractionEnabled = true
            tempImageView.frame = CGRect(x: 0, y: 0, width: banner.iwidth, height: banner.iheight)
            let imageScale : String = imgAry[index] + "?imageView2/0/w/" + "\(Int(banner.iwidth * Constant.ScreenSize.SCALE_SCREEN))" + "/h/" + "\(Int(banner.iheight * Constant.ScreenSize.SCALE_SCREEN))"
            tempImageView.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "Default_800_800"))
            return tempImageView
        }
        
        func numberOfItems() -> Int {
            return imgAry.count
        }
        
        fileprivate var imgAry = [String]()
        
        override func reloadData(_ dataModel: GoodsModel) {
            
            self.goodsModel = dataModel
            
            if let mimgAry = dataModel.goods_images{
                if mimgAry.count > 0 {
                    let _ = mimgAry.map{self.imgAry.append($0.image)}
                }
            }
            if self.imgAry.count > 0 {
                imageView.removeFromSuperview()
                banner.reloadData()
                banner.startAutoScroll(4)
            }
        }
    }
    
  
    //MARK: 2 - UnitView - 限时抢购
    class LimitSaleView:BaseGoodsDetailUnitView{
        
//        let SCREEN = Constant.Sc÷reenSizeV2.SCREEN_WIDTH_SCALE
        
        //背景图
        let backgroundView:UIView = {
            let view = UIView()
            view.backgroundColor = UIColor(rgba: "#ff3a48")
            view.alpha = 0.1
            return view
        }()
        
        let contentView:UIView = {
            let view = UIView()
            return view
        }()
        
        let titleLabel:UILabel = {
            let lbl = UILabel()
            lbl.font = Constant.Theme.Font_15
            lbl.textColor = Constant.Theme.Color1
            lbl.numberOfLines = 0
            return lbl
        }()

        let sloganLabel:UILabel = {
            let lbl = UILabel()
            lbl.font = Constant.Theme.Font_11
            lbl.textColor = UIColor(rgba: "#ED8129")
            return lbl
        }()
        
        let typeLabel:UILabel = {
            let typeLabel = UILabel()
            typeLabel.font = UIFont.boldSystemFont(ofSize: Constant.Theme.FontSize_19)
            typeLabel.textColor = Constant.Theme.Color1
            return typeLabel
        }()
        
        
        let countdownLabel:UILabel = {
            let lbl = UILabel()
            lbl.font = UIFont.systemFont(ofSize: 14)
            lbl.textColor = UIColor(rgba: "#999999")
            lbl.textAlignment = .right
            return lbl
        }()
        
        let timeView:TimeView = {
            let view = TimeView()
            view.isHidden = true
            return view
        }()

        fileprivate var  startDate: Date?
        fileprivate var endDate: Date?
        
        
        func onClick(){
//            self.delegate?.baseGoodsDetailUnitViewTouchdAction(GoodsDetailUnitViewActionType.limitSale, content:nil)
        }
        
        func countDown() {
            updateCountdownLabel()
            gcd.async(.main, delay: 1.0) {
                self.countDown()
            }
        }
        
        func updateCountdownLabel() {
            
            if let interval = startDate?.timeIntervalSinceNow {
                // 尚未开始
                if interval > 0 {
                    let tempArray = countStringWithInterval(interval)
                    countdownLabel.text = tempArray[0] == "0" ? "距离开始" : "距离开始\(tempArray[0])天"
                    if timeView.isHidden {
                        timeView.isHidden = false
                    }
                    timeView.hourLabel.text = tempArray[1]
                    timeView.minuteLabel.text = tempArray[2]
                    timeView.secondLabel.text = tempArray[3]
                    if tempArray[0] != "0" {
                        let attributedString = NSMutableAttributedString(string: countdownLabel.text! as String)
                        let attributes = [NSForegroundColorAttributeName: UIColor(rgba: "#333333")]
                        attributedString.addAttributes(attributes, range: NSMakeRange(3, tempArray[0].characters.count + 1))
                        countdownLabel.attributedText = attributedString
                    }
                    return
                }
            }
            if let interval = endDate?.timeIntervalSinceNow {
//                 尚未结束
                if interval > 0 {
                    let tempArray = countStringWithInterval(interval)
                    countdownLabel.text = tempArray[0] == "0" ? "仅剩" : "仅剩\(tempArray[0])天"
                    if timeView.isHidden {
                        timeView.isHidden = false
                    }
                    timeView.hourLabel.text = tempArray[1]
                    timeView.minuteLabel.text = tempArray[2]
                    timeView.secondLabel.text = tempArray[3]
                    if tempArray[0] != "0" {
                        let attributedString = NSMutableAttributedString(string: countdownLabel.text! as String)
                        let attributes = [NSForegroundColorAttributeName: UIColor(rgba: "#333333")]
                        attributedString.addAttributes(attributes, range: NSMakeRange(2, tempArray[0].characters.count + 1))
                        countdownLabel.attributedText = attributedString
                    }
                    return
                }else{
                    // 活动已结束
                    if !timeView.isHidden {
                        timeView.isHidden = true
                    }
                     let attributedString = NSMutableAttributedString(string: "活动已结束")
                    let attributes = [NSForegroundColorAttributeName: UIColor(rgba: Constant.common_C1_color),NSFontAttributeName : UIFont.systemFont(ofSize: 13)]
                    attributedString.addAttributes(attributes, range:  NSMakeRange(0, 5))
                    countdownLabel.attributedText = attributedString
                    countdownLabel.iright = Constant.ScreenSizeV2.SCREEN_WIDTH - 30 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
                }
            }
        }
        
        // return xml string
        func countStringWithInterval(_ interval: TimeInterval)->[String] {
            let day = String(format:"%i", Int(interval/(3600 * 24)))
            let hour = String(format:"%.2i", Int(interval/3600))
            let minute = String(format: "%.2i", Int(interval.truncatingRemainder(dividingBy: 3600)/60))
            let second = String(format:"%.2i", Int(interval.truncatingRemainder(dividingBy: 60)))
            
            return [day,hour,minute,second]
            
        }

        
        fileprivate var model = GoodsModel()
        
        func initUI() {
            
            self.iheight = 120 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
            self.iwidth = Constant.ScreenSizeV2.SCREEN_WIDTH
            
            self.addSubview(contentView)

            contentView.snp_makeConstraints(closure: { (make) -> Void in
                let _ = make.left.equalTo(self)
                let _ = make.top.equalTo(self)
                let _ = make.size.equalTo(self)
            })
            
            
            contentView.addSubview(titleLabel)
            contentView.addSubview(sloganLabel)
            contentView.addSubview(timeView)
            contentView.addSubview(countdownLabel)
            
            contentView.addSubview(typeLabel)
            contentView.addSubview(backgroundView)
            
            
            typeLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.bottom.equalTo(contentView.snp_centerY)
                let _ = make.left.equalTo(Constant.ScreenSizeV2.MARGIN_26)
                
            }

            sloganLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(typeLabel)
                let _ = make.top.equalTo(typeLabel.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_4)
            }
            
            titleLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(sloganLabel)
                let _ = make.bottom.equalTo(contentView.snp_bottom).offset(-Constant.ScreenSizeV2.MARGIN_16)
            }
            
            timeView.snp_makeConstraints { (make) in
                let _ = make.centerY.equalTo(contentView)
                let _ = make.right.equalTo(contentView).offset(-30 * SCREEN)
                let _ = make.height.equalTo(timeView.iheight)
                let _ = make.width.equalTo(timeView.iwidth)
            }
            
            countdownLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.centerY.equalTo(contentView)
                let _ = make.right.equalTo(timeView.snp_left).offset(-MARGIN_8)
                let _ = make.height.equalTo(15)
            }
            
            backgroundView.snp_makeConstraints { (make) in
                let _ = make.top.equalTo(contentView)
                let _ = make.bottom.equalTo(contentView)
                let _ = make.width.equalTo(contentView)
                let _ = make.left.equalTo(contentView)
            }
            
        }
        

        
        override func reloadData(_ dataModel: GoodsModel) {

            let hasEvent = dataModel.has_event
            if hasEvent == 1 {
                initUI()
                self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(LimitSaleView.onClick)))
                
                if let event = dataModel.event {
                    let type = event.type
                    let title = event.name
                    let typeDes = [1: "让利", 2: "折扣", 3: "仅剩", 4: "仅剩", 5: "仅剩", 6: "仅剩"][type]
                    var price = "0.00"
                    let eventPrice : Double = event.price
                    if eventPrice > 0{
                        let floatPrice = round(eventPrice)
                        if floatPrice == eventPrice{
                            price = String(format: "￥%.0f", eventPrice)
                        }
                        else{
                            let priceStr1 : NSString = String(format: "%.2f", eventPrice) as NSString
                            let priceStr2 : NSString = String(format: "%.1f", eventPrice) as NSString
                            let floatPrice1 : Float = priceStr1.floatValue
                            let floatPrice2 : Float = priceStr2.floatValue
                            if floatPrice1 == floatPrice2{
                                price = String(format: "￥%.1f", eventPrice)
                            }
                            else{
                                price = String(format: "￥%.2f", eventPrice)
                            }
                        }
                    }
                    
                    var slogan = event.slogan
                    var startDateStr = event.start_time
                    var endDateStr = event.end_time
                    let value = event.point
                    
                    switch type {
                    case 1: slogan = ""; price += " "
                    case 2: slogan = ""; price += " "
                    case 3: price = ""; slogan = ""
                    case 4: price = ""; endDateStr = ""; startDateStr = ""
                    case 5: price += " "; endDateStr = ""; startDateStr = ""
                    case 6: price = ""; endDateStr = ""; startDateStr = ""
                    default: break
                    }
                    
                    titleLabel.text = title
                    typeLabel.text = typeDes! + value
                    sloganLabel.text = slogan
                    if !startDateStr.isEmpty && !endDateStr.isEmpty {
                        let formatter = DateFormatter()
                        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                        startDate = formatter.date(from: startDateStr)
                        endDate = formatter.date(from: endDateStr)
                        countDown()
                    }
                }
            }else{
                self.iheight = 0
            }
        }

        
    }
    
    
    
    
    //MARK: 3 - UnitView - 商品标题
    class GoodsTitleInfo:BaseGoodsDetailUnitView{
        
        //商品主标题
        fileprivate let goodsNameLabel:UILabel = {
            let lbl = UILabel()
//            lbl.font = Constant.Theme.Font_19
            lbl.font = UIFont.boldSystemFont(ofSize: 18)
            lbl.textColor = Constant.Theme.Color14
            lbl.numberOfLines = 0
            return lbl
        }()
        
        //商品内容
        fileprivate let goodsSubTitleLabel:UILabel = {
            let lbl = UILabel()
            lbl.font = UIFont.systemFont(ofSize: 14)
            lbl.numberOfLines = 0
            
            lbl.textColor = Constant.Theme.Color7
            return lbl
        }()
        
        
        override func setupUI() {
            
            self.iwidth = Constant.ScreenSizeV2.SCREEN_WIDTH
            let _ = [goodsNameLabel, goodsSubTitleLabel].map{self.addSubview($0)}
            self.backgroundColor = UIColor.white
        }
        
        //布局
        func composition() {
            
            
            let height = CalculateHeightOrWidth.getLabOrBtnHeigh(self.goodsNameLabel.text! as NSString, font: self.goodsNameLabel.font, width: Constant.ScreenSizeV2.SCREEN_WIDTH - 2 * Constant.ScreenSizeV2.MARGIN_30)
            
            goodsNameLabel.frame = CGRect(x: Constant.ScreenSizeV2.MARGIN_30, y: 35 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE, width: Constant.ScreenSizeV2.SCREEN_WIDTH - 2 * Constant.ScreenSizeV2.MARGIN_30 , height: height)
            
            
            let subHeight = CalculateHeightOrWidth.getLabOrBtnHeigh(self.goodsSubTitleLabel.text! as NSString, font: self.goodsSubTitleLabel.font, width: Constant.ScreenSizeV2.SCREEN_WIDTH - 2 * Constant.ScreenSizeV2.MARGIN_30)
            goodsSubTitleLabel.frame = CGRect(x: Constant.ScreenSizeV2.MARGIN_30, y: Constant.ScreenSizeV2.MARGIN_49 + height, width: Constant.ScreenSizeV2.SCREEN_WIDTH - 2 * Constant.ScreenSizeV2.MARGIN_30, height: subHeight)
            
            self.iheight = Constant.ScreenSizeV2.MARGIN_40 + height + Constant.ScreenSizeV2.MARGIN_20 + subHeight
        }
        
        
        
        override func reloadData(_ dataModel: GoodsModel) {
            
            goodsNameLabel.text = dataModel.goods_name
            if dataModel.goods_subtitle != "null" {
                goodsSubTitleLabel.text = dataModel.goods_subtitle
            }
            
            
            composition()
            
            if dataModel.goods_subtitle == ""{
                self.iheight = Constant.ScreenSizeV2.MARGIN_40 + self.goodsNameLabel.frame.size.height
            }
            
            
        }
    }
    
    
    
    //MARK: 4 - UnitView - 商品价格
    class GoodsPriceInfo:BaseGoodsDetailUnitView{
        
        //商品现在的价格
        fileprivate let discountPrice:UILabel = {
            let lbl = UILabel()
            //            lbl.font = Constant.Theme.Font_29
            lbl.font = UIFont.boldSystemFont(ofSize: 22)
            lbl.text = "￥"
            lbl.textColor = Constant.Theme.Color1
            return lbl
        }()
        
        //市场价
        let marketPriceLabel:StrikeThroughLabel = {
            let label = StrikeThroughLabel()
            label.textColor = UIColor(rgba: Constant.common_C7_color)
            label.textAlignment = .left
            label.text = "￥"
            label.font = UIFont.systemFont(ofSize: 14)
            label.strikeThroughEnabled = true
            return label
        }()
        
        
        func onClick(_ button:UIButton){
            let index = GoodsDetailUnitViewActionType.goodsPriceInfo.rawValue
            self.delegate?.baseGoodsDetailUnitViewTouchdAction(GoodsDetailUnitViewActionType.goodsPriceInfo, content: [ActionKeys[index]:button])
        }
        
        override func setupUI() {
            self.backgroundColor = UIColor.white
            let _ = [discountPrice, marketPriceLabel].map{self.addSubview($0)}
            
            
        }
        
        
        deinit{
            NotificationCenter.default.removeObserver(self)
        }
        
        override func reloadData(_ dataModel: GoodsModel) {
            
            
            if dataModel.event == nil {
                priceSet(dataModel)
            }else{
                if let eventModel = dataModel.event {
                    
                    if eventModel.type == 1 || eventModel.type == 2 || eventModel.type == 5 {
                        discountPrice.text = normalPrice(price: Float("\(eventModel.price)")!)
                        marketPriceLabel.text = normalPrice(price: Float("\(dataModel.market_price)")!)
                        if discountPrice.text == marketPriceLabel.text {
                            self.marketPriceLabel.removeFromSuperview()
                        }
                        if dataModel.price == 0 {
                            self.marketPriceLabel.removeFromSuperview()
                        }
                    }else{
                        priceSet(dataModel)
                    }

                }
                
                
            }
            //对商品价格之前的人民币符号大小设置
            let attributedString = NSMutableAttributedString(string: discountPrice.text! as String)
            attributedString.addAttribute(NSFontAttributeName, value: Constant.Theme.Font_13, range: NSRange(location: 0, length: 1))
            
            discountPrice.attributedText = attributedString
            composition()
        }
        
        func priceSet(_ dataModel:GoodsModel){
            discountPrice.text = normalPrice(price: Float("\(dataModel.price)")!)
            marketPriceLabel.text = normalPrice(price: Float("\(dataModel.market_price)")!)
            if discountPrice.text == marketPriceLabel.text {
                self.marketPriceLabel.removeFromSuperview()
            }
            if dataModel.market_price == 0 {
                self.marketPriceLabel.removeFromSuperview()
            }
        }

        func normalPrice(price:Float) -> String {
            var resultPrice = "0"
            if price > 0{
                let floatPrice = roundf(price)
                if floatPrice == price{
                    let tempString = String(format: "￥%.2f", price)
                    if (tempString.contains(".00")) {
                        resultPrice = String(format: "￥%.0f", price)
                    }
                }
                else{
                    let priceStr1 : NSString = String(format: "%.2f", price) as NSString
                    let priceStr2 : NSString = String(format: "%.1f", price) as NSString
                    let floatPrice1 : Float = priceStr1.floatValue
                    let floatPrice2 : Float = priceStr2.floatValue
                    if floatPrice1 == floatPrice2{
                        resultPrice = String(format: "￥%.1f", price)
                    }
                    else{
                        resultPrice = String(format: "￥%.2f", price)
                    }
                }
            }
            
            return resultPrice
        }
        
        //布局
        func composition(){
            let widthOfDiscountPrice = CalculateHeightOrWidth.getLabOrBtnWidth(discountPrice.text! as NSString, font: UIFont.boldSystemFont(ofSize: 22), height: Constant.ScreenSizeV2.MARGIN_40)
            discountPrice.frame = CGRect(x: Constant.ScreenSizeV2.MARGIN_30, y: Constant.ScreenSizeV2.MARGIN_40, width: widthOfDiscountPrice, height: Constant.ScreenSizeV2.MARGIN_40)
            
            let widthOfMarketPrice = CalculateHeightOrWidth.getLabOrBtnWidth(marketPriceLabel.text! as NSString, font: marketPriceLabel.font, height: Constant.ScreenSizeV2.MARGIN_40)
            marketPriceLabel.frame = CGRect(x: Constant.ScreenSizeV2.MARGIN_49 + Constant.ScreenSizeV2.MARGIN_20 + widthOfDiscountPrice, y: Constant.ScreenSizeV2.MARGIN_40, width: widthOfMarketPrice, height: Constant.ScreenSizeV2.MARGIN_40)
            marketPriceLabel.center.y = discountPrice.center.y
           
            self.iheight = 3 * Constant.ScreenSizeV2.MARGIN_40
        }
    }
    
    
    //MARK: 5 - UnitView - 心意说
    class StoryView:BaseGoodsDetailUnitView{
        
        fileprivate let giftStory:UILabel = {
            let lbl = UILabel()
            lbl.font = UIFont.boldSystemFont(ofSize: Constant.Theme.FontSize_16)
            lbl.text = "心意说"
            lbl.textColor = Constant.Theme.Color14
            lbl.textAlignment = .center
            return lbl
        }()
        
        fileprivate let seperateLine:UIView = {
            let view = UIView()
            view.alpha = 0.5
            view.backgroundColor = UIColor(rgba: "#ff818a")
            return view
        }()
        
        fileprivate let contentLabel:UILabel = {
            let lbl = UILabel()
            lbl.font = UIFont.systemFont(ofSize: 14)
            lbl.textColor = Constant.Theme.Color14
            lbl.numberOfLines = 0
            return lbl
        }()
        let seperateView = UIView()
        
       func initUI() {
        
            self.backgroundColor = UIColor.white
            self.addSubview(seperateView)
            self.addSubview(giftStory)
            self.addSubview(seperateLine)
            self.addSubview(contentLabel)
            let scale = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
            giftStory.frame = CGRect(x: 30 * scale, y: 48 * scale, width: Constant.ScreenSizeV2.SCREEN_WIDTH - 60 * scale, height: 17)
            seperateLine.frame = CGRect(x: 30 * scale, y: giftStory.ibottom + 27 * scale, width: 140 * scale, height: 4 * scale)
            seperateLine.icenterX = giftStory.icenterX
        
            seperateView.backgroundColor = UIColor(rgba: Constant.common_separatLine_color)
            seperateView.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: 1)
            self.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: seperateLine.ibottom + 199 * scale)
        }
        
        override func reloadData(_ dataModel: GoodsModel) {
            contentLabel.text = dataModel.gift_story
            
            if dataModel.gift_story != "" {
                initUI()
                let height = CalculateHeightOrWidth.getLabOrBtnHeigh(dataModel.gift_story as NSString, font: contentLabel.font, width: Constant.ScreenSizeV2.SCREEN_WIDTH - 2 * Constant.ScreenSizeV2.MARGIN_30)
                contentLabel.frame = CGRect(x: Constant.ScreenSizeV2.MARGIN_30, y: seperateLine.ibottom + 27 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE, width: Constant.ScreenSizeV2.SCREEN_WIDTH - 2 * Constant.ScreenSizeV2.MARGIN_30, height: height)
                
                let attributedString = NSMutableAttributedString(string: contentLabel.text! as String)
                
                let paragraphStyle = NSMutableParagraphStyle()
                paragraphStyle.lineSpacing = 4
                attributedString.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: NSMakeRange(0, (contentLabel.text?.characters.count)!))
                
                contentLabel.attributedText = attributedString
                contentLabel.sizeToFit()
                self.iheight = contentLabel.ibottom + 40 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
            }else {
                self.iheight = 0
            }
           
        }
    }
    
    
    
    //MARK: 6 - UnitView - 礼品参数
    class GoodsAttributeView:BaseGoodsDetailUnitView{
        
        
        //礼品参数Label
        lazy fileprivate var titleLabel:UILabel = {
            let lbl = UILabel()
            lbl.font = UIFont.boldSystemFont(ofSize: Constant.Theme.FontSize_16)
            lbl.textColor = Constant.Theme.Color14
            lbl.textAlignment = .center
            
            return lbl
        }()
        
        lazy fileprivate var titleLineView:UIView = {
            let view = UIView()
            view.backgroundColor = UIColor(rgba: "#ff818a")
            view.alpha = 0.5
            return view
        }()
        
        lazy fileprivate var contentView:UIView = {
            let view = UIView()
            view.backgroundColor = UIColor.white
            return view
        }()
        
       override func reloadData(_ dataModel:GoodsModel){
            let scale = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
            if let tempData = dataModel.paramsInfo {
                if tempData.count > 0 {
                    if let temp = tempData[0].detail {
                        if temp.count > 0 {
                            self.titleLabel.text = "礼品参数"
                            self.addSubview(contentView)
                            contentView.addSubview(titleLabel)
                            contentView.addSubview(titleLineView)
                            contentView.frame = CGRect(x: 0, y: 20 * scale, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: 20)
                            titleLabel.frame = CGRect(x: 30 * scale, y: 48 * scale, width: Constant.ScreenSizeV2.SCREEN_WIDTH - 60 * scale, height: 14 + Constant.ScreenSizeV2.MARGIN_4)
                            titleLineView.frame = CGRect(x: 0, y: titleLabel.ibottom + 27 * scale, width: 140 * scale, height: 4 * scale)
                            titleLineView.icenterX = Constant.ScreenSizeV2.SCREEN_WIDTH / 2
                            let viewHeight = titleLineView.ibottom + 27 * scale
                            createLabel(labelHeight: viewHeight, labelArray: temp)
                        }
                        
                    }
                }
            }
        }
        
        func createLabel(labelHeight:CGFloat,labelArray:[DetailParamsInfo]) {
            let scale = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
            let seperate = Constant.ScreenSizeV2.MARGIN_4
            var height = labelHeight
            for item in labelArray {
                let nameLabel = UILabel()
                nameLabel.font = Constant.Theme.Font_14
                nameLabel.textColor = Constant.Theme.Color7
                nameLabel.textAlignment = .left
                
                let contentLabel = UILabel()
                contentLabel.font = Constant.Theme.Font_14
                contentLabel.textColor = Constant.Theme.Color14
                contentLabel.textAlignment = .left
                contentLabel.numberOfLines = 0
                
                self.addSubview(nameLabel)
                self.addSubview(contentLabel)
                
                nameLabel.text = item.key
                contentLabel.text = item.value

                let tempWidth = CalculateHeightOrWidth.getLabOrBtnWidth("测试测试测试" as NSString, font: contentLabel.font, height: 48 * scale)
                nameLabel.frame = CGRect(x: 30 * scale, y: height + seperate * 2, width: tempWidth + 30 * scale, height: 48 * scale)
                
                var tempHeight = CalculateHeightOrWidth.getLabOrBtnHeigh(item.value as NSString, font: contentLabel.font, width: Constant.ScreenSizeV2.SCREEN_WIDTH - nameLabel.iright - seperate * 2 - 30 * scale)
        
                tempHeight = max(nameLabel.iheight, tempHeight)
                contentLabel.frame = CGRect(x: nameLabel.iright + seperate * 2, y: height + seperate * 2, width: Constant.ScreenSizeV2.SCREEN_WIDTH - nameLabel.iright - 30 * scale - seperate * 2, height: tempHeight)
                height += tempHeight
            }
            contentView.iheight = height + seperate * 2
            self.iheight = contentView.iheight + scale * 40
        }
        
    }
    
    
    
    //MARK: 7 - UnitView - 商品特权
    class GoodsFeatureView:BaseGoodsDetailUnitView{
        
        let imageArr = ["icon_ authentic", "icon_aftersale", "icon_giftbox", "icon_deliver"]
        let labelArr = ["正品保证", "售后无忧", "定制礼盒", "单件包邮"]
        
        override func setupUI() {
            self.isHidden = true
            self.backgroundColor = UIColor.white
            self.iheight = Constant.ScreenSizeV2.MARGIN_64 + Constant.ScreenSizeV2.MARGIN_40
            let seperateLineView = UIView()
            seperateLineView.backgroundColor = UIColor(rgba: Constant.common_separatLine_color)
            self.addSubview(seperateLineView)
            seperateLineView.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: 1)
            
            
            for i in 0...3 {
                let view = UIView()
                view.frame.size = CGSize(width: 2 * Constant.ScreenSizeV2.MARGIN_44 + Constant.ScreenSizeV2.MARGIN_64, height: Constant.ScreenSizeV2.MARGIN_44)
                let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.MARGIN_40, height: Constant.ScreenSizeV2.MARGIN_40))
                imageView.image = UIImage(named: imageArr[i])
                imageView.center.y = view.center.y
                
                let label = UILabel()
                label.frame = CGRect(x: Constant.ScreenSizeV2.MARGIN_44, y: 0, width: Constant.ScreenSizeV2.MARGIN_64 + Constant.ScreenSizeV2.MARGIN_49, height: Constant.ScreenSizeV2.MARGIN_20)
                label.text = labelArr[i]
                label.font = Constant.Theme.Font_13
                
                label.textColor = Constant.Theme.Color7
                label.center.y = view.center.y
                view.addSubview(imageView)
                view.addSubview(label)
                view.frame.origin = CGPoint(x: Constant.ScreenSizeV2.MARGIN_30 + CGFloat(i) * (2 * Constant.ScreenSizeV2.MARGIN_40 + Constant.ScreenSizeV2.MARGIN_60 + Constant.ScreenSizeV2.MARGIN_36), y: 0)
                view.center.y = self.center.y
                self.addSubview(view)
            }
        }
        override func reloadData(_ dataModel: GoodsModel) {
            self.isHidden = false
            let gr = UITapGestureRecognizer(target: self, action: #selector(GoodsFeatureView.onClick))
            gr.numberOfTapsRequired = 1
            isUserInteractionEnabled = true
            addGestureRecognizer(gr)
        }
        
        func onClick(){
            let index = GoodsDetailUnitViewActionType.goodsFeature.rawValue
            Statistics.count(Statistics.Goods.goods_detail_description_click)
            self.delegate?.baseGoodsDetailUnitViewTouchdAction(GoodsDetailUnitViewActionType.goodsFeature, content: [ActionKeys[index]:123 as AnyObject])
        }
    }
    
    
    
    
    //MARK: 8 - UnitView - 商品推荐
    class GoodsRecommandView:BaseGoodsDetailUnitView,LMHorizontalTableViewDelegate,LMHorizontalTableViewDataSource{
        
        let linsView:UIView = {
            let view = UIView()
            view.backgroundColor = Constant.Theme.Color15
            view.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: Constant.ScreenSizeV2.MARGIN_20)
            return view
        }()
        
        //label
        let goodsAttributeLabel:UILabel = {
            let lbl = UILabel()
            lbl.text = "买了又买"
            lbl.textAlignment = NSTextAlignment.center
            lbl.font = Constant.Theme.Font_15
            lbl.textColor = Constant.Theme.Color14
            lbl.frame = CGRect(x: 100, y: Constant.ScreenSizeV2.MARGIN_60, width: 3*Constant.ScreenSizeV2.MARGIN_60, height: Constant.ScreenSizeV2.MARGIN_20)
            
            return lbl
        }()
        
        //cell的高度
        fileprivate let cell_height:CGFloat = 316 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        //cell的宽度
        fileprivate let cell_width:CGFloat = 250 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        
        
        fileprivate var goodsAry = [GoodsRelationModel]()
        //横向TableView
        var tableView:LMHorizontalTableView!
        
        override func setupUI() {
            self.backgroundColor = UIColor.white
            self.iheight = 2 * Constant.ScreenSizeV2.MARGIN_49 + cell_height
            self.iwidth = Constant.ScreenSizeV2.SCREEN_WIDTH
            let widthScale:CGFloat = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
            self.addSubview(linsView)
            tableView = LMHorizontalTableView(frame: CGRect(x: 10,y: 120 * widthScale,width: Constant.ScreenSize.SCREEN_WIDTH - 20,height: cell_height))
            
            tableView.dataSource = self
            tableView.delegate = self
            
            goodsAttributeLabel.center.x = self.center.x
            self.addSubview(goodsAttributeLabel)
            self.addSubview(tableView)
        }
        
        override func reloadDataArr(_ dataModelArr: [GoodsRelationModel]) {
            if dataModelArr.count == 0 {
                self.iheight = 0
                goodsAttributeLabel.removeFromSuperview()
                linsView.removeFromSuperview()
            }else{
                goodsAry = dataModelArr
                self.tableView.reloadData()
            }

        }
        
        
        func numberOfRows(_ horizontalTableView: LMHorizontalTableView) -> Int {
            return goodsAry.count
        }
        
        
        func horizontalTableView(_ horizontalTableView: LMHorizontalTableView, cellForRow row: Int ,reuseCell:((_ reuseInfentifer:String) ->LMHorizontalTableViewCell?)) -> LMHorizontalTableViewCell {
            
            var cell:SubCell? = reuseCell("indetifer") as? SubCell
            
            if cell == nil{
                cell = SubCell(reuseIdentifier:"indetifer")
            }
            
            cell!.reloadData(goodsAry[row])
            
            return cell!
        }
        
        
        func horizontalTableView(_ horizontalTableView: LMHorizontalTableView, heightForRow row: Int) -> CGFloat{
            return cell_height
        }
        
        func horizontalTableView(_ horizontalTableView: LMHorizontalTableView, widthForRow row: Int) -> CGFloat{
            return cell_width
        }
        
        
        func horizontalTableView(_ horizontalTableView: LMHorizontalTableView, didSelectRow row: Int) {
            
            let index = GoodsDetailUnitViewActionType.recommondGoodsSelected.rawValue
            Statistics.count(Statistics.Goods.goods_detail_recommend_click, andAttributes: ["gid":String(goodsAry[row].gid)])
            self.delegate?.baseGoodsDetailUnitViewTouchdAction(GoodsDetailUnitViewActionType.recommondGoodsSelected, content: [ActionKeys[index]:goodsAry[row].gid as AnyObject])
        }
    }
    
    
    fileprivate class SubCell:LMHorizontalTableViewCell{
        
        fileprivate var itemView:CustItemView!
        
        override init(reuseIdentifier: String) {
            super.init(reuseIdentifier: reuseIdentifier)
            setupUI()
        }
        
        func reloadData(_ data:GoodsRelationModel){
            
            itemView.reloadData(data)
        }
        
        fileprivate func setupUI(){
            self.clipsToBounds = true
            itemView = CustItemView()
            self.addSubview(itemView)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    //MARK: - CustItemView
    class CustItemView: UIView {
        //商品图片
        let goodsImageView:UIImageView = {
            let imgView = UIImageView()
            imgView.image = UIImage(named: "Default_370_340")
            imgView.clipsToBounds = true
            //            imgView.contentMode = UIViewContentMode.Center
            
            return imgView
        }()
        
        //心意价
        let priceLabel:UILabel = {
            let label = UILabel()
            label.textColor = UIColor(rgba: "#EE5151")
            label.textAlignment = .right
            label.font = Constant.Theme.Font_11
            return label
        }()
        
        //市场价
        let marketPriceLabel:StrikeThroughLabel = {
            let label = StrikeThroughLabel()
            label.textColor = UIColor(rgba: Constant.common_C7_color)
            label.textAlignment = .left
            label.font = Constant.Theme.Font_10
            label.strikeThroughEnabled = true
            return label
        }()
        
        //商品名称
        let goodsLabel:UILabel = {
            let label = UILabel()
            label.textColor = UIColor(rgba: "#666666")
            label.textAlignment = .center
            label.font = Constant.Theme.Font_13
            return label
        }()
        
        var goods_url:String?
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            self.frame = CGRect.zero
            setUpUI()
        }
        
        
        func reloadData(_ data:GoodsRelationModel){
            if let url = URL(string: Utils.scaleImage((data.goods_image), width:goodsImageView.iwidth * Constant.ScreenSizeV2.SCALE_SCREEN, height: goodsImageView.iheight * Constant.ScreenSizeV2.SCALE_SCREEN)){
                self.goodsImageView.af_setImageWithURL(url, placeholderImage: UIImage(named: "Default_370_340"))
            }
//            if let event = data.
            
            self.priceLabel.text = "￥" + "\(data.discount_price)"
            if data.market_price == 0 {
                self.marketPriceLabel.removeFromSuperview()
                priceLabel.center.x = goodsImageView.center.x
            }else{
                self.marketPriceLabel.text = "\(data.price)"
            }
            
            self.goodsLabel.text = data.goods_name
            
        }
        func setUpUI() {
            
            
            for view in [goodsImageView,priceLabel,marketPriceLabel,goodsLabel] {
                self.addSubview(view)
                view.frame = CGRect.zero
            }
            
            goodsImageView.iwidth = 195 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
            goodsImageView.iheight = goodsImageView.iwidth
            
            priceLabel.iy = goodsImageView.ibottom + 10 * Constant.ScreenSize.SCREEN_WIDTH_SCALE
            priceLabel.iheight = 16 * Constant.ScreenSize.SCREEN_WIDTH_SCALE
            priceLabel.iwidth = goodsImageView.iwidth / 2 - 2
            
            marketPriceLabel.iy = priceLabel.iy
            marketPriceLabel.iheight = priceLabel.iheight
            marketPriceLabel.ileft = priceLabel.iright + 4
            marketPriceLabel.iwidth = priceLabel.iwidth
            
            goodsLabel.iy = marketPriceLabel.ibottom
            goodsLabel.iheight = 16 * Constant.ScreenSize.SCREEN_WIDTH_SCALE
            goodsLabel.iwidth = goodsImageView.iwidth
            
            self.iwidth = goodsImageView.iwidth
            self.iheight = goodsLabel.ibottom + 18 * Constant.ScreenSize.SCREEN_WIDTH_SCALE
        }
        
        required init(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
    }
    
    // MARK: 商品各种控件的基类
    class BaseGoodsDetailUnitView:UIView{
        
        
        var goodsModel:GoodsModel?
        
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            self.clipsToBounds = true
            self.setupUI()
        }
        
        
        func setupUI(){}
        
        weak var delegate:BaseGoodsDetailUnitViewDelegate?
        
        //刷新数据
        func reloadData(_ dataModel:GoodsModel){}
        
        func reloadDataArr(_ dataModelArr:[GoodsRelationModel]) {
            
        }
        
        func reloadDataArrOfSpce(_ dataModelArr:[GoodsSpceInfoModel]) {
            
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
    }
    
    
    //MARK: - 底部工具栏
    class ToolBar: UIView
    {
        var goodInfo: JSON?
            {
            didSet {
//                let outer = goodInfo?["Goods_outer"].int
                
                self.addSubview(separatLine)
                separatLine.snp_makeConstraints { (make) -> Void in
                    let _ = make.left.equalTo(self)
                    let _ = make.right.equalTo(self)
                    let _ = make.top.equalTo(self)
                    let _ = make.height.equalTo(0.5)
                }
                
                
//                if outer == 1
//                {//自营
//                    
                    if let status = goodInfo?["Status"].int
                    {
                        // 1正常 2预售中(已售罄) 3下架 4删除
                        if status == 3 || status == 4 {
                            statusLabel.text = "该商品已下架"
                            self.buyView.removeFromSuperview()
                            self.consultButton.removeFromSuperview()
                            self.favButton.removeFromSuperview()
                            self.addSubview(statusLabel)
                            statusLabel.textColor = UIColor(rgba: Constant.common_C7_color)
                            statusLabel.snp_makeConstraints(closure: { (make) -> Void in
                                let _ = make.left.equalTo(self).offset(MARGIN_8)
                                let _ = make.top.equalTo(self).offset(4)
                                let _ = make.bottom.equalTo(self).offset(4)
                                let _ = make.right.equalTo(self).offset(-MARGIN_8)
                            })
                            
                            
                        }else{
                            // 商品数量
                            let goodNum = goodInfo?["Goods_num"].int
                            // 商品数不为0正常显示，为0已售罄
                            if goodNum != 0 {
                                let collectionStatus = goodInfo?["Collect_status"].bool ?? false
                                let image = UIImage(named: collectionStatus ? "icon_liked" : "icon_like")
                                favButton.setImage(image, for: UIControlState())
                                
                                self.normalShowView()
                                
                            }else{
                                statusLabel.text = "该礼物已售罄"
                                self.buyView.removeFromSuperview()
                                self.consultButton.removeFromSuperview()
                                self.favButton.removeFromSuperview()
                                self.addSubview(statusLabel)
                                self.statusLabel.textColor = UIColor(rgba: Constant.common_C7_color)
                                statusLabel.snp_makeConstraints(closure: { (make) -> Void in
                                    let _ = make.centerX.equalTo(self)
                                    let _ = make.top.equalTo(self).offset(4)
                                    let _ = make.bottom.equalTo(self).offset(-4)
                                })
                                
                            }
                        }
                    }
                    
//                }
                
                /*
                else//第三方商店
                {
                    self.addSubview(favButton)
                    
                    self.addSubview(gobuyButton)
                    
                    let collectionStatus = goodInfo?["Collect_status"].bool ?? false
                    let image = UIImage(named: collectionStatus ? "goods_general_toolbar_heart_press" : "goods_general_toolbar_heart")
                    
                    favButton.setImage(image, forState: .Normal)
                    favButton.snp_makeConstraints(closure: { (make) -> Void in
                        make.top.equalTo(self).offset(4)
                        make.left.equalTo(self).offset(MARGIN_8)
                        make.bottom.equalTo(self).offset(-4)
                        make.width.equalTo(self).multipliedBy(0.33)
                    })
                    
                    
                    gobuyButton.setTitle((goodInfo?["Outside_platform"].string), forState: .Normal)
                    gobuyButton.snp_makeConstraints(closure: { (make) -> Void in
                        make.top.equalTo(favButton)
                        make.bottom.equalTo(favButton)
                        make.left.equalTo(favButton.snp_right).offset(4)
                        make.right.equalTo(self).offset(-MARGIN_8)
                    })
                    
                }
                */
                
            }
        }
        
        func normalShowView() {
            // 显示收藏、送自已、送TA   按钮
            self.addSubview(consultButton)
            self.addSubview(favButton)
            self.addSubview(buyView)
            
            consultButton.snp_makeConstraints(closure: { (make) in
                let _ = make.left.equalTo(self)
                let _ = make.centerY.equalTo(self).offset(-4 - 1 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
                let _ = make.width.equalTo(40)
            })
            
            favButton.snp_makeConstraints(closure: { (make) -> Void in
                let _ = make.left.equalTo(consultButton.snp_right)
                let _ = make.centerY.equalTo(self)
                let _ = make.width.equalTo(40)
            })
            
            buyView.snp_makeConstraints(closure: { (make) -> Void in
                let _ = make.top.equalTo(self)
                let _ = make.left.equalTo(favButton.snp_right).offset(4)
                let _ = make.bottom.equalTo(self)
                let _ = make.right.equalTo(self)
            })
            
            buyView.addSubview(addBasketButton)
            buyView.addSubview(buyNowButton)
            
            
            addBasketButton.snp_makeConstraints(closure: { (make) -> Void in
                let _ = make.top.equalTo(buyView)
                let _ = make.bottom.equalTo(buyView)
                let _ = make.left.equalTo(buyView)
                let _ = make.right.equalTo(buyView.snp_centerX)
            })
            
            buyNowButton.snp_makeConstraints(closure: { (make) -> Void in
                let _ = make.top.equalTo(buyView)
                let _ = make.bottom.equalTo(buyView)
                let _ = make.left.equalTo(buyView.snp_centerX)
                let _ = make.right.equalTo(buyView)
            })

        }
        
        // 四个按钮
        var consultButton = CustomConsultButton()
        var favButton:UIButton = {
            var btn = UIButton(type: UIButtonType.custom)
            btn.setTitle("喜欢", for: UIControlState())
            btn.titleLabel?.font = UIFont.systemFont(ofSize: Constant.common_F5_font)
            btn.setImage(UIImage(named: "icon_liked"), for: UIControlState())
            btn.setTitleColor(UIColor.black, for: UIControlState())
            btn.titleEdgeInsets = UIEdgeInsetsMake(0,-20, -20, 0)
            btn.imageEdgeInsets = UIEdgeInsetsMake(-15, (btn.titleLabel?.frame)!.maxX + 10, 0, 0)
            return btn
        }()
        var addBasketButton = UIButton()
        var buyNowButton = UIButton()
        var buyView = UIView()
        var gobuyButton = UIButton()
        
        var statusLabel = UILabel()
        var nav : UINavigationController = UINavigationController()
        var separatLine = UIView()
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            // 只初始化，不显示
            self.backgroundColor = UIColor.white
            
            separatLine.backgroundColor = UIColor(rgba: Constant.common_separatLine_color)
            
            addBasketButton.setTitle("加入礼物篮", for: UIControlState())
            addBasketButton.backgroundColor = UIColor(rgba: "#ff818a")
            addBasketButton.setTitleColor(UIColor.white, for: UIControlState())
//            addBasketButton.addTarget(self, action: #selector(ToolBar.changeBackColor2(_:)), for: UIControlEvents.touchDown)
            
            
            buyNowButton.setTitle("立即送", for: UIControlState())
            buyNowButton.backgroundColor = UIColor(rgba: Constant.common_C1_color)
            buyNowButton.setTitleColor(UIColor(rgba: Constant.common_C12_color), for: UIControlState())
            buyNowButton.addTarget(self, action: #selector(ToolBar.changeBackColor(_:)), for: UIControlEvents.touchDown)
            
            
            statusLabel.textColor = UIColor.black
            statusLabel.textAlignment = NSTextAlignment.center
            
            
            gobuyButton.backgroundColor = UIColor(rgba: Constant.common_C1_color)
            gobuyButton.setTitleColor(UIColor(rgba: Constant.common_C12_color), for: UIControlState())
            gobuyButton.addTarget(self, action: #selector(ToolBar.changeBackColor(_:)), for: UIControlEvents.touchDown)
            
            
            for label in [statusLabel, addBasketButton.titleLabel!, buyNowButton.titleLabel!] {
                label.font = UIFont.systemFont(ofSize: Constant.common_F2_font)
            }
            
            NotificationCenter.default.addObserver(self, selector: #selector(ToolBar.heartBtnClick(_:)), name: NSNotification.Name(rawValue: GoodsDetailViewController.COLLECT_INFO), object: nil)
        }
        
        func heartBtnClick(_ notification:Notification){
            
            if let data = notification.object as? [String:AnyObject]{
                
                let collect_status = data[GoodsDetailViewController.COLLECT_STATE] as? Int
                let collectionStatus = collect_status == 1
                let image = UIImage(named: collectionStatus ? "icon_liked" : "icon_like")
                favButton.setImage(image, for: UIControlState())
            
            }
        }
        
        deinit{
            NotificationCenter.default.removeObserver(self)
        }
        
        // 按钮点击 按钮背景颜色变化
        func changeBackColor2(_ sender: UIButton)
        {
            sender.backgroundColor = UIColor(rgba: Constant.common_C101_color)
        }
        
        func changeBackColor(_ sender: UIButton)
        {
            sender.backgroundColor = UIColor(rgba: Constant.common_C100_color)
        }
        
        
        required init(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    
    
    
    // MARK:  服务说明
    class ServicePromiseView: UIView {
        
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            setupUI()
            print(self.frame.size.height)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        //服务说明
        fileprivate let serviceLabel:UILabel = {
            let lbl = UILabel()
            lbl.text = "服务说明"
            lbl.textColor = Constant.Theme.Color14
            lbl.textAlignment = NSTextAlignment.center
            lbl.font = UIFont.boldSystemFont(ofSize: Constant.Theme.FontSize_17)
            //        lbl.frame = CGRectMake(0, Constant.ScreenSizeV2.MARGIN_60, Constant.ScreenSizeV2.SCREEN_WIDTH, Constant.ScreenSizeV2.MARGIN_30)
            return lbl
        }()
        
        //正品保证
        fileprivate let productAssuranceImageView:UIImageView = {
            let imageView = UIImageView()
            imageView.image = UIImage(named:"zhengpin_icon")
            //        imageView.frame = CGRectMake(Constant.ScreenSizeV2.MARGIN_30, 140 * SCREEN, Constant.ScreenSizeV2.MARGIN_49, Constant.ScreenSizeV2.MARGIN_49)
            return imageView
        }()
        fileprivate let productAssuranceLabel:UILabel = {
            let lbl = UILabel()
            lbl.text = "正品保证"
            lbl.textColor = Constant.Theme.Color14
            lbl.font = UIFont.boldSystemFont(ofSize: Constant.Theme.FontSize_15)
            //        lbl.frame = CGRectMake(130 * SCREEN, 140 * SCREEN, Constant.ScreenSizeV2.MARGIN_150, Constant.ScreenSizeV2.MARGIN_30)
            return lbl
        }()
        fileprivate let productAssuranceDetail:UILabel = {
            let lbl = UILabel()
            lbl.text = "心意点点所售礼物均为正品，通过正规渠道采购及定制，保证货品质量和价格优势。"
            lbl.textColor = Constant.Theme.Color7
            lbl.numberOfLines = 0
            lbl.font = Constant.Theme.Font_13
            return lbl
        }()
        
        //售后无忧
        fileprivate let afterSalesImageView:UIImageView = {
            let imageView = UIImageView()
            imageView.image = UIImage(named:"shouhou_icon")
            return imageView
        }()
        fileprivate let afterSalesLabel:UILabel = {
            let lbl = UILabel()
            lbl.text = "售后无忧"
            lbl.textColor = Constant.Theme.Color14
            lbl.font = UIFont.boldSystemFont(ofSize: Constant.Theme.FontSize_15)
            return lbl
        }()
        fileprivate let afterSalesDetail:UILabel = {
            let lbl = UILabel()
            lbl.text = "心意点点为您提供礼物售后服务，如您有任何疑问或需要，欢迎咨询客服热线021-68411255 或在线客服。"
            lbl.textColor = Constant.Theme.Color7
            lbl.numberOfLines = 0
            lbl.font = Constant.Theme.Font_13
            return lbl
        }()
        
        //定制礼盒
        fileprivate let giftBoxImageView:UIImageView = {
            let imageView = UIImageView()
            imageView.image = UIImage(named:"dingzhi_icon")
            return imageView
        }()
        fileprivate let giftBoxLabel:UILabel = {
            let lbl = UILabel()
            lbl.text = "定制礼盒"
            lbl.textColor = Constant.Theme.Color14
            lbl.font = UIFont.boldSystemFont(ofSize: Constant.Theme.FontSize_15)
            return lbl
        }()
        fileprivate let giftBoxDetail:UILabel = {
            let lbl = UILabel()
            lbl.text = "心意点点独有“心意服务”，提供定制礼盒包装、人工手写卡片、个性语音祝福，把礼物送得更有个性和心意，您可以在确认订单时进行选择。"
            lbl.textColor = Constant.Theme.Color7
            lbl.numberOfLines = 0
            lbl.font = Constant.Theme.Font_13
            return lbl
        }()
        
        //单件包邮
        fileprivate let freeMailPostImageView:UIImageView = {
            let imageView = UIImageView()
            imageView.image = UIImage(named:"baoyou_icon")
            return imageView
        }()
        fileprivate let freeMailPostLabel:UILabel = {
            let lbl = UILabel()
            lbl.text = "单件包邮"
            lbl.textColor = Constant.Theme.Color14
            lbl.font = UIFont.boldSystemFont(ofSize: Constant.Theme.FontSize_15)
            return lbl
        }()
        fileprivate let freeMailPostDetail:UILabel = {
            let lbl = UILabel()
            lbl.text = "心意点点所售礼物均为包邮商品，不额外收取快递费用，请放心购买。"
            lbl.textColor = Constant.Theme.Color7
            lbl.numberOfLines = 0
            lbl.font = Constant.Theme.Font_13
            return lbl
        }()
        
        
        //返回
        fileprivate let backButton:UIButton = {
            let btn = UIButton()
            btn.setTitle("返回", for: UIControlState())
            btn.titleLabel?.font = Constant.Theme.Font_21
            btn.titleLabel?.textColor = UIColor.white
            btn.backgroundColor = Constant.Theme.Color1
            btn.titleLabel?.textAlignment = NSTextAlignment.center
            
            return btn
        }()
        
        fileprivate var linesArr = [UIView]()
        
        
        //子控件的父视图
        fileprivate let subcontentView:UIView = {
            let view = UIView()
            view.backgroundColor = UIColor.white
            return view
        }()
        
        fileprivate func setupUI(){
            
            for _ in 1...3 {
                let lineView = UIView()
                lineView.backgroundColor = Constant.Theme.Color9
                subcontentView.addSubview(lineView)
                self.linesArr.append(lineView)
            }
            
            self.addSubview(subcontentView)
            subcontentView.snp_makeConstraints { (make) in
                let _ = make.bottom.equalTo(self)
                let _ = make.left.equalTo(self)
                let _ = make.right.equalTo(self)
            }
            
            let _ = [serviceLabel, productAssuranceLabel, productAssuranceDetail, productAssuranceImageView, afterSalesLabel, afterSalesDetail, afterSalesImageView, giftBoxLabel, giftBoxDetail, giftBoxImageView, freeMailPostLabel, freeMailPostDetail, freeMailPostImageView].map{subcontentView.addSubview($0)}
            backButton.addTarget(self, action: #selector(ServicePromiseView.onClick), for: .touchUpInside)
            subcontentView.addSubview(backButton)
            
            composition()
        }
        
        func onClick(){
            NotificationCenter.default.post(name: Notification.Name(rawValue: "datepicker_doneBtnClick"), object: self, userInfo: nil)
        }
        //布局
        func composition(){
            
            serviceLabel.snp_makeConstraints { (make) in
                let _ = make.top.equalTo(subcontentView).offset(Constant.ScreenSizeV2.MARGIN_60)
                let _ = make.left.equalTo(subcontentView)
                let _ = make.right.equalTo(subcontentView)
                let _ = make.centerX.equalTo(subcontentView)
            }
            
            productAssuranceImageView.snp_makeConstraints { (make) in
                let _ = make.top.equalTo(serviceLabel.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_49)
                let _ = make.width.equalTo(Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * 47)
                let _ = make.height.equalTo(Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * 47)
                let _ = make.left.equalTo(subcontentView.snp_left).offset(Constant.ScreenSizeV2.MARGIN_30)
            }
            productAssuranceLabel.snp_makeConstraints { (make) in
                let _ = make.left.equalTo(productAssuranceImageView.snp_right).offset(Constant.ScreenSizeV2.MARGIN_10)
//                make.top.equalTo(productAssuranceImageView.snp_top)
                let _ = make.centerY.equalTo(productAssuranceImageView)
            }
            productAssuranceDetail.snp_makeConstraints { (make) in
                let _ = make.top.equalTo(productAssuranceImageView.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_15)
                let _ = make.left.equalTo(productAssuranceLabel.snp_left)
                let _ = make.right.equalTo(subcontentView).offset(-Constant.ScreenSizeV2.MARGIN_30)
            }
            linesArr[0].snp_makeConstraints { (make) in
                let _ = make.top.equalTo(productAssuranceDetail.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_30)
                let _ = make.left.equalTo(productAssuranceImageView.snp_left)
                let _ = make.height.equalTo(0.5)
                let _ = make.width.equalTo(Constant.ScreenSizeV2.SCREEN_WIDTH - 2 * Constant.ScreenSizeV2.MARGIN_30)
            }
            afterSalesImageView.snp_makeConstraints { (make) in
                let _ = make.width.equalTo(Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * 47)
                let _ = make.height.equalTo(Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * 47)
                let _ = make.top.equalTo(linesArr[0].snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_30)
                let _ = make.left.equalTo(subcontentView.snp_left).offset(Constant.ScreenSizeV2.MARGIN_30)
            }
            afterSalesLabel.snp_makeConstraints { (make) in
                let _ = make.left.equalTo(afterSalesImageView.snp_right).offset(Constant.ScreenSizeV2.MARGIN_10)
//                make.top.equalTo(afterSalesImageView.snp_top)
                let _ = make.centerY.equalTo(afterSalesImageView)
            }
            afterSalesDetail.snp_makeConstraints { (make) in
                let _ = make.top.equalTo(afterSalesImageView.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_15)
                let _ = make.left.equalTo(afterSalesLabel.snp_left)
                let _ = make.right.equalTo(subcontentView).offset(-Constant.ScreenSizeV2.MARGIN_30)
            }
            linesArr[1].snp_makeConstraints { (make) in
                let _ = make.top.equalTo(afterSalesDetail.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_30)
                let _ = make.left.equalTo(productAssuranceImageView.snp_left)
                let _ = make.height.equalTo(0.5)
                let _ = make.width.equalTo(Constant.ScreenSizeV2.SCREEN_WIDTH - 2 * Constant.ScreenSizeV2.MARGIN_30)
            }
            giftBoxImageView.snp_makeConstraints { (make) in
                let _ = make.width.equalTo(Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * 47)
                let _ = make.height.equalTo(Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * 47)
                let _ = make.top.equalTo(linesArr[1].snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_30)
                let _ = make.left.equalTo(subcontentView.snp_left).offset(Constant.ScreenSizeV2.MARGIN_30)
            }
            giftBoxLabel.snp_makeConstraints { (make) in
                let _ = make.left.equalTo(giftBoxImageView.snp_right).offset(Constant.ScreenSizeV2.MARGIN_10)
//                make.top.equalTo(giftBoxImageView.snp_top)
                let _ = make.centerY.equalTo(giftBoxImageView)
            }
            giftBoxDetail.snp_makeConstraints { (make) in
                let _ = make.top.equalTo(giftBoxImageView.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_15)
                let _ = make.left.equalTo(giftBoxLabel.snp_left)
                let _ = make.right.equalTo(subcontentView).offset(-Constant.ScreenSizeV2.MARGIN_30)
            }
            linesArr[2].snp_makeConstraints { (make) in
                let _ = make.top.equalTo(giftBoxDetail.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_30)
                let _ = make.left.equalTo(productAssuranceImageView.snp_left)
                let _ = make.height.equalTo(0.5)
                let _ = make.width.equalTo(Constant.ScreenSizeV2.SCREEN_WIDTH - 2 * Constant.ScreenSizeV2.MARGIN_30)
            }
            freeMailPostImageView.snp_makeConstraints { (make) in
                let _ = make.width.equalTo(Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * 47)
                let _ = make.height.equalTo(Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * 47)
                let _ = make.top.equalTo(linesArr[2].snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_30)
                let _ = make.left.equalTo(subcontentView.snp_left).offset(Constant.ScreenSizeV2.MARGIN_30)
            }
            freeMailPostLabel.snp_makeConstraints { (make) in
                let _ = make.left.equalTo(freeMailPostImageView.snp_right).offset(Constant.ScreenSizeV2.MARGIN_10)
//                make.top.equalTo(freeMailPostImageView.snp_top)
                let _ = make.centerY.equalTo(freeMailPostImageView)
            }
            freeMailPostDetail.snp_makeConstraints { (make) in
                let _ = make.top.equalTo(freeMailPostImageView.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_15)
                let _ = make.left.equalTo(freeMailPostLabel.snp_left)
                let _ = make.right.equalTo(subcontentView).offset(-Constant.ScreenSizeV2.MARGIN_30)
            }
            
            backButton.snp_makeConstraints { (make) in
                let _ = make.top.equalTo(freeMailPostDetail.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_30)
                let _ = make.height.equalTo(100 * SCREEN)
                let _ = make.left.equalTo(subcontentView)
                let _ = make.right.equalTo(subcontentView)
                let _ = make.bottom.equalTo(subcontentView)
            }
        }
    }
    
    
    
    // MARK: 商品参数说明
    class GoodsAttributeDateilView: UIView {
        fileprivate var
        contentTitlt:[String] = []
        fileprivate var contentDetail:[String] = []
        
        init(frame: CGRect, dataModelAry:[GoodsSpceInfoModel]) {
            super.init(frame: frame)
            for model in dataModelAry {
                contentTitlt.append(model.name)
                contentDetail.append(model.value)
            }
            setupUI()
            //
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        
        fileprivate let giftParameter:UILabel = {
            let lbl = UILabel()
            lbl.text = "礼品参数"
            lbl.textColor = Constant.Theme.Color14
            lbl.textAlignment = NSTextAlignment.center
            lbl.font = UIFont.boldSystemFont(ofSize: Constant.Theme.FontSize_17)
            return lbl
        }()
        
        //返回
        fileprivate let backLabel:UILabel = {
            let lbl = UILabel()
            lbl.text = "返回"
            lbl.font = Constant.Theme.Font_21
            lbl.textColor = UIColor.white
            lbl.backgroundColor = Constant.Theme.Color1
            lbl.textAlignment = NSTextAlignment.center
            return lbl
        }()
        
        //子控件的父视图
        fileprivate let subcontentView:UIView = {
            let view = UIView()
            view.backgroundColor = UIColor.white
            return view
        }()
        
        
        fileprivate var labelTitleArr = [UILabel]()
        fileprivate var labelDetailArr = [UILabel]()
        fileprivate var linesArr = [UIView]()

        
        func setupUI() {
            
            self.addSubview(subcontentView)
            subcontentView.snp_makeConstraints { (make) in
                let _ = make.bottom.equalTo(self)
                let _ = make.left.equalTo(self)
                let _ = make.right.equalTo(self)
            }
            
            subcontentView.addSubview(giftParameter)
            subcontentView.addSubview(backLabel)
            for i in 0...(contentDetail.count - 1) {
                let lbl = UILabel()
                lbl.text = contentTitlt[i]
                lbl.textColor = Constant.Theme.Color14
                lbl.font = UIFont.boldSystemFont(ofSize: Constant.Theme.FontSize_15)
                subcontentView.addSubview(lbl)
                labelTitleArr.append(lbl)
                
                let lineView = UIView()
                lineView.backgroundColor = Constant.Theme.Color9
                subcontentView.addSubview(lineView)
                self.linesArr.append(lineView)
                
                let label = UILabel()
                label.text = contentDetail[i]
                label.textColor = Constant.Theme.Color7
                label.numberOfLines = 0
//                label.textAlignment = NSTextAlignment.Left
                label.font = Constant.Theme.Font_13
                subcontentView.addSubview(label)
                labelDetailArr.append(label)
            }
            
            composition()
        }
        
        //布局
        func composition() {
            
            let TITLE_WIDTH:CGFloat = 100 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
            
            
            giftParameter.snp_makeConstraints { (make) in
                let _ = make.top.equalTo(subcontentView.snp_top).offset(Constant.ScreenSizeV2.MARGIN_60)
                let _ = make.centerX.equalTo(subcontentView.snp_centerX)
            }
            self.labelTitleArr[0].snp_makeConstraints { (make) in
                let _ = make.width.equalTo(TITLE_WIDTH)
                let _ = make.top.equalTo(giftParameter.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_60)
                let _ = make.left.equalTo(subcontentView.snp_left).offset(Constant.ScreenSizeV2.MARGIN_30)
            }
            
            self.labelDetailArr[0].snp_makeConstraints { (make) in
                let _ = make.top.equalTo(labelTitleArr[0].snp_top)
                let _ = make.left.equalTo(labelTitleArr[0].snp_right).offset(Constant.ScreenSizeV2.MARGIN_30)
                let _ = make.right.lessThanOrEqualTo(subcontentView.snp_right).offset(-Constant.ScreenSizeV2.MARGIN_30)
            }
            self.linesArr[0].snp_makeConstraints { (make) in
                let _ = make.top.equalTo(labelDetailArr[0].snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_30)
                let _ = make.left.equalTo(subcontentView.snp_left).offset(Constant.ScreenSizeV2.MARGIN_30)
                let _ = make.right.equalTo(subcontentView.snp_right).offset(-Constant.ScreenSizeV2.MARGIN_30)
                let _ = make.height.equalTo(0.5)
            }
            
            if contentDetail.count == 1 {
                
            }else{
                for i in 1...(contentDetail.count - 1) {
                    self.labelTitleArr[i].snp_makeConstraints { (make) in
                        let _ = make.width.equalTo(TITLE_WIDTH)
                        let _ = make.top.equalTo(linesArr[i - 1].snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_30)
                        let _ = make.left.equalTo(subcontentView.snp_left).offset(Constant.ScreenSizeV2.MARGIN_30)
                    }
                    self.labelDetailArr[i].snp_makeConstraints { (make) in
                        let _ = make.top.equalTo(labelTitleArr[i].snp_top)
                        let _ = make.left.equalTo(labelDetailArr[0].snp_left)
                        let _ = make.right.lessThanOrEqualTo(subcontentView.snp_right).offset(-Constant.ScreenSizeV2.MARGIN_30)
                    }
                    self.linesArr[i].snp_makeConstraints { (make) in
                        let _ = make.top.equalTo(labelDetailArr[i].snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_30)
                        let _ = make.left.equalTo(subcontentView.snp_left).offset(Constant.ScreenSizeV2.MARGIN_30)
                        let _ = make.right.equalTo(subcontentView.snp_right).offset(-Constant.ScreenSizeV2.MARGIN_30)
                        let _ = make.height.equalTo(0.5)
                    }
                }

            }
            
                backLabel.snp_makeConstraints { (make) in
                    let _ = make.top.equalTo(self.linesArr[contentDetail.count - 1].snp_bottom).offset(95 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
                    let _ = make.height.equalTo(100 * SCREEN)
                    let _ = make.left.equalTo(subcontentView)
                    let _ = make.right.equalTo(subcontentView)
                    let _ = make.bottom.equalTo(subcontentView)
            }
            
        }
        
    }
    
    //MARK: 图文详情
    class DetailInfoView: BaseGoodsDetailUnitView {
        
        lazy fileprivate var titleLabel:UILabel = {
           let lbl = UILabel()
            lbl.font = UIFont.boldSystemFont(ofSize: Constant.Theme.FontSize_16)
            lbl.textColor = Constant.Theme.Color14
            lbl.textAlignment = .center
            return lbl
        }()
        
        lazy fileprivate var titleLineView:UIView = {
            let view = UIView()
            view.backgroundColor = UIColor(rgba: "#ff818a")
            view.alpha = 0.5
            return view
        }()
        
        fileprivate let contentView:UIView = {
            let view = UIView()
            view.backgroundColor = UIColor.white
            return view
        }()
        
        override func setupUI() {
            self.clipsToBounds = true
            let scale = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
            self.titleLabel.text = "图文描述"
            self.addSubview(contentView)
            contentView.addSubview(titleLabel)
            contentView.addSubview(titleLineView)
            contentView.frame = CGRect(x: 0, y: 20 * scale, width: Constant.ScreenSizeV2.SCREEN_WIDTH , height: 200)
            titleLabel.frame = CGRect(x: 30 * scale, y: 48 * scale, width: Constant.ScreenSizeV2.SCREEN_WIDTH - 60 * scale, height: 14 + Constant.ScreenSizeV2.MARGIN_4)
            titleLineView.frame = CGRect(x: 0, y: titleLabel.ibottom + 27 * scale, width: 140 * scale, height: 4 * scale)
            titleLineView.icenterX = Constant.ScreenSizeV2.SCREEN_WIDTH / 2
            self.iheight = 0
        }
        
        override func reloadData(_ dataModel: GoodsModel) {
           let scale = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
            let viewHeight = titleLineView.ibottom + 27 * scale
            if let temp = dataModel.detailInfo {
                if temp.count > 0 {
                    let detailDict = temp[0]
                    if let imageArray = detailDict.detail {
                        if imageArray.count > 0 {
                            createImageView(viewHeight: viewHeight, imageArray: imageArray,index:0)
                        }
                    }
                }
            }
        }
        
        func createImageView(viewHeight:CGFloat,imageArray:[String],index:Int) {
            var tempHeight = viewHeight
            var tempIndex = index
            let imageView = UIImageView()
            imageView.contentMode = UIViewContentMode.scaleAspectFill
            self.addSubview(imageView)
            
            OriginalDownLoad().downloadWithUrlString(context: self,shouldShowHud: false, urlString: imageArray[index], completionHandle: { [weak self](data) in
                if let temp = data {
                    let tempImage = UIImage(data: temp)
                    imageView.image = tempImage
                        imageView.frame = CGRect(x: 0, y: tempHeight, width: Constant.ScreenSizeV2.SCREEN_WIDTH , height: Constant.ScreenSizeV2.SCREEN_WIDTH / (tempImage?.size.width)! * (tempImage?.size.height)!)
                   tempHeight += imageView.iheight
                    self?.contentView.iheight = tempHeight
                  self?.iheight = (self?.contentView.ibottom)!
                    tempIndex += 1
                    if tempIndex < imageArray.count {
                        self?.createImageView(viewHeight: tempHeight, imageArray: imageArray, index: tempIndex)
                    }
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "detailInfoViewHeight"), object: nil)
                }
          })
        }
    }
    
}



// MARK: 分享
extension GoodsDetailViewController{
    

    // 事件处理
    func share() {
       let id:Int = self.goodsModel.gid
        let id_web  = String(id)
        
        Statistics.count(Statistics.Goods.goods_detail_share_click, andAttributes: ["gid":id_web])
        
        let Objs : NSArray = [MenuLabel.createlabelIconName("share_icon_wechat_friend", title: "微信好友"),MenuLabel.createlabelIconName("share_icon_wechat_timeline", title: "微信朋友圈"),MenuLabel.createlabelIconName("share_icon_qq_friend", title: "QQ好友"),MenuLabel.createlabelIconName("share_icon_qq_zone", title: "QQ空间")]
        HyPopMenuView.creatingPopMenuObjectItmes(Objs as! [MenuLabel],isGeneral:true, topView: nil, selectdCompletionBlock: {index  in
            if index == 0{
                self.shareToWx(Int32(WXSceneSession.rawValue))
            }
            else if index == 1{
                self.shareToWx(Int32(WXSceneTimeline.rawValue))
            }
            else if index == 2{
                self.shareToQQ(true)
            }
            else if index == 3{
                self.shareToQQ(false)
            }
            
            }
        )
        
    }
    
    
    
    func shareToWx(_ wxScene:Int32){
        let id:Int = self.goodsModel.gid
        let id_web  = String(id)
        let link_web = "http://wechat.giftyou.me/goods_item?id=" + id_web
        if let imgNames = self.goodsModel.goods_images
        {
            let imgName = imgNames[0].image
            OriginalDownLoad().downloadWithUrlString(context:self.view,urlString: imgName + "?imageView2/0/w/100/h/100", completionHandle: { (data) -> Void in
                if let mdata = data
                {
                    let avatarImage = UIImage(data: mdata as Data)
                    let shareDict:NSDictionary = [
                        LDSDKShareContentTitleKey : self.goodsModel.goods_name,   LDSDKShareContentDescriptionKey : self.goodsModel.gift_story, LDSDKShareContentWapUrlKey : link_web,LDSDKShareContentImageKey : avatarImage!]
                    let wechatShare : LDSDKWXServiceImpl = LDSDKManager.getShareService(LDSDKPlatformType.weChat) as! LDSDKWXServiceImpl
                    
                    wechatShare.share(withContent: shareDict as! [AnyHashable: Any], shareModule:wxScene == Int32(WXSceneSession.rawValue) ? 1 : 2, onComplete: ({(success,error) in
                        if error != nil{
                            Utils.showError(context: self.view, errorStr: (error?.localizedDescription)!)
                        }
                        
                    }))
                }
                
            })
        }
        
    }
    
    func shareToQQ(_ isShareToQQ : Bool){
        let id:Int = self.goodsModel.gid
        let id_web  = String(id)
        let link_web = "http://wechat.giftyou.me/goods_item?id=" + id_web
        let utf8String = link_web
        let title = self.goodsModel.goods_name
        let description = self.goodsModel.gift_story
        if let previewImageUrls = self.goodsModel.goods_images {
            let previewImageUrl = previewImageUrls[0].image
            
            let shareDict:NSDictionary = [
                LDSDKShareContentTitleKey : title,   LDSDKShareContentDescriptionKey : description,LDSDKShareContentWapUrlKey : utf8String,LDSDKShareContentImageUrlKey : previewImageUrl]
            let qqShare : LDSDKQQServiceImpl = LDSDKManager.getShareService(LDSDKPlatformType.QQ) as! LDSDKQQServiceImpl
            
            qqShare.share(withContent: shareDict as! [AnyHashable: Any], shareModule:isShareToQQ == true ? 1 : 2, onComplete: ({(success,error) in
                if error != nil{
                    Utils.showError(context: self.view, errorStr: (error?.localizedDescription)!)
                }
                
            }))

        }
        
        
    }

}

protocol BaseGoodsDetailUnitViewDelegate:NSObjectProtocol {
    func baseGoodsDetailUnitViewTouchdAction(_ actionType:GoodsDetailUnitViewActionType,content:[String:AnyObject]?)
}


class TimeView: UIView {
    var hourLabel = UILabel()
    var minuteLabel = UILabel()
    var secondLabel = UILabel()
    
    fileprivate let leftColonLabel = UILabel()
    fileprivate let middleColonLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setUpUI()
    }
    
    func setUpUI() {
        let widthScale:CGFloat = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        for view in [hourLabel,leftColonLabel,minuteLabel,middleColonLabel,secondLabel] {
            self.addSubview(view)
            view.frame = CGRect(x: 0, y: 0, width: 48 * widthScale, height: 48 * widthScale)
        }
        
        timeLabelAttribute(hourLabel)
        timeLabelAttribute(minuteLabel)
        timeLabelAttribute(secondLabel)
        
        colonLabelAttribute(leftColonLabel)
        colonLabelAttribute(middleColonLabel)
        
        leftColonLabel.ileft = hourLabel.iright
        leftColonLabel.iwidth = MARGIN_13
        
        minuteLabel.ileft = leftColonLabel.iright
        
        middleColonLabel.ileft = minuteLabel.iright
        middleColonLabel.iwidth = MARGIN_13
        
        secondLabel.ileft = middleColonLabel.iright
        secondLabel.backgroundColor = UIColor(rgba: Constant.common_C1_color)
        
        self.iwidth = secondLabel.iright
        self.iheight = 48 * widthScale
        
    }
    
    func timeLabelAttribute(_ timeLabel:UILabel) {
        timeLabel.textColor = UIColor.white
        timeLabel.font = UIFont.systemFont(ofSize: 14)
        timeLabel.layer.cornerRadius = 4
        timeLabel.layer.masksToBounds = true
        timeLabel.textAlignment = .center
        timeLabel.backgroundColor = UIColor(rgba: "#333333")
        timeLabel.text = "00"
    }
    
    func colonLabelAttribute(_ colonLabel:UILabel) {
        colonLabel.textColor = UIColor(rgba: "#333333")
        colonLabel.font = UIFont.systemFont(ofSize: 14)
        colonLabel.textAlignment = .center
        colonLabel.text = ":"
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
