//
//  GoodsDetailViewController.swift
//  Limi
//
//  Created by 千云锋 on 16/7/18.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
import UIKit
import CoreData
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


/**
 商品详情主页面
 */

enum GoodsDetailUnitViewActionType:NSInteger {
    //商品图片点击
    case goodsPictureSelected = 0
    //限时抢购
    case limitSale
    
    //商品价格
    case goodsPriceInfo

    //商品特权
    case goodsFeature
    //推荐商品被选中
    case recommondGoodsSelected
}


let ActionKeys = ["GoodsPictureSelected","LimitSale", "GoodsPriceInfo", "GoodsFeature","RecommondGoodsSelected"]


class GoodsDetailViewController: BaseViewController,LoginDialogDelegate{
    
    
    static let COLLECT_NUM = "collect_num"
    static let COLLECT_INFO = "collect_info"
    static let COLLECT_STATE = "collect_state"
    
    
    enum TriggerType :Int{
        case collectType = 1
        case notiMeType
        case getNowType
    }
    
    fileprivate let goodsDetailHeadView = GoodsDetailHeadView()
    
    fileprivate let scrollView = UIScrollView()
    
    fileprivate var tempBannerView = UIView()

    fileprivate let toolBar = ToolBar()
    
    var goodsModel:GoodsModel!{return _goodsModel}
    
    fileprivate var _goodsModel:GoodsModel!
    
    fileprivate let basketButton = ShoppingBasketBadgeIcon()
    
    fileprivate var QYFbackButton:UIButton = {
        var btn = UIButton()
        btn.setImage(UIImage(named: "common_back_icon"), for: UIControlState.normal)
        
        return btn
    }()
    
    fileprivate var QYFshareButton:UIButton = {
        var btn = UIButton()
        btn.setImage(UIImage(named: "common_share_icon"), for: UIControlState.normal)
        
        return btn
    }()
    
    
    //假导航
    fileprivate var selfNavigationBar:UIView = {
        var view = UIView()
        view.backgroundColor = UIColor.white
        view.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: 64)
        return view
    }()
    
    //readOnly
    var goods_ID:Int{return _goods_ID}
    
    
    fileprivate lazy var basket:ShoppingBasketViewController = {
        let  basketVC = ShoppingBasketViewController()
        basketVC.hidesBottomBarWhenPushed = true
        return basketVC
    }()
    
    ///返回顶部按钮
    let scrollToTopButton:UIButton = {
        let button = UIButton(type: .custom)
        let space_X = Constant.ScreenSizeV2.SCREEN_WIDTH - 54
        let space_Y = Constant.ScreenSizeV2.SCREEN_HEIGHT / 2 * 1.8 - 52
        button.frame = CGRect(x: space_X, y: space_Y, width: 44, height: 44)
        button.setImage(UIImage(named: "goods_detail_float_button"), for: UIControlState())
        button.isHidden = true
        return button
    }()
    
    //writeOnly
    fileprivate var _goods_ID = 0
    
    fileprivate var isCollect = false
    
    var triggerType : Int = 0  //触发类型
    fileprivate let loginDialog = LoginDialog()
    
    class func newGoodsDetailWithID(_ goodsID:Int) ->GoodsDetailViewController{
        
        let goodsDetailVC =  GoodsDetailViewController()
        goodsDetailVC._goods_ID = goodsID
        return goodsDetailVC
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if _goods_ID != 0 {
            Statistics.count(Statistics.Goods.goods_inside_detail_click, andAttributes: ["aid":String(_goods_ID)])
            Statistics.count(Statistics.Goods.goods_detail_click, andAttributes: ["aid":String(_goods_ID)])
        }
        
        self.view.addSubview(scrollView)
        scrollView.delegate = self
        scrollView.frame = self.view.bounds
        scrollView.iheight -= 119 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        scrollView.contentH = self.view.iheight
        scrollToTopButton.addTarget(self, action: #selector(GoodsDetailViewController.scrollToTop), for: .touchUpInside)
        self.view.addSubview(scrollToTopButton)
        
        prepareData()
        fetchDataSource()
        
        self.countGoodsScanning()
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "detailInfoViewHeight"), object: nil, queue: OperationQueue.main) { (notification) -> Void in
            self.goodsDetailHeadView.refreshUI()
            self.scrollView.contentH = self.goodsDetailHeadView.iheight
            
        }
        
        loginDialog.delegate = self
    }
    
    func wechatLoginSuccess() {
        fetchDataSource()
    }
    
    /**返回顶部动作*/
    func scrollToTop(){
        
        self.scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
//        self.scrollView.scrollRectToVisible(CGRect(x: 0, y: 0, width: 1, height: 1), animated: true)
    }
    
    func countGoodsScanning() {
        var countArray = [[String: AnyObject]]()
        if let temp = UserDefaults.standard.array(forKey: "goods_scanning") {
            countArray = temp as! [[String : AnyObject]]
        }
        var tempDict = [String: AnyObject]()
        let tempCome_from = UserDefaults.standard.integer(forKey: "come_from")
        
        let zone : TimeZone = TimeZone.current
        let intervalCurrent : Int = zone.secondsFromGMT(for: Date())
        let nowDate = (Date().addingTimeInterval(TimeInterval(intervalCurrent)))
        let timeInterval: Int = Int(nowDate.timeIntervalSince1970)

        var userInfomation: User? = nil
        let requestUserInfo: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        let datasourceUserInfo : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(requestUserInfo)!
        if datasourceUserInfo.count > 0{
            
            userInfomation = datasourceUserInfo[0] as? User
        }
        if  let mobilePhone = userInfomation?.user_name{
             tempDict = ["mobile":mobilePhone as AnyObject,"channel_id":1 as AnyObject,"item_id":goods_ID as AnyObject,"view_time":timeInterval as AnyObject,"come_from":tempCome_from as AnyObject]
        }else {
             tempDict = ["mobile":"" as AnyObject,"channel_id":1 as AnyObject,"item_id":goods_ID as AnyObject,"view_time":timeInterval as AnyObject,"come_from":tempCome_from as AnyObject]
        }
    
        countArray.append(tempDict)
        var goods_rule = 3
        if let temp = UserDefaults.standard.object(forKey: "goods_rule") {
            goods_rule = temp as! Int
        }
       
        var shoudSubmit = false
        if goods_rule == 1 {
            //每条上传
            shoudSubmit = true
        }else if goods_rule == 2 {
            //每5条上传
            shoudSubmit = countArray.count < 5 ? false : true
        }else if goods_rule == 3 {
            //每10条上传
            shoudSubmit = countArray.count < 10 ? false : true
        }else {
            //打开APP上传
            shoudSubmit = false
        }
        
        if shoudSubmit {
            var params = [String:AnyObject]()
            params["type"] = 1 as AnyObject?
            params["requetst_frequency"] = goods_rule as AnyObject?
            
            let jsonData = try! JSONSerialization.data(withJSONObject: countArray, options: JSONSerialization.WritingOptions.prettyPrinted)
            let jsonString: String = String(data: jsonData, encoding: String.Encoding.utf8)!
            params["request_json"] = jsonString as AnyObject
            let _ = LMRequestManager.requestByDataAnalyses(context:self.view, URLString: Constant.JTAPI.submitScanning,parameters: params,isNeedUserTokrn :true,successHandler: {data, status, msg in
                if status != 200 {return}
                if let tempGoodsRule = JSON(data as AnyObject)["data"]["request_rule"].arrayObject {
                    if tempGoodsRule.count >= 2 {
                        UserDefaults.standard.set(tempGoodsRule[0].object(forKey: "requetst_frequency"), forKey: "goods_rule")
                        UserDefaults.standard.set(tempGoodsRule[1].object(forKey: "requetst_frequency"), forKey: "article_rule")
                        UserDefaults.standard.synchronize()
                    }
                    UserDefaults.standard.removeObject(forKey: "goods_scanning")
                    
                }
            })
        }else {
            //添加到数据库
            UserDefaults.standard.set(countArray, forKey: "goods_scanning")
            UserDefaults.standard.synchronize()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.lt_reset()
        self.navigationController?.navigationBar.lt_setElementsAlpha(1)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        //显示navigationBar最下面的线
//        showBottomHairline()
        updateNaviBarColor()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.navigationBarStyle = 1
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.changeTitleColor(self.scrollView)
        //隐藏navigationBar最下面的线
//        hideBottomHairline()
        self.navigationItem.title = ""
    }

    func goToBasket() {
        Statistics.count(Statistics.Goods.goods_detail_basket_click, andAttributes: ["did":"\(self.goods_ID)"])
        self.navigationController?.pushViewController(self.basket, animated: true)
    }
    
    fileprivate func prepareData(){
        
        self.view.addSubview(toolBar)
        
        self.scrollToTopButton.setImage(UIImage(named: "goods_detail_btn_top"), for: UIControlState())
        
        if self.goods_ID == 0{
            
            //关闭手势返回
            self.gesturBackEnable = false
            if let params : NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: eventURL?.query) as NSDictionary?{

                if let id  = params.object(forKey: "id") as? String{
                   _goods_ID = (id as NSString).integerValue
//                    _goods_ID = Int(id)!
                    Statistics.count(Statistics.Goods.goods_inside_detail_click, andAttributes: ["aid":String(_goods_ID)])
                     Statistics.count(Statistics.Goods.goods_detail_click, andAttributes: ["aid":String(_goods_ID)])
                }
            }
        }
        
        
        goodsDetailHeadView.delegate = self
       self.scrollView.addSubview(goodsDetailHeadView)
        
        toolBar.snp_makeConstraints { (make) in
            let _ = make.bottom.equalTo(self.view)
            let _ = make.left.equalTo(self.view)
            let _ = make.right.equalTo(self.view)
            let _ = make.height.equalTo(119 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
        }
        
        self.view.addSubview(tempBannerView)
        tempBannerView.isUserInteractionEnabled = false
        
        self.selfNavigationBar.alpha = scrollView.offsetY / (Constant.ScreenSizeV2.SCREEN_WIDTH - 64)
        self.view.addSubview(selfNavigationBar)
        
        
        self.view.addSubview(QYFbackButton)
        QYFbackButton.addTarget(self, action: #selector(BaseViewController.backItemAction), for: UIControlEvents.touchUpInside)
        QYFbackButton.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(self.view).offset(26)
            let _ = make.left.equalTo(self.view).offset(17)
            let _ = make.width.equalTo(36)
            let _ = make.height.equalTo(36)
            
        }
        
        self.view.addSubview(QYFshareButton)
        QYFshareButton.addTarget(self, action: #selector(GoodsDetailViewController.share), for: UIControlEvents.touchUpInside)
        QYFshareButton.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(self.view).offset(26)
            let _ = make.right.equalTo(self.view).offset(-15)
            let _ = make.width.equalTo(36)
            let _ = make.height.equalTo(36)
        }
        
        self.view.addSubview(basketButton)
        basketButton.addTarget(self, action: #selector(GoodsDetailViewController.goToBasket), for: UIControlEvents.touchUpInside)
        basketButton.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(self.view).offset(26)
            let _ = make.right.equalTo(self.QYFshareButton.snp_left).offset(-24)
            let _ = make.width.equalTo(36)
            let _ = make.height.equalTo(36)
        }

        if let temp = self.navigationController {
            self.toolBar.nav = temp
        }
        
//        toolBar.alpha = 0
        
        self.toolBar.gobuyButton.addTarget(self, action: #selector(GoodsDetailViewController.gobuyButtonAction(_:)), for: UIControlEvents.touchUpInside)
        // 喜欢
        let _ = self.toolBar.favButton.handle(events: .touchUpInside, withBlock: {[weak self] (sender, event) -> Void in
            self?.collectGoods(sender)
            })
        
        let _ = self.toolBar.consultButton.handle(events: .touchUpInside, withBlock: {[weak self] (sender,event) -> Void in
            if UserInfoManager.didLogin {
                Statistics.count(Statistics.Goods.goods_detail_service_click, andAttributes: ["gid":String(describing: self?.goodsModel.gid)])
                
                //商品信息
                let source = QYSource()
                source.title = "商品详情"
                source.urlString = self!._goodsModel.detail
                let commodityInfo = QYCommodityInfo()
                commodityInfo.title = self?._goodsModel.goods_name
                
                let goodsArray = self!.qyGoodsInfo(self!._goodsModel)
                commodityInfo.desc = goodsArray[0]
                if self?._goodsModel.goods_images?.count > 0 {
                    commodityInfo.pictureUrlString = self?._goodsModel.goods_images![0].image
                }
                
                commodityInfo.urlString = self!._goodsModel.detail
                commodityInfo.note = goodsArray[1]
                
                //页面跳转
                let qyvc = QYSDK.shared().sessionViewController()
                qyvc?.navigationItem.leftBarButtonItem = self!.qyCustomBackButton()
                qyvc?.sessionTitle = ""
                qyvc?.commodityInfo = commodityInfo
                qyvc?.source = source
                qyvc?.groupId = 74126
                QYSDK.shared().customActionConfig().linkClickBlock = {
                    [weak self](QYLinkClickBlock) -> Void in
                    let temp = QYLinkClickBlock! as String
                    EventDispatcher.dispatch(temp, onNavigationController: self?.navigationController)
                }
                self?.navigationController?.pushViewController(qyvc!, animated: true)
            }else {
                self?.jumpToLogin(Constant.InLoginType.consultType, tipe: nil)
            }
            })
        
        
        // 送自己
        let _ = self.toolBar.addBasketButton.handle(events: .touchUpInside) {[weak self] (sender, event) -> Void in
            
            self?.showGoodsInfoSelector(true)
        }
        
        let _ = self.toolBar.buyNowButton.handle(events: .touchUpInside) {[weak self] (sender, event) -> Void in
            
            self?.toolBar.buyNowButton.backgroundColor = UIColor(rgba: Constant.common_C1_color)
            
            if UserInfoManager.didLogin == true{
                self?.showGoodsInfoSelector(false)
            }
            else{
                self?.triggerType = TriggerType.getNowType.rawValue
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.loginType = Constant.InLoginType.goodsGeneraType.rawValue
                self?.loginDialog.controller = self
                self?.loginDialog.show()
            }
        }
        
    }
    
    fileprivate func collectGoods(_ view:AnyObject){

        
        let sender = view as! UIButton
            let goodId = self.goodsModel.gid
            Statistics.count(Statistics.Goods.goods_detail_like_click, andAttributes: ["gid":"\(goodId)"])
            
            if UserInfoManager.didLogin == true{
                
                let collectionStatus = (self.goodsModel.collect_status == 1)
                
                if collectionStatus{

                    let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.collect_del, parameters: ["collect_type": FavouriteType.goods.rawValue as AnyObject, "obj_id": goodId as AnyObject], isNeedUserTokrn: true, isShowErrorStatuMsg: true,isShowSuccessStatuMsg:true,successHandler: {data, status, msg in

                        if status == 200
                        {
                            sender.setImage(UIImage(named: "goods_general_toolbar_heart"), for: UIControlState())
                            
                            self.goodsModel.collect_status = 0
                            self.goodsModel.collect_num  = self.goodsModel.collect_num - 1
                            self.isCollect = false
                            
                            let dic = [GoodsDetailViewController.COLLECT_NUM: self.goodsModel.collect_num, GoodsDetailViewController.COLLECT_STATE: self._goodsModel.collect_status];
                            NotificationCenter.default.post(name: Notification.Name(rawValue: GoodsDetailViewController.COLLECT_INFO), object: dic)
                            
                        }
                        
                    })
                    
                }
                else{
//                    let params = ["collect_type": FavouriteType.goods.rawValue as AnyObject, "obj_id": [goodId]]
                    let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.collect_add, parameters: ["collect_type": FavouriteType.goods.rawValue as AnyObject, "obj_id": goodId as AnyObject], isNeedUserTokrn: true, isShowErrorStatuMsg: true,isShowSuccessStatuMsg:true,successHandler: {data, status, msg in

                        if status == 200
                        {
                            sender.setImage(UIImage(named: "goods_general_toolbar_heart_press"), for: UIControlState())
                            self._goodsModel.collect_status = 1
                            self._goodsModel.collect_num  = self.goodsModel.collect_num + 1
                            self.isCollect = true
                            let dic = [GoodsDetailViewController.COLLECT_NUM: self.goodsModel.collect_num, GoodsDetailViewController.COLLECT_STATE: self._goodsModel.collect_status];
                            NotificationCenter.default.post(name: Notification.Name(rawValue: GoodsDetailViewController.COLLECT_INFO), object: dic)

                        }
                    })
                }
            }
            else{
                self.jumpToLogin(Constant.InLoginType.goodsGeneraType, tipe: nil)
//                let animation = CATransition()
//                animation.duration = 0.3
//                animation.type = kCATransitionMoveIn
//                animation.subtype = kCATransitionFromTop
//                animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
//                self.navigationController?.view.layer.add(animation, forKey: "")
//                //                    self?.triggerType = TriggerType.collectType.rawValue
//                let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                appDelegate.loginType = Constant.InLoginType.goodsGeneraType.rawValue
//                let loginVC = LoginViewController()
//                self.navigationController?.pushViewController(loginVC, animated: false)
//                Utils.showError(context: self.view, errorStr: "登录后才能添加喜欢")
            }
//        }
    }
    
    
    //
    fileprivate func showImagesView(_ imageSource:[String],currentIndex:Int){
        
        let igvc = ImageGalleryViewController()
        
        igvc.imageSource = imageSource
        igvc.show(from: self.tempBannerView,currentIndex:currentIndex)
    }
    
    
    
    
    
    //网易七鱼
    fileprivate func qyGoodsInfo(_ goodInfo:GoodsModel) ->[String] {
        var goodsArray = ["",""]
        var goods_attribute = [Int:String]()
        
        if let attribute = goodInfo.goods_attr {
            //取出所有属性
            for index0 in 0 ..< attribute.count {
                if let detail = attribute[index0].attr_value {
                    for index1 in 0 ..< detail.count {
                        let tempKey = detail[index1].id
                        goods_attribute[tempKey] = detail[index1].name
                    }
                }
            }
            
            if let goods_sub = goodInfo.goods_sub {
                var goodsNum = 0
                var price = 0
                var itemId = ""
                for index2 in 0 ..< goods_sub.count {
                    itemId = goods_sub[index2].attr_item_ids
                    goodsNum = goods_sub[index2].Goods_num
                    price = Int(goods_sub[index2].price)
                    
                    let tempArray = itemId.components(separatedBy: ",")
                    for index3 in 0 ..< tempArray.count {
                        if index3 < tempArray.count - 1{
                            goodsArray[0] += goods_attribute[Int(tempArray[index3])!]! + ","
                        }else {
                            goodsArray[0] += goods_attribute[Int(tempArray[index3])!]!
                        }
                    }
                    
                    goodsArray[0] += "￥\(price)" + " " + "x\(goodsNum)" + ";"
                }
            }
            
        }
        let discount_price = goodInfo.discount_price
        goodsArray[1] += "￥\(discount_price)"
        return goodsArray
    }
    
    
    func qyCustomBackButton() -> UIBarButtonItem
    {
        let backButton = UIButton(type: UIButtonType.custom)
        backButton.frame = CGRect(x: 0, y: 0, width: 36, height: 36)
        backButton.setBackgroundImage(UIImage(named: "common_back_icon"), for: UIControlState())
        backButton.addTarget(self, action: #selector(GoodsDetailViewController.backButtonClicked), for: UIControlEvents.touchUpInside)
        backButton.clipsToBounds = true
        backButton.tintColor = UIColor.red
        
        let item = UIBarButtonItem(customView: backButton)
        
        return item
    }
    
    
    func backButtonClicked(){
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    @objc fileprivate func gobuyButtonAction(_ btn:UIButton){
        
        btn.backgroundColor = UIColor(rgba: Constant.common_C1_color)
        
        let goodsVC = VenderGoodsViewController()
        goodsVC.title_nav = _goodsModel.goods_name
        goodsVC.urlStr = _goodsModel.goods_link
        self.navigationController!.pushViewController(goodsVC, animated: true)
    }
    
    
    //获取数据
    func fetchDataSource() {
        let requestManager = GoodsRequestManager(delegate: self)
        requestManager.goodsLoadDetail(self.view, goodsId: self.goods_ID)
        requestManager.goodsLoadRelationList(self.view, goodsId: self.goods_ID)
        requestManager.goodsLoadSpceInfo(self.view, goodsId: self.goods_ID)
    }
    
    
    // MARK: 弹出规格选择界面
    func showGoodsInfoSelector(_ isAddInBasket: Bool) {
        
        
        if let info = self._goodsModel{
            let selectView : UIView = SpecificationsSelectView(frame: CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: 90 + ViewHeight() + 80 + 44), goodInfo: JSON(info.ORIGINAL_DATA! as AnyObject),isAddInBasket: isAddInBasket,nav: self.navigationController!)
            
            self.presentSemiView(selectView, withOptions:nil, completion: {
                
            })
        }
    }
    
    
//    
    func changeTitleColor(_ scrollView:UIScrollView) {
        
        if scrollView.offsetY < Constant.ScreenSizeV2.SCREEN_WIDTH - 64{
            selfNavigationBar.alpha = scrollView.offsetY / (Constant.ScreenSizeV2.SCREEN_WIDTH - 64)
        }else{
            selfNavigationBar.alpha = 1
        }
        
        self.view.bringSubview(toFront: QYFbackButton)
        self.view.bringSubview(toFront: QYFshareButton)
        self.view.bringSubview(toFront: basketButton)
        
    }

    
    func ViewHeight() ->CGFloat
    {
        var ruleViewHeight : CGFloat = 0.0
        if let attrs = _goodsModel.goods_attr{
            for index in 0 ..< attrs.count
            {
                if let attr_value = attrs[index].attr_value{
                    var arr = [JSON]()
                    let _ = attr_value.map{arr.append(JSON($0.ORIGINAL_DATA! as AnyObject))}
                    ruleViewHeight = ruleViewHeight + ModuleViewHeight(arr)
                }
            }
        }
        return ruleViewHeight
    }
    
    
    func ModuleViewHeight(_ array : [JSON]) -> CGFloat{
        let xGap : CGFloat = 5
        let height : CGFloat = 30
        var row : Int = 0
        let number : Int = array.count
        if number%3 == 0{
            row = number/3
        }
        else
        {
            row = number/3+1;
        }
        return 25 + CGFloat(row) * (height+xGap)
        
    }
    
    
}

// MARK: 代理--
extension GoodsDetailViewController: BaseGoodsDetailUnitViewDelegate,GoodsRequestManagerDelegate,UIScrollViewDelegate,LMWebViewWidgetsDelegate{
    
    
    //MARK-:delegate:
    func baseGoodsDetailUnitViewTouchdAction(_ actionType:GoodsDetailUnitViewActionType,content:[String:AnyObject]?){
        
        let index = actionType.rawValue
        let key = ActionKeys[index]
        
        switch actionType {
        //预览图片被点击
        case .goodsPictureSelected:
            
            if let imageNameAry = content?["imageAry"] as? [String],
                let currentIndex = content?["currentIndex"] as? Int{
                self.showImagesView(imageNameAry, currentIndex: currentIndex)
            }
            
            break
            
        case .limitSale:
            
            if let eventMoedl = self.goodsModel.event {
                let VC = GoodsOtherEventListViewController.newGoodsOtherEventWithID(self.goodsModel.gid, eventID: eventMoedl.event_id)
                self.navigationController?.pushViewController(VC, animated: true)
            }
            break
            
        case .goodsPriceInfo:
            if let sender = content![key] {
                collectGoods(sender)
            }
            
            break
        case .goodsFeature:
            let view = ServicePromiseView(frame: CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: 960 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE))

            view.backgroundColor = UIColor.white
            self.presentSemiView(view, withOptions:nil, completion: {
                
            })
            
            break
        case .recommondGoodsSelected:
            if let goodsId = content?[key] as? Int{
                self.navigationController?.pushViewController(GoodsDetailViewController.newGoodsDetailWithID(goodsId), animated: true)
            }
            break
            
//        default:
//            break
        }
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //最上边nav随offset改变   1.3.13 现在不需要了
        changeTitleColor(scrollView)
        self.tempBannerView.frame = goodsDetailHeadView.goodsBannerView.frame
        if scrollView.offsetY > Constant.ScreenSizeV2.SCREEN_HEIGHT - (MARGIN_64 + MARGIN_44) {
            if self.scrollToTopButton.isHidden {
                self.scrollToTopButton.isHidden = false
            }
        }else {
            if !self.scrollToTopButton.isHidden {
                self.scrollToTopButton.isHidden = true
            }
        }
    }
    
    
    func lmWebViewNavigationItemMenuDidselected(_ navigationView: NavigationItemMenu, selectedIndex: Int) {
        
        //礼物篮
        if selectedIndex == 0 {
            
            Statistics.count(Statistics.Goods.goods_detail_basket_click, andAttributes: ["did":"\(self.goods_ID)"])
            self.navigationController?.pushViewController(self.basket, animated: true)
            
        }else{
            //去分享 需要设置一些分享的字段：
            

            
            self.share()
        }
    }
    
    
    
    func goodsRequestDidLoadGoodsSpceInfo(_ manager: LMRequestManager, result: LMResultMode, detailModel: [GoodsSpceInfoModel]) {
        goodsDetailHeadView.reloadDataArrOfSpce(detailModel)
    }
    
    func goodsRequestDidLoadGoodsDetail(_ manager: LMRequestManager, result: LMResultMode, detailModel: GoodsModel?) {
        
        if let model =  detailModel  {
            toolBar.alpha = 1
            goodsDetailHeadView.reloadData(model)
//            self.setHeadView(goodsDetailHeadView)
            self.scrollView.contentH = goodsDetailHeadView.iheight
            self.toolBar.goodInfo = JSON(model.ORIGINAL_DATA! as AnyObject)
            if let id = detailModel?.gid{
                Statistics.count(Statistics.Goods.goods_inside_detail_click, andAttributes: ["gid":"\(id)"])
            }
            self._goodsModel = model
        }
    }
    
    func goodsRequestDidLoadRelationGoodsList(_ manager: LMRequestManager, result: LMResultMode, goodsList: [GoodsRelationModel]) {
        goodsDetailHeadView.reloadDataArr(goodsList)
        self.scrollView.contentH = goodsDetailHeadView.iheight
    }
}

extension UINavigationBar {
    
    
}

