//
//  GoodsOtherEventListViewController.swift
//  Limi
//
//  Created by 千云锋 on 16/7/21.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
/**
点击限时活动条 --> 其他活动商品
 
 */
import UIKit

class GoodsOtherEventListViewController: BaseViewController, GoodsRequestManagerDelegate,  CommonCollectionViewDelegate{

    var dataSource:[JSON] = []
    
    var goods_ID:Int{return _goods_ID}
    var event_ID:Int{return _event_ID}
    
    fileprivate var _goods_ID = 0
    fileprivate var _event_ID = 0
    
    fileprivate var goods_listView: CommonCollectionView!

    
    class func newGoodsOtherEventWithID(_ goodsID:Int, eventID:Int) ->GoodsOtherEventListViewController{
        
        let goodsOtherEventVC =  GoodsOtherEventListViewController()
        
        goodsOtherEventVC._goods_ID = goodsID
        goodsOtherEventVC._event_ID = eventID
        
        return goodsOtherEventVC
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.goods_listView.collectionView.beginHeaderRefresh()

    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "其他活动商品"
        
        fetchDataSource()
        setupUI()
    }
    
    //创建UI
    func setupUI() {
        goods_listView = CommonCollectionView.newCommonCollectionView(backView: nil, type: GoodsFavType.fav, userVC: self, stowBool: false)
        var frame = self.view.bounds
        frame.size.height = self.view.bounds.size.height - 64
        goods_listView.frame = frame
        goods_listView.delegate  = self
        self.view.addSubview(goods_listView)
    }
    
    //MARK-:返回的数据
    func goodsRequestDidLoadOtherEventGoodsList(_ manager: LMRequestManager, result: LMResultMode, goodsList: [GoodsModel]) {

        if goodsList.count > 0 {
            for model in goodsList {
                self.dataSource.append(JSON(model.ORIGINAL_DATA! as AnyObject))
            }
            self.goods_listView.goodsList = self.dataSource
        }else{
            //self.goods_listView.collectionView.showTipe(UIScrollTipeMode.NullData).tipeContent("啊哦，还没有数据").tipeIcon("")
            self.goods_listView.collectionView.showTipe(UIScrollTipeMode.nullData)
        }
        
//        }
    }
    

    //请求数据
    func fetchDataSource() {
//        if self.goods_listView.collectionView.isHeaderRefreshing {return}
        
        let requestManager = GoodsRequestManager(delegate: self)
        requestManager.goodsLoadOtherList(self.view, goodsId: goods_ID, eventId: event_ID)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK-:Collection代理
    
    func commonCollectionViewDidSelected(_ data: Int) {
        
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
