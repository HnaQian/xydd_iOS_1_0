//
//  SpecificationSelectedGoodInfoView.swift
//  Limi
//
//  Created by maohs on 16/4/21.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class SpecificationSelectedGoodInfoView: UIView {

    //商品
    var imageView = UIImageView(image: UIImage(named: "loadingDefault"))   //商品图片
    fileprivate var titleLabel = UILabel()  //商品名称
    fileprivate var closeImage = UIImageView()  //关闭图片
    fileprivate var closeButton = UIButton(type: .custom)    //关闭按钮
    
    fileprivate var priceLabel = StrikeThroughLabel()//现价
    fileprivate var mPriceLabel = StrikeThroughLabel()//浮动价
    fileprivate var convertPrice = "0"
    fileprivate var convertmPrice = "0"
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setUpUI()
    }
    
    func setUpUI() {
        
        for view in [imageView, titleLabel, priceLabel, mPriceLabel, closeImage,closeButton] as [Any] {
            self.addSubview(view as! UIView)
        }

        imageView.backgroundColor = UIColor.clear
        imageView.layer.cornerRadius = 3
        imageView.clipsToBounds = true
        imageView.contentMode = UIViewContentMode.scaleAspectFill
        
        titleLabel.font = UIFont.systemFont(ofSize: Constant.common_F2_font)
        titleLabel.textColor = UIColor(rgba: Constant.common_C2_color)
        
        priceLabel.font = UIFont.systemFont(ofSize: Constant.common_F3_font)
        priceLabel.strikeThroughEnabled = false
        priceLabel.textColor = UIColor(rgba: Constant.common_C1_color)
        
        mPriceLabel.font = UIFont.systemFont(ofSize: Constant.common_F3_font)
        mPriceLabel.strikeThroughEnabled = true
        mPriceLabel.textColor = UIColor(rgba: Constant.common_C8_color)
        
        closeImage.image = UIImage(named: "goods_order_info_attribute_button_delegate")
        closeButton.addTarget(self, action: #selector(SpecificationSelectedGoodInfoView.dismissContent), for: .touchUpInside)
        
        imageView.snp_makeConstraints { (make) -> Void in
            let _ = make.left.equalTo(self).offset(MARGIN_10)
            let _ = make.top.equalTo(self).offset(10)
            let _ = make.width.equalTo(70)
            let _ = make.height.equalTo(imageView.snp_width)
        }
        
        titleLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(imageView)
            let _ = make.left.equalTo(imageView.snp_right).offset(2)
            let _ = make.right.lessThanOrEqualTo(closeImage.snp_left).offset(-10)
        }
        
        priceLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.bottom.equalTo(imageView)
            let _ = make.left.equalTo(titleLabel)
        }
        
        mPriceLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.centerY.equalTo(priceLabel)
            let _ = make.left.equalTo(priceLabel.snp_right).offset(10)
        }
        
        closeImage.snp_makeConstraints { (make) -> Void in
            let _ = make.right.equalTo(self)
            let _ = make.top.equalTo(self)
            let _ = make.height.equalTo(40)
            let _ = make.width.equalTo(40)
        }
        
        closeButton.snp_makeConstraints { (make) -> Void in
            let _ = make.right.equalTo(self)
            let _ = make.top.equalTo(self)
            let _ = make.width.equalTo(40)
           let _ =  make.height.equalTo(40)
        }
        
        // separator
        let sep = UIView()
        sep.backgroundColor = UIColor(rgba: Constant.common_separatLine_color)
        self.addSubview(sep)
        sep.snp_makeConstraints { (make) -> Void in
            let _ = make.left.equalTo(self).offset(MARGIN_10)
            let _ = make.right.equalTo(self).offset(-MARGIN_10)
            let _ = make.bottom.equalTo(self)
            let _ = make.height.equalTo(0.5)
        }

    }
    
    func updateUI(_ goodInfo : JSON) {
        
        let imageScale_1 : String = (goodInfo["Goods_image"].string)! + "?imageView2/0/w/" + "\(Int(54*Constant.ScreenSize.SCALE_SCREEN))"
        let imageScale_2 : String = "/h/" + "\(Int(54*Constant.ScreenSize.SCALE_SCREEN))"
        let imageScale = imageScale_1 + imageScale_2
        
        self.imageView.af_setImageWithURL(URL(string:imageScale)!, placeholderImage: UIImage(named: "loadingDefault"))
        
        self.titleLabel.text = goodInfo["Goods_name"].string
        
        if let _ = goodInfo["Goods_outer"].int {
            self.titleLabel.snp_makeConstraints(closure: { (make) -> Void in
                let _ = make.top.equalTo(self.imageView)
                let _ = make.left.equalTo(self.imageView.snp_right).offset(2)
                let _ = make.left.lessThanOrEqualTo(self.closeImage.snp_left).offset(-10)
            })
            
            priceLabel.snp_makeConstraints(closure: { (make) -> Void in
                let _ = make.bottom.equalTo(self.imageView)
                let _ = make.left.equalTo(self.titleLabel)
            })
        }
        
        if let discount_price : Float = goodInfo["Discount_price"].float{
            convertPrice = normalPrice(price: discount_price)
            priceLabel.text = normalPrice(price: discount_price)
        }
        if let market_price : Float = goodInfo["Market_price"].float{
            if market_price > 0{
                convertmPrice = normalPrice(price: market_price)
                mPriceLabel.text = normalPrice(price: market_price)
            }
        }
        
        if let event = goodInfo["Event"].dictionary
        {
            let type = event["Type"]!.int ?? 1
            
            switch type
            {
            case 1,2,5:
                priceLabel.text = "￥0"
                if let price = goodInfo["Discount_price"].float {
                    convertPrice = normalPrice(price: price)
                    priceLabel.text = normalPrice(price: price)
                
                }
                
                if let price = goodInfo["Price"].float {
                    convertmPrice = normalPrice(price: price)
                    mPriceLabel.text = normalPrice(price: price)
                    
                }
                
            default:
                
                break
            }
        }
    }
    
    func normalPrice(price:Float) -> String {
        var resultPrice = "0"
        if price > 0{
            let floatPrice = roundf(price)
            if floatPrice == price{
                let tempString = String(format: "￥%.2f", price)
                if (tempString.contains(".00")) {
                    resultPrice = String(format: "￥%.0f", price)
                }
            }
            else{
                let priceStr1 : NSString = String(format: "%.2f", price) as NSString
                let priceStr2 : NSString = String(format: "%.1f", price) as NSString
                let floatPrice1 : Float = priceStr1.floatValue
                let floatPrice2 : Float = priceStr2.floatValue
                if floatPrice1 == floatPrice2{
                    resultPrice = String(format: "￥%.1f", price)
                }
                else{
                    resultPrice = String(format: "￥%.2f", price)
                }
            }
        }
        
        return resultPrice
    }
    
    func priceChanged(_ price:Float,marketPrice:Float)  {
        convertPrice = normalPrice(price: price)
        priceLabel.text = normalPrice(price: price)
        
        convertmPrice = normalPrice(price: marketPrice)
        mPriceLabel.text = normalPrice(price: marketPrice)
    }
    
    func goodsNumberChanged(_ goodsNum: Int) {
//        self.priceLabel.text = String(format: "￥%.2f", convertPrice * Float(goodsNum))
//        if self.convertmPrice * Float(goodsNum) > 0{
//            self.mPriceLabel.text = String(format: "￥%.2f", self.convertmPrice * Float(goodsNum))
//        }

    }
    
    func dismissContent(){
        NotificationCenter.default.post(name: Notification.Name(rawValue: "datepicker_doneBtnClick"), object: self, userInfo: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
