//
//  SpecificationsSelectView.swift
//  Limi
//
//  Created by ZhiQiangMi on 15/12/16.
//  Copyright © 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit
import CoreData
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


private var singIdArr  = [String]()

class SpecificationsSelectView: UIView ,SpecificationSelectedMoreRuleViewDelegate,GiftBasketRequestManagerDelegate{
    
    var animationLayers: [CALayer]?
    
    fileprivate var isAddInBasket : Bool = false
    
    var navcontroller : UINavigationController = UINavigationController()
    var goodInfo: JSON!
    let goodInfoView = SpecificationSelectedGoodInfoView()
    var commitButton = UIButton()
    
    //规格
    let moreRuleView = SpecificationSelectedMoreRuleView()
    
    var totalNum : Int = 0
    var currentNum : Int = 1
    var Goods_sub : [JSON] = []
    var viewHeight : CGFloat = 0
    
    
    init(frame: CGRect,goodInfo : JSON,isAddInBasket : Bool , nav : UINavigationController) {
        super.init(frame: frame)
        viewHeight = frame.size.height
        self.isAddInBasket = isAddInBasket
        self.navcontroller = nav
        self.goodInfo = goodInfo
        self.backgroundColor = UIColor.white
        
        self.addSubview(goodInfoView)
        goodInfoView.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: 90)
        self.goodInfoView.updateUI(goodInfo)
        
        self.addSubview(moreRuleView)
        moreRuleView.frame = CGRect(x: 0, y: 90, width: Constant.ScreenSize.SCREEN_WIDTH, height: ViewHeight() + 80)
        self.moreRuleView.delegate = self
        singIdArr = self.moreRuleView.updateUI(goodInfo)
        
        self.moreRuleView.goodInfo = GoodsModel(goodInfo.dictionaryObject)
        
        
        commitButton.backgroundColor = UIColor(rgba: Constant.common_C1_color)
        commitButton.setTitle("确定", for: UIControlState())
        commitButton.setTitleColor(UIColor(rgba: Constant.common_C12_color), for: UIControlState())
        commitButton.titleLabel?.font = UIFont.systemFont(ofSize: Constant.common_F2_font)
        commitButton.frame = CGRect(x: 0, y: 90 + ViewHeight() + 80, width: Constant.ScreenSize.SCREEN_WIDTH, height: 44)
        commitButton.addTarget(self, action: #selector(SpecificationsSelectView.changeBackColor(_:)), for: UIControlEvents.touchDown)
        commitButton.addTarget(self, action: #selector(SpecificationsSelectView.generateOrder(_:)), for: .touchUpInside)
        self.addSubview(commitButton)
    }
    
    
//MARK: - SpecificationSelectedMoreRuleView delegate
    func didSelectedSpecialRuleWithPrice(_ price: Float,marketPrice: Float) {
        self.goodInfoView.priceChanged(price,marketPrice:marketPrice)
    }
    
    func didChangeGoodsNumber(_ goodsNum: Int) {
        self.goodInfoView.goodsNumberChanged(goodsNum)
    }
    
    func moreRuleChangedSingIdArr(_ singIdArray:[String]) {
        singIdArr = singIdArray
    }
    
    
    func ViewHeight() ->CGFloat
    {
        var ruleViewHeight : CGFloat = 0.0
        if let attrs = goodInfo["Goods_attr"].array{
            for index in 0 ..< attrs.count
            {
                if let Attr_value = attrs[index]["Attr_value"].array{
                    ruleViewHeight = ruleViewHeight + ModuleViewHeight(Attr_value)
                }
            }
        }
        return ruleViewHeight
    }
    
    func ModuleViewHeight(_ array : [JSON]) -> CGFloat{
        let xGap : CGFloat = 5
        let height : CGFloat = 30
        var row : Int = 0
        let number : Int = array.count
        if number%3 == 0{
            row = number/3
        }
        else
        {
            row = number/3+1;
        }
        return 25 + CGFloat(row) * (height+xGap)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func changeBackColor(_ sender: UIButton) {
        
        sender.backgroundColor = UIColor(rgba: Constant.common_C100_color)
    }
    
    @objc func generateOrder(_ sender: UIButton) {
        
        if isAddInBasket{

            if ShoppingBasketDataManager.shareManager().totalGoodsCount >= 20{
                LCActionSheet(title: "您的礼物篮已满，请将部分礼物移至\n收藏后再添加新的礼物", buttonTitles: ["前往礼物篮"], cancelTitle: "取消", redButtonIndex: 0) { (buttonIndex) -> Void in
                    if buttonIndex == 1{
                        self.navcontroller.pushViewController(ShoppingBasketViewController(), animated: true)
                         NotificationCenter.default.post(name: Notification.Name(rawValue: "datepicker_doneBtnClick"), object: self, userInfo: nil)
                    }
                    }.show()
                
                return
            }
        }
        
        var selectedAttrCollection : JSON?
        if let Goods_sub : [JSON] = goodInfo["Goods_sub"].array{
            for index in 0 ..< Goods_sub.count
            {
                let sub = Goods_sub[index]
                let idArr : [String] = (sub["Attr_item_ids"].string?.components(separatedBy: ","))!
                if singIdArr == idArr{
                    selectedAttrCollection = sub
                    break
                }
                
            }
        }
        
        var attributeTable = [JSON]()
        if singIdArr.count > 0{
            for index in 0 ..< singIdArr.count
            {
                let idStr = singIdArr[index]
                if let attrs = goodInfo["Goods_attr"].array{
                    if let Attr_value = attrs[index]["Attr_value"].array{
                        for i in 0 ..< Attr_value.count
                        {
                            let singId = Attr_value[i]
                            if singId["Id"].stringValue == idStr{
                                attributeTable.append(singId)
                            }
                        }
                    }
                }
            }
        }
        
        
        if isAddInBasket == false{
            
            if let gid:Int = self.goodInfo["Gid"].int{
                Statistics.count(Statistics.Goods.goods_detail_sent_click,andAttributes: ["gid":"\(gid)"])
            }
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "datepicker_doneBtnClick"), object: self, userInfo: nil)
            
            let goodsModel = GoodsModel(goodInfo.dictionaryObject)
            
            
            var subModel:GoodsSubModel?
            
            if let js = selectedAttrCollection
            {
                subModel = GoodsSubModel(js.dictionaryObject)
            }
            
            goodsModel.setSelectedSpecification(subModel, selectedGoodsSubModelCount: Int(self.moreRuleView.valueTextField.text!)!)
            
            let ogvc = OrderGeneratorViewControllerV1.orderGenerator([goodsModel],sourceType:1)
            
            navcontroller.pushViewController(ogvc, animated: true)
            
        }
        else{
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.usercenter_changed), object: nil)
            let time: TimeInterval = 1.0
            let delay = DispatchTime.now() + Double(Int64(time * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delay) {
                sender.isEnabled = true
                NotificationCenter.default.post(name: Notification.Name(rawValue: "datepicker_doneBtnClick"), object: self, userInfo: nil)
            }
            sender.isEnabled = false
            self.addProductsAnimation(self.goodInfoView.imageView)
            self.addToBasket(attributeTable,selectedAttrCollection: selectedAttrCollection)
        }
        
        
    }
    
    func addToBasket(_ attributeTable : [JSON],selectedAttrCollection : JSON?){
        
        if let gid:Int = self.goodInfo["Gid"].int{
            Statistics.count(Statistics.Goods.goods_detail_cart_click,andAttributes: ["gid":"\(gid)"])
        }
        
        if UserInfoManager.didLogin == true{
            var goods_sub_id : Int = 0
            if let subInfo = selectedAttrCollection {
                goods_sub_id =  subInfo["Id"].intValue
            }
            
            GiftBasketRequestManager(delegate: self).basketAdd(self, gids: [self.goodInfo["Gid"].intValue], subIds: [goods_sub_id], num: [Int(self.moreRuleView.valueTextField.text!)!])
        }
        else{
            let request: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "BasketItem")
            request.sortDescriptors = [NSSortDescriptor(key: "insertDate" , ascending: true)]
            
            CoreDataManager.shared.executeFetchRequest(request) { results in
                if results?.count > 0{
                    var isFindRepeat : Bool = false
                    var repeatIndex : Int = 0
                    for index in 0 ..< results!.count
                    {
                        let item : BasketItem = results![index] as! BasketItem
                        if item.goodInfo?.gid == String(self.goodInfo["Gid"].intValue){
                            if let subInfo = selectedAttrCollection {
                                print(subInfo)
                                print(item.selectedAttrCollection)
                                if String(format: "%d", subInfo["Id"].intValue) == item.selectedAttrCollection?.id{
                                    isFindRepeat = true
                                    repeatIndex = index
                                    break
                                }
                            }
                            else{
                                isFindRepeat = true
                                repeatIndex = index
                                break
                            }
                            
                        }
                        
                    }
                    if isFindRepeat == false{
                        if results?.count < 20{
                            self.addToCartCoraData(attributeTable, selectedAttrCollection: selectedAttrCollection)
                        }
                        else{
                            
                            NotificationCenter.default.post(name: Notification.Name(rawValue: "datepicker_doneBtnClick"), object: self, userInfo: nil)
                            
                        }
                        
                    }
                    else{
                        let cartItem : BasketItem = results![repeatIndex] as! BasketItem
                        cartItem.planCount = cartItem.planCount + Int64(self.moreRuleView.valueTextField.text!)!
                        CoreDataManager.shared.save()
                    }
                }
                else{
                    self.addToCartCoraData(attributeTable, selectedAttrCollection: selectedAttrCollection)
                }
            }
            NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.add_good_to_basket), object: nil)
        }
    }
    
    
    func giftBasketRequestManagerDidAdd(_ manager: LMRequestManager, result: LMResultMode) {
        if result.status == 200{
            NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.add_good_to_basket), object: nil)
        }
    }
    
    
    func addToCartCoraData(_ attributeTable : [JSON],selectedAttrCollection : JSON?){
        let item: BasketItem = NSEntityDescription.insertNewObject(forEntityName: "BasketItem", into: CoreDataManager.shared.managedObjectContext) as! BasketItem
        if attributeTable.count > 0{
            var str = ""
            for (info) in attributeTable {
                str += " " + (info["Name"].string ?? "")
            }
            item.attributeStr = str
        }
        print(Int64(self.moreRuleView.valueTextField.text!)!)
        item.insertDate = Date().timeIntervalSince1970
        item.planCount = Int64(self.moreRuleView.valueTextField.text!)!
        
        let good: GoodInfo = NSEntityDescription.insertNewObject(forEntityName: "GoodInfo", into: CoreDataManager.shared.managedObjectContext) as! GoodInfo
        
        if let num : Int = goodInfo["Goods_num"].int{
            print(num)
            good.totalNum = Int64(num)
        }
        good.gid = String(self.goodInfo["Gid"].intValue)
        good.goodsImage = goodInfo?["Goods_image"].string
        good.goodsName = goodInfo?["Goods_name"].string
        var price = goodInfo?["Discount_price"].floatValue ?? 0
        price += selectedAttrCollection?["Price"].floatValue ?? 0
        good.discountPrice = String(price)
        
        item.goodInfo = good
        if let subInfo = selectedAttrCollection {
            print(subInfo)
            let selectedAttrCollection: SelectedAttrCollection = NSEntityDescription.insertNewObject(forEntityName: "SelectedAttrCollection", into: CoreDataManager.shared.managedObjectContext) as! SelectedAttrCollection
            
            selectedAttrCollection.goodsNum = String(format: "%d", subInfo["Goods_num"].intValue)
            selectedAttrCollection.price = String(format: "%f", subInfo["Price"].floatValue)
            selectedAttrCollection.attrItemIds = subInfo["Attr_item_ids"].stringValue
            selectedAttrCollection.id = String(format: "%d", subInfo["Id"].intValue)
            item.selectedAttrCollection = selectedAttrCollection
        }
        CoreDataManager.shared.save()
    }
    
    
    func addProductsAnimation(_ imageView: UIImageView) {
        
        if (self.animationLayers == nil)
        {
            self.animationLayers = [CALayer]();
        }
        
        let frame = imageView.convert(imageView.bounds, to: self)
        let transitionLayer = CALayer()
        transitionLayer.frame = frame
        transitionLayer.contents = imageView.layer.contents
        self.layer.addSublayer(transitionLayer)
        self.animationLayers?.append(transitionLayer)
        let p1 = transitionLayer.position;
        let p3 = CGPoint(x: Constant.ScreenSize.SCREEN_WIDTH + 10, y: -(Constant.ScreenSize.SCREEN_HEIGHT - viewHeight + 25));
        
        let positionAnimation = CAKeyframeAnimation(keyPath: "position")
        let path = CGMutablePath();
        path.move(to: CGPoint(x: p1.x, y: p1.y))
        path.addCurve(to: CGPoint(x: p1.x, y: p1.y - 30), control1: CGPoint(x: p1.x, y: p3.y - 30), control2: CGPoint(x: p3.x, y: p3.y))
//        CGPathMoveToPoint(path, nil, p1.x, p1.y);
//        CGPathAddCurveToPoint(path, nil, p1.x, p1.y - 30, p1.x, p3.y - 30, p3.x, p3.y);
        positionAnimation.path = path;
        
        let opacityAnimation = CABasicAnimation(keyPath: "opacity")
        opacityAnimation.fromValue = 1
        opacityAnimation.toValue = 0.9
        opacityAnimation.fillMode = kCAFillModeForwards
        opacityAnimation.isRemovedOnCompletion = true
        
        let transformAnimation = CABasicAnimation(keyPath: "transform")
        transformAnimation.fromValue = NSValue(caTransform3D: CATransform3DIdentity)
        transformAnimation.toValue = NSValue(caTransform3D: CATransform3DScale(CATransform3DIdentity, 0.1, 0.1, 1))
        
        let groupAnimation = CAAnimationGroup()
        groupAnimation.animations = [positionAnimation, transformAnimation, opacityAnimation];
        groupAnimation.duration = 0.8
//        groupAnimation.delegate = self;
        
        transitionLayer.add(groupAnimation, forKey: "cartParabola")
        
        let time: TimeInterval = 0.8
        let delay = DispatchTime.now() + Double(Int64(time * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delay) {
            if self.animationLayers?.count > 0 {
                let transitionLayer = self.animationLayers![0]
                transitionLayer.isHidden = true
                transitionLayer.removeFromSuperlayer()
                self.animationLayers?.removeFirst()
                self.layer.removeAnimation(forKey: "cartParabola")
            }
        }
    }
    
}
