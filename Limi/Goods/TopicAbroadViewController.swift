//
//  TopicAbroadViewController.swift
//  Limi
//
//  Created by 程巍巍 on 6/24/15.
//  Copyright (c) 2015 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit


class TopicAbroadViewController: BirthdayTopicViewController {
    
    
    override var api_: String! {
        return Constant.JTAPI.abroad_page
    }
    
    // 表头
    var abroadHeaderView: AbroadHeaderView!
    
    override func refreshHeaderView(_ data: JSON) {

        if let children = data["children"].array
        {
        var reuseButtons = [Button]()
        for subview in self.abroadHeaderView.topicView.subviews as! [Button] {
            subview.removeFromSuperview()
            reuseButtons.append(subview)
        }
        var lastView: UIView?
        for item in children {
            let button = reuseButtons.isEmpty ?  Button() : reuseButtons.removeLast()
            if let src = item["attributes"]["src"].string {
                
                let imageScale : String = src + "?imageView2/0/w/" + "\(Int(Constant.ScreenSize.SCREEN_WIDTH*0.125*Constant.ScreenSize.SCALE_SCREEN))" + "/h/" + "\(Int(Constant.ScreenSize.SCREEN_WIDTH*0.125*Constant.ScreenSize.SCALE_SCREEN))"
                
                button.imageView.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "loadingDefault"))
            }
            if let title = item["attributes"]["title"].string {
                button.titleLabel.font = UIFont.systemFont(ofSize: 15)
                button.titleLabel.text = title
            }
            if let href = item["attributes"]["href"].string {
                var param = [String: AnyObject]()
                if let title = item["attributes"]["title"].string {
                    param["_title"] = title as AnyObject?
                }
                button.tapHandler = {
                    EventDispatcher.dispatch(href, params: param, onNavigationController: self.navigationController)
                }
            }
            
            self.abroadHeaderView.topicView.addSubview(button)

            button.snp_makeConstraints(closure: { (make) -> Void in
                
                let _ = make.top.equalTo(self.abroadHeaderView)
                let _ = make.bottom.equalTo(self.abroadHeaderView)
                
                let _ = make.width.equalTo(button.snp_height)
                
                if lastView == nil {
                    let _ = make.left.equalTo(self.abroadHeaderView)
                }else{
                    let _ = make.left.equalTo(lastView!.snp_right)
                }
            })
            
            lastView = button
        }
            self.colleView.headerView = self.abroadHeaderView
    }
        
    }
    
    override func reloadTopicNewGoodData(_ data: JSON) {
        if let src = data["children"]["attributes"]["src"].string {
            let imageScale : String = src + "?imageView2/0/w/" + "\(Int(Constant.ScreenSize.SCREEN_WIDTH*Constant.ScreenSize.SCALE_SCREEN))" + "/h/" + "\(Int(Constant.ScreenSize.SCREEN_WIDTH*0.56*Constant.ScreenSize.SCALE_SCREEN))"
            
            self.abroadHeaderView.imageView.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "loadingDefault"))
        }
        if let href = data["children"]["attributes"]["href"].string {
            self.abroadHeaderView.imageView.href = href
        }
        self.colleView.headerView = self.abroadHeaderView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.navigationItem.title = "海外"
        
        self.abroadHeaderView = AbroadHeaderView()
        self.abroadHeaderView.imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(TopicAbroadViewController.imageViewTaped(_:))))
        self.colleView.headerView = self.abroadHeaderView
        
        HeardHeight = margin +  (self.view.frame.size.width * 0.25) + headerLabelHeight + (self.view.frame.size.width * 0.56) + margin
        
        (colleView.collectionView.collectionViewLayout as? UICollectionViewFlowLayout)!.headerReferenceSize = CGSize(width: Constant.ScreenSize.SCREEN_WIDTH,height: HeardHeight)
        colleView.headerView.frame.size = CGSize(width: Constant.ScreenSize.SCREEN_WIDTH,height: HeardHeight)
    }
    
    override func imageViewTaped(_ sender: UITapGestureRecognizer) {
        if let href = (sender.view as? ImageView)?.href {
            EventDispatcher.dispatch(href, onNavigationController: self.navigationController)
        }
    }
    
    
    // 表头
    class AbroadHeaderView: UIView {
        
        let margin: CGFloat = 8
        
        let label1 = UILabel()
        let lineView = UIView()
        let newView = UIView()
        let hotSpotView = UIView()
        
        let topicView = UIView()
        let imageView = ImageView()
        
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            
            imageView.isUserInteractionEnabled = true
            
            topicView.backgroundColor = UIColor.white
            newView.backgroundColor = UIColor.white
            label1.text = "最热"
            label1.font = UIFont.systemFont(ofSize: 15)
            
            label1.backgroundColor = UIColor(rgba: Constant.common_background_color)
            
            label1.textColor = UIColor.lightGray
            
            label1.textAlignment = NSTextAlignment.center
            
            lineView.backgroundColor = UIColor.lightGray
            
            lineView.alpha = 0.4
            
            self.addSubview(topicView)
            self.addSubview(newView)
            self.addSubview(hotSpotView)
            self.addSubview(lineView)
            self.addSubview(label1)
            

            topicView.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(self)
                let _ = make.right.equalTo(self)
                let _ = make.top.equalTo(self).offset(8)
                let _ = make.height.equalTo(topicView.snp_width).multipliedBy(0.25)
            }

            label1.snp_makeConstraints { (make) -> Void in
                let _ = make.centerY.equalTo(topicView.snp_bottom).offset(headerLabelSpacingWithTop)
                let _ = make.width.equalTo(50)
                let _ = make.centerX.equalTo(self)
                let _ = make.height.equalTo(headerLabelHeight)
            }
            
            lineView.snp_makeConstraints { (make) -> Void in
                let _ = make.centerY.equalTo(label1)
                let _ = make.width.equalTo(self).offset(-margin * 2)
                let _ = make.centerX.equalTo(label1)
                let _ = make.height.equalTo(0.5)
            }

            

            newView.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(self).offset(margin)
                let _ = make.right.equalTo(self).offset(margin)
                let _ = make.top.equalTo(label1.snp_bottom)
            }

            hotSpotView.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(self)
                let _ = make.right.equalTo(self)
                let _ = make.top.equalTo(newView.snp_bottom).offset(8)
                let _ = make.height.equalTo(44 * 0.25)
            }

            newView.addSubview(imageView)

            imageView.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(newView)
                let _ = make.right.equalTo(newView)
                let _ = make.top.equalTo(newView)
                let _ = make.bottom.equalTo(newView)
                let _ = make.height.equalTo(imageView).multipliedBy(0.56)
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        
    }
    
    class Button: UIView {
        
        class ImageView: UIImageView {
            override func layoutSubviews() {
                super.layoutSubviews()
                self.clipsToBounds = true
                self.layer.cornerRadius = self.frame.width/2
            }
        }
        
        var href: String?
        var imageView = ImageView()
        var titleLabel = UILabel()
        var contentView = UIView()
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            self.isUserInteractionEnabled = true
            self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(Button.buttonTaped(_:))))
            self.addSubview(contentView)
            contentView.snp_makeConstraints { (make) -> Void in
                let _ = make.center.equalTo(self)
            }
            
            contentView.addSubview(imageView)
            contentView.addSubview(titleLabel)
            imageView.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(contentView)
                let _ = make.width.equalTo(self).multipliedBy(0.5)
                let _ = make.height.equalTo(imageView.snp_width)
            }

            titleLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(contentView)
                let _ = make.right.equalTo(contentView)
                let _ = make.top.equalTo(imageView.snp_bottom).offset(4)
                let _ = make.bottom.equalTo(contentView)
                let _ = make.centerX.equalTo(imageView)
            }
        }
        
        required init(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        var tapHandler: (()->())?
        @objc func buttonTaped(_ sender: UITapGestureRecognizer) {
            tapHandler?()
        }
    }
    
}

    
