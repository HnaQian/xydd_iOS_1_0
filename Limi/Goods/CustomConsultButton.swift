//
//  CustomConsultButton.swift
//  Limi
//
//  Created by maohs on 16/7/8.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class CustomConsultButton: UIButton {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

    fileprivate let iconImage:UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "icon_contactService")
        return image
    }()
    
    fileprivate let badgeLabel:UILabel = {
        let label = UILabel()
        let badgeLbl_width:CGFloat = 14
        
        label.clipsToBounds = true
        label.layer.cornerRadius = badgeLbl_width/2
        label.textColor = UIColor.white
        label.textAlignment = NSTextAlignment.center
        label.font = UIFont.systemFont(ofSize: Constant.common_F6_font)
        label.backgroundColor = UIColor(rgba: Constant.common_red_color)
        return label
    }()
    
    fileprivate let headLabel:UILabel = {
        let label = UILabel()
        label.textColor = UIColor(rgba: Constant.common_C2_color)
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: Constant.common_F5_font)
        label.text = "客服"
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: CGRect(x: 0, y: 0, width: 40, height: 44))
        self.setUpUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func setUpUI() {
        self.addSubview(iconImage)
        self.addSubview(badgeLabel)
        self.addSubview(headLabel)
        
        iconImage.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(self).offset(2)
            let _ = make.centerX.equalTo(self)
            let _ = make.width.equalTo(25)
            let _ = make.height.equalTo(24)
        }
        
        badgeLabel.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(self)
            let _ = make.right.equalTo(self)
            let _ = make.width.equalTo(14)
            let _ = make.height.equalTo(14)
        }
        
        headLabel.snp_makeConstraints { (make) in
            let _ = make.centerX.equalTo(self)
            let _ = make.top.equalTo(iconImage.snp_bottom).offset(-1)
            let _ = make.width.equalTo(self)
            let _ = make.height.equalTo(13)
        }
        
        refreshBageValue(0)
        
    }
    
    func refreshBageValue(_ count:Int) {
        if count == 0 {
            self.badgeLabel.isHidden = true
        }else {
            self.badgeLabel.isHidden = false
            if count < 9 {
                self.badgeLabel.font = UIFont.systemFont(ofSize: Constant.common_F5_font)
            }else {
                self.badgeLabel.font = UIFont.systemFont(ofSize: Constant.common_F7_font)
            }
            
            self.badgeLabel.text = count > 99 ? "...":"\(count)"
        }
    }
    
}
