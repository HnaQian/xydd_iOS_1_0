//
//  ShoppingBasketLoggedInManager.swift
//  Limi
//
//  Created by Richie on 16/4/7.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//登录之后的数据管理类

import UIKit

class ShoppingBasketLoggedInManager: ShoppingBasketDataManager {
    
    override func refreshData(){
        self.requestManager.basketList(self.hudSuperView!)
    }
    
    
    fileprivate var timer:Timer!
    fileprivate var delayTime:TimeInterval = 0
    fileprivate var timeHandler:(()->Void)!
    
    
    func delayHandler(_ delayTime:TimeInterval,handler:@escaping (()->Void)){
        self.delayTime = delayTime
        self.timeHandler = handler
        
        if self.timer != nil{self.timer.invalidate();self.timer = nil}
        self.timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(ShoppingBasketLoggedInManager.clock), userInfo: nil, repeats: true)
    }
    
    func clock(){
        if delayTime < 0.1{
            if self.timer != nil{self.timer.invalidate();self.timer = nil;delayTime = 0}
            if self.timeHandler != nil{
                self.timeHandler()
            }
        }
        delayTime = delayTime - 0.1
    }
    
    
    //编辑数量
    override func changeGoodsNum(_ section:Int,row:Int,increase:Bool,count:Int = 0){
        
        //登录状态，编辑模式，不实时更新数据
        let item = allBasketDataArray[section][row]
        
        if count != 0{
            item.num = count
        }else{
            item.num = item.num + (increase ? 1 : -1)
        }
        
        if editMode && section != 1{
            self.delegate?.shoppingBasketDataManagerTableRefresh()
            self.delegate?.shoppingBasketDataManagerRefreshCost()
            
        }else if section != 1{
            
            self.delegate?.shoppingBasketDataManagerTableRefresh()
            self.delegate?.shoppingBasketDataManagerRefreshCost()
            
            //登录状态，编辑模式，实时更新数据
            var params = [String: AnyObject]()
            params["id"] = item.id as AnyObject?
            params["num"] = item.num as AnyObject?

            if count != 0{
                
               let _ = LMRequestManager.requestByDataAnalyses(context:self.hudSuperView!,URLString: Constant.JTAPI.gift_cart_update_num,parameters: params,isNeedUserTokrn: true, isShowErrorStatuMsg: true, isNeedHud: false,successHandler: {data, status, msg in
                    
                    if let giftData = JSON(data as AnyObject)["data"]["gift_basket"].dictionaryObject
                    {
                        let model = GoodsOfBasketModel(giftData)
                        
                        if self.allBasketDataArray.count > section{
                            if self.allBasketDataArray[section].count > row{
                                model.indexPath = self.allBasketDataArray[section][row].indexPath
                                model.didSelected = self.allBasketDataArray[section][row].didSelected
                                self._allBasketDataArray[section][row] = model
                            }
                        }
                        
                        self.delegate?.shoppingBasketDataManagerTableRefresh()
                    }
                })
                
            }else{
                
                self.delayHandler(0.5)
                {
                   let _ = LMRequestManager.requestByDataAnalyses(context:self.hudSuperView!,URLString: Constant.JTAPI.gift_cart_update_num,parameters: params,isNeedUserTokrn: true, isShowErrorStatuMsg: true, isNeedHud: false,successHandler: {data, status, msg in
                        
                        if let giftData = JSON(data as AnyObject)["data"]["gift_basket"].dictionaryObject
                        {
                            let model = GoodsOfBasketModel(giftData)
                            
                            if self.allBasketDataArray.count > section{
                                if self.allBasketDataArray[section].count > row{
                                    model.indexPath = self.allBasketDataArray[section][row].indexPath
                                    model.didSelected = self.allBasketDataArray[section][row].didSelected
                                    self._allBasketDataArray[section][row] = model
                                }
                            }
                            self.delegate?.shoppingBasketDataManagerTableRefresh()
                        }
                    })
                }
            }
        }
    }
    
    
    ///删除已选中的
    override func deleteSome(){
        
        if self.getSelectedItems().count == 0{
            return
        }
        
        var ids = [Int]()
        
        for item in self.getSelectedItems(){
            ids.append(item.id)
        }

       let _ = LMRequestManager.requestByDataAnalyses(context:self.hudSuperView!,URLString: Constant.JTAPI.gift_cart_del, parameters: ["id":ids as AnyObject], isNeedUserTokrn: true, isShowErrorStatuMsg: true,successHandler: {data, status, msg in
            if status == 200{
                self.delegate?.shoppingBasketDataManagerTableReloadData()
            }
            
        }){Void in }
        
    }
    
    
    ///删除指定的
    override func deleteOne(_ section:Int,row:Int,finishHandler:(()->Void)! = nil){
        if allBasketDataArray.count > section{
            if allBasketDataArray[section].count <= row{return}
        }else{return}
        
       let _ = LMRequestManager.requestByDataAnalyses(context:self.hudSuperView!,URLString: Constant.JTAPI.gift_cart_del, parameters: (["id":[allBasketDataArray[section][row].id]] as AnyObject) as? [String : AnyObject], isNeedUserTokrn: true, isShowErrorStatuMsg: true,successHandler: {data, status, msg in
            if status == 200{
                self.refreshData()
            }
            
        }){Void in }
    }
    
    
    //收藏已选中的
    override func collectSome(){
        
        if self.getSelectedItems().count == 0{return}
        
        var goodsIds = [Int]()
        var ids = [Int]()
        
        
        for item in self.getSelectedItems(){
            goodsIds.append(item.goods.gid)
            ids.append(item.id)
        }
        
        
       let _ = LMRequestManager.requestByDataAnalyses(context:self.hudSuperView!,URLString: Constant.JTAPI.collect_add, parameters: ["collect_type": FavouriteType.goods.rawValue as AnyObject, "obj_id": goodsIds as AnyObject], isNeedUserTokrn: true, isShowErrorStatuMsg: true,successHandler: {data, status, msg in
            if status == 200{
               let _ = LMRequestManager.requestByDataAnalyses(context:self.hudSuperView!,URLString: Constant.JTAPI.gift_cart_del, parameters: ["id":ids as AnyObject], isNeedUserTokrn: true, isShowErrorStatuMsg: true,successHandler: {data, status, msg in
                    if status == 200{
                        self.refreshData()
                    }
                    
                }){Void in }
            }
        }){Void in }
    }
    
    
    //收藏指定的
    override func collectOne(_ section:Int,row:Int,finishHandler:(()->Void)! = nil){
        if allBasketDataArray.count > section{
            if allBasketDataArray[section].count <= row{return}
        }else{return}
        let collectId = [allBasketDataArray[section][row].goodsId]
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let _ = LMRequestManager.requestByDataAnalyses(context:appDelegate.currentViewController!.view,URLString: Constant.JTAPI.collect_add, parameters: (["collect_type": FavouriteType.goods.rawValue as AnyObject, "obj_id": [collectId]] as AnyObject) as? [String : AnyObject], isNeedUserTokrn: true, isShowErrorStatuMsg: true,successHandler: {data, status, msg in
            if finishHandler != nil{
                self.deleteOne(section, row: row,finishHandler: finishHandler)
            }else{
                self.deleteOne(section, row: row)
            }
        }){Void in }
    }
    
    
    ///提交编辑 type: 0:修改数量(点击完成按钮) 1:删除 2:移到喜欢并删除
    override func commitEdit(_ type:Int){
        
        if type == 0{
            //上传本地数据
            var gids = [Int]()
            var subIds = [Int]()
            var nums = [Int]()
            
            for itemAry in allBasketDataArray{
                for item in itemAry{
                    gids.append(item.goodsId)
                    nums.append(item.selectedGoodsSubModelCount)
                    if let  model = item.selelctedGoodsSubModel{
                        subIds.append(model.id)
                    }else{
                        subIds.append(0)
                    }
                }
            }
            
            self.requestManager.basketUpdate(self.hudSuperView!,gids: gids, subIds: subIds, num: nums)
            
        }else if type == 1{
            
            Statistics.count(Statistics.SearchAndGiftBasket.basket_delete_all_click)
            self.deleteSome()
            
        }else if type == 2{
            
            Statistics.count(Statistics.SearchAndGiftBasket.basket_move_all_to_favor_click)
            self.collectSome()
        }
    }
    
    
    ///当用户退出时，清空数据,刷新tableview
    override func clearDataWhenLoginout(){
        self.refreshData()
    }
}



extension ShoppingBasketLoggedInManager{
    
    func giftBasketRequestManagerDidLoadList(_ manager: LMRequestManager, result: LMResultMode, list: [GoodsOfBasketModel]) {
        
        if result.status != 200 {return}
        
        _allBasketDataArray.removeAll()
        _uninvalidGoodsArray.removeAll()
        _invalidGoodsArray.removeAll()
        if list.count > 0{
            for item in list{
                if item.goods_status{
                    _uninvalidGoodsArray.append(item)
                }else{
                    _invalidGoodsArray.append(item)
                }
            }
        }
        
        if self._invalidGoodsArray.count > 0{
            _allBasketDataArray.append(_uninvalidGoodsArray)
            _allBasketDataArray.append(_invalidGoodsArray)
        }else{
            if _uninvalidGoodsArray.count > 0{
                _allBasketDataArray.append(_uninvalidGoodsArray)
            }
        }
        
        self.dataDidLoadFinish()
    }
    
    
    func giftBasketRequestManagerDidUpdate(_ manager: LMRequestManager, result: LMResultMode) {
        self.delegate?.shoppingBasketDataManagerTableReloadData()
    }
    
    
    func networkTaskDidFail(_ networkTask: LMRequestManager, error: NSError?, msg: String?) {
        
        if networkTask.taskSign == requestManager.BASKET_LIST{
            self.dataDidLoadFinish()
        }
//        self.delegate?.shoppingBasketDataManagerTableReloadData()
    }
    
}
