//
//  ShoppingBasketBadgeIcon.swift
//  Limi
//
//  Created by Richie on 16/4/11.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//右上角礼物篮图标：自带数字小标，自刷新

import UIKit

class ShoppingBasketBadgeIcon: UIButton {
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    fileprivate let iconImage = UIImageView()
    
    fileprivate let badgeLbl = UILabel()
    
    init() {
        super.init(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        self.setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    fileprivate func refreshBageValue(){
    
        if ShoppingBasketDataManager.shareManager().totalGoodsCount == 0{
            self.badgeLbl.isHidden = true
        }else{
            self.badgeLbl.isHidden = false
            if ShoppingBasketDataManager.shareManager().totalGoodsCount > 9{
                self.badgeLbl.font = UIFont.systemFont(ofSize: Constant.common_F7_font)
            }else{
                self.badgeLbl.font = UIFont.systemFont(ofSize: Constant.common_F5_font)
            }
            self.badgeLbl.text = "\(ShoppingBasketDataManager.shareManager().totalGoodsCount)"
        }
    }
    
    
    
    fileprivate func setupUI(){
        
        self.addSubview(iconImage)
        self.addSubview(badgeLbl)
        
        let badgeLbl_width:CGFloat = 18
        
        badgeLbl.clipsToBounds = true
        badgeLbl.layer.cornerRadius = badgeLbl_width/2
        badgeLbl.textColor = UIColor.white
        badgeLbl.textAlignment = NSTextAlignment.center
        badgeLbl.font = UIFont.systemFont(ofSize: Constant.common_F5_font)
        badgeLbl.backgroundColor = UIColor(rgba: Constant.common_red_color)
        
        iconImage.contentMode = UIViewContentMode.scaleAspectFill
        iconImage.image = UIImage(named: "common_basket_icon")
        
        iconImage.snp_makeConstraints { (make) in
            let _ = make.left.equalTo(self)
            let _ = make.bottom.equalTo(self)
            let _ = make.height.equalTo(36)
            let _ = make.width.equalTo(36)
        }
        
        self.badgeLbl.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(self)
            let _ = make.right.equalTo(self)
            let _ = make.height.equalTo(badgeLbl_width)
            let _ = make.width.equalTo(badgeLbl_width)
        }
        
        self.refreshBageValue()

        //商品数量加通知
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: Constant.shopBasketDataReloadFinish), object: nil, queue: OperationQueue.main) { [weak self](notification) -> Void in
            self?.refreshBageValue()
        }
    }
    
    deinit{
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: Constant.shopBasketDataReloadFinish), object: nil)
    }
}
