//
//  ShoppingBasketLoggedOutManager.swift
//  Limi
//
//  Created by Richie on 16/4/7.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//未登录的数据管理类

import UIKit
import CoreData
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class ShoppingBasketLoggedOutManager: ShoppingBasketDataManager {

    override func refreshData(){
        self.unArchive()
    }
    
    
    override func changeGoodsNum(_ section:Int,row:Int,increase:Bool,count:Int = 0){
        
        let extra = increase ? 1 : -1
        
        if editMode{
            if section == 1{return}
            
            //未登录状态，编辑模式，不实时更新本地数据
            let item = allBasketDataArray[section][row]
            item.num = count == 0 ? item.num + extra : count
            self.delegate?.shoppingBasketDataManagerTableRefresh()
            self.delegate?.shoppingBasketDataManagerRefreshCost()
            
        }else if section != 1{
            //未登录状态，非编辑模式，实时存更新本地数据  self.reloadData(true)
            let item = allBasketDataArray[section][row]
            item.num = count == 0 ? item.num + extra : count
            self.archive()
            
            self.delegate?.shoppingBasketDataManagerTableRefresh()
            self.delegate?.shoppingBasketDataManagerRefreshCost()
        }
    }
    
    
    ///删除已选中的
    override func deleteSome(){
        
        if self.getSelectedItems().count == 0{
            return
        }
        
        for item in self.getSelectedItems(){
            item.didDelete = true
        }
        
        self.archive()
    }
    
    
    ///删除指定的
    override func deleteOne(_ section:Int,row:Int,finishHandler:(()->Void)! = nil){
        
        if allBasketDataArray.count > section{
            if allBasketDataArray[section].count <= row{return}
        }else{return}
        
        allBasketDataArray[section][row].didDelete = true
        self.archive()
    }
    
    
    ///提交编辑 type: 0:修改数量(点击完成按钮) 1:删除 2:移到喜欢并删除
    override func commitEdit(_ type:Int){
        if type == 0{
            self.archive()
        }else if type == 1{
            self.deleteSome()
        }else if type == 2{
        }
        
        delegate?.shoppingBasketDataManagerDidEditFinish(true)
    }
    
    /**
     此方法会把 allBasketDataArray 中的数据都保存到本地数据库中
     
     如果未登录，需要执行此方法
     执行时机：
     1.用户刷新数据之前、
     2.礼物篮消失之前
     3.程序即将进入后台之前
     */
    ///数据归档
    
    override func saveData(){
        self.archive(false)
    }
    
    fileprivate func archive(_ shouldRefresh:Bool = true){
        //此处应在线程中进行
        if UserInfoManager.didLogin{return}
        
        for itemAry in allBasketDataArray{
            for item in itemAry{
                for sitem in dataSource{
                    if sitem.goodInfo?.gid?.intValue == item.goodsId{
                        //如果有属性，需要属性对应
                        print(sitem.selectedAttrCollection?.id)
                        print(item.goods_sub_id)
                        if let sattribute = sitem.selectedAttrCollection{
                            if sattribute.id?.intValue == item.goods_sub_id{
                                
                                if item.didDelete{
                                    //删除数据
                                    CoreDataManager.shared.managedObjectContext.delete(sitem as NSManagedObject)
                                    CoreDataManager.shared.save()
                                    //self.dataView.tableview.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
                                }else{
                                    //更新数量
                                    if sitem.planCount != Int64(item.selectedGoodsSubModelCount){
                                        sitem.planCount = Int64(item.selectedGoodsSubModelCount)
                                        CoreDataManager.shared.save()
                                    }
                                }
                            }
                            
                        }else{
                            if item.didDelete{
                                //删除数据
                                CoreDataManager.shared.managedObjectContext.delete(sitem as NSManagedObject)
                                CoreDataManager.shared.save()
                                //self.dataView.tableview.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
                            }else{
                                //更新数量
                                if sitem.planCount != Int64(item.selectedGoodsSubModelCount){
                                    sitem.planCount = Int64(item.selectedGoodsSubModelCount)
                                    CoreDataManager.shared.save()
                                }
                            }
                        }
                    }
                }
            }
        }
        
        if shouldRefresh{
            self.refreshData()
        }
    }
    
    
    ///解归档
    fileprivate func unArchive(_ finishHandler:(()->Void)! = nil){
        // 此处应放在线程中处理
        _uninvalidGoodsArray.removeAll()
        _allBasketDataArray.removeAll()
        _invalidGoodsArray.removeAll()
        let request: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "BasketItem")
        request.sortDescriptors = [NSSortDescriptor(key: "insertDate" , ascending: true)]
        
        CoreDataManager.shared.executeFetchRequest(request) { results in
            
            if results?.count > 0{
                for index in 0 ..< results!.count{
                    
                    let basketItem = results![index] as! BasketItem
                    
                    self._dataSource.append(basketItem)
                    
                    let goodsOfBasketModel = GoodsOfBasketModel()
                    
                    let goodsModel : GoodsModel = GoodsModel()
                    
                    goodsModel.gid = Int(basketItem.goodInfo!.gid!)!
                    goodsModel.discount_price = Double(basketItem.goodInfo!.discountPrice!)!
                    goodsModel.goods_image = basketItem.goodInfo!.goodsImage!
                    goodsModel.goods_name = basketItem.goodInfo!.goodsName!
                    goodsModel.goods_num = Int(basketItem.goodInfo!.totalNum)
                    
                    let goodsSubModel : GoodsSubModel = GoodsSubModel()
                    
                    if basketItem.selectedAttrCollection != nil{
                        goodsSubModel.attr_item_ids = basketItem.selectedAttrCollection!.attrItemIds!
                        goodsSubModel.Goods_num = Int(basketItem.selectedAttrCollection!.goodsNum!)!
                        goodsSubModel.price = Double(basketItem.selectedAttrCollection!.price!)!
                        goodsSubModel.id = Int(basketItem.selectedAttrCollection!.id!)!
                    }
                    
                    goodsModel.setSelectedSpecification(goodsSubModel, selectedGoodsSubModelCount: Int(basketItem.planCount))
                    
                    goodsOfBasketModel.num = goodsModel.selectedGoodsSubModelCount
                    goodsOfBasketModel.goods_id = goodsModel.gid
                    
                    if let id = goodsModel.selelctedGoodsSubModel?.id{
                        goodsOfBasketModel.goods_sub_id = id
                    }
                    goodsOfBasketModel.goods_status = true
                    
                    goodsOfBasketModel.price = goodsModel.currentPrice
                    
                    if let id = goodsModel.selelctedGoodsSubModel?.id{
                        goodsOfBasketModel.goods_sub_id = id
                    }
                    
                    goodsOfBasketModel.goods = goodsModel
                    
                    self._uninvalidGoodsArray.append(goodsOfBasketModel)
                }
                
                if self.uninvalidGoodsArray.count > 0{
                    self._allBasketDataArray.append(self.uninvalidGoodsArray)
                }
            }
            
            self.dataDidLoadFinish()
            
            if finishHandler != nil{
                finishHandler()
            }
        }
    }
    
    
    ///当用户退出时，清空数据,刷新tableview
    override  func clearDataWhenLoginout(){
        self.refreshData()
    }
    
    override func uploadDataWhenLogin() {
        if self.allBasketDataArray.count == 0{
            ShoppingBasketDataManager.shareManager().reloadData(true)
            return}
        
        //上传本地数据
        var gids = [Int]()
        var subIds = [Int]()
        var nums = [Int]()
        
        for itemAry in allBasketDataArray{
            for item in itemAry{
                gids.append(item.goodsId)
                nums.append(item.selectedGoodsSubModelCount)
                if let  model = item.selelctedGoodsSubModel{
                    subIds.append(model.id)
                }else{
                    subIds.append(0)
                }
            }
        }
        
        self.requestManager.basketAdd(self.hudSuperView!,gids: gids, subIds: subIds, num: nums)
        
        //删除本地数据
        for sitem in dataSource{
            //删除数据
            CoreDataManager.shared.managedObjectContext.delete(sitem as NSManagedObject)
            CoreDataManager.shared.save()
        }
    }
    
    override func giftBasketRequestManagerDidAdd(_ manager: LMRequestManager, result: LMResultMode) {
        ShoppingBasketDataManager.shareManager().reloadData(true)
    }
    
    func networkTaskDidFail(_ networkTask: LMRequestManager, error: NSError?, msg: String?) {
//        ShoppingBasketDataManager.shareManager().reloadData(true)
    }
    
}

