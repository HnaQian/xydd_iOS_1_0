//
//  ShoppingBasketViewController.swift
//  Limi
//
//  Created by ZhiQiangMi on 16/2/29.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//礼物篮

import UIKit
import CoreData


class ShoppingBasketViewController: BaseViewController {
    
    private let NullGoodsSelectedTupe = "您还未选择任何商品"
    
    fileprivate let ShoppingBasket_CELL_HEIGHT:CGFloat = 140 * Constant.ScreenSize.SCREEN_HEIGHT_SCALE
    
    //空视图
    fileprivate var emptyView = EmptyView()
    
    //礼物篮列表视图
    fileprivate var dataView = DataView()
    
    //推荐礼物视图
    fileprivate var recommandView = RecommendGoodsBanner(style: 1, frame: CGRect.zero)
    
    //底部工具栏
    fileprivate var bottomToolsBar = BottomBarTool()
    
    //数据管理
    var dataManager: GiftBasketDataManager{
        return ShoppingBasketDataManager.shareManager()
    }
    
    //是否应该显示tableview的footer
    fileprivate var shoudldShowTableFooter = false
    
    //是否是编辑模式
    fileprivate var shoppingEditMode:Bool{return _shoppingEditMode}
    fileprivate var _shoppingEditMode = false
    
    fileprivate var isrefreshing:Bool{return _isrefreshing}
    fileprivate var _isrefreshing = false
    
    //失效提示Label
    fileprivate lazy var invalidTipeLabel:UILabel = {
        let sectionLabel = UILabel()
        sectionLabel.frame = CGRect(x: 0, y: 10, width: Constant.ScreenSize.SCREEN_WIDTH, height: 20)
        sectionLabel.backgroundColor = UIColor(rgba: Constant.common_background_color)
        sectionLabel.textColor = UIColor.black
        sectionLabel.font = UIFont.systemFont(ofSize: 13)
        sectionLabel.textAlignment = .center
        sectionLabel.text = "由于活动结束或库存不足，以下商品已失效。"
        return sectionLabel
    }()
    
    fileprivate var isShowBackItem : Bool = true
    
    var billViewSelectedAll:Bool{return _billViewSelectedAll}
    var editViewSelectedAll:Bool{return _editViewSelectedAll}
    
    var _billViewSelectedAll = false
    var _editViewSelectedAll = false
    
    fileprivate var shouldShowInvalidGoods:Bool{return _shouldShowInvalidGoods}
    fileprivate var _shouldShowInvalidGoods = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()
        self.setFunction()
        
        self.gesturBackEnable = false
        
         self.dataView.tableview.delegate = self
        
        dataView.recommendView.delegate = self
        recommandView.delegate = self
        
       self.shouldAddHeadFresh(true)
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: Constant.shopBasketShouldShowInvalidGoods), object: nil, queue: OperationQueue.main) { (notification) -> Void in
            self._shouldShowInvalidGoods = true
        }
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: Constant.notification_login_success), object: nil, queue: OperationQueue.main) { (notification) -> Void in
            self._shouldShowInvalidGoods = true
        }
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.dataManager.delegate = self
        self.dataManager.tempView = self.view
        
        self.dataView.tableview.dataSource = self.dataManager
        
        self.dataView.tableview.beginHeaderRefresh()
//        self.shoppingQuitEditMode(5)
        
        self._isrefreshing = true
        self.dataManager.reloadData()
        IQKeyboardManager.shared().shouldResignOnTouchOutside = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        self.shoppingQuitEditMode(5)
        
        IQKeyboardManager.shared().shouldResignOnTouchOutside = false
    }
    
    
    //推荐商品横向
    func reloadRecommandData(){
        recommandView.loadRecommendGoodsList("giftbasket")
        self.dataView.recommendView.loadRecommendGoodsList("giftbasket")
    }
    
    //bottombar的一些动作
    func setFunction(){
        
        //支付工具栏
        bottomToolsBar.billActionHandler { [weak self](actionType) in
            if actionType == 1{
                //全选
                self?.dataManager.switchSelectedAll(!self!.billViewSelectedAll)
            }else{
                //提交订单
                if self?.dataManager.getSelectedItems().count == 0{
                    Utils.showError(context: self!.view, errorStr: "您还未选择任何商品")
                }else{
                    self?.billingBtnClick()
                }
            }
        }
        
        //编辑工具栏
        bottomToolsBar.editActionHandler { [weak self](actionType) in
            if actionType == 1{
                //全选
                self?.dataManager.switchSelectedAll(!self!.editViewSelectedAll)
                
            }else if actionType == 2{
                //收藏
                if self?.dataManager.getSelectedItems().count == 0{
                   Utils.showError(context: self!.view, errorStr: "您还未选择任何商品")
                }else{
                    if UserInfoManager.didLogin{
                        self?.shoppingQuitEditMode(2)
                    }else{
                        self?.jumpToLogin(Constant.InLoginType.defaultType, tipe: nil)
                    }
                }
                
            }else{
                //移除
                if self?.dataManager.getSelectedItems().count == 0{
                    Utils.showError(context: self!.view, errorStr: "您还未选择任何商品")
                }else{
                    self?.deleteGood()
                }
            }
        }
        
        self.emptyView.collectLabel?.interactionDelegate = self
        self.emptyView.shoppingLabel?.interactionDelegate = self
    }
    
    
    
    //刷新“推荐礼物”栏的显示
    fileprivate func refreshRecommandView(){
        
        if self.dataManager.allBasketDataArray.count > 0{
//            self.emptyView.hidden = true
            self.dataView.tableview.tableHeaderView = nil
            if self.navigationItem.rightBarButtonItem == nil{
                self.navigationItem.rightBarButtonItem = CommonWidgets.NavigationRightBtn.newRightBtn("编辑",target: self, action: #selector(ShoppingBasketViewController.editBtnAction(_:)))}
            
            self.recommandView.transform = CGAffineTransform(translationX: 0, y: 0)
            self.bottomToolsBar.transform = CGAffineTransform(translationX: 0, y: 0)
            self.bottomToolsBar.alpha = 1
        }else{
            
//            self.emptyView.hidden = false
            self.dataView.tableview.tableHeaderView = self.emptyView
            self.navigationItem.rightBarButtonItem = nil
            
            self.recommandView.transform = CGAffineTransform(translationX: 0, y: 43)
            self.bottomToolsBar.transform = CGAffineTransform(translationX: 0, y: 43)
            self.bottomToolsBar.alpha = 0
        }
        

        if shoppingEditMode{
            dataView.hiddenFooter(true, editMode: shoppingEditMode)
            if recommandView.alpha == 1{recommandView.disPlay(false)}
            
        }else{
            if  self.getTableViewContentHeight() > self.getTableViewHeight(){
                dataView.hiddenFooter(false, editMode: shoppingEditMode)
                recommandView.disPlay(false)
                
            }else{
                
                recommandView.disPlay(true)
                dataView.hiddenFooter(true, editMode: shoppingEditMode)
            }
        }
    }
    
    //更新是否全选了
    fileprivate func refreshSelectedAllStatus(){
        
        if shoppingEditMode{
            _editViewSelectedAll = true
            for itemAry in dataManager.allBasketDataArray{
                for item in itemAry{
                    if item.didSelected == false{_editViewSelectedAll = false;break}
                }
                if !editViewSelectedAll{break}
            }
            
            bottomToolsBar.editView.didSelectedAll = editViewSelectedAll
            
        }else{

            if dataManager.uninvalidGoodsArray.count > 0{
                
                _billViewSelectedAll = true
                
                for item in dataManager.uninvalidGoodsArray{
                    if item.didSelected == false{_billViewSelectedAll = false;break}
                }
            
                bottomToolsBar.billView.didSelectedAll = billViewSelectedAll
                
            }else{
                bottomToolsBar.billView.didSelectedAll = false
            }
        }
    }
    

    //进入编辑模式
    fileprivate func shoppingEnterEditMode(){

        _shoppingEditMode = true
        
        bottomToolsBar.openEditMode()
        //进入编辑模式，删除下拉刷新功能
        self.shouldAddHeadFresh(false)
        self.refreshRecommandView()
        self.dataManager.enterEditMode()
        
        self.navigationItem.rightBarButtonItem = CommonWidgets.NavigationRightBtn.newRightBtn("完成",target: self, action: #selector(ShoppingBasketViewController.editBtnAction(_:)))
    }
    
    
    fileprivate func shouldAddHeadFresh(_ isAdd:Bool){
        
        if isAdd{
            self.dataView.tableview.removeHeaderRefresh()
            self.dataView.tableview.addHeaderRefresh { [weak self] in
                if !(self?.isrefreshing)!{
                    self?.dataManager.reloadData()
                    self?.reloadRecommandData()
                }
            }
        }else{
            self.dataView.tableview.endHeaderRefresh()
            self.dataView.tableview.removeHeaderRefresh()
        }
    }

    //退出编辑模式 commitType: 0:修改数量(点击完成按钮) 1:删除 2:移到喜欢并删除 5:不做任何操作
    fileprivate func shoppingQuitEditMode(_ commitType:Int){
        
        //退出编辑模式，添加下拉刷新功能
        self.shouldAddHeadFresh(true)
        
        if !shoppingEditMode{return}
        _shoppingEditMode = false
        bottomToolsBar.closeEditMode()
        
        self.refreshRecommandView()
        self.dataManager.quitEditMode(commitType)
        
        self.navigationItem.rightBarButtonItem = CommonWidgets.NavigationRightBtn.newRightBtn("编辑",target: self, action: #selector(ShoppingBasketViewController.editBtnAction(_:)))
    }
    
    
    /**
     提交订单按钮
     */
    func billingBtnClick(){
        
        Statistics.count(Statistics.SearchAndGiftBasket.basket_account_click)
        
        if !UserInfoManager.didLogin{
            self.jumpToLogin(Constant.InLoginType.defaultType, tipe: nil)
            return
        }
        
        if self.dataManager.getSelectedItems().count == 0{
            Utils.showError(context: self.view, errorStr: NullGoodsSelectedTupe)
            return
        }
        
        let ogvc = OrderGeneratorViewControllerV1.orderGenerator(self.dataManager.getSelectedItems(), sourceType: 2)
        ogvc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(ogvc, animated: true)
    }
    
    
    /**
     删除已选择的所有礼物
     */
    func deleteGood(){
        
        if self.dataManager.getSelectedItems().count == 0{
            Utils.showError(context: self.view, errorStr: NullGoodsSelectedTupe)
            return
        }
        
        LCActionSheet(title: "确认删除所选礼物吗？", buttonTitles: ["确定"], cancelTitle: "取消", redButtonIndex: 0) { (buttonIndex) -> Void in
                if buttonIndex == 1{
                    self.shoppingQuitEditMode(1)
                }
            }.show()
    }
    
    
    

    //编辑按钮
    func editBtnAction(_ item:UIButton){
        
        if item.titleLabel?.text == "编辑"{
            
            if !self.dataView.tableview.isHeaderRefreshing{
                self.shoppingEnterEditMode()
            }
            
        }else{
            self.shoppingQuitEditMode(0)
//            dataManager.switchSelectedAll(true)
        }
    }
    
    
    //获取tableview的内容高度
    func getTableViewContentHeight() ->CGFloat{
        
        var extraHeight:CGFloat = 0
        
        if self.dataManager.allBasketDataArray.count == 2{
            extraHeight = 40
        }
        
        return CGFloat(dataManager.invalidGoodsArray.count + dataManager.uninvalidGoodsArray.count) * ShoppingBasket_CELL_HEIGHT + extraHeight
    }
    
    //获取tableview的高度
    func getTableViewHeight() ->CGFloat{
        
        var selfHeight:CGFloat = 0
        
        var tableview_height:CGFloat = 0
        
        if isShowBackItem{
            selfHeight = Constant.ScreenSize.SCREEN_HEIGHT - MARGIN_20 - MARGIN_44
        }else{
            selfHeight = Constant.ScreenSize.SCREEN_HEIGHT - MARGIN_20 - MARGIN_44 - MARGIN_49
        }
        
        tableview_height = selfHeight - bottomToolsBar.iheight
        
        return tableview_height
    }
    
    
    func refreshBageNum(){
        
        if let nav = self.navigationController,
            let tabVC = nav.tabBarController{
                tabVC.index(ofAccessibilityElement: nav)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit{
    
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: Constant.shopBasketShouldShowInvalidGoods), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: Constant.notification_login_success), object: nil)
    }
}


//MARK: delegate :RecommendGoodsBannerDelegate,ShoppingBasketDataManagerDelegate
extension ShoppingBasketViewController:RecommendGoodsBannerDelegate,ShoppingBasketDataManagerDelegate,LinkLabelInteractionDelegate{
 
    func recommendGoodsBannerdidSelectedItem(_ data:GoodsModel){

        if data.gid != 0{
            
            Statistics.count(Statistics.SearchAndGiftBasket.basket_recommend_gift_click, andAttributes: ["goodsId":"\(data.gid)"])
            EventDispatcher.dispatch("local://goods_item", params: ["id": data.gid as AnyObject], onNavigationController: self.navigationController)
        }
    }
    
    
    func linkLabelDidSelectLink(linkLabel: LinkLabel, url: URL) {
        if url.host == "shopping.com"{
            
            Statistics.count(Statistics.SearchAndGiftBasket.basket_selectgift_click)
            
            let userInfo: Dictionary<String,Int>! = [
                "index": 2,
                ]

            NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.show_rootController), object: nil, userInfo: userInfo)
        }
        else if url.host == "collect.com"{
            Statistics.count(Statistics.SearchAndGiftBasket.basket_like_click)
            
            if UserInfoManager.didLogin == true{
                let favList = FavListViewController()
                favList.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(favList, animated: true)
            }
            else{
                self.jumpToLogin(Constant.InLoginType.defaultType, tipe: nil)
            }
        }
    }
    
    
    
    //MARK: 数据加载完毕
    func shoppingBasketDataManagerDidLoadList(){
        
        //todo推荐礼物数据是否刷新
        //self.reloadRecommandData()
        
        //此处的两行代码顺序不可变：收起刷新动作会调起tableview的返回cell函数--只调起返回cell代理函数（因为cell的复用）
        //所以如果新数据有变化，而cell代理函数中使用的indexPath还是原来的老数据，中发生不可控的情况
        
        self.dataView.tableview.reloadData()
        self.dataView.tableview.endHeaderRefresh()
        if _isrefreshing{_isrefreshing = false}

        if self.shouldShowInvalidGoods{

            if self.dataManager.allBasketDataArray.count > 1{
                
                self.dataView.tableview.scrollRectToVisible(CGRect(x: self.dataView.tableview.ix, y: self.dataView.tableview.rect(forSection: 1).origin.y, width: self.dataView.tableview.iwidth, height: self.dataView.tableview.iheight), animated: true)
            }
            
            _shouldShowInvalidGoods = false
        }
        
        
        self.refreshRecommandView()
        self.refreshSelectedAllStatus()
        self.shoppingBasketDataManagerRefreshCost()
    }
    
    
    //MARK: 刷新tableview
    func shoppingBasketDataManagerTableRefresh(){
        self.dataView.tableview.reloadData()
        
        self.refreshSelectedAllStatus()
        self.shoppingBasketDataManagerRefreshCost()
    }
    
    func shoppingBasketDataManagerTableReloadData(){
        self.dataView.tableview.beginHeaderRefresh()
    }
    
    //MARK: 编辑模式编辑完毕
    func shoppingBasketDataManagerDidEditFinish(_ success: Bool) {
        self.refreshSelectedAllStatus()
        self.refreshRecommandView()
    }
    

    //MARK: 刷新礼物篮的花费
    func shoppingBasketDataManagerRefreshCost(){
        let attributedString1 = NSMutableAttributedString(string: String(format: "共 %d 件", dataManager.getSelectedItems().count) as String)
        
        let firstAttributes = [NSForegroundColorAttributeName: UIColor.black, NSFontAttributeName: UIFont.systemFont(ofSize: 13)]
        let secondAttributes = [NSForegroundColorAttributeName: UIColor(rgba: Constant.common_red_color), NSFontAttributeName: UIFont.systemFont(ofSize: 13)]
        
        attributedString1.addAttributes(firstAttributes, range: NSMakeRange(0, 1))
        attributedString1.addAttributes(secondAttributes, range: NSMakeRange(2, attributedString1.length - 2))
        attributedString1.addAttributes(firstAttributes, range: NSMakeRange(attributedString1.length - 1, 1))
        
        self.bottomToolsBar.billView.totalLabel.attributedText = attributedString1
        self.bottomToolsBar.billView.priceLabel.text = String(format: "￥%.2f", dataManager.totalCost)
    }
}


//MARK: delegate :UITableViewDelegate
extension ShoppingBasketViewController:UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        return section == 1 ? 40 : 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        return section == 1 ? self.invalidTipeLabel : nil
    }
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
    {
        var null = "         "
        if Constant.DeviceType.IS_IPHONE_5{
            null = "       "
        }else if Constant.DeviceType.IS_IPHONE_6P{
            null = "          "
        }
        
        
        let collect = UITableViewRowAction(style: .default, title:null) { action, index in
            
            Statistics.count(Statistics.SearchAndGiftBasket.basket_move_to_favor_click)

            if UserInfoManager.didLogin == false{
                self.jumpToLogin(Constant.InLoginType.defaultType, tipe: nil)
                return}
            self.dataManager.collectOne((indexPath as NSIndexPath).section,row:(indexPath as NSIndexPath).row,finishHandler:nil)
        }
        
        
        let delete = UITableViewRowAction(style: .default, title: null) { action, index in
            
            Statistics.count(Statistics.SearchAndGiftBasket.basket_delete_gift_click)
            
            self.dataManager.deleteOne((indexPath as NSIndexPath).section,row:(indexPath as NSIndexPath).row,finishHandler:nil)
        }
        
        collect.backgroundColor = UIColor(rgba:"#D5D5D5")
        delete.backgroundColor = UIColor(rgba:Constant.common_red_color)
        
        return [delete, collect]
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return ShoppingBasket_CELL_HEIGHT
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if !self.shoppingEditMode{
            EventDispatcher.dispatch("local://goods_item", params: ["id": dataManager.allBasketDataArray[(indexPath as NSIndexPath).section][(indexPath as NSIndexPath).row].goods_id as AnyObject], onNavigationController: self.navigationController)}
    }
    
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        IQKeyboardManager.shared().resignFirstResponder()
    }    
    
}


//MARK: SetUpUI()
extension ShoppingBasketViewController{
    
    
    fileprivate func setupUI(){
        
        self.navigationItem.title = "礼物篮"
        
        self.view.backgroundColor = UIColor.white
        
        if let nav = self.navigationController{
            if nav.viewControllers.first == self{
                self.navigationItem.leftBarButtonItem = nil
                self.isShowBackItem = false
            }
        }
        
        /*
         布局：
         三段式布局：
         第一段：tableview + emptyView
         第二段：推荐礼物View
         第三段：bottombartool
         
         */
        
        recommandView.recommendTipe = "推荐礼物"
        recommandView.loadRecommendGoodsList("giftbasket")
        
        self.navigationItem.rightBarButtonItem = CommonWidgets.NavigationRightBtn.newRightBtn("编辑",target: self, action: #selector(ShoppingBasketViewController.editBtnAction(_:)))
        
        
        let _ = [dataView,bottomToolsBar].map{self.view.addSubview($0)}
        
        dataView.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(self.view)
            let _ = make.left.equalTo(self.view)
            let _ = make.right.equalTo(self.view)
            let _ = make.bottom.equalTo(self.view).offset(-49)
        }
        
//        emptyView.snp_makeConstraints { (make) in
//            make.top.equalTo(self.view)
//            make.left.equalTo(self.view)
//            make.right.equalTo(self.view)
//            make.bottom.equalTo(recommandView.snp_top).offset(bottomToolsBar.iheight)
//        }
        
        bottomToolsBar.snp_makeConstraints { (make) in
            
            if isShowBackItem{
                let _ = make.bottom.equalTo(self.view)
            }else{
                let _ = make.bottom.equalTo(self.view).offset(-MARGIN_49)
            }

            let _ = make.left.equalTo(self.view)
            let _ = make.right.equalTo(self.view)
            let _ = make.height.equalTo(bottomToolsBar.iheight)
        }
    }
    
    
    //MARK: 购物车的礼物列表视图
    class DataView: UIView {
        
        fileprivate let tableview = UITableView()
        fileprivate let recommendView = RecommendGoodsBanner(style: 1, frame: CGRect.zero)//推荐商品
        
        func hiddenFooter(_ hidden:Bool,editMode:Bool){
            if editMode{
                tableview.tableFooterView = nil
            }else{
                
                if recommendView.goodsList.count > 0{
                    if tableview.tableFooterView == nil{
                        tableview.tableFooterView = nil
                    }
                }else{
                    tableview.tableFooterView = nil
                }
                
                recommendView.disPlay(!hidden)
            }
        }
        
        
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            self.backgroundColor = UIColor.white
            
            self.tableview.showsVerticalScrollIndicator = false
            self.tableview.register(UINib(nibName: "BasketItemCell", bundle: nil), forCellReuseIdentifier: "BasketItemCell")
            tableview.separatorStyle = UITableViewCellSeparatorStyle.none
            
            self.addSubview(self.tableview)
            tableview.snp_makeConstraints { (make) in
                let _ = make.edges.equalTo(self)
            }
            
            recommendView.recommendTipe = "推荐礼物"
//            recommendView.iheight = recommendView.iheight + MARGIN_10 * 4
            recommendView.alpha = 0
            
            tableview.tableFooterView = nil

            recommendView.loadRecommendGoodsList("giftbasket")
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    //MARK: 没有礼物的视图
    class EmptyView: UIView {
        let emptyIcon: UIImageView = UIImageView(image: UIImage(named: "basket_icon_empty"))
        var shoppingLabel: LinkLabel?
        var collectLabel: LinkLabel?
        
        init() {
            super.init(frame: CGRect.zero)
            
            self.backgroundColor = UIColor.white
            self.clipsToBounds = true
            
            let shoppingText = "啊噢！你的礼物篮还是空的，赶紧去挑礼物吧"
            let shoppingFullRange = NSMakeRange(0, (shoppingText as NSString).length)
            let shoppingLinkRange = (shoppingText as NSString).range(of: "挑礼物")
            let shoppingAttributedString = NSMutableAttributedString(string: shoppingText)
            shoppingAttributedString.addAttribute(NSFontAttributeName, value: UIFont.systemFont(ofSize: (Constant.ScreenSize.SCREEN_WIDTH >= 375 ? 15 : 13)), range: shoppingFullRange)
            shoppingAttributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.black, range: shoppingFullRange)
            shoppingAttributedString.addAttribute(NSLinkAttributeName, value: URL(string: "https://shopping.com")!, range: shoppingLinkRange)
            
            let linkTextAttributes = [
                NSUnderlineStyleAttributeName: NSNumber(value: NSUnderlineStyle.styleSingle.rawValue as Int),
                NSForegroundColorAttributeName: UIColor(rgba: Constant.common_red_color)
            ]
            
            let highlightedLinkTextAttributes = [
                NSUnderlineStyleAttributeName: NSNumber(value: NSUnderlineStyle.styleSingle.rawValue as Int),
                NSForegroundColorAttributeName: UIColor.red
            ]
            shoppingLabel = LinkLabel()
            shoppingLabel?.textAlignment = .center
            shoppingLabel!.attributedText = shoppingAttributedString
            shoppingLabel!.linkTextAttributes = linkTextAttributes
            shoppingLabel!.highlightedLinkTextAttributes = highlightedLinkTextAttributes
            
            
            let collectText = "或去看看 我喜欢的"
            let collectFullRange = NSMakeRange(0, (collectText as NSString).length)
            let collectLinkRange = (collectText as NSString).range(of: "我喜欢的")
            let collectAttributedString = NSMutableAttributedString(string: collectText)
            collectAttributedString.addAttribute(NSFontAttributeName, value: UIFont.systemFont(ofSize: (Constant.ScreenSize.SCREEN_WIDTH >= 375 ? 15 : 13)), range: collectFullRange)
            collectAttributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.black, range: collectFullRange)
            collectAttributedString.addAttribute(NSLinkAttributeName, value: URL(string: "https://collect.com")!, range: collectLinkRange)
            
            collectLabel = LinkLabel()
            collectLabel?.textAlignment = .center
            collectLabel!.attributedText = collectAttributedString
            collectLabel!.linkTextAttributes = linkTextAttributes
            collectLabel!.highlightedLinkTextAttributes = highlightedLinkTextAttributes
            
            
            self.addSubview(emptyIcon)
            self.addSubview(shoppingLabel!)
            self.addSubview(collectLabel!)
            
            emptyIcon.snp_makeConstraints { (make) -> Void in
                let _ = make.centerX.equalTo(self)
                let _ = make.top.equalTo(70 * Constant.ScreenSize.SCREEN_HEIGHT_SCALE)
                
                let _ = make.width.equalTo(Constant.ScreenSize.SCREEN_WIDTH >= 375 ? 85 : 60)
                let _ = make.height.equalTo(Constant.ScreenSize.SCREEN_WIDTH >= 375 ? 85 : 60)
            }
            
            shoppingLabel!.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(self).offset(20)
                let _ = make.right.equalTo(self).offset(-20)
                let _ = make.top.equalTo(emptyIcon.snp_bottom).offset(10)
            }
            
            collectLabel!.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(self).offset(20)
                let _ = make.right.equalTo(self).offset(-20)
                let _ = make.top.equalTo(shoppingLabel!.snp_bottom).offset(10)
            }
            
            self.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: 400)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    
    //底部工具栏
    class BottomBarTool:UIView{
        
        let billView = BillingView()
        let editView = EditView()
        
        fileprivate var billHandler:((_ actionType:Int)->Void)?
        fileprivate var editHandler:((_ actionType:Int)->Void)?
        
        let contentView = UIView(frame: CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: 88))
        
        init(){
            super.init(frame: CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: 44))
            editView.iy = editView.iheight
            self.backgroundColor = UIColor.white
            self.clipsToBounds = true
            self.addSubview(contentView)
            contentView.addSubview(editView)
            contentView.addSubview(billView)
            
            let line = UIView(frame: CGRect(x: 0,y: 0,width: Constant.ScreenSize.SCREEN_WIDTH,height: 0.5))
            line.backgroundColor = UIColor(rgba:Constant.common_separatLine_color)
            self.addSubview(line)
            
            self.setFunction()
        }
        
        
        fileprivate func setFunction(){
            
            billView.allSelectedBtn.addTarget(self, action: #selector(BottomBarTool.bill_allSelecteAction), for: UIControlEvents.touchUpInside)
            
            billView.billingBtn.addTarget(self, action: #selector(BottomBarTool.bill_orderGeneratorAction), for: UIControlEvents.touchUpInside)
            
            editView.allSelectedBtn.addTarget(self, action: #selector(BottomBarTool.edit_allSelecteAction), for: UIControlEvents.touchUpInside)
            
            editView.deleteBtn.addTarget(self, action: #selector(BottomBarTool.edit_deleteAction), for: UIControlEvents.touchUpInside)
            
            editView.collectBtn.addTarget(self, action: #selector(BottomBarTool.edit_collectAction), for: UIControlEvents.touchUpInside)
        }
        
        
        @objc func bill_allSelecteAction(){
            if let handler = self.billHandler{handler(1)}
        }
        
        @objc func bill_orderGeneratorAction(){
            if let handler = self.billHandler{handler(2)}
        }
        
        @objc func edit_allSelecteAction(){
            if let handler = self.editHandler{handler(1)}
        }
        
        @objc func edit_collectAction(){
            if let handler = self.editHandler{handler(2)}
        }
        
        @objc func edit_deleteAction(){
            if let handler = self.editHandler{handler(3)}
        }
        
        //actionType: 1:全选 2:立即送
        func billActionHandler(_ handler:@escaping ((_ actionType:Int)->Void)){
            self.billHandler = handler
        }
        
        //actionType: 1:全选 2:移至喜欢 3:删除
        func editActionHandler(_ handler:@escaping ((_ actionType:Int)->Void)){
            self.editHandler = handler
        }
        
        func openEditMode(){
            UIView.animate(withDuration: 0.25, animations: { 
                self.contentView.iy = -44
            }) 
        }
        
        
        func closeEditMode(){
            UIView.animate(withDuration: 0.25, animations: {
                self.contentView.iy = 0
            }) 
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    
    //MARK: 工具视图：立即送
    class BillingView: UIView {
        fileprivate var _allSelectedBtn: UIButton?
        fileprivate var _billingBtn: UIButton?
        fileprivate var _totalLabel: UILabel?
        fileprivate var _priceLabel: UILabel?
        
        var didSelectedAll = false{didSet{
            //UIImage(named: goodsModel.didSelected ? "choosed" : "noChoose")
            _allSelectedBtn?.setImage(UIImage(named: didSelectedAll == true ? "choosed" : "noChoose"), for: UIControlState())
            
            }}
        
        var allSelectedBtn: UIButton {
            get {
                if _allSelectedBtn == nil {
                    _allSelectedBtn = UIButton(type: .custom)
                    _allSelectedBtn?.titleLabel?.font = UIFont.systemFont(ofSize: 15)
                    _allSelectedBtn?.setTitle("全选", for: UIControlState())
                    _allSelectedBtn?.setTitleColor(UIColor.black, for: UIControlState())
                    _allSelectedBtn?.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
                    _allSelectedBtn?.setImage(UIImage(named: "choosed"), for: UIControlState())
                    _allSelectedBtn?.tag = 1
                }
                return _allSelectedBtn!
            }
        }
        
        var billingBtn: UIButton {
            get {
                if _billingBtn == nil {
                    _billingBtn = UIButton(type: .custom)
                    _billingBtn?.setTitle("去结算", for: UIControlState())
                    _billingBtn?.layer.masksToBounds = true
                    _billingBtn?.layer.cornerRadius = 5
                    _billingBtn?.setTitleColor(UIColor(rgba: Constant.common_white_color), for: UIControlState())
                    _billingBtn?.backgroundColor = UIColor(rgba: "#F92039")
                    
                }
                return _billingBtn!
            }
        }
        
        var totalLabel: UILabel {
            get {
                if _totalLabel == nil {
                    _totalLabel = UILabel()
                    _totalLabel?.textAlignment = .center
                    _totalLabel?.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
                    
                }
                return _totalLabel!
            }
        }
        
        var priceLabel: UILabel {
            get {
                if _priceLabel == nil {
                    _priceLabel = UILabel()
                    _priceLabel?.textAlignment = .center
                    _priceLabel?.font = UIFont.systemFont(ofSize: Constant.common_F5_font)
                    _priceLabel?.textColor = UIColor(rgba: Constant.common_red_color)
                }
                return _priceLabel!
            }
        }
        
        
        
        init() {
            
            super.init(frame: CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: 44))
            
            self.backgroundColor = UIColor.white
            
            self.addSubview(self.allSelectedBtn)
            self.addSubview(self.billingBtn)
            self.addSubview(self.totalLabel)
            self.addSubview(self.priceLabel)
            
            self.allSelectedBtn.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(self.snp_left).offset(12)
                let _ = make.centerY.equalTo(self)
                let _ = make.width.equalTo(70)
                let _ = make.height.equalTo(40)
            }
            
            self.billingBtn.snp_makeConstraints { (make) -> Void in
                let _ = make.right.equalTo(self.snp_right).offset(-MARGIN_10)
                let _ = make.centerY.equalTo(self)
                let _ = make.height.equalTo(34)
                let _ = make.width.equalTo(Constant.ScreenSize.SCREEN_WIDTH/4)
            }
            
            self.priceLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.right.equalTo(self.billingBtn.snp_left).offset(-MARGIN_4)
                let _ = make.centerY.equalTo(self)
            }
            
            self.totalLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.right.equalTo(self.priceLabel.snp_left).offset(-MARGIN_4)
                let _ = make.left.equalTo(allSelectedBtn.snp_right).offset(-MARGIN_4)
                let _ = make.centerY.equalTo(self)
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
    }
    
    
    //MARK: 编辑视图
    class EditView: UIView {
        fileprivate var _allSelectedBtn: UIButton?
        fileprivate var _deleteBtn: UIButton?
        fileprivate var _collectBtn: UIButton?
        
        var didSelectedAll = false{didSet{
            //UIImage(named: goodsModel.didSelected ? "choosed" : "noChoose")
            _allSelectedBtn?.setImage(UIImage(named: didSelectedAll == true ? "choosed" : "noChoose"), for: UIControlState())
            
            }}
        
        var allSelectedBtn: UIButton {
            get {
                if _allSelectedBtn == nil {
                    _allSelectedBtn = UIButton(type: .custom)
                    _allSelectedBtn?.setTitle("全部礼物", for: UIControlState())
                    _allSelectedBtn?.titleLabel?.font = UIFont.systemFont(ofSize: 15)
                    _allSelectedBtn?.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
                    _allSelectedBtn?.setTitleColor(UIColor.black, for: UIControlState())
                    _allSelectedBtn?.setImage(UIImage(named: "noChoose"), for: UIControlState())
                    _allSelectedBtn?.tag = 2
                }
                return _allSelectedBtn!
            }
        }
        
        var deleteBtn: UIButton {
            get {
                if _deleteBtn == nil {
                    _deleteBtn = UIButton(type: .custom)
                    _deleteBtn?.setTitle("删除", for: UIControlState())
                    _deleteBtn?.layer.masksToBounds = true
                    _deleteBtn?.layer.cornerRadius = 5
                    _deleteBtn?.setTitleColor(UIColor(rgba: Constant.common_white_color), for: UIControlState())
                    _deleteBtn?.backgroundColor = UIColor(rgba: "#F92039")
                    
                }
                return _deleteBtn!
            }
        }
        
        var collectBtn: UIButton {
            get {
                if _collectBtn == nil {
                    _collectBtn = UIButton(type: .custom)
                    _collectBtn!.layer.borderColor = UIColor(rgba: Constant.common_red_color).cgColor
                    _collectBtn!.layer.borderWidth = 0.5
                    _collectBtn?.layer.masksToBounds = true
                    _collectBtn?.layer.cornerRadius = 5
                    _collectBtn?.setTitle("移至喜欢", for: UIControlState())
                    _collectBtn?.backgroundColor = UIColor(rgba: Constant.common_white_color)
                    _collectBtn?.setTitleColor(UIColor(rgba: "#F92039"), for: UIControlState())
                }
                return _collectBtn!
            }
        }
        
        
        init() {
            super.init(frame: CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: 44))
            
            self.backgroundColor = UIColor.white
            
            self.addSubview(self.allSelectedBtn)
            self.addSubview(self.deleteBtn)
            self.addSubview(self.collectBtn)
            
            self.allSelectedBtn.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(self.snp_left).offset(12)
                let _ = make.centerY.equalTo(self)
                let _ = make.height.equalTo(40)
                let _ = make.width.equalTo(100)
            }
            
            self.deleteBtn.snp_makeConstraints { (make) -> Void in
                let _ = make.right.equalTo(self).offset(-20)
                let _ = make.centerY.equalTo(self)
                let _ = make.height.equalTo(34)
                let _ = make.width.equalTo(Constant.ScreenSize.SCREEN_WIDTH/4)
            }
            
            self.collectBtn.snp_makeConstraints { (make) -> Void in
                let _ = make.right.equalTo(self.deleteBtn.snp_left).offset(-20)
                let _ = make.centerY.equalTo(self)
                let _ = make.height.equalTo(34)
                let _ = make.width.equalTo(Constant.ScreenSize.SCREEN_WIDTH/4)
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
    }
    
   
}
