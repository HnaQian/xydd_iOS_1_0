//
//  BuyView.swift
//  Limi
//
//  Created by ZhiQiangMi on 16/3/10.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class BuyView: UIView,UITextFieldDelegate {
    
    var totalNum : Int = 0
    /// 添加按钮
    fileprivate var addGoodsButton: ChangeCountButton = {
        let addGoodsButton = ChangeCountButton(type: 1)
        return addGoodsButton
    }()
    
    /// 删除按钮
    fileprivate var reduceGoodsButton: ChangeCountButton = {
        let reduceGoodsButton = ChangeCountButton(type: 2)
        reduceGoodsButton.isHidden = false

        return reduceGoodsButton
    }()
    
    /// 购买数量文本框：可编辑
    var buyCountTextField: UITextField = {
        let buyCountLabel = UITextField()
        buyCountLabel.text = "0"
        buyCountLabel.textColor = UIColor.black
        buyCountLabel.textAlignment = NSTextAlignment.center
        buyCountLabel.font = UIFont.systemFont(ofSize: 14)
        buyCountLabel.layer.borderColor = UIColor.lightGray.cgColor
        buyCountLabel.layer.borderWidth = 0.5
        return buyCountLabel
    }()
    var buyNumber: Int{return _buyNumber}
    fileprivate var _buyNumber: Int = 0
   
    fileprivate var goodsModel:GoodsOfBasketModel!
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(addGoodsButton)
        addSubview(reduceGoodsButton)
        addSubview(buyCountTextField)
        
        addGoodsButton.addTarget(self, action: #selector(BuyView.addGoodsButtonClick), for: .touchUpInside)
        
        reduceGoodsButton.addTarget(self, action: #selector(BuyView.reduceGoodsButtonClick), for: .touchUpInside)
        
        buyCountTextField.keyboardType = UIKeyboardType.numberPad
        buyCountTextField.addTarget(self, action: #selector(BuyView.buyCountChangeAction(_:)), for: UIControlEvents.allEditingEvents)
        
        //addDoneOnKeyboardWithTarget:(nullable id)target action:(nullable SEL)action
        buyCountTextField.addDoneOnKeyboard(withTarget: self, action:#selector(BuyView.doneAction))
        
        buyCountTextField.delegate  = self
        
        addGoodsButton.snp_makeConstraints { (make) -> Void in
            let _ = make.right.equalTo(self)
            let _ = make.centerY.equalTo(self)
            let _ = make.width.equalTo(50)
            let _ = make.height.equalTo(50)
        }
        
        buyCountTextField.snp_makeConstraints { (make) -> Void in
            let _ = make.center.equalTo(self)
            let _ = make.width.equalTo(30)
            let _ = make.height.equalTo(20)
        }
        
        reduceGoodsButton.snp_makeConstraints { (make) -> Void in
            let _ = make.left.equalTo(self)
            let _ = make.centerY.equalTo(self)
            let _ = make.width.equalTo(50)
            let _ = make.height.equalTo(50)
        }
    }
 
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    func refreshData(_ goodsModel:GoodsOfBasketModel){

        self.goodsModel = goodsModel

        totalNum = goodsModel.goods.goods_num > 99 ? 99 : goodsModel.goods.goods_num
        
        if let subModel = goodsModel.goods.selelctedGoodsSubModel{
            if subModel.Goods_num != NULL_INT{
                totalNum = subModel.Goods_num > 99 ? 99 : subModel.Goods_num}
        }
        
        _buyNumber = goodsModel.selectedGoodsSubModelCount
        
        
        if goodsModel.goods_status{
            buyCountTextField.isEnabled = true
            buyCountTextField.text = "\(goodsModel.selectedGoodsSubModelCount)"
        }else{
            buyCountTextField.text = "0"
            buyCountTextField.isEnabled = false
            return
        }
        
        if _buyNumber >= self.totalNum{
            addGoodsButton.tapDisEnable()
        }else{
            addGoodsButton.tapEnable()
        }

        if _buyNumber <= 1{
            reduceGoodsButton.tapDisEnable()
        }else{
            reduceGoodsButton.tapEnable()
        }
    }
    
    fileprivate var userInfo: Dictionary<String,Int>! {
        _userInfo["row"] = goodsModel.row
        _userInfo["section"] = goodsModel.section
        
        return _userInfo
    }
    
    fileprivate var _userInfo:Dictionary<String,Int> = Dictionary()
    
    //MARK: - 增加按钮
    func addGoodsButtonClick() {
        if !goodsModel.goods_status{return}
        
        if _buyNumber >= self.totalNum {
            addGoodsButton.tapDisEnable()
            if _buyNumber == 1{
                reduceGoodsButton.tapDisEnable()
            }else{
                reduceGoodsButton.tapEnable()
            }
        }
        
//        _userInfo["count"] = 0
        NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.shop_cart_item_add), object: nil, userInfo: userInfo)
    }
    
    
    //MARK: - 减少按钮
    func reduceGoodsButtonClick() {
        if !goodsModel.goods_status{return}
        
        if _buyNumber <= 1 {
            reduceGoodsButton.tapDisEnable()
            if _buyNumber >= self.totalNum{
                addGoodsButton.tapDisEnable()
            }else{
                addGoodsButton.tapEnable()
            }
            
            return
        }
        
//        _userInfo["count"] = 0
        NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.shop_cart_item_reduce), object: nil, userInfo: userInfo)
    }
    
    
    
    @objc fileprivate func buyCountChangeAction(_ textField: UITextField){
        
        if let content = textField.text{
            if content.characters.count > 0{
                
                if content.characters.count > 2{
                    textField.text = NSString(string: content).substring(to: 2)
                    return
                }
                
                if content.intValue < 1{
                    textField.text = "1"
                }
                
                if content.intValue > totalNum{
                    textField.text = "\(totalNum)"
                }
            }
        }
    }
    
    //文本框内的数量变化监听
    func textFieldDidEndEditing(_ textField: UITextField) {
        //do nonthing
        textField.text = "\(buyNumber)"
    }
    
    
    //编辑完成
    @objc fileprivate func doneAction(){
        
        if let content = buyCountTextField.text{
            if content.characters.count > 0{
                
                //如果数值不等于0或原来的值，可以上传
                if content.intValue > 0 && content.intValue != buyNumber{

                    _userInfo["count"] = content.intValue
                    NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.shop_cart_item_add), object: nil, userInfo: userInfo)
                    
                    _userInfo["count"] = 0
                    
                }else{
                    //不合法就换还原
                    buyCountTextField.text = "\(buyNumber)"
                }
            }else{
                //不合法就换还原
                buyCountTextField.text = "\(buyNumber)"
            }
            
        }else{
            buyCountTextField.text = "\(buyNumber)"
        }
        
        buyCountTextField.resignFirstResponder()
    }
    
    
    class ChangeCountButton:UIButton{
        
        init(type:Int){
            super.init(frame:CGRect.zero)
            
            self.setupUI(type)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func tapEnable(){
            self.ctitleLable.alpha = 1
        }
        
        func tapDisEnable(){
            self.ctitleLable.alpha = 0.35
        }
        
        fileprivate let ctitleLable = UILabel()
        fileprivate let bgView = UIView()
        
        fileprivate func setupUI(_ type:Int){
            
            self.addSubview(bgView)
            self.addSubview(ctitleLable)
            bgView.backgroundColor = UIColor(rgba: Constant.common_C10_color)
            
            ctitleLable.backgroundColor = UIColor.white
            
            ctitleLable.isUserInteractionEnabled = false
            bgView.isUserInteractionEnabled = false
            
            ctitleLable.snp_makeConstraints { (make) -> Void in
                let _ = make.centerY.equalTo(self)
                
                if type == 1{
                    let _ = make.left.equalTo(self).offset(2)
                }else{
                    let _ = make.right.equalTo(self).offset(-2)
                }
                let _ = make.width.equalTo(20)
                let _ = make.height.equalTo(20)
            }
            
            bgView.snp_makeConstraints { (make) in
                let _ = make.edges.equalTo(self.ctitleLable)
            }
            
            ctitleLable.font = UIFont.systemFont(ofSize: Constant.common_F3_font)
            ctitleLable.text = type == 1 ? "+" : "-"
            ctitleLable.textAlignment = NSTextAlignment.center
            ctitleLable.textColor =  UIColor.black
            ctitleLable.layer.borderColor = UIColor.lightGray.cgColor
            ctitleLable.layer.borderWidth = 0.5
        }
    }
    
}
