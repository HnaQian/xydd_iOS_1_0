//
//  ShoppingBasketDataManager.swift
//  Limi
//
//  Created by Richie on 16/3/30.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

/*
 1.管理礼物篮的数量
 2.管理礼物篮的增删改查
 3.
 */

import UIKit
import CoreData


//所有礼物篮的数据获取和各种操作都通过 GiftbasketDataManager 实现
protocol GiftBasketDataManager:NSObjectProtocol,UITableViewDataSource {
    
    ///总花费
    var totalCost:Double{get}
    
    //礼物篮的所有数据
    var totalGoodsCount:Int{get}
    
    ///礼物蓝数据：包括失效数据和未失效数据
    var allBasketDataArray:[[GoodsOfBasketModel]]{get}
    
    ///正常数据
    var uninvalidGoodsArray:[GoodsOfBasketModel]{get}
    
    ///失效数据
    var invalidGoodsArray:[GoodsOfBasketModel]{get}
    
    //显示hud的View
    weak var tempView:UIView?{get set}
    
    //delegate
    weak var delegate:ShoppingBasketDataManagerDelegate?{get set}
    
    ///获取当前应该使用的manager 如果登录使用登录的loggedInManager，未登录使用未登录的loggedOutManager
    static func shareManager() ->GiftBasketDataManager
    
    ///获取指定的数据管理类，type 0:未登录 1:登录
    static func getManagerForType(_ type:Int) ->GiftBasketDataManager
    
    
    
    //进入编辑模式
    func enterEditMode()
    
    //退出编辑模式
    func quitEditMode(_ commitType:Int)
    
    ///当用户退出时，清空数据,刷新tableview
    func clearDataWhenLoginout()
    
    //全选或者不全选
    func switchSelectedAll(_ isAll:Bool)
    
    //刷新数据
    func reloadData(_ backGroundMode:Bool)
    
    ///删除已选中的
    func deleteSome()
    
    ///删除指定的
    func deleteOne(_ section:Int,row:Int,finishHandler:(()->Void)?)
    
    //收藏已选中的
    func collectSome()
    
    //收藏指定的
    func collectOne(_ section:Int,row:Int,finishHandler:(()->Void)?)
    
    //刷新数据
    func reloadData()
    
    //获取所有已经选择的数据
    func getSelectedItems() ->[GoodsOfBasketModel]
    //获取所有未选中的数据
    func getUnSelectedItems() ->[GoodsOfBasketModel]
}




class ShoppingBasketDataManager: NSObject,GiftBasketRequestManagerDelegate,GiftBasketDataManager {
    
    //编辑模式 ,只有选择商品和改变商品数量中需要
    var editMode:Bool{return _editMode}
    
    var totalGoodsCount:Int{get{
        //        self.reloadData()
        return invalidGoodsArray.count + uninvalidGoodsArray.count}}
    
    ///礼物蓝数据：包括失效数据和未失效数据
    var allBasketDataArray:[[GoodsOfBasketModel]]{return _allBasketDataArray}
    
    ///失效数据
    var invalidGoodsArray:[GoodsOfBasketModel]{return _invalidGoodsArray}
    
    ///正常数据
    var uninvalidGoodsArray:[GoodsOfBasketModel]{return _uninvalidGoodsArray}
    
    ///本地数据
    var dataSource:[BasketItem]{return _dataSource}
    
    ///总花费
    var totalCost:Double{return _totalCost}
    
    ///是否正在加载数据
    var isLoading:Bool {return _isLoading}
    
    weak var delegate:ShoppingBasketDataManagerDelegate?
    
    weak var tempView:UIView?
    
    weak var hudSuperView:UIView?{
        if tempView != nil{
            return tempView!
        }else{
            return UIApplication.shared.keyWindow!
        }
    }
    
    
    
    //老数据
    fileprivate var oldGoodsArray: [GoodsOfBasketModel]{return _oldGoodsArray}
    //被选中的goods ID
    fileprivate var selectedGoodsArray: [GoodsOfBasketModel]{return _selectedGoodsArray}
    //未被选择的goods ID
    fileprivate var unselectedGoodsArray: [GoodsOfBasketModel]{return _unselectedGoodsArray}
    
    var backGroundMode:Bool{return _backGroundMode}
    var _allBasketDataArray = [[GoodsOfBasketModel]]()
    var _uninvalidGoodsArray = [GoodsOfBasketModel]()
    var _invalidGoodsArray = [GoodsOfBasketModel]()
    
    fileprivate var _isLoading = false
    
    var _oldGoodsArray = [GoodsOfBasketModel]()
    var _selectedGoodsArray = [GoodsOfBasketModel]()
    var _unselectedGoodsArray = [GoodsOfBasketModel]()
    
    
    //数据库数据
    var _dataSource = [BasketItem]()
    
    fileprivate var _backGroundMode = false
    
    lazy var requestManager:GiftBasketRequestManager = {return GiftBasketRequestManager(delegate:self)}()
    
    fileprivate var _editMode = false
    
    fileprivate var _totalCost:Double{
        
        if self.getSelectedItems().count > 0{
            var total:Double = 0
            let _ = self.getSelectedItems().map{total += $0.totalCost}
            return total
        }
        
        return 0
    }
    
    
    struct ManagerStruct{
        
        //此处两个manager，在适当的时候应该需要释放一个无用的
        static var dataManager: ShoppingBasketDataManager{
            return UserInfoManager.didLogin ? loggedInManager : loggedOutManager
        }
        
        static var loggedInManager:ShoppingBasketLoggedInManager = {
            return ShoppingBasketLoggedInManager()
        }()
        
        
        static var loggedOutManager:ShoppingBasketLoggedOutManager = {
            return ShoppingBasketLoggedOutManager()
        }()
        
    }
    
    
    ///获取当前应该使用的manager 如果登录使用登录的loggedInManager，未登录使用未登录的loggedOutManager
    static func shareManager() ->GiftBasketDataManager{
        return ManagerStruct.dataManager
    }
    
    
    ///type 0:未登录 1:登录
    static func getManagerForType(_ type:Int) ->GiftBasketDataManager{
        return type == 1 ? ManagerStruct.loggedInManager : ManagerStruct.loggedOutManager
    }
    
    override init() {
        super.init()
        self.addObservation()
        //        self.customPrepare()
        
    }
    
    
    
    ///进入编辑模式
    func enterEditMode(){
        _editMode = true
        self.switchSelectedAll(false)
    }
    
    ///退出编辑模式  commitType: 0:修改数量(点击完成按钮) 1:删除 2:移到喜欢并删除 5:不做任何操作
    func quitEditMode(_ commitType:Int){
        if !editMode{return}
        _editMode = false
        
        if commitType != 5{
            self.commitEdit(commitType)
        }
    }
    
    ///选中或者不选择某一个
    func switchChooseOne(_ section:Int,row:Int){
        
        if editMode{
            allBasketDataArray[section][row].didSelected = !allBasketDataArray[section][row].didSelected
        }else{
            if section != 1{
                allBasketDataArray[section][row].didSelected = !allBasketDataArray[section][row].didSelected
            }
            
            self.refreshSelectedGoods()
        }
        
        delegate?.shoppingBasketDataManagerTableRefresh()
    }
    
    
    ///全选，或全不选
    func switchSelectedAll(_ isAll:Bool){
        
        if allBasketDataArray.count == 0{return}
        
        if editMode == true{
            for modelAry in allBasketDataArray{
                for model in modelAry{
                    model.didSelected = isAll
                }
            }
        }
        else{
            
            for model in allBasketDataArray[0]{
                model.didSelected = isAll
            }
            
            //把失效的设置为false
            if allBasketDataArray.count > 1{
                for model in allBasketDataArray[1]{
                    model.didSelected = false
                }
            }
            
            self.refreshSelectedGoods()
        }
        
        delegate?.shoppingBasketDataManagerTableRefresh()
    }
    
    
    ///获取选中的
    func getSelectedItems() ->[GoodsOfBasketModel]{
        
        var selectedItems = [GoodsOfBasketModel]()
        
        for itemAry in allBasketDataArray{
            for item in itemAry{
                if item.didSelected{
                    selectedItems.append(item)
                }
            }
        }
        
        return selectedItems
    }
    
    ///获取选未中选的
    func getUnSelectedItems() ->[GoodsOfBasketModel]{
        
        var selectedItems = [GoodsOfBasketModel]()
        
        for itemAry in allBasketDataArray{
            for item in itemAry{
                if !item.didSelected{
                    selectedItems.append(item)
                }
            }
        }
        
        return selectedItems
    }
    
    
    ///当用户登录时，上传数据,并清空本地数据
    func uploadDataWhenLogin(){}
    
    func giftBasketRequestManagerDidAdd(_ manager: LMRequestManager, result: LMResultMode) {
        print(result.msg)
        Utils.showError(context: UIApplication.shared.keyWindow!, errorStr: result.msg)
    }
    
    
    fileprivate var shouldInfocation:Bool{return (UserInfoManager.didLogin && self.isKind(of: ShoppingBasketLoggedInManager.self)) || (!UserInfoManager.didLogin && self.isKind(of: ShoppingBasketLoggedOutManager.self))}
    
    //添加监听器
    fileprivate func addObservation(){
        
        //商品数量加通知
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: Constant.shop_cart_item_add), object: nil, queue: OperationQueue.main) { (notification) -> Void in
            if !self.shouldInfocation{return}
            if let section = (notification as NSNotification).userInfo!["section"] as? Int,
                let row = (notification as NSNotification).userInfo!["row"] as? Int
            {
                if let count = (notification as NSNotification).userInfo!["count"] as? Int{
                    self.changeGoodsNum(section, row: row,increase: true,count: count)
                }else{
                    self.changeGoodsNum(section, row: row,increase: true)
                }
            }
        }
        
        //商品数量减通知
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: Constant.shop_cart_item_reduce), object: nil, queue: OperationQueue.main) { (notification) -> Void in
            
            if !self.shouldInfocation{return}
            if let section = (notification as NSNotification).userInfo!["section"] as? Int,
                let row = (notification as NSNotification).userInfo!["row"] as? Int
            {
                self.changeGoodsNum(section, row: row,increase: false)
            }
        }
        
        //选择商品
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: Constant.cart_selected), object: nil, queue: OperationQueue.main) { (notification) -> Void in
            if !self.shouldInfocation{return}
            if let section = (notification as NSNotification).userInfo!["section"] as? Int,
                let row = (notification as NSNotification).userInfo!["row"] as? Int
            {
                self.switchChooseOne(section, row: row)
            }
        }
        
        /*
         //刷新礼物篮
         ShoppingBasketDataManager.shareManager().reloadData()
         */
        
        //退出登录
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: Constant.notification_logout_success), object: nil, queue: OperationQueue.main) { (notification) -> Void in
            ShoppingBasketDataManager.shareManager().clearDataWhenLoginout()
        }
        
        //登录
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: Constant.notification_login_success), object: nil, queue: OperationQueue.main) { (notification) -> Void in
            self.uploadDataWhenLogin()
        }
        
        
        //添加到礼物篮
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: Constant.add_good_to_basket), object: nil, queue: OperationQueue.main) { (notification) -> Void in
            if !self.shouldInfocation{return}
            self.reloadData(true)
        }
        
        //Constant.add_good_to_basket
        
        //程序即将进入后台时保存数据
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: Constant.APP_LCA.WillResignActive), object: nil, queue: OperationQueue.main) { (notification) -> Void in
            if !self.shouldInfocation{return}
            self.saveData()
        }
    }
    
    
    deinit{
        for name in  [Constant.shop_cart_item_add,Constant.cart_selected,Constant.shop_cart_item_reduce,Constant.notification_login_success,Constant.notification_logout_success,Constant.add_good_to_basket,Constant.APP_LCA.WillResignActive]{
            
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: name), object: nil)
        }
    }
}


//MARK:tableView dataSource------>
extension ShoppingBasketDataManager{
    
    @objc(numberOfSectionsInTableView:) func numberOfSections(in tableView: UITableView) -> Int{
        
        return allBasketDataArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return allBasketDataArray.count > 0 ? allBasketDataArray[section].count : 0
    }
    
    
    @objc(tableView:cellForRowAtIndexPath:) func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell: BasketItemCell! = tableView.dequeueReusableCell(withIdentifier: "BasketItemCell") as? BasketItemCell
        allBasketDataArray[(indexPath as NSIndexPath).section][(indexPath as NSIndexPath).row].indexPath = indexPath
        cell.reloadWithData(allBasketDataArray[(indexPath as NSIndexPath).section][(indexPath as NSIndexPath).row])
        
        return cell
    }
    
    
    @objc(tableView:canEditRowAtIndexPath:) func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool{
        return true
    }
    
    
    
    func dataDidLoadFinish(){
        
        _isLoading = false
        NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.shopBasketDataReloadFinish), object: nil)
        
        self.refreshSelectedGoods(true)
        
        if !self.backGroundMode{
            self.delegate?.shoppingBasketDataManagerDidLoadList()
        }
    }
    
    
    //刷新礼物篮的商品选中状态
    fileprivate func refreshSelectedGoods(_ shouldReset:Bool = false){
        
        //此时觉不涉及页面刷新，只是数据处理
        
        if !shouldReset{
            
            _selectedGoodsArray.removeAll()
            for goods in self.getSelectedItems(){
                _selectedGoodsArray.append(goods)
            }
            
            return
        }
        
        if allBasketDataArray.count == 0{
            _oldGoodsArray.removeAll()
            _selectedGoodsArray.removeAll()
            
            return
        }else{
            
            if allBasketDataArray[0].count == 0{
                _oldGoodsArray.removeAll()
                _selectedGoodsArray.removeAll()
                
                return}
        }
        
        
        if oldGoodsArray.count > 0{
            
            //遍历现在的未失效数组
            for goods in allBasketDataArray[0]{
                
                for oldModel in selectedGoodsArray{
                    if goods.goodsId == oldModel.goodsId{
                        if let oldSubModel = oldModel.selelctedGoodsSubModel,
                            let subModel = goods.selelctedGoodsSubModel
                        {
                            if oldSubModel.id == subModel.id{
                                goods.didSelected = true
                                break
                            }
                        }else{
                            goods.didSelected = true
                            break
                        }
                    }
                }
                
                var isExist = false
                for oldModel in oldGoodsArray{
                    if oldModel.goodsId == goods.goodsId{
                        if let oldSubModel = oldModel.selelctedGoodsSubModel,
                            let subModel = goods.selelctedGoodsSubModel
                        {
                            if oldSubModel.id == subModel.id{
                                isExist = true
                                break
                            }
                        }else{
                            isExist = true
                            break
                        }
                    }
                }
                
                if !isExist{
                    goods.didSelected = true
                }
                
            }
            
            _selectedGoodsArray.removeAll()
            self.getSelectedItems().forEach({ (model) in
                _selectedGoodsArray.append(model)
            })
            
            _oldGoodsArray = allBasketDataArray[0]
            
        }else{
            _oldGoodsArray.removeAll()
            _selectedGoodsArray.removeAll()
            
            //如果原先没有数据
            for model in allBasketDataArray[0]{
                model.didSelected = true
            }
            
            //把失效的设置为false
            if allBasketDataArray.count > 1{
                for model in allBasketDataArray[1]{
                    model.didSelected = false
                }
            }
            
            for goods in self.getSelectedItems(){
                _selectedGoodsArray.append(goods)
                _oldGoodsArray.append(goods)
            }
        }
    }
    
    ///backGroundMode: 是否进入后台刷新模式，后台刷新模式不会调用tableview刷新列表。
    ///因为每次打开shoppingBaskertViewController都会刷新tableview，为了避免数据冲突，添加刷新模式
    func reloadData(_ backGroundMode:Bool){
        
        if self.isLoading{return}else{_isLoading = true}
        
        _backGroundMode = backGroundMode
        self.refreshData()
    }
    
    
    func reloadData(){
        reloadData(false)
    }
}


//MARK: 子类需要实现的函数
extension ShoppingBasketDataManager{
    
    func refreshData(){}
    
    ///删除已选中的
    func deleteSome(){}
    
    ///删除指定的
    func deleteOne(_ section:Int,row:Int,finishHandler:(()->Void)?){}
    
    //收藏已选中的
    func collectSome(){}
    
    //收藏指定的
    func collectOne(_ section:Int,row:Int,finishHandler:(()->Void)?){}
    
    ///提交编辑 type: 0:修改数量(点击完成按钮) 1:删除 2:移到喜欢并删除
    func commitEdit(_ type:Int){}
    
    ///当用户退出时，清空数据,刷新tableview
    func clearDataWhenLoginout(){}
    
    func saveData(){}
    
    ///改变商品数量
    func changeGoodsNum(_ section:Int,row:Int,increase:Bool,count:Int = 0){}
}


protocol ShoppingBasketDataManagerDelegate:NSObjectProtocol {
    
    ///此操作可能有数量变化
    func shoppingBasketDataManagerDidLoadList()
    
    ///此操作无数量变化，只需要tableview.reloadData()操作
    func shoppingBasketDataManagerTableRefresh()
    
    ///此操作无数量变化，只需要tableview.reloadData()操作
    func shoppingBasketDataManagerTableReloadData()
    
    ///此操作更新价格
    func shoppingBasketDataManagerRefreshCost()
    
    ///此操作与操作1类似
    func shoppingBasketDataManagerDidEditFinish(_ success:Bool)
}
