//
//  GoodsListViewController.swift
//  Limi
//
//  Created by 程巍巍 on 6/1/15.
//  Copyright (c) 2015 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class GoodsListViewController: BaseViewController,PCDropdownMenuViewDataSource, PCDropdownMenuViewDelegate {
    var isNeedActiveRefersh : Bool = true
    var sortType : Int = 0
    let dropdownMenuView = PCDropdownMenuView()
    let menuWidth: CGFloat = 100.0
    var items: [PCDropdownMenuItem] = []
    
    var colleView: CommonCollectionView!
    
    var goodsList = [JSON]()
    
    var cate_id: String = "0"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = ""
        
        // 初始化 collectionview
        colleView = CommonCollectionView.newCommonCollectionView(backView: RemindNoGoodsView(), type: GoodsFavType.fav, userVC: self)
        
        self.view.addSubview(colleView)
        colleView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.view)
            let _ = make.left.equalTo(self.view)
            let _ = make.right.equalTo(self.view)
            let _ = make.bottom.equalTo(self.view)
        }
        
        if let params : NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: eventURL?.query) as NSDictionary?{
            if let cateId: AnyObject  = params.object(forKey: "cate_id") as AnyObject?{
                self.cate_id = "\(cateId)"
            }
        }
        
        let sortButton = UIButton(type: .custom)
        sortButton.frame = CGRect(x: 0, y: 0, width: 36, height: 36)
        sortButton.setBackgroundImage(UIImage(named: "sort_icon"), for: UIControlState())
        sortButton.addTarget(self, action: #selector(GoodsListViewController.sort), for: UIControlEvents.touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: sortButton)
        

        self.colleView.collectionView.addHeaderRefresh { () -> Void in
            DispatchQueue.global().async(execute: { [weak self]() -> Void in
                self?.loadGoodsList(true)
            })
            
        }
        self.colleView.collectionView.addFooterRefresh { () -> Void in
            DispatchQueue.global().async(execute: { [weak self]() -> Void in
                self?.loadGoodsList(false)
            })
            
        }
        
        self.colleView.collectionView.beginHeaderRefresh()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func sort()
    {
        dropdownMenuView.dataSource = self
        dropdownMenuView.delegate = self
        items = [PCDropdownMenuItem(name: "按热度", icon: nil),
            PCDropdownMenuItem(name: "按新鲜度", icon: nil),PCDropdownMenuItem(name: "按价格", icon: nil),PCDropdownMenuItem(name: "默认", icon: nil)]
        var index = 0
        if sortType == 0{
            index = 3
        }
        else if sortType == 1{
            index = 1
        }
        else if sortType == 2{
            index = 0
        }
        else if sortType == 3{
            index = 2
        }
        dropdownMenuView.showWithAnimate(true,isNeedHint: true,selectedIndex: index)

    }
    
    //
    func loadGoodsList(_ refresh: Bool = true)
    {
        var params = [String: AnyObject]()
        
        if !refresh
        {
            if let id = goodsList.last?["Gid"].int
            {
                params["last_id"] = id as AnyObject?
            }
        }
        
        if let param : NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: eventURL?.query) as NSDictionary?{
            if let code  = param.object(forKey: "code") as? String{
            params["code"] = code as AnyObject?
            
            params["sort"] = self.sortType as AnyObject?
            
            let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.goods_list, parameters: params, isNeedUserTokrn: true, isShowErrorStatuMsg: true,successHandler: {data, status, msg in
            
                self.colleView.collectionView.closeAllRefresh()
                
                if status != 200 {
                    return
                }
                
                // 设置导航栏 title
                var title: String = "心意点点"
                if let string = (data["data"] as? [String: AnyObject])?["title"] as? String
                {
                    if string != ""
                    {
                        title = string
                    }
                }
                
                self.navigationItem.title = title
                if let list = JSON(data as AnyObject)["data"]["list"].array
                {
                    if refresh
                    {
                        self.colleView.collectionView.scrollRectToVisible(CGRect(x: 0, y: 0, width: 1, height: 1), animated: true)
                        self.goodsList = list
                        
                    }
                    else
                    {
                        for item in list
                        {
                            self.goodsList.append(item)
                        }
                    }
                    
                    self.colleView.goodsList = self.goodsList
                    if list.count < 20 {
                        self.colleView.collectionView.showTipe(UIScrollTipeMode.noMuchMore)
                    }
                    
                }
                else
                {
                    if !refresh
                    {
                        self.colleView.collectionView.showTipe(UIScrollTipeMode.noMuchMore)
                    }
                    else
                    {
                        self.colleView.goodsList = []
                        self.colleView.collectionView.showTipe(UIScrollTipeMode.nullData).tipeContent(Constant.TipeWords.nullData_goods)
                    }
                }
            })
            
        }
    }
    }
    
    // MARK: - PCDropdownMenuViewDataSource
    
    func numberOfRowInDropdownMenuView(_ view: PCDropdownMenuView) -> Int {
        return items.count
    }
    
    func dropdownMenuView(_ view: PCDropdownMenuView, itemForRowAtIndexPath indexPath: IndexPath) -> PCDropdownMenuItem {
        return items[(indexPath as NSIndexPath).row]
    }
    
    func dropdownMenuViewArrowImageOffset(_ view: PCDropdownMenuView) -> UIOffset {
        return UIOffset(horizontal: menuWidth - 25.0, vertical: 0.0)
    }
    
    func dropdownMenuViewContentFrame(_ view: PCDropdownMenuView) -> CGRect {
        let isPortrait = UIInterfaceOrientationIsPortrait(UIApplication.shared.statusBarOrientation)
        let x: CGFloat = Constant.ScreenSize.SCREEN_WIDTH - menuWidth - 5.0
        let y: CGFloat = isPortrait ? 64.0 : (self.navigationController?.navigationBar.frame.size.height ?? 44.0)
        return CGRect(x: x, y: y, width: menuWidth, height: 0.0)
    }
    
    func dropdownMenuViewArrowImage(_ view: PCDropdownMenuView) -> UIImage? {
        return nil
    }
    
    // MARK: - PCDropdownMenuViewDelegate
    
    func dropdownMenuViewDidSelectedItem(_ view: PCDropdownMenuView, inIndex index: Int) {
        view.hiddenWithAnimate(true)
        
       // let item = items[index]
        if index == 0{
            self.sortType = 2
        }
        else if index == 1{
            self.sortType = 1
        }
        else if index == 2{
            self.sortType = 3
        }
        else if index == 3{
            self.sortType = 0
        }
        
        self.loadGoodsList(true)
        
    }
    
    
    
}


    
