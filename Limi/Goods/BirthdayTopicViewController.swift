//
//  BirthdayTopicViewController.swift
//  Limi
//
//  Created by 程巍巍 on 6/18/15.
//  Copyright (c) 2015 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//



import UIKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


let headerLabelSpacingWithTop : CGFloat = 15
let headerLabelHeight : CGFloat = 30
let headerImageViewScaleWithWidth : CGFloat = 0.58


class BirthdayTopicViewController: BaseViewController
{

    var segmentedControl:LMSegmentedControl!

    var HeardHeight: CGFloat = 0
    
    let margin: CGFloat = 8
    
    var index : Int = 0
    var listHref: String = ""
    
    var colleView: CommonCollectionView!
    
    var headerView: HeaderView!
    
    var pageSouecr : JSON = []{
        didSet {
            
            if let children = pageSouecr.array {
                
                for child in children {
                    
                    if let name = child["attributes"]["class"].string {
                        
                        switch name
                        {
                            
                        case "classification": refreshToolBar(child)
                            
                        case "topic" : refreshHeaderView(child)
                        
                        case "newGoods" : reloadTopicNewGoodData(child)
                            
                        default: print("Birthday unsuport type \(name)")
                            
                        }
                    }
                }
            }
            
        }
    }
    var currentDataSource = [JSON]()
    var allDataSource = [String: [JSON]]()
    
    var api_: String! {
        return Constant.JTAPI.birthday_page
    }
    
    class ImageView: UIImageView {
        var href: String?
    }
    

    
    func reloadTopicNewGoodData(_ data: JSON){
        
    }
    
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
//        self.navigationItem.title = "生日专区"
        self.navigationItem.title = ""

        segmentedControl = LMSegmentedControl(frame: CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: 40), selectedHandle: { (index) -> Void in

        })

        HeardHeight = margin + ((Constant.ScreenSize.SCREEN_WIDTH - 24)/2*headerImageViewScaleWithWidth) + headerLabelHeight + margin

        
        self.headerView = HeaderView()
        self.headerView.leftImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(BirthdayTopicViewController.imageViewTaped(_:))))
        self.headerView.rightImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(BirthdayTopicViewController.imageViewTaped(_:))))

        // 初始化商品列表
        colleView = CommonCollectionView.newCommonCollectionView(backView: RemindNoGoodsView(), type: GoodsFavType.fav, userVC: self, stowBool: false, headerView: self.headerView)
        
        // 设置collectionView表头size， layout内headerReferenceSize
        (colleView.collectionView.collectionViewLayout as? UICollectionViewFlowLayout)!.headerReferenceSize = CGSize(width: Constant.ScreenSize.SCREEN_WIDTH,height: HeardHeight)
        colleView.headerView.frame.size = CGSize(width: Constant.ScreenSize.SCREEN_WIDTH,height: HeardHeight)
        
        self.view.addSubview(colleView)
        
        colleView.snp_makeConstraints { (make) -> Void in
            let _ = make.left.equalTo(self.view)
            let _ = make.right.equalTo(self.view)
            let _ = make.top.equalTo(self.view).offset(40)
            let _ = make.bottom.equalTo(self.view)
        }
        
        gcd.async(.main) {
            self.loadTempData()
            
            self.loadPageData()
        }
        
        
        self.colleView.collectionView.addHeaderRefresh { () -> Void in
            DispatchQueue.global().async(execute: { [weak self]() -> Void in
                self?.loadGoodsList((self?.listHref)!,refresh: true,isPullDown: true)
            })
        }
        
        self.colleView.collectionView.addFooterRefresh { () -> Void in
            
            DispatchQueue.global().async(execute: { [weak self]() -> Void in
                self?.loadGoodsList((self?.listHref)!,refresh: false)
            })
        }
 
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func imageViewTaped(_ sender: UITapGestureRecognizer) {
        if let href = (sender.view as? ImageView)?.href {
            EventDispatcher.dispatch(href, onNavigationController: self.navigationController)
        }
    }
    
    func loadTempData()
    {
        let tempName = Crypto.MD5(data: api_.data(using: String.Encoding.utf8, allowLossyConversion: true)!)
        
        let data = try? Data(contentsOf: URL(fileURLWithPath: NSTemporaryDirectory().stringByAppendingPathComponent(tempName)))
        
        if data == nil {return}
        self.pageSouecr = JSON(data! as AnyObject)
    }
    
    func loadPageData()
    {
        self.navigationController?.pushViewController(CouponListViewController(), animated: true)
        
        let _ = LMRequestManager.requestByDataAnalyses(.get,context:self.view, URLString: api_,isShowErrorStatuMsg:true,isNeedHud : true,successHandler: {data, status, msg in

            if status == 200
            {
                var dataDic: [String: AnyObject] = data["data"] as! [String: AnyObject]
                
                self.pageSouecr = JSON(dataDic as AnyObject)["layout"]
                
                if let children = self.pageSouecr.array {
                    
                    for child in children {
                        
                        if let name = child["attributes"]["class"].string {
                            
                            switch name
                            {
                                
                            case "classification":
                                if let children = child["children"].array {
                                    let child = children[0]
                                    if let href = child["href"].string {
                                        let url = URL(string: href)!
                                        self.getCodeWithHref(url)
                                        
                                    }
                                }
                                
                            default: print("Birthday unsuport type \(name)")
                                
                            }
                        }
                    }
                }
                
                // 缓存数据
                let tempName = Crypto.MD5(data: self.api_.data(using: String.Encoding.utf8, allowLossyConversion: true)!)
                
                do {
                    let data = try JSONSerialization.data(withJSONObject: dataDic["layout"] as! [[String: AnyObject]], options: JSONSerialization.WritingOptions())
                    try? data.write(to: URL(fileURLWithPath: NSTemporaryDirectory().stringByAppendingPathComponent(tempName)), options: [.atomic])
                } catch let error as NSError {
                    print(error)
                }
            }
        })
    }


    func loadGoodsList(_ code: String,refresh: Bool = true,isPullDown: Bool = false)
    {
        if refresh == true{
            currentDataSource.removeAll()
            if isPullDown == false{
            }
        }
        var params:[String: AnyObject] = ["code": code as AnyObject]
        
        if !refresh
        {
            if let id = currentDataSource.last?["Gid"].int
            {
                params["last_id"] = id as AnyObject?
            }
        }
        let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.goods_list, parameters: params, isNeedUserTokrn: true,isShowErrorStatuMsg:true,isNeedHud : true,successHandler: {data, status, msg in
        
                if self.colleView.collectionView.isHeaderRefreshing == true{
                    self.colleView.collectionView.endHeaderRefresh()
                }
                if self.colleView.collectionView.isFooterRefreshing == true{
                    self.colleView.collectionView.endFooterRefresh()
                }
                if status == 200 {

                    var title: String = "心意点点"
                    if let string = JSON(data as AnyObject)["data"]["title"].string
                    {
                        if string != ""
                        {
                            title = string
                        }
                    }
                    
                    self.navigationItem.title = title
                    
                    if let list = JSON(data as AnyObject)["data"]["list"].array {
                        if refresh {
                            self.allDataSource[code] = list
                            
                            self.currentDataSource = list
                            
                        }else{
                            for item in list
                            {
                                var array = self.allDataSource[code]! as [JSON]
                                
                                array.append(item)
                                
                                self.allDataSource[code] = array
                                
                                self.currentDataSource = array
                            }
                            
                        }
                        
                        self.colleView.goodsList = self.currentDataSource
                    }
                    else{
                        
                        if self.currentDataSource.count > 0
                        {
                            self.colleView.collectionView.showTipe(UIScrollTipeMode.noMuchMore)
                        }
                        else
                        {
                            if refresh
                            {
                                self.colleView.goodsList = []
                            }
                        }
                    }
                }

            })
            
    }
    
    func getCodeWithHref(_ url: URL){
        if let params : NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: url.query) as NSDictionary?{
            if let app: AnyObject  = params.object(forKey: "app") as AnyObject?{
            if "\(app)" == "1" {
                if let code = params.object(forKey: "code") as? String{
                    self.listHref = code
                    if self.allDataSource[code]?.count > 0{
                        self.colleView.collectionView.scrollRectToVisible(CGRect(x: 0, y: 0, width: 1, height: 1), animated: true)
                        self.currentDataSource = self.allDataSource[code]!
                        self.colleView.goodsList = self.currentDataSource
                        }
                    else{
                        self.loadGoodsList(self.listHref, refresh: true)
                    }
                    }
                else{
                    if let lastCom = url.path.lastPathComponent {
                        self.listHref = lastCom
                        if self.allDataSource[self.listHref]?.count > 0{
                            self.colleView.collectionView.scrollRectToVisible(CGRect(x: 0, y: 0, width: 1, height: 1), animated: true)
                            self.currentDataSource = self.allDataSource[self.listHref]!
                            self.colleView.goodsList = self.currentDataSource
                        }
                        else{
                            self.loadGoodsList(self.listHref, refresh: true)
                        }
                    }
                }
            }
        }
    }
    }

    
    func refreshToolBar(_ data: JSON) {
        let classificationTitle:NSMutableArray? = NSMutableArray()
        let classificationAction:NSMutableArray? = NSMutableArray()
        if let children = data["children"].array {
            for child in children {
                if let title = child["title"].string {
                    classificationTitle?.add(title)
                }
                if let href = child["href"].string {
                    classificationAction?.add(href)
                    
                }
            }
            segmentedControl.selectedHandle = {index in
                
                self.index = index
                if index == 0{
                    let flow = self.colleView.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
                    flow.headerReferenceSize = CGSize(width: self.colleView.collectionView.frame.size.width,height: self.HeardHeight)
                }
                else{
                    let flow = self.colleView.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
                    flow.headerReferenceSize = CGSize(width: self.colleView.collectionView.frame.size.width,height: 0)
                }
                if let url = URL(string: (classificationAction?.object(at: index) as? String)!){
                    self.getCodeWithHref(url)
                }
            }
            
            self.segmentedControl.titles = classificationTitle as AnyObject as! [String]
            
            self.view.addSubview(self.segmentedControl)
        }
    }
    
    
    func refreshHeaderView(_ data: JSON)
    {
        if let children = data["children"].array
        {
            if children.count != 2
            {
                print("严重错误，这里必须是两个 child")
            }else
            {
                if let src = children[0]["attributes"]["src"].string {
                    let imageScale : String = src + "?imageView2/0/w/" + "\(Int(((Constant.ScreenSize.SCREEN_WIDTH/2) - 9)*Constant.ScreenSize.SCALE_SCREEN))" + "/h/" + "\(Int((((Constant.ScreenSize.SCREEN_WIDTH/2) - 9)*headerImageViewScaleWithWidth)*Constant.ScreenSize.SCALE_SCREEN))"
                    
                    self.headerView.leftImageView.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "loadingDefault"))
                }
                
                if let href = children[0]["attributes"]["href"].string {
                    self.headerView.leftImageView.href = href
                }
                
                if let src = children[1]["attributes"]["src"].string {
                    let imageScale : String = src + "?imageView2/0/w/" + "\(Int(((Constant.ScreenSize.SCREEN_WIDTH/2) - 9)*Constant.ScreenSize.SCALE_SCREEN))" + "/h/" + "\(Int((((Constant.ScreenSize.SCREEN_WIDTH/2) - 9)*headerImageViewScaleWithWidth)*Constant.ScreenSize.SCALE_SCREEN))"
                    
                    self.headerView.rightImageView.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "loadingDefault"))
                }
                
                if let href = children[1]["attributes"]["href"].string {
                    self.headerView.rightImageView.href = href
                }
            }
            self.colleView.headerView = self.headerView
        }
    }
    
    
    // 表头
    class HeaderView: UIView {
        
        
        let leftImageView = ImageView()
        let rightImageView = ImageView()
        
        let margin: CGFloat = 8
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            
            let label1 = UILabel()
            label1.font = UIFont.systemFont(ofSize: 15)
            let lineView = UIView()
            
            for view in [leftImageView, rightImageView, lineView, label1] {
                self.addSubview(view)
            }
            
            for image in [leftImageView, rightImageView] {
                image.isUserInteractionEnabled = true
                image.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(BirthdayTopicViewController.imageViewTaped(_:))))
            }
            
            label1.text = "最热"
            label1.font = UIFont.systemFont(ofSize: 15)
            label1.textColor = UIColor.black
            
            label1.backgroundColor = UIColor(rgba: Constant.common_background_color)
            
            label1.textColor = UIColor.lightGray
            
            label1.textAlignment = NSTextAlignment.center
            
            lineView.backgroundColor = UIColor.lightGray
            
            lineView.alpha = 0.4
            
            leftImageView.snp_makeConstraints { (make) -> Void in

                let _ = make.top.equalTo(self).offset(self.margin)
                
               let _ =  make.left.equalTo(self).offset(self.margin)
                
                let _ = make.right.equalTo(self.snp_centerX).offset(-4)
                
                let _ = make.height.equalTo(self.leftImageView.snp_width).multipliedBy(headerImageViewScaleWithWidth)
            }
            
            rightImageView.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(self).offset(self.margin)
                
                let _ = make.right.equalTo(self).offset(-self.margin)
                
                let _ = make.left.equalTo(self.snp_centerX).offset(4)
                
                let _ = make.height.equalTo(self.rightImageView.snp_width).multipliedBy(headerImageViewScaleWithWidth)
            }
            
            
            label1.snp_makeConstraints { (make) -> Void in
                
                let _ = make.width.equalTo(50)
                
                let _ = make.height.equalTo(headerLabelHeight)
                
                let _ = make.centerY.equalTo(self.leftImageView.snp_bottom).offset(headerLabelSpacingWithTop)
                
                let _ = make.centerX.equalTo(self.leftImageView.snp_right).offset(4)
            }
            
            
            lineView.snp_makeConstraints { (make) -> Void in
                
                let _ = make.width.equalTo(self).offset(-self.margin * 2)
                
                let _ = make.height.equalTo(0.5)
                
                let _ = make.center.equalTo(label1)
            }
        }

        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        
    }

    
}

