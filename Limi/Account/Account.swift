//
//  Account.swift
//  Limi
//
//  Created by 程巍巍 on 4/30/15.
//  Copyright (c) 2015 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//


import Foundation
import CoreData
import UIKit


class UserInfoManager
{
    private static var __once: () = {
            UserInfoManager.Static.instance = UserInfoManager()
        }()
    
    struct Static{
        static var instance : UserInfoManager? = nil
        static var once : Int = 0
    }

    class func shareUserInfoManager() ->UserInfoManager{
        
        _ = UserInfoManager.__once
        
        return Static.instance!
    }
    
    class var didLogin:Bool{get{
        
        if let token = UserDefaults.standard.string(forKey: "UserToken"){
            if token.characters.count > 0{
                return true
            }
        }
        return false
        }
    }
    
    
    //MARK:更新用户信息
    func updateUserInfo(_ context:UIView,completionBlock:((_ success:Bool)->Void)? = nil)
    {
        let _ = LMRequestManager.requestByDataAnalyses(context:context,URLString: Constant.JTAPI.user_info_v2, isNeedUserTokrn: true, successHandler: {data, status, msg in
            
            if status != 200{
                if completionBlock != nil{
                    completionBlock!(false)
                }
                
                return
            }
            
            if let dataJson : JSON = JSON(data["data"]!) as JSON?{
                if let user : JSON = dataJson["user"] as JSON? {
                    let localUser = CoreDataManager.shared.getDBManager(Constant.CoreDataType.userInfoType) as? User
                    CoreDataManager.shared.deleteEntity(localUser!)
                    CoreDataManager.shared.update(user, entityType: Constant.CoreDataType.userInfoType,successHandler: {Void in
                        
                        if completionBlock != nil{
                            completionBlock!(false)
                        }
                        
                        },failureHandler: {Void in
                            if completionBlock != nil{
                                completionBlock!(false)
                            }
                        }
                        )
                }
            }
            
            
            },failureHandler:{Void in
                if completionBlock != nil
                {
                    completionBlock!(false)
                }})
    }

}
