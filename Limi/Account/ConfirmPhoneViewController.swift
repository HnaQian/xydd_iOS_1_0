//
//  ConfirmPhoneViewController.swift
//  Limi
//
//  Created by ZhiQiangMi on 15/9/1.
//  Copyright (c) 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit
import CoreData
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}



class ConfirmPhoneViewController: BaseViewController,UITextFieldDelegate {
    
    @IBOutlet var signField: UITextField!
    @IBOutlet var phoneField: UITextField!
    @IBOutlet var fetchSignCodeButton: UIButton!
    @IBOutlet var voiceCodeButton: HyperlinksButton!
    
    var phoneNo: String!
    
    fileprivate var timeout: TimeInterval = 0
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.phoneField.becomeFirstResponder()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "修改绑定手机号"
        self.navigationItem.rightBarButtonItem = CommonWidgets.NavigationRightBtn.newRightBtn("绑定", target: self, action: #selector(ConfirmPhoneViewController.confirmAction))
        self.voiceCodeButton.isHidden = true
        self.voiceCodeButton.setColor(UIColor.lightGray)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func confirmAction()
    {
        // 验证验证码是否正确
        let signCode = self.signField.text
        let phoneNo = self.phoneField.text
        if phoneNo!.isEmpty {
            Utils.showError(context: self.view, errorStr: "请输入手机号")
            return
        }
        if phoneNo?.characters.count < 11 {
            Utils.showError(context: self.view, errorStr: "请输入正确手机号")
            return
        }
        
        if signCode!.isEmpty {
            Utils.showError(context: self.view, errorStr: "请输入验证码")
            return
        }
        if phoneNo != self.phoneNo {
            Utils.showError(context: self.view, errorStr: "请保持手机号与接收验证码的手机号相同！")
            return
        }
       let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.sms_check, parameters: ["user_name": self.phoneField.text! as AnyObject, "sms_type": 3 as AnyObject, "code": signCode! as AnyObject],isShowErrorStatuMsg:true,isNeedHud : true, successHandler: {data, status, msg in
            if status == 200 {
                self.bindPhoneNum(phoneNo!,code: signCode!)
            }
            
        })
    }
    
    @IBAction func fetchSignCodeButtonClick(_ sender: AnyObject) {
        if self.phoneField.text!.isEmpty {
            Utils.showError(context: self.view, errorStr: "请输入手机号")
            return
        }
        if self.phoneField.text?.characters.count < 11 {
            Utils.showError(context: self.view, errorStr: "请输入正确手机号")
            return
        }
        self.voiceCodeButton.isHidden = false
        let user_name : AnyObject = self.phoneField.text! as AnyObject
        
        
       let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.sms, parameters: ["user_name": user_name, "sms_type": 3 as AnyObject],isShowErrorStatuMsg:true,isNeedHud : true,successHandler: {data, status, msg in
            if status == 200
            {
                
                self.phoneNo = self.phoneField.text
                self.signField.becomeFirstResponder()
                self.fetchSignCodeButton.isEnabled = false
                self.timeout = 60
                self.signCodeButtonCountDown()
            }
            
        })
    }
    
    
    func bindPhoneNum(_ phoneNo: String,code: String){
       let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.user_bind_mobile, parameters: ["user_name": phoneNo as AnyObject, "code": code as AnyObject], isNeedUserTokrn: true, isShowErrorStatuMsg: true,successHandler: {data, status, msg in
            if status == 200 {
                let request: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
                let datasource : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(request)!
                if datasource.count > 0{
                    
                    let userInfo = datasource[0] as! User
                    userInfo.user_name = phoneNo
                    
                }
                else{
                    let entity = NSEntityDescription.entity(forEntityName: "User", in: CoreDataManager.shared.managedObjectContext)
                    request.entity = entity
                    do {
                        try! CoreDataManager.shared.managedObjectContext.fetch(request)
                        let user: User = NSEntityDescription.insertNewObject(forEntityName: "User", into: CoreDataManager.shared.managedObjectContext) as! User
                        user.user_name = phoneNo
                    }
                }
                CoreDataManager.shared.save()
                Utils.showError(context: self.view, errorStr: "绑定成功")
               let _ = self.navigationController?.popViewController(animated: true)
                
            }
        })
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        if textField == self.phoneField
        {
            if range.location > 10{
                return false
            }
            
        }
        return true
        
    }
    
    func signCodeButtonCountDown() {
        if timeout < 0 {
            self.fetchSignCodeButton.isEnabled = true
            self.fetchSignCodeButton.setTitle("获取验证码", for: UIControlState())
            self.fetchSignCodeButton.setTitleColor(UIColor.black, for: UIControlState())
            return
        }
        self.fetchSignCodeButton.isEnabled = false
        let nf = NumberFormatter()
        nf.numberStyle = NumberFormatter.Style.decimal
        nf.maximumFractionDigits = 0
        nf.string(from: (timeout - 1 as NSNumber))
        let timeoutShow = "(" + nf.string(from: timeout as NSNumber)! + ")"
        self.fetchSignCodeButton.setTitle("重新获取" + timeoutShow, for: UIControlState())
        self.fetchSignCodeButton.setTitleColor(UIColor.lightGray, for: UIControlState())
        gcd.async(.main, delay: 1) {
            self.timeout = self.timeout - 1
            self.signCodeButtonCountDown()
        }
        
    }
    
    @IBAction func voiceCodeButtonClick(_ sender: AnyObject) {
        
        if self.phoneField.text!.isEmpty {
            Utils.showError(context: self.view, errorStr: "请输入手机号")
            return
        }
        if self.phoneField.text?.characters.count < 11 {
            Utils.showError(context: self.view, errorStr: "请输入正确手机号")
            return
        }
        
        if self.timeout > 0{
            Utils.showError(context: self.view, errorStr: "请" + String(Int(self.timeout)) + "秒后点我接收语音验证")
            return
        }
        
        let user_name : AnyObject = self.phoneField.text! as AnyObject
        
        
       let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.voice_sms, parameters: ["user_name": user_name, "sms_type": 3 as AnyObject],isShowErrorStatuMsg:true,isNeedHud : true,successHandler: {data, status, msg in
            if status == 200
            {
                Utils.showError(context: self.view, errorStr: "请注意接听电话")
                self.phoneNo = self.phoneField.text
                self.signField.becomeFirstResponder()
                self.fetchSignCodeButton.isEnabled = false
                self.timeout = 60
                self.signCodeButtonCountDown()
            }
            
        })
        
        
        
    }
    
    
}
