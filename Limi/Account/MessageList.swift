//
//  MessageList.swift
//  Limi
//
//  Created by ZhiQiangMi on 15/9/17.
//  Copyright (c) 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//消息中心
import UIKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}

fileprivate func <= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l <= r
  default:
    return !(rhs < lhs)
  }
}


class MessageList: BaseViewController,UITableViewDelegate, UITableViewDataSource{
    
    var rightBarButtonItem:UIBarButtonItem!
    
    var tableView: UITableView {
        return self.view as! UITableView
    }
    
    override func loadView() {
        super.loadView()
        self.view = UITableView(frame: CGRect.zero, style: UITableViewStyle.plain)
    }
    
    var msgList = [MessageListModel]()
    {
        didSet
        {
            if msgList.count == 0{
                
                self.navigationItem.rightBarButtonItem = nil
            }else{
                self.navigationItem.rightBarButtonItem = self.rightBarButtonItem
            }
            
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        tableView.beginHeaderRefresh()
        
        gcd.async(.default) {
            //用户信息
            let qyUserInfo = QYUserInfo()
            qyUserInfo.userId = UserDefaults.standard.string(forKey: "UserToken")
            qyUserInfo.data = QYLocalUserInfo.shareInstance.getLocalUserInfo()
            QYSDK.shared().setUserInfo(qyUserInfo)
        }
    }
    
    override func viewDidLoad() {
        
        
        
        super.viewDidLoad()
//        self.edgesForExtendedLayout = UIRectEdge()
        self.navigationItem.title = "消息中心"
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor(rgba: "#F2F2F2")
        tableView.backgroundView = backgroundView
        
        tableView.register(MessageCenterListCell.self, forCellReuseIdentifier: MessageCenterListCell.identifier)
        tableView.rowHeight = MessageCenterListCell.cellHeight
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        let rightButton = UIButton(type: .custom)
        rightButton.frame = CGRect(x: 0, y: 0, width: 36, height: 36)
        rightButton.setTitle("清空", for: UIControlState())
        rightButton.titleLabel?.font = UIFont.systemFont(ofSize: Constant.common_F5_font)
        rightButton.showsTouchWhenHighlighted = true
        rightButton.setTitleColor(UIColor.black, for: UIControlState())
        rightButton.addTarget(self, action: #selector(MessageList.rightClick), for: UIControlEvents.touchUpInside)
        
        self.rightBarButtonItem = UIBarButtonItem(customView: rightButton)
        
        
        weak var weakSelf = self as MessageList
        self.tableView.addHeaderRefresh { () -> Void in
            DispatchQueue.global().async(execute: { () -> Void in
                weakSelf?.loadMessage()
            })
        }
    }
    
    func rightClick(){
        if msgList.count > 0{
            let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.message_del,isNeedUserTokrn: true,isShowErrorStatuMsg: true, isNeedHud: true,successHandler: {data, status, msg in
                if status == 200
                {
                    self.msgList.removeAll(keepingCapacity: true)
                    
                    self.tableView.reloadData()
                    
                    self.tableView.tableFooterView = nil
                    
                    self.navigationItem.rightBarButtonItem = nil
                }
                
                
            })
            
        }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func loadMessage() {
        let tempString = UserDefaults.standard.string(forKey: "UserToken")
        
        let params = ["token":tempString!]
        let _ = LMRequestManager.requestByDataAnalyses(context: self.view, URLString: Constant.JTAPI.message_list,parameters:params as [String : AnyObject]?,successHandler: { (data, status, msg) in
            self.tableView.endHeaderRefresh()
            if status != 200 {return}
             gcd.async(.main) {
                if let tempArray = JSON(data as AnyObject)["data"].arrayObject {
                    self.msgList = [MessageListModel]()
                    for index in 0 ..< tempArray.count {
                        let detailData = MessageListModel(tempArray[index] as! [String:AnyObject])
                        self.msgList.append(detailData)
                    }
                }
                self.tableView.reloadData()
            }
        }){Void in
            self.tableView.endHeaderRefresh()
        }
    }
    
    
    //MARK: UITableView DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return msgList.count + 1

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        var cell: MessageCenterListCell
        cell = tableView.dequeueReusableCell(withIdentifier: MessageCenterListCell.identifier, for: indexPath) as! MessageCenterListCell
        if (indexPath as NSIndexPath).row == 0 {
            cell.titleLabel.text = "在线客服"
            cell.subTitleLabel.text = "周一至周五09:00-12:00  13:30-19:00"
            cell.typeImage.image = UIImage(named: "icon_contactService")
            if Constant.UNREAD != 0 {
                cell.tagLbl.isHidden = false
                cell.tagLbl.text = String(Constant.UNREAD)
                if Constant.UNREAD > 99 {
                    cell.tagLbl.text = "···"
                }
            }else {
                cell.tagLbl.isHidden = true
            }
            
        }else {
            cell.data = msgList[(indexPath as NSIndexPath).row - 1]
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath as NSIndexPath).row == 0 {
            if UserInfoManager.didLogin{
                let qyvc = QYSDK.shared().sessionViewController()
                qyvc?.navigationItem.leftBarButtonItem = qyCustomBackButton()
                qyvc?.sessionTitle = ""
                qyvc?.groupId = 74126
                QYSDK.shared().customActionConfig().linkClickBlock = {
                    [weak self](QYLinkClickBlock) -> Void in
                    let temp = QYLinkClickBlock! as String
                    EventDispatcher.dispatch(temp, onNavigationController: self?.navigationController)
                }
                self.navigationController?.pushViewController(qyvc!, animated: true)
            }
            else
            {
                self.jumpToLogin(Constant.InLoginType.consultType, tipe: nil)
            }
           
        }else {
            let detailListVC = DetailMessageListViewController()
            detailListVC.type = msgList[(indexPath as NSIndexPath).row - 1].type
            detailListVC.titleString = msgList[(indexPath as NSIndexPath).row - 1].type_name
            
            self.navigationController?.pushViewController(detailListVC, animated: true)
        }
    }
    
    func qyCustomBackButton() -> UIBarButtonItem
    {
        let backButton = UIButton(type: UIButtonType.custom)
        backButton.frame = CGRect(x: 0, y: 0, width: 36, height: 36)
        backButton.setBackgroundImage(UIImage(named: "common_back_icon"), for: UIControlState())
        backButton.addTarget(self, action: #selector(MessageList.backButtonClicked), for: UIControlEvents.touchUpInside)
        backButton.clipsToBounds = true
        backButton.tintColor = UIColor.red
        
        let item = UIBarButtonItem(customView: backButton)
        
        return item
    }
    
    func backButtonClicked() {
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
}


private let TypeTable: JSON = JSON([
    "1": ["name": "系统通知", "icon": "message_type_system"],
    "2": ["name": "订单消息", "icon": "message_type_logistics"],
    "3": ["name": "活动通知", "icon": "message_type_activity"],
    "4": ["name": "红包提醒", "icon": "message_type_coupon"],
    "5": ["name": "送礼攻略", "icon": "message_type_tractial"],
    "6": ["name": "到货通知", "icon": "message_type_interFlow"],
    ] as AnyObject)
extension MessageList {
    fileprivate class MessageCenterListCell: UITableViewCell {
        
        static let identifier = "MessageCenterListCell"
        static let cellHeight:CGFloat = 65
        var data: MessageListModel? {
            didSet {
                // 1系统通知 2物流消息 3活动通知 4红包提醒
                
                if let type = data?.type {
                    let type_int = Int(type)
                    if type_int > 6 || type_int < 1{
                        typeImage.image = UIImage(named: "messageDefault")
                        titleLabel.text = "提醒通知"
                    }
                    else{
                        typeImage.image = UIImage(named: TypeTable[String(type_int)]["icon"].stringValue)
                        titleLabel.text = data?.type_name
                    }
                }
                subTitleLabel.text = data?.content
                
                // time
                timeLabel.text = nil
                if let dateStr = data?.add_time {
                     timeLabel.text = dateStr
                }
                
                //标记
                if let tag = data?.not_read_num {
                    if tag > 0 {
                        tagLbl.isHidden = false
                        tagLbl.text = String(tag)
                        if tag > 99 {
                            tagLbl.text = "···"
                        }
                    }else {
                        tagLbl.isHidden = true
                    }
                }
                
            }
        }
        
        let backGroundView = UIView()
        
        let typeImage = UIImageView()
        
        let titleLabel = UILabel()
        
        let subTitleLabel = UILabel()
        
        let timeLabel = UILabel()
        
        // 用于标识是否已读
        let tagLbl:UILabel = {
            let lbl = UILabel()
            lbl.textAlignment = .center
            lbl.layer.cornerRadius = 7
            lbl.layer.masksToBounds = true
            lbl.textColor = UIColor.white
            lbl.font = UIFont.systemFont(ofSize: Constant.common_F6_font)
            lbl.backgroundColor = UIColor.red
            return lbl
        }()
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)
            self.backgroundColor = UIColor.clear
            self.selectionStyle = .none
            
            backGroundView.backgroundColor = UIColor.white
            
            titleLabel.font = UIFont.systemFont(ofSize: 17)
            titleLabel.textColor = UIColor.black
            
            subTitleLabel.font = UIFont.systemFont(ofSize: 13)
            subTitleLabel.textColor = UIColor.lightGray
            
            timeLabel.font = UIFont.systemFont(ofSize: 11)
            timeLabel.textColor = UIColor.lightGray
            
            self.contentView.addSubview(backGroundView)
            
            backGroundView.addSubview(typeImage)
            backGroundView.addSubview(titleLabel)
            backGroundView.addSubview(subTitleLabel)
            backGroundView.addSubview(timeLabel)
            backGroundView.addSubview(tagLbl)
            
            
            // 布局
            backGroundView.snp_makeConstraints { (make) in
                let _ = make.top.equalTo(self)
                let _ = make.left.equalTo(self)
                let _ = make.right.equalTo(self)
                let _ = make.bottom.equalTo(self).offset(-MARGIN_10)
            }
            
            typeImage.snp_makeConstraints { (make) -> Void in
                let _ = make.centerY.equalTo(self)
                let _ = make.left.equalTo(self).offset(MARGIN_10)
                let _ = make.height.equalTo(20)
                let _ = make.width.equalTo(typeImage.snp_height)
            }

            titleLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(self).offset(MARGIN_10)
                let _ = make.left.equalTo(typeImage.snp_right).offset(MARGIN_10)
                let _ = make.right.equalTo(timeLabel.snp_left).offset(-MARGIN_10)
                let _ = make.height.equalTo(20)
            }

            timeLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.right.equalTo(self).offset(-MARGIN_10)
                let _ = make.top.equalTo(self).offset(MARGIN_10)
                let _ = make.height.equalTo(20)
            }

            subTitleLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(titleLabel.snp_bottom).offset(5)
                let _ = make.left.equalTo(titleLabel)
                let _ = make.right.equalTo(self).offset(-MARGIN_10)
            }

            tagLbl.snp_makeConstraints { (make) -> Void in
                let _ = make.centerY.equalTo(typeImage.snp_top)
                let _ = make.centerX.equalTo(typeImage.snp_right)
                let _ = make.width.equalTo(14)
                let _ = make.height.equalTo(14)
            }

        }
        
        required init(coder aDecoder: NSCoder)
        {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
}

class MessageListModel: BaseDataMode {
    /*
     "add_time" : "2016-07-10 17:30:02",
     "id" : 5869490,
     "content" : "您的订单116581911还未付款，请尽快完成付款。超时未付我们将在60分钟后为您关闭订单。",
     "not_read_num" : 11,
     "type" : 2,
     "type_name" : "订单消息"
    */
    var add_time = NULL_STRING {
        didSet {
            add_time = changeTime(add_time)
        }
    }
    
    var id = NULL_INT
    
    var content = NULL_STRING
    
    var not_read_num = NULL_INT
    
    var type = NULL_INT
    
    var type_name = NULL_STRING
    
    func changeTime(_ time:String) ->String {
        //获取当天0点
        let calendar = Calendar.current
        
        let unitTag = NSCalendar.Unit.year.rawValue | NSCalendar.Unit.month.rawValue | NSCalendar.Unit.day.rawValue
        let currentDate = Date()
//        let zone = NSTimeZone.systemTimeZone()
//        let tempInterval0 : Int = zone.secondsFromGMTForDate(currentDate)
//        let localDate = currentDate.dateByAddingTimeInterval(NSTimeInterval(tempInterval0))
        let dateComponents = (calendar as NSCalendar).components(NSCalendar.Unit(rawValue: unitTag), from: currentDate)
        
        let year = dateComponents.year
        let month = dateComponents.month
        let day = dateComponents.day
        
        let nowTime = "\(year!)" + "-\(month!)" + "-\(day!)" + " 00:00:00"
        
        //服务器时间字符串转换成NSdate
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        //比较
        if let serverDate = formatter.date(from: time)  {
            let nowDate = formatter.date(from: nowTime)
            
            let tempArray = time.components(separatedBy: " ")
            let interval = nowDate?.timeIntervalSince(serverDate)
            if interval < 0 {
                //今天
                if tempArray.count > 0 {
                    return tempArray[1]
                }
            }else if interval >= 0 && interval <= 24 * 3600 {
                //昨天
                if tempArray.count > 0 {
                    return "昨天" + "\(tempArray[1])"
                }
            }else {
                return time
            }
            
        }
        return time
    }

}


