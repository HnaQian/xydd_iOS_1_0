//
//  AboutViewController.swift
//  Limi
//
//  Created by ZhiQiangMi on 16/2/16.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit
import CoreData
import WebKit

class AboutViewController: BaseViewController {
    @IBOutlet var appIconView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var versionLabel: UILabel!
    
    @IBOutlet var protocolButton: UIButton!
    @IBOutlet var declareLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "关于心意点点"
        
        protocolButton.layer.borderColor = self.view.backgroundColor?.cgColor
        protocolButton.layer.borderWidth = 0.5
        protocolButton.addTarget(self, action: #selector(AboutViewController.pushAgreementView), for: UIControlEvents.touchUpInside)
        
        let str = (Bundle.main.infoDictionary! as [String: AnyObject])["CFBundleShortVersionString"] as! String
        versionLabel.text = "版本" + str
        
        if let btn = DevelopTestManager.getSwitchBtn(){
            self.view.addSubview(btn)
        }
    }
    
    func pushAgreementView(){
        
        if let url = URL(string: SystemInfoRequestManager.shareInstance().user_protocol)  {
            let webVC = WebViewController(URL: url)
            self.navigationController?.pushViewController(webVC, animated: true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}


