//
//  UpdateNickNameViewController.swift
//  Limi
//
//  Created by 程巍巍 on 5/25/15.
//  Copyright (c) 2015 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit
import CoreData

class UpdateNickNameViewController: BaseViewController
{
    
    var nnView: NickNameView{   return self.view as! NickNameView }
    
    override func loadView()
    {
        super.loadView()
        self.view = NickNameView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.nnView.textField.becomeFirstResponder()
    }

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
//        self.edgesForExtendedLayout = UIRectEdge()
        
        self.navigationItem.title = "修改昵称"
        
        let requestUserInfo: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        
        let datasourceUserInfo : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(requestUserInfo)!
        
        if datasourceUserInfo.count > 0{
            
            let userInfo = datasourceUserInfo[0] as? User
            if let nickName : String = userInfo?.nick_name{
                nnView.textField.text = nickName
            }
        }
        self.navigationItem.rightBarButtonItem = CommonWidgets.NavigationRightBtn.newRightBtn("确定", target: self, action: #selector(UpdateNickNameViewController.confirm))
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @objc func confirm()
    {
        self.nnView.textField.resignFirstResponder()
        // 非空验证
        let nickName = self.nnView.textField.text
        
        if !Utils.checkNiName(nickName)
        {
            Utils.showError(context: self.view, errorStr: "输入4-16个字符的昵称")
            return
        }

       let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.user_nick_name, parameters: ["nick_name": nickName! as AnyObject], isNeedUserTokrn: true,isShowErrorStatuMsg: true, isNeedHud: true, hudTitle:  "正在提交", successHandler: {data, status, msg in
            if status == 200
            {
                Utils.showError(context: self.view, errorStr: "修改昵称成功")
                // 成功
                let request: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
                let datasource : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(request)!
                if datasource.count > 0{
                    
                    let userInfo = datasource[0] as! User
                    userInfo.nick_name = nickName
                }
                else{
                    let entity = NSEntityDescription.entity(forEntityName: "User", in: CoreDataManager.shared.managedObjectContext)
                    request.entity = entity
                    do {
                        try! CoreDataManager.shared.managedObjectContext.fetch(request)
                        let user: User = NSEntityDescription.insertNewObject(forEntityName: "User", into: CoreDataManager.shared.managedObjectContext) as! User
                        user.nick_name = nickName
                    }
                }
                CoreDataManager.shared.save({Void in
                    let _ = self.navigationController?.popViewController(animated: true)
                    },failureHandler: {Void in
                        let _ = self.navigationController?.popViewController(animated: true)
                        
                })
            }
        })
        
        
    }

}

extension UpdateNickNameViewController
{
    class NickNameView: BaseView
    {
        var inputBgView = UIView()
        
        var textField = UITextField()
        
        var msgLabel = UILabel()
        
        var confirmButton = UIButton()
        
        override func defaultInit()
        {
            
            self.backgroundColor = UIColor(rgba: Constant.common_background_color)
            
            textField.placeholder = "请输入昵称"
            
            textField.clearButtonMode = UITextFieldViewMode.whileEditing
            
            textField.backgroundColor = UIColor.white
            
            textField.borderStyle = UITextBorderStyle.none
            
            textField.clearButtonMode = .always
            
            textField.font = UIFont.systemFont(ofSize: 15)
            
            msgLabel.text = "4-16个字符，可由中英文、数字、下划线组成。"
            
            msgLabel.font = Constant.CustomFont.Default(size: 12)
            
            msgLabel.textColor = UIColor(rgba: "#A5A5A5")
            
            inputBgView.backgroundColor = UIColor.white
            
            inputBgView.layer.borderColor = UIColor(rgba: "#EAEAEA").cgColor
            
            inputBgView.layer.borderWidth = 0.5

            self.addSubview(inputBgView)
            
            inputBgView.addSubview(textField)
            
            self.addSubview(msgLabel)
            
            // constraint
            inputBgView.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(self).offset(8)
                let _ = make.left.equalTo(self).offset(-1)
                let _ = make.right.equalTo(self).offset(1)
                let _ = make.height.equalTo(36)
            }

            textField.snp_makeConstraints { (make) -> Void in
                let _ = make.centerY.equalTo(inputBgView)
                let _ = make.left.equalTo(inputBgView).offset(9)
                let _ = make.right.equalTo(inputBgView).offset(-9)
            }
            
            msgLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(inputBgView.snp_bottom).offset(8)
                let _ = make.left.equalTo(self).offset(8)
            }
            
        }
        
    }
}
