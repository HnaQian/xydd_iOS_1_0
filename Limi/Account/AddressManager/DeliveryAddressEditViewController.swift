//
//  DeliveryAddressEditViewController.swift
//  Limi
//
//  Created by 程巍巍 on 5/20/15.
//  Copyright (c) 2015 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//


import UIKit
import CoreData
import AddressBookUI
import ContactsUI
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


protocol EditAddressDelegate{
    func reloadAddressToOrder(_ info: AddressMode?)
}

class DeliveryAddressEditViewController: BaseViewController,ABPeoplePickerNavigationControllerDelegate,UITextFieldDelegate,UITextViewDelegate,LCActionSheetDelegate,AddressRequestManagerDelegate,CNContactPickerDelegate {
    
    var delegate: EditAddressDelegate?
    var deliveryAddress:DeliveryAddressManageViewController?
    var editView: EditView
        {
            return self.view as! EditView
    }
    var cells = [UITableViewCell]()
    
    var addressInfo: AddressMode?
    
    // name
    var nameCell: NameCell!
    
    var phoneCell: InputCell!
    
    var regionCell: RegionCell!
    
    var addressCell: AddressCell!
    
    var checkbutton: RadioCell!
    
    let regionPicker = RegionPickerView()
    
    // 地址栏一行字的高度
    var oneRowHeight: CGFloat = 0
    
    override func loadView()
    {
        super.loadView()
        self.view = EditView()
    }
    

    var navTitle = "新增收货地址"
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
//        self.edgesForExtendedLayout = UIRectEdge()
        self.navigationItem.rightBarButtonItem = CommonWidgets.NavigationRightBtn.newRightBtn("保存", target: self, action: #selector(DeliveryAddressEditViewController.addDeliveryAddress))
        
        // 一行高度
        oneRowHeight = NSString(string: "一行").boundingRect(with: CGSize(width: Constant.ScreenSize.SCREEN_WIDTH - 115, height: CGFloat(MAXFLOAT)), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 15)], context: nil).size.height
        
        editView.tableView.delegate = self
        
        editView.tableView.dataSource = self
        
        // addressInfo 为空，则是新增
        //关闭手势返回
        self.gesturBackEnable = false
        self.navigationItem.title = addressInfo == nil ? navTitle : "收货地址编辑"
        
        initPropertise()
        
        regionPicker.frame = Constant.ScreenSize.SCREEN_BOUNDS
        regionPicker.delegate = self
        regionPicker.alpha = 0
        regionPicker.isHidden = true
        self.view.addSubview(regionPicker)
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
    }
    
    
    override func backItemAction() {
        // 输入框响应取消
        nameCell.inputField.resignFirstResponder()
        phoneCell.inputField.resignFirstResponder()
        addressCell.inputField.resignFirstResponder()
        
        
        let name = nameCell.inputField.text
        let phone = phoneCell.inputField.text
        let region = regionCell.detailTextLabel?.text
        let address = addressCell.inputField.text
        let checkBool = checkbutton.radioButton.isOn
        
        // 新增收货地址页面
        if addressInfo == nil
        {
            // 新增收货地址页面 没有任何填写
            if name?.characters.count == 0 && phone?.characters.count == 0 && region?.characters.count == 2 && address?.characters.count == 0 && checkBool == false
            {
               let _ = self.navigationController?.popViewController(animated: true)
            }
            else
            {
                // 新增收货地址页面 最少填写一项
                LCActionSheet(title: "确定要放弃填写地址吗?", buttonTitles: ["放弃填写"], cancelTitle: "继续填写", redButtonIndex: -1, clicked: { (buttonIndex) -> Void in
                    
                    if buttonIndex == 1
                    {
                        let _ = self.navigationController?.popViewController(animated: true)
                    }
                    
                }).show()
            }
        }
            // 编辑收货地址界面
        else
        {
            // 获取 编辑前信息
            var origName: String = ""
            var origPhone: String = ""
            var origRegion: String = "  "
            var origAddress: String = ""
            var origCheckBool: Bool = false
            
            var previenceName: String = ""
            var cityName: String = ""
            var countryName: String = ""
            
            
            if let string = addressInfo?.name     {origName = string }
            if let string = addressInfo?.mobile   {origPhone = string }
            if let string = addressInfo?.province {previenceName = string }
            if let string = addressInfo?.city     {cityName = string }
            if let string = addressInfo?.area{countryName = string}
            
            origRegion = "\(previenceName) \(cityName) \(countryName)"
            if let string = addressInfo?.address
            {
                origAddress = string
            }
            if let is_default = addressInfo?.is_default
            {
                origCheckBool = is_default == 1
            }
            
            // 判断是否在编辑收货地址页面更改过信息
            if name == origName && phone == origPhone && region == origRegion && address == origAddress && checkBool == origCheckBool
            {
                let _ = self.navigationController?.popViewController(animated: true)
            }
            else
            {
                LCActionSheet(title: "确定要放弃填写地址吗?", buttonTitles: ["放弃填写"], cancelTitle: "继续填写", redButtonIndex: -1, clicked: { (buttonIndex) -> Void in
                    
                    if buttonIndex == 1
                    {
                        let _ = self.navigationController?.popViewController(animated: true)
                    }
                    
                }).show()
            }
        }
        
    }
    
    
    func initPropertise()
    {
        // name
        nameCell = NameCell()
        
        let font = UIFont.systemFont(ofSize: 15)
        
        
        nameCell.titleLabel.font = font
        nameCell.titleLabel.textColor = UIColor.black
        nameCell.titleLabel.text = "收货人"
        
        
        
        nameCell.inputField.font = font
        nameCell.inputField.placeholder = "请填写收货人姓名"
        
        if let name = addressInfo?.name
        {
            nameCell.inputField.text = name
        }
        nameCell.rightView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(DeliveryAddressEditViewController.openContact)))
        
        cells.append(nameCell)
        
        // phone
        phoneCell = InputCell()
        
        phoneCell.inputField.delegate = self
        
        
        phoneCell.titleLabel.font = font
        phoneCell.titleLabel.textColor = UIColor.black
        phoneCell.titleLabel.text = "收货人手机号"
        
        
        phoneCell.inputField.font = font
        phoneCell.inputField.placeholder = "请填写手机号"
        
        
        
        if let phone = addressInfo?.mobile
        {
            phoneCell.inputField.text = phone
        }
        
        phoneCell.inputField.keyboardType = UIKeyboardType.numberPad
        
        cells.append(phoneCell)
        
        
        
        // region
        
        regionCell = RegionCell(style: .value1, reuseIdentifier: nil)
        regionCell.textLabel?.font = font
        
        regionCell.textLabel?.textColor = UIColor.black
        regionCell.textLabel?.text = "所在地"
        
        regionCell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        
        regionCell.selectionStyle = .none
        
        var province_id = 0
        if let id = addressInfo?.province_id{
            province_id = id
        }
        regionCell.previence = ["name":addressInfo?.province as AnyObject? ?? "" as AnyObject, "id": "\(province_id)" as AnyObject]
        
        var city_id = 0
        if let id = addressInfo?.city_id{
            city_id = id
        }
        
        regionCell.city = ["name":addressInfo?.city as AnyObject? ?? "" as AnyObject, "id": "\(city_id)" as AnyObject]
        
        var area_id = 0
        if let id = addressInfo?.area_id{
            area_id = id
        }
        
        
        regionCell.country = ["name":addressInfo?.area as AnyObject? ?? "" as AnyObject, "id": "\(area_id)" as AnyObject]
        
        cells.append(regionCell)
        
        // address
        addressCell = AddressCell()
        
        
        addressCell.titleLabel.font = font
        addressCell.titleLabel.textColor = UIColor.black
        addressCell.titleLabel.text = "详细地址"
        
        
        addressCell.inputField.placeHolder = "请填写详细地址"
        addressCell.inputField.placeHolderColor = UIColor(rgba: "#e2e2e2")
        addressCell.inputField.placeHolderFont = font
        addressCell.inputField.text = addressInfo?.address
        if let string = addressInfo?.address
        {
            let size = NSString(string: string).boundingRect(with: CGSize(width: Constant.ScreenSize.SCREEN_WIDTH - 115, height: CGFloat(MAXFLOAT)), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 15)], context: nil).size
            
            if size.height > oneRowHeight
            {
                addressCell.inputField.textAlignment = NSTextAlignment.left
            }
            else
            {
                addressCell.inputField.textAlignment = NSTextAlignment.right
            }
        }
        
        addressCell.inputField.delegate = self
        addressCell.inputField.controlPlaceHolder()
        
        let size = NSString(string: addressCell.inputField.text).boundingRect(with: CGSize(width: Constant.ScreenSize.SCREEN_WIDTH - 115, height: CGFloat(MAXFLOAT)), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 15)], context: nil).size
        
        if size.height > oneRowHeight
        {
            addressCell.inputField.textContainerInset.top = 4
        }
        
        cells.append(addressCell)
        
        // checkbutton
        checkbutton = RadioCell()
        if UserInfoManager.didLogin == true{
            
            if let Is_default = addressInfo?.is_default
            {
                checkbutton.radioButton.isOn = Is_default == 1
            }
            cells.append(checkbutton)
        }
    }
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == self.phoneCell.inputField
        {
            if string.characters.count > 0
            {
                if textField.text?.characters.count > 10
                {
                    return false
                }
            }
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        textField.endEditing(true)
    }
    
    
    fileprivate var tempAddressModel:AddressMode!
    
    func addDeliveryAddress()
    {
        if nameCell.inputField.text!.isEmpty
        {
            Utils.showError(context: self.view, errorStr: "请填写收货人姓名！")
            return
        }
        else
        {
            if  !Utils.checkName(nameCell.inputField.text)
            {
                Utils.showError(context: self.view, errorStr: "请正确填写收货人姓名！")
                return
            }
        }
        if phoneCell.inputField.text!.isEmpty
        {
            Utils.showError(context: self.view, errorStr: "请填写收货人联系方式！")
            return
        }
        else
        {
            if  phoneCell.inputField.text?.characters.count != 11
            {
                Utils.showError(context: self.view, errorStr: "请正确填写联系方式!")
                return
            }
        }
        if addressCell.inputField.text!.isEmpty
        {
            Utils.showError(context: self.view, errorStr: "请填写收货人详细地址！")
            return
        }
        if addressCell.inputField.text?.characters.count < 5 || addressCell.inputField.text?.characters.count > 60
        {
            Utils.showError(context: self.view, errorStr: "请填写正确的详细地址")
            return
        }
        
        
        if regionCell.previence == nil || regionCell.city == nil || regionCell.country == nil
        {
            Utils.showError(context: self.view, errorStr: "请选择收货人所在地！")
            return
        }
        
        if let temp = self.navigationItem.rightBarButtonItem?.customView  {
            (temp as! UIButton).isEnabled = false
        }
       
        //Params
        var params = [String: AnyObject]()
        
        var is_default:Int = 2
        if checkbutton.radioButton.isOn
        {
            is_default = 1
        }
        
        params["name"] = nameCell.inputField.text as AnyObject?
        params["mobile"] = phoneCell.inputField.text as AnyObject?
        
        var idAry  = [Int]()

        for dic in [regionCell.previence,regionCell.city,regionCell.country]{
            
            if  let dict = dic,
                let idStr = dict["id"] as? String{
                idAry.append(idStr.intValue)
            }
        }
        
        let province_id = idAry[0]
        let city_id = idAry[1]
        let area_id = idAry[2]
        
        print(regionCell.country!["id"] as! String)
        
        params["province_id"] = province_id as AnyObject?
        
        params["city_id"] = city_id as AnyObject?
    
        params["area_id"] = area_id as AnyObject?

        
        params["address"] = addressCell.inputField.text as AnyObject?
        
        params["is_default"] = is_default as AnyObject?
        
        if addressInfo != nil
        {
            params["id"] = addressInfo?.id as AnyObject?
        }
        
        //Model
        let newMode = AddressMode()
        
        let provine:String =  self.regionCell.previence?["name"] as! String//省
        let city:String = self.regionCell.city?["name"] as! String//市
        let area:String = self.regionCell.country?["name"] as! String//区
        let id : Int = 0
        let Name : String = self.nameCell.inputField.text!
        let Mobile : String = self.phoneCell.inputField.text!
        let Address : String = self.addressCell.inputField.text!
        
        if provine.characters.count < 2 || city.characters.count < 2 || area.characters.count < 2 {
            Utils.showError(context: self.view, errorStr: "请选择收货人所在地！")
            return
        }
        
        newMode.id = id
        newMode.name = Name
        newMode.province_id = province_id
        newMode.city_id = city_id
        newMode.area_id = area_id
        newMode.province = provine
        newMode.city = city
        newMode.area = area
        newMode.mobile = Mobile
        newMode.address = Address
        newMode.is_default = is_default
        
        tempAddressModel = newMode
        
        print(params)
        if UserInfoManager.didLogin == true{
            self.addNewAddress(params)
        }
        else{
            self.delegate?.reloadAddressToOrder(newMode)
            let _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    func addNewAddress(_ params:[String: AnyObject])
    {
        
        if addressInfo == nil
        {
            AddressRequestManager.shareInstance(self).addressPlusNew(self.view,parameters: params, isShowErrorStatuMsg: true, isNeedHud: true)
        }else
        {
            AddressRequestManager.shareInstance(self).addressEdit(self.view,parameters: params, isShowErrorStatuMsg: true, isNeedHud: true)
        }
    }
    
    
    //编辑地址 response
    func addressRequestManagerDidEditSuccess(_ requestManager: LMRequestManager, result: LMResultMode) {
        if let temp = self.navigationItem.rightBarButtonItem?.customView  {
            (temp as! UIButton).isEnabled = true
        }
        if result.status == 200
        {
            self.delegate?.reloadAddressToOrder(tempAddressModel)
            let _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    //添加地址 response
    func addressRequestManagerDidPlusSuccess(_ requestManager: LMRequestManager, result: LMResultMode) {
        if let temp = self.navigationItem.rightBarButtonItem?.customView  {
            (temp as! UIButton).isEnabled = true
        }
        if result.status == 200
        {
            if let model = tempAddressModel,
                let data = result.data,
                let id = data["id"] as? Int
            {
                model.id = id
                self.delegate?.reloadAddressToOrder(model)
            }
            
            let _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    func networkTaskDidFail(_ networkTask: LMRequestManager, error: NSError?, msg: String?) {
        if let temp = self.navigationItem.rightBarButtonItem?.customView  {
            (temp as! UIButton).isEnabled = true
        }
    }
    
    func openContact()
    {
        self.navigationBarAppearance(true)
        if #available(iOS 9.0, *) {
            let newPicker = CNContactPickerViewController()
            newPicker.delegate = self
            self.present(newPicker, animated: true, completion: {
                
            })
        } else {
            let picker = ABPeoplePickerNavigationController()
            
            picker.peoplePickerDelegate = self
            picker.displayedProperties = [3]
            picker.predicateForSelectionOfPerson = NSPredicate(value:false)
            self.present(picker, animated: true) { () -> Void in
                
            }
            
        }
    }
    
    @available(iOS 9.0, *)
    @objc(contactPicker:didSelectContactProperty:)
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contactProperty: CNContactProperty) {
        let firstName = contactProperty.contact.givenName
        let name = contactProperty.contact.familyName
        nameCell.inputField.text = (name + firstName)
        
        if contactProperty.value != nil {
            let tempPhoneNumber = contactProperty.value as! CNPhoneNumber
            let phoneNumber = tempPhoneNumber.stringValue
            phoneCell.inputField.text = Utils.detectionTelphoneSimple(phoneNumber)
        }
        
    }
    
    @available(iOS 9.0, *)
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        //去除地址选择界面
        picker.dismiss(animated: true, completion: { () -> Void in
            
        })
    }

    
    func peoplePickerNavigationController(_ peoplePicker: ABPeoplePickerNavigationController,
        didSelectPerson person: ABRecord) {
            
            /* Get all the phone numbers this user has */
            let unmanagedPhones = ABRecordCopyValue(person, kABPersonPhoneProperty)
            let phones: ABMultiValue =
            Unmanaged.fromOpaque(unmanagedPhones!.toOpaque()).takeUnretainedValue()
                as NSObject as ABMultiValue
            
            let countOfPhones = ABMultiValueGetCount(phones)
            
            for index in 0..<countOfPhones{
                let unmanagedPhone = ABMultiValueCopyValueAtIndex(phones, index)
               let _ = Unmanaged.fromOpaque(
                    (unmanagedPhone?.toOpaque())!).takeUnretainedValue() as NSObject
            }
    }
    
    func peoplePickerNavigationController(_ peoplePicker: ABPeoplePickerNavigationController,
        didSelectPerson person: ABRecord, property: ABPropertyID,
        identifier: ABMultiValueIdentifier) {
            //            UINavigationBar.appearance().tintColor = UIColor(rgba: Constant.titlebar_color)
            let multiValue: ABMultiValue = ABRecordCopyValue(person, property).takeRetainedValue()
            var index = ABMultiValueGetIndexForIdentifier(multiValue, identifier)
            if index == -1{
                index = 0 as CFIndex
            }
            let phone = ABMultiValueCopyValueAtIndex(multiValue, index).takeRetainedValue() as? String ?? ""
            
            let first = ABRecordCopyValue(person, kABPersonFirstNameProperty)?.takeRetainedValue() as? String ?? ""
            let last  = ABRecordCopyValue(person, kABPersonLastNameProperty)?.takeRetainedValue() as? String ?? ""
            
            nameCell.inputField.text = last + first
            phoneCell.inputField.text = Utils.detectionTelphoneSimple(phone)
            
    }
    
    //取消按钮点击
    func peoplePickerNavigationControllerDidCancel(_ peoplePicker: ABPeoplePickerNavigationController) {
        //去除地址选择界面
        
        peoplePicker.dismiss(animated: true, completion: { () -> Void in
            
        })
    }
    
    func peoplePickerNavigationController(_ peoplePicker: ABPeoplePickerNavigationController,
        shouldContinueAfterSelectingPerson person: ABRecord) -> Bool {
            return false
    }
    
    func peoplePickerNavigationController(_ peoplePicker: ABPeoplePickerNavigationController,
        shouldContinueAfterSelectingPerson person: ABRecord, property: ABPropertyID,
        identifier: ABMultiValueIdentifier) -> Bool {
            return false
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        if textView == addressCell.inputField
        {
            if text == "\n"
            {
                return false
            }
            
        }
        return true
    }
    
    func textViewDidChange(_ textView: UITextView)
    {
        if textView == addressCell.inputField
        {
            
            let size = NSString(string: textView.text).boundingRect(with: CGSize(width: Constant.ScreenSize.SCREEN_WIDTH - 115, height: CGFloat(MAXFLOAT)), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 15)], context: nil).size
            
            if size.height > oneRowHeight
            {
                addressCell.inputField.textContainerInset.top = 4
            }
            else
            {
                addressCell.inputField.textContainerInset.top = 13
            }
            
            
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == addressCell.inputField
        {
            
            let size = NSString(string: textView.text).boundingRect(with: CGSize(width: Constant.ScreenSize.SCREEN_WIDTH - 115, height: CGFloat(MAXFLOAT)), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 15)], context: nil).size
            
            if size.height > oneRowHeight
            {
                addressCell.inputField.textAlignment = NSTextAlignment.left
            }
            else
            {
                addressCell.inputField.textAlignment = NSTextAlignment.right
            }
            
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView == addressCell.inputField
        {
            addressCell.inputField.textAlignment = NSTextAlignment.left
        }
    }
    
}

extension DeliveryAddressEditViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        return cells[(indexPath as NSIndexPath).row]
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        
        return 44
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 8
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if cells[(indexPath as NSIndexPath).row] == regionCell
        {
            if nameCell.inputField.isFirstResponder{
                nameCell.inputField.resignFirstResponder()
            }
            if phoneCell.inputField.isFirstResponder{
                phoneCell.inputField.resignFirstResponder()
            }
            if addressCell.inputField.isFirstResponder{
                addressCell.inputField.resignFirstResponder()
            }
            // 弹出地址选择器
            regionPicker.region.previence = regionCell.previence
            regionPicker.region.city = regionCell.city
            regionPicker.region.country = regionCell.country
            regionPicker.regionDidChanged = {[weak self](region: RegionPickerView.Region)->Void in
                //
                self?.regionCell.previence = region.previence
                self?.regionCell.city = region.city
                self?.regionCell.country = region.country
            }
            
            // 获取全部 省
            regionPicker.loadPrevience()
            
            // 获取当前省 下面全部市
            regionPicker.loadCity()
            
            // 获取当前市 下面全部地区
            regionPicker.loadCountry()
            
            regionPicker.pickerView.reloadAllComponents()
            
            regionPicker.showRegionView()
            
            // 匹配当前 省 市 地区 在pickerView中位置
            var previenceIndex: Int = 0
            var cityIndex: Int = 0
            var countryIndex: Int = 0
            if let _ = regionPicker.region.previence?["cities"] as? [[String: AnyObject]] {
                
                previenceIndex = regionPicker.previenceArray.index(of: regionPicker.region.previence!)
            }
            
            if let _ = regionPicker.region.city?["counties"] as? [[String: AnyObject]] {
                
                cityIndex = regionPicker.cityArray.index(of: regionPicker.region.city!)
                
            }
            
            if (regionPicker.region.country?["name"] as? String)?.characters.count != 0 {
                
                countryIndex = regionPicker.countryArray.index(of: regionPicker.region.country!)
                
            }
            
            
            regionPicker.pickerView.selectRow(previenceIndex, inComponent: 0, animated: false)
            regionPicker.pickerView.selectRow(cityIndex, inComponent: 1, animated: false)
            regionPicker.pickerView.selectRow(countryIndex, inComponent: 2, animated: false)
            
        }
        if cells[(indexPath as NSIndexPath).row] == phoneCell
        {
            phoneCell.inputField.keyboardType = UIKeyboardType.numberPad
        }
    }
}

extension DeliveryAddressEditViewController {
    class EditView: BaseView {
        
        var tableView = UITableView(frame: CGRect.zero, style: UITableViewStyle.grouped)
        
        override func defaultInit() {
            self.backgroundColor = UIColor(rgba: Constant.common_background_color)
            
            self.addSubview(tableView)
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            
            tableView.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
            tableView.separatorColor = UIColor(rgba: Constant.common_separatLine_color)
            tableView.showsHorizontalScrollIndicator = false
            tableView.showsVerticalScrollIndicator = false
            
        }
        
        
    }
    
    class InputCell: UITableViewCell {
        var titleLabel = UILabel()
        var inputField = UITextField()
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)
            defaultInit()
        }
        
        required init(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func defaultInit() {
            titleLabel.frame = CGRect(x: 15, y: 0, width: 100, height: self.frame.size.height)
            self.addSubview(titleLabel)
            inputField.frame = CGRect(x: 105, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH - 115, height: 44)
            self.addSubview(inputField)
            self.selectionStyle = UITableViewCellSelectionStyle.none
            
            inputField.clearButtonMode = UITextFieldViewMode.whileEditing
            inputField.font = UIFont.systemFont(ofSize: 15)
            titleLabel.font = UIFont.systemFont(ofSize: 15)
            
            inputField.textAlignment = NSTextAlignment.right
            
            
            
            
        }
    }
    
    class AddressCell: UITableViewCell {
        var titleLabel = UILabel()
        var inputField = UITextView()
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)
            defaultInit()
        }
        
        required init(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func defaultInit() {
            titleLabel.frame = CGRect(x: 15, y: 0, width: 90, height: self.frame.size.height)
            self.addSubview(titleLabel)
            inputField.frame = CGRect(x: 105, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH - 115, height: 44)
            self.addSubview(inputField)
            self.selectionStyle = UITableViewCellSelectionStyle.none
            
            inputField.font = UIFont.systemFont(ofSize: 15)
            titleLabel.font = UIFont.systemFont(ofSize: 15)
            inputField.alignment = NSTextAlignment.right
            inputField.placeHolderTextView.textContainerInset.top = 13
            inputField.textContainerInset.top = 13
            
        }
    }
    
    
    class NameCell: UITableViewCell {
        var titleLabel = UILabel()
        var inputField = UITextField()
        var rightView = UIView()
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)
            defaultInit()
        }
        
        required init(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func defaultInit() {
            
            self.selectionStyle = UITableViewCellSelectionStyle.none
            //            self.accessoryView?.userInteractionEnabled = true
            titleLabel.frame = CGRect(x: 15, y: 0, width: 50, height: self.frame.size.height)
            self.addSubview(titleLabel)
            inputField.frame = CGRect(x: 65, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH - 115, height: self.frame.size.height)
            self.addSubview(inputField)
            inputField.font = UIFont.systemFont(ofSize: 15)
            titleLabel.font = UIFont.systemFont(ofSize: 15)
            
            inputField.textAlignment = NSTextAlignment.right
            
            inputField.clearButtonMode = UITextFieldViewMode.whileEditing
            
            rightView = UIView(frame: CGRect(x: Constant.ScreenSize.SCREEN_WIDTH - 50, y: 0, width: 50, height: self.frame.size.height))
            let imageView : UIImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
            imageView.center = rightView.center
            imageView.image = UIImage(named: "chargeContactIcon")
            imageView.contentMode = UIViewContentMode.scaleAspectFill
            imageView.clipsToBounds = true
            self.addSubview(imageView)
            
            self.addSubview(rightView)
        }
    }
    
    class RegionCell: UITableViewCell {
        var previence: [String: AnyObject]? {
            didSet{
                reloadDetailLabel()
            }
        }
        var city: [String: AnyObject]? {
            didSet{
                reloadDetailLabel()
            }
        }
        var country: [String: AnyObject]? {
            didSet{
                reloadDetailLabel()
            }
        }
        
        func reloadDetailLabel() {
            var previenceName: String = ""
            if let title = previence?["name"] as? String {
                previenceName = title
            }
            var cityName = ""
            if let title = city?["name"] as? String {
                cityName = title
            }
            var countryName = ""
            if let title = country?["name"] as? String {
                countryName = title
            }
            self.detailTextLabel?.font = UIFont.systemFont(ofSize: 15)
            self.detailTextLabel?.textColor = UIColor.black
            self.detailTextLabel?.text = String(format: "%@ %@ %@",previenceName,cityName,countryName)
            
            //            do {
            //                let attribText : NSAttributedString = try NSAttributedString(data: "<t font='\(Constant.CustomFont.DefaultFontName)' size='15' color='black' >\(previenceName) \(cityName) \(countryName)</t>".dataUsingEncoding(NSUnicodeStringEncoding)!, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil)
            //                self.detailTextLabel?.attributedText = attribText
            //            } catch {
            //                print(error)
            //            }
            
            self.setNeedsLayout()
        }
    }
    
    class RadioCell: UITableViewCell {
        var radioButton = RadioButton()
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)
            defaultInit()
        }
        
        required init(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func defaultInit(){
            self.contentView.addSubview(radioButton)
            radioButton.isOn = false
            self.textLabel?.font = UIFont.systemFont(ofSize: 15)
            self.selectionStyle = UITableViewCellSelectionStyle.none
            
            let _ = radioButton.handle(events: .touchUpInside) { (sender, event) -> Void in
                if let radio = sender as? RadioButton {
                    radio.isOn = !radio.isOn
                }
            }
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            radioButton.frame = CGRect(x: self.frame.width - 8 - 88, y: (self.frame.height - 26)/2, width: 88, height: 26)
            radioButton.setNeedsLayout()
        }
        
        class RadioButton: AccountDeliveryAddressListCell.ActionButton {
            var isOn: Bool = false {
                didSet {
                    let imageName = isOn ? "choosed" : "noChoose"
                    self.setImage(UIImage(named: imageName), for: UIControlState())
                    self.setTitle("设为默认", for: UIControlState())
                }
            }
        }
    }
    
    
    class RegionPickerView: BaseView, UIPickerViewDataSource, UIPickerViewDelegate{
        
        weak var delegate:DeliveryAddressEditViewController?
        // 屏幕宽、高
        let screenWidth = Constant.ScreenSize.SCREEN_WIDTH
        let screenHeight = Constant.ScreenSize.SCREEN_HEIGHT
        
        struct Region {
            
            var previence: [String: AnyObject]?
            var city: [String: AnyObject]?
            var country: [String: AnyObject]?
        }
        
        var pickerView = UIPickerView()
        fileprivate var topBar = UIView()
        fileprivate var actionButton = UIButton()
        fileprivate var cancelButton = UIButton()
        var contentView = UIView()
        fileprivate var chineseAreaCode: NSArray!
        fileprivate var region = Region(previence: nil, city: nil, country: nil)
        
        var regionDidChanged: ((_ region: Region)->Void)?
        
        var originYConstraint: NSLayoutConstraint!
        
        var previenceArray = NSMutableArray()
        
        var cityArray = NSMutableArray()
        
        var countryArray = NSMutableArray()
        
        override func defaultInit() {
            do {
                let json = try JSONSerialization.jsonObject(with: Data(contentsOf: URL(fileURLWithPath: Bundle.main.path(forResource: "region_information", ofType: "json")!)), options: JSONSerialization.ReadingOptions.allowFragments)
                print(json)
                self.chineseAreaCode = json as! NSArray
                print(chineseAreaCode)
            } catch let error as NSError {
                print(error)
            }
            
        }
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            
            self.backgroundColor = UIColor(white: 0.5, alpha: 0.5)
            
            pickerView.dataSource = self
            pickerView.delegate = self
            pickerView.backgroundColor = UIColor.white
            
            self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(RegionPickerView.cancelAction)))
            
            topBar.backgroundColor = UIColor.white
            topBar.layer.borderColor = UIColor(rgba: "#e2e2e2").cgColor
            topBar.layer.borderWidth = 0.5
            
            let font = UIFont.systemFont(ofSize: 16)
            
            
            actionButton.titleLabel?.font = font
            actionButton.setTitle("确定", for: UIControlState())
            actionButton.setTitleColor(UIColor(rgba: "#E13636"), for: UIControlState())
            
            
            actionButton.layer.cornerRadius = 2.5
            actionButton.addTarget(self, action: #selector(RegionPickerView.dismiss), for: UIControlEvents.touchUpInside)
            
            
            cancelButton.titleLabel?.font = font
            cancelButton.setTitle("取消", for: UIControlState())
            cancelButton.setTitleColor(UIColor(rgba: "#E13636"), for: UIControlState())
            
            
            cancelButton.layer.cornerRadius = 2.5
            cancelButton.addTarget(self, action: #selector(RegionPickerView.cancelAction), for: UIControlEvents.touchUpInside)
            
            topBar.frame = CGRect(x: 0, y: 0, width: screenWidth, height: 36)
            cancelButton.frame = CGRect(x: 10, y: 6, width: 50, height: 20)
            actionButton.frame = CGRect(x: screenWidth - 10 - 50, y: 6, width: 50, height: 20)
            pickerView.frame = CGRect(x: 0, y: 36, width: screenWidth, height: pickerView.iheight)
            
            self.contentView.frame = CGRect(x: 0,y: screenHeight + 36,width: screenWidth,height: pickerView.iheight + topBar.iheight)
            
            topBar.isUserInteractionEnabled = true
            
            contentView.isUserInteractionEnabled = true
            contentView.backgroundColor = UIColor.red
            contentView.addSubview(pickerView)
            contentView.addSubview(topBar)
            
            self.topBar.addSubview(actionButton)
            self.topBar.addSubview(cancelButton)
            
        }
        
        required init(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        
        
        // 获取 省名称
        func loadPrevience() {
            
            if let area_code = chineseAreaCode as? [[String: AnyObject]] {
                previenceArray.removeAllObjects()
                print(area_code)
                for item in area_code {
                    print(item)
                    previenceArray.add(item)
                }
            }
            
        }
        
        
        // 获取 市名称
        func loadCity() {
            
            var cities: [[String: AnyObject]]?
            let prevName = region.previence?["name"] as! String
            print(prevName)
            // 当前 省 匹配
            if prevName.characters.count != 0 {
                
                for item in previenceArray {
                    let str:String = (item as! [String:AnyObject])["name"] as! String
                    print("\(prevName)....\(str)")
                    if prevName.components(separatedBy: str).count > 1 {
                       
                        region.previence = item as? [String : AnyObject]
                        
                    }
                }
                
                // 获取当前省 下面的 市
                cities = region.previence!["cities"] as? [[String: AnyObject]]
                print(cities)
            }
            else
            {
                // 获取当前省 下面的 市
                cities = (previenceArray[0] as! [String: AnyObject])["cities"] as? [[String: AnyObject]]
            }
            cityArray.removeAllObjects()
            if let chengshi = cities {
                for item in chengshi {
                    cityArray.add(item)
                }

            }
            
        }
        
        // 获取 地区名称
        func loadCountry() {
            
            var counties: [[String: AnyObject]]?
            let cityName = region.city?["name"] as! String
            // 当前 市 匹配
            if cityName.characters.count != 0 {
                
                for item in cityArray {
                    if (item as! [String:AnyObject])["name"] as! String == cityName {
                        region.city = item as? [String: AnyObject]
                    }
                }
                // 获取当前市 下面的 地区
                counties = region.city!["counties"] as? [[String: AnyObject]]
            }
            else
            {
                // 获取当前市 下面的 地区
                counties = (cityArray[0] as! [String: AnyObject])["counties"] as? [[String: AnyObject]]
            }
            
            countryArray.removeAllObjects()
            if let items = counties {
                print(items)
                for item in items {
                    countryArray.add(item)
                }
            }
            
            
            // 当前 地区 匹配
            let countryName = region.country?["name"] as! String
            if countryName.characters.count != 0 {
                for item in countryArray {
                    if (item as! [String:AnyObject])["name"] as! String == countryName {
                        region.country = item as? [String : AnyObject]
                    }
                }
            }
            
        }
        
        
        @objc func cancelAction(){
            
            self.regionViewdisAppear()
        }
        
        @objc func dismiss(){
            
            self.regionViewdisAppear()
            updateRegion()
        }
        
        // 显示pickerView
        func showRegionView() {
            if let del = delegate{
                del.presentSemiView(contentView, withOptions:nil, completion: {
                    
                })
            }
            
            #if false
                let regionHeight: CGFloat = 36 + pickerView.frame.height
                self.alpha = 1
                self.hidden = false
                
                UIView.animateWithDuration(0.35, animations: { () -> Void in
                    
                    //                self.topBar.iy = self.screenHeight - regionHeight - 64
                    self.pickerView.iy = self.screenHeight - regionHeight + 36 - 64
                    
                })
            #endif
        }
        
        // 隐藏pickerView
        func regionViewdisAppear() {
            if let del = delegate{
                del.dismissSemiModalView()
            }
            
            #if false
                UIView.animateWithDuration(0.35, animations: { () -> Void in
                    
                    //                self.topBar.iy = self.screenHeight
                    self.pickerView.iy = self.screenHeight + 36
                    
                    }) { (value: Bool) -> Void in
                        self.alpha = 0
                        self.hidden = true
                }
            #endif
        }
        
        // UIPickerViewDataSource
        // returns the number of 'columns' to display.
        func numberOfComponents(in pickerView: UIPickerView) -> Int{
            return 3
        }
        
        // returns the # of rows in each component..
        func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
            if component == 0{
                
                return previenceArray.count
                
            }else if component == 1{
                
                return cityArray.count
                
            }else if component == 2{
                
                return countryArray.count
            }
            
            return 0
            
        }
        
        // UIPickerViewDelegate
        func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
        {
            if component == 0
            {
                return (previenceArray.object(at: row) as! NSDictionary).object(forKey: "name") as? String
                
            }else if component == 1
            {
                
                return (cityArray.object(at: row) as! NSDictionary).object(forKey: "name") as? String
                
            }else if component == 2
            {
                
                return (countryArray.object(at: row) as! NSDictionary).object(forKey: "name") as? String
            }
            return ""
        }
        
        
        func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
        {
            if component == 0
            {
                let index = pickerView.selectedRow(inComponent: 0)
                
                region.previence = previenceArray[index] as? [String: AnyObject]
                
                loadCity()
                
                pickerView.reloadComponent(1)
                pickerView.selectRow(0, inComponent: 1, animated: true)
                
                region.city = cityArray[0] as? [String: AnyObject]
                
                loadCountry()
                
                pickerView.reloadComponent(2)
                
                pickerView.selectRow(0, inComponent: 2, animated: true)
                
                
            }
            
            if component == 1
            {
                let index = pickerView.selectedRow(inComponent: 1)
                
                region.city = cityArray[index] as? [String: AnyObject]
                
                loadCountry()
                
                pickerView.reloadComponent(2)
                pickerView.selectRow(0, inComponent: 2, animated: true)
                
            }
            
            if component == 2
            {
                let index = pickerView.selectedRow(inComponent: 2)
                region.country = countryArray[index] as? [String: AnyObject]
                
            }
        }
        
        func updateRegion()
        {
            if pickerView.numberOfRows(inComponent: 0) > 0
            {
                let index = pickerView.selectedRow(inComponent: 0)
                
                self.region.previence = previenceArray.object(at: index) as? [String: AnyObject]
            }
            
            if pickerView.numberOfRows(inComponent: 1) > 0
            {
                let index = pickerView.selectedRow(inComponent: 1)
                
                self.region.city = cityArray[index] as? [String: AnyObject]
            }
            
            if pickerView.numberOfRows(inComponent: 2) > 0
            {
                let index = pickerView.selectedRow(inComponent: 2)
                
                self.region.country = countryArray[index] as? [String: AnyObject]
            }
            
            self.regionDidChanged?(self.region)
        }
    }
    
}
