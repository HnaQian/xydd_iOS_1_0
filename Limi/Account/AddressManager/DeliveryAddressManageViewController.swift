//
//  DeliveryAddressManageViewController.swift
//  Limi
//
//  Created by 程巍巍 on 6/5/15.
//  Copyright (c) 2015 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

protocol AddressManagerDelegate{
    func reloaddataFromManager(_ isDelete:Bool,index:Int)
}

class DeliveryAddressManageViewController: BaseViewController,LCActionSheetDelegate,EditAddressDelegate,AddressRequestManagerDelegate
{
    
    var delegate: AddressManagerDelegate?
    
    var listView: DeliveryAddressListView
        {
            return self.view as! DeliveryAddressListView
    }
    
    override func loadView() {
        super.loadView()
        self.view = DeliveryAddressListView()
    }
    
    // 选择后的回调 如果不存在
    var actionHandler: ((_ addressInfo: [String: AnyObject])->Void)?
    
    // 界面状态 管理/选择
    var status: Bool = false
    
    var addressList = [AddressMode]() {
        didSet {
            if addressList.count != 0 {
                listView.titlelabel.isHidden = true
                listView.emptyAddButton.isHidden = true
                listView.imageView.isHidden = true
//                listView.addButton.hidden = false
            }
            else
            {
                listView.titlelabel.isHidden = false
                listView.emptyAddButton.isHidden = false
                listView.imageView.isHidden = false
//                listView.addButton.hidden = true
            }
        }
    }
    
    
    
    func reloadAddressToOrder(_ info: AddressMode?){
        reloadAddressList()
        self.delegate?.reloaddataFromManager(false,index:0)
    }
    
    var navTitle = "收货地址"
    
    var addBtnTitle = "新增收货地址"
    
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationItem.title = navTitle
        // table view  cell register
        listView.tableView.register(AddressListCell.self, forCellReuseIdentifier: "AddressListCell")
        
//        self.listView.addButton.hidden = true
        
        self.listView.addButton.setTitle(self.addBtnTitle, for: UIControlState())
        let _ = self.listView.addButton.handle(events: UIControlEvents.touchUpInside) {[weak self] (sender, event) -> Void in
            
            if (self?.addressList.count)! >= 20 {
                Utils.showError(context: (self?.view)!, errorStr: "最多只能添加20个地址")
                return
            }
            self?.listView.addButton.backgroundColor = UIColor(rgba: Constant.common_C1_color)
            
            let vc = DeliveryAddressEditViewController()
            vc.delegate = self
            
            self?.navigationController?.pushViewController(vc, animated: true)
            
        }
        
        let _ = self.listView.addButton.handle(events: UIControlEvents.touchDown) {[weak self] (sender, event) -> Void in
            
            self?.listView.addButton.backgroundColor = UIColor(rgba: Constant.common_C100_color)
        }
        
        let _ = self.listView.emptyAddButton.handle(events: UIControlEvents.touchUpInside) {[weak self] (sender, event) -> Void in
            
            let vc = DeliveryAddressEditViewController()
            vc.delegate = self
            
            self?.navigationController?.pushViewController(vc, animated: true)
        }
        
        weak var weakSelf = self as DeliveryAddressManageViewController
        listView.tableView.addHeaderRefresh {
            DispatchQueue.global().async(execute: { ()->Void in
                weakSelf?.reloadAddressList()
            })
        }
        listView.tableView.beginHeaderRefresh()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        listView.emptyAddButton.isHidden = true
        listView.titlelabel.isHidden = true
        listView.imageView.isHidden = true
        
        listView.tableView.delegate = self
        
        listView.tableView.dataSource = self
        listView.tableView.beginHeaderRefresh()
       
    }
    
    //MARK: 加载地址列表
    func reloadAddressList() {
        
        AddressRequestManager.shareInstance(self).addressLoadList(self.view,isShowErrorStatuMsg: true)
    }
    
    
    //MARK: 删除
    func deleteAddress(_ info: AddressMode,index:Int) {
        LCActionSheet(title: "确定删除此条地址？删除后不可恢复", buttonTitles: ["确定"], redButtonIndex: 0) { (buttonIndex) -> Void in
            
            if buttonIndex == 1
            {
                AddressRequestManager.shareInstance(self).addressDelete(self.view,addressId: info.id, isShowErrorStatuMsg: true, isNeedHud: true,index:index)
            }
            
            }.show()
        
    }
    
    //MARK: 设为默认
    func setDefaultAddress(_ info: AddressMode) {
        
        if info.is_default == 1{
            Utils.showError(context: self.view, errorStr: "已经是默认了！")
            return
        }
        
        AddressRequestManager.shareInstance(self).addressSetDefault(self.view,addressId: info.id, isShowErrorStatuMsg: true, isNeedHud: true)
    }
    
    func addressRequestManagerDidLoadListFinish(_ requestManager: LMRequestManager, result: LMResultMode, addressList: [AddressMode])
    {
        self.addressList  = addressList
        listView.tableView.endHeaderRefresh()

        if addressList.count == 0 {
            self.listView.tableView.frame.size.height = Constant.ScreenSizeV2.SCREEN_HEIGHT - 64
            listView.addButton.removeFromSuperview()
        }else{
            self.listView.tableView.frame.size.height = Constant.ScreenSizeV2.SCREEN_HEIGHT - 64 - 44
            listView.addSubview(listView.addButton)
        }
        self.listView.tableView.reloadData()
    }
    
    func networkTaskDidFail(_ networkTask: LMRequestManager, error: NSError?, msg: String?) {
        listView.tableView.endHeaderRefresh()
    }
    
    func addressRequestManagerDidDeleteSuccess(_ requestManager: LMRequestManager, result: LMResultMode,index:Int) {
        if result.status == 200
        {
            self.delegate?.reloaddataFromManager(true,index:index)
            self.reloadAddressList()
        }
    }
    
    func addressRequestManagerDidDefaultSuccess(_ requestManager: LMRequestManager, result: LMResultMode) {
        if result.status == 200
        {
            self.delegate?.reloaddataFromManager(false,index:0)
            self.reloadAddressList()
        }
    }
    
}

extension DeliveryAddressManageViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return addressList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddressListCell", for: indexPath) as! AddressListCell
        let data = addressList[(indexPath as NSIndexPath).section]
        cell.reloadData(data)
        
        if data.is_default == 1{
            cell.setSelected(true, animated: false)
        }
        
        
        let _ = cell.checkButton.handle(events: UIControlEvents.touchUpInside) { [weak self](sender, event) -> Void in
            self?.setDefaultAddress(data)
        }
        let _ = cell.delButton.handle(events: UIControlEvents.touchUpInside) { [weak self](sender, event) -> Void in
            self?.deleteAddress(data,index:indexPath.section)
        }
        
        let _ = cell.editButton.handle(events: .touchUpInside) {[weak self] (sender, event) -> Void in
            
            let vc = DeliveryAddressEditViewController()
            
            vc.addressInfo = data
            
            vc.delegate = self
            
            self?.navigationController?.pushViewController(vc, animated: true)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 8
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect.zero)
        return view
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect.zero)
        return view
    }
}


extension DeliveryAddressManageViewController {
    class DeliveryAddressListView: BaseView {
        var tableView = UITableView(frame: CGRect.zero, style: UITableViewStyle.grouped)
        var addButton = UIButton()
        var emptyAddButton = UIButton(type: .custom)
        var imageView = UIImageView(image: UIImage(named: "emptyAddressIcon"))
        var titlelabel = UILabel()
        
        override func defaultInit() {
            self.backgroundColor = UIColor(rgba: Constant.common_background_color)
            self.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: Constant.ScreenSizeV2.SCREEN_HEIGHT - 64)
            tableView.showsHorizontalScrollIndicator = false
            tableView.showsVerticalScrollIndicator = false
            
            tableView.separatorStyle = UITableViewCellSeparatorStyle.none
            self.addSubview(tableView)
            
            addButton.setTitle("新增收货地址", for: UIControlState())
            addButton.titleLabel?.font = Constant.CustomFont.Default(size: Constant.common_F2_font)
            addButton.setTitleColor(UIColor(rgba: Constant.common_C12_color), for: UIControlState())
            addButton.backgroundColor = UIColor(rgba: Constant.common_C1_color)
            addButton.frame = CGRect(x: 0, y: Constant.ScreenSizeV2.SCREEN_HEIGHT - 44 - 64, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: 44)
            self.addSubview(addButton)
            
            titlelabel.font = Constant.CustomFont.Default(size: 13)
            titlelabel.textColor = UIColor.lightGray
            titlelabel.text = "您还没有收货地址哦！"
            titlelabel.textAlignment = NSTextAlignment.center
            self.addSubview(titlelabel)
            
            tableView.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: Constant.ScreenSizeV2.SCREEN_HEIGHT - 64 - 44)
//            tableView.snp_makeConstraints { (make) -> Void in
//                make.top.equalTo(self)
//                make.left.equalTo(self)
//                make.right.equalTo(self)
//                make.bottom.equalTo(addButton.snp_top)
//            }
            
//            addButton.snp_makeConstraints { (make) -> Void in
//                make.left.equalTo(self)
//                make.right.equalTo(self)
//                make.bottom.equalTo(self)
//                make.height.equalTo(44)
//            }
            
            titlelabel.snp_makeConstraints { (make) -> Void in
                let _ = make.center.equalTo(self)
            }
            
            imageView.backgroundColor = UIColor.clear
            self.addSubview(imageView)
            
            imageView.snp_makeConstraints { (make) -> Void in
                let _ = make.height.equalTo(70)
                let _ = make.width.equalTo(70)
                let _ = make.bottom.equalTo(titlelabel.snp_top).offset(-16)
                let _ = make.centerX.equalTo(self)
            }
            
            
            emptyAddButton.setTitle("去添加", for: UIControlState())
            emptyAddButton.titleLabel?.font = Constant.CustomFont.Default(size: 15)
            emptyAddButton.setTitleColor((UIColor(rgba: Constant.common_red_color)), for: UIControlState())
            self.addSubview(emptyAddButton)
            
            emptyAddButton.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(titlelabel.snp_bottom).offset(16)
                let _ = make.centerX.equalTo(self)
            }
        }
    }
    
    class AddressListCell: BaseTableViewCell {
        var nameLabel = UILabel()
        var phoneLabel = UILabel()
        var addressLabel = UILabel()
        var separatorLine = UIView()
        var checkButton = UIButton()
        var editButton = UIButton()
        var delButton = UIButton()
        var headerLine = UIView()
        var footerLine = UIView()
        
        
        override func reloadData(_ data:BaseDataMode) {
            
            let mdata = data as! AddressMode
            
            nameLabel.font = UIFont.systemFont(ofSize: 17)
            phoneLabel.font = UIFont.systemFont(ofSize: 15)
            
            nameLabel.text = mdata.name
            phoneLabel.text = mdata.mobile
            
            addressLabel.font = UIFont.systemFont(ofSize: 13)
            addressLabel.textColor = UIColor(rgba: "#5B5B5B")
            addressLabel.text = "\(mdata.province) \(mdata.city) \(mdata.area) \(mdata.address)"
            addressLabel.lineBreakMode = NSLineBreakMode.byCharWrapping
            
            if mdata.is_default == 1 {
                checkButton.setImage(UIImage(named: "choosed"), for: UIControlState())
            }
            else{
                checkButton.setImage(UIImage(named: "noChoose"), for: UIControlState())
            }
        }
        
        override func defaultInit() {
            
            checkButton.setImage(UIImage(named: "noChoose"), for: UIControlState())
            checkButton.titleLabel?.font = UIFont.systemFont(ofSize: 15)
            
            
            checkButton.titleLabel?.font = UIFont.systemFont(ofSize: 15)
            checkButton.setTitleColor(UIColor(rgba: "#000000"), for: UIControlState())
            checkButton.setTitle("默认地址", for: UIControlState())
            
            delButton.titleLabel?.font = UIFont.systemFont(ofSize: 15)
            delButton.setImage(UIImage(named: "delete_button_gray"), for: UIControlState())
            
            delButton.titleLabel?.font = UIFont.systemFont(ofSize: 15)
            delButton.setTitleColor(UIColor(rgba: "#000000"), for: UIControlState())
            delButton.setTitle("删除", for: UIControlState())
            
            editButton.titleLabel?.font = UIFont.systemFont(ofSize: 15)
            editButton.setImage(UIImage(named: "edit_button_gray"), for: UIControlState())
            
            editButton.titleLabel?.font = UIFont.systemFont(ofSize: 15)
            editButton.setTitleColor(UIColor(rgba: "#000000"), for: UIControlState())
            editButton.setTitle("编辑", for: UIControlState())
            
            separatorLine.backgroundColor = UIColor(rgba: Constant.common_separatLine_color)
            headerLine.backgroundColor = UIColor(rgba: Constant.common_separatLine_color)
            footerLine.backgroundColor = UIColor(rgba: Constant.common_separatLine_color)
            
            self.selectionStyle = .none
            
            for view in [nameLabel,phoneLabel,addressLabel,separatorLine,checkButton,editButton,delButton,headerLine,footerLine]
            {
                self.contentView.addSubview(view)
            }
            
            
            // constraint
            nameLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(self.contentView).offset(10)
                let _ = make.bottom.equalTo(self.contentView.snp_top).offset(30)
            }
            
            phoneLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(nameLabel.snp_right).offset(4)
                let _ = make.bottom.equalTo(self.contentView.snp_top).offset(30)
            }
            
            addressLabel.numberOfLines = 2
            
            addressLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(nameLabel)
                let _ = make.right.equalTo(self.contentView).offset(-10)
                let _ = make.top.equalTo(nameLabel.snp_bottom)
                let _ = make.height.equalTo(40)
            }
            
            separatorLine.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(self.contentView).offset(10)
                let _ = make.right.equalTo(self.contentView).offset(-10)
                let _ = make.top.equalTo(addressLabel.snp_bottom)
                let _ = make.height.equalTo(0.5)
            }
            
            checkButton.snp_makeConstraints { (make) -> Void in
                let _ = make.centerY.equalTo(self.contentView.snp_bottom).offset(-18)
                let _ = make.left.equalTo(separatorLine)
            }
            
            delButton.snp_makeConstraints { (make) -> Void in
                let _ = make.centerY.equalTo(checkButton)
                let _ = make.right.equalTo(separatorLine)
            }
            
            editButton.snp_makeConstraints { (make) -> Void in
                let _ = make.centerY.equalTo(checkButton)
                let _ = make.right.equalTo(delButton.snp_left).offset(-20)
            }
            
            
            headerLine.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(self.contentView)
                let _ = make.right.equalTo(self.contentView)
                let _ = make.top.equalTo(self.contentView)
                let _ = make.height.equalTo(0.5)
            }
            
            
            footerLine.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(self.contentView)
                let _ = make.right.equalTo(self.contentView)
                let _ = make.bottom.equalTo(self.contentView)
                let _ = make.height.equalTo(0.5)
            }
            
        }
    }
}
