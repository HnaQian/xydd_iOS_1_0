//
//  AddressMode.swift
//  Limi
//
//  Created by Richie on 16/3/4.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class AddressMode: BaseDataMode {
  
    var address  = NULL_STRING
    
    var area     = NULL_STRING
    
    var area_id     = NULL_INT
    
    var city     = NULL_STRING
    
    var city_id     = NULL_INT
    
    var id          = NULL_INT
    
    // 1：默认
    var is_default  = NULL_INT
    
    var mobile   = NULL_STRING
    
    var name     = NULL_STRING
    
    var province = NULL_STRING
    
    var province_id = NULL_INT

    /*
    {
    Address = "\U6211\U60f3\U8981\U4e48\U662f\U7684\U4f60\U8bf4\U4f60";
    Area = "\U4e1c\U57ce\U533a";
    "Area_id" = 110101;
    City = "\U5317\U4eac\U5e02";
    "City_id" = 110100;
    Id = 6815;
    "Is_default" = 2;
    Mobile = 18521337456;
    Name = "\U6b32\U671b";
    Province = "\U5317\U4eac";
    "Province_id" = 110000;
    }
    */
}
