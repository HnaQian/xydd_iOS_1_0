//
//  AddressListViewController.swift
//  Limi
//
//  Created by ZhiQiangMi on 15/12/18.
//  Copyright © 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

protocol AddressListDelegate{
    func reloadAddressCellFromList(_ info: AddressMode?,index : Int)
}

private let TITLE = "收货地址"

class AddressListViewController: BaseViewController,EditAddressDelegate,AddressManagerDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bottomBtn: UIButton!
    @IBOutlet weak var emptyView: UIView!
    
    var addressList = [AddressMode]()
    var selectIndex : Int = 10086
    var delegate: AddressListDelegate?
    var deleteIndex = 2000
    
    override func backItemAction()
    {
        if addressList.count > 0 && addressList.count > selectIndex{
            
            self.delegate?.reloadAddressCellFromList(addressList[self.selectIndex], index: self.selectIndex)
            if deleteIndex == selectIndex {
                self.delegate?.reloadAddressCellFromList(nil, index: self.selectIndex)
            }
        }
        else{
            if self.selectIndex != 10086{
                self.delegate?.reloadAddressCellFromList(nil, index: self.selectIndex)
            }
        }
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    func reloadAddressToOrder(_ info: AddressMode?){
//        self.selectIndex = 0
//        self.addressList.insert(info!, atIndex: self.selectIndex)
        self.addressList.append(info!)
        self.tableView.reloadData()
        self.addRightItem()
    }
    
    func reloaddataFromManager(_ isDelete:Bool,index:Int){
        if isDelete {
            self.deleteIndex = index
        }
        
        if tableView != nil
        {
            tableView.beginHeaderRefresh()
        }
    }
    
    var navTitle = TITLE
    
    var addBtnTitle = "新增收货地址"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.bottomBtn.setTitle(addBtnTitle, for: UIControlState())
        self.navigationItem.title = navTitle
        //关闭手势返回
        self.gesturBackEnable = false
        
        self.addRightItem()
        
        self.tableView.addHeaderRefresh { () -> Void in
            
           gcd.async(.default) {
                self.loadData()
            }
        }
        
       self.tableView.beginHeaderRefresh()
        
    }
    fileprivate var isFirstAppear = true
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !isFirstAppear{
            self.loadData()
        }
        
        isFirstAppear = false
    }
    
    func loadData(_ successHandler:(()->Void)! = nil){
        let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.address_list, parameters: ["page": 0 as AnyObject, "size": 20 as AnyObject], isNeedUserTokrn: true, isShowErrorStatuMsg: true,successHandler: {data, status, msg in
            if self.tableView != nil{
                if self.tableView.isHeaderRefreshing == true{
                    self.tableView.endHeaderRefresh()
                }
            }
            if status != 200
            {
                return
            }
            
            if let list = JSON(data as AnyObject)["data"]["list"].array
            {
                if self.tableView != nil{
                    self.emptyView.isHidden = true
                    self.tableView.isHidden = false
                }
                
                self.addressList = [AddressMode]()
                let _ = list.map{self.addressList.append(AddressMode($0.dictionaryObject!))}
                self.addRightItem()
            }
            else{
                if self.tableView != nil{
                    self.navigationItem.rightBarButtonItem = nil
                    self.emptyView.isHidden = false
                    self.tableView.isHidden = true
                    self.addressList.removeAll()
                }
                
            }
            
            if successHandler != nil{
                successHandler()
            }
            
            if self.tableView != nil{
                self.tableView.reloadData()
            }
            
        })
    }
    
    func addRightItem(){
//        let managerButton = UIButton(type: .Custom)
//        managerButton.frame = CGRectMake(0, 0, 36, 36)
//        managerButton.setTitle("管理", forState: .Normal)
//        managerButton.addTarget(self, action: "reviewAddress", forControlEvents: UIControlEvents.TouchUpInside)
//        managerButton.setTitleColor(UIColor(rgba: "#12171F"), forState: .Normal)
//        managerButton.titleLabel?.font = UIFont.systemFontOfSize(13)
//        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: managerButton)
        self.navigationItem.rightBarButtonItem = CommonWidgets.NavigationRightBtn.newRightBtn("管理", target: self, action: #selector(AddressListViewController.reviewAddress))

    }
    
    func reviewAddress() {
        let vc = DeliveryAddressManageViewController()
        vc.addBtnTitle = self.addBtnTitle
        vc.navTitle = self.navTitle
        vc.delegate = self
        
        self.navigationController?.pushViewController(vc, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func numberOfSectionsInTableView(_ tableView: UITableView) -> Int {
        return addressList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        return 72
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell
    {
        let identifier = "AddressListCell"
        
        var cell: AddressListCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? AddressListCell
        
        if cell == nil {
            tableView.register(UINib(nibName: "AddressListCell", bundle: nil), forCellReuseIdentifier: identifier)
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? AddressListCell
        }
        if (indexPath as NSIndexPath).section == self.selectIndex{
        cell.setData(addressList[(indexPath as NSIndexPath).section],isSelected: true)
        }else{
            cell.setData(addressList[(indexPath as NSIndexPath).section],isSelected: false)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 8
    }

    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath)
    {
      tableView.deselectRow(at: indexPath, animated: false)
        if self.addressList.count > 0{
        self.selectIndex = (indexPath as NSIndexPath).section
        self.tableView.reloadData()
        self.delegate?.reloadAddressCellFromList(addressList[(indexPath as NSIndexPath).section], index: self.selectIndex)
        let _ = self.navigationController?.popViewController(animated: true)
        }
    }
    

    func reloadAddressCell(_ info: JSON){
        tableView.beginHeaderRefresh()
    }

    @IBAction func bottomButtonClick(_ sender: AnyObject) {
        let vc = DeliveryAddressEditViewController()
        
        if self.navTitle != TITLE{
            vc.navTitle = "新增地址"
        }
        vc.delegate = self
        if self.addressList.count >= 20 {
            Utils.showError(context: self.view, errorStr: "最多只能添加20个地址")
            return
        }
        
       self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func addNewButtonClick(_ sender: AnyObject) {
        let vc = DeliveryAddressEditViewController()
        if self.navTitle != TITLE{
            vc.navTitle = "新增地址"
        }
        vc.delegate = self
        if self.addressList.count >= 20 {
            Utils.showError(context: self.view, errorStr: "最多只能添加20个地址")
            return
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }

}
