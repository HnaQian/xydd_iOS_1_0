//
//  UserCenterViewController.swift
//  Limi
//
//  Created by ZhiQiangMi on 15/12/28.
//  Copyright © 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit
import CoreData

protocol CheckMessageDelegate{
    func reloadMainTabMessage(_ isHaveMessage : Bool)
}

class UserCenterViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate,LoginDialogDelegate {
    
    var delegate: CheckMessageDelegate?
    let tableView = UITableView()
    let headerView = UserCenterHeaderView()
    
     let userCenterIconImageView : UIImageView = UIImageView(frame: CGRect(x: 5, y: 5, width: 25, height: 25))
    
    let basketStateView : UILabel = {
        let lbl = UILabel()
        lbl.layer.cornerRadius = 7
        lbl.layer.masksToBounds = true
        lbl.backgroundColor = UIColor.red
        lbl.textColor = UIColor.white
        lbl.textAlignment = .center
        lbl.font = UIFont.systemFont(ofSize: Constant.common_F6_font)
        lbl.frame = CGRect(x: 22, y: 5, width: 14 , height: 14 )
        lbl.isHidden = true
        return lbl
    }()
    
    let versionLabel:UILabel = {
        let lbl = UILabel()
        lbl.font = Constant.Theme.Font_11
        lbl.textColor = UIColor(rgba: Constant.common_C7_color)
        lbl.textAlignment = .center
        return lbl
    }()
    
    // 存储 生日百科链接 XX未提交订单 XX可用红包
    fileprivate var contentArr = ["","",""]
    fileprivate var userInfomation : User? = nil
    
    var myBirthWikiUrl : String = "http://wechat.giftyou.me/app/myBirthdayWiki"
    var isfirstLoad = true
    fileprivate var loginDialog = LoginDialog()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.gesturBackEnable = false
        
        self.view.addSubview(self.tableView)
        self.tableView.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: self.view.iheight - MARGIN_49)
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.headerView.headerDelegate = self
        self.tableView.tableHeaderView = self.headerView
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        tableView.backgroundColor = UIColor.clear
        tableView.backgroundView = nil
       
        //left item
        addLeftItem()
        
        //right item
        addRightItem()
        
        self.view.addSubview(versionLabel)
        self.versionLabel.frame = CGRect(x: 30 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE, y: self.view.iheight - MARGIN_49 - 11 - 22 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE, width: self.view.iwidth - 60 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE, height: 11 + 6 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
          let str = (Bundle.main.infoDictionary! as [String: AnyObject])["CFBundleShortVersionString"] as! String
        self.versionLabel.text = "心意点点  v" + str
        
        //商品数量加通知
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: Constant.shopBasketDataReloadFinish), object: nil, queue: OperationQueue.main) { [weak self](notification) -> Void in
             self?.refreshShoppingBasketData()
        }
        loginDialog.delegate = self
    }
    
    func wechatLoginSuccess() {
        ShoppingBasketDataManager.shareManager().reloadData()
        refreshTableViewDataWithHub()
    }
    
    func addLeftItem() {
        let userCenterView : UIView = UIView(frame: CGRect(x: 10, y: 26, width: 36, height: 36))
        userCenterView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(UserCenterViewController.leftItemTapped)))
        
        userCenterIconImageView.image = UIImage(named: "userCenterMessageIcon")
        userCenterIconImageView.isUserInteractionEnabled = true
        userCenterView.addSubview(userCenterIconImageView)
        
        self.view.addSubview(userCenterView)
    }
    
    @objc func leftItemTapped() {
        Statistics.count(Statistics.Mine.my_messagedetails_click)
        if UserInfoManager.didLogin == false{
            self.loginWithType()
        }
        else
        {
            self.delegate?.reloadMainTabMessage(false)
            let message = MessageList()
            message.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(message, animated: true)
        }
    }
    
    func addRightItem() {
       let basketView = UIView(frame: CGRect(x: Constant.ScreenSizeV2.SCREEN_WIDTH - 46, y: 26, width: 36, height: 36))
        basketView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(UserCenterViewController.rightItemTapped)))
        let basketImageView = UIImageView(frame: CGRect(x: 5, y: 5, width: 25, height: 25))
        basketImageView.image = UIImage(named: "userCenterBasket")
        basketImageView.isUserInteractionEnabled = true
        basketView.addSubview(basketImageView)
        
        basketView.addSubview(basketStateView)
        self.view.addSubview(basketView)
    }
    
    @objc func rightItemTapped() {
        let shoppingBasket = ShoppingBasketViewController()
        shoppingBasket.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(shoppingBasket, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.delegate?.reloadMainTabMessage(false)
        isfirstLoad = false
        self.navigationController?.isNavigationBarHidden = false
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func refreshShoppingBasketData() {
        let unreadString = "\(ShoppingBasketDataManager.shareManager().totalGoodsCount)"
        if unreadString != "0" {
            self.basketStateView.isHidden = false
            self.basketStateView.text = unreadString
        }else {
            self.basketStateView.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        userCenterIconImageView.image = UIImage(named: "userCenterMessageIcon")
        self.delegate?.reloadMainTabMessage(false)
        
        self.contentArr = ["","",""]
        let requestUserInfo: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        let datasourceUserInfo : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(requestUserInfo)!
        if datasourceUserInfo.count > 0{
            userInfomation = datasourceUserInfo[0] as? User
            self.setTableViewHeaderView(userInfomation)
        }
        else{
            userInfomation = nil
            self.setTableViewHeaderView(nil)
        }
        
        
        // 获取生日百科链接，未提交订单，可使用红包 等信息
        if UserInfoManager.didLogin == true{
            self.refreshTableViewDataWithHub()
        }
        
        self.tableView.reloadData()
        
        gcd.async(.default) {
            self.queryUnreadMsgCount()
        }
        
        if let url = SystemInfoRequestManager.shareInstance().birthday_wiki_owen{
            myBirthWikiUrl = url
        }
        
        if UserInfoManager.didLogin {
            gcd.async(.default) {
                //用户信息
                let qyUserInfo = QYUserInfo()
                qyUserInfo.userId = UserDefaults.standard.string(forKey: "UserToken")
                qyUserInfo.data = QYLocalUserInfo.shareInstance.getLocalUserInfo()
                QYSDK.shared().setUserInfo(qyUserInfo)
            }
        }
        
        self.basketStateView.isHidden = true
        if !isfirstLoad {
            self.refreshShoppingBasketData()
        }else {
            ShoppingBasketDataManager.shareManager().reloadData()
        }
    }
    
    // 设置tableView的表头
    func setTableViewHeaderView(_ userInfo : User?)
    {
        headerView.reloadData(userInfo: userInfo)
        headerView.refreshCouponUnreadData(data: ["order":self.contentArr[1],"coupon":self.contentArr[2]])
    }
    
    func queryUnreadMsgCount() {
        if UserInfoManager.didLogin == true{
            
            let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.message_unread_count,isNeedUserTokrn: true,successHandler: {data, status, msg in
                if status == 200 {
                    gcd.async(.main) {
                        if JSON(data as AnyObject)["data"]["num"].intValue > 0 || Constant.UNREAD > 0 {
                            self.userCenterIconImageView.image = UIImage(named: "userCenterMessageIcon_unread")
                            self.delegate?.reloadMainTabMessage(true)
                        }
                        else{
                             self.userCenterIconImageView.image = UIImage(named: "userCenterMessageIcon")
                            self.delegate?.reloadMainTabMessage(false)
                        }
                    }
                }
            })
            
        }
    }
    
    
    
    func loginWithType(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.loginType = Constant.InLoginType.accountManagerType.rawValue
        loginDialog.controller = self
        loginDialog.show()
    }
    
    
    func tapUserInfoViewView(_ sender: UIGestureRecognizer)
    {
        Statistics.count(Statistics.Mine.my_id_click)
        let accountInfo = AccountInfoViewController()
        accountInfo.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(accountInfo, animated: true)
    }
    
    func tapcompleteView(_ sender: UIGestureRecognizer)
    {
        self.loginWithType()
    }
    
    
    // tableView代理方法
    
    func numberOfSections(in tableView: UITableView) -> Int{
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        
        return 8.0
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        view.tintColor = UIColor(rgba: Constant.common_background_color)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }
        return 3
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        var cell : UserCenterCell? = tableView.dequeueReusableCell(withIdentifier: UserCenterCell.identifier) as? UserCenterCell
        if(cell == nil)
        {
            cell = UserCenterCell(style: UITableViewCellStyle.value1, reuseIdentifier: UserCenterCell.identifier)
            cell?.selectionStyle = UITableViewCellSelectionStyle.default
        }
        
        cell?.textLabel?.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        cell?.textLabel?.textColor = Constant.Theme.Color14
        
        cell?.detailTextLabel?.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        cell?.detailTextLabel?.textColor = Constant.Theme.Color14
        
        cell?.detailTextLabel?.text = ""
        
        cell?.accessoryView = UIImageView(image: UIImage(named: "cellInfo"))
        
        if (indexPath as NSIndexPath).section == 0{
            if (indexPath as NSIndexPath).row == 0{
                cell?.textLabel?.text = "我的生日百科"
            }
        }
        else if (indexPath as NSIndexPath).section == 1{
            switch (indexPath as NSIndexPath).row{
            case 0:
                cell?.textLabel?.text = "设置"
                let separatLine2 = UIView(frame:CGRect(x: 15,y: cell!.frame.size.height - 0.5,width: cell!.frame.size.width,height: 0.5))
                separatLine2.backgroundColor = UIColor(rgba: Constant.common_separatLine_color)
                cell?.addSubview(separatLine2)
            case 1:
                cell?.textLabel?.text = "联系客服"
    
                let separatLine2 = UIView(frame:CGRect(x: 15,y: cell!.frame.size.height - 0.5,width: cell!.frame.size.width,height: 0.5))
                separatLine2.backgroundColor = UIColor(rgba: Constant.common_separatLine_color)
                cell?.addSubview(separatLine2)
            case 2:
                cell?.textLabel?.text = "邀请好友"
                
            default:
                break
                
            }
        }
        return cell!
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if (indexPath as NSIndexPath).section == 0{
            if (indexPath as NSIndexPath).row == 0{
                
                Statistics.count(Statistics.Mine.my_birthwiki_click)
                if UserInfoManager.didLogin {
                    if self.userInfomation?.birthday_wiki != "" && self.userInfomation?.birthday_wiki != nil{
                        let webVC = WebViewController(URL: URL(string: self.userInfomation!.birthday_wiki!)!)
                        webVC.hidesBottomBarWhenPushed = true
                        self.navigationController?.pushViewController(webVC, animated: true)
                    }
                }else {
                    let alertView = UIAlertController(title: nil, message: "需要设置您的生日，才能查看生日百科哦", preferredStyle: .alert)
                    alertView.addAction(UIAlertAction(title: "去设置", style: .default, handler: { (alertAction) -> Void in
                        let accountInfo = AccountInfoViewController()
                        accountInfo.hidesBottomBarWhenPushed = true
                        self.navigationController?.pushViewController(accountInfo, animated: true)
                    }))
                    alertView.addAction(UIAlertAction(title: "暂不设置", style: .cancel, handler: nil))
                    present(alertView, animated: true, completion: nil)
                }
                
            }
        }else if (indexPath as NSIndexPath).section == 1{
            switch (indexPath as NSIndexPath).row{
            case 0:
                //设置
                Statistics.count(Statistics.Mine.my_settings_click)
                let generalSettingVc = GeneralSettingViewController()
                generalSettingVc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(generalSettingVc, animated: true)
            case 1:
                //联系客服
                let contactServiceVc = ContactServiceViewController()
                contactServiceVc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(contactServiceVc, animated: true)
            case 2:
                Statistics.count(Statistics.Mine.my_shareapp_click)
                share()
            default:
                break
            }
        }
        
    }
    
    //分享
    func share() {
        let Objs : NSArray = [MenuLabel.createlabelIconName("share_icon_wechat_friend", title: "微信好友"),MenuLabel.createlabelIconName("share_icon_wechat_timeline", title: "微信朋友圈"),MenuLabel.createlabelIconName("share_icon_qq_friend", title: "QQ好友"),MenuLabel.createlabelIconName("share_icon_qq_zone", title: "QQ空间")]
        HyPopMenuView.creatingPopMenuObjectItmes(Objs as! [MenuLabel],isGeneral:true, topView: nil, selectdCompletionBlock: {index  in
            if index == 0{
                self.shareToWx(Int32(WXSceneSession.rawValue))
            }
            else if index == 1{
                self.shareToWx(Int32(WXSceneTimeline.rawValue))
            }
            else if index == 2{
                self.shareToQQ(true)
            }
            else if index == 3{
                self.shareToQQ(false)
            }
            
        }
        )
        
    }
    
    func shareToWx(_ wxScene:Int32){
        let link_web = "http://a.app.qq.com/o/simple.jsp?pkgname=com.zhizhuogroup.mind"
        let avatarImage = UIImage(named: "share")
        let shareDict:NSDictionary = [
            LDSDKShareContentTitleKey : "心意小蜜，让你的人脉活起来",   LDSDKShareContentDescriptionKey : "人见人爱的心意小蜜， 为你送礼日程，提供送礼建议，活络社交人脉，爱上了绝对离不开。",LDSDKShareContentWapUrlKey : link_web,LDSDKShareContentImageKey : (Utils.imageResize( avatarImage!,sizeChange: CGSize(width: 256, height: 256)))]
        let wechatShare : LDSDKWXServiceImpl = LDSDKManager.getShareService(LDSDKPlatformType.weChat) as! LDSDKWXServiceImpl
        
        wechatShare.share(withContent: shareDict as! [AnyHashable: Any], shareModule:wxScene == Int32(WXSceneSession.rawValue) ? 1 : 2, onComplete: ({(success,error) in
            if error != nil{
                Utils.showError(context: self.view, errorStr: (error?.localizedDescription)!)
            }
            
        }))
        
    }
    
    func shareToQQ(_ isShareToQQ : Bool){
        let link_web = "http://a.app.qq.com/o/simple.jsp?pkgname=com.zhizhuogroup.mind"
        let utf8String = link_web
        let title = "心意小蜜，让你的人脉活起来"
        let description = "人见人爱的心意小蜜， 为你送礼日程，提供送礼建议，活络社交人脉，爱上了绝对离不开。"
        let previewImage = UIImage(named: "share")
        let shareDict:NSDictionary = [
            LDSDKShareContentTitleKey : title,   LDSDKShareContentDescriptionKey : description,LDSDKShareContentWapUrlKey : utf8String,LDSDKShareContentImageKey : (Utils.imageResize( previewImage!,sizeChange: CGSize(width: 256, height: 256)))]
        let qqShare : LDSDKQQServiceImpl = LDSDKManager.getShareService(LDSDKPlatformType.QQ) as! LDSDKQQServiceImpl
        
        qqShare.share(withContent: shareDict as! [AnyHashable: Any], shareModule:isShareToQQ == true ? 1 : 2, onComplete: ({(success,error) in
            if error != nil{
                Utils.showError(context: self.view, errorStr: (error?.localizedDescription)!)
            }
            
        }))
        
    }

    // 获取订单，红包， 消息  等信息
    func refreshTableViewDataWithHub()
    {
        let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.user_info_v2,isNeedUserTokrn: true, isShowErrorStatuMsg: true, successHandler: { (data, status, msg) -> Void in
            if status != 200
            {
                return
            }
            
            if let dataJson : JSON = JSON(data["data"]! as AnyObject) as JSON?{
                
                if let user : JSON = dataJson["user"] as JSON?{
                    CoreDataManager.shared.update(user, entityType: Constant.CoreDataType.userInfoType)
                }
            }
            
            if let coupon_num = ((data["data"] as? [String: AnyObject])?["user"] as? [String: AnyObject])?["coupon_num"] as? Int
            {
                if coupon_num != 0
                {
                    self.contentArr[2] = "\(coupon_num)"
                }else
                {
                    self.contentArr[2] = ""
                }
            }else
            {
                self.contentArr[2] = ""
            }
            
            
            
            if let order_num = ((data["data"] as? [String: AnyObject])?["user"] as? [String: AnyObject])?["order_num"] as? Int
            {
                if order_num != 0
                {
                    self.contentArr[1] = "\(order_num)"
                }else
                {
                    self.contentArr[1] = ""
                }
                
            }else
            {
                self.contentArr[1] = ""
            }
//            self.headerView.refreshCouponUnreadData(data: ["order":self.contentArr[1],"coupon":self.contentArr[2]])
            if let string = ((data["data"] as? [String: AnyObject])?["user"] as? [String: AnyObject])?["birthday_wiki"] as? String
            {
                self.contentArr[0] = string
            }else
            {
                self.contentArr[0] = ""
            }
            
            let requestUserInfo: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
            let datasourceUserInfo : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(requestUserInfo)!
            if datasourceUserInfo.count > 0{
                self.userInfomation = datasourceUserInfo[0] as? User
                self.setTableViewHeaderView(self.userInfomation)
            }
            else{
                self.userInfomation = nil
                self.setTableViewHeaderView(nil)
            }
            self.tableView.reloadData()
            
        })
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
}

extension UserCenterViewController:UserCenterHeaderViewDelegate {
    func shouldVisitUserInfo() {
        if UserInfoManager.didLogin {
            Statistics.count(Statistics.Mine.my_id_click)
            let accountInfo = AccountInfoViewController()
            accountInfo.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(accountInfo, animated: true)
        }else {
            Statistics.count(Statistics.Mine.my_sign_click)
            self.loginWithType()
        }
    }
    
    func shouldVisitOrderVc() {
        if UserInfoManager.didLogin {
            Statistics.count(Statistics.Mine.my_order_click)
            let giftList = OrderListViewControllerV1()
            giftList.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(giftList, animated: true)
        }else {
            self.loginWithType()
        }
    }
    
    func shouldVisitFavVc() {
        if UserInfoManager.didLogin {
            Statistics.count(Statistics.Mine.my_like_click)
            let favList = FavListViewController()
            favList.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(favList, animated: true)
        }else {
            self.loginWithType()
        }
    }
    
    func shouldVistCouponVc() {
        if UserInfoManager.didLogin == false
        {
            self.loginWithType()
        }
        else
        {
            Statistics.count(Statistics.Mine.my_coupon_click)
            let couponList = CouponListViewController()
            couponList.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(couponList, animated: true)
        }
    }
    
}

class UserCenterCell: UITableViewCell {
    static let identifier = "UserCenterCell"
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
