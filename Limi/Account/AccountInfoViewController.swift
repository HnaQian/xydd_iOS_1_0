//
//  AccountInfoViewController.swift
//  Limi
//
//  Created by 程巍巍 on 5/20/15.
//  Copyright (c) 2015 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit
import CoreData
import Photos

class AccountInfoViewController: BaseViewController,UIActionSheetDelegate,UITableViewDataSource,UITableViewDelegate,DatePickerViewDelegate,LCActionSheetDelegate,LoginDialogDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIPopoverControllerDelegate {
    var picker:UIImagePickerController?=UIImagePickerController()
    var tableView = UITableView()
    var exitBtn = UIButton()
    var avatarView = UIImageView()
    
    var userInfomation : User? = nil
    fileprivate let loginDialog = LoginDialog()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if UserInfoManager.didLogin == true{
            self.exitBtn.isHidden = false
        }
        else{
            self.exitBtn.isHidden = true
        }
        
        
        let requestUserInfo: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        
        let datasourceUserInfo : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(requestUserInfo)!
        
        if datasourceUserInfo.count > 0{
            
            userInfomation = datasourceUserInfo[0] as? User
            self.tableView.reloadData()
        }
        getUserInfo()
        
        
    }
    
    func notifyNewCalendar(_ cal: XYDDCalendar!, type _type: Int32, date: Date!, isYearHidden _isYearHidden: Bool, onlyMonthDay monthDay: String!) {
        var birth = ""
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let birthString = formatter.string(from: date)
        if UserInfoManager.didLogin == true{
            if _isYearHidden{
                if _type == 2{
                    let endDate: Date? = formatter.date(from: birthString)
                    let time = formatter.string(from: endDate!)
                    birth = String(format: "1600-%@", CalendarUtil.alTolunar(time))
                    StartGuidSettingView.shareObject.birthday = birth
                }
                else{
                    birth = birthString.replacingOccurrences(of: (birthString as NSString).substring(to: 4) as String, with: "1600")
                    StartGuidSettingView.shareObject.birthday = birth
                }
            }
            else{
                birth = birthString
                StartGuidSettingView.shareObject.birthday = birth
            }
            
            StartGuidSettingView.shareObject.birthdayType = Int(_type)
            
            StartGuidSettingView.shareObject.uploadInfo(UpLoadInfoType.birthday) { (result) -> Void in
                if result
                {
                    self.saveBirthdayDataToUser(birth, birthdayType: Int(_type))
                }
            }
        }
        else{
            if _isYearHidden{
                if _type == 2{
                    let endDate: Date? = formatter.date(from: birthString)
                    let time = formatter.string(from: endDate!)
                    birth = String(format: "1600-%@", CalendarUtil.alTolunar(time))
                }
                else{
                    birth = birthString.replacingOccurrences(of: (birthString as NSString).substring(to: 4) as String, with: "1600")
                }
            }
            else{
                birth = birthString
            }
            self.saveBirthdayDataToUser(birth, birthdayType: Int(_type))
        }
        
        
    }
    
    
    func saveBirthdayDataToUser(_ birthday : String,birthdayType: Int?){
        let request: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        let datasource : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(request)!
        if datasource.count > 0{
            
            let userInfo = datasource[0] as! User
            userInfo.birthday = birthday + " 00:00:00"
            userInfo.birthday_type = Int64(birthdayType!)
        }
        else{
            
            let request: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
            let entity = NSEntityDescription.entity(forEntityName: "User", in: CoreDataManager.shared.managedObjectContext)
            request.entity = entity
            do {
                try! CoreDataManager.shared.managedObjectContext.fetch(request)
                let user: User = NSEntityDescription.insertNewObject(forEntityName: "User", into: CoreDataManager.shared.managedObjectContext) as! User
                user.birthday = birthday + " 00:00:00"
                user.birthday_type = Int64(birthdayType!)
            }
        }
        CoreDataManager.shared.save({Void in
            self.reloadUserInfo()
            
            let indexPath = IndexPath(row: 3, section: 0)
            self.tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
            },failureHandler: {Void in
                let indexPath = IndexPath(row: 3, section: 0)
                self.tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
                
        })
    }
    
    func reloadUserInfo(){
        let requestUserInfo: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        
        let datasourceUserInfo : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(requestUserInfo)!
        
        if datasourceUserInfo.count > 0{
            
            self.userInfomation = datasourceUserInfo[0] as? User
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.navigationItem.title = "账号管理"
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.separatorColor = UIColor(rgba: Constant.common_separatLine_color)
        self.tableView.tableHeaderView = nil
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        self.view.addSubview(self.tableView)
        self.tableView.backgroundColor = UIColor.clear
        
        self.tableView.showsHorizontalScrollIndicator = false
        self.tableView.showsVerticalScrollIndicator = false
        
        self.tableView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.view)
            let _ = make.left.equalTo(self.view)
            let _ = make.right.equalTo(self.view)
            let _ = make.bottom.equalTo(self.view).offset(-44)
        }
        
        self.view.addSubview(self.exitBtn)
        
        self.exitBtn.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.view.snp_bottom).offset(-44)
            let _ = make.left.equalTo(self.view)
            let _ = make.right.equalTo(self.view)
            let _ = make.bottom.equalTo(self.view)
        }
        
        self.exitBtn.setTitle("退出登录", for: UIControlState())
        self.exitBtn.titleLabel?.font = Constant.CustomFont.Default(size: Constant.common_F2_font)
        self.exitBtn.backgroundColor = UIColor(rgba: Constant.common_C1_color)
        self.exitBtn.setTitleColor(UIColor(rgba: Constant.common_C12_color), for: UIControlState())
        self.exitBtn.addTarget(self, action: #selector(AccountInfoViewController.exit(_:)), for: UIControlEvents.touchUpInside)
        self.exitBtn.addTarget(self, action: #selector(AccountInfoViewController.changeBackColor(_:)), for: UIControlEvents.touchDown)
        
        loginDialog.delegate = self
    }
    
    func wechatLoginSuccess() {
        getUserInfo()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        if UserInfoManager.didLogin == true{
            if section == 1{
                return 30.0
            }
            else if section == 0{
                return 0.0
            }
            else{
                return 8.0
            }
        }
        else{
            if section == 0{
                return 0.0
            }
            else{
                return 8.0
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        let label = UILabel()
        let separatLine1 = UIView()
        let separatLine2 = UIView()
        label.font = UIFont.systemFont(ofSize: 13)
        separatLine1.backgroundColor = UIColor(rgba: Constant.common_separatLine_color)
        separatLine2.backgroundColor = UIColor(rgba: Constant.common_separatLine_color)
        
        view.addSubview(label)
        view.addSubview(separatLine1)
        view.addSubview(separatLine2)
        if UserInfoManager.didLogin == true{
            if section == 1
            {
                view.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: 30)
                label.text = "第三方绑定账号"
                label.frame = CGRect(x: 10, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH-20, height: 30)
                separatLine1.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: 0.5)
                separatLine2.frame = CGRect(x: 0, y: 29.5, width: Constant.ScreenSize.SCREEN_WIDTH, height: 0.5)
            }
            else if section == 0
            {
                view.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: 8)
                
                separatLine1.frame = CGRect.zero
                separatLine2.frame = CGRect(x: 0, y: 7.5, width: Constant.ScreenSize.SCREEN_WIDTH, height: 0.5)
            }
            else
            {
                view.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: 8)
                separatLine1.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: 0.5)
                separatLine2.frame = CGRect(x: 0, y: 7.5, width: Constant.ScreenSize.SCREEN_WIDTH, height: 0.5)
            }
        }
        else{
            view.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: 8)
            separatLine1.frame = CGRect.zero
            separatLine2.frame = CGRect(x: 0, y: 7.5, width: Constant.ScreenSize.SCREEN_WIDTH, height: 0.5)
        }
        
        
        return view
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if UserInfoManager.didLogin == true{
            if section == 0{
                return 5
            }
            else if section == 1{
                return 2
            }
            else if section == 2{
                return 1
            }
            else{
                return 0
            }
        }
        else{
            return 5
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (indexPath as NSIndexPath).section == 0
        {
            if (indexPath as NSIndexPath).row == 0
            {
                return 52
            }
        }
        return 44
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        var cell:UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "cell")! as UITableViewCell
        
        if (cell != nil){
            cell = UITableViewCell(style: UITableViewCellStyle.value1,
                reuseIdentifier: "cell")
        }
        
        
        cell!.textLabel?.font = UIFont.systemFont(ofSize: 15)
        cell!.detailTextLabel?.font = UIFont.systemFont(ofSize: 13)
        
        
        if (indexPath as NSIndexPath).section == 0{
            
            cell!.detailTextLabel?.textColor = UIColor.black
            cell!.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
            if (indexPath as NSIndexPath).row == 0{
                cell!.accessoryType = UITableViewCellAccessoryType.none
                cell!.textLabel?.text = "头像"
                avatarView.image = UIImage(named: "accountIcon")
                avatarView.frame = CGRect(x: self.view.frame.width - 36 - 10, y: 8, width: 36, height: 36)
                avatarView.layer.cornerRadius = avatarView.frame.width/2
                avatarView.clipsToBounds = true
                avatarView.layer.masksToBounds = true
                avatarView.layer.borderColor = UIColor.lightGray.cgColor
                avatarView.layer.borderWidth = 0.5
                cell!.contentView.addSubview(avatarView)
                if UserInfoManager.didLogin == true{
                    if self.userInfomation?.avatar != nil && self.userInfomation?.avatar != ""
                    {
                        let imageScale : String = self.userInfomation!.avatar! as String + "?imageView2/0/w/" + "\(Int(44*Constant.ScreenSize.SCALE_SCREEN))" + "/h/" + "\(Int(44*Constant.ScreenSize.SCALE_SCREEN))"
                        avatarView.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "accountIcon"))
                    }
                }
                else{
                    if self.userInfomation != nil{
                        if self.userInfomation!.avatarData != nil{
                            avatarView.image = UIImage(data: self.userInfomation!.avatarData! as Data)
                        }
                    }
                }
            }
            else if (indexPath as NSIndexPath).row == 1{
                cell!.textLabel?.text = "昵称"
                if UserInfoManager.didLogin == true{
                    cell?.textLabel?.textColor = UIColor.black
                    cell?.detailTextLabel?.textColor = UIColor.black
                    if self.userInfomation != nil{
                        if userInfomation?.nick_name != "" && userInfomation?.nick_name != nil{
                            cell!.detailTextLabel?.text = userInfomation?.nick_name
                        }
                        else{
                            cell!.detailTextLabel?.text = "未设置"
                        }
                    }
                }else{
                    cell?.textLabel?.textColor = UIColor.lightGray
                    cell?.detailTextLabel?.textColor = UIColor.lightGray
                    cell!.detailTextLabel?.text = "未设置"
                }
                
            }
            else if (indexPath as NSIndexPath).row == 2{
                cell!.textLabel?.text = "性别"
                if self.userInfomation != nil{
                    if let gender = self.userInfomation?.gender {
                        cell!.detailTextLabel?.text = [0: "未设置",1: "男", 2: "女"][gender]
                    }
                }
                else{
                    cell!.detailTextLabel?.text = "未设置"
                }
            }
            else if (indexPath as NSIndexPath).row == 3{
                cell!.textLabel?.text = "生日"
                if self.userInfomation != nil{
                    if userInfomation?.birthday != nil && userInfomation?.birthday != ""{
                        if let showLunar = userInfomation?.birthday_type{
                            if userInfomation?.birthday != ""{
                                if showLunar == 2 {
                                    if (userInfomation!.birthday! as NSString).substring(to: 4) == "1600"
                                    {
                                        let formatter = DateFormatter()
                                        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                        let unitFlags: NSCalendar.Unit = [.day, .month]
                                        let components = (Calendar.current as NSCalendar).components(unitFlags, from: formatter.date(from: userInfomation!.birthday!)!)
                                        cell!.detailTextLabel?.text = String(format: "%d-%d", components.month!,components.day!)
                                        
                                    }
                                    else{
                                        
                                        if CalendarUtil.isleapMonth((userInfomation?.birthday)!) == true
                                        {
                                            let fornt = CalendarUtil.subString(CalendarUtil.alTolunarYear(userInfomation?.birthday), withTo: 9)
                                            
                                            let rest = CalendarUtil.subString(CalendarUtil.alTolunarYear(userInfomation?.birthday), withFrom: 9)
                                            
                                            cell!.detailTextLabel?.text = fornt! + "闰" + rest!
                                            
                                        }else
                                        {
                                            let time2 = CalendarUtil.alTolunarYear(userInfomation?.birthday)
                                            
                                            cell!.detailTextLabel?.text = time2
                                        }
                                    }
                                    
                                }
                                else
                                {
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                    let endDate: Date? = formatter.date(from: (userInfomation?.birthday)!)
                                    let gregorian = Calendar(identifier: Calendar.Identifier.gregorian)
                                    let flags: NSCalendar.Unit = [NSCalendar.Unit.year,NSCalendar.Unit.month,NSCalendar.Unit.day]
                                    if let temp = endDate {
                                        let components = (gregorian as NSCalendar).components(flags, from: temp)
                                        if (userInfomation!.birthday! as NSString).substring(to: 4) == "1600"
                                        {
                                            let unitFlags: NSCalendar.Unit = [.day, .month]
                                            let components = (Calendar.current as NSCalendar).components(unitFlags, from: formatter.date(from: userInfomation!.birthday!)!)
                                            cell!.detailTextLabel?.text = String(format: "%d-%d", components.month!,components.day!)
                                        }
                                        else{
                                            
                                            cell!.detailTextLabel?.text = "\(components.year!) 年 \(components.month!) 月 \(components.day!) 日"
                                        }
                                    }else {
                                        cell!.detailTextLabel?.text = "未设置"
                                    }
                                    
                                }
                            }
                        }
                    }
                    else{
                        cell!.detailTextLabel?.text = "未设置"
                    }
                }
                else{
                    cell!.detailTextLabel?.text = "未设置"
                }
            }
            else if (indexPath as NSIndexPath).row == 4{
                cell!.textLabel?.text = "手机号"
                if UserInfoManager.didLogin == true{
                    if  userInfomation?.user_name != "" && userInfomation?.user_name != nil{
                        cell!.detailTextLabel?.text = userInfomation?.user_name
                    }
                }
                else{
                    cell!.detailTextLabel?.text = "登录绑定"
                }
            }
        }
        else if (indexPath as NSIndexPath).section == 1{
            cell!.accessoryType = UITableViewCellAccessoryType.none
            if (indexPath as NSIndexPath).row == 0{
                cell?.textLabel!.text = "微信"
                var isBind_wx = false
                var isBind = false
                if let temp = self.userInfomation {
                    isBind = temp.wx_bind_status
                }
                isBind_wx = isBind
                cell?.imageView?.image =  isBind_wx ? UIImage(named: "userCenterWeiXinBinded") : UIImage(named: "userCenterWeiXinUnBind")
                
                if isBind_wx == false{
                    cell?.detailTextLabel!.text = "未绑定"
                }
                else{
                    cell?.detailTextLabel?.text = "已绑定"
                    
                }
                
                cell?.detailTextLabel?.textColor = isBind_wx ? UIColor.black : UIColor.red
            }
            else if (indexPath as NSIndexPath).row == 1{
                cell?.textLabel!.text = "QQ"
                var isBind_qq = false
                var isBind = false
                if let temp = self.userInfomation {
                    isBind = temp.qq_bind_status
                }
                isBind_qq = isBind
                
                cell?.imageView?.image = isBind_qq ?  UIImage(named: "userCenterQQBinded") : UIImage(named: "userCenterQQUnBind")
                if isBind_qq == false{
                    cell?.detailTextLabel!.text = "未绑定"
                }else{
                    cell?.detailTextLabel?.text  = "已绑定"
                }
                
                cell?.detailTextLabel!.textColor = isBind_qq ? UIColor.black : UIColor.red
            }
        }
        else if (indexPath as NSIndexPath).section == 2{
            cell!.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
            if userInfomation!.has_password{
                cell!.textLabel?.text = "修改密码"
            }
            else{
                cell!.textLabel?.text = "设置密码"
            }
        }
        return cell!;
    }
    
    func numberOfSections(in tableView: UITableView) -> Int{
        if UserInfoManager.didLogin == true{
            return 3
        }
        else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if (indexPath as NSIndexPath).section == 0{
            if (indexPath as NSIndexPath).row == 0{

                picker?.delegate = self
                picker!.allowsEditing = true
                LCActionSheet(title: nil, buttonTitles: ["拍照","我的相册"], redButtonIndex: -1, clicked: { (buttonIndex: Int) -> Void in
                    
                    if buttonIndex == 1
                    {
                        let authStatus:AVAuthorizationStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
                        
                        if(authStatus == AVAuthorizationStatus.denied || authStatus == AVAuthorizationStatus.restricted) {
                            let alertController = UIAlertController(title: "温馨提示",
                                                                    message: "请到“设置-隐私-相机”中打开心意点点获取手机相机的授权才能使用此功能", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "好的", style: .default,
                                                         handler: {
                                                            action in
                                                            return
                            })
                            
                            alertController.addAction(okAction)
                            self.present(alertController, animated: true, completion: {
                                
                            })
                        }else {
                            self.openCamera()
                        }
                    }
                    if buttonIndex == 2
                    {
                        let library:PHAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
                        if(library == PHAuthorizationStatus.denied || library == PHAuthorizationStatus.restricted){
                            let alertController = UIAlertController(title: "温馨提示",
                                                                    message: "请到“设置-隐私-照片”中打开心意点点读取手机相册的授权才能使用此功能", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "好的", style: .default,
                                                         handler: {
                                                            action in
                                                            return
                            })
                            
                            alertController.addAction(okAction)
                            self.present(alertController, animated: true, completion: {
                                
                            })
                            
                        }else {
                            
                            self.openGallary()
                        }

                    }
                    
                }).show()
            }
            else if (indexPath as NSIndexPath).row == 1{
                if UserInfoManager.didLogin == true{
                    let unvc = UpdateNickNameViewController()
                    self.navigationController?.pushViewController(unvc, animated: true)
                }
                else{
                    Utils.showError(context: self.view, errorStr: "登录后才可设置昵称")
                }
            }
            else if (indexPath as NSIndexPath).row == 2{
                let usvc = UpdateGenderCellViewController()
                self.navigationController?.pushViewController(usvc, animated: true)
            }
            else if (indexPath as NSIndexPath).row == 3{
                let currentDate: Date?
                var currentType: Bool = false
                
                let requestUserInfo: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
                
                let datasourceUserInfo : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(requestUserInfo)!
                
                if datasourceUserInfo.count > 0{
                    
                    let userInfo = datasourceUserInfo[0] as? User
                    let birthday_type : Int = Int((userInfo?.birthday_type)!)
                    currentType = birthday_type == 2 ? true : false
                    if userInfo?.birthday != "" && userInfo?.birthday != nil{
                        let formatter = DateFormatter()
                        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                        currentDate = formatter.date(from: (userInfo?.birthday)!)
                    }else
                    {
                        currentDate = CalendarUtil.string(toSolarDate: "1984-01-01")
                        currentType = false
                    }
                }
                else
                {
                    currentDate = CalendarUtil.string(toSolarDate: "1984-01-01")
                    currentType = false
                }
                
                let picker = DatePickerView(frame: CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: 300), type: currentType, display: currentDate, yearStart: 1905, yearEnd: 2049,toolBarHeight:Constant.TOOLBAR_HEIGHT,pickerHeight:Constant.PICKER_HEIGHT,isGuideBirthdayView:false)
                picker?.hideYearButton.isHidden = true
                
                picker?.delegate = self
                self.presentSemiView(picker, withOptions:nil, completion: {
                    
                })
            }
            else if (indexPath as NSIndexPath).row == 4{
                if UserInfoManager.didLogin == true{
                    let bindPhone = ConfirmPhoneViewController(nibName: "ConfirmPhoneViewController", bundle: nil)
                    self.navigationController?.pushViewController(bindPhone, animated: true)
                }
                else{
                    self.inLogin()
                }
                
            }
        }
        else if (indexPath as NSIndexPath).section == 1{
            if (indexPath as NSIndexPath).row == 0{
                var isBind_wx = false
                var userInfoWithWX : User? = nil
                let requestUserInfo: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
                
                let datasourceUserInfo : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(requestUserInfo)!
                
                if datasourceUserInfo.count > 0{
                    
                    userInfoWithWX = datasourceUserInfo[0] as? User
                }
                
                
                let isBind : Bool = userInfoWithWX!.wx_bind_status
                isBind_wx = isBind
                if isBind_wx
                {
                    let alertView = UIAlertController(title: "解除绑定", message: "是否解除账号与微信的关联？解除后将无法使用微信登录此账号。", preferredStyle: .alert)
                    alertView.addAction(UIAlertAction(title: "确定", style: .default, handler: { (alertAction) -> Void in
                        if userInfoWithWX?.wx_bind_id != nil{
                        self.unBindOpen(3,bind_id: (userInfoWithWX?.wx_bind_id)!)
                        }
                    }))
                    alertView.addAction(UIAlertAction(title: "取消", style: .cancel, handler: nil))
                    present(alertView, animated: true, completion: nil)
                    
                }else{
                    self.loginWithWechat()
                }
            }
            else if (indexPath as NSIndexPath).row == 1{
                var isBind_qq = false
                var userInfoWithQQ : User? = nil
                let requestUserInfo: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
                
                let datasourceUserInfo : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(requestUserInfo)!
                
                if datasourceUserInfo.count > 0{
                    
                    userInfoWithQQ = datasourceUserInfo[0] as? User
                }
                
               let isBind : Bool = userInfoWithQQ!.qq_bind_status
                isBind_qq = isBind
                if isBind_qq {
                    let alertView = UIAlertController(title: "解除绑定", message: "是否解除账号与QQ的关联？解除后将无法使用QQ登录此账号。", preferredStyle: .alert)
                    alertView.addAction(UIAlertAction(title: "确定", style: .default, handler: { (alertAction) -> Void in
                        self.unBindOpen(2,bind_id: (userInfoWithQQ?.qq_bind_id)!)
                    }))
                    alertView.addAction(UIAlertAction(title: "取消", style: .cancel, handler: nil))
                    present(alertView, animated: true, completion: nil)
                    
                }else
                {
                 self.loginWithQQ()
                }
            }
        }
        else if (indexPath as NSIndexPath).section == 2{
            if self.userInfomation?.has_password == true{
                let updvc = UpdatePasswdViewController()
                self.navigationController?.pushViewController(updvc, animated: true)
            }
            else{
                //设置密码
                let sp = SetPassWordViewController(nibName: "SetPassWordViewController", bundle: nil)
                self.navigationController?.pushViewController(sp, animated: true)
            }
        }
    }
    
    
  func loginWithWechat()
    {
        let wechatAuth : LDSDKWXServiceImpl = LDSDKManager.getAuthService(LDSDKPlatformType.weChat) as! LDSDKWXServiceImpl
        
        wechatAuth.loginToPlatform (callback: {(oauthInfo,userInfoData,error) in
            if error == nil{
                if (userInfoData != nil && oauthInfo != nil) {
                    let json = (userInfoData as Any) as! NSDictionary
                    
                    // 保存微信内获取到的 用户昵称
                    let request: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
                    let datasource : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(request)!
                    if datasource.count > 0{
                        
                        let userInfo = datasource[0] as! User
                        if let headImageUrl = json.object(forKey: "headimgurl") as? String{
                            userInfo.wx_head_image_url = headImageUrl
                        }
                        if let nickName = json.object(forKey: "nickname") as? String{
                            userInfo.qq_nickName = nickName
                            if userInfo.nick_name == nil || userInfo.nick_name == ""{
                                userInfo.nick_name = nickName
                            }
                            
                        }
                        if let gender = json.object(forKey: "sex") as? Int{
                            if userInfo.gender == 0{
                            userInfo.gender = Int64(gender)
                            }
                        }
                    }
                    else{
                        let entity = NSEntityDescription.entity(forEntityName: "User", in: CoreDataManager.shared.managedObjectContext)
                        request.entity = entity
                        do {
                            try! CoreDataManager.shared.managedObjectContext.fetch(request)
                            let user: User = NSEntityDescription.insertNewObject(forEntityName: "User", into: CoreDataManager.shared.managedObjectContext) as! User
                            if let headImageUrl = json.object(forKey: "headimgurl") as? String{
                                user.wx_head_image_url = headImageUrl
                            }
                            if let nickName = json.object(forKey: "nickname") as? String{
                                user.qq_nickName = nickName
                            }
                            if let gender = json.object(forKey: "sex") as? Int{
                                user.gender = Int64(gender)
                            }
                        }
                    }
                    CoreDataManager.shared.save({Void in
                        self.reloadUserInfo()
                        self.tableView.reloadData()
                        },failureHandler: {Void in
                            self.tableView.reloadData()
                            
                    })
                    if let gender = json.object(forKey: "sex") as? Int{
                        if self.userInfomation?.gender == 0{
                        self.uploadSex(gender)
                        }
                    }
                    let authInfo = (oauthInfo as Any) as! NSDictionary
                    self.bindOpen((authInfo.object(forKey: "openid") as? String)!,sns_token: (authInfo.object(forKey: "access_token") as? String)!, type: 3)
                }

            }
            else{
                Utils.showError(context: self.view, errorStr: (error?.localizedDescription)!)
            }
            }
        )
    }
    
   func loginWithQQ(){
        let qqAuth : LDSDKQQServiceImpl = LDSDKManager.getAuthService(LDSDKPlatformType.QQ) as! LDSDKQQServiceImpl
        
        qqAuth.loginToPlatform (callback: {(oauthInfo,userInfoData,error) in
            if error == nil{
                if (userInfoData != nil && oauthInfo != nil) {
                    let json = (userInfoData as Any) as! NSDictionary
                    
                    let request: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
                    let datasource : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(request)!
                    if datasource.count > 0{
                        
                        let userInfo = datasource[0] as! User
                        if let headImageUrl = json.object(forKey: "figureurl_qq_2") as? String{
                            userInfo.qq_head_image_url = headImageUrl
                        }
                        if let nickName = json.object(forKey: "nickname") as? String{
                            userInfo.qq_nickName = nickName
                            if userInfo.nick_name == nil || userInfo.nick_name == ""{
                                userInfo.nick_name = nickName
                            }
                        }
                        if let gender = json.object(forKey: "gender") as? String {
                            if userInfo.gender == 0{
                            if gender == "男"{
                                userInfo.gender = 1
                            }
                            else{
                                userInfo.gender = 2
                            }
                            }
                        }
                    }
                    else{
                        let entity = NSEntityDescription.entity(forEntityName: "User", in: CoreDataManager.shared.managedObjectContext)
                        request.entity = entity
                        do {
                            try! CoreDataManager.shared.managedObjectContext.fetch(request)
                            let user: User = NSEntityDescription.insertNewObject(forEntityName: "User", into: CoreDataManager.shared.managedObjectContext) as! User
                            if let headImageUrl = json.object(forKey: "figureurl_qq_2") as? String{
                                user.qq_head_image_url = headImageUrl
                            }
                            if let nickName = json.object(forKey: "nickname") as? String{
                                user.qq_nickName = nickName
                                user.nick_name = nickName
                            }
                            if let gender = json.object(forKey: "gender") as? String {
                                if gender == "男"{
                                    user.gender = 1
                                }
                                else{
                                    user.gender = 2
                                }
                            }
                        }
                    }
                    CoreDataManager.shared.save({Void in
                        self.reloadUserInfo()
                        },failureHandler: {Void in
                            
                    })
                    
                    
                    // 获取并存储用户信息
                    gcd.async(.default) {
                        var sex = 0
                        if let gender = json.object(forKey: "gender") as? String {
                            if gender == "男"{
                                sex = 1
                            }
                            else{
                                sex = 2
                            }
                        }
                        if sex != 0{
                            if self.userInfomation?.gender == 0{
                            self.uploadSex(sex)
                            }
                        }
                    }
                    let authInfo = (oauthInfo as Any) as! NSDictionary
                     self.bindOpen((authInfo.object(forKey: "openId") as? String)!,sns_token: (authInfo.object(forKey: "access_token") as? String)!, type: 2)
                }
            }
            else{
                Utils.showError(context: self.view, errorStr: (error?.localizedDescription)!)
            }
            }
        )
    }

    
    func inLogin(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.loginType = Constant.InLoginType.accountManagerType.rawValue
        
        loginDialog.controller = self
        loginDialog.show()
       
    }
    
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            BaseViewController.staticNavigationBarAppearance(true)
            picker!.sourceType = UIImagePickerControllerSourceType.camera
            picker!.cameraViewTransform = CGAffineTransform(scaleX: 1.5, y: 1.5)//缩放控制
            self .present(picker!, animated: true, completion: nil)
        }
        else
        {
            openGallary()
        }
    }
    func openGallary()
    {
        picker!.sourceType = UIImagePickerControllerSourceType.photoLibrary
        BaseViewController.staticNavigationBarAppearance(true)
        self.present(picker!, animated: true, completion: nil)
        
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        UINavigationBar.appearance().tintColor = UIColor(rgba: Constant.titlebar_color)
        picker .dismiss(animated: true, completion: nil)
        if let imageOri : UIImage = info[UIImagePickerControllerEditedImage] as? UIImage{
           gcd.async(.default) {
                let newSize : CGSize = CGSize(width: Constant.ScreenSize.SCREEN_WIDTH, height: Constant.ScreenSize.SCREEN_WIDTH)
                UIGraphicsBeginImageContext(newSize)
                imageOri.draw(in: CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: Constant.ScreenSize.SCREEN_WIDTH))
                // Get the new image from the context
                let image : UIImage = UIGraphicsGetImageFromCurrentImageContext()!;
                // End the context
                UIGraphicsEndImageContext();
                DispatchQueue.main.async { // 2
                    self.uploadHeaderImage(image)
                }
            }
            
        }
        else
        {
            return
        }
        
        
        
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        UINavigationBar.appearance().tintColor = UIColor(rgba: Constant.titlebar_color)
        picker .dismiss(animated: true, completion: nil)
    }
    
    
    
    func exit(_ sender: UIButton){
        
        sender.backgroundColor = UIColor(rgba: Constant.common_C1_color)
        
        let sheet: LCActionSheet = LCActionSheet(title: "确定退出登录?", buttonTitles: ["确定"], redButtonIndex: -1, delegate: self)
        
        sheet.show()
    }
    
    // 点击按钮 按钮背景颜色变化
    func changeBackColor(_ sender: UIButton)
    {
        sender.backgroundColor = UIColor(rgba: Constant.common_C100_color)
    }
    
    func actionSheet(_ actionSheet: LCActionSheet!, didClickedButtonAt buttonIndex: Int) {
        
        if buttonIndex == 1{
            let user = CoreDataManager.shared.getDBManager(Constant.CoreDataType.userInfoType) as? User
            
            CoreDataManager.shared.deleteEntity(user!)
            
            CoreDataManager.shared.save({Void in
                
                self.backToUserCenter()
                },failureHandler: {Void in
                    self.backToUserCenter()
            })
            
            //清除送礼提醒数据
            let homeAAGiftChoice = CoreDataManager.shared.getDBManager(Constant.CoreDataType.homeAAGiftChoiceType) as? HomeAAGiftChoice
            
            homeAAGiftChoice?.homeRmind = nil
            
            AccountManager.shared.removeParentReminder()//还原父母设置信息
            
            // 退出登录，清空用户账号信息
            NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.notification_logout_success), object: nil)
            
            //退出登录需要刷新个人中心礼物篮的数据
            UserCenterViewController().isfirstLoad = true
            //注销
            QYSDK.shared().logout({
            })
        }
    }
    
    func backToUserCenter(){
        UserDefaults.standard.removeObject(forKey: "UserToken")
        UserDefaults.standard.removeObject(forKey: "UserUid")
        UserDefaults.standard.synchronize()
        for temp in self.navigationController!.viewControllers{
            if temp.isKind(of: UserCenterViewController.self){
               let _ = self.navigationController?.popToViewController(temp, animated: true)
            }
            
        }
    }
    
    
    // MARK: 调用 API
    
    // 上传头像
    func uploadHeaderImage(_ image: UIImage?)
    {
        let imageData = UIImageJPEGRepresentation(image!, 0.8)
        if UserInfoManager.didLogin == true{
            ImageUploader.uploadimage(context:self.view,data: imageData!) { [weak self](id,domain) -> Void in
                if id != nil
                {
                    let imageName = id
                   let _ = LMRequestManager.requestByDataAnalyses(context:self!.view,URLString: Constant.JTAPI.user_avatar, parameters: ["avatar": imageName! as AnyObject], isNeedUserTokrn: true, isShowErrorStatuMsg: true, isNeedHud: true,successHandler: {data, status, msg in
                        if status == 200
                        {
                              self?.saveImageUrlToUser(domain! + id!)
                        }
                        })
                }
                

            }
        }
        else{
            saveImageDataToUser(imageData!,image:image)
        }
    }
    
    
    func saveImageUrlToUser(_ birthdayUrl : String){
        let request: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        let datasource : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(request)!
        if datasource.count > 0{
            
            let userInfo = datasource[0] as! User
            userInfo.avatar = birthdayUrl
        }
        else{
            
            let request: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
            let entity = NSEntityDescription.entity(forEntityName: "User", in: CoreDataManager.shared.managedObjectContext)
            request.entity = entity
            do {
                try! CoreDataManager.shared.managedObjectContext.fetch(request)
                let user: User = NSEntityDescription.insertNewObject(forEntityName: "User", into: CoreDataManager.shared.managedObjectContext) as! User
                user.avatar = birthdayUrl
            }
        }
        CoreDataManager.shared.save({Void in
            self.reloadUserInfo()
            let indexPath = IndexPath(row: 0, section: 0)
            self.tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
            },failureHandler: {Void in
                let indexPath = IndexPath(row: 0, section: 0)
                self.tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
                
        })
    }
    
    
    func saveImageDataToUser(_ imageData : Data,image: UIImage?){
        let request: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        let datasource : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(request)!
        if datasource.count > 0{
            
            let userInfo = datasource[0] as! User
            userInfo.avatarData = imageData
        }
        else{
            
            let request: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
            let entity = NSEntityDescription.entity(forEntityName: "User", in: CoreDataManager.shared.managedObjectContext)
            request.entity = entity
            do {
                try! CoreDataManager.shared.managedObjectContext.fetch(request)
                let user: User = NSEntityDescription.insertNewObject(forEntityName: "User", into: CoreDataManager.shared.managedObjectContext) as! User
                user.avatarData = imageData
            }
        }
        CoreDataManager.shared.save({Void in
            self.reloadUserInfo()
            let indexPath = IndexPath(row: 0, section: 0)
            self.tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
            },failureHandler: {Void in
                let indexPath = IndexPath(row: 0, section: 0)
                self.tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
                
        })
    }
    
    
    func uploadSex(_ sex: Int){
       let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.user_gender, parameters: ["gender": sex as AnyObject], isNeedUserTokrn: true,successHandler: {data, status, msg in
            self.tableView.reloadData()
        })
    }
    
    
    // MARK: 解除绑定第三方账号
    func unBindOpen(_ type: Int, bind_id: String)
    {
       let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.user_unbind_open, parameters: ["type": type as AnyObject,"open_id": bind_id as AnyObject], isNeedUserTokrn: true,isShowErrorStatuMsg: true, isNeedHud: true, successHandler: {data, status, msg in
            if status != 200
            {
                return
            }
            
            if type == 3
            {
                
                let request: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
                let datasource : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(request)!
                if datasource.count > 0{
                    
                    let userInfo = datasource[0] as! User
                    userInfo.wx_bind_status = false
                }
                else{
                    let entity = NSEntityDescription.entity(forEntityName: "User", in: CoreDataManager.shared.managedObjectContext)
                    request.entity = entity
                    do {
                        try! CoreDataManager.shared.managedObjectContext.fetch(request)
                        let user: User = NSEntityDescription.insertNewObject(forEntityName: "User", into: CoreDataManager.shared.managedObjectContext) as! User
                        user.wx_bind_status = false
                    }
                }
                
            }else
            {
                let request: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
                let datasource : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(request)!
                if datasource.count > 0{
                    
                    let userInfo = datasource[0] as! User
                    userInfo.qq_bind_status = false
                }
                else{
                    let entity = NSEntityDescription.entity(forEntityName: "User", in: CoreDataManager.shared.managedObjectContext)
                    request.entity = entity
                    do {
                        try! CoreDataManager.shared.managedObjectContext.fetch(request)
                        let user: User = NSEntityDescription.insertNewObject(forEntityName: "User", into: CoreDataManager.shared.managedObjectContext) as! User
                        user.qq_bind_status = false
                    }
                }
                
            }
            CoreDataManager.shared.save({Void in
                self.reloadUserInfo()
                self.tableView.reloadData()
                },failureHandler: {Void in
                    self.tableView.reloadData()
                    
            })
            
        })
    }
    
    
    
    // MARK: 邦定第三方账号
    func bindOpen(_ openID: String,sns_token:String, type: Int) {
       let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.user_bind_open, parameters: ["open_id": openID as AnyObject,"sns_token":sns_token as AnyObject, "type": type as AnyObject], isNeedUserTokrn: true,isShowErrorStatuMsg: true, isNeedHud: true,successHandler: {data, status, msg in
            if status != 200 {
                return
            }
        self.getUserInfo()
            if type == 3{
                let request: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
                let datasource : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(request)!
                if datasource.count > 0{
                    
                    let userInfo = datasource[0] as! User
                    userInfo.wx_bind_status = true
                }
                else{
                    let entity = NSEntityDescription.entity(forEntityName: "User", in: CoreDataManager.shared.managedObjectContext)
                    request.entity = entity
                    do {
                        try! CoreDataManager.shared.managedObjectContext.fetch(request)
                        let user: User = NSEntityDescription.insertNewObject(forEntityName: "User", into: CoreDataManager.shared.managedObjectContext) as! User
                        user.wx_bind_status = true
                    }
                }
                CoreDataManager.shared.save({Void in
                    self.reloadUserInfo()
                    },failureHandler: {Void in
                        
                })
                
            }else{
                let request: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
                let datasource : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(request)!
                if datasource.count > 0{
                    
                    let userInfo = datasource[0] as! User
                    userInfo.qq_bind_status = true
                }
                else{
                    let entity = NSEntityDescription.entity(forEntityName: "User", in: CoreDataManager.shared.managedObjectContext)
                    request.entity = entity
                    do {
                        try! CoreDataManager.shared.managedObjectContext.fetch(request)
                        let user: User = NSEntityDescription.insertNewObject(forEntityName: "User", into: CoreDataManager.shared.managedObjectContext) as! User
                        user.qq_bind_status = true
                    }
                }
                CoreDataManager.shared.save({Void in
                    self.reloadUserInfo()
                    },failureHandler: {Void in
                        
                })
            }
            
            self.tableView.reloadData()
        })
        
    }
    
    func getUserInfo()
    {
        
        UserInfoManager.shareUserInfoManager().updateUserInfo(self.view){(success) ->Void in
            
            if success == true
            {
                let requestUserInfo: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
                
                let datasourceUserInfo : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(requestUserInfo)!
                
                if datasourceUserInfo.count > 0{
                    
                    self.userInfomation = datasourceUserInfo[0] as? User
                }
                self.tableView.reloadData()
            }
        }
    }
    
}







