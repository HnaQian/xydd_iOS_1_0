//
//  LoginDialog.swift
//  Limi
//
//  Created by maohs on 16/12/8.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit
import CoreData

public protocol LoginDialogDelegate:NSObjectProtocol {
    func wechatLoginSuccess()
}
class LoginDialog: UIView {

    fileprivate let dialogView:UIView = {
        let tempView = UIView()
        tempView.backgroundColor = UIColor.white
        tempView.layer.cornerRadius = 5.0 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        tempView.layer.masksToBounds = true
        return tempView
    }()
    
    fileprivate let titleLabel:UILabel = {
        let lbl = UILabel()
        lbl.font = Constant.Theme.Font_19
        lbl.textColor = Constant.Theme.Color6
        lbl.textAlignment = .center
        lbl.text = "请先登录"
        return lbl
    }()
    
    fileprivate let rightCancelButton:UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named:"login_close"), for: UIControlState())
        return btn
    }()
    
    fileprivate let wechatImageView:UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "login_wechat")
        return image
    }()
    
    fileprivate let iphoneImageView:UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "login_iphone")
        return image
    }()
    
    fileprivate let wechatNameLabel:UILabel = {
        let lbl = UILabel()
        lbl.font = Constant.Theme.Font_16
        lbl.textColor = Constant.Theme.Color6
        lbl.textAlignment = .center
        lbl.text = "微信登录"
        return lbl
    }()
    
    fileprivate let iphoneNameLabel:UILabel = {
        let lbl = UILabel()
        lbl.font = Constant.Theme.Font_16
        lbl.textColor = Constant.Theme.Color6
        lbl.textAlignment = .center
        lbl.text = "免密码登录/注册"
        return lbl
    }()
    
    var controller:UIViewController?
    var userInfoation : User?
    weak var delegate:LoginDialogDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }
    
    func setupUI() {
        self.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        self.alpha = 0
        
        self.addSubview(dialogView)
        
        for view in [titleLabel,rightCancelButton,wechatImageView,iphoneImageView,wechatNameLabel,iphoneNameLabel] {
            dialogView.addSubview(view)
        }
        
        let leftTapView = UIView()
        let rightTapView = UIView()
        dialogView.addSubview(leftTapView)
        dialogView.addSubview(rightTapView)
        
        let scale = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        dialogView.frame = CGRect(x: (Constant.ScreenSizeV2.SCREEN_WIDTH - 644 * scale) / 2, y: (Constant.ScreenSizeV2.SCREEN_HEIGHT - 515 * scale) / 2, width: 644 * scale, height: 515 * scale)
        
        titleLabel.frame = CGRect(x: 30 * scale, y: 44 * scale, width: dialogView.iwidth - 60 * scale, height: 24)
        rightCancelButton.frame = CGRect(x: dialogView.iwidth - 25 * scale - 30, y: 44 * scale, width: 30, height: 30)
        rightCancelButton.icenterY = titleLabel.icenterY
        
        wechatImageView.frame = CGRect(x: 102 * scale, y: titleLabel.ibottom + 100 * scale, width: 170 * scale, height: 170 * scale)
        iphoneImageView.frame = CGRect(x: wechatImageView.iright + 100 * scale, y: titleLabel.ibottom + 100 * scale, width: 170 * scale, height: 170 * scale)
        
        wechatNameLabel.frame = CGRect(x: 50 * scale, y: wechatImageView.ibottom + 60 * scale, width: dialogView.iwidth / 2 - 50 * scale, height: 20)
        iphoneNameLabel.frame = CGRect(x: dialogView.iwidth / 2, y:  wechatNameLabel.itop, width: wechatNameLabel.iwidth, height: 20)
        
        leftTapView.frame = CGRect(x: 52 * scale, y: titleLabel.ibottom + 100 * scale, width: 270 * scale, height: (170 + 60) * scale + 20)
        rightTapView.frame = CGRect(x:  dialogView.iwidth / 2, y: leftTapView.itop, width: 270 * scale, height: (170 + 60) * scale + 20)
        
        rightCancelButton.addTarget(self, action: #selector(LoginDialog.rightCancelButtonOnClicked), for: .touchUpInside)
        
        //微信
        let leftTapGesture = UITapGestureRecognizer(target: self, action: #selector(LoginDialog.leftTapViewTaped))
        leftTapView.addGestureRecognizer(leftTapGesture)
        
        //手机
        let rightTapGesture = UITapGestureRecognizer(target: self, action: #selector(LoginDialog.rightTapViewTaped))
        rightTapView.addGestureRecognizer(rightTapGesture)
    }
    
    func show() {
        let requestUserInfo: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        let datasourceUserInfo : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(requestUserInfo)!
        if datasourceUserInfo.count > 0{
            userInfoation = datasourceUserInfo[0] as? User
        }
        else{
            userInfoation = nil
        }
        if self.superview == nil {
            if let window = UIApplication.shared.keyWindow {
                self.frame = window.bounds
                window.addSubview(self)
            }
        }
        
        UIView.animate(withDuration: 0.25, animations: {
            self.dialogView.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.alpha = 1
        }, completion: { (done) in
        })
    }
    
    func dismiss() {
        UIView.animate(withDuration: 0.25, animations: {
            self.alpha = 0
        }, completion: { (done) in
            self.dialogView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        })
    }
    
    func rightCancelButtonOnClicked() {
       dismiss()
    }
    
    func leftTapViewTaped() {
        //微信登录
        dismiss()
        loginWithWechat()
    }
    
    func rightTapViewTaped() {
        //手机号登录
        dismiss()
        inLogin()
    }
    
    func inLogin(){
        let animation = CATransition()
        
        animation.duration = 0.3
        
        animation.type = kCATransitionMoveIn
        
        animation.subtype = kCATransitionFromTop
        
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        
        self.controller?.navigationController?.view.layer.add(animation, forKey: "")
        let login = LoginViewController()
        login.hidesBottomBarWhenPushed = true
        self.controller?.navigationController?.pushViewController(login, animated: false)
    }
    
    //微信账号登录
    func loginWithWechat()
    {
        let wechatAuth : LDSDKWXServiceImpl = LDSDKManager.getAuthService(LDSDKPlatformType.weChat) as! LDSDKWXServiceImpl
        
        wechatAuth.loginToPlatform (callback: {(oauthInfo,userInfoData,error) in
            if error == nil{
                if (userInfoData != nil && oauthInfo != nil) {
                    let json = (userInfoData as Any) as! NSDictionary
                    
                    // 保存微信内获取到的 用户昵称
                    let request: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
                    let datasource : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(request)!
                    if datasource.count > 0{
                        
                        let userInfo = datasource[0] as! User
                        if let headImageUrl = json.object(forKey: "headimgurl") as? String{
                            userInfo.wx_head_image_url = headImageUrl
                        }
                        if let nickName = json.object(forKey: "nickname") as? String{
                            userInfo.qq_nickName = nickName
                            if userInfo.nick_name == nil || userInfo.nick_name == ""{
                                userInfo.nick_name = nickName
                            }
                        }
                        if let gender = json.object(forKey: "sex") as? Int{
                            if userInfo.gender == 0{
                                userInfo.gender = Int64(gender)
                            }
                        }
                    }
                    else{
                        let entity = NSEntityDescription.entity(forEntityName: "User", in: CoreDataManager.shared.managedObjectContext)
                        request.entity = entity
                        do {
                            try! CoreDataManager.shared.managedObjectContext.fetch(request)
                            let user: User = NSEntityDescription.insertNewObject(forEntityName: "User", into: CoreDataManager.shared.managedObjectContext) as! User
                            if let headImageUrl = json.object(forKey: "headimgurl") as? String{
                                user.wx_head_image_url = headImageUrl
                            }
                            if let nickName = json.object(forKey: "nickname") as? String{
                                user.qq_nickName = nickName
                                if user.nick_name == nil || user.nick_name == ""{
                                    user.nick_name = nickName
                                }
                            }
                            if let gender = json.object(forKey: "sex") as? Int{
                                user.gender = Int64(gender)
                            }
                        }
                    }
                    CoreDataManager.shared.save()
                    let authInfo = (oauthInfo as Any) as! NSDictionary
                    self.getUserInfoWithOpen((authInfo.object(forKey: "openid") as? String)!,sns_token: (authInfo.object(forKey: "access_token"))! as! String, type: 3)
                }
                
            }
        }
        )
    }
    
    func getUserInfoWithOpen(_ openId : String,sns_token:String,type : Int){
        var params = [String: AnyObject]()
        params["open_id"] = openId as AnyObject?
        params["type"] = type as AnyObject?
        params["app_type"] = 1 as AnyObject?
        params["sns_token"] = sns_token as AnyObject?
        let _ = LMRequestManager.requestByDataAnalyses(context:self,URLString: Constant.JTAPI.login_open, parameters: params, isNeedAppTokrn: true, isShowErrorStatuMsg: true, isNeedHud: true,successHandler: {data, status, msg in
            print(data)
            if status == 201{
                let loginWithWechatVC = LoginWithWechatViewController()
                loginWithWechatVC.isOutLogin = false
                loginWithWechatVC.open_id = openId
                loginWithWechatVC.accessToken = sns_token
                loginWithWechatVC.hidesBottomBarWhenPushed = true
                self.controller?.navigationController?.pushViewController(loginWithWechatVC, animated: true)
            }
            else if status == 200{
                self.SynchronizeUserAccountInfo(false)
                
            }
            
        })
        
    }
    
    //MARK: 同步用户信息
    func SynchronizeUserAccountInfo(_ is_reg : Bool){
        
        if userInfoation != nil{
            if userInfoation!.avatarData != nil{
                self.uploadHeaderImage(userInfoation!.avatarData! as Data,is_reg: is_reg)
            }
            else if userInfoation!.gender != 0 || userInfoation!.birthday != "" || userInfoation!.birthday_type != 0{
                var userGender : Int = 0
                var userBirthday_type : Int = 0
                var userBirthday : String = ""
                if userInfoation?.gender != 0 && userInfoation?.gender != nil{
                    userGender = Int(userInfoation!.gender)
                }
                if userInfoation?.birthday_type != 0 && userInfoation?.birthday_type != nil{
                    userBirthday_type = Int(userInfoation!.birthday_type)
                }
                if userInfoation?.birthday != nil && userInfoation?.birthday != ""{
                    userBirthday = userInfoation!.birthday!
                }
                self.uploadUserInfo(userGender, birthday_type: userBirthday_type, birthday: userBirthday, avatar: "",is_reg: is_reg)
            }
            else{
                self.getUserInfo(is_reg)
            }
        }
        else{
            self.getUserInfo(is_reg)
        }
    }
    
    //MARK: 上传头像
    func uploadHeaderImage(_ imageData: Data,is_reg : Bool)
    {
        var isUpoadSuccess = true
        ImageUploader.uploadimage(context:self,data: imageData) { [weak self](id,domain) -> Void in
            if id == nil
            {
                isUpoadSuccess = false
            }
            
            if isUpoadSuccess{
                if self!.userInfoation!.gender != 0 || self!.userInfoation!.birthday != "" || self!.userInfoation!.birthday_type != 0{
                    var userGender : Int = 0
                    var userBirthday_type : Int = 0
                    var userBirthday : String = ""
                    if self!.userInfoation?.gender != 0 && self!.userInfoation?.gender != nil{
                        userGender = Int(self!.userInfoation!.gender)
                    }
                    if self!.userInfoation?.birthday_type != 0 && self!.userInfoation?.birthday_type != nil{
                        userBirthday_type = Int(self!.userInfoation!.birthday_type)
                    }
                    if self!.userInfoation?.birthday != "" && self!.userInfoation?.birthday != nil{
                        userBirthday = self!.userInfoation!.birthday!
                    }
                    self!.uploadUserInfo(userGender, birthday_type: userBirthday_type, birthday: userBirthday, avatar: id!,is_reg: is_reg)
                }
                else{
                    self!.getUserInfo(is_reg)
                }
            }
            else{
                if self!.userInfoation!.gender != 0 || self!.userInfoation!.birthday != "" || self!.userInfoation!.birthday_type != 0{
                    var userGender : Int = 0
                    var userBirthday_type : Int = 0
                    var userBirthday : String = ""
                    if self!.userInfoation?.gender != 0 && self!.userInfoation?.gender != nil{
                        userGender = Int(self!.userInfoation!.gender)
                    }
                    if self!.userInfoation?.birthday_type != 0 && self!.userInfoation?.birthday_type != nil{
                        userBirthday_type = Int(self!.userInfoation!.birthday_type)
                    }
                    if self!.userInfoation?.birthday != "" && self!.userInfoation?.birthday != nil{
                        userBirthday = self!.userInfoation!.birthday!
                    }
                    self!.uploadUserInfo(userGender, birthday_type: userBirthday_type, birthday: userBirthday, avatar: "",is_reg: is_reg)
                }
                else{
                    self!.getUserInfo(is_reg)
                }
            }
            
        }
        
    }
    
    
    //MARK: 上传用户信息
    func uploadUserInfo(_ gender : Int?,birthday_type : Int?,birthday : String?,avatar : String,is_reg : Bool){
        var params = [String: AnyObject]()
        if gender != 0{
            params["gender"] = gender as AnyObject?
        }
        if birthday_type != 0{
            params["birthday_type"] = birthday_type as AnyObject?
        }
        if birthday != ""{
            params["birthday"] = birthday as AnyObject?
        }
        if avatar != ""{
            params["avatar"] = avatar as AnyObject?
        }
        let _ = LMRequestManager.requestByDataAnalyses(context:self,URLString: Constant.JTAPI.syn_userSetting, parameters: params, isNeedUserTokrn: true,isShowErrorStatuMsg: true, isNeedHud: true,successHandler: { (data, status, msg) -> Void in
            if status == 200
            {
                self.getUserInfo(is_reg)
            }
        })
        
    }
    
    //MARK: 获取用户信息
    func getUserInfo(_ is_reg : Bool){
        let _ = LMRequestManager.requestByDataAnalyses(context:self,URLString: Constant.JTAPI.user_info_v2,isNeedUserTokrn: true, isShowErrorStatuMsg: true, isNeedHud: true, hudTitle: "",successHandler: { (data, status, msg) -> Void in
            if status == 200
            {
                
                let user = JSON(data as AnyObject)["data"]["user"]
                CoreDataManager.shared.update(user, entityType: Constant.CoreDataType.userInfoType,successHandler:{Void in
                },failureHandler: {Void in
                    
                }
                )}
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.isLoginSuccess = true
//            let _ = self.controller?.navigationController?.popToViewController(self.controller!, animated: false)
            NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.notification_login_success), object: nil)
            self.delegate?.wechatLoginSuccess()
        })
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
