//
//  LoginWithPasswordViewController.swift
//  Limi
//
//  Created by maohs on 16/12/6.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit
import CoreData

class LoginWithPasswordViewController: BaseLoginViewController {
    
    fileprivate let cancelButton:UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named: "webview_cancel"), for: UIControlState.normal)
        
        return btn
    }()

    var userInfoation : User?
    var isOutLogin = false
    var phoneNumber = ""
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let requestUserInfo: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        let datasourceUserInfo : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(requestUserInfo)!
        if datasourceUserInfo.count > 0{
            userInfoation = datasourceUserInfo[0] as? User
        }
        else{
            userInfoation = nil
        }
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

   override func resetUI() {
        self.view.addSubview(cancelButton)
    
        cancelButton.frame = CGRect(x: loginBackButton.iright + 4, y: 26, width: 36, height: 36)
    
        phoneField.keyboardType = UIKeyboardType.namePhonePad
        phoneField.isSecureTextEntry = true
        phoneField.placeholder  = ""
    
        titleLabel.text = "请输入密码"
        nextButton.setTitle("登 录", for: UIControlState())
        nextButton.backgroundColor = Constant.Theme.ColorSeparatLine
        nextButton.isEnabled = false
        clearButton.isHidden = false
        clearButton.setImage(UIImage(named: "login_hide"), for: UIControlState.normal)
        clearButton.setImage(UIImage(named: "login_show"), for: UIControlState.selected)
        cancelButton.addTarget(self, action: #selector(LoginWithPasswordViewController.cancelButtonOnclicked), for: .touchUpInside)
        self.passwordLabel.text = "忘记密码"
    
    }
    
    override func loginBackButtonOnclicked() {
        let _ = self.navigationController?.popViewController(animated: false)
    }
    
    func cancelButtonOnclicked() {
        if !isOutLogin {
            if let navigationController = self.navigationController{
                var formerVC : UIViewController?
                for temp in navigationController.viewControllers{
                    if temp.isKind(of: LoginViewController.self){
                        let animation = CATransition()
                        animation.duration = 0.3
                        
                        animation.type = kCATransitionReveal
                        
                        animation.subtype = kCATransitionFromBottom
                        
                        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                        
                        self.navigationController?.view.layer.add(animation, forKey: "")
                        let _ = self.navigationController?.popToViewController(formerVC!, animated: true)
                        
                        NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.notification_login_success), object: nil)
                    }
                    formerVC = temp
                }
            }
        }else {
            NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.show_mainTabBarController), object: nil)
        }
    }
    
    override func clearButtonOnClicked() {
        clearButton.isSelected = !clearButton.isSelected
        if clearButton.isSelected {
            phoneField.isSecureTextEntry = false
        }else {
            phoneField.isSecureTextEntry = true
        }
    }
    
    override func nextButtonOnClicked() {
        if self.phoneField.text!.isEmpty {
            Utils.showError(context: self.view, errorStr: "请填写密码")
            return
        }
        else if (self.phoneField.text?.characters.count)! < 6 || (self.phoneField.text?.characters.count)! > 15{
            Utils.showError(context: self.view, errorStr: "请输入6-15位数字、字母组合")
            return
        }
        
        var params = [String: AnyObject]()
        
        params["user_name"] = phoneNumber as AnyObject?
        params["password"] = phoneField.text as AnyObject?
        
        params["app_type"] = 1 as AnyObject?
        
        let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.login, parameters: params,isNeedAppTokrn: true, isShowErrorStatuMsg: true, isNeedHud: true, hudTitle: "正在登录", successHandler: {data, status, msg in
            
            if status == 200
            {
                if let is_reg = (data["data"] as? [String: AnyObject])?["is_reg"] as? Bool
                {
                    UserDefaults.standard.setValue(is_reg, forKey: "is_reg")
                    UserDefaults.standard.synchronize()
                    
                    //如果不是第一次登录
                    if is_reg == false{
                        self.SynchronizeUserAccountInfo(false)
                    }else{
                        //第一次用短信登录
                        self.SynchronizeUserAccountInfo(true)
                    }
                    
                }else{
                    self.SynchronizeUserAccountInfo(false)
                }
            }
        })
    }
    
    //MARK: 同步用户信息
    func SynchronizeUserAccountInfo(_ is_reg : Bool){
        
        if userInfoation != nil{
            if userInfoation!.avatarData != nil{
                self.uploadHeaderImage(userInfoation!.avatarData! as Data,is_reg: is_reg)
            }
            else if userInfoation!.gender != 0 || userInfoation!.birthday != "" || userInfoation!.birthday_type != 0{
                var userGender : Int = 0
                var userBirthday_type : Int = 0
                var userBirthday : String = ""
                if userInfoation?.gender != 0 && userInfoation?.gender != nil{
                    userGender = Int(userInfoation!.gender)
                }
                if userInfoation?.birthday_type != 0 && userInfoation?.birthday_type != nil{
                    userBirthday_type = Int(userInfoation!.birthday_type)
                }
                if userInfoation?.birthday != nil && userInfoation?.birthday != ""{
                    userBirthday = userInfoation!.birthday!
                }
                self.uploadUserInfo(userGender, birthday_type: userBirthday_type, birthday: userBirthday, avatar: "",is_reg: is_reg)
            }
            else{
                self.getUserInfo(is_reg)
            }
        }
        else{
            self.getUserInfo(is_reg)
        }
    }
    
    //MARK: 上传头像
    func uploadHeaderImage(_ imageData: Data,is_reg : Bool)
    {
        var isUpoadSuccess = true
        ImageUploader.uploadimage(context:self.view,data: imageData) { [weak self](id,domain) -> Void in
            if id == nil
            {
                isUpoadSuccess = false
            }
            
            if isUpoadSuccess{
                if self!.userInfoation!.gender != 0 || self!.userInfoation!.birthday != "" || self!.userInfoation!.birthday_type != 0{
                    var userGender : Int = 0
                    var userBirthday_type : Int = 0
                    var userBirthday : String = ""
                    if self!.userInfoation?.gender != 0 && self!.userInfoation?.gender != nil{
                        userGender = Int(self!.userInfoation!.gender)
                    }
                    if self!.userInfoation?.birthday_type != 0 && self!.userInfoation?.birthday_type != nil{
                        userBirthday_type = Int(self!.userInfoation!.birthday_type)
                    }
                    if self!.userInfoation?.birthday != "" && self!.userInfoation?.birthday != nil{
                        userBirthday = self!.userInfoation!.birthday!
                    }
                    self!.uploadUserInfo(userGender, birthday_type: userBirthday_type, birthday: userBirthday, avatar: id!,is_reg: is_reg)
                }
                else{
                    self!.getUserInfo(is_reg)
                }
            }
            else{
                if self!.userInfoation!.gender != 0 || self!.userInfoation!.birthday != "" || self!.userInfoation!.birthday_type != 0{
                    var userGender : Int = 0
                    var userBirthday_type : Int = 0
                    var userBirthday : String = ""
                    if self!.userInfoation?.gender != 0 && self!.userInfoation?.gender != nil{
                        userGender = Int(self!.userInfoation!.gender)
                    }
                    if self!.userInfoation?.birthday_type != 0 && self!.userInfoation?.birthday_type != nil{
                        userBirthday_type = Int(self!.userInfoation!.birthday_type)
                    }
                    if self!.userInfoation?.birthday != "" && self!.userInfoation?.birthday != nil{
                        userBirthday = self!.userInfoation!.birthday!
                    }
                    self!.uploadUserInfo(userGender, birthday_type: userBirthday_type, birthday: userBirthday, avatar: "",is_reg: is_reg)
                }
                else{
                    self!.getUserInfo(is_reg)
                }
            }
            
        }
        
    }
    
    
    //MARK: 上传用户信息
    func uploadUserInfo(_ gender : Int?,birthday_type : Int?,birthday : String?,avatar : String,is_reg : Bool){
        var params = [String: AnyObject]()
        if gender != 0{
            params["gender"] = gender as AnyObject?
        }
        if birthday_type != 0{
            params["birthday_type"] = birthday_type as AnyObject?
        }
        if birthday != ""{
            params["birthday"] = birthday as AnyObject?
        }
        if avatar != ""{
            params["avatar"] = avatar as AnyObject?
        }
        let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.syn_userSetting, parameters: params, isNeedUserTokrn: true,isShowErrorStatuMsg: true, isNeedHud: true,successHandler: { (data, status, msg) -> Void in
            if status == 200
            {
                self.getUserInfo(is_reg)
            }
        })
        
    }
    
    //MARK: 获取用户信息
    func getUserInfo(_ is_reg : Bool){
        let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.user_info_v2,isNeedUserTokrn: true, isShowErrorStatuMsg: true, isNeedHud: true, hudTitle: "",successHandler: { (data, status, msg) -> Void in
            if status == 200
            {
                
                let user = JSON(data as AnyObject)["data"]["user"]
                CoreDataManager.shared.update(user, entityType: Constant.CoreDataType.userInfoType,successHandler:{Void in
                    self.pushAndPopView(is_reg)
                },failureHandler: {Void in
                    self.pushAndPopView(is_reg)
                }
                )}
        })
        
    }
    
   override func textFieldDidChange(_ textField: UITextField) {
        if let _ = textField.text{
            
            if textField.text!.characters.count >= 6{
                nextButton.backgroundColor = Constant.Theme.Color1
                nextButton.isEnabled = true
            }else {
                nextButton.backgroundColor = Constant.Theme.ColorSeparatLine
                nextButton.isEnabled = false
            }
        }
    }
    
    //MARK:
    func pushAndPopView(_ is_reg : Bool){
        
        if is_reg == false//如果不是第一次登录
        {
            if !self.isOutLogin{
                if let navigationController = self.navigationController{
                    var formerVC : UIViewController?
                    for temp in navigationController.viewControllers{
                        if temp.isKind(of: LoginViewController.self){
                            let animation = CATransition()
                            animation.duration = 0.3
                            
                            animation.type = kCATransitionReveal
                            
                            animation.subtype = kCATransitionFromBottom
                            
                            animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                            
                            self.navigationController?.view.layer.add(animation, forKey: "")
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.isLoginSuccess = true
                            let _ = self.navigationController?.popToViewController(formerVC!, animated: true)
                            
                            NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.notification_login_success), object: nil)
                        }
                        formerVC = temp
                    }
                }
            }
            else{
                NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.show_mainTabBarController), object: nil)
                
            }
        }
    }
    
    override func passwordLabelTaped() {
        let alertView = UIAlertController(title: "忘记密码了？", message: "您可以使用验证码登录", preferredStyle: .alert)
        alertView.addAction(UIAlertAction(title: "重置密码", style: .default, handler:
            { (alertAction) -> Void in
                let resetLoginVC = ResetLoginViewController()
                resetLoginVC.isOutLogin = self.isOutLogin
                self.navigationController?.pushViewController(resetLoginVC, animated: true)
            }))

        alertView.addAction(UIAlertAction(title: "验证码登录", style: .default, handler: { (alertAction) -> Void in
            
            let resetLoginVC = ResetLoginViewController()
            resetLoginVC.isOutLogin = self.isOutLogin
            resetLoginVC.isCodeSign = true
            self.navigationController?.pushViewController(resetLoginVC, animated: true)
        }))
        alertView.addAction(UIAlertAction(title: "取消", style: .cancel, handler: { (alertAction) -> Void in
            
        }))
        present(alertView, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
