//
//  BaseLoginViewController.swift
//  Limi
//
//  Created by maohs on 16/12/7.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class BaseLoginViewController: BaseViewController {

    let loginBackButton:UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named: "common_back_icon"), for: UIControlState.normal)
        
        return btn
    }()
    
    let titleLabel:UILabel = {
        let lbl = UILabel()
        lbl.font = Constant.Theme.Font_21
        lbl.textAlignment = .center
        lbl.text = "无需注册 一键登录"
        lbl.numberOfLines = 0
        return lbl
    }()
    
    let phoneField:CustomTextField = {
        let field = CustomTextField()
        field.placeholder = "请输入手机号码"
        field.setValue(Constant.Theme.Color9, forKeyPath: "_placeholderLabel.textColor")
        field.keyboardType = UIKeyboardType.numberPad
        field.font = UIFont.systemFont(ofSize: Constant.Theme.FontSize_19)//19
        field.textAlignment = .center
        return field
    }()
    
    let clearButton:UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named: "login_clear"), for: UIControlState.normal)
        btn.isHidden = true
        return btn
    }()
    
    let lineView:UIView = {
        let view = UIView()
        view.backgroundColor = Constant.Theme.Color9
        return view
    }()
    
    let nextButton:UIButton = {
        let btn = UIButton()
        btn.setTitle("下一步", for: UIControlState())
        btn.titleLabel?.font = UIFont.systemFont(ofSize: Constant.Theme.FontSize_19)
        btn.setTitleColor(Constant.Theme.Color12, for: UIControlState())
        btn.backgroundColor = Constant.Theme.ColorSeparatLine
        btn.layer.cornerRadius = 3
        btn.isEnabled = false
        return btn
    }()
    
    let passwordLabel:UILabel = {
        let lbl = UILabel()
        lbl.font = Constant.Theme.Font_15
        lbl.textColor = UIColor(rgba: Constant.common_C7_color)
        lbl.text = "密码登录"
        lbl.textAlignment = .center
        lbl.isUserInteractionEnabled = true
        return lbl
    }()
    
    fileprivate let protocolLabel:UILabel = {
        let lbl = UILabel()
        lbl.font = Constant.Theme.Font_13
        lbl.textColor = UIColor(rgba: Constant.common_C7_color)
        lbl.textAlignment = .center
        lbl.text = "点击\"登录\"即表示您已阅读并同意用户协议"
        lbl.isUserInteractionEnabled = true
        return lbl
    }()

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }

    func setupUI() {
        self.gesturBackEnable = false
        self.view.backgroundColor = UIColor.white
        for view in [loginBackButton,titleLabel,phoneField,clearButton,lineView,nextButton,passwordLabel,protocolLabel] {
            self.view.addSubview(view)
        }
        
        let scale = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        loginBackButton.frame = CGRect(x: 10, y: 26, width: 36, height: 36)
        titleLabel.frame = CGRect(x: 30 * scale, y: 264 * scale, width: Constant.ScreenSizeV2.SCREEN_WIDTH - 60 * scale, height: 25)
        phoneField.frame = CGRect(x: 60 * scale, y: titleLabel.ibottom + 180 * scale, width: Constant.ScreenSizeV2.SCREEN_WIDTH - 120 * scale, height: 22)
        
        clearButton.frame = CGRect(x: Constant.ScreenSizeV2.SCREEN_WIDTH - 140 * scale - 5, y: titleLabel.ibottom + 175 * scale, width: 48 * scale, height: 48 * scale)
        lineView.frame = CGRect(x: 60 * scale, y: phoneField.ibottom + 10 * scale, width: phoneField.iwidth, height: 1 * scale)
        nextButton.frame = CGRect(x: 60 * scale, y: lineView.ibottom + 50 * scale, width: phoneField.iwidth, height: 88 * scale)
        
        passwordLabel.frame = CGRect(x: 160 * scale, y: nextButton.ibottom + 36 * scale, width: phoneField.iwidth - 200 * scale, height: 16)
        protocolLabel.frame = CGRect(x: 60 * scale, y: self.view.iheight - 30 * scale - 16, width: Constant.ScreenSizeV2.SCREEN_WIDTH - 120 * scale, height: 16)
        
        
        self.phoneField.addTarget(self, action: #selector(BaseLoginViewController.textFieldDidChange(_:)), for: .editingChanged)
        clearButton.addTarget(self, action: #selector(BaseLoginViewController.clearButtonOnClicked), for: .touchUpInside)
        
        loginBackButton.addTarget(self, action: #selector(BaseLoginViewController.loginBackButtonOnclicked), for: .touchUpInside)
        
        nextButton.addTarget(self, action: #selector(BaseLoginViewController.nextButtonOnClicked), for: .touchUpInside)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(BaseLoginViewController.passwordLabelTaped))
        passwordLabel.addGestureRecognizer(tapGesture)
        
        let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(BaseLoginViewController.protocolLabelTaped))
        protocolLabel.addGestureRecognizer(tapGesture1)
        
        resetUI()
    }
    
    func resetUI() {
        
    }

    func loginBackButtonOnclicked()
    {
    }
    
    func clearButtonOnClicked() {
        self.phoneField.text = ""
        self.clearButton.isHidden = true
        self.nextButton.isEnabled = false
        self.nextButton.backgroundColor = Constant.Theme.ColorSeparatLine
    }
    
    func nextButtonOnClicked() {
    }
    
    func passwordLabelTaped() {
    }
    
    func protocolLabelTaped() {
        if let url = URL(string: SystemInfoRequestManager.shareInstance().user_protocol)  {
            let webVC = WebViewController(URL: url)
            self.navigationController?.pushViewController(webVC, animated: true)
        }
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        }
    
    func checkNumber(_ text:String) ->String{
        //去除所有空格，重新添加空格，防止手动删除空格之后造成不必要的问题
        
        if text.characters.count == 0{
            return ""
        }
        
        var newContent = text
        var tempContent = ""
        
        //去除 +86
        newContent = Utils.detectionTelphoneSimple(text)
        
        //筛选出newContent中的数字
        for index in 0..<newContent.characters.count{
            let char = NSString(string:newContent).substring(with: NSMakeRange(index,1))
            if Utils.checkNumber(char){
                tempContent = tempContent + char
            }
        }
        
        let newText:NSMutableString = NSMutableString(string: tempContent)
        
        if newText.length > 3{
            //在第三位之后添加空格
            newText.insert(" ", at: 3)
        }
        
        if newText.length > 8{
            //在第8位之后添加空格
            newText.insert(" ", at: 8)
        }
        
        return newText as String
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
