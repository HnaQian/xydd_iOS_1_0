//
//  LoginWithWechatViewController.swift
//  Limi
//
//  Created by maohs on 16/12/6.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit
import CoreData

class LoginWithWechatViewController: BaseLoginViewController,LoginWithNoCodeViewControllerDelegate {
    
    fileprivate let titleIcon:UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "login_wechatIcon")
        return image
    }()
    
    var isOutLogin : Bool = false
    var open_id : String = ""
    var accessToken : String = ""
    let nocodeLoginVc = LoginWithNoCodeViewController()
    var time:TimeInterval = 0
    var voiceTime:TimeInterval = 0
    
    override func loginBackButtonOnclicked()
    {
        if !self.isOutLogin{
            let animation = CATransition()
            animation.duration = 0.3
            
            animation.type = kCATransitionReveal
            
            animation.subtype = kCATransitionFromBottom
            
            animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            
            self.navigationController?.view.layer.add(animation, forKey: "")
            let _ = self.navigationController?.popViewController(animated: false)
        }
        else{
            let _ = self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func resetUI() {
        for view in [titleIcon] {
            self.view.addSubview(view)
        }
        self.passwordLabel.isHidden = true
        let scale = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        
        titleIcon.frame = CGRect(x: 0, y: 176 * scale, width: 140 * scale, height: 140 * scale)
        titleIcon.icenterX = self.view.icenterX
        titleIcon.layer.cornerRadius = titleIcon.iwidth / 2
        
        titleLabel.frame = CGRect(x: 30 * scale, y: 366 * scale, width: Constant.ScreenSizeV2.SCREEN_WIDTH - 60 * scale, height: 20)
        titleLabel.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        titleLabel.textColor = UIColor(rgba: Constant.common_C2_color)
        
        titleLabel.text = "为了保障您的个人权益，请绑定手机号"
        let height = CalculateHeightOrWidth.getLabOrBtnHeigh(titleLabel.text! as NSString, font: titleLabel.font, width:  Constant.ScreenSizeV2.SCREEN_WIDTH - 60 * scale)
        titleLabel.iheight = height
        self.nocodeLoginVc.delegate = self
        var userInfoation:User?
        
        let requestUserInfo: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        let datasourceUserInfo : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(requestUserInfo)!
        if datasourceUserInfo.count > 0{
            userInfoation = datasourceUserInfo[0] as? User
            if userInfoation!.wx_head_image_url != nil{
                if let tempString = userInfoation?.wx_head_image_url , let _ = URL(string:tempString) {
                    self.titleIcon.af_setImageWithURL(URL(string: tempString)!, placeholderImage: UIImage(named: "login_wechatIcon"))
                }else {
                    self.titleIcon.image = UIImage(named: "login_wechatIcon")
                }
            }
            else{
                self.titleIcon.image = UIImage(named: "login_wechatIcon")
            }
        }
        else{
            userInfoation = nil
            self.titleIcon.image = UIImage(named: "login_wechatIcon")
        }
    
    }
    
    func timeCountDown(shouldStop:Bool = false) {
        
        if self.time < 0 || shouldStop
        {
            return
        }
        
        gcd.async(.main, delay: 1) {
            self.time = self.time - 1
            self.timeCountDown()
        }
    }
    
    func voiceTimeCountDown(shouldStop:Bool = false) {
        
        if self.voiceTime < 0
        {
            return
        }
        
        gcd.async(.main, delay: 1) {
            self.voiceTime = self.voiceTime - 1
            self.voiceTimeCountDown()
        }
    }
    
    func countdownNumber(time: TimeInterval) {
        self.time = time
        self.timeCountDown()
    }
    
    func voiceCodeCountdownNumber(time: TimeInterval) {
        self.voiceTime = time
        self.voiceTimeCountDown()
    }
    
   override func nextButtonOnClicked() {
        if !Utils.isValiableMobile(phoneNumber: self.phoneField.text!) {
            Utils.showError(context: self.view, errorStr: "请输入正确的手机号")
            return
        }
        self.timeCountDown(shouldStop: true)
        self.voiceTimeCountDown(shouldStop: true)
        self.nocodeLoginVc.timeout = self.time
        self.nocodeLoginVc.voiceTimeout = self.voiceTime
        self.nocodeLoginVc.open_id = self.open_id
        self.nocodeLoginVc.accessToken = self.accessToken
        self.nocodeLoginVc.isFirstLogin = self.isOutLogin
        self.nocodeLoginVc.phoneNumber = (self.phoneField.text?.replacingOccurrences(of: " ", with: ""))!
        self.navigationController?.pushViewController(self.nocodeLoginVc, animated: true)
    }
    
   override func textFieldDidChange(_ textField: UITextField) {
        
        if let _ = textField.text{
            
            textField.text = checkNumber(textField.text!)
            
            //长度控制在11个数字加两个空格
            if textField.text!.characters.count >= 13{
                textField.text = NSString(string: textField.text!).substring(to: 13)
                self.nextButton.isEnabled = true
                self.nextButton.backgroundColor = Constant.Theme.Color1
            }else {
                self.nextButton.isEnabled = false
                self.nextButton.backgroundColor = Constant.Theme.ColorSeparatLine
            }
            
        }
        if (self.phoneField.text?.characters.count)! > 0{
            self.clearButton.isHidden = false
        }else{
            self.clearButton.isHidden = true
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
