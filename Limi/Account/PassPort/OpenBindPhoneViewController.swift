//
//  OpenBindPhoneViewController.swift
//  Limi
//
//  Created by ZhiQiangMi on 15/8/18.
//  Copyright (c) 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

import CoreData

class OpenBindPhoneViewController: BaseViewController
{
    
    var bindType:Int = 0
    
    var name:NSString = ""
    
    var album:NSString = ""
    
    var openId : String = ""
    
    var accessToken:String = ""
    
    var isOutLogin : Bool = false
    
    var albumImageView = UIImageView()
    
    var doneBtn:UIButton = {
        let btn = UIButton()
        btn.setTitle("绑定手机号", for: UIControlState())
        btn.setTitleColor(Constant.Theme.Color12, for: UIControlState())
        btn.backgroundColor = Constant.Theme.Color1
        btn.layer.cornerRadius = 3
        return btn
    }()
    
    var verCodeBtn:UIButton = {
        let btn = UIButton()
        btn.setTitle("获取验证码", for: UIControlState())
        btn.titleLabel?.font = Constant.Theme.Font_19
        btn.setTitleColor(UIColor.black, for: UIControlState())
        return btn
    }()
    
    fileprivate var nameLabel:UILabel = {
        let lbl = UILabel()
        lbl.textColor = Constant.Theme.Color6
        lbl.font = Constant.Theme.Font_19
        return lbl
    }()
    
    fileprivate var titleLabel:UILabel = {
        let lbl = UILabel()
        lbl.text = "为了更好的为您服务，请绑定一个手机号"
        lbl.textColor = Constant.Theme.Color6
        lbl.font = Constant.Theme.Font_16
        return lbl
    }()
    
    var phoneTextField = UITextField()
    
    var verCodeTextField = UITextField()
    
    let SCALE = Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE
    
    var voiceCodeButton: HyperlinksButton = {
        let btn = HyperlinksButton()
        btn.titleLabel?.font = Constant.Theme.Font_13
        btn.setTitle("收不到验证码? 点我接收验证码电话", for: UIControlState())
        btn.setTitleColor(Constant.Theme.Color7, for: UIControlState())
        return btn
    }()
    
    var remindLabel:UILabel = {
        let lbl = UILabel()
        lbl.text = "绑定完成后，微信账号和手机号都能登录"
        lbl.textColor = Constant.Theme.Color6
        lbl.font = Constant.Theme.Font_11
        return lbl
    }()
    
    let ruleLabel:UILabel = {
        let lbl = UILabel()
        lbl.text = "点击“绑定手机号”即表示同意"
        lbl.textColor = Constant.Theme.Color7
        lbl.font = Constant.Theme.Font_11
        return lbl
    }()
    
    var ruleBtn: HyperlinksButton = {
        let btn = HyperlinksButton()
        btn.setTitle("《心意点点服务协议》", for: UIControlState())
        btn.titleLabel?.font = Constant.Theme.Font_11
        btn.setTitleColor(Constant.Theme.Color7, for: UIControlState())
        return btn
    }()

    let ruleView = UIView()
    
    var userInfoation : User?
    
    fileprivate var timeout: TimeInterval = 0
    
    
    func aboutRule()
    {
        if let url = URL(string: SystemInfoRequestManager.shareInstance().user_protocol)
        {
            let webVC = WebViewController(URL: url)
            self.navigationController?.pushViewController(webVC, animated: true)
        }
        
    }
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationItem.title = ""
//        self.view.backgroundColor = Constant.Theme.Color12
        
        albumImageView.clipsToBounds = true
        albumImageView.layer.cornerRadius = SCALE * 75
        self.view.addSubview(albumImageView)
        albumImageView.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(SCALE * 20)
            let _ = make.centerX.equalTo(self.view)
            let _ = make.height.equalTo(SCALE * 150)
            let _ = make.width.equalTo(SCALE * 150)
        }
        
        self.view.addSubview(nameLabel)
        nameLabel.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(albumImageView.snp_bottom).offset(SCALE * 10)
            let _ = make.centerX.equalTo(self.view)
        }
        
        self.view.addSubview(titleLabel)
        titleLabel.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(nameLabel.snp_bottom).offset(SCALE * 30)
            let _ = make.centerX.equalTo(self.view)
        }
        
        phoneTextField.placeholder = "手机号"
        phoneTextField.setValue(Constant.Theme.Color9, forKeyPath: "_placeholderLabel.textColor")
        phoneTextField.setValue(Constant.Theme.Font_15, forKeyPath: "_placeholderLabel.font")
        self.view.addSubview(phoneTextField)
        phoneTextField.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(titleLabel.snp_bottom).offset(100 * SCALE)
            let _ = make.left.equalTo(self.view).offset(Constant.ScreenSizeV2.MARGIN_30)
            let _ = make.right.equalTo(self.view).offset(-Constant.ScreenSizeV2.MARGIN_30)
        }
        
        let phoneTextField_line = UIView()
        phoneTextField_line.backgroundColor = Constant.Theme.Color9
        self.view.addSubview(phoneTextField_line)
        phoneTextField_line.snp_makeConstraints { (make) in
            let _ = make.left.equalTo(self.view).offset(Constant.ScreenSizeV2.MARGIN_30)
            let _ = make.right.equalTo(self.view).offset(-Constant.ScreenSizeV2.MARGIN_30)
            let _ = make.top.equalTo(phoneTextField.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_30)
            let _ = make.height.equalTo(0.5)
        }
        
        verCodeTextField.placeholder = "验证码"
        verCodeTextField.setValue(Constant.Theme.Color9, forKeyPath: "_placeholderLabel.textColor")
        verCodeTextField.setValue(Constant.Theme.Font_15, forKeyPath: "_placeholderLabel.font")
        self.view.addSubview(verCodeTextField)
        
        verCodeBtn.titleLabel?.font = UIFont.boldSystemFont(ofSize: Constant.Theme.FontSize_15)
        verCodeBtn.addTarget(self, action: #selector(OpenBindPhoneViewController.getVerCode), for: UIControlEvents.touchUpInside)
        self.view.addSubview(verCodeBtn)
        
        verCodeTextField.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(phoneTextField_line.snp_bottom).offset(30 * SCALE)
            let _ = make.left.equalTo(self.view).offset(Constant.ScreenSizeV2.MARGIN_30)
            let _ = make.right.equalTo(verCodeBtn.snp_left)
        }
        
        verCodeBtn.snp_makeConstraints { (make) in
            let _ = make.centerY.equalTo(verCodeTextField)
            let _ = make.right.equalTo(self.view).offset(-Constant.ScreenSizeV2.MARGIN_30)
            let _ = make.height.equalTo(90 * SCALE)
            let _ = make.width.equalTo(SCALE * 200)
        }
        
        let verCodeTextField_line = UIView()
         verCodeTextField_line.backgroundColor = Constant.Theme.Color9
        self.view.addSubview(verCodeTextField_line)
        verCodeTextField_line.snp_makeConstraints { (make) in
            let _ = make.left.equalTo(self.view).offset(Constant.ScreenSizeV2.MARGIN_30)
            let _ = make.right.equalTo(self.view).offset(-Constant.ScreenSizeV2.MARGIN_30)
            let _ = make.top.equalTo(verCodeTextField.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_30)
            let _ = make.height.equalTo(0.5)
        }
        
        self.view.addSubview(voiceCodeButton)
        voiceCodeButton.addTarget(self, action: #selector(OpenBindPhoneViewController.voiceCodeButtonClick), for: UIControlEvents.touchUpInside)
        voiceCodeButton.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(verCodeTextField_line.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_20)
            let _ = make.centerX.equalTo(self.view)
        }
        
        self.view.addSubview(doneBtn)
        doneBtn.backgroundColor = Constant.Theme.Color1
        doneBtn.frame.size.height = Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 80
        doneBtn.titleLabel?.font = Constant.Theme.Font_19
        doneBtn.addTarget(self, action: #selector(OpenBindPhoneViewController.commit), for: UIControlEvents.touchUpInside)
        doneBtn.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(voiceCodeButton.snp_bottom).offset(SCALE * 60)
            let _ = make.height.equalTo(SCALE * 80)
            let _ = make.left.equalTo(self.view).offset(Constant.ScreenSizeV2.MARGIN_30)
            let _ = make.right.equalTo(self.view).offset(-Constant.ScreenSizeV2.MARGIN_30)
        }

        
        self.phoneTextField.keyboardType = UIKeyboardType.numberPad
        
        
        if self.bindType == 2{
            self.remindLabel.text = "绑定完成后，QQ账号和手机号都能登录"
        }
        self.view.addSubview(remindLabel)
        remindLabel.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(doneBtn.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_30)
            let _ = make.centerX.equalTo(self.view)
        }
        
        
        ruleView.addSubview(ruleLabel)
        
        ruleBtn.addTarget(self, action: #selector(OpenBindPhoneViewController.aboutRule), for: UIControlEvents.touchUpInside)
        ruleView.addSubview(ruleBtn)
        self.view.addSubview(ruleView)
        
        ruleLabel.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(ruleView)
            let _ = make.bottom.equalTo(ruleView)
            let _ = make.left.equalTo(ruleView)
        }
        
        ruleBtn.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(ruleLabel)
            let _ = make.bottom.equalTo(ruleView)
            let _ = make.left.equalTo(ruleLabel.snp_right)
            let _ = make.right.equalTo(ruleView)
        }
        
        ruleView.snp_makeConstraints { (make) in
            let _ = make.bottom.equalTo(self.view).offset(-Constant.ScreenSizeV2.MARGIN_30)
            let _ = make.centerX.equalTo(self.view)
        }
        
        phoneTextField.clearButtonMode = UITextFieldViewMode.whileEditing
        verCodeTextField.clearButtonMode = UITextFieldViewMode.whileEditing
        
        
        self.voiceCodeButton.isHidden = true
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.phoneTextField.becomeFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let image = UIImage(named: "titlebarGrey")
        self.navigationController?.navigationBar.setBackgroundImage(image, for: UIBarMetrics.default)
        
        let requestUserInfo: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        let datasourceUserInfo : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(requestUserInfo)!
        if datasourceUserInfo.count > 0{
            userInfoation = datasourceUserInfo[0] as? User
        }
        else{
            userInfoation = nil
        }
        
        if bindType == 3//微信登录
        {
            if userInfoation != nil{
                if userInfoation!.wx_nickName != nil && userInfoation!.wx_nickName != ""{
                    self.nameLabel.text = userInfoation?.wx_nickName
                }
                if userInfoation!.wx_head_image_url != nil{
//                    self.albumImageView.af_setImage(withURL: URL(string: userInfoation!.wx_head_image_url!)!, placeholderImage: UIImage(named: "loadingDefault"))
                    if let tempString = userInfoation?.wx_head_image_url , let _ = URL(string:tempString) {
                        self.albumImageView.af_setImageWithURL(URL(string: tempString)!, placeholderImage: UIImage(named: "loadingDefault"))
                    }else {
                        self.albumImageView.image = UIImage(named: "loadingDefault")
                    }
                }
                else{
                    self.albumImageView.image = UIImage(named: "loadingDefault")
                }
            }
            
            
        }else//QQ登录
        {
            if userInfoation != nil{
                if userInfoation!.qq_nickName != nil && userInfoation!.qq_nickName != ""{
                    self.nameLabel.text = userInfoation?.qq_nickName
                }
                if userInfoation!.qq_head_image_url != nil{
//                    self.albumImageView.af_setImage(withURL: URL(string: userInfoation!.qq_head_image_url!)!, placeholderImage: UIImage(named: "loadingDefault"))
                    self.albumImageView.af_setImageWithURL(URL(string: userInfoation!.qq_head_image_url!)!, placeholderImage: UIImage(named: "loadingDefault"))
                }
                else{
                    self.albumImageView.image = UIImage(named: "loadingDefault")
                }
            }
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        UserInfoManager.shareUserInfoManager().updateUserInfo(self.view)
        let image = UIImage(named: "titlebar")
        self.navigationController?.navigationBar.setBackgroundImage(image, for: UIBarMetrics.default)
    }
    
    
    //获取验证码
    func getVerCode()
    {
        // 检查手机号
        if self.phoneTextField.text!.isEmpty
        {
            Utils.showError(context: self.view,errorStr: "请输入手机号码!")
            return
        }
        self.voiceCodeButton.isHidden = false
       let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.isUserNameExistByOpen, parameters: ["user_name": self.phoneTextField.text! as AnyObject, "open_id": openId as AnyObject,"sns_token":accessToken as AnyObject,"type":self.bindType as AnyObject],isShowErrorStatuMsg : true,isNeedHud : true,successHandler: {data, status, msg in
            if status == 200{
                self.requestVerCode(1)
            }
        })
        
        
        
    }
    
    
    func requestVerCode(_ sms_type:Int){
       let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString:sms_type == 1 ? Constant.JTAPI.sms : Constant.JTAPI.voice_sms, parameters: ["user_name": self.phoneTextField.text! as AnyObject, "sms_type": 3 as AnyObject],isShowErrorStatuMsg : true,isNeedHud : true, successHandler: {data, status, msg in
            if status == 200{
                self.verCodeBtn.isEnabled = false
                self.timeout = 60
                self.signCodeButtonCountDown()
                if sms_type == 2{
                    Utils.showError(context: self.view, errorStr: "请注意接听电话")
                }
                self.verCodeTextField.becomeFirstResponder()
            }
        })
        
        
        
    }
    
    // 验证码按钮 倒计时
    func signCodeButtonCountDown()
    {
        if timeout < 0
        {
            self.verCodeBtn.isEnabled = true
            
            self.verCodeBtn.setTitle("获取验证码", for: UIControlState())
            self.verCodeBtn.setTitleColor(UIColor.black, for: UIControlState())
            
            return
        }
        self.verCodeBtn.isEnabled = false
        
        let nf = NumberFormatter()
        
        nf.numberStyle = NumberFormatter.Style.decimal
        
        nf.maximumFractionDigits = 0
        
        nf.string(from: (timeout - 1) as NSNumber)
        
        let timeoutShow = "(" + nf.string(from: timeout as NSNumber)! + ")"
        
        self.verCodeBtn.setTitle("重新获取" + timeoutShow, for: UIControlState())
        self.verCodeBtn.setTitleColor(UIColor.lightGray, for: UIControlState())
        
        gcd.async(.main, delay: 1) {
            self.timeout = self.timeout - 1
            self.signCodeButtonCountDown()
        }
        
    }
    
    
    func commit()
    {
        if self.phoneTextField.text!.isEmpty
        {
            Utils.showError(context: self.view,errorStr: "请输入手机号！")
            return
        }
        
        if self.verCodeTextField.text!.isEmpty
        {
            Utils.showError(context: self.view,errorStr: "请输入验证码！")
            return
        }
        phoneTextField.resignFirstResponder()
        verCodeTextField.resignFirstResponder()
        
       let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.sms_check, parameters: ["user_name": self.phoneTextField.text! as AnyObject, "sms_type": 3 as AnyObject, "code": self.verCodeTextField.text! as AnyObject],isShowErrorStatuMsg : true,isNeedHud : true, successHandler: {data, status, msg in
            if status == 200
            {
                self.bindPhone()
            }
        })
        
        
    }
    
    func voiceCodeButtonClick() {
        if self.phoneTextField.text!.isEmpty {
            Utils.showError(context: self.view, errorStr: "请输入手机号码")
            return
        }
        
        if self.phoneTextField.text!.characters.count != 11
        {
            Utils.showError(context: self.view, errorStr: "请输入正确的手机号码")
            return
        }
        
        if self.timeout > 0{
            Utils.showError(context: self.view, errorStr: "请" + String(Int(self.timeout)) + "秒后点我接收语音验证")
            return
        }
        
        let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.isUserNameExistByOpen, parameters: ["user_name": self.phoneTextField.text! as AnyObject, "open_id": openId as AnyObject,"sns_token":accessToken as AnyObject,"type":self.bindType as AnyObject],isShowErrorStatuMsg : true,isNeedHud : true,successHandler: {data, status, msg in
            if status == 200{
                self.requestVerCode(2)
            }
        })
        
        
    }
    
    
    
    func bindPhone()
    {
        var params = [String: AnyObject]()
        params["user_name"] = self.phoneTextField.text! as AnyObject?
        params["open_id"] = openId as AnyObject?
        params["code"] = self.verCodeTextField.text! as AnyObject?
        params["type"] = self.bindType as AnyObject?
        params["app_type"] = 1 as AnyObject?
        params["sns_token"] = accessToken as AnyObject?
        let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.regOpen, parameters: params, isNeedAppTokrn: true, isShowErrorStatuMsg: true, isNeedHud: true, successHandler: {data, status, msg in
            if status != 200
            {
                return
            }
            gcd.async(.default) {
                var nickName = ""
                if self.bindType == 3{
                    if let wxName = self.userInfoation?.wx_nickName{
                        nickName = wxName
                    }
                    
                }
                else if self.bindType == 2{
                    if let qqName = self.userInfoation?.qq_nickName{
                        nickName = qqName
                    }
                }
                var sex = 0
                if let wxSex = self.userInfoation?.gender{
                    sex = Int(wxSex)
                }
                self.getUserInfo(nickName, gender: sex)
                
            }
            
            if self.bindType == 3
            {//=1 微信
                let request: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
                let datasource : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(request)!
                if datasource.count > 0{
                    
                    let userInfo = datasource[0] as! User
                    userInfo.wx_bind_status = true
                }
                else{
                    let entity = NSEntityDescription.entity(forEntityName: "User", in: CoreDataManager.shared.managedObjectContext)
                    request.entity = entity
                    do {
                        try! CoreDataManager.shared.managedObjectContext.fetch(request)
                        let user: User = NSEntityDescription.insertNewObject(forEntityName: "User", into: CoreDataManager.shared.managedObjectContext) as! User
                        user.wx_bind_status = true
                    }
                }
                CoreDataManager.shared.save()
            }else
            {//=2 QQ
                let request: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
                let datasource : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(request)!
                if datasource.count > 0{
                    
                    let userInfo = datasource[0] as! User
                    userInfo.qq_bind_status = true
                }
                else{
                    let entity = NSEntityDescription.entity(forEntityName: "User", in: CoreDataManager.shared.managedObjectContext)
                    request.entity = entity
                    do {
                        try! CoreDataManager.shared.managedObjectContext.fetch(request)
                        let user: User = NSEntityDescription.insertNewObject(forEntityName: "User", into: CoreDataManager.shared.managedObjectContext) as! User
                        user.qq_bind_status = true
                    }
                }
                CoreDataManager.shared.save()
            }
            
            // 保存用户是否设置了新手引导
            let guide = (data["data"] as? [String: AnyObject])?["beginner_guide_status"] as? Bool
            UserDefaults.standard.set(guide!, forKey: "guide")
            UserDefaults.standard.synchronize()
            
            self.SynchronizeUserInfo()
        })
        
        
    }
    
    
    func SynchronizeUserInfo(){
        self.SynchronizeUserAccountInfo()
    }
    
    func SynchronizeUserAccountInfo(){
        if userInfoation != nil{
            if userInfoation!.avatarData != nil{
                self.uploadHeaderImage(userInfoation!.avatarData! as Data)
            }
            else if userInfoation!.gender != 0 || userInfoation!.birthday != "" || userInfoation!.birthday_type != 0{
                var userGender : Int = 0
                var userBirthday_type : Int = 0
                var userBirthday : String = ""
                if userInfoation?.gender != 0 && userInfoation?.gender != nil{
                    userGender = Int(userInfoation!.gender)
                }
                if userInfoation?.birthday_type != 0 && userInfoation?.birthday_type != nil{
                    userBirthday_type = Int(userInfoation!.birthday_type)
                }
                if userInfoation?.birthday != "" && userInfoation?.birthday != nil{
                    userBirthday = userInfoation!.birthday!
                }
                self.uploadUserInfo(userGender, birthday_type: userBirthday_type, birthday: userBirthday, avatar: "")
            }
            else{
                self.getUserInfo()
            }
        }
        else{
            self.getUserInfo()
        }
    }
    
    func uploadHeaderImage(_ imageData: Data)
    {
        var isUpoadSuccess = true
        ImageUploader.uploadimage(context:self.view,data: imageData) { [weak self](id,domain) -> Void in
            if id == nil
            {
                isUpoadSuccess = false
            }
            
            if isUpoadSuccess{
                if self!.userInfoation!.gender != 0 || self!.userInfoation!.birthday != "" || self!.userInfoation!.birthday_type != 0{
                    var userGender : Int = 0
                    var userBirthday_type : Int = 0
                    var userBirthday : String = ""
                    if self!.userInfoation?.gender != 0 && self!.userInfoation?.gender != nil{
                        userGender = Int(self!.userInfoation!.gender)
                    }
                    if self!.userInfoation?.birthday_type != 0 && self!.userInfoation?.birthday_type != nil {
                        userBirthday_type = Int(self!.userInfoation!.birthday_type)
                    }
                    if self!.userInfoation?.birthday != "" && self!.userInfoation?.birthday != nil{
                        userBirthday = self!.userInfoation!.birthday!
                    }
                    self!.uploadUserInfo(userGender, birthday_type: userBirthday_type, birthday: userBirthday, avatar: id!)
                }
                else{
                    self!.getUserInfo()
                }
            }
            else{
                if self!.userInfoation!.gender != 0 || self!.userInfoation!.birthday != "" || self!.userInfoation!.birthday_type != 0{
                    var userGender : Int = 0
                    var userBirthday_type : Int = 0
                    var userBirthday : String = ""
                    if self!.userInfoation?.gender != 0 && self!.userInfoation?.gender != nil{
                        userGender = Int(self!.userInfoation!.gender)
                    }
                    if self!.userInfoation?.birthday_type != 0 && self!.userInfoation?.birthday_type != nil{
                        userBirthday_type = Int(self!.userInfoation!.birthday_type)
                    }
                    if self!.userInfoation?.birthday != "" && self!.userInfoation?.birthday != nil{
                        userBirthday = self!.userInfoation!.birthday!
                    }
                    self!.uploadUserInfo(userGender, birthday_type: userBirthday_type, birthday: userBirthday, avatar: "")
                }
                else{
                    self!.getUserInfo()
                }
            }
            
        }
        
    }
    
    
    
    func uploadUserInfo(_ gender : Int?,birthday_type : Int?,birthday : String?,avatar : String){
        var params = [String: AnyObject]()
        if gender != 0{
            params["gender"] = gender as AnyObject?
        }
        if birthday_type != 0{
            params["birthday_type"] = birthday_type as AnyObject?
        }
        if birthday != ""{
            params["birthday"] = birthday as AnyObject?
        }
        if avatar != ""{
            params["avatar"] = avatar as AnyObject?
        }
       let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.syn_userSetting,parameters: params, isNeedUserTokrn: true,isShowErrorStatuMsg: true, isNeedHud: true,successHandler: { (data, status, msg) -> Void in
            if status == 200
            {
                self.getUserInfo()
            }
        })
        
    }
    
    
    func getUserInfo(){
       let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.user_info_v2,isNeedUserTokrn: true,isShowErrorStatuMsg: true, isNeedHud: true, successHandler: { (data, status, msg) -> Void in
            if status == 200
            {
                if let dataJson : JSON = JSON(data["data"]!) as JSON?{
                    
                    if let user : JSON = dataJson["user"] as JSON?{
                        CoreDataManager.shared.update(user, entityType: Constant.CoreDataType.userInfoType,successHandler:{Void in
                            self.popView()
                            },failureHandler: {Void in
                                self.popView()
                        })
                    }
                    
                    
                }
            }
        })
        
    }
    
    func popView(){
        if !self.isOutLogin{
            var formerVC : UIViewController?
            for temp in self.navigationController!.viewControllers{
                if temp.isKind(of: LoginViewController.self){
                    let animation = CATransition()
                    animation.duration = 0.3
                    
                    animation.type = kCATransitionReveal
                    
                    animation.subtype = kCATransitionFromBottom
                    
                    animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                    
                    self.navigationController?.view.layer.add(animation, forKey: "")
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.isLoginSuccess = true
                   let _ = self.navigationController?.popToViewController(formerVC!, animated: true)
                    NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.notification_login_success), object: nil)
                }
                formerVC = temp
                
            }
        }
        else{
            NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.show_mainTabBarController), object: nil)
        }
    }
    
    
    func getUserInfo(_ nickName : String,gender : Int){
       let _ = LMRequestManager.requestByDataAnalyses(context:UIApplication.shared.keyWindow!,URLString: Constant.JTAPI.user_info_v2,isNeedUserTokrn: true, successHandler: {data, status, msg in
            if status == 200
            {
                if let dataJson : JSON = JSON(data["data"]!) as JSON?{
                    
                    if let user : JSON = dataJson["user"] as JSON?{
                        
                        CoreDataManager.shared.update(user, entityType: Constant.CoreDataType.userInfoType)
                        
                        let userInfo = CoreDataManager.shared.getDBManager(Constant.CoreDataType.userInfoType) as? User
                        if (userInfo?.gender == 0 || userInfo?.gender == -1) && (gender != 0){
                            self.uploadSex(gender)
                            
                        }
                    }
                }
            }
            
        })
    }
    
    
    func uploadSex(_ sex: Int)
    {
       let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.user_gender, parameters: [ "gender": sex as AnyObject], isNeedUserTokrn: true)
    }
    
    
}
