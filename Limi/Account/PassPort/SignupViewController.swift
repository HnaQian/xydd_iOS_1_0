//
//  SignupViewController.swift
//  Limi
//
//  Created by 程巍巍 on 7/23/15.
//  Copyright (c) 2015 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//注册

import UIKit

import CoreData
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class SignupViewController: BaseViewController {

    var userInfoation : User?
    var isOutLogin : Bool = false
    
    //widgets
    fileprivate let tipeLbl = UILabel()
    
    fileprivate let passwdField = UITextField()
    
    fileprivate let rePasswdField = UITextField()
    
    fileprivate let sureBtn = UIButton()
    
    fileprivate let sayLaterBtn = UIButton()
    fileprivate let thisJumpBtn = UIButton()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationItem.title = ""
        self.navigationController?.navigationBar.isHidden = true
        self.view.backgroundColor = Constant.Theme.Color12
        setupUI()
    }
    
    override func backItemAction(){
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    //以后再说
    @objc fileprivate func passSetPWD(){
        self.SynchronizeUserAccountInfo()
    }
    
    @objc fileprivate func onThisJump(){
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.passwdField.becomeFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        self.navigationBarStyle = 1
        
        userInfoation = CoreDataManager.shared.getDBManager(Constant.CoreDataType.userInfoType) as? User
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationBarStyle = 0
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // 设置密码
    @objc fileprivate func commitPassword() {
        
        // 检查密码设置框
        if passwdField.text!.isEmpty {
            Utils.showError(context: self.view, errorStr: "请设置密码！")
            return
        }
        
        if passwdField.text?.characters.count < 6 || passwdField.text?.characters.count > 15 {
            Utils.showError(context: self.view, errorStr: "密码为6-15位字母、数字、符号组合，请重新设定！")
            return
        }
        
        // 检查再次输入密码框
        if rePasswdField.text!.isEmpty {
            Utils.showError(context: self.view, errorStr: "请再次输入密码！")
            return
        }
        
        
        if passwdField.text != rePasswdField.text {
            Utils.showError(context: self.view, errorStr: "密码不一致，请重新设置！")
            return
        }
        
        passwdField.resignFirstResponder()
        rePasswdField.resignFirstResponder()
        
        //  提交
        let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.login_resetPassword, parameters: ["password": passwdField.text! as AnyObject,"re_password": rePasswdField.text! as AnyObject], isNeedUserTokrn: true,isShowErrorStatuMsg: true,isShowSuccessStatuMsg:true, isNeedHud: true,successHandler: {data, status, msg in
            if status == 200
            {
                    let request: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
                    let datasource : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(request)!
                    if datasource.count > 0{
                        
                        let userInfo = datasource[0] as! User
                        userInfo.has_password = true
                    }
                    else{
                        let entity = NSEntityDescription.entity(forEntityName: "User", in: CoreDataManager.shared.managedObjectContext)
                        request.entity = entity
                        do {
                            try! CoreDataManager.shared.managedObjectContext.fetch(request)
                            let user: User = NSEntityDescription.insertNewObject(forEntityName: "User", into: CoreDataManager.shared.managedObjectContext) as! User
                            user.has_password = true
                        }
                    }
                    CoreDataManager.shared.save()
                    
                    self.SynchronizeUserAccountInfo()
            }
        })
        
    }
    
    
    
    fileprivate func SynchronizeUserAccountInfo(){
        if userInfoation != nil{
            if userInfoation!.avatarData != nil{
                self.uploadHeaderImage(userInfoation!.avatarData! as Data)
            }
            else if userInfoation!.gender != 0 || userInfoation!.birthday != "" || userInfoation!.birthday_type != 0{
                var userGender : Int = 0
                var userBirthday_type : Int = 0
                var userBirthday : String = ""
                if userInfoation?.gender != 0 && userInfoation?.gender != nil{
                    userGender = Int(userInfoation!.gender)
                }
                if userInfoation?.birthday_type != 0 && userInfoation?.birthday_type != nil{
                    userBirthday_type = Int(userInfoation!.birthday_type)
                }
                if userInfoation?.birthday != "" && userInfoation?.birthday != nil{
                    userBirthday = userInfoation!.birthday!
                }
                self.uploadUserInfo(userGender, birthday_type: userBirthday_type, birthday: userBirthday, avatar: "")
            }
            else{
                self.getUserInfo()
            }
        }
        else{
            self.getUserInfo()
        }
    }
    
    fileprivate  func uploadHeaderImage(_ imageData: Data)
    {
        var isUpoadSuccess = true
        ImageUploader.uploadimage(context:self.view,data: imageData) { [weak self](id,domain) -> Void in
            if id == nil
            {
                isUpoadSuccess = false
            }

            if isUpoadSuccess{
                if self!.userInfoation!.gender != 0 || self!.userInfoation!.birthday != "" || self!.userInfoation!.birthday_type != 0{
                    var userGender : Int = 0
                    var userBirthday_type : Int = 0
                    var userBirthday : String = ""
                    if self!.userInfoation?.gender != 0 && self!.userInfoation?.gender != nil{
                        userGender = Int(self!.userInfoation!.gender)
                    }
                    if self!.userInfoation?.birthday_type != 0 && self!.userInfoation?.birthday_type != nil{
                        userBirthday_type = Int(self!.userInfoation!.birthday_type)
                    }
                    if self!.userInfoation?.birthday != "" && self!.userInfoation?.birthday != nil{
                        userBirthday = self!.userInfoation!.birthday!
                    }
                    self!.uploadUserInfo(userGender, birthday_type: userBirthday_type, birthday: userBirthday, avatar: id!)
                }
                else{
                    self!.getUserInfo()
                }
            }
            else{
                if self!.userInfoation!.gender != 0 || self!.userInfoation!.birthday != "" || self!.userInfoation!.birthday_type != 0{
                    var userGender : Int = 0
                    var userBirthday_type : Int = 0
                    var userBirthday : String = ""
                    if self!.userInfoation?.gender != 0 && self!.userInfoation?.gender != nil{
                        userGender = Int(self!.userInfoation!.gender)
                    }
                    if self!.userInfoation?.birthday_type != 0 && self!.userInfoation?.birthday_type != nil{
                        userBirthday_type = Int(self!.userInfoation!.birthday_type)
                    }
                    if self!.userInfoation?.birthday != "" && self!.userInfoation?.birthday != nil{
                        userBirthday = self!.userInfoation!.birthday!
                    }
                    self!.uploadUserInfo(userGender, birthday_type: userBirthday_type, birthday: userBirthday, avatar: "")
                }
                else{
                    self!.getUserInfo()
                }
            }
            
        }
        
    }
    
    
    
    fileprivate func uploadUserInfo(_ gender : Int?,birthday_type : Int?,birthday : String?,avatar : String){
        var params = [String: AnyObject]()
        if gender != 0{
            params["gender"] = gender as AnyObject?
        }
        if birthday_type != 0{
            params["birthday_type"] = birthday_type as AnyObject?
        }
        if birthday != ""{
            params["birthday"] = birthday as AnyObject?
        }
        if avatar != ""{
            params["avatar"] = avatar as AnyObject?
        }
        
        let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.syn_userSetting,parameters: params, isNeedUserTokrn: true,isShowErrorStatuMsg: true, isNeedHud: true, hudTitle: "正在同步用户信息",successHandler: { (data, status, msg) -> Void in
            if status == 200
            {
                self.getUserInfo()
            }

        })
    }
    
    
    fileprivate func getUserInfo(){
        let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.user_info_v2, isNeedUserTokrn: true, isShowErrorStatuMsg: true, isNeedHud: true, hudTitle: "",successHandler: { (data, status, msg) -> Void in
            if status == 200
            {
                if let dataJson : JSON = JSON(data["data"]!) as JSON?{

                    if let user : JSON = dataJson["user"] as JSON?{
                        CoreDataManager.shared.update(user, entityType: Constant.CoreDataType.userInfoType,successHandler:{Void in
                            self.popView()
                            },failureHandler: {Void in
                                self.popView()
                            }
                        )
                    }
                }
            }
        })
    }
    
    
    fileprivate func popView(){
        if self.isOutLogin == false{
            var formerVC : UIViewController?
            for temp in self.navigationController!.viewControllers{
                if temp.isKind(of: LoginViewController.self){
                    let animation = CATransition()
                    animation.duration = 0.3
                    
                    animation.type = kCATransitionReveal
                    
                    animation.subtype = kCATransitionFromBottom
                    
                    animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                    
                    self.navigationController?.view.layer.add(animation, forKey: "")
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.isLoginSuccess = true
                    self.navigationController?.navigationBar.isHidden = false
                    let _ = self.navigationController?.popToViewController(formerVC!, animated: true)
                    NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.notification_login_success), object: nil)
                }
                formerVC = temp
                
            }
        }
        else{
            NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.show_mainTabBarController), object: nil)
        }
    }
   
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
}



extension SignupViewController {
    
    
    fileprivate func setupUI(){
        
        let rePasswdField_line = UIView()
        let passwdField_line = UIView()
        
        for view in [tipeLbl,passwdField,passwdField_line,rePasswdField,rePasswdField_line,sureBtn,sayLaterBtn]{
            self.view.addSubview(view)
        }
        
        
        rePasswdField_line.backgroundColor = Constant.Theme.Color9
        passwdField_line.backgroundColor = Constant.Theme.Color9
        
        
//        thisJumpBtn.setTitle("此次跳过", forState: UIControlState.Normal)
//        thisJumpBtn.titleLabel?.font = Constant.Theme.Font_15
//        thisJumpBtn.setTitleColor(Constant.Theme.Color7, forState: UIControlState.Normal)
//        thisJumpBtn.addTarget(self, action: #selector(SignupViewController.onThisJump), forControlEvents: .TouchUpInside)
        
        tipeLbl.textAlignment = NSTextAlignment.center
        tipeLbl.text = "如果你习惯使用密码登录\n请设置一个密码"
        tipeLbl.font = Constant.Theme.Font_19
        tipeLbl.numberOfLines = 0
        
        passwdField.placeholder = "请输入密码"
        passwdField.setValue(Constant.Theme.Color9, forKeyPath: "_placeholderLabel.textColor")
        rePasswdField.placeholder = "请再次输入密码"
        rePasswdField.setValue(Constant.Theme.Color9, forKeyPath: "_placeholderLabel.textColor")
        
        passwdField.isSecureTextEntry = true
        rePasswdField.isSecureTextEntry = true
        
        sureBtn.setTitle("确定", for: UIControlState())
        sureBtn.setTitleColor(Constant.Theme.Color12, for: UIControlState())
        sureBtn.backgroundColor = Constant.Theme.Color1
        sureBtn.layer.cornerRadius = 3
        sureBtn.addTarget(self, action: #selector(SignupViewController.commitPassword), for: .touchUpInside)
        
        sayLaterBtn.setTitle("以后再说", for: UIControlState())
        sayLaterBtn.titleLabel?.font = Constant.Theme.Font_15
        sayLaterBtn.setTitleColor(Constant.Theme.Color7, for: UIControlState())
        
        sayLaterBtn.addTarget(self, action: #selector(SignupViewController.passSetPWD), for: .touchUpInside)
        
        let SCALE = Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE
        //layout
//        thisJumpBtn.snp_makeConstraints { (make) in
//            make.top.equalTo(SCALE * 148)
//            make.right.equalTo(self.view).offset(-Constant.ScreenSizeV2.MARGIN_30)
//        }
//        let tipWidth = CalculateHeightOrWidth.getLabOrBtnWidth(tipeLbl.text! as NSString, font: tipeLbl.font, height: 60)
        tipeLbl.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(self.view).offset(SCALE * 240)
            let _ = make.centerX.equalTo(self.view)
            let _ = make.height.equalTo(SCALE * 100)
            let _ = make.width.equalTo(self.view)
        }
        
        
        passwdField.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(tipeLbl.snp_bottom).offset(SCALE * 100)
           let _ =  make.left.equalTo(self.view).offset(Constant.ScreenSizeV2.MARGIN_30)
            let _ = make.right.equalTo(self.view).offset(-Constant.ScreenSizeV2.MARGIN_30)
            let _ = make.height.equalTo(20)
        }

        passwdField_line.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(passwdField.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_30)
            let _ = make.left.equalTo(passwdField)
            let _ = make.right.equalTo(passwdField)
            let _ = make.height.equalTo(0.5)
        }
        
        //rePasswdField_line
        rePasswdField.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(passwdField_line.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_30)
            let _ = make.left.equalTo(self.passwdField)
            let _ = make.right.equalTo(self.passwdField)
            let _ = make.height.equalTo(20)
        }
        
        
        rePasswdField_line.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(rePasswdField.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_30)
            let _ = make.left.equalTo(passwdField)
            let _ = make.right.equalTo(passwdField)
            let _ = make.height.equalTo(0.5)
        }
        
        
        sureBtn.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(rePasswdField_line.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_60)
            let _ = make.left.equalTo(passwdField)
            let _ = make.right.equalTo(passwdField)
            let _ = make.height.equalTo(SCALE * 80)
        }
        
        
        
        sayLaterBtn.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(sureBtn.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_30)
            let _ = make.left.equalTo(passwdField)
           let _ =  make.right.equalTo(passwdField)
//            make.height.equalTo(20)
        }
    }
}
