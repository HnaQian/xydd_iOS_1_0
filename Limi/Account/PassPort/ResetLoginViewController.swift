//
//  ResetLoginViewController.swift
//  Limi
//
//  Created by maohs on 16/12/7.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//重置密码，输手机号的页面
import UIKit

class ResetLoginViewController: BaseLoginViewController,LoginWithNoCodeViewControllerDelegate {
    
    var time:TimeInterval = 0
    var voiceTime:TimeInterval = 0
    let nocodeLoginVc = LoginWithNoCodeViewController()
    var isOutLogin = false
    var isCodeSign = false
    
    fileprivate let cancelButton:UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named: "webview_cancel"), for: UIControlState.normal)
        
        return btn
    }()
    
    override func loginBackButtonOnclicked()
    {
        
        let _ = self.navigationController?.popViewController(animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.passwordLabel.isHidden = true
        self.titleLabel.text = "请输入手机号"
        self.nocodeLoginVc.delegate = self
        
        self.view.addSubview(cancelButton)
        cancelButton.frame = CGRect(x: loginBackButton.iright + 4, y: 26, width: 36, height: 36)
        cancelButton.addTarget(self, action: #selector(ResetLoginViewController.cancelButtonOnclicked), for: .touchUpInside)

    }
    
    func cancelButtonOnclicked() {
        if !isOutLogin {
            if let navigationController = self.navigationController{
                var formerVC : UIViewController?
                for temp in navigationController.viewControllers{
                    if temp.isKind(of: LoginViewController.self){
                        let animation = CATransition()
                        animation.duration = 0.3
                        
                        animation.type = kCATransitionReveal
                        
                        animation.subtype = kCATransitionFromBottom
                        
                        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                        
                        self.navigationController?.view.layer.add(animation, forKey: "")
                        let _ = self.navigationController?.popToViewController(formerVC!, animated: true)
                        
                        NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.notification_login_success), object: nil)
                    }
                    formerVC = temp
                }
            }
        }else {
            NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.show_mainTabBarController), object: nil)
        }
    }
    
   override func nextButtonOnClicked() {
        if !Utils.isValiableMobile(phoneNumber: self.phoneField.text!) {
            Utils.showError(context: self.view, errorStr: "请输入正确的手机号")
            return
        }
        self.timeCountDown(shouldStop: true)
        self.voiceTimeCountDown(shouldStop: true)
        self.nocodeLoginVc.timeout = self.time
        self.nocodeLoginVc.voiceTimeout = self.voiceTime
        self.nocodeLoginVc.isSetPassword = isCodeSign ? false : true
        self.nocodeLoginVc.phoneNumber = (self.phoneField.text?.replacingOccurrences(of: " ", with: ""))!
        self.navigationController?.pushViewController(self.nocodeLoginVc, animated: true)

    }
    
    func timeCountDown(shouldStop:Bool = false) {
        
        if self.time < 0 || shouldStop
        {
            return
        }
        
        gcd.async(.main, delay: 1) {
            self.time = self.time - 1
            self.timeCountDown()
        }
    }
    
    func voiceTimeCountDown(shouldStop:Bool = false) {
        
        if self.voiceTime < 0 || shouldStop
        {
            return
        }
        
        gcd.async(.main, delay: 1) {
            self.voiceTime = self.voiceTime - 1
            self.voiceTimeCountDown()
        }
    }
    
    func countdownNumber(time: TimeInterval) {
        self.time = time
        self.timeCountDown()
    }
    
    func voiceCodeCountdownNumber(time: TimeInterval) {
        self.voiceTime = time
        self.voiceTimeCountDown()
    }
    
    override func textFieldDidChange(_ textField: UITextField) {
        
        if let _ = textField.text{
            
            textField.text = checkNumber(textField.text!)
            
            //长度控制在11个数字加两个空格
            if textField.text!.characters.count >= 13{
                textField.text = NSString(string: textField.text!).substring(to: 13)
                self.nextButton.isEnabled = true
                self.nextButton.backgroundColor = Constant.Theme.Color1
            }else {
                self.nextButton.isEnabled = false
                self.nextButton.backgroundColor = Constant.Theme.ColorSeparatLine
            }
            
        }
        if (self.phoneField.text?.characters.count)! > 0{
            self.clearButton.isHidden = false
        }else{
            self.clearButton.isHidden = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
