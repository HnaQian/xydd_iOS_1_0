//
//  LoginWithNoCodeViewController.swift
//  Limi
//
//  Created by maohs on 16/12/5.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit
import CoreData

@objc protocol LoginWithNoCodeViewControllerDelegate:NSObjectProtocol {
    func countdownNumber(time:TimeInterval)
    func voiceCodeCountdownNumber(time:TimeInterval)
}

class LoginWithNoCodeViewController: BaseViewController {

    
    fileprivate let loginBackButton:UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named: "common_back_icon"), for: UIControlState.normal)
        
        return btn
    }()
    
    fileprivate let cancelButton:UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named: "webview_cancel"), for: UIControlState.normal)
        
        return btn
    }()
    
    fileprivate let titleLabel:UILabel = {
        let lbl = UILabel()
        lbl.font = Constant.Theme.Font_21
        lbl.textAlignment = .center
        lbl.text = "请输入验证码"
        return lbl
    }()
    
    fileprivate let hintLabel:UILabel = {
        let lbl = UILabel()
        lbl.font = Constant.Theme.Font_14
        lbl.textColor = UIColor(rgba: Constant.common_C7_color)
        return lbl
    }()
    
    fileprivate let countDownLabel:UILabel = {
        let lbl = UILabel()
        lbl.font = Constant.Theme.Font_14
        lbl.textColor = UIColor(rgba: Constant.common_C7_color)
        lbl.text = "重新获取"
        return lbl
    }()
    
    fileprivate let nocodeLabel1:UILabel = {
        let lbl = UILabel()
        lbl.font = Constant.Theme.Font_19
        lbl.textColor = UIColor(rgba: Constant.common_C2_color)
        lbl.layer.cornerRadius = 5 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        lbl.layer.borderColor = UIColor(rgba: Constant.common_C9_color).cgColor
        lbl.layer.borderWidth = 1 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        lbl.textAlignment = .center
        
        return lbl
    }()
    
    fileprivate let nocodeLabel2:UILabel = {
        let lbl = UILabel()
        lbl.font = Constant.Theme.Font_19
        lbl.textColor = UIColor(rgba: Constant.common_C2_color)
        lbl.layer.cornerRadius = 5 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        lbl.layer.borderColor = UIColor(rgba: Constant.common_C9_color).cgColor
        lbl.layer.borderWidth = 1 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        lbl.textAlignment = .center
        return lbl
    }()
    
    fileprivate let nocodeLabel3:UILabel = {
        let lbl = UILabel()
        lbl.font = Constant.Theme.Font_19
        lbl.textColor = UIColor(rgba: Constant.common_C2_color)
        lbl.layer.cornerRadius = 5 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        lbl.layer.borderColor = UIColor(rgba: Constant.common_C9_color).cgColor
        lbl.layer.borderWidth = 1 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        lbl.textAlignment = .center
        return lbl
    }()
    
    fileprivate let nocodeLabel4:UILabel = {
        let lbl = UILabel()
        lbl.font = Constant.Theme.Font_19
        lbl.textColor = UIColor(rgba: Constant.common_C2_color)
        lbl.layer.cornerRadius = 5 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        lbl.layer.borderColor = UIColor(rgba: Constant.common_C9_color).cgColor
        lbl.layer.borderWidth = 1 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        lbl.textAlignment = .center
        return lbl
    }()
    
    fileprivate let nocodeLabel5:UILabel = {
        let lbl = UILabel()
        lbl.font = Constant.Theme.Font_19
        lbl.textColor = UIColor(rgba: Constant.common_C2_color)
        lbl.layer.cornerRadius = 5 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        lbl.layer.borderColor = UIColor(rgba: Constant.common_C9_color).cgColor
        lbl.layer.borderWidth = 1 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        lbl.textAlignment = .center
        return lbl
    }()
    
    fileprivate let nocodeLabel6:UILabel = {
        let lbl = UILabel()
        lbl.font = Constant.Theme.Font_19
        lbl.textColor = UIColor(rgba: Constant.common_C2_color)
        lbl.layer.cornerRadius = 5 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        lbl.layer.borderColor = UIColor(rgba: Constant.common_C9_color).cgColor
        lbl.layer.borderWidth = 1 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        lbl.textAlignment = .center
        return lbl
    }()
    
    fileprivate let nocodeTextField:CustomTextField = {
        let textField = CustomTextField()
        textField.keyboardType = UIKeyboardType.numberPad
        textField.textColor = UIColor.clear
        textField.tintColor = UIColor.clear
        return textField 
    }()
    
    fileprivate let loginButton:UIButton = {
        let btn = UIButton()
        btn.setTitle("登录", for: UIControlState())
        btn.titleLabel?.font = UIFont.systemFont(ofSize: Constant.Theme.FontSize_19)
        btn.setTitleColor(Constant.Theme.Color12, for: UIControlState())
        btn.backgroundColor = Constant.Theme.ColorSeparatLine
        btn.layer.cornerRadius = 3
        btn.isEnabled = false
        return btn
    }()
    
    fileprivate let noCodeHintLabel:UILabel = {
        let lbl = UILabel()
        lbl.font = Constant.Theme.Font_15
        lbl.textColor = UIColor(rgba: Constant.common_C7_color)
        lbl.text = "收不到验证码?"
        lbl.textAlignment = .center
        lbl.isUserInteractionEnabled = true
        lbl.isHidden = true
        return lbl
    }()
    
    var phoneNumber: String = "" {
        didSet {
            self.hintLabel.text = "请输入发送到\(phoneNumber)上的6位验证码"
        }
    }
    
    var userInfoation : User?
    fileprivate var nocodeArray = ["","","","","",""]
    var isFirstLogin = false
    var timeout: TimeInterval = 0
    var voiceTimeout:TimeInterval = 0
    var open_id:String = ""
    var accessToken : String = ""
    var isSetPassword = false
    var isCodeLogin = false
    
    weak var delegate:LoginWithNoCodeViewControllerDelegate?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let requestUserInfo: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        let datasourceUserInfo : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(requestUserInfo)!
        if datasourceUserInfo.count > 0{
            userInfoation = datasourceUserInfo[0] as? User
        }
        else{
            userInfoation = nil
        }
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.nocodeTextField.text = ""
        self.loginButton.backgroundColor = Constant.Theme.ColorSeparatLine
        
        
        self.loginButton.isEnabled = false
        nocodeArray = ["","","","","",""]
        nocodeLabel1.text = nocodeArray[0]
        nocodeLabel2.text = nocodeArray[1]
        nocodeLabel3.text = nocodeArray[2]
        nocodeLabel4.text = nocodeArray[3]
        nocodeLabel5.text = nocodeArray[4]
        nocodeLabel6.text = nocodeArray[5]
        
        if self.timeout <= 0 && self.voiceTimeout > 0 {
            self.countDownLabel.text = "重新获取"
        }
        
        if self.timeout <= 0 && self.voiceTimeout <= 0 {
            self.requestSignCode()
        }

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
    }
    
    func setupUI() {
        
        self.view.backgroundColor = UIColor.white
        for view in [loginBackButton,cancelButton,titleLabel,hintLabel,countDownLabel] {
            self.view.addSubview(view)
        }
        
        //验证码输入框
        for view in [nocodeLabel1,nocodeLabel2,nocodeLabel3,nocodeLabel4,nocodeLabel5,nocodeLabel6,nocodeTextField] {
            self.view.addSubview(view)
        }
        
        for view in [loginButton,noCodeHintLabel] {
            self.view.addSubview(view)
        }
        
        let scale = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        loginBackButton.frame = CGRect(x: 10, y: 26, width: 36, height: 36)
        cancelButton.frame = CGRect(x: loginBackButton.iright + 4, y: 26, width: 36, height: 36)
        
        titleLabel.frame = CGRect(x: 60 * scale, y: 264 * scale, width: Constant.ScreenSizeV2.SCREEN_WIDTH - 120 * scale, height: 25)
        hintLabel.frame = CGRect(x: 30 * scale, y: titleLabel.ibottom + 90 * scale, width: Constant.ScreenSizeV2.SCREEN_WIDTH - 60 * scale, height: 16)
        countDownLabel.frame = CGRect(x: hintLabel.iright, y: titleLabel.ibottom + 90 * scale, width: 80, height: 16)
        
        nocodeLabel1.frame = CGRect(x: 55 * scale, y: hintLabel.ibottom + 40 * scale, width: 90 * scale, height: 90 * scale)
        nocodeLabel2.frame = CGRect(x: nocodeLabel1.iright + 20 * scale, y: hintLabel.ibottom + 40 * scale, width: 90 * scale, height: 90 * scale)
        nocodeLabel3.frame = CGRect(x: nocodeLabel2.iright + 20 * scale, y: hintLabel.ibottom + 40 * scale, width: 90 * scale, height: 90 * scale)
        nocodeLabel4.frame = CGRect(x: nocodeLabel3.iright + 20 * scale, y: hintLabel.ibottom + 40 * scale, width: 90 * scale, height: 90 * scale)
        nocodeLabel5.frame = CGRect(x: nocodeLabel4.iright + 20 * scale, y: hintLabel.ibottom + 40 * scale, width: 90 * scale, height: 90 * scale)
        nocodeLabel6.frame = CGRect(x: nocodeLabel5.iright + 20 * scale, y: hintLabel.ibottom + 40 * scale, width: 90 * scale, height: 90 * scale)
        nocodeTextField.frame = CGRect(x: 55 * scale, y: hintLabel.ibottom + 40 * scale, width: Constant.ScreenSizeV2.SCREEN_WIDTH - 110 * scale, height: 90 * scale)
        
        loginButton.frame = CGRect(x: 60 * scale, y: nocodeTextField.ibottom + 50 * scale, width: Constant.ScreenSizeV2.SCREEN_WIDTH - 120 * scale, height: 88 * scale)
        noCodeHintLabel.frame = CGRect(x: 160 * scale, y: loginButton.ibottom + 40 * scale, width: Constant.ScreenSizeV2.SCREEN_WIDTH - 320 * scale, height: 18)
        
        
        loginBackButton.addTarget(self, action: #selector(LoginWithNoCodeViewController.loginBackButtonOnclicked), for: .touchUpInside)
        cancelButton.addTarget(self, action: #selector(LoginWithNoCodeViewController.cancelButtonOnclicked), for: .touchUpInside)
        
        self.nocodeTextField.addTarget(self, action: #selector(LoginWithNoCodeViewController.textFieldDidChange(_:)), for: .editingChanged)
        
        loginButton.addTarget(self, action: #selector(LoginWithNoCodeViewController.loginButtonOnClicked), for: .touchUpInside)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(LoginWithNoCodeViewController.nocodeHintLabelTaped))
        noCodeHintLabel.addGestureRecognizer(tapGesture)
        
        let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(LoginWithNoCodeViewController.countdownLabelTaped))
        countDownLabel.addGestureRecognizer(tapGesture1)
        
    
        let width = CalculateHeightOrWidth.getLabOrBtnWidth(self.hintLabel.text! as NSString, font: self.hintLabel.font, height: 16)
        self.hintLabel.iwidth = width
        let width1 =  CalculateHeightOrWidth.getLabOrBtnWidth(self.countDownLabel.text! as NSString, font: self.countDownLabel.font, height: 16)
        self.countDownLabel.iwidth = width1
        self.hintLabel.ileft = (Constant.ScreenSizeV2.SCREEN_WIDTH - width - width1 - 20 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE) / 2
        self.countDownLabel.ileft = self.hintLabel.iright + 20 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        if isSetPassword {
            loginButton.setTitle("下一步", for: UIControlState())
        }
        else if self.open_id != ""{
            self.loginButton.setTitle("绑 定", for: UIControlState())
        }
    }
    
    //MARK: 请求验证码
    func requestSignCode() {
        let sms_type = self.open_id == "" ? 4 : 3
        
        
        let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.sms, parameters: ["user_name": phoneNumber as AnyObject, "sms_type": sms_type as AnyObject],isShowErrorStatuMsg:true ,isNeedHud : true,successHandler: {data, status, msg in
            if status == 200 {
              self.timeout = 60
              self.countDownLabel.text = "60秒"
              let width1 =  CalculateHeightOrWidth.getLabOrBtnWidth(self.countDownLabel.text! as NSString, font: self.countDownLabel.font, height: 16)
              self.countDownLabel.iwidth = width1
              self.hintLabel.ileft = (Constant.ScreenSizeV2.SCREEN_WIDTH - self.hintLabel.iwidth - width1 - 20 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE) / 2
              self.countDownLabel.ileft = self.hintLabel.iright + 20 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
              self.noCodeHintLabel.isHidden = true
              self.signCodeButtonCountDown()
              self.countDownLabel.isUserInteractionEnabled = false
              self.nocodeTextField.becomeFirstResponder()
            }
            else{
                self.noCodeHintLabel.isHidden = false
            }
            
        })
        
    }
    
    // 验证码按钮 倒计时
    func signCodeButtonCountDown(shouldStop:Bool = false) {
        
        if timeout < 0 || shouldStop
        {
            self.countDownLabel.text = "重新获取"
            self.countDownLabel.isUserInteractionEnabled = true
            let width = CalculateHeightOrWidth.getLabOrBtnWidth(self.hintLabel.text! as NSString, font: self.hintLabel.font, height: 16)
            self.hintLabel.iwidth = width
            let width1 =  CalculateHeightOrWidth.getLabOrBtnWidth(self.countDownLabel.text! as NSString, font: self.countDownLabel.font, height: 16)
            self.countDownLabel.iwidth = width1
            self.hintLabel.ileft = (Constant.ScreenSizeV2.SCREEN_WIDTH - width - width1 - 20 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE) / 2
            self.countDownLabel.ileft = self.hintLabel.iright + 20 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
            return
        }
        
        if timeout <= 39 && self.noCodeHintLabel.isHidden {
            self.noCodeHintLabel.isHidden = false
        }
        
        let nf = NumberFormatter()
        nf.numberStyle = NumberFormatter.Style.decimal
        nf.maximumFractionDigits = 0
        nf.string(from: timeout - 1 as NSNumber)
        let timeoutShow = nf.string(from: timeout as NSNumber)! + "秒"
        self.countDownLabel.text = timeoutShow
        gcd.async(.main, delay: 1) {
            self.timeout = self.timeout - 1
            self.signCodeButtonCountDown()
        }
    }
    
    func countdownLabelTaped() {
//        self.noCodeHintLabel.isHidden = true
        self.loginButton.backgroundColor = Constant.Theme.ColorSeparatLine
        self.loginButton.isEnabled = false
        self.nocodeTextField.text = ""
        nocodeArray = ["","","","","",""]
        nocodeLabel1.text = nocodeArray[0]
        nocodeLabel2.text = nocodeArray[1]
        nocodeLabel3.text = nocodeArray[2]
        nocodeLabel4.text = nocodeArray[3]
        nocodeLabel5.text = nocodeArray[4]
        nocodeLabel6.text = nocodeArray[5]
        if self.voiceTimeout <= 0 {
            requestSignCode()
            return
        }
       Utils.showError(context: self.view, errorStr: "验证码已发送，请勿重复获取")
    }
    
    func loginBackButtonOnclicked() {
        self.signCodeButtonCountDown(shouldStop: true)
        self.voiceCodeCountDown(shouldStop: true)
        self.delegate?.countdownNumber(time: timeout)
        self.delegate?.voiceCodeCountdownNumber(time: voiceTimeout)
        let _ = self.navigationController?.popViewController(animated: false)
    }
    
    func cancelButtonOnclicked() {
        if !isFirstLogin {
            if let navigationController = self.navigationController{
                var formerVC : UIViewController?
                for temp in navigationController.viewControllers{
                    if temp.isKind(of: LoginViewController.self) || temp.isKind(of: LoginWithWechatViewController.self){
                        let animation = CATransition()
                        animation.duration = 0.3
                        
                        animation.type = kCATransitionReveal
                        
                        animation.subtype = kCATransitionFromBottom
                        
                        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                        
                        self.navigationController?.view.layer.add(animation, forKey: "")
                        let _ = self.navigationController?.popToViewController(formerVC!, animated: true)
                        
                        NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.notification_login_success), object: nil)
                    }
                    formerVC = temp
                }
            }
        }else {
             NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.show_mainTabBarController), object: nil)
        }
      
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        
        if let _ = textField.text{
            
            if textField.text!.characters.count >= 6{
                textField.text = NSString(string: textField.text!).substring(to: 6)
                self.loginButton.isEnabled = true
                self.loginButton.backgroundColor = Constant.Theme.Color1
            }else {
                self.loginButton.isEnabled = false
                self.loginButton.backgroundColor = Constant.Theme.ColorSeparatLine
            }
            
            nocodeArray = ["","","","","",""]
            var i = 0
            for item in (textField.text?.characters)! {
                nocodeArray[i] = item.description
                i += 1
            }
            
            nocodeLabel1.text = nocodeArray[0]
            nocodeLabel2.text = nocodeArray[1]
            nocodeLabel3.text = nocodeArray[2]
            nocodeLabel4.text = nocodeArray[3]
            nocodeLabel5.text = nocodeArray[4]
            nocodeLabel6.text = nocodeArray[5]
        }
       
    }
    
    func bindPhone()
    {
        var params = [String: AnyObject]()
        params["user_name"] = phoneNumber as AnyObject?
        params["open_id"] = open_id as AnyObject?
        params["code"] = self.nocodeTextField.text! as AnyObject?
        params["type"] = 3 as AnyObject?
        params["app_type"] = 1 as AnyObject?
        params["sns_token"] = accessToken as AnyObject?
        let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.regOpen, parameters: params, isNeedAppTokrn: true, isShowErrorStatuMsg: true, isNeedHud: true, successHandler: {data, status, msg in
            if status != 200
            {
                return
            }
            self.pushAndPopView(false)

        })
        
        
    }
    
    func loginButtonOnClicked() {
        if self.open_id != ""{
            bindPhone()
        }
        else{
        if isSetPassword {
            requestLogin(isfirst: self.isSetPassword)
            return
        }
        requestLogin(isfirst: self.isFirstLogin)
        }
    }
    
    func requestLogin(isfirst:Bool) {
        var params = [String: AnyObject]()
        
        params["user_name"] = phoneNumber as AnyObject?
        params["code"] = self.nocodeTextField.text! as AnyObject?
        
        params["app_type"] = 1 as AnyObject?
        
        let hintString = isfirst ? "" : "正在登录"
        let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.login_sms, parameters: params,isNeedAppTokrn: true, isShowErrorStatuMsg: true, isNeedHud: true, hudTitle: hintString, successHandler: {data, status, msg in
            
            if status == 200
            {
                
                if let is_reg = (data["data"] as? [String: AnyObject])?["is_reg"] as? Bool
                {
                    UserDefaults.standard.setValue(is_reg, forKey: "is_reg")
                    UserDefaults.standard.synchronize()
                    
                    //如果不是第一次登录
                    if is_reg == false{
                        self.SynchronizeUserAccountInfo(false)
                    }else{
                        //第一次用短信登录
                        self.SynchronizeUserAccountInfo(true)
                    }
                    
                }else{
                    self.SynchronizeUserAccountInfo(false)
                }
            }
        })
    }
    
    //MARK: 同步用户信息
    func SynchronizeUserAccountInfo(_ is_reg : Bool){
        
        if userInfoation != nil{
            if userInfoation!.avatarData != nil{
                self.uploadHeaderImage(userInfoation!.avatarData! as Data,is_reg: is_reg)
            }
            else if userInfoation!.gender != 0 || userInfoation!.birthday != "" || userInfoation!.birthday_type != 0{
                var userGender : Int = 0
                var userBirthday_type : Int = 0
                var userBirthday : String = ""
                if userInfoation?.gender != 0 && userInfoation?.gender != nil{
                    userGender = Int(userInfoation!.gender)
                }
                if userInfoation?.birthday_type != 0 && userInfoation?.birthday_type != nil{
                    userBirthday_type = Int(userInfoation!.birthday_type)
                }
                if userInfoation?.birthday != nil && userInfoation?.birthday != ""{
                    userBirthday = userInfoation!.birthday!
                }
                self.uploadUserInfo(userGender, birthday_type: userBirthday_type, birthday: userBirthday, avatar: "",is_reg: is_reg)
            }
            else{
                self.getUserInfo(is_reg)
            }
        }
        else{
            self.getUserInfo(is_reg)
        }
    }
    
    //MARK: 上传头像
    func uploadHeaderImage(_ imageData: Data,is_reg : Bool)
    {
        var isUpoadSuccess = true
        ImageUploader.uploadimage(context:self.view,data: imageData) { [weak self](id,domain) -> Void in
            if id == nil
            {
                isUpoadSuccess = false
            }
            
            if isUpoadSuccess{
                if self!.userInfoation!.gender != 0 || self!.userInfoation!.birthday != "" || self!.userInfoation!.birthday_type != 0{
                    var userGender : Int = 0
                    var userBirthday_type : Int = 0
                    var userBirthday : String = ""
                    if self!.userInfoation?.gender != 0 && self!.userInfoation?.gender != nil{
                        userGender = Int(self!.userInfoation!.gender)
                    }
                    if self!.userInfoation?.birthday_type != 0 && self!.userInfoation?.birthday_type != nil{
                        userBirthday_type = Int(self!.userInfoation!.birthday_type)
                    }
                    if self!.userInfoation?.birthday != "" && self!.userInfoation?.birthday != nil{
                        userBirthday = self!.userInfoation!.birthday!
                    }
                    self!.uploadUserInfo(userGender, birthday_type: userBirthday_type, birthday: userBirthday, avatar: id!,is_reg: is_reg)
                }
                else{
                    self!.getUserInfo(is_reg)
                }
            }
            else{
                if self!.userInfoation!.gender != 0 || self!.userInfoation!.birthday != "" || self!.userInfoation!.birthday_type != 0{
                    var userGender : Int = 0
                    var userBirthday_type : Int = 0
                    var userBirthday : String = ""
                    if self!.userInfoation?.gender != 0 && self!.userInfoation?.gender != nil{
                        userGender = Int(self!.userInfoation!.gender)
                    }
                    if self!.userInfoation?.birthday_type != 0 && self!.userInfoation?.birthday_type != nil{
                        userBirthday_type = Int(self!.userInfoation!.birthday_type)
                    }
                    if self!.userInfoation?.birthday != "" && self!.userInfoation?.birthday != nil{
                        userBirthday = self!.userInfoation!.birthday!
                    }
                    self!.uploadUserInfo(userGender, birthday_type: userBirthday_type, birthday: userBirthday, avatar: "",is_reg: is_reg)
                }
                else{
                    self!.getUserInfo(is_reg)
                }
            }
            
        }
        
    }
    
    
    //MARK: 上传用户信息
    func uploadUserInfo(_ gender : Int?,birthday_type : Int?,birthday : String?,avatar : String,is_reg : Bool){
        var params = [String: AnyObject]()
        if gender != 0{
            params["gender"] = gender as AnyObject?
        }
        if birthday_type != 0{
            params["birthday_type"] = birthday_type as AnyObject?
        }
        if birthday != ""{
            params["birthday"] = birthday as AnyObject?
        }
        if avatar != ""{
            params["avatar"] = avatar as AnyObject?
        }
        let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.syn_userSetting, parameters: params, isNeedUserTokrn: true,isShowErrorStatuMsg: true, isNeedHud: true,successHandler: { (data, status, msg) -> Void in
            if status == 200
            {
                self.getUserInfo(is_reg)
            }
        })
        
    }
    
    //MARK: 获取用户信息
    func getUserInfo(_ is_reg : Bool){
        let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.user_info_v2,isNeedUserTokrn: true, isShowErrorStatuMsg: true, isNeedHud: true, hudTitle: "",successHandler: { (data, status, msg) -> Void in
            if status == 200
            {
                
                let user = JSON(data as AnyObject)["data"]["user"]
                CoreDataManager.shared.update(user, entityType: Constant.CoreDataType.userInfoType,successHandler:{Void in
                },failureHandler: {Void in
                }
                )}

            if self.open_id == ""{
            if is_reg {
                //第一次用短信登录
                let settingPasswordVC = SettingPasswordViewController()
                settingPasswordVC.isFirstLogin = true
                self.navigationController?.pushViewController(settingPasswordVC, animated: true)
                return
 
            }
            else if self.isSetPassword {
                let settingPasswordVC = SettingPasswordViewController()
                settingPasswordVC.isFirstLogin = false
                self.navigationController?.pushViewController(settingPasswordVC, animated: true)
                return
                }
            }
            
            self.pushAndPopView(is_reg)
        })
        
    }

    //MARK:
    func pushAndPopView(_ is_reg : Bool){
        
        if is_reg == false//如果不是第一次登录
        {
            if !self.isFirstLogin{
                if let navigationController = self.navigationController{
                    var formerVC : UIViewController?
                    for temp in navigationController.viewControllers{
                        if temp.isKind(of: LoginViewController.self) || temp.isKind(of: LoginWithWechatViewController.self){
                            let animation = CATransition()
                            animation.duration = 0.3
                            
                            animation.type = kCATransitionReveal
                            
                            animation.subtype = kCATransitionFromBottom
                            
                            animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                            
                            self.navigationController?.view.layer.add(animation, forKey: "")
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.isLoginSuccess = true
                            let _ = self.navigationController?.popToViewController(formerVC!, animated: true)
                            
                            NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.notification_login_success), object: nil)
                        }
                        formerVC = temp
                    }
                }
            }
            else{
                NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.show_mainTabBarController), object: nil)
                
            }
        }
    }

    func nocodeHintLabelTaped() {
        if self.voiceTimeout <= 0 {
            let alertView = UIAlertController(title: "接收语音验证码", message: "验证码将以电话的方式通知您，请注意接听电话", preferredStyle: .alert)
            alertView.addAction(UIAlertAction(title: "取消", style: .default, handler: nil))
            alertView.addAction(UIAlertAction(title: "接收", style: .default, handler: { (alertAction) -> Void in
                self.voiceCodeRequest()
            }))
            present(alertView, animated: true, completion: nil)
        }
        else{
        Utils.showError(context: self.view, errorStr: "验证码已发送，请勿重复获取")
        }
        
        
       
    }

    func voiceCodeRequest() {
        let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.voice_sms, parameters: ["user_name": phoneNumber as AnyObject, "sms_type": 4 as AnyObject],isShowErrorStatuMsg:true ,isNeedHud : true,successHandler: {data, status, msg in
            if status == 200 {
                Utils.showError(context: self.view, errorStr: "请注意接听电话")
                self.voiceTimeout = 60
                self.voiceCodeCountDown()
                self.nocodeTextField.becomeFirstResponder()
            }
            
        })
    }
    
    // 验证码按钮 倒计时
    func voiceCodeCountDown(shouldStop:Bool = false) {
        if shouldStop {
            return
        }
        gcd.async(.main, delay: 1) {
            self.voiceTimeout = self.voiceTimeout - 1
            self.voiceCodeCountDown()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

class CustomTextField: UITextField {
    open override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(paste(_:)) {
            return false
        }
        if action == #selector(select(_:)) {
            return false
        }
        if action == #selector(selectAll(_:)) {
            return false
        }
        return super.canPerformAction(action, withSender: sender)
    }
}
