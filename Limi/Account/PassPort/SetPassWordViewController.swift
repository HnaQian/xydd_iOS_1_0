//
//  SetPassWordViewController.swift
//  Limi
//
//  Created by ZhiQiangMi on 15/8/19.
//  Copyright (c) 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit
import CoreData
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


class SetPassWordViewController: BaseViewController,UITextFieldDelegate {
    
    
    @IBOutlet weak var setPWDTextField: UITextField!
    @IBOutlet weak var confirmPWDTextField: UITextField!
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        let image = UIImage(named: "titlebarGrey")
        self.navigationController?.navigationBar.setBackgroundImage(image, for: UIBarMetrics.default)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setPWDTextField.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        let image = UIImage(named: "titlebar")
        self.navigationController?.navigationBar.setBackgroundImage(image, for: UIBarMetrics.default)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "设置密码"
        
        let confimBtn = UIButton(frame: CGRect(x: 0,y: 0,width: 80,height: 40))
        
        confimBtn.setTitle("确认提交", for: UIControlState())
        confimBtn.titleLabel?.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        confimBtn.setTitleColor(UIColor(rgba: Constant.common_C1_color), for: UIControlState())
        confimBtn.addTarget(self, action: #selector(SetPassWordViewController.doneBtnClick(_:)), for: UIControlEvents.touchUpInside)
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: confimBtn)
        
        
        self.setPWDTextField.delegate = self
        self.confirmPWDTextField.delegate = self
        self.setPWDTextField.isSecureTextEntry = true
        self.confirmPWDTextField.isSecureTextEntry = true
        self.setPWDTextField.clearButtonMode = UITextFieldViewMode.whileEditing
        self.confirmPWDTextField.clearButtonMode = UITextFieldViewMode.whileEditing
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        if range.location >= 15{
            return false
        }
        if string == "\n"{
            textField.resignFirstResponder()
            return true
        }
        
        let validTimeCharacterSet = CharacterSet(charactersIn: Constant.ALPHANUM)
        let invalidTimeCharacterSet = validTimeCharacterSet.inverted
        let array:NSArray = string.components(separatedBy: invalidTimeCharacterSet) as NSArray
        let filtered:NSString = array.componentsJoined(by: "") as NSString
        if string.characters.count > 0 {
            if string != filtered as String{
                Utils.showError(context: self.view, errorStr: "密码只限于大小写字母与数字")
                return false
            }
            else{
                return true
            }
        }
        return true
    }
    
    @objc func doneBtnClick(_ sender: AnyObject) {
        if self.setPWDTextField.text!.isEmpty {
            Utils.showError(context: self.view, errorStr: "请填写密码")
            return
        }
        else if self.setPWDTextField.text?.characters.count < 6{
            Utils.showError(context: self.view, errorStr: "密码应为6-15位字母、数字、符号组合，请重新设定")
            return
        }
        else if self.confirmPWDTextField.text!.isEmpty {
            Utils.showError(context: self.view, errorStr: "请再次输入密码")
            return
        }
        else if self.setPWDTextField.text != self.confirmPWDTextField.text{
            Utils.showError(context: self.view, errorStr: "两次填写的密码不一致,请重新输入")
            return
        }
        
        let password : String = self.confirmPWDTextField.text!
        let re_password : String = self.confirmPWDTextField.text!
        
        let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.login_resetPassword, parameters: ["password": password as AnyObject,"re_password": re_password as AnyObject], isNeedUserTokrn: true,isShowErrorStatuMsg: true, isShowSuccessStatuMsg:true,isNeedHud: true,successHandler: {data, status, msg in
            if status == 200
            {
                let userInfo = CoreDataManager.shared.getDBManager(Constant.CoreDataType.userInfoType) as? User
                
                userInfo?.has_password = true
                
                CoreDataManager.shared.save({Void in
                    let _ = self.navigationController?.popViewController(animated: true)
                    },failureHandler: {Void in
                       let _ = self.navigationController?.popViewController(animated: true)
                        
                })
            }
        })
        
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
