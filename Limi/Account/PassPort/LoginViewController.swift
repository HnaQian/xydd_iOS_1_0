//
//  LoginViewController.swift
//  Limi
//
//  Created by 程巍巍 on 7/23/15.
//  Copyright (c) 2015 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import Foundation
import CoreData

final class LoginViewController: BaseLoginViewController,LoginWithNoCodeViewControllerDelegate{
    
    
    var isOutLogin : Bool = false
    var isCodeLogin = true //是否是验证码登录，默认是验证码登录
    let nocodeLoginVc = LoginWithNoCodeViewController()
    var time:TimeInterval = 0
    var voiceTime:TimeInterval = 0
    
    override func loginBackButtonOnclicked()
    {
        if !self.isOutLogin{
            let animation = CATransition()
            animation.duration = 0.3
            
            animation.type = kCATransitionReveal
            
            animation.subtype = kCATransitionFromBottom
            
            animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            
            self.navigationController?.view.layer.add(animation, forKey: "")
            let _ = self.navigationController?.popViewController(animated: false)
        }
        else{
            let _ = self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.nocodeLoginVc.delegate = self
    }
    
    
   override func nextButtonOnClicked() {
        if !Utils.isValiableMobile(phoneNumber: self.phoneField.text!) {
            Utils.showError(context: self.view, errorStr: "请输入正确的手机号")
            return
        }
        if self.isCodeLogin {
            self.timeCountDown(shoulStop: true)
            self.voiceTimeCountDown(shoulStop: true)
            self.nocodeLoginVc.timeout = self.time
            self.nocodeLoginVc.voiceTimeout = self.voiceTime
            self.nocodeLoginVc.isFirstLogin = self.isOutLogin
            self.nocodeLoginVc.isCodeLogin = true
            self.nocodeLoginVc.phoneNumber = (self.phoneField.text?.replacingOccurrences(of: " ", with: ""))!
            self.navigationController?.pushViewController(self.nocodeLoginVc, animated: true)
        }else {
            let passwordLoginVc = LoginWithPasswordViewController()
            passwordLoginVc.isOutLogin = self.isOutLogin
            passwordLoginVc.phoneNumber = (self.phoneField.text?.replacingOccurrences(of: " ", with: ""))!
            self.navigationController?.pushViewController(passwordLoginVc, animated: true)
        }
        
    }
    
    func timeCountDown(shoulStop:Bool = false) {
        
        if self.time < 0 || shoulStop
        {
            return
        }

        gcd.async(.main, delay: 1) {
            self.time = self.time - 1
            self.timeCountDown()
        }
    }
    
    func voiceTimeCountDown(shoulStop:Bool = false) {
        
        if self.voiceTime < 0
        {
            return
        }
        
        gcd.async(.main, delay: 1) {
            self.voiceTime = self.voiceTime - 1
            self.voiceTimeCountDown()
        }
    }
    
    func countdownNumber(time: TimeInterval) {
        self.time = time
        self.timeCountDown()
    }
    
    func voiceCodeCountdownNumber(time: TimeInterval) {
        self.voiceTime = time
        self.voiceTimeCountDown()
    }
    
   override func passwordLabelTaped() {
        self.isCodeLogin = !self.isCodeLogin
        if isCodeLogin {
            self.titleLabel.text = "无需注册 一键登录"
            self.passwordLabel.text = "密码登录"
        }else {
            self.titleLabel.text = "使用密码登录"
            self.passwordLabel.text = "验证码登录"
        }
    }
    
   override func textFieldDidChange(_ textField: UITextField) {
        
        if let _ = textField.text{
            
            textField.text = checkNumber(textField.text!)
            
            //长度控制在11个数字加两个空格
            if textField.text!.characters.count >= 13{
                textField.text = NSString(string: textField.text!).substring(to: 13)
                self.nextButton.isEnabled = true
                self.nextButton.backgroundColor = Constant.Theme.Color1
            }else {
                self.nextButton.isEnabled = false
                self.nextButton.backgroundColor = Constant.Theme.ColorSeparatLine
            }
            
        }
        if (self.phoneField.text?.characters.count)! > 0{
            self.clearButton.isHidden = false
        }else{
            self.clearButton.isHidden = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
