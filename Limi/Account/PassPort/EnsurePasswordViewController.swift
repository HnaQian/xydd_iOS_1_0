//
//  EnsurePasswordViewController.swift
//  Limi
//
//  Created by maohs on 16/12/7.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//确认密码
import UIKit

class EnsurePasswordViewController: BaseLoginViewController {

    
    fileprivate let cancelButton:UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named: "webview_cancel"), for: UIControlState.normal)
        
        return btn
    }()
    
    fileprivate let hintLabel:UILabel = {
        let lbl = UILabel()
        lbl.font = Constant.Theme.Font_14
        lbl.textColor = UIColor(rgba: Constant.common_C7_color)
        lbl.textAlignment = .center
        lbl.text = "请输入6-15位数字、字母组合"
        return lbl
    }()
    
    var isFirstLogin = false
    var phoneNumber = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func resetUI() {
        self.view.addSubview(cancelButton)
        
        let scale = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        cancelButton.frame = CGRect(x: loginBackButton.iright + 4, y: 26, width: 36, height: 36)
        hintLabel.frame = CGRect(x: 30 * scale, y: titleLabel.ibottom + 90 * scale, width: Constant.ScreenSizeV2.SCREEN_WIDTH - 60 * scale, height: 16)
        cancelButton.addTarget(self, action: #selector(EnsurePasswordViewController.cancelButtonOnclicked), for: .touchUpInside)
        
        self.titleLabel.text = "请再次输入新密码"
        self.passwordLabel.isHidden = true
        phoneField.keyboardType = UIKeyboardType.namePhonePad
        phoneField.isSecureTextEntry = true
        phoneField.placeholder  = ""

        clearButton.isHidden = false
        nextButton.backgroundColor = Constant.Theme.ColorSeparatLine
        nextButton.isEnabled = false
        nextButton.setTitle("确定", for: UIControlState())
        clearButton.setImage(UIImage(named: "login_hide"), for: UIControlState.normal)
        clearButton.setImage(UIImage(named: "login_show"), for: UIControlState.selected)
    }
    
    override func loginBackButtonOnclicked() {
        let _ = self.navigationController?.popViewController(animated: false)
    }
    
    func cancelButtonOnclicked() {
        if let navigationController = self.navigationController{
            var formerVC : UIViewController?
            for temp in navigationController.viewControllers{
                if temp.isKind(of: LoginViewController.self){
                    let animation = CATransition()
                    animation.duration = 0.3
                    
                    animation.type = kCATransitionReveal
                    
                    animation.subtype = kCATransitionFromBottom
                    
                    animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                    
                    self.navigationController?.view.layer.add(animation, forKey: "")
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.isLoginSuccess = true
                    let _ = self.navigationController?.popToViewController(formerVC!, animated: true)
                    
                    NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.notification_login_success), object: nil)
                }
                formerVC = temp
            }
        }
    }
    
    override func clearButtonOnClicked() {
        clearButton.isSelected = !clearButton.isSelected
        if clearButton.isSelected {
            phoneField.isSecureTextEntry = false
        }else {
            phoneField.isSecureTextEntry = true
        }
    }
    
    override func textFieldDidChange(_ textField: UITextField) {
        if let _ = textField.text{
            
            if textField.text!.characters.count >= 6{
                nextButton.backgroundColor = Constant.Theme.Color1
                nextButton.isEnabled = true
            }else {
                nextButton.backgroundColor = Constant.Theme.ColorSeparatLine
                nextButton.isEnabled = false
            }
        }
    }
    
    override func nextButtonOnClicked() {
        if self.phoneField.text!.isEmpty {
            Utils.showError(context: self.view, errorStr: "请填写密码")
            return
        }else if (self.phoneField.text?.characters.count)! < 6 || (self.phoneField.text?.characters.count)! > 15{
            Utils.showError(context: self.view, errorStr: "请输入6-15位数字、字母组合")
            return
        }else if self.phoneField.text! != self.phoneNumber {
            Utils.showError(context: self.view, errorStr: "两次输入不一致，请重新设置")
            return
        }
        
        firstUploadPassword()
    }
    
    func firstUploadPassword() {
        
        let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.login_resetPassword, parameters: ["password": phoneField.text! as AnyObject,"re_password": phoneField.text! as AnyObject], isNeedUserTokrn: true,isShowErrorStatuMsg: true, isShowSuccessStatuMsg:true,isNeedHud: true,successHandler: {data, status, msg in
            if status == 200
            {
                let userInfo = CoreDataManager.shared.getDBManager(Constant.CoreDataType.userInfoType) as? User
                
                userInfo?.has_password = true
                let isreg = UserDefaults.standard.bool(forKey: "is_reg")
                CoreDataManager.shared.save({Void in
                  self.pushAndPopView(isreg)
                },failureHandler: {Void in
                   self.pushAndPopView(isreg)
                })
                
            }
        })
    }
    
    //MARK:
    func pushAndPopView(_ is_reg : Bool){
        
        if is_reg == false//如果不是第一次登录
        {
            if !self.isFirstLogin{
                if let navigationController = self.navigationController{
                    var formerVC : UIViewController?
                    for temp in navigationController.viewControllers{
                        if temp.isKind(of: LoginViewController.self){
                            let animation = CATransition()
                            animation.duration = 0.3
                            
                            animation.type = kCATransitionReveal
                            
                            animation.subtype = kCATransitionFromBottom
                            
                            animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                            
                            self.navigationController?.view.layer.add(animation, forKey: "")
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.isLoginSuccess = true
                            let _ = self.navigationController?.popToViewController(formerVC!, animated: true)
                            
                            NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.notification_login_success), object: nil)
                        }
                        formerVC = temp
                    }
                }
            }
            else{
                NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.show_mainTabBarController), object: nil)
                
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
