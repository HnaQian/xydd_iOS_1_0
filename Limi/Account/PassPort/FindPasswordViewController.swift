//
//  FindPasswordViewController.swift
//  Limi
//
//  Created by 程巍巍 on 7/24/15.
//  Copyright (c) 2015 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit
import CoreData
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


class FindPasswordViewController: BaseViewController,UITextFieldDelegate{
    // 手机号
    var phoneString: String?
    var userInfoation : User?
    var isOutLogin : Bool = false
    fileprivate var findView: FindView {return self.view as! FindView}
    override func loadView() {
        super.loadView()
        self.view = FindView()
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.navigationItem.title = ""
        
        
        findView.phoneField.keyboardType = UIKeyboardType.numberPad
        findView.phoneField.text = phoneString ?? nil
        
        findView.pwField.isSecureTextEntry = true
        findView.comfirmPwField.isSecureTextEntry = true
        
        
        findView.pwField.delegate = self
        findView.comfirmPwField.delegate = self
        
        
        findView.nextStepButton.isEnabled = true
        findView.voiceCodeButton.isEnabled = true
        findView.commitButton.isEnabled = true
        
        // 业务逻辑
        // 获取验证码
        findView.signCodeButton.addTarget(self, action: #selector(FindPasswordViewController.requestSignCode), for: .touchUpInside)
        // 验证 验证码有效性
        
        findView.voiceCodeButton.addTarget(self, action: #selector(FindPasswordViewController.voiceCodeButtonClick), for: .touchUpInside)
        findView.voiceCodeButton.isHidden = true
        
        findView.nextStepButton.addTarget(self, action: #selector(FindPasswordViewController.vertifySignCode), for: .touchUpInside)
        // 提交修改密码
        findView.commitButton.addTarget(self, action: #selector(FindPasswordViewController.commitPassword), for: .touchUpInside)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        findView.phoneField.becomeFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        let image = UIImage(named: "titlebarGrey")
        self.navigationController?.navigationBar.setBackgroundImage(image, for: UIBarMetrics.default)
        
        let requestUserInfo: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        let datasourceUserInfo : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(requestUserInfo)!
        if datasourceUserInfo.count > 0{
            userInfoation = datasourceUserInfo[0] as? User
        }
        else{
            userInfoation = nil
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        let image = UIImage(named: "titlebar")
        self.navigationController?.navigationBar.setBackgroundImage(image, for: UIBarMetrics.default)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // 请求验证码
    func requestSignCode() {
        // 检查手机号
        if findView.phoneField.text!.isEmpty {
            Utils.showError(context: self.view, errorStr: "请输入手机号")
            return
        }
        
        if findView.phoneField.text?.characters.count < 11{
            Utils.showError(context: self.view, errorStr: "请输入正确的手机号")
            return
        }
        findView.voiceCodeButton.isHidden = false
        let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.sms, parameters: ["user_name": findView.phoneField.text! as AnyObject, "sms_type": 2 as AnyObject],isShowErrorStatuMsg:true,isNeedHud : true,successHandler: {data, status, msg in
            if status == 200
            {
                self.findView.signCodeButton.isEnabled = false
                self.findView.timeout = 60
                self.findView.signCodeButtonCountDown()
                self.findView.signCodeField.becomeFirstResponder()
            }
        })
        
        
        
    }
    
    func voiceCodeButtonClick() {
        // 检查手机号
        if findView.phoneField.text!.isEmpty {
            Utils.showError(context: self.view, errorStr: "请输入手机号")
            return
        }
        
        if findView.phoneField.text?.characters.count < 11{
            Utils.showError(context: self.view, errorStr: "请输入正确的手机号")
            return
        }
        
        if self.findView.timeout > 0{
            Utils.showError(context: self.view, errorStr: "请" + String(Int(self.findView.timeout)) + "秒后点我接收语音验证")
            return
        }
        
        let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.voice_sms, parameters: ["user_name": findView.phoneField.text! as AnyObject, "sms_type": 2 as AnyObject],isShowErrorStatuMsg:true,isNeedHud : true,successHandler: {data, status, msg in
            if status == 200
            {
                Utils.showError(context: self.view, errorStr: "请注意接听电话")
                self.findView.signCodeButton.isEnabled = false
                self.findView.timeout = 60
                self.findView.signCodeButtonCountDown()
                self.findView.signCodeField.becomeFirstResponder()
            }
        })
        
        
        
    }
    // 验证 验证码
    func vertifySignCode() {
        
        if findView.phoneField.text!.isEmpty {
            Utils.showError(context: self.view, errorStr: "请输入手机号")
            return
        }
        
        if findView.phoneField.text?.characters.count != 11
        {
            Utils.showError(context: self.view, errorStr: "请输入正确的手机号")
            return
        }
        
        // 检查验证码输入框
        if findView.signCodeField.text!.isEmpty {
            Utils.showError(context: self.view, errorStr: "请输入验证码！")
            return
        }
        
        let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.sms_check, parameters: ["user_name": findView.phoneField.text! as AnyObject, "sms_type": 2 as AnyObject, "code": findView.signCodeField.text! as AnyObject],isShowErrorStatuMsg:true,isNeedHud : true,successHandler: {data, status, msg in
            if status == 200
            {
                self.findView.phoneField.resignFirstResponder()
                self.findView.signCodeField.resignFirstResponder()
                
                // 成功，则进入下一步
                var frame: CGRect = self.findView.scrollView.frame
                    
                frame.origin.x = frame.width
                
                self.findView.scrollView.scrollRectToVisible(frame, animated: true)
                
                // 更新 VC  title
                self.navigationItem.title = "重置密码"
                
                // 进入密码设置界面后，密码框获取焦点
                self.findView.pwField.becomeFirstResponder()
                
            }
        })
        
    }
    
    // 设置密码
    func commitPassword() {
        
        if findView.pwField.text!.isEmpty{
            Utils.showError(context: self.view, errorStr: "请设置密码！")
            return
        }
        
        if findView.comfirmPwField.text!.isEmpty {
            Utils.showError(context: self.view, errorStr: "请再次输入密码")
            return
        }
            
        else if findView.pwField.text != findView.comfirmPwField.text{
            Utils.showError(context: self.view, errorStr: "两次填写的密码不一致，请重新输入")
            return
        }
        
        if findView.pwField.text?.characters.count < 6 {
            Utils.showError(context: self.view, errorStr: "密码应为6-15位字母、数字、符号组合，请重新设定")
            return
        }
        
        //  提交
        findView.pwField.resignFirstResponder()
        findView.comfirmPwField.resignFirstResponder()
        
        let userName : String = findView.phoneField.text!
        let password : String = findView.pwField.text!
        let code : String = findView.signCodeField.text!
        
        let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.login_findPassword, parameters: ["user_name": userName as AnyObject, "code": code as AnyObject, "password": password as AnyObject, "re_password": password as AnyObject],isShowErrorStatuMsg:true,isNeedHud : true,successHandler: {data, status, msg in
            if status == 200
            {
                self.SynchronizeUserInfo()
            }
        })
        
    }
    
    func SynchronizeUserInfo(){
        self.SynchronizeUserAccountInfo()
    }
    
    func SynchronizeUserAccountInfo(){
        if userInfoation != nil{
            if userInfoation!.avatarData != nil{
                self.uploadHeaderImage(userInfoation!.avatarData! as Data)
            }
            else if userInfoation!.gender != 0 || userInfoation!.birthday != "" || userInfoation!.birthday_type != 0{
                var userGender : Int = 0
                var userBirthday_type : Int = 0
                var userBirthday : String = ""
                if userInfoation?.gender != 0 && userInfoation?.gender != nil{
                    userGender = Int(userInfoation!.gender)
                }
                if userInfoation?.birthday_type != 0 && userInfoation?.birthday_type != nil{
                    userBirthday_type = Int(userInfoation!.birthday_type)
                }
                if userInfoation?.birthday != "" && userInfoation?.birthday != nil{
                    userBirthday = userInfoation!.birthday!
                }
                self.uploadUserInfo(userGender, birthday_type: userBirthday_type, birthday: userBirthday, avatar: "")
            }
            else{
                self.getUserInfo()
            }
        }
        else{
            self.getUserInfo()
        }
    }
    
    func uploadHeaderImage(_ imageData: Data)
    {
        var isUpoadSuccess = true
        ImageUploader.uploadimage(context:self.view,data: imageData) { [weak self](id,domain) -> Void in
            if id == nil
            {
                isUpoadSuccess = false
            }
            if isUpoadSuccess{
                if self!.userInfoation!.gender != 0 || self!.userInfoation!.birthday != "" || self!.userInfoation!.birthday_type != 0{
                    var userGender : Int = 0
                    var userBirthday_type : Int = 0
                    var userBirthday : String = ""
                    if self!.userInfoation?.gender != 0 && self!.userInfoation?.gender != nil{
                        userGender = Int(self!.userInfoation!.gender)
                    }
                    if self!.userInfoation?.birthday_type != 0 && self!.userInfoation?.birthday_type != nil{
                        userBirthday_type = Int(self!.userInfoation!.birthday_type)
                    }
                    if self!.userInfoation?.birthday != "" && self!.userInfoation?.birthday != nil{
                        userBirthday = self!.userInfoation!.birthday!
                    }
                    self!.uploadUserInfo(userGender, birthday_type: userBirthday_type, birthday: userBirthday, avatar: id!)
                }
                else{
                    self!.getUserInfo()
                }
            }
            else{
                if self!.userInfoation!.gender != 0 || self!.userInfoation!.birthday != "" || self!.userInfoation!.birthday_type != 0{
                    var userGender : Int = 0
                    var userBirthday_type : Int = 0
                    var userBirthday : String = ""
                    if self!.userInfoation?.gender != 0 && self!.userInfoation?.gender != nil {
                        userGender = Int(self!.userInfoation!.gender)
                    }
                    if self!.userInfoation?.birthday_type != 0 && self!.userInfoation?.birthday_type != nil{
                        userBirthday_type = Int(self!.userInfoation!.birthday_type)
                    }
                    if self!.userInfoation?.birthday != "" && self!.userInfoation?.birthday != nil{
                        userBirthday = self!.userInfoation!.birthday!
                    }
                    self!.uploadUserInfo(userGender, birthday_type: userBirthday_type, birthday: userBirthday, avatar: "")
                }
                else{
                    self!.getUserInfo()
                }
            }
            
        }
        
    }
    
    func uploadUserInfo(_ gender : Int?,birthday_type : Int?,birthday : String?,avatar : String){
        var params = [String: AnyObject]()
        if gender != 0{
            params["gender"] = gender as AnyObject?
        }
        if birthday_type != 0{
            params["birthday_type"] = birthday_type as AnyObject?
        }
        if birthday != ""{
            params["birthday"] = birthday as AnyObject?
        }
        if avatar != ""{
            params["avatar"] = avatar as AnyObject?
        }
        let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.syn_userSetting, parameters: params, isNeedUserTokrn: true, isShowErrorStatuMsg: true, isNeedHud: true,successHandler: { (data, status, msg) -> Void in
            if status == 200
            {
                self.getUserInfo()
            }
        })
        
    }
    
    
    func getUserInfo(){
        let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.user_info_v2,isNeedUserTokrn: true,isShowErrorStatuMsg: true, isNeedHud: true, successHandler: { (data, status, msg) -> Void in
            if status == 200
            {
                if let dataJson : JSON = JSON(data["data"]!) as JSON?{
                    if let user : JSON = dataJson["user"] as JSON?{
                        CoreDataManager.shared.update(user, entityType: Constant.CoreDataType.userInfoType,successHandler: {Void in
                            self.popView()},failureHandler: {Void in
                                self.popView()
                        })
                    }
                    
                    
                }
            }
        })
        
    }
    
    func popView(){
        if !self.isOutLogin{
            var formerVC : UIViewController?
            for temp in self.navigationController!.viewControllers{
                
                if temp.isKind(of: LoginViewController.self){
                    let animation = CATransition()
                    animation.duration = 0.3
                    
                    animation.type = kCATransitionReveal
                    
                    animation.subtype = kCATransitionFromBottom
                    
                    animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                    
                    self.navigationController?.view.layer.add(animation, forKey: "")
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.isLoginSuccess = true
                    let _ = self.navigationController?.popToViewController(formerVC!, animated: true)
                    NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.notification_login_success), object: nil)
                }
                formerVC = temp
                
            }
        }
        else{
            NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.show_mainTabBarController), object: nil)
            
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        if textField == findView.pwField || textField == findView.comfirmPwField
        {
            if range.location >= 15{
                return false
            }
            
            let validTimeCharacterSet = CharacterSet(charactersIn: Constant.ALPHANUM)
            let invalidTimeCharacterSet = validTimeCharacterSet.inverted
            let array:NSArray = string.components(separatedBy: invalidTimeCharacterSet) as NSArray
            let filtered:NSString = array.componentsJoined(by: "") as NSString
            
            if string != filtered as String{
                Utils.showError(context: self.view, errorStr: "密码只限大小写字母与数字")
                return false
            }
            else{
                return true
            }
            
        }
        return false
        
    }
    
}


extension FindPasswordViewController{
    fileprivate final class FindView: UIView {
        
        fileprivate var timeout: TimeInterval = 0
        var scrollView = UIScrollView()
        var contentView = UIView()
        
        var phoneField = UITextField()
        var signCodeField = UITextField()
        var signCodeButton = UIButton()
        var nextStepButton = UIButton()
        var voiceCodeButton = HyperlinksButton()
        
        var pwField = UITextField()
        var comfirmPwField = UITextField()
        var commitButton = UIButton()
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            self.backgroundColor = UIColor(rgba: Constant.common_background_color)
            
            scrollView.isScrollEnabled = false
            scrollView.showsHorizontalScrollIndicator = false
            scrollView.showsVerticalScrollIndicator = false
            
            let leftTitleLabel = UILabel()
            let rightTitleLabel = UILabel()
            
            let sl0 = separatorLine()
            let sl1 = separatorLine()
            let sl2 = separatorLine()
            let sl3 = separatorLine()
            
            leftTitleLabel.text = "请输入注册或绑定的手机号"
            
            rightTitleLabel.text = "请设置一个新密码"
            
            leftTitleLabel.textColor = UIColor(rgba: "#555555")
            rightTitleLabel.textColor = UIColor(rgba: "#555555")
            
            leftTitleLabel.font = UIFont.systemFont(ofSize: 19)
            rightTitleLabel.font = UIFont.systemFont(ofSize: 19)
            
            phoneField.clearButtonMode = UITextFieldViewMode.whileEditing
            signCodeField.clearButtonMode = UITextFieldViewMode.whileEditing
            signCodeField.keyboardType = UIKeyboardType.numberPad
            
            phoneField.placeholder = "手机号"
            signCodeField.placeholder = "验证码"
            signCodeButton.setTitle("获取验证码", for: UIControlState())
            signCodeButton.setTitleColor(UIColor(rgba: "#555555"), for: UIControlState())
            // signCodeButton.titleLabel?.textAlignment = NSTextAlignment.Left
            signCodeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
            signCodeButton.contentEdgeInsets.left = 0
            signCodeButton.titleLabel?.font = UIFont.systemFont(ofSize: 15)
            
            nextStepButton.setTitle("下一步", for: UIControlState())
            voiceCodeButton.setTitle("收不到验证码? 点我接收验证码电话", for: UIControlState())
            pwField.placeholder = "请设置密码"
            comfirmPwField.placeholder = "请再次输入密码"
            
            pwField.clearButtonMode = UITextFieldViewMode.whileEditing
            
            comfirmPwField.clearButtonMode = UITextFieldViewMode.whileEditing
            
            commitButton.setTitle("提 交", for: UIControlState())
            
            
            nextStepButton.titleLabel?.font = UIFont.systemFont(ofSize: 15)
            voiceCodeButton.titleLabel?.font = UIFont.systemFont(ofSize: 13)
            
            
            
            signCodeButton.setTitleColor(UIColor.black, for: UIControlState())
            nextStepButton.setTitleColor(UIColor(rgba: "#FFFFFF"), for: UIControlState())
            nextStepButton.setTitleColor(UIColor.lightGray, for: UIControlState.disabled)
            nextStepButton.backgroundColor = UIColor(rgba: "#E13636")
            
            voiceCodeButton.setTitleColor(UIColor.lightGray, for: UIControlState())
            voiceCodeButton.titleLabel?.textAlignment = .right
            voiceCodeButton.setColor(UIColor.lightGray)
            
            commitButton.setTitleColor(UIColor(rgba: "#FFFFFF"), for: UIControlState())
            commitButton.setTitleColor(UIColor.lightGray, for: UIControlState.disabled)
            commitButton.backgroundColor = UIColor(rgba: "#E13636")
            
            signCodeButton.titleLabel?.font = UIFont.systemFont(ofSize: 15)
            
            nextStepButton.layer.borderColor = UIColor(rgba: "#E13636").cgColor
            nextStepButton.layer.borderWidth = 0.5
            nextStepButton.layer.cornerRadius = 5
            commitButton.layer.borderColor = UIColor(rgba: "#E13636").cgColor
            commitButton.layer.borderWidth = 0.5
            commitButton.layer.cornerRadius = 5
            
            phoneField.font = UIFont.systemFont(ofSize: 15)//15
            signCodeField.font = UIFont.systemFont(ofSize: 15)//15
            pwField.font = UIFont.systemFont(ofSize: 15)//15
            comfirmPwField.font = UIFont.systemFont(ofSize: 15)//15
            
            
            // 布局
            self.addSubview(scrollView)
            scrollView.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(self)
                let _ = make.left.equalTo(self)
                let _ = make.right.equalTo(self)
                let _ = make.bottom.equalTo(self)
            }
            
            
            scrollView.addSubview(contentView)
            
            contentView.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(scrollView)
                let _ = make.left.equalTo(scrollView)
                let _ = make.right.equalTo(scrollView)
                let _ = make.bottom.equalTo(scrollView)
                let _ = make.width.equalTo(scrollView).multipliedBy(2)
                let _ = make.height.equalTo(scrollView)
            }
            
            
            for view in [phoneField, sl0, signCodeField, signCodeButton, sl1,voiceCodeButton, nextStepButton, pwField, sl2, comfirmPwField, sl3, commitButton, leftTitleLabel, rightTitleLabel] {
                contentView.addSubview(view)
            }
            // 左边 手机验证界面
            
            leftTitleLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(contentView).offset(20)
                let _ = make.centerX.equalTo(contentView).multipliedBy(0.5)
            }
            
            phoneField.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(leftTitleLabel.snp_bottom).offset(66)
                let _ = make.width.equalTo(contentView).multipliedBy(0.5).offset(-30)
                let _ = make.centerX.equalTo(leftTitleLabel)
            }
            
            
            sl0.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(phoneField.snp_bottom).offset(4)
                let _ = make.width.equalTo(contentView).multipliedBy(0.5).offset(-16)
                let _ = make.centerX.equalTo(phoneField)
            }
            
            
            signCodeField.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(sl0.snp_bottom).offset(18)
                let _ = make.left.equalTo(phoneField)
            }
            
            signCodeButton.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(signCodeField.snp_right).offset(8)
                let _ = make.right.equalTo(phoneField)
                let _ = make.centerY.equalTo(signCodeField)
                let _ = make.width.equalTo(100)
            }
            
            sl1.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(signCodeField.snp_bottom).offset(4)
                let _ = make.width.equalTo(sl0)
                let _ = make.centerX.equalTo(sl0)
            }
            
            voiceCodeButton.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(sl1.snp_bottom).offset(20)
                let _ = make.width.equalTo(210)
                let _ = make.centerX.equalTo(sl1)
                let _ = make.height.equalTo(30)
            }
            
            nextStepButton.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(voiceCodeButton.snp_bottom).offset(36)
                let _ = make.width.equalTo(sl1)
                let _ = make.centerX.equalTo(sl1)
                let _ = make.height.equalTo(34)
            }
            
            
            // 右侧 设置密码界面
            rightTitleLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(leftTitleLabel)
                let _ = make.centerX.equalTo(contentView).multipliedBy(1.5)
            }
            
            pwField.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(phoneField)
                let _ = make.width.equalTo(phoneField)
                let _ = make.centerX.equalTo(contentView).multipliedBy(1.5)
            }
            
            sl2.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(sl0)
                let _ = make.width.equalTo(sl0)
                let _ = make.centerX.equalTo(pwField)
            }
            
            comfirmPwField.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(signCodeField)
                let _ = make.width.equalTo(pwField)
                let _ = make.centerX.equalTo(pwField)
            }
            
            
            sl3.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(sl1)
                let _ = make.width.equalTo(sl1)
                let _ = make.centerX.equalTo(pwField)
            }
            
            commitButton.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(nextStepButton)
                let _ = make.centerX.equalTo(pwField)
                let _ = make.width.equalTo(nextStepButton)
                let _ = make.height.equalTo(nextStepButton)
            }
            
        }
        
        required init(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func separatorLine() ->UIView {
            let view = UIView()
            view.backgroundColor = UIColor(rgba: Constant.common_separatLine_color)
            
            view.snp_makeConstraints { (make) -> Void in
                let _ = make.height.equalTo(0.5)
            }
            
            return view
        }
        
        // 验证码按钮 倒计时
        func signCodeButtonCountDown() {
            if timeout < 0 {
                signCodeButton.isEnabled = true
                signCodeButton.setTitle("获取验证码", for: UIControlState())
                signCodeButton.setTitleColor(UIColor.black, for: UIControlState())
                return
            }
            signCodeButton.isEnabled = false
            let nf = NumberFormatter()
            nf.numberStyle = NumberFormatter.Style.decimal
            nf.maximumFractionDigits = 0
            nf.string(from: (timeout - 1) as NSNumber)
            let timeoutShow = "(" + nf.string(from: timeout as NSNumber)! + ")"
            signCodeButton.setTitle("重新获取" + timeoutShow, for: UIControlState())
            signCodeButton.setTitleColor(UIColor.lightGray, for: UIControlState())
            gcd.async(.main, delay: 1) {
                self.timeout = self.timeout - 1
                self.signCodeButtonCountDown()
            }   
        }    
    }
    
    
}
