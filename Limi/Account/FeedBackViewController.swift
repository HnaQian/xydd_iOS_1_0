//
//  FeedBackViewController.swift
//  Limi
//
//  Created by 程巍巍 on 5/20/15.
//  Copyright (c) 2015 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit
import CoreData

import Foundation

class FeedBackViewController: BaseViewController,UITextViewDelegate,UITextFieldDelegate{
    
    let MAX_LIMIT_NUMS = 199
    var isStopPop : Bool = false
    
    var feedbackView: FeedbackView{
        return self.view as! FeedbackView
    }
    
    override func loadView() {
        super.loadView()
        self.view = FeedbackView()
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "直接向CEO吐槽"
        //关闭手势返回
        self.gesturBackEnable = false
//        self.edgesForExtendedLayout = UIRectEdge()
        self.navigationItem.rightBarButtonItem = CommonWidgets.NavigationRightBtn.newRightBtn("提交", target: self, action: #selector(FeedBackViewController.commit))
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UITextViewTextDidChange, object: nil, queue: OperationQueue.main) { (notification) in
            if !self.feedbackView.textView.placeHolderTextView.isHidden {
                self.feedbackView.textView.placeHolderTextView.isHidden = true
            }
            
            if let text = self.feedbackView.textView.text {
                if text.characters.count <= 4 {
                    self.feedbackView.subTitleLabel.text = "最少输入\(4 - text.characters.count)个字"
                }else if text.characters.count > 4 && text.characters.count <= 100 {
                    self.feedbackView.subTitleLabel.text = "还可以输入\(100 - text.characters.count)个字"
                }else {
                    self.feedbackView.textView.text = (text as NSString).substring(to: 100)
                }
                
            }
        }
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UITextViewTextDidBeginEditing, object: nil, queue: OperationQueue.main) { (notification) in
            if self.feedbackView.textView.text.characters.count == 0 {
                self.feedbackView.textView.placeHolderTextView.isHidden = false
            }else {
                self.feedbackView.textView.placeHolderTextView.isHidden = true
            }
            
        }
        
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.feedbackView.textView.becomeFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.feedbackView.textView.delegate = self
        self.isStopPop = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.feedbackView.textView.delegate = nil
        self.isStopPop = true
    }

    
    override func backItemAction() {
        if feedbackView.textView.isFirstResponder{
            feedbackView.textView.resignFirstResponder()
        }
        if feedbackView.textView.text.characters.count != 0
        {
            LCActionSheet(title: "确定放弃吐槽吗?", buttonTitles: ["放弃"], cancelTitle: "说两句", redButtonIndex: 0, clicked: { (buttonIndex) -> Void in
                
                if buttonIndex == 1
                {
                    let _ = self.navigationController?.popViewController(animated: true)
                }
            }).show()
        }
        else
        {
            let _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    func commit(){
        if self.feedbackView.textView.text.characters.count < 4{
            Utils.showError(context: self.view, errorStr: "反馈内容不可少于4个字")
            return
        }
        
        if self.feedbackView.textView.text.characters.count > 200{
            Utils.showError(context: self.view, errorStr: "反馈内容不可超过200个字")
            return
        }
        
        self.feedbackView.textView.resignFirstResponder()
        var params = [String: AnyObject]()
        params["content"] = self.feedbackView.textView.text as AnyObject?
        if UserInfoManager.didLogin == true{
            params["token"] = UserDefaults.standard.string(forKey: "UserToken") as AnyObject?
        }
        
        let requestUserInfo: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        let datasourceUserInfo : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(requestUserInfo)!
        if datasourceUserInfo.count > 0{
            
            let userInfo : User? = datasourceUserInfo[0] as? User
            if userInfo?.user_name != "" && userInfo?.user_name != nil{
            params["contact"] = userInfo?.user_name as AnyObject?
            }
        }
        let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.feedback, parameters: params,isShowErrorStatuMsg: true,isShowSuccessStatuMsg:true, isNeedHud: true,successHandler: {data, status, msg in
            
            if status == 200
            {
                let time: TimeInterval = 1.0
                let delay = DispatchTime.now() + Double(Int64(time * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                DispatchQueue.main.asyncAfter(deadline: delay) {
                    if !self.isStopPop{
                       let _ = self.navigationController?.popViewController(animated: true)
                    }
                }
            }
        })

    }
    
}

extension FeedBackViewController{
    class FeedbackView: BaseView {
        var contentView = UIScrollView()
        var titleLabel = UILabel()
        var textView = UITextView()
        var subTitleLabel = UILabel()
        
        override func defaultInit() {
            self.backgroundColor = UIColor(rgba: Constant.common_background_color)
            contentView.backgroundColor = UIColor.white
            let borderColor = UIColor(rgba: Constant.common_separatLine_color)
            
            // title
            titleLabel.text = "请输入你要吐槽的内容"
            titleLabel.font = Constant.CustomFont.Default(size: 15)
            titleLabel.textColor = UIColor.black
            
            // textview
            textView.font = Constant.CustomFont.Default(size: 15)
            textView.backgroundColor = UIColor.white
            textView.layer.borderWidth = 0.5
            textView.layer.borderColor = borderColor.cgColor
            textView.placeHolder = "请输入内容"
            
            //subtitle
            subTitleLabel.font = UIFont.systemFont(ofSize: Constant.common_F5_font)
            subTitleLabel.textAlignment = .right
            subTitleLabel.textColor = UIColor.lightGray
            subTitleLabel.text = "最少输入4个字"
            
            contentView.layer.borderWidth = 0.5
            contentView.layer.borderColor = UIColor(rgba: Constant.common_separatLine_color).cgColor
            
            for view in [titleLabel, textView,subTitleLabel] as [Any] {
                contentView.addSubview(view as! UIView)
            }
            self.addSubview(contentView)

        }
        
        
        override func layoutSubviews() {
            super.layoutSubviews()
            titleLabel.sizeToFit()
            titleLabel.frame = CGRect(x: 8, y: 12, width: titleLabel.frame.width, height: titleLabel.frame.height)
            textView.frame = CGRect(x: 8, y: titleLabel.frame.maxY + 12, width: self.frame.width - 16, height: 128)
            contentView.frame = CGRect(x: 0, y: 12, width: self.frame.width, height: textView.frame.maxY + 12)
            subTitleLabel.frame = CGRect(x: textView.frame.maxX - 150, y: textView.frame.maxY - 20, width: 140, height: 10)
        }
        

    }
    
}
