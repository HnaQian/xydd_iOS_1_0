//
//  DetailMessageListViewController.swift
//  Limi
//
//  Created by maohs on 16/7/11.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//
//消息中心  详情
import UIKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}

fileprivate func <= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l <= r
  default:
    return !(rhs < lhs)
  }
}


class DetailMessageListViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate {

    var type = 0
    var titleString = ""
    fileprivate var last_id = 0
    fileprivate let pageSize = 10
    
    var tableView:UITableView {
       return self.view as! UITableView
    }
    
    var msgList = [DetailMessageListModel]() {
        didSet {
            if msgList.count > 0 {
                let temp = msgList.last
                last_id = Int((temp?.id)!)!
            }
        }
    }
    
    
    override func loadView() {
         super.loadView()
        self.view = UITableView(frame: CGRect.zero, style: .plain)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.edgesForExtendedLayout = UIRectEdge()
        self.navigationItem.title = titleString
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor(rgba: "#F2F2F2")
        tableView.backgroundView = backgroundView
        tableView.register(DetaiMessagelListCell.self, forCellReuseIdentifier: DetaiMessagelListCell.identifier)
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        self.tableView.addHeaderRefresh { () -> Void in
           gcd.async(.default) {
                self.loadMessage(0)
            }
        }
        
        tableView.beginHeaderRefresh()
        
        self.tableView.addFooterRefresh { () -> Void in
            gcd.async(.default) {
                self.loadMessage((self.last_id))
            }
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadMessage(_ outId:Int) {
        let tempString = UserDefaults.standard.string(forKey: "UserToken")
        
        let params = ["token":tempString!,"type":type,"last_id":outId,"size":pageSize] as [String : Any]
        let _ = LMRequestManager.requestByDataAnalyses(context: self.view, URLString: Constant.JTAPI.message_typeList,parameters:(params as [String : AnyObject]),isShowErrorStatuMsg:true,successHandler: { (data, status, msg) in
            if self.tableView.isHeaderRefreshing {
                self.tableView.endHeaderRefresh()
            }
            
            if self.tableView.isFooterRefreshing {
                self.tableView.endFooterRefresh()
            }
            if status != 200 {return}
            if let tempArray = JSON(data as AnyObject)["data"].arrayObject {
                if outId == 0 {
                    self.msgList = [DetailMessageListModel]()
                }
                for index in 0 ..< tempArray.count {
                    let detailData = DetailMessageListModel(tempArray[index] as! [String:AnyObject])
                    self.msgList.append(detailData)
                }
                
                if tempArray.count < self.pageSize{
                    //加载全部
                    self.tableView.showTipe(UIScrollTipeMode.noMuchMore)
                    
                }
            }
            self.tableView.reloadData()
            
        })

    }
    
    
    //MARK: UITableView DataSource
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return msgList[(indexPath as NSIndexPath).row].cellHeight
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return msgList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell:DetaiMessagelListCell
        cell = tableView.dequeueReusableCell(withIdentifier: DetaiMessagelListCell.identifier, for: indexPath) as! DetaiMessagelListCell
        cell.titleString = titleString
        cell.reloadCellData(msgList[(indexPath as NSIndexPath).row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       let href = msgList[(indexPath as NSIndexPath).row].url
        if !href.isEmpty {
            EventDispatcher.dispatch(href, onNavigationController: self.navigationController)
            
        }
    }
    
}

class DetaiMessagelListCell: UITableViewCell {
    
    static let identifier = "DetaiMessagelListCell"
    var titleString = ""
    
    let groundView:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    let titleLabel:UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: Constant.common_F2_font)
        lbl.textColor = UIColor.black
        return lbl
    }()
    
    let contentLabel:UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: Constant.common_F5_font)
        lbl.textColor = UIColor.lightGray
        lbl.numberOfLines = 0
        return lbl
    }()
    
    let timeLabel:UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .right
        lbl.font = UIFont.systemFont(ofSize: Constant.common_F5_font)
        lbl.textColor = UIColor.lightGray
        return lbl
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        self.backgroundColor = UIColor.clear
        setUpUI()
    }
    
    fileprivate func setUpUI() {
        
        self.contentView.addSubview(groundView)
        for view in [titleLabel,contentLabel,timeLabel] {
            groundView.addSubview(view)
        }
        
        
    }
    
    func reloadCellData(_ data:DetailMessageListModel?) {
        if let content = data?.content {
            contentLabel.text = content
        }
        
        if let time = data?.add_time {
            timeLabel.text = time
        }
        
        if let title = data?.title {
            if title != "" {
                titleLabel.text = title
            }else {
                titleLabel.text = titleString
            }
        }else {
            titleLabel.text = titleString
        }
        
        refreshUI(data!.cellHeight)
    }
    
    
    func refreshUI(_ cellHeight:CGFloat){
    
        groundView.frame = CGRect(x: 0, y: 0, width: self.iwidth, height: cellHeight - 10)
        titleLabel.frame = CGRect(x: MARGIN_10, y: MARGIN_10, width: Constant.ScreenSize.SCREEN_WIDTH / 2, height: 20)
        contentLabel.frame = CGRect(x: MARGIN_10, y: titleLabel.ibottom, width: self.iwidth - 20, height: groundView.iheight - titleLabel.ibottom)
        timeLabel.frame = CGRect(x: titleLabel.iright + MARGIN_10, y: MARGIN_10, width: Constant.ScreenSize.SCREEN_WIDTH - titleLabel.iright - MARGIN_10 * 2, height: 20)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class DetailMessageListModel: BaseDataMode {
    /*
     id: "5608328",
     uid: "133489",
     type: "5",
     content_id: "132",
     content: "震惊！圣诞老人也是颜控",
     url: "http://wechat.giftyou.me/article_item?id=430&app=1",
     is_notice: "1",
     is_index: "2",
     is_read: "2",
     status: "1",
     add_time: "2016-03-01 15:20:01",
     title: "这个看脸的世界"
     */
    
    var id = NULL_STRING
    
    var uid = NULL_STRING
    
    var type = NULL_STRING
    
    var content_id = NULL_STRING
    
    var content = NULL_STRING
    
    var url = NULL_STRING
    
    var is_notice = NULL_STRING
    
    var is_index = NULL_STRING
    
    //Is_read 1未读 2已读
    var is_read = NULL_STRING
    
    var status = NULL_STRING
    
    var add_time = NULL_STRING {
        didSet {
            add_time = changeTime(add_time)
        }
    }
    
    var title = NULL_STRING
    
    var cellHeight:CGFloat{ return 45 + heightForCellWithString(content,width:Constant.ScreenSize.SCREEN_WIDTH - 20,fontsize:13)}
    
    func heightForCellWithString(_ content:String,width:CGFloat,fontsize:CGFloat) -> CGFloat{
        var sizeToFit : CGSize
        if content.isEmpty || content == ""{
            return 0
        }
        let attributedText = NSAttributedString(string: content, attributes: [NSFontAttributeName: Constant.CustomFont.Default(size: fontsize)])
        let rect : CGRect = attributedText.boundingRect(with: CGSize(width: width, height: CGFloat.greatestFiniteMagnitude), options: NSStringDrawingOptions.usesLineFragmentOrigin, context: nil)
        sizeToFit = CGSize(width: rect.size.width, height: rect.size.height);
        
        return sizeToFit.height;
    }
    
    func changeTime(_ time:String) ->String {
        //获取当天0点
        let calendar = Calendar.current
        
        let unitTag = NSCalendar.Unit.year.rawValue | NSCalendar.Unit.month.rawValue | NSCalendar.Unit.day.rawValue
        let currentDate = Date()
//        let zone = NSTimeZone.systemTimeZone()
//        let tempInterval0 : Int = zone.secondsFromGMTForDate(currentDate)
//        let localDate = currentDate.dateByAddingTimeInterval(NSTimeInterval(tempInterval0))
        let dateComponents = (calendar as NSCalendar).components(NSCalendar.Unit(rawValue: unitTag), from: currentDate)
        
        let year = dateComponents.year
        let month = dateComponents.month
        let day = dateComponents.day
        
        let nowTime = "\(year!)" + "-\(month!)" + "-\(day!)" + " 00:00:00"
        
        //服务器时间字符串转换成NSdate
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        //比较
        if let serverDate = formatter.date(from: time)  {
            let nowDate = formatter.date(from: nowTime)
            
            let tempArray = time.components(separatedBy: " ")
            let interval = nowDate?.timeIntervalSince(serverDate)
            if interval < 0 {
                //今天
                if tempArray.count > 0 {
                    return tempArray[1]
                }
            }else if interval >= 0 && interval <= 24 * 3600 {
                //昨天
                if tempArray.count > 0 {
                    return "昨天" + "\(tempArray[1])"
                }
            }else {
                return time
            }
            
        }
        return time
    }

    
}
