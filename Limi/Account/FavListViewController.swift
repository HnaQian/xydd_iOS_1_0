//
//  FavListViewController.swift
//  Limi
//
//  Created by 程巍巍 on 5/19/15.
//  Copyright (c) 2015 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

var tempSelf:FavListViewController?

protocol FavListViewDelegate:NSObjectProtocol {
   func FavListViewChangeIndex(_ index: Int)
}

class FavListViewController: BaseViewController,FavListViewDelegate
{
    fileprivate let pageSize = 10
    
    fileprivate var datatype:Int = 0{didSet{
        
        if datatype == 0{
            if self.listData[0].count == 0{
                favListView.tabView.tableView.beginHeaderRefresh()
            }
        }else{
            if self.listData[1].count == 0{
                favListView.colleView.collectionView.beginHeaderRefresh()
            }
        }
        
        }}
    
    var listData = [[JSON]](repeating: [JSON](), count: 2)
    
    var favListView: FavListView
    {
        return self.view as! FavListView
    }
    
    override func loadView()
    {
        tempSelf = self
        self.view = FavListView()
        (self.view as! FavListView).delegate = self
    }

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.navigationItem.title = "我喜欢的"
        
        self.view.backgroundColor = UIColor(rgba: Constant.common_background_color)
        
        favListView.tabView.tableView.addHeaderRefresh { () -> Void in
            DispatchQueue.global().async(execute: { [weak self]() -> Void in
                self?.loadListData(true, type: 2)
            })
        }
        
        favListView.tabView.tableView.addFooterRefresh { () -> Void in
            DispatchQueue.global().async(execute: { [weak self]() -> Void in
                self?.loadListData(false, type: 2)
            })
        }
        
        favListView.colleView.collectionView.addHeaderRefresh { () -> Void in
            DispatchQueue.global().async(execute: { [weak self]() -> Void in
                self?.loadListData(true, type: 1)
            })
        }
        
        favListView.colleView.collectionView.addFooterRefresh { () -> Void in
            DispatchQueue.global().async(execute: { [weak self]() -> Void in
                self?.loadListData(false, type: 1)
            })
        }
        
        if datatype == 0{
            favListView.tabView.tableView.beginHeaderRefresh()
        }else{
            favListView.colleView.collectionView.beginHeaderRefresh()
        }
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue:Constant.tactic_collectnumber), object: nil, queue: OperationQueue.main) {[weak self] (notification) in
            self?.favListView.tabView.tableView.beginHeaderRefresh()
        }
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue:GoodsDetailViewController.COLLECT_INFO), object: nil, queue: OperationQueue.main) { [weak self](notification) in
            self?.favListView.colleView.collectionView.beginHeaderRefresh()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        

    }
    
    func FavListViewChangeIndex(_ index: Int) {
        self.datatype = index
    }
    
    
    @objc func tapped(_ sender: UITapGestureRecognizer) {
        if let backView = (sender.view as? UITableView)?.backgroundView as? BackView {
            let location = sender.location(in: backView.actionButton)
            let rect = backView.actionButton.bounds
            if location.x >= 0 && location.x <= rect.width && location.y >= 0 && location.y <= rect.height {
                self.favListView.tableBackViewAction?()
            }
        }
    }
    
    
    
    // type 收藏类型 1商品 2文章
    func loadListData(_ refresh: Bool, type: Int)
    {
        var params: [String: AnyObject] = ["collect_type": type as AnyObject]
        
        if !refresh
        {
            if let lastId = listData[type == 2 ? 0 : 1].last?["Id"].int
            {
                params["last_id"] = lastId as AnyObject?
            }
        }
        
        params["size"] = pageSize as AnyObject?
        let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.collect_list, parameters: params, isNeedUserTokrn: true,isShowErrorStatuMsg: true,successHandler: {data, status, msg in

            print(data)
            
            if  self.favListView.tabView.tableView.isHeaderRefreshing == true{
                self.favListView.tabView.tableView.endHeaderRefresh()
            }
            
            if self.favListView.tabView.tableView.isFooterRefreshing == true{
                self.favListView.tabView.tableView.endFooterRefresh()
            }
            if self.favListView.colleView.collectionView.isHeaderRefreshing == true{
                self.favListView.colleView.collectionView.endHeaderRefresh()
            }
            
            if self.favListView.colleView.collectionView.isFooterRefreshing == true
            {
                self.favListView.colleView.collectionView.endFooterRefresh()
            }
            if status != 200
            {
                self.listData[type == 2 ? 0 : 1] = []
                return
            }

            if let list = JSON(data as AnyObject)["data"]["list"].array {
                if list.count != 0
                {
                    if refresh
                    {
                        self.listData[type == 2 ? 0 : 1] = list
                    }
                    else
                    {
                        for ll in list
                        {
                            self.listData[type == 2 ? 0 : 1].append(ll)
                        }
                    }
                    
                    //文章
                    if type == 2
                    {
                        self.favListView.tabView.articleList = self.listData[0]
                        
                        if list.count < self.pageSize
                        {
                            self.favListView.tabView.tableView.endFooterRefreshWithNoMoreData()
                        }
                    }
                    else
                    {
                        //商品
                        self.favListView.colleView.goodsList = self.listData[1]
                        if list.count < self.pageSize
                        {
                            
                            self.favListView.colleView.collectionView.endFooterRefreshWithNoMoreData()
                        }
                    }
                }
                else
                {
                    if refresh
                    {
                        self.listData[type == 2 ? 0 : 1] = []
                        //文章
                        if type == 2
                        {
                            self.favListView.tabView.articleList = []
                            
                            self.favListView.tabView.tableView.showTipe(UIScrollTipeMode.nullData).tipeContent(Constant.TipeWords.nullData_article)
                        }else
                        {
                            //商品
                            self.favListView.colleView.goodsList = []
                            
                            self.favListView.colleView.collectionView.showTipe(UIScrollTipeMode.nullData).tipeContent(Constant.TipeWords.nullData_goods)
                        }
                        
                    }else
                    {
                        //文章
                        if type == 2
                        {
                            self.favListView.tabView.tableView.endFooterRefreshWithNoMoreData()
                        }
                        else
                        {
                            //商品
                            self.favListView.colleView.collectionView.endFooterRefreshWithNoMoreData()
                        }
                    }
                }
            }
            
            
            },failureHandler:{Void in
                
                self.favListView.colleView.collectionView.closeAllRefresh()
                self.favListView.tabView.tableView.closeAllRefresh()
                
        })
    }
    
}



extension FavListViewController
{
    
    
    class FavListView: BaseView, LazyPageScrollViewDelegate
    {
        var isScrollAction = false
        var pageScrollView = LazyPageScrollView(verticalDistance: 0, tabItemType: TabItemType.customView)
        
        var tabView: CommonTableView!
        
        var colleView: CommonCollectionView!
        
        weak var delegate:FavListViewDelegate?
        
        let toolBarItemTitleFontSize:CGFloat = 15
        
        var tableBackViewAction: (()->())?
        
        override func defaultInit() {
            let colleBgView = BackView(title: "您还没有喜欢的礼品哦!",imageName: "self_giftlist_nogift")
            
            tabView = CommonTableView.newCommonTableView(type: ArticleFavType.cancelFav, userVC: tempSelf, stowBool: true)
            colleView = CommonCollectionView.newCommonCollectionView(backView: colleBgView, type: GoodsFavType.cancelFav, userVC: tempSelf, stowBool: true)
            
            // 设置collectionView的sectionInset距离top的 值
            (colleView.collectionView.collectionViewLayout as? UICollectionViewFlowLayout)!.sectionInset.top = 0
            
            self.backgroundColor = UIColor(rgba: Constant.common_background_color)
            self.addSubview(pageScrollView)
            pageScrollView.segment(["攻略", "礼品"], views: [tabView,colleView], frame: CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: Constant.ScreenSize.SCREEN_HEIGHT - 64))
            
            pageScrollView.delegate = self
            
        }
        
        func lazyPageScrollView(_ pageScrollView: LazyPageScrollView, changeIndex index: Int) {
            if index == 0
            {
                if tempSelf?.datatype != 0{
                    tempSelf?.datatype = 0
                }
            }else
            {
                if tempSelf?.datatype != 1{
                    tempSelf?.datatype = 1
                }
            }
            delegate?.FavListViewChangeIndex(index)
        }
        
        @objc func tapped(_ sender: UITapGestureRecognizer) {
            if let backView = (sender.view as? UITableView)?.backgroundView as? BackView {
                let location = sender.location(in: backView.actionButton)
                let rect = backView.actionButton.bounds
                if location.x >= 0 && location.x <= rect.width && location.y >= 0 && location.y <= rect.height {
                    tableBackViewAction?()
                }
            }
        }
    }
    
    
    fileprivate class BackView: UIView {
        var imageView = UIImageView()
        var titleLabel = UILabel()
        var actionButton = UIButton()
        init(title: String,imageName: String) {
            super.init(frame: CGRect.zero)
            
            titleLabel.text = title
            imageView.image = UIImage(named: imageName)
            actionButton.setTitle("去看看", for: UIControlState())
            
            titleLabel.font = UIFont.systemFont(ofSize: 13)
            actionButton.titleLabel?.font = UIFont.systemFont(ofSize: 15)
            
            titleLabel.textColor = UIColor.darkGray
            actionButton.setTitleColor(UIColor(rgba: "#E68283"), for: UIControlState())
            
            //titleLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "buttonClicked"))
            
            let content = UIView()
            self.addSubview(content)
            content.snp_makeConstraints { (make) -> Void in
                let _ = make.centerX.equalTo(self)
                let _ = make.centerY.equalTo(self).multipliedBy(0.7)
            }
            
            content.addSubview(imageView)
            content.addSubview(titleLabel)
            content.addSubview(actionButton)
            actionButton.isHidden = true
            
            imageView.snp_makeConstraints { (make) -> Void in
                let _ = make.height.equalTo(70)
                let _ = make.width.equalTo(70)
                let _ = make.top.equalTo(content)
                let _ = make.centerX.equalTo(content)
            }
            
            titleLabel.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(imageView.snp_bottom).offset(16)
                let _ = make.centerX.equalTo(content)
            }
            
            actionButton.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(titleLabel.snp_bottom).offset(32)
                let _ = make.centerX.equalTo(content)
                let _ = make.bottom.equalTo(content)
            }
        }
        
        required init(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    
    
}
