//
//  UserCenterHeaderView.swift
//  Limi
//
//  Created by maohs on 16/9/23.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

protocol UserCenterHeaderViewDelegate:NSObjectProtocol {
    func shouldVisitUserInfo()
    func shouldVisitOrderVc()
    func shouldVisitFavVc()
    func shouldVistCouponVc()
}

class UserCenterHeaderView: BaseUnitView,UserCenterCouponViewDelegate {

    weak var headerDelegate: UserCenterHeaderViewDelegate?
    
    fileprivate var gapHeight:CGFloat = 20 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
    
    fileprivate let bgImage:UIImageView = {
        let image = UIImageView()
        image.clipsToBounds  = true
        image.contentMode = UIViewContentMode.scaleAspectFill
        return image
    }()
    
    fileprivate let userImage:UIImageView = {
        let image = UIImageView()
        image.clipsToBounds  = true
        image.contentMode = UIViewContentMode.scaleAspectFill
        image.isUserInteractionEnabled = true
        return image
    }()
    
    fileprivate let userNameLabel:UILabel = {
       let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: Constant.common_F2_font)
        lbl.textColor = Constant.Theme.Color14
        lbl.textAlignment = .center
        lbl.isUserInteractionEnabled = true
        return lbl
    }()
    
    fileprivate let userRightTagImage:UIImageView = {
        let image = UIImageView()
        image.contentMode = UIViewContentMode.scaleAspectFit
        image.image = UIImage(named: "userCenter_right_icon")
        return image
    }()
    
    fileprivate let hintLabel:UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: 12)
        lbl.textColor = UIColor(rgba:Constant.common_C7_color)
        lbl.textAlignment = .center
        lbl.text = "登录立享新人红包"
        return lbl
    }()
    
    fileprivate let couponView:UserCenterCouponView = {
        let view = UserCenterCouponView()
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }
    
    override func setupUI() {
        let scale = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        self.backgroundColor = UIColor.white
        self.clipsToBounds = true
        for view in [bgImage,userImage,userNameLabel,userRightTagImage,hintLabel,couponView] {
            self.addSubview(view)
        }
        bgImage.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: 360 * scale + 64)
        userImage.frame = CGRect(x: 0, y: 60 * scale + 64, width: 136 * scale, height: 136 * scale)
        userImage.icenterX = bgImage.icenterX
        userNameLabel.frame = CGRect(x: 20 * scale, y: userImage.ibottom + 20 * scale, width: Constant.ScreenSizeV2.SCREEN_WIDTH - 40 * scale, height: 20)
        hintLabel.frame = CGRect(x: 20 * scale, y: userNameLabel.ibottom + 5 * scale, width: userNameLabel.iwidth, height: 15)
        userRightTagImage.frame = CGRect(x: bgImage.iwidth / 2, y: userImage.ibottom + 20 * scale, width: 20, height: 20)
        userRightTagImage.icenterY = (userNameLabel.icenterY + hintLabel.icenterY) / 2
        couponView.itop = bgImage.ibottom
        self.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: couponView.ibottom)
        
        couponView.delegate = self
        bgImage.image = UIImage(named: "userCenterHeaderBgImage")
        
        let userImageTapedGesture = UITapGestureRecognizer(target: self, action: #selector(UserCenterHeaderView.userImageTaped))
        userImage.addGestureRecognizer(userImageTapedGesture)
        
        let userNameLabelTapedGesture = UITapGestureRecognizer(target: self, action: #selector(UserCenterHeaderView.userNameLabelTaped))
        userNameLabel.addGestureRecognizer(userNameLabelTapedGesture)
        
        userImage.layer.cornerRadius = userImage.iwidth/2
        userImage.layer.masksToBounds = true
    }
    
    func reloadData(userInfo : User?) {
        if userInfo != nil {
            
            //头像
            if UserInfoManager.didLogin {
                if userInfo!.avatar != "" && userInfo!.avatar != nil {
                    let imageScale : String = userInfo!.avatar! as String + "?imageView2/0/w/" + "\(Int(userImage.iwidth*Constant.ScreenSize.SCALE_SCREEN))" + "/h/" + "\(Int(userImage.iheight*Constant.ScreenSize.SCALE_SCREEN))"
                    
                    self.userImage.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "accountIcon"))
                }else {
                    userImage.image = UIImage(named: "accountIcon")
                }
            }
            else
            {
                self.userImage.image = UIImage(named: "accountIcon")
            }
            
            //姓名
            var content: String?
            if UserInfoManager.didLogin {
                if userInfo!.nick_name != "" && userInfo!.nick_name != nil
                {
                    content = userInfo!.nick_name
                }
                else
                {
                    content = userInfo!.user_name
                }
                
                if content != nil {
                    self.userNameLabel.text = Utils.htmalTrusletToText(content)
                    self.hintLabel.text = ""
                    userRightTagImage.icenterY = (userNameLabel.icenterY + hintLabel.icenterY) / 2 - 10
                    userRightTagImage.ileft = bgImage.iwidth / 2
                }else {
                    self.userNameLabel.text = "请点击登录"
                    self.hintLabel.text = "登录立享新人红包"
                    userRightTagImage.icenterY = (userNameLabel.icenterY + hintLabel.icenterY) / 2
                    
                }
            }else {
                self.userNameLabel.text = "请点击登录"
                self.hintLabel.text = "登录立享新人红包"
                userRightTagImage.icenterY = (userNameLabel.icenterY + hintLabel.icenterY) / 2
            }
             self.bgImage.image = UIImage(named: "userCenterHeaderBgImage")
        }else {
            self.userImage.image = UIImage(named: "accountIcon")
            self.userNameLabel.text = "请点击登录"
            self.hintLabel.text = "登录立享新人红包"
            self.bgImage.image = nil
            userRightTagImage.icenterY = (userNameLabel.icenterY + hintLabel.icenterY) / 2
        }
        
        //右边箭头
        let option = NSStringDrawingOptions.usesLineFragmentOrigin
        let attributes = NSDictionary(object: self.userNameLabel.font, forKey: NSFontAttributeName as NSCopying)
        let size = CGSize(width: self.frame.size.width - 4 * Constant.ScreenSizeV2.MARGIN_30, height: 20)
        let stringRect = self.userNameLabel.text!.boundingRect(with: size, options: option, attributes: attributes as? [String : AnyObject], context: nil)
        let width = stringRect.size.width
        let attributes1 = NSDictionary(object: self.hintLabel.font, forKey: NSFontAttributeName as NSCopying)
        let stringRect1 = self.hintLabel.text!.boundingRect(with: size, options: option, attributes: attributes1 as? [String : AnyObject], context: nil)
        let width1 = stringRect1.size.width
        let temp = width > width1 ? width : width1
        userRightTagImage.ileft = Constant.ScreenSizeV2.SCREEN_WIDTH / 2 + temp / 2 + MARGIN_8
    
        
    }
    
    //礼物篮数量
    func refreshCouponUnreadData(data:[String:String]) {
        couponView.refreshUnreadData(data: data)
    }
    
    func userImageTaped() {
        headerDelegate?.shouldVisitUserInfo()
    }
    
    func userNameLabelTaped() {
        headerDelegate?.shouldVisitUserInfo()
    }
    
    //订单、我喜欢的、红包代理
    func orderViewClicked() {
        headerDelegate?.shouldVisitOrderVc()
    }
    
    func favViewClicked() {
        headerDelegate?.shouldVisitFavVc()
    }
    
    func couponViewClicked() {
        headerDelegate?.shouldVistCouponVc()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}

protocol UserCenterCouponViewDelegate:NSObjectProtocol {
    func orderViewClicked()
    func favViewClicked()
    func couponViewClicked()
}

class UserCenterCouponView:UIView {
    
    weak var delegate:UserCenterCouponViewDelegate?
    
    fileprivate let orderView:CustomView = {
        let view = CustomView(name:"我的订单",image:"userCenterOrderIcon")
        return view
    }()
    
    fileprivate let favView:CustomView = {
        let view = CustomView(name: "我喜欢的",image:"userCenterFav")
        return view
    }()
    
    fileprivate let couponView:CustomView = {
        let view = CustomView(name: "我的红包",image:"userCenterCouponIcon",isCoupon:true)
        return view
    }()
    
    fileprivate let seperateLineView:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(rgba: Constant.common_separatLine_color)
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    func setupUI() {
        self.addSubview(seperateLineView)
        self.addSubview(orderView)
        self.addSubview(favView)
        self.addSubview(couponView)
        
        orderView.itop = 1
        favView.itop = 1
        couponView.itop = 1
        favView.ileft = orderView.iright + 2
        couponView.ileft = favView.iright + 2
        
        seperateLineView.backgroundColor = UIColor(rgba: Constant.common_separatLine_color)
        seperateLineView.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: 1)
        self.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: orderView.iheight + 1)
        
        let orderTapGesture = UITapGestureRecognizer(target: self, action: #selector(UserCenterCouponView.orderViewTaped))
        orderView.addGestureRecognizer(orderTapGesture)
        
        let favTapGesture = UITapGestureRecognizer(target: self, action: #selector(UserCenterCouponView.favViewTaped))
        favView.addGestureRecognizer(favTapGesture)
        
        let couponTapGesture = UITapGestureRecognizer(target: self, action: #selector(UserCenterCouponView.couponViewTaped))
        couponView.addGestureRecognizer(couponTapGesture)
    }
    
    func orderViewTaped() {
        delegate?.orderViewClicked()
    }
    
    func favViewTaped() {
        delegate?.favViewClicked()
    }
    
    func couponViewTaped() {
        delegate?.couponViewClicked()
    }
    
    func refreshUnreadData(data:[String:String]) {
        couponView.refreshUnreadData(data: data)
        orderView.refreshUnreadData(data: data)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    final class CustomView:UIView {
        
        fileprivate let titleLabel:UILabel = {
            let lbl = UILabel()
            lbl.textColor = UIColor.white
            lbl.font = Constant.Theme.Font_11
            lbl.backgroundColor = UIColor(rgba: "#facd89")
            lbl.textAlignment = .center
            lbl.text = "兑换"
            lbl.isHidden = true
            return lbl
        }()
        
        fileprivate let titleImage:UIImageView = {
            let image = UIImageView()
            return image
        }()
        
        fileprivate let contentLabel:UILabel = {
            let lbl = UILabel()
            lbl.font = UIFont.systemFont(ofSize: Constant.common_F5_font)
            lbl.textColor = Constant.Theme.Color14
            lbl.textAlignment = .center
            return lbl
        }()
        
        fileprivate let verticalSeperateLine:UIView = {
            let view = UIView()
            view.backgroundColor = UIColor(rgba: Constant.common_separatLine_color)
            return view
        }()
        
        fileprivate var name = ""
        fileprivate var imageString = ""
        fileprivate var isCoupon = false
        init(name:String,image:String = "",isCoupon:Bool = false) {
            super.init(frame: CGRect.zero)
            self.name = name
            self.imageString = image
            self.isCoupon = isCoupon
            setupUI()
        }
        
        func setupUI() {
            let scale = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
            self.backgroundColor = UIColor.white
            self.isUserInteractionEnabled = true
            self.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH / 3 - 1, height: 140 * scale)
            
            self.addSubview(self.titleLabel)
            self.addSubview(self.titleImage)
            self.addSubview(self.contentLabel)
            self.addSubview(self.verticalSeperateLine)
            
            self.titleLabel.frame = CGRect(x: self.iwidth - 60 * scale, y: 0, width: 60 * scale, height: 30 * scale)
            self.titleImage.frame = CGRect(x: self.icenterX - 30 * scale, y:20 * scale, width: 60 * scale, height: 60 * scale)
            self.contentLabel.frame = CGRect(x: 20 * scale, y: self.titleImage.ibottom + 10 * scale, width: self.iwidth - 40 * scale, height: 15)
            self.verticalSeperateLine.frame = CGRect(x: self.iwidth - 1, y: 0, width: 1, height: self.iheight)
        
            self.titleImage.image = UIImage(named: self.imageString)
            self.contentLabel.text = self.name
        }
        
        func refreshUnreadData(data:[String:String]) {
            if isCoupon {
                //红包
                if UserInfoManager.didLogin {
                    self.titleLabel.isHidden = false
                }else {
                    self.titleLabel.isHidden = true
                }
            }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
    }
}
