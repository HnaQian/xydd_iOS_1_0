//
//  ContactServiceViewController.swift
//  Limi
//
//  Created by maohs on 16/9/28.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class ContactServiceViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate {

    fileprivate let tableView = UITableView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = "联系客服"
        self.view.addSubview(tableView)
        
        tableView.frame = self.view.bounds
        tableView.delegate = self
        tableView.dataSource = self
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        tableView.backgroundColor = UIColor.clear
        tableView.backgroundView = nil
        
        tableView.register(ContactServiceCell.self, forCellReuseIdentifier: ContactServiceCell.identifier)
        tableView.rowHeight = ContactServiceCell.cellHeight
        tableView.tableFooterView = UIView(frame: CGRect.zero)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: UITableView DataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell:ContactServiceCell = tableView.dequeueReusableCell(withIdentifier: ContactServiceCell.identifier) as! ContactServiceCell
        if indexPath.row == 0 {
            cell.nameLabel.text = "在线客服"
            cell.subNameLabel.text = "周一至周五 09:00-12:00 13:30-19:00"
        }else {
            cell.nameLabel.text = "电话客服"
            cell.subNameLabel.text = "点击直接拨打  021-68411255"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row == 0 {
            if UserInfoManager.didLogin{
                Statistics.count(Statistics.Mine.my_contact_online_click)
                let qyvc = QYSDK.shared().sessionViewController()
                qyvc?.navigationItem.leftBarButtonItem = qyCustomBackButton()
                qyvc?.sessionTitle = ""
                qyvc?.groupId = 74126
                QYSDK.shared().customActionConfig().linkClickBlock = {
                    [weak self](QYLinkClickBlock) -> Void in
                    let temp = QYLinkClickBlock! as String
                    EventDispatcher.dispatch(temp, onNavigationController: self?.navigationController)
                }
                self.navigationController?.pushViewController(qyvc!, animated: true)
            } else
            {
                self.jumpToLogin(Constant.InLoginType.consultType, tipe: nil)
            }
          
        }else {
             Statistics.count(Statistics.Mine.my_contact_tel_click)
            Utils.callServicer(self.view)
        }
    }
    
    func qyCustomBackButton() -> UIBarButtonItem
    {
        let backButton = UIButton(type: UIButtonType.custom)
        backButton.frame = CGRect(x: 0, y: 0, width: 36, height: 36)
        backButton.setBackgroundImage(UIImage(named: "common_back_icon"), for: UIControlState())
        backButton.addTarget(self, action: #selector(ContactServiceViewController.backButtonClicked), for: UIControlEvents.touchUpInside)
        backButton.clipsToBounds = true
        backButton.tintColor = UIColor.red
        
        let item = UIBarButtonItem(customView: backButton)
        
        return item
    }
    
    func backButtonClicked() {
        let _ = self.navigationController?.popViewController(animated: true)
    }

}

class ContactServiceCell: UITableViewCell {
    static let identifier = "ContactServiceCell"
    static let cellHeight:CGFloat = 130 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE + 8.0
    
    fileprivate let bgView = UIView()
    
    let nameLabel:UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        lbl.textColor = Constant.Theme.Color14
        return lbl
    }()
    
    let subNameLabel:UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: Constant.common_F5_font)
        lbl.textColor = UIColor(rgba: Constant.common_C7_color)
        return lbl
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = UIColor.clear
        self.accessoryView = UIImageView(image: UIImage(named: "cellInfo"))
        setupUI()
    }
    
    func setupUI() {
        self.contentView.addSubview(bgView)
        self.contentView.addSubview(nameLabel)
        self.contentView.addSubview(subNameLabel)
        let scale = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        bgView.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: 130 * scale)
        nameLabel.frame = CGRect(x: 40 * scale, y: 30 * scale, width: 200, height: 15)
        subNameLabel.frame = CGRect(x: 40 * scale, y: nameLabel.ibottom + 10 * scale, width: Constant.ScreenSizeV2.SCREEN_WIDTH - 80 * scale, height: 15)
        
        bgView.backgroundColor = UIColor.white
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

