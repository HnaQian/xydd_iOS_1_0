//
//  GeneralSettingViewController.swift
//  Limi
//
//  Created by maohs on 16/9/28.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class GeneralSettingViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {

    fileprivate let tableView = UITableView()
    fileprivate var cacheData : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = "设置"
        
        self.view.addSubview(tableView)
        
        tableView.frame = self.view.bounds
        tableView.delegate = self
        tableView.dataSource = self
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        tableView.backgroundColor = UIColor.clear
        tableView.backgroundView = nil
        
//        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.tableFooterView = UIView(frame: CGRect.zero)
      
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        gcd.async(.default) {
            self.cacheData = CacheDataManager.sharedInstance.calculateCacheData()
            gcd.async(.main, closure: { 
                self.tableView.reloadData()
            })
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 8.0
    }
    
    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        view.tintColor = UIColor(rgba: Constant.common_background_color)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        if cell == nil {

            cell = UITableViewCell(style: .value1, reuseIdentifier: "cell")
            cell?.textLabel?.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
            cell?.textLabel?.textColor = Constant.Theme.Color14
            
            cell?.detailTextLabel?.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
            cell?.detailTextLabel?.textColor = Constant.Theme.Color14
        }
        cell?.detailTextLabel?.text = ""
        cell?.accessoryView = UIImageView(image: UIImage(named: "cellInfo"))
        
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                cell?.textLabel?.text = "账号管理"
                let separatLine = UIView(frame:CGRect(x: 40 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE,y: cell!.frame.size.height - 0.5,width: cell!.frame.size.width,height: 0.5))
                separatLine.backgroundColor = UIColor(rgba: Constant.common_separatLine_color)
                cell?.addSubview(separatLine)
            }else {
                cell?.textLabel?.text = "收货地址管理"
            }
        }else if indexPath.section == 1 {
            switch indexPath.row {
            case 0:
                cell?.textLabel?.text = "给个好评"
                let separatLine = UIView(frame:CGRect(x: 40 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE,y: cell!.frame.size.height - 0.5,width: cell!.frame.size.width,height: 0.5))
                separatLine.backgroundColor = UIColor(rgba: Constant.common_separatLine_color)
                cell?.addSubview(separatLine)
            case 1:
                cell?.textLabel?.text = "直接向CEO吐槽"
            default:
                break
            }
        }else {
            switch indexPath.row {
            case 0:
                cell?.accessoryView = nil
                cell?.textLabel?.text = "消除缓存"
                cell?.detailTextLabel?.text = self.cacheData
                let separatLine = UIView(frame:CGRect(x: 40 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE,y: cell!.frame.size.height - 0.5,width: cell!.frame.size.width,height: 0.5))
                separatLine.backgroundColor = UIColor(rgba: Constant.common_separatLine_color)
                cell?.addSubview(separatLine)
            case 1:
                cell?.textLabel?.text = "关于心意点点"
            default:
                break
            }
        }
        
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                if UserInfoManager.didLogin {
                    Statistics.count(Statistics.Mine.my_settings_id_click)
                    let accountInfo = AccountInfoViewController()
                    self.navigationController?.pushViewController(accountInfo, animated: true)
                }else {
                    self.jumpToLogin(Constant.InLoginType.defaultType, tipe: nil)
                }
            }else {
                if UserInfoManager.didLogin
                {
                    Statistics.count(Statistics.Mine.my_settings_address_click)
                    let address = DeliveryAddressManageViewController()
                    self.navigationController?.pushViewController(address, animated: true)
                }
                else
                {
                    self.jumpToLogin(Constant.InLoginType.defaultType, tipe: nil)
                }
            }
        }else if indexPath.section == 1 {
            switch indexPath.row {
            case 0:
                Statistics.count(Statistics.Mine.my_settings_comment_click)
                let str = "itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=" + "988254401" + "&onlyLatestVersion=true&pageNumber=0&sortOrdering=1&type=Purple+Software"
                
                let url = URL(string: str)
                if UIApplication.shared.canOpenURL(url!)
                {
                    UIApplication.shared.openURL(url!)
                }
                else
                {
                    print("can not open")
                }
            case 1:
                Statistics.count(Statistics.Mine.my_settings_suggest_click)
                let feedBack = FeedBackViewController()
                self.navigationController?.pushViewController(feedBack, animated: true)
            default:
                break
            }
        }else {
            switch indexPath.row {
            case 0:
                gcd.async(.default, closure: {
                    let _ = CacheDataManager.sharedInstance.emptyCacheData()
                })
                
                Utils.showError(context: self.view, errorStr: "清除缓存成功!")
                self.cacheData = "0.00B"
                let indexArray = [indexPath]
                self.tableView.reloadRows(at: indexArray, with: .none)
            default:
                Statistics.count(Statistics.Mine.my_settings_about_click)
                let about = AboutViewController(nibName: "AboutViewController", bundle: nil)
                self.navigationController?.pushViewController(about, animated: true)

            }
        }
    }
    
}
