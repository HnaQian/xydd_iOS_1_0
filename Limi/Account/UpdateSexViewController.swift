//
//  UpdateSexViewController.swift
//  Limi
//
//  Created by 程巍巍 on 5/26/15.
//  Copyright (c) 2015 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit
import CoreData

class UpdateGenderCellViewController: BaseViewController {
    
    var genderSelectView: GenderSelectView {
        return self.view as! GenderSelectView
    }
    
    override func loadView() {
        super.loadView()
        self.view = GenderSelectView()
    }

    var gender : Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.edgesForExtendedLayout = UIRectEdge()
        self.navigationItem.title = "修改性别"
        
        let requestUserInfo: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        
        let datasourceUserInfo : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(requestUserInfo)!
        
        if datasourceUserInfo.count > 0{
            let userInfo = datasourceUserInfo[0] as? User
            if let gender : Int = Int((userInfo?.gender)!){
                self.gender = gender
            }
        }

        self.navigationItem.rightBarButtonItem = CommonWidgets.NavigationRightBtn.newRightBtn("确定", target: self, action: #selector(UpdateGenderCellViewController.updateUserSex))
        
        genderSelectView.maleButton.on = gender == 1
        genderSelectView.femaleButton.on = gender == 2
        let _ = genderSelectView.maleButton.handle(events: UIControlEvents.touchUpInside) { [weak self](sender, event) -> Void in
            self?.gender = 1
            self?.genderSelectView.maleButton.on = true
            self?.genderSelectView.femaleButton.on = false
        }
        let _ = genderSelectView.femaleButton.handle(events: UIControlEvents.touchUpInside) { [weak self](sender, event) -> Void in
            self?.gender = 2
            self?.genderSelectView.maleButton.on = false
            self?.genderSelectView.femaleButton.on = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func updateUserSex() {
        if gender < 0 {
            Utils.showError(context: self.view, errorStr: "请选择性别！")
            return
        }
        if UserInfoManager.didLogin == true{
        let gen = gender
       let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.user_gender, parameters: [ "gender": gender as AnyObject], isNeedUserTokrn: true,isShowErrorStatuMsg: true,isShowSuccessStatuMsg:true, isNeedHud: true, hudTitle: "正在提交",successHandler: {data, status, msg in
            
            if status == 200
            {
                let request: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
                let datasource : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(request)!
                if datasource.count > 0{
                    
                    let userInfo = datasource[0] as! User
                    userInfo.gender = Int64(gen)
                }
                else{
                    let entity = NSEntityDescription.entity(forEntityName: "User", in: CoreDataManager.shared.managedObjectContext)
                    request.entity = entity
                    do {
                        try! CoreDataManager.shared.managedObjectContext.fetch(request)
                        let user: User = NSEntityDescription.insertNewObject(forEntityName: "User", into: CoreDataManager.shared.managedObjectContext) as! User
                        user.gender = Int64(gen)
                    }
                }
                CoreDataManager.shared.save({Void in
                    let _ = self.navigationController?.popViewController(animated: true)
                    },failureHandler: {Void in
                        let _ = self.navigationController?.popViewController(animated: true)
                        
                })
            }

            
        })
    }
        else{
            self.saveGenderr(Int64(gender))
        }
    
    }
    
    func saveGenderr(_ gender : Int64){
        let request: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        let datasource : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(request)!
        if datasource.count > 0{
            
            let userInfo = datasource[0] as! User
            userInfo.gender = gender
        }
        else{
            
            let request: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
            let entity = NSEntityDescription.entity(forEntityName: "User", in: CoreDataManager.shared.managedObjectContext)
            request.entity = entity
            do {
                try! CoreDataManager.shared.managedObjectContext.fetch(request)
                let user: User = NSEntityDescription.insertNewObject(forEntityName: "User", into: CoreDataManager.shared.managedObjectContext) as! User
                user.gender = gender
            }
        }
        CoreDataManager.shared.save({Void in
            let _ = self.navigationController?.popViewController(animated: true)
            },failureHandler: {Void in
                let _ = self.navigationController?.popViewController(animated: true)
                
        })
    }

}

extension UpdateGenderCellViewController {
    
    class CustomButton: UIButton {
        var on: Bool! {
            didSet{
                self.setImage(UIImage(named: on! ? "choosed" : "noChoose"), for: UIControlState())
            }
        }
        override func layoutSubviews() {
            super.layoutSubviews()
            self.titleLabel!.sizeToFit()
            let width = self.titleLabel!.frame.width
            let height = self.titleLabel!.frame.height
            self.imageView!.frame = CGRect(x: 0, y: 0, width: height, height: height)

            self.imageView?.center = CGPoint(x: height/2, y: self.frame.height/2)
            self.titleLabel?.center = CGPoint(x: self.frame.width - width/2 + 3, y: self.frame.height/2)
        }
    }
    
    class GenderSelectView: BaseView {
        var contentView = UIView()
        
        var maleButton = CustomButton()
        var femaleButton = CustomButton()
        var seperatorLine = UIView()
        var commitButton = UIButton()
        
        override func defaultInit() {
            
            self.backgroundColor = UIColor(rgba: Constant.common_background_color)
            
            contentView.backgroundColor = UIColor.white
            contentView.layer.borderWidth = 0.5
            contentView.layer.borderColor = UIColor(rgba: Constant.common_separatLine_color).cgColor
            contentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(GenderSelectView.contentTapped(_:))))
            
            let image : UIImage = UIImage(named: "noChoose")!
            let imageon : UIImage = UIImage(named: "choosed")!

            let font  = UIFont.systemFont(ofSize: 15)
            
            maleButton.titleLabel?.font = font
            maleButton.setTitle("男", for: UIControlState())
            maleButton.setTitleColor(UIColor.black, for: UIControlState())
            
            maleButton.setImage(image, for: UIControlState())
            maleButton.setImage(imageon, for: UIControlState.highlighted)

            femaleButton.titleLabel?.font = font
            femaleButton.setTitle("女", for: UIControlState())
            femaleButton.setTitleColor(UIColor.black, for: UIControlState())
            
            
            femaleButton.setImage(image, for: UIControlState())
            femaleButton.setImage(imageon, for: UIControlState.highlighted)
            
            seperatorLine.backgroundColor = UIColor(rgba: Constant.common_separatLine_color)
            
            commitButton.titleLabel?.font = font
            commitButton.setTitle("确定", for: UIControlState())
            commitButton.setTitleColor(UIColor.white, for: UIControlState())
            
            commitButton.setBackgroundImage(Utils.imageWithColor(UIColor(rgba: "#FD6769")), for: UIControlState())
            
            self.addSubview(contentView)
            
            contentView.addSubview(maleButton)
            contentView.addSubview(seperatorLine)
            contentView.addSubview(femaleButton)

           contentView.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(self).offset(-1)
                let _ = make.right.equalTo(self).offset(-1)
                let _ = make.top.equalTo(self).offset(8)
                let _ = make.height.equalTo(88)
            }

            maleButton.snp_makeConstraints { (make) -> Void in
                let _ = make.centerY.equalTo(contentView).multipliedBy(0.5).offset(-0.5)
                let _ = make.left.equalTo(contentView).offset(10)
            }

            seperatorLine.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(contentView).offset(10)
                let _ = make.right.equalTo(contentView).offset(10)
                let _ = make.centerY.equalTo(contentView)
                let _ = make.height.equalTo(0.5)
            }

            femaleButton.snp_makeConstraints { (make) -> Void in
                let _ = make.centerY.equalTo(contentView).multipliedBy(1.5).offset(0.5)
                let _ = make.left.equalTo(contentView).offset(10)
            }

        }
    
        
        @objc func contentTapped(_ sender: UITapGestureRecognizer) {
            let location = sender.location(in: contentView)
            if location.y > contentView.bounds.height / 2 {
                femaleButton.sendActions(for: .touchUpInside)
            }else{
                maleButton.sendActions(for: .touchUpInside)
            }
        }
    }
}
