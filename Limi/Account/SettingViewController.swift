//
//  SettingViewController.swift
//  Limi
//
//  Created by 程巍巍 on 5/20/15.
//  Copyright (c) 2015 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit
import CoreData

class SettingViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate {
    var tableView = UITableView()
    var phone : String = "021-68411255"
    let evaluateStr = "itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=" + "988254401" + "&onlyLatestVersion=true&pageNumber=0&sortOrdering=1&type=Purple+Software"
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.delegate = self
        self.tableView.dataSource = self

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tableView.delegate = nil
        self.tableView.dataSource = nil
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "设置"
        self.view.addSubview(self.tableView)
        self.tableView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.view)
            let _ = make.bottom.equalTo(self.view)
            let _ = make.left.equalTo(self.view)
            let _ = make.right.equalTo(self.view)
        }
        
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        self.tableView.separatorColor = UIColor(rgba: Constant.common_separatLine_color)
        self.tableView.showsHorizontalScrollIndicator = false
        self.tableView.showsVerticalScrollIndicator = false
        
//        let request: NSFetchRequest = NSFetchRequest(entityName: "SettingInfo")
//        
//        let datasource : Array<AnyObject> = CoreDataManager.shared.executeFetchRequest(request)!
//        
//        if datasource.count > 0{
//            
//            let settingInfo = datasource[0] as! SettingInfo
//            phone = settingInfo.service_phone!
//            
//        }
        
        phone = SystemInfoRequestManager.shareInstance().service_number
        
        }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        var cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "cell")
            cell!.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
            cell?.textLabel?.font = Constant.CustomFont.Default(size: 15)
            cell?.detailTextLabel?.font = Constant.CustomFont.Default(size: 12)
            cell?.detailTextLabel?.textColor = UIColor.black
        }
        switch (indexPath as NSIndexPath).row
        {
        case 0:
            cell?.textLabel?.text = "点个赞"
        case 1:
            cell?.textLabel?.text = "吐个槽"
        case 2:
            cell?.textLabel?.text = "联系客服"
            cell?.detailTextLabel?.text = self.phone
        case 3:
            cell?.textLabel?.text = "关于心意点点"
        default:
            break
        }
        
        return cell!
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch (indexPath as NSIndexPath).row
        {
        case 0:
            let url = URL(string: evaluateStr)
            if UIApplication.shared.canOpenURL(url!)
            {
                UIApplication.shared.openURL(url!)
            }
        case 1:
            let feedBack = FeedBackViewController()
            feedBack.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(feedBack, animated: true)
        case 2:
            self.callPhone()
        case 3:
            let about = AboutViewController(nibName: "AboutViewController", bundle: nil)
            about.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(about, animated: true)
        default:
            break
        }
        
    }
    
    //  电话联系客服
    func callPhone()
    {
        let phoneURL:URL = URL(string: "tel:" + phone)!
        let requestURL = Foundation.URLRequest(url: phoneURL)
        let webview = UIWebView(frame: CGRect.zero)
        webview.loadRequest(requestURL)
        self.view.addSubview(webview)
    }


}

