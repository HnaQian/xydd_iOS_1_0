//
//  CacheData.swift
//  Limi
//
//  Created by maohs on 16/6/30.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class CacheDataManager: NSObject {

    static let sharedInstance = CacheDataManager()
    fileprivate override init() {}
    
    func emptyCacheData() -> Bool {
        let paths:Array = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)
        let cachePath = paths[0]
        var isSuccess = true
        
        var contentArray:[String] = []
        do{
            contentArray = try FileManager.default.contentsOfDirectory(atPath: cachePath)}catch {}
            let enumerator:NSEnumerator = (contentArray as NSArray).objectEnumerator()
            while let filename = enumerator.nextObject() {
                if filename as! String == "Snapshots" {
                    continue
                }
                do {
                    try FileManager.default.removeItem(atPath: cachePath.stringByAppendingPathComponent(filename as! String))}catch{
                        isSuccess = false
                }

            }
        
        return isSuccess
    }
    
    func calculateCacheData() -> String {
        let fileManager = FileManager.default
        let paths:Array = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)
        let cachePath = paths[0]
        
        var fileSize:Float = 0
        if let temp = fileManager.subpaths(atPath: cachePath) {
            for index in temp {
                do {
                    if let attributes:NSDictionary = try fileManager.attributesOfItem(atPath: cachePath.stringByAppendingPathComponent(index)) as NSDictionary?{
                        fileSize += attributes["NSFileSize"] as! Float
                    }
                } catch {
                    
                }
            }
        }
       
        var resultSize:String = ""
        if fileSize < 1024 {
            resultSize = String(format: "%.2f",fileSize) + "B"
        }else if fileSize < 1024 * 1024 {
            resultSize =  String(format:"%.2f",fileSize / 1024) + "KB"
        }else {
            resultSize = String(format:"%.2f",fileSize / (1024 * 1024)) + "MB"
        }
        return resultSize
    }
    
}
