//
//  UpdatePasswdViewController.swift
//  Limi
//
//  Created by 程巍巍 on 5/20/15.
//  Copyright (c) 2015 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


class UpdatePasswdViewController: BaseViewController,UITextFieldDelegate
{
    var oldPWCell = InputCell(title: "旧密码", placeholder: "请输入旧密码")
    
    var newPWCell = InputCell(title: "新密码", placeholder: "请输入新密码")
    
    var confirmCell = InputCell(title: "确认新密码", placeholder: "请再次输入新密码")
    
    var tableView: UITableView {
        return self.view as! UITableView
    }
    override func loadView() {
        super.loadView()
        self.view = UITableView(frame: CGRect.zero, style: UITableViewStyle.grouped)
    }
    
    var cells: [[UITableViewCell]] = []

    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        tableView.delegate = self
        tableView.dataSource = self
        oldPWCell.textField.delegate = self
        newPWCell.textField.delegate = self
        confirmCell.textField.delegate = self
        

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tableView.delegate = nil
        tableView.dataSource = nil
        oldPWCell.textField.delegate = nil
        newPWCell.textField.delegate = nil
        confirmCell.textField.delegate = nil
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        oldPWCell.textField.becomeFirstResponder()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "修改密码"
//        self.edgesForExtendedLayout = UIRectEdge()
        

        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        
        self.cells = [[oldPWCell, newPWCell, confirmCell]]
        
        let doneButton = UIButton(type: .custom)
        doneButton.frame = CGRect(x: 0, y: 0, width: 80, height: 36)
        doneButton.titleLabel?.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        doneButton.setTitleColor(UIColor(rgba: Constant.common_C1_color), for: UIControlState())
        doneButton.setTitle("确认修改", for: UIControlState())
        doneButton.addTarget(self, action: #selector(UpdatePasswdViewController.updatePassword), for: UIControlEvents.touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: doneButton)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        if range.location >= 15{
            return false
        }
        
        let validTimeCharacterSet = CharacterSet(charactersIn: Constant.ALPHANUM)
        let invalidTimeCharacterSet = validTimeCharacterSet.inverted
        let array:NSArray = string.components(separatedBy: invalidTimeCharacterSet) as NSArray
        let filtered:NSString = array.componentsJoined(by: "") as NSString
        
        if string != filtered as String{
            Utils.showError(context: self.view, errorStr: "密码只限于大小写字母与数字")
            return false
        }
        else{
            return true
        }
        
    }
    
    @objc func updatePassword() {
        if oldPWCell.textField.text!.isEmpty {
            Utils.showError(context: self.view, errorStr: "请输入旧密码")
            return
        }
        else if oldPWCell.textField.text?.characters.count < 6{
            Utils.showError(context: self.view, errorStr: "密码应为6-15位字母、数字、符号组合，请重新设定")
            return
        }
        if newPWCell.textField.text!.isEmpty {
            Utils.showError(context: self.view, errorStr: "请输入新密码")
            return
        }
        else if newPWCell.textField.text?.characters.count < 6{
            Utils.showError(context: self.view, errorStr: "密码应为6-15位字母、数字、符号组合，请重新设定")
            return
        }
        else if confirmCell.textField.text!.isEmpty {
            Utils.showError(context: self.view, errorStr: "请输入确认新密码")
            return
        }
        else if confirmCell.textField.text?.characters.count < 6{
            Utils.showError(context: self.view, errorStr: "密码应为6-15位字母、数字、符号组合，请重新设定")
            return
        }
        
      if newPWCell.textField.text != confirmCell.textField.text {
        Utils.showError(context: self.view, errorStr: "两次填写的新密码不一致,请重新输入")
            return
        }
        oldPWCell.textField.resignFirstResponder()
        newPWCell.textField.resignFirstResponder()
        confirmCell.textField.resignFirstResponder()
        let old_password : AnyObject = self.oldPWCell.textField.text as AnyObject? ?? "" as AnyObject
        let password : AnyObject = self.confirmCell.textField.text! as AnyObject
        let re_password : AnyObject = self.confirmCell.textField.text! as AnyObject

       let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.login_resetPassword, parameters: ["old_password":old_password, "password": password, "re_password": re_password], isNeedUserTokrn: true, isShowErrorStatuMsg: true,isShowSuccessStatuMsg: true, isNeedHud: true,successHandler: {data, status, msg in
        
            if status == 200
            {
               let _ = self.navigationController?.popViewController(animated: true)
            }
        })
    }
}

extension UpdatePasswdViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cells[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = cells[(indexPath as NSIndexPath).section][(indexPath as NSIndexPath).row] as! InputCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        if (indexPath as NSIndexPath).row == 0 && (indexPath as NSIndexPath).section == 0
        {
            cell.separatorLine1.isHidden = false
        }
        if (indexPath as NSIndexPath).row == 2 && (indexPath as NSIndexPath).section == 0
        {
            cell.separatorLine3.isHidden = false
        }
        return cells[(indexPath as NSIndexPath).section][(indexPath as NSIndexPath).row]
    }
    
    func numberOfSections(in tableView: UITableView) -> Int{
        return cells.count
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 8.0
    }
}

extension UpdatePasswdViewController {
    class InputCell: UITableViewCell {
        var titleLabel = UILabel()
        var textField = UITextField()
        var separatorLine1 = UIView()
        var separatorLine2 = UIView()
        var separatorLine3 = UIView()
        
        init(title: String, placeholder: String){
            super.init(style: UITableViewCellStyle.default, reuseIdentifier: nil)
            
            separatorLine1.isHidden = true
            separatorLine3.isHidden = true
            
            titleLabel.font = Constant.CustomFont.Default(size: 15)
            titleLabel.textColor = UIColor.black
            titleLabel.text = title
            
            textField.clearButtonMode = UITextFieldViewMode.whileEditing
            
            textField.font = Constant.CustomFont.Default(size: 13)
            textField.placeholder = placeholder
            textField.isSecureTextEntry = true
            
            separatorLine1.backgroundColor = UIColor(rgba: Constant.common_separatLine_color)
            separatorLine2.backgroundColor = UIColor(rgba: Constant.common_separatLine_color)
            separatorLine3.backgroundColor = UIColor(rgba: Constant.common_separatLine_color)
            
            self.contentView.addSubview(titleLabel)
            self.contentView.addSubview(textField)
            self.contentView.addSubview(separatorLine1)
            self.contentView.addSubview(separatorLine2)
            self.contentView.addSubview(separatorLine3)

        }
        
       required init(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }

        override func layoutSubviews() {
            super.layoutSubviews()
            titleLabel.sizeToFit()
            titleLabel.center = CGPoint(x: 10 + titleLabel.frame.width/2, y: self.frame.height/2)
            
            textField.frame = CGRect(x: titleLabel.frame.maxX + 8, y: 4, width: self.frame.width - 28 - titleLabel.frame.maxX, height: self.frame.height - 8)
            separatorLine1.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: 0.5)
            separatorLine2.frame = CGRect(x: 10, y: self.frame.height - 0.5, width: self.frame.width - 20, height: 0.5)
            separatorLine3.frame = CGRect(x: 0, y: self.frame.height - 0.5, width: self.frame.width, height: 0.5)
        }
        
    }
}
