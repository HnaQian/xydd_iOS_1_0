//
//  GiftChoiceUnitView.swift
//  Limi
//
//  Created by guo chen on 15/12/28.
//  Copyright © 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//选礼单元视图



import UIKit

class GiftChoiceUnitView: UIView
{
    private var headerView = BBannerView(frame: CGRectMake(0,0,Constant.ScreenSize.SCREEN_WIDTH,Constant.ScreenSize.SCREEN_WIDTH * 0.21))
    
    private var footerView = UIView()
    
    private var isExistGoodsTitle = false
    
    var scrollView: UIScrollView!
    
    private var giftChoiceUnitData:JSON?//总数据
    
    private var topicListData = [GiftChoiceUnitModel]()//topic数组
    
    private var adListData = [JSON]()//广告数据
    
    private var topicDisplayByImage_count = 4//以图片形式显示的topic数量
    
    private weak var giftChoiceViewController:GiftChooseViewController?
    
    private var orderIndex = 110//顺序位置
    
    class func newGiftChoiceUnitView(orderIndex:Int,giftChoiceUnitData:JSON?,giftChoiceViewController:GiftChooseViewController)-> GiftChoiceUnitView
    {
        let giftChoiceUnitView = GiftChoiceUnitView()
        
        giftChoiceUnitView.orderIndex = orderIndex
        
        giftChoiceUnitView.giftChoiceUnitData = giftChoiceUnitData
        
        giftChoiceUnitView.giftChoiceViewController = giftChoiceViewController
        
        giftChoiceUnitView.setupUI()
        
        return giftChoiceUnitView
    }
    
    
    
    private func setupUI()//视图
    {
        self.frame = CGRectMake(0, 0, Constant.ScreenSize.SCREEN_WIDTH, Constant.ScreenSize.SCREEN_HEIGHT - 20 - 36 - 44 - 49)

        scrollView = UIScrollView(frame: self.bounds)
        
        scrollView.backgroundColor = UIColor.clearColor()

        self.addSubview(scrollView)

        self.scrollView.showsHorizontalScrollIndicator = false
        
        self.scrollView.showsVerticalScrollIndicator = false

        
        self.headerView.delegate = self
        
        self.headerView.dataSource = self
        
        self.prepareData()
    }
    
    
    
    func prepareData()
    {
        self.topicListData = [GiftChoiceUnitModel]()
        
        self.adListData = [JSON]()
        
        if let data = self.giftChoiceUnitData
        {
            if let ary = data["children"].array
            {
                if ary.count > 0
                {
                    for json in ary{
                    
                        self.topicListData.append(GiftChoiceUnitModel(dataDic: json))
                    }
                }
            }
            
            if let ary = data["ad"].array
            {
                if ary.count > 0
                {
                    self.adListData = ary
                }
            }
        }
        
        self.headerView.reloadData()
        
        self.createTagView()
        self.createTopicView()
        
    }
    
    
    func createTopicView()
    {
        if self.adListData.count > 0
        {
            scrollView.addSubview(self.headerView)
        }
        
        var lastCell:GiftChoiceUnitViewCellV1!
        
        var tempCell0:GiftChoiceUnitViewCellV1!
        
        var tempCell1:GiftChoiceUnitViewCellV1!
        
        var count = 0
        if topicListData.count > 0{
        count = topicListData.count > 3 ? 3 : topicListData.count - 1
        for i in 0...count
        {
            let cell = GiftChoiceUnitViewCellV1(data: self.topicListData[i],selectedHandle:{data in
                
            self.pushDetailVC(data)
            })
            
            scrollView.addSubview(cell)
            
            switch i
            {
                
            case 0:
                tempCell0 = cell
                
                lastCell = cell
                
                cell.ix = 0

                cell.iy = self.adListData.count > 0 ? headerView.ibottom + 2 : 2
                
            case 1:
                tempCell1 = cell
                
                cell.ix = tempCell0.iright + 2
                
                cell.iy = tempCell0.iy
                
            case 2:
                
                cell.iy = tempCell0.ibottom + 2
                
                cell.ix = tempCell0.ix
                
            case 3:
            
                 cell.iy = tempCell1.ibottom + 2
                 
                 cell.ix = tempCell1.ix
            default:break
            }
            
            
            if lastCell != nil
            {
                if lastCell.ibottom < cell.ibottom
                {
                    lastCell = cell
                }
            }
            
        }
    
        if topicListData.count < 5
        {
            scrollView.contentSize = CGSizeMake(scrollView.iwidth, lastCell.ibottom)
        }else
        {
        
        self.footerView.iy = lastCell.ibottom
        
        scrollView.addSubview(self.footerView)
        
            scrollView.contentSize = CGSizeMake(scrollView.iwidth, footerView.ibottom)
        }
        }
    }
    
    //MARK:创建footer标签栏
    private func createTagView()
    {
        if topicListData.count < 5{return}

        let count = topicListData.count - topicDisplayByImage_count
        
        for view in footerView.subviews
        {
            view.removeFromSuperview()
        }
        
        var rowsCountAry = [Int]()
        
        let rest:Int = count%2
        
        let rows:Int = count%2 == 0 ? count/2 : Int(count/2) + 1

        for i in 0 ..< rows
        {
            if i == rows - 1 && rest != 0
            {
                rowsCountAry.append(rest)
            }
            else
            {
                rowsCountAry.append(2)
            }
        }
        
        let gap:CGFloat = 2
        
        let btn_width:CGFloat = (Constant.ScreenSize.SCREEN_WIDTH - 2)/2
        
        let btn_height:CGFloat = 76 * btn_width/206
        
        for row in 0...(rowsCountAry.count - 1)
        {
            for index in 0...(rowsCountAry[row] - 1)
            {
                let curIndex = row * 2 + index
                
                let data = self.topicListData[curIndex + topicDisplayByImage_count]
                
                let tagBtn = TagButton(frame: CGRectMake((gap + btn_width) * CGFloat(index), gap + (gap + btn_height) * CGFloat(row), btn_width, btn_height), data: data)
                
                tagBtn.addTarget(self, action: #selector(GiftChoiceUnitView.tagButtonAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
                
                tagBtn.tag = curIndex + topicDisplayByImage_count
                
                footerView.addSubview(tagBtn)
            }
        }
        
        footerView.frame = CGRectMake(0, 0, Constant.ScreenSize.SCREEN_WIDTH, CGFloat(rows) * (gap + btn_height))
    }
    
    
    
    func tagButtonAction(tagBtn:TagButton)
    {
        self.pushDetailVC(topicListData[tagBtn.tag])
    }
    
    
    
    //MARK:跳转到专题详情界面
    func pushDetailVC(data:GiftChoiceUnitModel)
    {
//        if let VC = self.giftChoiceViewController
//        {
//            MobClick.event("giftChoice_click", attributes: ["mode_title": data.title])
//            print(data)
//            let giftListVC = GiftChoiceListViewController.newGiftChoiceListViewController(data)
//            
//            giftListVC.hidesBottomBarWhenPushed = true
//            
//            VC.navigationController?.pushViewController(giftListVC, animated: true)
//        }
    }
}



extension GiftChoiceUnitView
{
    class TagButton:UIButton
    {
        var data:GiftChoiceUnitModel!
        
        init(frame: CGRect,data:GiftChoiceUnitModel)
        {
            super.init(frame: frame)
            
            self.data = data
            
            self.setupUI()
        }
        
        
        
        private func setupUI()
        {
            self.setTitle(data.title, forState: UIControlState.Normal)
            
            if Constant.DeviceType.IS_IPHONE_4_OR_LESS == true || Constant.DeviceType.IS_IPHONE_5 == true
            {
                self.titleLabel?.font = UIFont.systemFontOfSize(Constant.common_F4_font)
            }else
            {
                self.titleLabel?.font = UIFont.systemFontOfSize(Constant.common_F2_font)
            }
            
            self.setTitleColor(UIColor(rgba: data.font_color), forState: UIControlState.Normal)
            
            self.backgroundColor = UIColor(rgba: data.bg_color)
        }
        
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
}



//代理 -- delegate

extension GiftChoiceUnitView:BBannerViewDataSource,BBannerViewDelegate
{
    //MARK:广告栏代理函数
    func didSelectItem(index: Int)
    {
        if let VC =  self.giftChoiceViewController
        {
            if let href = self.adListData[index]["attributes"]["href"].string
            {
                EventDispatcher.dispatch(href, onNavigationController: VC.navigationController)
            }
        }
    }
    
    
    func viewForItem(bannerView: BBannerView, index: Int) -> UIView
    {
        return self.newBannerImage(self.adListData[index])
    }
    
    
    func numberOfItems() -> Int
    {
        return self.adListData.count
    }
    
    
    //MARK:创建banner图片
    func newBannerImage(data:JSON)->UIImageView
    {
        let image = UIImageView(frame: headerView.bounds)
        
        if let imgStr = data["attributes"]["src"].string
        {
            let imageScale : String = imgStr + "?imageView2/0/w/" + "\(Int(headerView.bounds.size.width*Constant.ScreenSize.SCALE_SCREEN))" + "/h/" + "\(Int(headerView.bounds.size.height*Constant.ScreenSize.SCALE_SCREEN))"
            
            image.af_setImageWithURL(NSURL(string: imageScale)!, placeholderImage: UIImage(named: "accountIcon"))
        }
        
        return image
    }
    
}

