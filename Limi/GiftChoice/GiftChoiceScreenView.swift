//
//  GiftChoiceScreenView.swift
//  Limi
//
//  Created by guo chen on 15/12/30.
//  Copyright © 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//选礼筛选工具视图

import UIKit

class GiftChoiceScreenView: UIView,FFXRangeStepViewDelegate {

    fileprivate var moneyDues:[String]?
    
    fileprivate var sexP:String?
    
    fileprivate let priceScreenView = FFXRangeSlider(frame: CGRect(x: 10,y: 200,width: Constant.ScreenSize.SCREEN_WIDTH - 20,height: 52))//滑块
    
    fileprivate let sexScreenView = UIView(frame: CGRect(x: 0,y: 0,width: Constant.ScreenSize.SCREEN_WIDTH,height: 52))//性别筛选
    
    fileprivate let sexScreenContentView = UIView(frame: CGRect(x: 0,y: 0,width: Constant.ScreenSize.SCREEN_WIDTH,height: 97))
    
    fileprivate let priceScreenContentView = UIView(frame: CGRect(x: 0,y: 0,width: Constant.ScreenSize.SCREEN_WIDTH,height: 97))
    
    
    fileprivate let cancleBtn = UIButton(frame: CGRect(x: 10,y: 0,width: Constant.ScreenSize.SCREEN_WIDTH/2 - 15,height: 38))//取消按钮
    fileprivate let confirmeBtn = UIButton(frame: CGRect(x: Constant.ScreenSize.SCREEN_WIDTH/2 - 5 + 10,y: 0,width: Constant.ScreenSize.SCREEN_WIDTH/2 - 15,height: 38))//确定按钮
    
    fileprivate var sexBtns = [UIButton]()
    
    fileprivate let contentView = UIView(frame: CGRect(x: 0,y: 0,width: Constant.ScreenSize.SCREEN_WIDTH,height: 97))
    
    var sex:Int?
    
    var moneyDuesIndex:IndexSet!
    
    fileprivate var didChooseOverHandle:(()->Void)!
    
    var didDisplayHandle:((_ isShowing:Bool)->Void)?
    
    var isShowing = false
    
    
    class func newScreenView(_ frame:CGRect,moneyDues:[String],sexP:String?,didChooseOver:@escaping (()->Void))-> GiftChoiceScreenView
    {
        let screenV = GiftChoiceScreenView(frame: frame)
        
        screenV.didChooseOverHandle = didChooseOver
        
        screenV.moneyDues = moneyDues
        
        screenV.sexP = sexP
        
        screenV.setupUI()
        
        return screenV
    }
    
    
    
    fileprivate func setupUI()
    {
        priceScreenView.stepView.delegate = self
        
        priceScreenView.stepView.color = UIColor(rgba: Constant.common_C8_color)
        
        priceScreenView.stepView.activeColor = UIColor(rgba: Constant.common_C6_color)
        
        if let moneys  = moneyDues
        {
            priceScreenView.steps = moneys as NSArray
        }
        
        priceScreenView.handleTintColor = UIColor(rgba: "#F9868E")//两个球的初始颜色

        priceScreenView.handleBorderWidth = 2//两个球表边宽度
        
        priceScreenView.handleBorderColor = UIColor(rgba: "#FFC3C3")//两个球描边
        
        priceScreenView.trackTintColor = UIColor(rgba: Constant.common_C10_color)//两个球开区间颜色
        
        priceScreenView.selectedTrackTintColor = UIColor(rgba: Constant.common_C1_color)//两个球闭区间颜色

        
        
        let sexTitle = CustomLabel(font: Constant.common_F5_font, textColorHex: Constant.common_C7_color, textAlignment: NSTextAlignment.center)//性别
        
        let priceTitle = CustomLabel(font: Constant.common_F5_font, textColorHex: Constant.common_C7_color, textAlignment: NSTextAlignment.center)//价格
        
        priceTitle.text = "拖动滑块选择礼物价格区间"
        sexTitle.text = "请选择收礼人的性别"
        
        sexTitle.isUserInteractionEnabled = true
        priceTitle.isUserInteractionEnabled = true
        sexScreenView.isUserInteractionEnabled = true
        
        sexTitle.frame = CGRect(x: 0,y: 0,width: sexScreenContentView.iwidth,height: sexScreenContentView.iheight - sexScreenView.iheight)
        
        sexScreenView.iy = sexTitle.ibottom
        
        priceTitle.frame = CGRect(x: 0,y: 0,width: priceScreenContentView.iwidth,height: priceScreenContentView.iheight - priceScreenView.iheight)
        
        priceScreenView.iy = priceTitle.ibottom
        
        
        //创建性别按钮
        
        let titles = ["全部","男","女"]
        
        let btn_width:CGFloat = 52
        
        let distance_left:CGFloat = (Constant.ScreenSize.SCREEN_WIDTH - btn_width * 3)/4
        
        let bettwen_gap:CGFloat = distance_left
        
        for i in 0...2
        {
            let btn = UIButton(frame: CGRect(x: distance_left + CGFloat(i) * (btn_width + bettwen_gap),y: 0,width: btn_width,height: btn_width))
            
            sexBtns.append(btn)
            
            btn.layer.borderWidth = 1
            
            btn.layer.cornerRadius = btn.iwidth/2
            
            if i == 0
            {
                btn.setTitleColor(UIColor(rgba: Constant.common_C1_color),for: UIControlState())
                
                btn.layer.borderColor = UIColor(rgba: Constant.common_C1_color).cgColor
            }else
            {
                btn.setTitleColor(UIColor(rgba: Constant.common_C6_color),for: UIControlState())
                
                btn.layer.borderColor = UIColor(rgba: Constant.common_C7_color).cgColor
            }
            
            btn.tag = 100 + i
            btn.setTitle(titles[i], for: UIControlState())
            
            btn.titleLabel?.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
            
            btn.addTarget(self, action: #selector(GiftChoiceScreenView.sexBtnAction(_:)), for: UIControlEvents.touchUpInside)
            
            sexScreenView.addSubview(btn)
        }
        
        cancleBtn.setTitle("重置", for: UIControlState())
        cancleBtn.setTitleColor(UIColor(rgba: Constant.common_C6_color), for: UIControlState())
        cancleBtn.titleLabel?.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        
        cancleBtn.layer.cornerRadius = 3
        cancleBtn.layer.borderWidth = 1
        cancleBtn.layer.borderColor = UIColor(rgba: Constant.common_C7_color).cgColor
        
        
        confirmeBtn.setTitle("确定", for: UIControlState())
        confirmeBtn.setTitleColor(UIColor(rgba: Constant.common_C1_color), for: UIControlState())
        confirmeBtn.titleLabel?.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        
        confirmeBtn.layer.cornerRadius = 3
        confirmeBtn.layer.borderWidth = 1
        confirmeBtn.layer.borderColor = UIColor(rgba: Constant.common_C1_color).cgColor
        
        cancleBtn.addTarget(self, action: #selector(GiftChoiceScreenView.chooseBtnAction(_:)), for: UIControlEvents.touchUpInside)
        confirmeBtn.addTarget(self, action: #selector(GiftChoiceScreenView.chooseBtnAction(_:)), for: UIControlEvents.touchUpInside)
        
        
        sexScreenContentView.addSubview(sexTitle)
        sexScreenContentView.addSubview(sexScreenView)
        
        priceScreenContentView.addSubview(priceTitle)
        priceScreenContentView.addSubview(priceScreenView)
        
        if sexP == "她"
        {
            sex = 2
        }else if sexP == "他"
        {
            sex = 1
        }else
        {
            sexP = nil
        }
        
        if sexP == nil
        {
            contentView.addSubview(sexScreenContentView)
            
            priceScreenContentView.iy = sexScreenContentView.ibottom
        }

        cancleBtn.iy = priceScreenContentView.ibottom + 30
        confirmeBtn.iy = cancleBtn.iy
        
        contentView.iheight = confirmeBtn.ibottom + 10
        
        self.contentView.iy = -self.contentView.iheight
        
        contentView.backgroundColor = UIColor.white
        
        contentView.isUserInteractionEnabled = true
        
        contentView.clipsToBounds = true
        
        for view in [priceScreenContentView,cancleBtn,confirmeBtn]
        {
            contentView.addSubview(view)
        }
        
        self.addSubview(contentView)
        
        self.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4)
        
        self.clipsToBounds = true
        
        self.alpha = 0
    }
    
    
    //MARK:选择价格
    func didSelectedIndexSet(_ activeIndexes: IndexSet!)
    {
        
        self.moneyDuesIndex = activeIndexes
    }
    
    
    //MARK:选择性别
    func sexBtnAction(_ btn:UIButton)
    {
        if self.sex == btn.tag - 100{return}
        
        for mbtn in sexBtns
        {
            if mbtn == btn
            {
                mbtn.setTitleColor(UIColor(rgba: Constant.common_C1_color),for: UIControlState())
                
                mbtn.layer.borderColor = UIColor(rgba: Constant.common_C1_color).cgColor
            }else
            {
                mbtn.setTitleColor(UIColor(rgba: Constant.common_C6_color),for: UIControlState())
                
                mbtn.layer.borderColor = UIColor(rgba: Constant.common_C7_color).cgColor
            }
        }
        
        self.sex = btn.tag - 100
    }
    
    func resetScreenView() {
        chooseBtnAction(cancleBtn)
    }
    
    func chooseBtnAction(_ btn:UIButton)
    {
        if btn == confirmeBtn
        {
            if self.didChooseOverHandle != nil
            {
                self.didChooseOverHandle()
            }
            
            self.dismiss()
            
        }else
        {
            self.priceScreenView.resetHandle()
            
            self.moneyDuesIndex = IndexSet(integersIn: NSMakeRange(0,(moneyDues?.count)!).toRange()!)

            for index in 0...sexBtns.count - 1
            {
                let btn = sexBtns[index]
                
                if index == 0
                {
                    btn.setTitleColor(UIColor(rgba: Constant.common_C1_color),for: UIControlState())
                    
                    btn.layer.borderColor = UIColor(rgba: Constant.common_C1_color).cgColor
                }else
                {
                    btn.setTitleColor(UIColor(rgba: Constant.common_C6_color),for: UIControlState())
                    
                    btn.layer.borderColor = UIColor(rgba: Constant.common_C7_color).cgColor
                }
            }
            
            self.sex = 0
        }
    }
    
    
    
    ///显示
    func show()
    {
        isShowing = true
        if self.didDisplayHandle != nil
        {
            self.didDisplayHandle!(true)
        }
        self.alpha = 1
        UIView.animate(withDuration: 0.25, animations: { () -> Void in
            self.contentView.iy = 0
            self.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4)
            }, completion: { (compl) -> Void in

        }) 
    }
    
    
    
    ///隐藏
    func dismiss()
    {
        isShowing = false
        
        if self.didDisplayHandle != nil
        {
            self.didDisplayHandle!(false)
        }
        
        UIView.animate(withDuration: 0.25, animations: { () -> Void in
            
            self.contentView.iy = -self.contentView.iheight
            
            self.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
            
            }, completion: { (compl) -> Void in
                
                self.alpha = 0
        }) 
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        self.dismiss()
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        var touch:UITouch?
        for tempTouch in touches {
            touch = tempTouch
        }
        let point = touch!.location(in: self)
        if point.y > contentView.ibottom {
            self.dismiss()
        }
    }
}
