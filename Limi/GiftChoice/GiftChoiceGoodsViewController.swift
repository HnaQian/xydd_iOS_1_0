//
//  GiftChoiceGoodsViewController.swift
//  Limi
//
//  Created by 千云锋 on 16/8/4.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit


enum GiftChoiceGoodsUnitViewActionType: NSInteger {
    case object = 0
    case style
    case occasion
    case budget
}

let GiftChoiceGoodsActiontype = ["Object", "Style", "Occasion", "Budget"]


class GiftChoiceGoodsViewController: BaseViewController, BaseGiftChoiceGoodsUnitViewDelegate, CommonCollectionViewDelegate {

    var dataSource:[JSON] = []
    fileprivate var goods_listView: CommonCollectionView!
    
//    private let natureView = NatureResultView()
    fileprivate let bombBoxView = BombBoxView()
    fileprivate var isObjectShow = true
    fileprivate var isStyleShow = true
    fileprivate var isOccasionShow = true
    fileprivate var isBudgetShow = true
    
    fileprivate let headerButtonView:HeaderButtonView = HeaderButtonView()
    
    fileprivate var giftChoiceGoodsPeople:[GiftChoicePeopleModel] = []
    fileprivate var giftChoiceGoodsSupplement = GiftChoiceSupplementModel()
    fileprivate var giftChoiceGoodsDic = [String:String]()
    fileprivate var _number = 0
    fileprivate var number:Int{return _number}
    
    fileprivate var isRefresh: Bool = true
    
    fileprivate var dataPages = 1
    fileprivate var pageSize = 10
    
    class func giftChoiceGoods(_ dataSourcePeople:[GiftChoicePeopleModel], dataSourceSupplement:GiftChoiceSupplementModel, dataOption:[String:String], number:Int) -> GiftChoiceGoodsViewController{
        let giftGoods = GiftChoiceGoodsViewController()
        
        giftGoods.giftChoiceGoodsDic = dataOption
        giftGoods.giftChoiceGoodsDic["min"] = "0"
        giftGoods.giftChoiceGoodsDic["max"] = "99990000"
        giftGoods.giftChoiceGoodsPeople = dataSourcePeople
        giftGoods.giftChoiceGoodsSupplement = dataSourceSupplement
        giftGoods._number = number
        
        return giftGoods
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isRefresh {
            self.goods_listView.collectionView.beginHeaderRefresh()
            isRefresh = false
        }
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.gesturBackEnable = false
//        self.view.backgroundColor = UIColor.whiteColor()
        self.title = "选礼结果"
        
        self.headerButtonView.reloadData(self.giftChoiceGoodsDic, data:self.giftChoiceGoodsSupplement.chang_he)
        self.view.addSubview(headerButtonView)
        headerButtonView.delegate = self
        bombBoxView.delegate = self
        bombBoxView.natureResultView.delegate = self
        setupUI()
        // Do any additional setup after loading the view.
        goods_listView.collectionView.backgroundColor = UIColor.clear
        goods_listView.collectionView.addHeaderRefresh { 
            [weak self]() -> Void in
            
            self?.fetchDataSource(true)
        }
        
        goods_listView.collectionView.addFooterRefresh { 
            [weak self]() -> Void in
            
            self?.fetchDataSource(false)
        }
    }
    //创建UI
    func setupUI() {
        goods_listView = CommonCollectionView.newCommonCollectionView(backView: nil, type: GoodsFavType.fav, userVC: self, stowBool: false)
        goods_listView.frame = CGRect(x: 0, y: headerButtonView.frame.size.height, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: Constant.ScreenSizeV2.SCREEN_HEIGHT - headerButtonView.frame.size.height - 64)
        goods_listView.delegate  = self
        self.view.addSubview(goods_listView)
    }
    
    
    //请求数据
    func fetchDataSource(_ isRefresh:Bool) {
        var params = [String:AnyObject]()
        
        let ext_info = NSMutableArray()
        if (self.giftChoiceGoodsDic["occasion"] != nil) {
            var dic = NSMutableDictionary()
            dic = ["type_code":"chang_he", "value":self.giftChoiceGoodsDic["occasion"]!]
            ext_info.add(dic)
        }
        if (self.giftChoiceGoodsDic["ages"] != nil) {
            var dic = NSMutableDictionary()
            dic = ["type_code":"age", "value":self.giftChoiceGoodsDic["ages"]!]
            ext_info.add(dic)
        }
        if (self.giftChoiceGoodsDic["sex"] != nil) {
            var dic = NSMutableDictionary()
            dic = ["type_code":"sex", "value":self.giftChoiceGoodsDic["sex"]!]
            ext_info.add(dic)
        }
        if (self.giftChoiceGoodsDic["xinzuo"] != nil) {
            var dic = NSMutableDictionary()
            dic = ["type_code":"xin_zhuo", "value":self.giftChoiceGoodsDic["xinzuo"]!]
            ext_info.add(dic)
        }
        
        let jsonData = try! JSONSerialization.data(withJSONObject: ext_info, options: JSONSerialization.WritingOptions.prettyPrinted)
        let jsonString: String = String(data: jsonData, encoding: String.Encoding.utf8)!
        params["ext_info"] = jsonString as AnyObject?
        
        params["shen_fen"] = self.giftChoiceGoodsDic["name"]! as AnyObject?
        if let gexin = self.giftChoiceGoodsDic["style"] {
            params["ge_xin"] = gexin as AnyObject?
        }
        
        if let minMoney = self.giftChoiceGoodsDic["min"] {
            params["price_start"] = Int(minMoney) as AnyObject?
        }
        if let maxMoney = self.giftChoiceGoodsDic["max"] {
            params["price_end"] = Int(maxMoney) as AnyObject?
        }
        
        params["page"] =  isRefresh ? 1 as AnyObject : self.dataPages as AnyObject?
        params["size"] = self.pageSize as AnyObject?
        print(params)
        let _ = LMRequestManager.requestByDataAnalyses(context: self.view, URLString: Constant.JTAPI.choosegift_filterGoodsByReceiverInfo, parameters: params, isNeedUserTokrn: true, isShowErrorStatuMsg: true,successHandler: { (data, status, msg) in
            
            self.goods_listView.collectionView.endHeaderRefresh()
            self.goods_listView.collectionView.endFooterRefresh()
            if status != 200 {return}
            print(data)
            if let dataSource = JSON(data as AnyObject)["data"].dictionaryObject {
                if let list = JSON(dataSource as AnyObject)["list"].arrayObject {
                    print(list)
                    if isRefresh {
                     self.dataSource = [JSON]()
                    }
                    if list.count > 0 {
                        for item in list {
                            self.dataSource.append(JSON(item as AnyObject))
                        }
                        
                        if list.count < self.pageSize {
                            self.goods_listView.collectionView.endFooterRefreshWithNoMoreData()
                        }
                    }
                    
                }else{
                    self.goods_listView.collectionView.showTipe(UIScrollTipeMode.nullData)

                }
                
                self.goods_listView.goodsList = self.dataSource
                
                self.dataPages += 1
                
            }
        })
        
    }

    override func backItemAction() {
        let _ = self.navigationController?.popToRootViewController(animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    fileprivate var nameArr:[String] = []
    fileprivate var modelArr:[GiftGexinModel] = []
    
    fileprivate var occasionArr:[String] = []
    
    
    //--------------------------BaseGiftChoiceGoodsUnitViewDelegate--------------------------------
    
    
    //MARK:-BaseGiftChoiceGoodsUnitViewDelegate
    func baseGiftChoiceBombBoxViewUnitViewTouchdAction(_ actionType: String, content: Int?) {
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "updateButton1"), object: nil)
        
        switch actionType {
            
        case "Object":
            if content != nil {
                self.giftChoiceGoodsDic["name"] = nameArr[content!]
                for model in self.giftChoiceGoodsPeople {
                    nameArr.append(model.name)
                    if self.giftChoiceGoodsDic["name"] == model.name {
                        self.modelArr = model.ge_xin
                        break
                    }
                    
                }
                if modelArr.count > 0 {
                    self.giftChoiceGoodsDic["style"] = modelArr[0].name
                }
                
                print(self.giftChoiceGoodsDic)
                self.headerButtonView.reloadData(self.giftChoiceGoodsDic, data:self.giftChoiceGoodsSupplement.chang_he)
                self.bombBoxView.removeFromSuperview()
                isObjectShow = true
                self.goods_listView.collectionView.beginHeaderRefresh()
            }else{
                self.bombBoxView.removeFromSuperview()
                isObjectShow = true
            }
             Statistics.count(Statistics.GiftChoice.selectgift_resulttab1_click, andAttributes: ["object":String(describing: self.giftChoiceGoodsDic["name"])])
            
            break
        case "Style":
            if content != nil {
                self.giftChoiceGoodsDic["style"] = modelArr[content!].name
                self.headerButtonView.reloadData(self.giftChoiceGoodsDic, data:self.giftChoiceGoodsSupplement.chang_he)
                self.bombBoxView.removeFromSuperview()
                isStyleShow = true
                self.goods_listView.collectionView.beginHeaderRefresh()
            }else{
                self.bombBoxView.removeFromSuperview()
                isStyleShow = true
            }
            Statistics.count(Statistics.GiftChoice.selectgift_resulttab2_click, andAttributes: ["Style":String(describing: self.giftChoiceGoodsDic["name"])])
            break
        case "Occasion":
            if content != nil {
                self.giftChoiceGoodsDic["occasion"] = occasionArr[content!]
                self.headerButtonView.reloadData(self.giftChoiceGoodsDic, data:self.giftChoiceGoodsSupplement.chang_he)
                self.bombBoxView.removeFromSuperview()
                isOccasionShow = true
                self.goods_listView.collectionView.beginHeaderRefresh()
            }else{
                self.giftChoiceGoodsDic.removeValue(forKey: "occasion")
                self.headerButtonView.reloadData(self.giftChoiceGoodsDic, data:self.giftChoiceGoodsSupplement.chang_he)
                self.bombBoxView.removeFromSuperview()
                self.goods_listView.collectionView.beginHeaderRefresh()
                isOccasionShow = true
            }
            Statistics.count(Statistics.GiftChoice.selectgift_resulttab3_click, andAttributes: ["Occasion":String(describing: self.giftChoiceGoodsDic["name"])])
            break
        case "Budget":
            
            
            break
        default:
            break
        }
    
    }
    
    func baseGiftChoiceGoodsUnitViewTouchdAction(_ actionType: String, content: UIButton) {

        NotificationCenter.default.post(name: Notification.Name(rawValue: "updateButton1"), object: nil)
        
        nameArr.removeAll()
        for model in self.giftChoiceGoodsPeople {
            nameArr.append(model.name)
            if self.giftChoiceGoodsDic["name"] == model.name {
                modelArr = model.ge_xin
            }
        }
        var styleArr:[String] = []
        for model in modelArr {
            styleArr.append(model.name)
        }
        
        switch actionType {
        case "Object":
            print("Object")
            bombBoxView.natureResultView.reloadDataAndStyle(GiftChoiceGoodsUnitViewActionType.object, dataDic:self.giftChoiceGoodsDic, data:nameArr)
            if isObjectShow && isStyleShow && isOccasionShow && isBudgetShow {
                self.view.addSubview(bombBoxView)
                content.isSelected = true
            }else{
                if !isObjectShow {
                    bombBoxView.removeFromSuperview()
                    content.isSelected = false
                    
                }else{
                    content.isSelected = true
                    isStyleShow = true
                    isOccasionShow = true
                    isBudgetShow = true
                }
            }
            isObjectShow = !isObjectShow
            
            break
        case "Style":
            print("Style")
            bombBoxView.natureResultView.reloadDataAndStyle(GiftChoiceGoodsUnitViewActionType.style, dataDic:self.giftChoiceGoodsDic, data:styleArr)
            if isObjectShow && isStyleShow && isOccasionShow && isBudgetShow {
                self.view.addSubview(bombBoxView)
                content.isSelected = true
            }else{
                if !isStyleShow {
                    bombBoxView.removeFromSuperview()
                    content.isSelected = false
                }else{
                    content.isSelected = true
                    isObjectShow = true
                    isOccasionShow = true
                    isBudgetShow = true
                }
            }
            isStyleShow = !isStyleShow
            break
        case "Occasion":
            print("Occasion")

            let modelOccasionArr:[GiftChoiceSubModel] = self.giftChoiceGoodsSupplement.chang_he
            occasionArr.removeAll()
            for model in modelOccasionArr {
                occasionArr.append(model.name)
            }
            bombBoxView.natureResultView.reloadDataAndStyle(GiftChoiceGoodsUnitViewActionType.occasion, dataDic:self.giftChoiceGoodsDic, data:occasionArr)
            if isObjectShow && isStyleShow && isOccasionShow && isBudgetShow {
                self.view.addSubview(bombBoxView)
                content.isSelected = true
            }else{
                if !isOccasionShow {
                    bombBoxView.removeFromSuperview()
                    content.isSelected = false
                }else{
                    content.isSelected = true
                    isObjectShow = true
                    isStyleShow = true
                    isBudgetShow = true
                }
            }
            isOccasionShow = !isOccasionShow
            break
        case "Budget":
            print("Budget")
            bombBoxView.natureResultView.reloadDataAndStyle(GiftChoiceGoodsUnitViewActionType.budget, dataDic: ["":""], data: ["1"])
            if isObjectShow && isStyleShow && isOccasionShow && isBudgetShow {
                self.view.addSubview(bombBoxView)
                content.isSelected = true
                
            }else{
                if !isBudgetShow {
                    bombBoxView.removeFromSuperview()
                    content.isSelected = false
                }else{
                    content.isSelected = true
                    isObjectShow = true
                    isStyleShow = true
                    isOccasionShow = true
                }
            }
            isBudgetShow = !isBudgetShow
            break
        default:
            break
        }
    }
    
    func baseGiftChoiceMoneyUnitViewTouchdAction(_ content: [Int]?) {
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "updateButton1"), object: nil)
        
        if content != nil {
            self.giftChoiceGoodsDic["min"] = String(content![0])
            self.giftChoiceGoodsDic["max"] = String(content![1])
            bombBoxView.removeFromSuperview()
            self.goods_listView.collectionView.beginHeaderRefresh()
        }else{
            self.giftChoiceGoodsDic["min"] = "0"
            self.giftChoiceGoodsDic["max"] = "99000000"
            bombBoxView.removeFromSuperview()
//            self.goods_listView.collectionView.beginHeaderRefresh()
        }
        isBudgetShow = true
        isObjectShow = true
        isOccasionShow = true
        isStyleShow = true
        print(self.giftChoiceGoodsDic)
        let priceString = self.giftChoiceGoodsDic["min"]! + "--" + self.giftChoiceGoodsDic["max"]!
        Statistics.count(Statistics.GiftChoice.selectgift_resulttab3_click, andAttributes: ["Budget":priceString])
    }
    
    func commonCollectionViewDidSelected(_ data: Int) {
        
        
        
    }
}
