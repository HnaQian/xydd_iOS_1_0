//
//  GiftChoiceListViewController.swift
//  Limi
//
//  Created by guo chen on 15/12/29.
//  Copyright © 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//选礼模块点击进入页面：礼物 + 商品

import UIKit

class GiftChoiceListViewController: BaseViewController,LazyPageScrollViewDelegate
{
    fileprivate let scrollView = UIScrollView()
    
    fileprivate var goods_listView: CommonCollectionView!
    
    fileprivate var article_listView: CommonTableView!
    
    fileprivate var goods_data = [JSON]()
    
    fileprivate var article_data = [JSON]()
    
    fileprivate var goods_dataPage = 1
    
    fileprivate var article_dataPage = 1
    
    fileprivate var goods_PageSize = 20//商品每页加载数量
    
    fileprivate var article_PageSize = 10//文章每页加载数量
    
    //TotalFound
    
    fileprivate var currentIndex = 110
    
    fileprivate let scrollView_height:CGFloat = Constant.ScreenSize.SCREEN_HEIGHT - 20 - 36 - 49
    
    fileprivate var pageScrollView = LazyPageScrollView(verticalDistance: 3, tabItemType: TabItemType.customView)
    
    fileprivate var topicInfo:GiftChooseChildModel!
    
    fileprivate let moneyDuesString = ["0","200","500","1000","2000","2000+"]//金额显示
    
    fileprivate let moneyDues = [0,200,500,1000,2000,99990000]//金额区域
    
    fileprivate var giftChoiceScreenView:GiftChoiceScreenView!
    
    fileprivate var goods_words = ""
    
    fileprivate var article_words = ""
    
    fileprivate var words = ""
    
    class func newGiftChoiceListViewController(_ topicInfo:GiftChooseChildModel)-> GiftChoiceListViewController
    {
        let giftChoiceListViewController = GiftChoiceListViewController()
        
        giftChoiceListViewController.topicInfo =  topicInfo
        
        return giftChoiceListViewController
    }
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.setupUI()
    }
    
    
    
    //MARK:界面布局
    fileprivate func setupUI()
    {
        
        goods_listView = CommonCollectionView.newCommonCollectionView(backView: nil, type: GoodsFavType.fav, userVC: self, stowBool: false)
        
        article_listView = CommonTableView.newCommonTableView(type: ArticleFavType.fav, userVC: self, stowBool: false)
        
        goods_listView.delegate  = self
        article_listView.delegate  = self
        
        article_listView.tableView.scrollsToTop = false
        
       self.view.addSubview(pageScrollView)
       var frame = self.view.bounds
       frame.size.height = frame.size.height - 64
       pageScrollView.segment(["礼物","攻略"], views: [goods_listView,article_listView], frame: frame)
       pageScrollView.delegate = self
        
        self.addRightNavItemOnView()
        
        giftChoiceScreenView = GiftChoiceScreenView.newScreenView(frame, moneyDues: moneyDuesString, sexP: topicInfo.attributes!.title,didChooseOver: { [weak self]() -> Void in
            
            if let minMoney = self?.moneyDues[(self?.giftChoiceScreenView.moneyDuesIndex.first)!],
                let maxMoney = self?.moneyDues[(self?.giftChoiceScreenView.moneyDuesIndex.first)!]
            {
                if let sex = self?.giftChoiceScreenView.sex{
                    Statistics.count(Statistics.GiftChoice.selectgift_choice_click, andAttributes: ["rang":"\(minMoney)-\(maxMoney)","gender":"\(sex)"])
                }else{
                    Statistics.count(Statistics.GiftChoice.selectgift_choice_click, andAttributes: ["rang":"\(minMoney)-\(maxMoney)","gender":"0"])
                }
            }
            
            self?.goods_listView.collectionView.beginHeaderRefresh()
        })

        giftChoiceScreenView.didDisplayHandle = {ishowing in self.didDisplay(ishowing)}
        
        self.view.addSubview(giftChoiceScreenView)

        self.prepareData()
    }
    
    //MARK:选择条件框是否已经显示或隐藏
    func didDisplay(_ isShowing:Bool)
    {
        self.addRightNavItemOnView(isShowing)
    }
    
    
    //MARK:添加筛选按钮
    func addRightNavItemOnView(_ isShowing:Bool = false)
    {
        self.navigationItem.rightBarButtonItem = CommonWidgets.NavigationRightBtn.newRightBtn(!isShowing ? "筛选":"取消",target: self, action: #selector(GiftChoiceListViewController.rightNavItemSearchClick(_:)))
    }
    
    
    
    func rightNavItemSearchClick(_ sender:UIButton!)
    {
        if giftChoiceScreenView.isShowing
        {
            giftChoiceScreenView.dismiss()
            
            self.gesturBackEnable = true
        }else
        {
            giftChoiceScreenView.show()
            
            self.gesturBackEnable = false
        }
    }
    
    
    //MARK:准备数据
    fileprivate func prepareData()
    {
        
        goods_listView.collectionView.addHeaderRefresh { [weak self]() -> Void in
            self?.goods_reload(true)
        }
        
        goods_listView.collectionView.addFooterRefresh { [weak self]() -> Void in
            
            self?.goods_reload(false)
        }
        
        
        
        article_listView.tableView.addHeaderRefresh { [weak self]() -> Void in
            self?.article_reload(true)
        }
        
        
        article_listView.tableView.addFooterRefresh { [weak self]() -> Void in
            self?.article_reload(false)
        }
        
        
        words =  topicInfo.attributes!.title
        
        self.navigationItem.title = words
        
        goods_words =  topicInfo.attributes!.goods
        
        article_words =  topicInfo.attributes!.article
        
        goods_listView.collectionView.beginHeaderRefresh()
    }
    
    
    //MARK:加载文章列表
    fileprivate func article_reload(_ isHeadRefresh:Bool)
    {
        if article_listView.tableView.isFooterRefreshing && article_listView.tableView.isHeaderRefreshing{return}
        
        var params = [String: AnyObject]()
        
        params["words"] = article_words as AnyObject?
        
        if isHeadRefresh
        {
            article_dataPage = 1
        }else
        {
            article_dataPage += 1
        }
        
        params["page"] = article_dataPage as AnyObject?
        
        params["size"] = article_PageSize as AnyObject?
        
        params["type"] = "r" as AnyObject?
        
        let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.search_article, parameters: params, isNeedUserTokrn: true,isShowErrorStatuMsg:true, successHandler: {data, status, msg in
        
            self.article_listView.tableView.closeAllRefresh()
            
            if status != 200
            {
                self.article_listView.tableView.showTipe(UIScrollTipeMode.nullData).tipeContent(Constant.TipeWords.nullData_article)
                
                return
            }
            
            if let list = JSON(data as AnyObject)["data"]["list"].array
            {
                if list.count > 0
                {
                    if isHeadRefresh
                    {
                        self.article_data = list
                    }else
                    {
                        let _ = list.map{self.article_data.append($0)}
                    }
                    
                    if let total = JSON(data as AnyObject)["data"]["search"]["TotalFound"].int
                    {
                        //如果第一次加载数据，已经加载完全部内容，那么就不需要“上拉加载”更多的功能了
                        if total <= self.article_PageSize
                        {
                            self.article_listView.tableView.removeFooterRefresh()
                        }else
                        {
                            //截止此次数据加载，如果此理论加载数据总量大于等于实际获取到的数据总量,那么数据已经加载完毕
                            if (self.article_dataPage * self.article_PageSize) >= total
                            {
                                self.article_listView.tableView.showTipe(UIScrollTipeMode.noMuchMore)
                            }
                        }
                    }
                    
                    self.article_listView.articleList = self.article_data
                    
//                    self.article_listView.tableView.setContentOffset(CGPointMake(0, 0), animated: true)
                }
            }
            else
            {
                if isHeadRefresh
                {
                    self.article_data = []
                    
                    self.article_listView.articleList = self.article_data
                    
                    self.article_listView.tableView.showTipe(UIScrollTipeMode.nullData).tipeContent(Constant.TipeWords.nullData_article)
                }else
                {
                    
                    self.article_listView.tableView.showTipe(UIScrollTipeMode.noMuchMore)
                }
            }
            
            }){Void in
                self.article_listView.tableView.closeAllRefresh()
                
                self.article_listView.tableView.showTipe(UIScrollTipeMode.nullData).tipeContent(Constant.TipeWords.nullData_article)
        }
    }
    
    
    //MARK:加载商品列表
    fileprivate func goods_reload(_ isHeadRefresh:Bool)
    {
        if goods_listView.collectionView.isFooterRefreshing && goods_listView.collectionView.isHeaderRefreshing{return}
        
        var params = [String: AnyObject]()
        
        params["words"] = goods_words as AnyObject?
        
        params["gender"] = giftChoiceScreenView.sex as AnyObject?
        
        params["min_price"] = moneyDues[giftChoiceScreenView.moneyDuesIndex.first!] as AnyObject?
        
        params["max_price"] = moneyDues[giftChoiceScreenView.moneyDuesIndex.last!] as AnyObject?
        
        if isHeadRefresh
        {
            goods_dataPage = 1
        }else
        {
            goods_dataPage += 1
        }
        
        params["page"] = goods_dataPage as AnyObject?
        
        params["size"] = goods_PageSize as AnyObject?
        
        params["type"] = "r" as AnyObject?
        
        let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.search_goods, parameters: params, isNeedUserTokrn: true,isShowErrorStatuMsg:true,successHandler: {data, status, msg in
            self.goods_listView.collectionView.closeAllRefresh()
            print(data)
            if status != 200
            {
                self.goods_listView.collectionView.showTipe(UIScrollTipeMode.nullData).tipeContent(Constant.TipeWords.nullData_goods)
                
                return
            }
            
            if let list = JSON(data as AnyObject)["data"]["list"].array
            {
                if list.count > 0
                {
                    if isHeadRefresh
                    {
                        self.goods_data = list
                    }else
                    {
                        let _ = list.map{self.goods_data.append($0)}
                    }
                    
                    if let total = JSON(data as AnyObject)["data"]["search"]["TotalFound"].int
                    {
                        //如果第一次加载数据，已经加载完全部内容，那么就不需要“上拉加载”更多的功能了
                        if total <= self.goods_PageSize
                        {
                            self.goods_listView.collectionView.removeFooterRefresh()
                        }else
                        {
                            //截止此次数据加载，如果此理论加载数据总量大于等于实际获取到的数据总量,那么数据已经加载完毕
                            if (self.goods_dataPage * self.goods_PageSize) >= total
                            {
                                self.goods_listView.collectionView.showTipe(UIScrollTipeMode.noMuchMore)
                            }
                        }
                    }
                    
                    self.goods_listView.goodsList = self.goods_data
                }
            }
            else
            {
                if isHeadRefresh
                {
                    self.goods_data = []
                    
                    self.goods_listView.goodsList = self.goods_data
                    
                    self.goods_listView.collectionView.showTipe(UIScrollTipeMode.nullData).tipeContent(Constant.TipeWords.nullData_goods)
                }else
                {
                    self.goods_listView.collectionView.closeTipe()
                }
            }
            
            }){Void in
                self.goods_listView.collectionView.closeAllRefresh()
                self.goods_listView.collectionView.showTipe(UIScrollTipeMode.nullData).tipeContent(Constant.TipeWords.nullData_goods)
        }
    }
    
    
    fileprivate var isFirst = true
    
    //MARK:选中的Tab
//    private func selectedIndex(index:Int)
//    {
//        if index == 1
//        {
//            self.navigationItem.rightBarButtonItem = nil
//            
//            goods_listView.collectionView.scrollsToTop = false
//            
//            article_listView.tableView.scrollsToTop = true
//        }else
//        {
//            self.addRightNavItemOnView()
//            
//            goods_listView.collectionView.scrollsToTop = true
//            
//            article_listView.tableView.scrollsToTop = false
//        }
//        
//        if currentIndex == index
//        {
//            return
//        }else
//        {
//            currentIndex = index
//        }
//        
//        if index == 1 && isFirst
//        {
//            article_listView.tableView.beginHeaderRefresh()
//            
//            isFirst = false
//        }
//        
//        scrollView.scrollRectToVisible(CGRectMake(Constant.ScreenSize.SCREEN_WIDTH * CGFloat(index),0,scrollView.iwidth,scrollView.iheight), animated: true)
//    }
    
    func lazyPageScrollView(_ pageScrollView: LazyPageScrollView, changeIndex index: Int) {
        if index == 1
        {
            Statistics.count(Statistics.GiftChoice.selectgift_grouptab1_click,andAttributes: ["name":words])
            
            self.navigationItem.rightBarButtonItem = nil
            
            goods_listView.collectionView.scrollsToTop = false
            
            article_listView.tableView.scrollsToTop = true
        }else
        {
            Statistics.count(Statistics.GiftChoice.demo_test,andAttributes: ["name":words])
            
            self.addRightNavItemOnView()
            
            goods_listView.collectionView.scrollsToTop = true
            
            article_listView.tableView.scrollsToTop = false
        }
        
        if currentIndex == index
        {
            return
        }else
        {
            currentIndex = index
        }
        
        if index == 1 && isFirst
        {
            article_listView.tableView.beginHeaderRefresh()
            
            isFirst = false
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension GiftChoiceListViewController:CommonTableViewDelegate,CommonCollectionViewDelegate{
    
    func commonTableViewDidSelected(_ data: HomeCommonArticleModel) {
        Statistics.count(Statistics.GiftChoice.selectgift_articlelist_click, andAttributes: ["aid":"\(data.id)"])
    }
    
    
    func commonCollectionViewDidSelected(_ data:Int) {
        Statistics.count(Statistics.GiftChoice.selectgift_giftlist_click, andAttributes: ["gfId":"\(data)"])
    }
    
}
