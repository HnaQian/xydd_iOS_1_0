//
//  GiftChoiceWidgets.swift
//  Limi
//
//  Created by 千云锋 on 16/8/4.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import Foundation

//MARK:GiftChoiceOptionViewController
extension GiftChoiceOptionViewController {
    //MARK:-顶部图片
    class HeaderImageView: BaseGiftChoiceUnitView {
        
        fileprivate var dataName:String = ""
        fileprivate var dataImage:String = ""
        //选礼对象头像
        fileprivate let headerImageView:UIImageView = {
            let iv = UIImageView()
            
            return iv
        }()
        
        //选礼对象名字
        fileprivate let nameLabel:UILabel = {
            let lbl = UILabel()
            lbl.textColor = Constant.Theme.Color17
            lbl.font = Constant.Theme.Font_14
            return lbl
        }()
        
        //ta的个性
        fileprivate let natureLabel:UILabel = {
            let lbl = UILabel()
            lbl.text = "TA的个性"
            lbl.textColor = Constant.Theme.Color2
            lbl.font = Constant.Theme.Font_15
            return lbl
        }()
        
        
        //ta的个性选项
        let natureView:NatureView = NatureView()
        
        override func setupUI() {
            self.backgroundColor = UIColor.white
        }

        //布局
        func composition(){
            self.addSubview(self.headerImageView)
            self.headerImageView.snp_makeConstraints { (make) in
                let _ = make.top.equalTo(Constant.ScreenSizeV2.MARGIN_30)
                let _ = make.width.equalTo(Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * 216)
                let _ = make.height.equalTo(Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * 216)
                let _ = make.centerX.equalTo(self)
            }
            
            self.addSubview(self.nameLabel)
            self.nameLabel.snp_makeConstraints { (make) in
                let _ = make.top.equalTo(self.headerImageView.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_13)
                let _ = make.centerX.equalTo(self)
            }
            
            self.addSubview(self.natureLabel)
            self.natureLabel.snp_makeConstraints { (make) in
                let _ = make.top.equalTo(self.nameLabel.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_40)
                let _ = make.centerX.equalTo(self)
            }
            
            self.addSubview(self.natureView)
            self.natureView.snp_makeConstraints { (make) in
                let _ = make.top.equalTo(natureLabel.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_30)
                let _ = make.left.equalTo(self)
                let _ = make.right.equalTo(self)
                let _ = make.height.equalTo(360 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
            }
        }
        
        //高斯模糊
        func createBlurBackground (_ image:UIImage,blurRadius:Float) {
            //处理原始NSData数据
            let originImage = CIImage(cgImage: image.cgImage! )
            //创建高斯模糊滤镜
            let filter = CIFilter(name: "CIGaussianBlur")
            filter!.setValue(originImage, forKey: kCIInputImageKey)
            filter!.setValue(NSNumber(value: blurRadius as Float), forKey: "inputRadius")
            //生成模糊图片
            let context = CIContext(options: nil)
            let result:CIImage = filter!.value(forKey: kCIOutputImageKey) as! CIImage
            let blurImage = UIImage(cgImage: context.createCGImage(result, from: result.extent)!)
            //将模糊图片加入背景
            let w = self.frame.width
            let h = self.frame.height
            let blurImageView = UIImageView(frame: CGRect(x: -w*5/8, y: -h*5/8, width: 2.25 * w, height: 2.25 * h)) //模糊背景是界面的4倍大小
            blurImageView.contentMode = UIViewContentMode.scaleAspectFill //使图片充满ImageView
            blurImageView.autoresizingMask = UIViewAutoresizing.flexibleWidth //保   持原图长宽比
            blurImageView.autoresizingMask = UIViewAutoresizing.flexibleHeight
            blurImageView.image = blurImage
            blurImageView.alpha = 0.3
            gcd.async(.main){
                self.addSubview(blurImageView)
                self.sendSubview(toBack: blurImageView)
            }
            
        }
        
        override func reloadData(_ dataSource: [AnyObject]) {
            let viewW = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * 216
            let imageView = UIImageView()

            let imageScale : String = String(describing: dataSource[0]) + "?imageView2/0/w/" + "\(Int(viewW * Constant.ScreenSizeV1.SCALE_SCREEN))" + "/h/" + "\(Int(viewW * Constant.ScreenSizeV1.SCALE_SCREEN))"
            
            imageView.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "Default_giftChoice"))
            
            self.headerImageView.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "Default_giftChoice"))
            self.nameLabel.text = dataSource[1] as? String

            if #available(iOS 9.0, *) {
                gcd.async(.default) {
                    self.createBlurBackground(imageView.image!, blurRadius: 6)
                }
            }
            self.layer.masksToBounds = true
            
            let arr = dataSource[2] as? [String]
            print(arr)
            
            self.natureView.reloadData(arr! as [AnyObject])
            
            composition()

        }
        
    }
    
    class NatureView: BaseGiftChoiceUnitView {
        
        fileprivate var dataSourceName:[String] = []
        fileprivate var linesCount = 1
        fileprivate var lineWidth:CGFloat = 0.0
        fileprivate var numberArr:[Int] = [-1]
        fileprivate var buttonArr:[UIButton] = []
        fileprivate var lineViewArr:[UIView] = []
        fileprivate var widthArr:[CGFloat] = []
        
        func calculateLinesCountAndEveryLineNumber(_ dataArr:[String]) {
            
            for i in 0..<dataArr.count {
                let button = createBtn(dataArr[i])
                
                if i == 0 {
                    button.layer.borderWidth = 0
                    button.isSelected = true
                    button.backgroundColor = Constant.Theme.Color1
                }
                let widthButton = CalculateHeightOrWidth.getLabOrBtnWidth((button.titleLabel?.text)! as NSString, font: Constant.Theme.Font_14, height: Constant.ScreenSizeV2.MARGIN_60) + Constant.ScreenSizeV2.MARGIN_68 + 4
                lineWidth = lineWidth + widthButton + Constant.ScreenSizeV2.MARGIN_40
                buttonArr.append(button)
                widthArr.append(widthButton)
                if lineWidth > Constant.ScreenSizeV2.SCREEN_WIDTH - Constant.ScreenSizeV2.MARGIN_40 {
                    linesCount += 1
                    numberArr.append(i - 1)
                    
                    lineWidth = widthButton + Constant.ScreenSizeV2.MARGIN_40
                }
                
            }
            numberArr.append(dataArr.count - 1)
            
            compositionButton()
        }
        
        func compositionButton() {
            
            for i in 0...linesCount - 1 {
                let lineView = UIView()
                lineViewArr.append(lineView)
                self.addSubview(lineView)
                for j in numberArr[i] + 1...numberArr[i + 1] {
                    lineView.addSubview(buttonArr[j])
                    
                    
                    
                    if j == numberArr[i] + 1 {
                        
                        buttonArr[j].snp_makeConstraints(closure: { (make) in
                            let _ = make.top.equalTo(lineView)
                            let _ = make.left.equalTo(lineView)
                            let _ = make.width.equalTo(widthArr[j])
                            let _ = make.bottom.equalTo(lineView)
                            let _ = make.height.equalTo(Constant.ScreenSizeV2.MARGIN_60)
                        })
                        
                        
                    }else if j == numberArr[i + 1] {
                        buttonArr[j].snp_makeConstraints(closure: { (make) in
                            let _ = make.top.equalTo(lineView)
                            let _ = make.left.equalTo(buttonArr[j - 1].snp_right).offset(Constant.ScreenSizeV2.MARGIN_40)
                            let _ = make.right.equalTo(lineView)
                            let _ = make.width.equalTo(widthArr[j])
                            let _ = make.bottom.equalTo(lineView)
                            let _ = make.height.equalTo(Constant.ScreenSizeV2.MARGIN_60)
                        })
                    }else {
                        buttonArr[j].snp_makeConstraints(closure: { (make) in
                            let _ = make.top.equalTo(lineView)
                            let _ = make.left.equalTo(buttonArr[j - 1].snp_right).offset(Constant.ScreenSizeV2.MARGIN_40)
                            let _ = make.width.equalTo(widthArr[j])
                            let _ = make.bottom.equalTo(lineView)
                            let _ = make.height.equalTo(Constant.ScreenSizeV2.MARGIN_60)
                            
                        })
                    }
                    
                    
                    
                }
                
                if lineView.subviews.count == 1 {
                    lineView.subviews[0].snp_makeConstraints(closure: { (make) in
                        let _ = make.top.equalTo(lineView)
                        let _ = make.left.equalTo(lineView)
                        let _ = make.bottom.equalTo(lineView)
                        let _ = make.right.equalTo(lineView)
                        let _ = make.height.equalTo(Constant.ScreenSizeV2.MARGIN_60)
                        
                    })
                }
                
                
                if i == 0 {
                    lineView.snp_makeConstraints(closure: { (make) in
                        let _ = make.top.equalTo(self)
                        let _ = make.centerX.equalTo(self)
                    })
                }else {
                    lineView.snp_makeConstraints(closure: { (make) in
                        let _ = make.top.equalTo(lineViewArr[i - 1].snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_30)
                        let _ = make.centerX.equalTo(self)
                    })
                }
            }
            
        }
        
        
        func createBtn(_ title: String) -> UIButton {
            let btn = UIButton()
            //            btn.backgroundColor = UIColor.whiteColor()
            btn.setTitle(title, for: UIControlState())
            btn.setTitleColor(Constant.Theme.Color17, for: UIControlState())
            btn.setTitleColor(UIColor.white, for: UIControlState.selected)
            btn.titleLabel?.font = Constant.Theme.Font_14
            btn.layer.cornerRadius = Constant.ScreenSizeV2.MARGIN_30
            btn.layer.borderWidth = 0.5
            btn.addTarget(self, action: #selector(NatureView.onClick(_:)), for: UIControlEvents.touchUpInside)
            btn.layer.borderColor = Constant.Theme.Color17.cgColor
            btn.contentEdgeInsets.left = 34 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
            btn.contentEdgeInsets.right = 34 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE

            return btn
        }
        
        
        func onClick(_ button:UIButton) {
            
            
            if !button.isSelected {
                if let buttonName = button.titleLabel?.text {
                    self.delegate?.baseGiftChoiceUnitViewTouchdAction(GiftChoiceUnitViewActionType.natureView, content: ["NatureView":buttonName])
                    
                }
            }
            
            for button in buttonArr {
                button.layer.borderWidth = 0.5
                button.backgroundColor = UIColor.clear
                button.isSelected = false
            }
            
            button.layer.borderWidth = 0
            button.isSelected = true
            button.backgroundColor = Constant.Theme.Color1
            
            
        }

        
        override func reloadData(_ dataSource: [AnyObject]) {
            
            self.dataSourceName = dataSource as! [String]
            if self.dataSourceName.count != 0 {
                
                calculateLinesCountAndEveryLineNumber(self.dataSourceName)
                
            }
        }
        
    }
    
    //MARK:-补充提示头
    class SupplementView: BaseGiftChoiceUnitView {
        
        override func setupUI(){
            self.backgroundColor = Constant.Theme.Color18
            let mainLabel = UILabel()
            mainLabel.text = "还有要补充的么？"
            mainLabel.textColor = Constant.Theme.Color2
            mainLabel.font = Constant.Theme.Font_15
            self.addSubview(mainLabel)
            mainLabel.snp_makeConstraints { (make) in
                let _ = make.top.equalTo(self).offset(Constant.ScreenSizeV2.MARGIN_40)
                let _ = make.centerX.equalTo(self)
                
            }
            
            let subLabel = UILabel()
            subLabel.text = "TA的信息越多，推荐的礼物就越准哦"
            subLabel.textColor = Constant.Theme.Color17
            subLabel.font = Constant.Theme.Font_14
            self.addSubview(subLabel)
            subLabel.snp_makeConstraints { (make) in
                let _ = make.top.equalTo(mainLabel.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_20)
                let _ = make.centerX.equalTo(self)
            }
        }
        
        
        
    }
    
    //MARK:-性别
    class GirlOrBoyView: BaseGiftChoiceUnitView {
        
        fileprivate var linesCount = 1
        fileprivate var lineWidth:CGFloat = 0.0
        fileprivate var numberArr:[Int] = [-1]
        fileprivate var buttonArr:[UIButton] = []
        fileprivate var lineViewArr:[UIView] = []
        fileprivate var widthArr:[CGFloat] = []
        
        fileprivate let rootView = UIView()
        
        override func setupUI() {
            self.backgroundColor = Constant.Theme.Color18
            self.addSubview(rootView)

        }
        
        func calculateLinesCountAndEveryLineNumber(_ dataArr:[String]) {
            
            for i in 0..<dataArr.count {
                let button = createBtn(dataArr[i])
                button.tag = 520 + i
                let widthButton = CalculateHeightOrWidth.getLabOrBtnWidth((button.titleLabel?.text)! as NSString, font: Constant.Theme.Font_14, height: Constant.ScreenSizeV2.MARGIN_60) + Constant.ScreenSizeV2.MARGIN_68
                lineWidth = lineWidth + widthButton + Constant.ScreenSizeV2.MARGIN_40
                buttonArr.append(button)
                widthArr.append(widthButton)
                if lineWidth > Constant.ScreenSizeV2.SCREEN_WIDTH - Constant.ScreenSizeV2.MARGIN_40 {
                    linesCount += 1
                    numberArr.append(i - 1)
                    
                    lineWidth = widthButton + Constant.ScreenSizeV2.MARGIN_40
                }
                
            }
            numberArr.append(dataArr.count - 1)
            
            self.frame = CGRect(x: 0, y: Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * 88, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * (102 + CGFloat(linesCount) * 90))
            
            compositionButton()
        }
        
        func compositionButton() {
            
            for i in 0...linesCount - 1 {
                let lineView = UIView()
                lineViewArr.append(lineView)
                self.addSubview(lineView)
                for j in numberArr[i] + 1...numberArr[i + 1] {
                    lineView.addSubview(buttonArr[j])
                    
                    if j == numberArr[i] + 1 {
                        
                        buttonArr[j].snp_makeConstraints(closure: { (make) in
                            let _ = make.top.equalTo(lineView)
                            let _ = make.left.equalTo(lineView)
                            let _ = make.width.equalTo(widthArr[j])
                            let _ = make.bottom.equalTo(lineView)
                            let _ = make.height.equalTo(Constant.ScreenSizeV2.MARGIN_60)
                        })
                        
                        
                    }else if j == numberArr[i + 1] {
                        buttonArr[j].snp_makeConstraints(closure: { (make) in
                            let _ = make.top.equalTo(lineView)
                            let _ = make.left.equalTo(buttonArr[j - 1].snp_right).offset(Constant.ScreenSizeV2.MARGIN_40)
                            let _ = make.right.equalTo(lineView)
                            let _ = make.width.equalTo(widthArr[j])
                            let _ = make.bottom.equalTo(lineView)
                            let _ = make.height.equalTo(Constant.ScreenSizeV2.MARGIN_60)
                        })
                    }else {
                        buttonArr[j].snp_makeConstraints(closure: { (make) in
                            let _ = make.top.equalTo(lineView)
                            let _ = make.left.equalTo(buttonArr[j - 1].snp_right).offset(Constant.ScreenSizeV2.MARGIN_40)
                            let _ = make.width.equalTo(widthArr[j])
                            let _ = make.bottom.equalTo(lineView)
                            let _ = make.height.equalTo(Constant.ScreenSizeV2.MARGIN_60)
                            
                        })
                    }
                    
                    
                    
                }
                
                if lineView.subviews.count == 1 {
                    lineView.subviews[0].snp_makeConstraints(closure: { (make) in
                        let _ = make.top.equalTo(lineView)
                        let _ = make.left.equalTo(lineView)
                        let _ = make.bottom.equalTo(lineView)
                        let _ = make.right.equalTo(lineView)
                        let _ = make.height.equalTo(Constant.ScreenSizeV2.MARGIN_60)
                        
                    })
                }
                
                
                if i == 0 {
                    lineView.snp_makeConstraints(closure: { (make) in
                        let _ = make.top.equalTo(self).offset(Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * 40)
                        let _ = make.centerX.equalTo(self)
                    })
                }else {
                    lineView.snp_makeConstraints(closure: { (make) in
                        let _ = make.top.equalTo(lineViewArr[i - 1].snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_30)
                        let _ = make.centerX.equalTo(self)
                    })
                }
            }
            
        }
        
        func createBtn(_ title: String) -> UIButton {
            let btn = UIButton()
            //            btn.backgroundColor = UIColor.whiteColor()
            btn.setTitle(title, for: UIControlState())
            btn.setTitleColor(Constant.Theme.Color17, for: UIControlState())
            btn.setTitleColor(UIColor.white, for: UIControlState.selected)
            btn.titleLabel?.font = Constant.Theme.Font_14
            btn.layer.cornerRadius = Constant.ScreenSizeV2.MARGIN_30
            btn.layer.borderWidth = 0.5
            btn.layer.borderColor = Constant.Theme.Color17.cgColor
            btn.addTarget(self, action: #selector(GirlOrBoyView.onClick(_:)), for: UIControlEvents.touchUpInside)
            btn.contentEdgeInsets.left = 34 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
            btn.contentEdgeInsets.right = 34 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
            
            return btn
        }
        
        func onClick(_ button:UIButton) {
            if button.isSelected {
                button.isSelected = false
                button.backgroundColor = Constant.Theme.Color18
                button.layer.borderWidth = 0.5
                self.delegate?.baseGiftChoiceUnitViewTouchdAction(GiftChoiceUnitViewActionType.girlOrBoyView, content: nil)
            }else{
                for btn in buttonArr{
                    btn.isSelected = false
                    btn.backgroundColor = Constant.Theme.Color18
                    btn.layer.borderWidth = 0.5
                    
                }
                button.isSelected = true
                button.layer.borderWidth = 0
                button.backgroundColor = Constant.Theme.Color1
                let number = button.tag - 520
                let sexValue = String(self.dataSubMolde[number].value)
                self.delegate?.baseGiftChoiceUnitViewTouchdAction(GiftChoiceUnitViewActionType.girlOrBoyView, content: ["GirlOrBoyView":sexValue!])
            }

            
        }
        fileprivate var dataSubMolde = [GiftChoiceSubModel]()
        
        override func reloadData(_ dataSource: [AnyObject]) {
            self.dataSubMolde = dataSource as! [GiftChoiceSubModel]
            let data = dataSource as! [GiftChoiceSubModel]
            var arr:[String] = []
            for model in data {
                arr.append(model.name)
            }
            
            calculateLinesCountAndEveryLineNumber(arr)
        }
    }
    
    //MARK:-年代
    class AgesView:BaseGiftChoiceUnitView {
        
        //offSetWidth
        fileprivate var scrollWidth:CGFloat = 0
        
        fileprivate var dataSource:[String] = []
        lazy fileprivate var scrollView:UIScrollView = {
            var scroll = UIScrollView()
            scroll.showsHorizontalScrollIndicator = false
            scroll.showsVerticalScrollIndicator = false
            
            return scroll
        }()
        
        fileprivate let leftButton:UIButton = {
            let btn = UIButton()
//            btn.backgroundColor = UIColor.blueColor()
            btn.imageView?.contentMode = .scaleAspectFit
            btn.imageEdgeInsets = UIEdgeInsetsMake(0, Constant.ScreenSizeV2.MARGIN_15, 0, -Constant.ScreenSizeV2.MARGIN_15)
            btn.setImage(UIImage(named: "jiantou_left"), for: UIControlState())
            return btn
        }()
        
        fileprivate let rightButton:UIButton = {
            let btn = UIButton()
//            btn.backgroundColor = UIColor.blueColor()
            btn.imageView?.contentMode = .scaleAspectFit
            btn.imageEdgeInsets = UIEdgeInsetsMake(0, -Constant.ScreenSizeV2.MARGIN_15, 0, Constant.ScreenSizeV2.MARGIN_15)
            btn.setImage(UIImage(named: "jiantou_right"), for: UIControlState())
            return btn
        }()
        
        fileprivate let lineView:UIView = {
            let line = UIView()
            line.backgroundColor = UIColor.white
            return line
        }()
        
        func onLeftClick(_ button:UIButton){
            let buttonWidth = CalculateHeightOrWidth.getLabOrBtnWidth("70后", font: Constant.Theme.Font_14, height: Constant.ScreenSizeV2.MARGIN_60) + Constant.ScreenSizeV2.MARGIN_44
            if self.scrollView.offsetX > 0 {
                if buttonWidth > self.scrollView.offsetX {
                    self.scrollView.offsetX = 0
                }else{
                    self.scrollView.offsetX -= buttonWidth
                }
            }
        }
            

        func onRightClick(_ button:UIButton){
            let buttonWidth = CalculateHeightOrWidth.getLabOrBtnWidth("70后", font: Constant.Theme.Font_14, height: Constant.ScreenSizeV2.MARGIN_60) + Constant.ScreenSizeV2.MARGIN_44
            let subWidth = scrollWidth - self.scrollView.frame.size.width - self.scrollView.offsetX
            if subWidth > 0 {
                if buttonWidth > subWidth {
                    self.scrollView.offsetX += subWidth
                }else{
                    self.scrollView.offsetX += buttonWidth
                }
            }
        }
        
        override func setupUI() {
            self.backgroundColor = Constant.Theme.Color18
            
            self.addSubview(lineView)
            lineView.snp_makeConstraints { (make) in
                let _ = make.top.equalTo(self)
                let _ = make.height.equalTo(1)
                let _ = make.left.equalTo(self).offset(Constant.ScreenSizeV2.MARGIN_30)
                let _ = make.right.equalTo(self).offset(-Constant.ScreenSizeV2.MARGIN_30)
            }
            
            leftButton.addTarget(self, action: #selector(AgesView.onLeftClick(_:)), for: UIControlEvents.touchUpInside)
            self.addSubview(leftButton)
            rightButton.addTarget(self, action: #selector(AgesView.onRightClick(_:)), for: UIControlEvents.touchUpInside)
            self.addSubview(rightButton)
            
            leftButton.snp_makeConstraints { (make) in
                let _ = make.left.equalTo(self)
                let _ = make.centerY.equalTo(self)
                let _ = make.width.equalTo(70 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
                let _ = make.height.equalTo(100 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
            }
            
            rightButton.snp_makeConstraints { (make) in
                let _ = make.right.equalTo(self)
                let _ = make.centerY.equalTo(self)
                let _ = make.width.equalTo(70 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
                let _ = make.height.equalTo(100 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
            }
 
            
        }
        
        func createBtn(_ title: String) -> UIButton {
            let btn = UIButton()
            btn.setTitle(title, for: UIControlState())
            btn.setTitleColor(Constant.Theme.Color17, for: UIControlState())
            btn.titleLabel?.font = Constant.Theme.Font_14
            btn.layer.cornerRadius = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * 25
            btn.addTarget(self, action: #selector(AgesView.onClick(_:)), for: UIControlEvents.touchUpInside)
            btn.backgroundColor = Constant.Theme.Color18
            btn.setTitleColor(UIColor.white, for: UIControlState.selected)
            btn.contentEdgeInsets.left = 21 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
            btn.contentEdgeInsets.right = 21 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
            return btn
        }
        
        func onClick(_ button:UIButton) {
            if button.isSelected {
                button.isSelected = false
                button.backgroundColor = Constant.Theme.Color18
                self.delegate?.baseGiftChoiceUnitViewTouchdAction(GiftChoiceUnitViewActionType.agesView, content: nil)
            }else{
                for btn in self.scrollView.subviews as! [UIButton] {
                    btn.isSelected = false
                    btn.backgroundColor = Constant.Theme.Color18
                }
                button.isSelected = true
                button.backgroundColor = Constant.Theme.Color1
                let num = button.tag - 700
                let ageValue = String(self.dataAges[num].value)
                self.delegate?.baseGiftChoiceUnitViewTouchdAction(GiftChoiceUnitViewActionType.agesView, content: ["AgesView":ageValue!])
            }
        }

        fileprivate var dataAges = [GiftChoiceSubModel]()
        
        override func reloadData(_ dataSource: [AnyObject]) {
            self.dataAges = dataSource as! [GiftChoiceSubModel]
            let data = dataSource as! [GiftChoiceSubModel]
            for model in data {
                self.dataSource.append(model.name)
            }
            
            for i in 0...self.dataSource.count - 1 {
                let button = createBtn(self.dataSource[i])
                button.tag = 700 + i
                self.scrollView.addSubview(button)
                
                
                button.snp_makeConstraints(closure: { (make) in
                    let _ = make.left.equalTo(scrollWidth)
                    let _ = make.height.equalTo(50 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
                    let _ = make.centerY.equalTo(scrollView)
                })
                
                
                scrollWidth += CalculateHeightOrWidth.getLabOrBtnWidth((button.titleLabel?.text)! as NSString, font: (button.titleLabel?.font)!, height: Constant.ScreenSizeV2.MARGIN_60) + Constant.ScreenSizeV2.MARGIN_44
                
                
            }
            
            self.addSubview(scrollView)
            scrollView.contentSize = CGSize(width: scrollWidth, height: 50 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
            scrollView.snp_makeConstraints { (make) in
                let _ = make.centerY.equalTo(self)
                let _ = make.left.equalTo(leftButton.snp_right)
                let _ = make.right.equalTo(rightButton.snp_left)
                let _ = make.height.equalTo(100 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
            }

        }
        
    }
    
    //MARK:-星座
    class ConstellationView: BaseGiftChoiceUnitView {
        
        //每个view的宽、高
        fileprivate let viewWidth = (Constant.ScreenSizeV2.SCREEN_WIDTH - Constant.ScreenSizeV2.MARGIN_60 - 7) / 6
        fileprivate let viewHeight = (200 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE - 3) / 2
        
        //button数组
        fileprivate var buttonArr:[UIButton] = []
        
        fileprivate let rootView = UIView()
        
        override func setupUI() {
            self.backgroundColor = Constant.Theme.Color18
            rootView.backgroundColor = UIColor.white
            self.addSubview(rootView)
            rootView.snp_makeConstraints { (make) in
                let _ = make.top.equalTo(self)
                let _ = make.left.equalTo(self).offset(Constant.ScreenSizeV2.MARGIN_30)
                let _ = make.right.equalTo(self).offset(-Constant.ScreenSizeV2.MARGIN_30)
                let _ = make.bottom.equalTo(self)
            }
            
            
        }
        
        fileprivate var dataConstellation = [GiftChoiceSubModel]()
        
        override func reloadData(_ dataSource: [AnyObject]) {
            self.dataConstellation = dataSource as! [GiftChoiceSubModel]
            let data = dataSource as! [GiftChoiceSubModel]
            var dataArr1:[String] = []
            var dataArr2:[String] = []
            for i in 0..<data.count {
                if i < 6 {
                    dataArr1.append(data[i].name)
                }else{
                    dataArr2.append(data[i].name)
                }
            }
            let dataArr = [dataArr1, dataArr2]
            let buttonWidth = CalculateHeightOrWidth.getLabOrBtnWidth("双鱼", font: Constant.Theme.Font_14, height: Constant.ScreenSizeV2.MARGIN_44) + Constant.ScreenSizeV2.MARGIN_40
            
            
            for i in 0...dataArr.count - 1 {
                for j in 0...dataArr[i].count - 1 {
                    let blockView = UIView()
                    blockView.backgroundColor = Constant.Theme.Color18
                    rootView.addSubview(blockView)
                    
                    blockView.snp_makeConstraints(closure: { (make) in
                        let _ = make.width.equalTo(viewWidth)
                        let _ = make.height.equalTo(viewHeight)
                        let _ = make.top.equalTo(1 + CGFloat(i) * (viewHeight + 1))
                        let _ = make.left.equalTo(1 + CGFloat(j) * (viewWidth + 1))
                    })
                    
                    
                    let button = createBtn(dataArr[i][j])
                    button.tag = 400 + i * 6 + j
                    buttonArr.append(button)
                    blockView.addSubview(button)
                    button.snp_makeConstraints(closure: { (make) in
                        let _ = make.center.equalTo(blockView)
                        let _ = make.width.equalTo(buttonWidth)
                        let _ = make.height.equalTo(50 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
                    })
                }
            }


        }
        
        func createBtn(_ title: String) -> UIButton {
            let btn = UIButton()
            btn.setTitle(title, for: UIControlState())
            btn.setTitleColor(Constant.Theme.Color17, for: UIControlState())
            btn.titleLabel?.font = Constant.Theme.Font_14
            btn.layer.cornerRadius = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * 25
            btn.addTarget(self, action: #selector(ConstellationView.onClick(_:)), for: UIControlEvents.touchUpInside)
            btn.backgroundColor = Constant.Theme.Color18
            btn.setTitleColor(UIColor.white, for: UIControlState.selected)
            btn.contentEdgeInsets.left = 20 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
            btn.contentEdgeInsets.right = 20 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
            return btn
        }
        
        func onClick(_ button:UIButton){
            if button.isSelected {
                button.isSelected = false
                button.backgroundColor = Constant.Theme.Color18
                self.delegate?.baseGiftChoiceUnitViewTouchdAction(GiftChoiceUnitViewActionType.constellationView, content: nil)
            }else{
                for btn in buttonArr {
                    btn.isSelected = false
                    btn.backgroundColor = Constant.Theme.Color18
                }
                button.isSelected = true
                button.backgroundColor = Constant.Theme.Color1
                let num = button.tag - 400
                let value = String(self.dataConstellation[num].value)
                self.delegate?.baseGiftChoiceUnitViewTouchdAction(GiftChoiceUnitViewActionType.constellationView, content: ["ConstellationView":value!])
            }

        }

    }
    
    //MARK:-场合
    class OccasionView: BaseGiftChoiceUnitView {
        
        
        fileprivate var dataSource:[String] = []
        
        fileprivate var linesCount = 1
        fileprivate var lineWidth:CGFloat = 0.0
        fileprivate var numberArr:[Int] = [-1]
        fileprivate var buttonArr:[UIButton] = []
        fileprivate var lineViewArr:[UIView] = []
        fileprivate var widthArr:[CGFloat] = []
        
        fileprivate let titleLabel:UILabel = {
            let lbl = UILabel()
            lbl.text = "有些特殊场合，TA想要的礼物可能会不太一样哦"
            lbl.textColor = Constant.Theme.Color17
            lbl.font = Constant.Theme.Font_14
            return lbl
        }()
        
        override func setupUI() {
            self.backgroundColor = Constant.Theme.Color18
            

            
            self.addSubview(titleLabel)
            titleLabel.snp_makeConstraints { (make) in
                let _ = make.top.equalTo(self).offset(Constant.ScreenSizeV2.MARGIN_30)
                let _ = make.centerX.equalTo(self)
            }
   
        }
        
        
        func calculateLinesCountAndEveryLineNumber(_ dataArr:[String]) {
            
            for i in 0..<dataArr.count {
                let button = createBtn(dataArr[i])
                button.tag = 1100 + i
                let widthButton = CalculateHeightOrWidth.getLabOrBtnWidth((button.titleLabel?.text)! as NSString, font: Constant.Theme.Font_14, height: Constant.ScreenSizeV2.MARGIN_60) + Constant.ScreenSizeV2.MARGIN_68
                lineWidth = lineWidth + widthButton + Constant.ScreenSizeV2.MARGIN_40
                buttonArr.append(button)
                widthArr.append(widthButton)
                if lineWidth > Constant.ScreenSizeV2.SCREEN_WIDTH - Constant.ScreenSizeV2.MARGIN_40 {
                    linesCount += 1
                    numberArr.append(i - 1)
                    
                    lineWidth = widthButton + Constant.ScreenSizeV2.MARGIN_40
                }
                
            }
            numberArr.append(dataSource.count - 1)
            
            self.frame = CGRect(x: 0, y: Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * 88, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * (102 + CGFloat(linesCount) * 90))
            compositionButton()
        }
        
        func compositionButton() {
          
            for i in 0...linesCount - 1 {
                let lineView = UIView()
                lineViewArr.append(lineView)
                self.addSubview(lineView)
                for j in numberArr[i] + 1...numberArr[i + 1] {
                    lineView.addSubview(buttonArr[j])

                    if j == numberArr[i] + 1 {
                        
                        buttonArr[j].snp_makeConstraints(closure: { (make) in
                            let _ = make.top.equalTo(lineView)
                            let _ = make.left.equalTo(lineView)
                            let _ = make.width.equalTo(widthArr[j])
                            let _ = make.bottom.equalTo(lineView)
                            let _ = make.height.equalTo(Constant.ScreenSizeV2.MARGIN_60)
                        })
                        
                        
                    }else if j == numberArr[i + 1] {
                        buttonArr[j].snp_makeConstraints(closure: { (make) in
                            let _ = make.top.equalTo(lineView)
                            let _ = make.left.equalTo(buttonArr[j - 1].snp_right).offset(Constant.ScreenSizeV2.MARGIN_40)
                            let _ = make.right.equalTo(lineView)
                            let _ = make.width.equalTo(widthArr[j])
                            let _ = make.bottom.equalTo(lineView)
                            let _ = make.height.equalTo(Constant.ScreenSizeV2.MARGIN_60)
                        })
                    }else {
                        buttonArr[j].snp_makeConstraints(closure: { (make) in
                            let _ = make.top.equalTo(lineView)
                            let _ = make.left.equalTo(buttonArr[j - 1].snp_right).offset(Constant.ScreenSizeV2.MARGIN_40)
                            let _ = make.width.equalTo(widthArr[j])
                            let _ = make.bottom.equalTo(lineView)
                            let _ = make.height.equalTo(Constant.ScreenSizeV2.MARGIN_60)
                            
                        })
                    }
                    
                    
                    
                }
                
                if lineView.subviews.count == 1 {
                    lineView.subviews[0].snp_makeConstraints(closure: { (make) in
                        let _ = make.top.equalTo(lineView)
                        let _ = make.left.equalTo(lineView)
                        let _ = make.bottom.equalTo(lineView)
                        let _ = make.right.equalTo(lineView)
                        let _ = make.height.equalTo(Constant.ScreenSizeV2.MARGIN_60)
                        
                    })
                }
                
                
                if i == 0 {
                    lineView.snp_makeConstraints(closure: { (make) in
                        let _ = make.top.equalTo(self).offset(Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * 88)
                        let _ = make.centerX.equalTo(self)
                    })
                }else {
                    lineView.snp_makeConstraints(closure: { (make) in
                        let _ = make.top.equalTo(lineViewArr[i - 1].snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_30)
                        let _ = make.centerX.equalTo(self)
                    })
                }
            }
            
        }
        
        func createBtn(_ title: String) -> UIButton {
            let btn = UIButton()
            //            btn.backgroundColor = UIColor.whiteColor()
            btn.setTitle(title, for: UIControlState())
            btn.setTitleColor(Constant.Theme.Color17, for: UIControlState())
            btn.setTitleColor(UIColor.white, for: UIControlState.selected)
            btn.titleLabel?.font = Constant.Theme.Font_14
            btn.layer.cornerRadius = Constant.ScreenSizeV2.MARGIN_30
            btn.layer.borderWidth = 0.5
            btn.layer.borderColor = Constant.Theme.Color17.cgColor
            btn.addTarget(self, action: #selector(OccasionView.onClick(_:)), for: UIControlEvents.touchUpInside)
            btn.contentEdgeInsets.left = 34 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
            btn.contentEdgeInsets.right = 34 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
            return btn
        }
        
        func onClick(_ button:UIButton) {
            if button.isSelected {
                button.isSelected = false
                button.backgroundColor = Constant.Theme.Color18
                button.layer.borderWidth = 0.5
                self.delegate?.baseGiftChoiceUnitViewTouchdAction(GiftChoiceUnitViewActionType.occasionView, content: nil)
            }else{
                for btn in buttonArr{
                    btn.isSelected = false
                    btn.backgroundColor = Constant.Theme.Color18
                    btn.layer.borderWidth = 0.5
                }
                button.isSelected = true
                button.layer.borderWidth = 0
                button.backgroundColor = Constant.Theme.Color1
                let num = button.tag - 1100
                let value = String(self.dataOccasion[num].name)
                self.delegate?.baseGiftChoiceUnitViewTouchdAction(GiftChoiceUnitViewActionType.occasionView, content: ["OccasionView":value!])
            }
            
            
        }

        fileprivate var dataOccasion = [GiftChoiceSubModel]()
        
        override func reloadData(_ dataSource: [AnyObject]) {
            self.dataOccasion = dataSource as! [GiftChoiceSubModel]
            
            let data = dataSource as! [GiftChoiceSubModel]

            for model in data {
                self.dataSource.append(model.name)
            }
            
           calculateLinesCountAndEveryLineNumber(self.dataSource)

        }
        
    }
    
    //MARK:-ToolBar
    class ToolBar: BaseGiftChoiceUnitView {
        
        fileprivate let labelName:UILabel = {
            
            let lbl = UILabel()
            lbl.text = "选好了"
            lbl.textColor = Constant.Theme.Color12
            lbl.font = Constant.Theme.Font_19
            return lbl
            
        }()
        
        fileprivate let rootView:UIView = UIView()
        
        fileprivate let imageVie:UIImageView = {
            let iV = UIImageView()
            iV.image = UIImage(named:"jiantou")
            return iV
        }()
        
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            self.frame = CGRect(x: 0, y: Constant.ScreenSizeV2.SCREEN_HEIGHT - 64 - Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * 90, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * 90)
            setupToolBar()
        }
        
        func onClick(){
            self.delegate?.baseGiftChoiceUnitViewTouchdAction(GiftChoiceUnitViewActionType.toolBar, content: nil)
            
        }
        
        func setupToolBar(){
            self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ToolBar.onClick)))
            self.backgroundColor = Constant.Theme.Color1
            self.addSubview(rootView)
            rootView.addSubview(labelName)
            rootView.addSubview(imageVie)
            
            labelName.snp_makeConstraints { (make) in
                let _ = make.top.equalTo(rootView)
                let _ = make.left.equalTo(rootView)
                let _ = make.bottom.equalTo(rootView)
                let _ = make.centerY.equalTo(rootView)
            }
            imageVie.snp_makeConstraints { (make) in
                let _ = make.left.equalTo(labelName.snp_right)
                let _ = make.right.equalTo(rootView)
                let _ = make.centerY.equalTo(rootView)
            }
            rootView.snp_makeConstraints { (make) in
                let _ = make.center.equalTo(self)
            }
            
        }
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
       // MARK:-商品各种控件的基类
    class BaseGiftChoiceUnitView:UIView{
        
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            
            self.clipsToBounds = true
            self.setupUI()
        }
        
        weak var delegate:BaseGiftChoiceUnitViewDelegate?
        func setupUI(){}
        
        //        weak var delegate:BaseGoodsDetailUnitViewDelegate?
        
        //刷新数据
        func reloadData(_ dataSource:[AnyObject]){}
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
    }
}

//MARK:GiftChoiceOptionViewController 点击代理
protocol BaseGiftChoiceUnitViewDelegate:NSObjectProtocol {
    func baseGiftChoiceUnitViewTouchdAction(_ actionType:GiftChoiceUnitViewActionType,content:[String:String]?)
}



//MARK:GiftChoiceGoodsViewController
extension GiftChoiceGoodsViewController {
    
    //MARK:-顶部按钮
    class HeaderButtonView: UIView {
        
        fileprivate let buttonWidth = (Constant.ScreenSizeV2.SCREEN_WIDTH - Constant.ScreenSizeV2.MARGIN_60 - 1.5)/4.0
        
        fileprivate let objectButton:UIButton = {
            let btn = UIButton()
            btn.setTitle("对象", for: UIControlState())
            btn.setImage(UIImage(named:"jiantou_down"), for: UIControlState())
            btn.setImage(UIImage(named:"jiantou_down_hight"), for: UIControlState.selected)
            btn.setTitleColor(Constant.Theme.Color17, for: UIControlState())
            btn.titleLabel?.font = Constant.Theme.Font_14
            
            btn.tag = 777
            return btn
        }()
        
        fileprivate let styleButton:UIButton = {
            let btn = UIButton()
            btn.setTitle("风格", for: UIControlState())
            btn.setImage(UIImage(named:"jiantou_down"), for: UIControlState())
            btn.setImage(UIImage(named:"jiantou_down_hight"), for: UIControlState.selected)
            btn.setTitleColor(Constant.Theme.Color17, for: UIControlState())
            btn.titleLabel?.font = Constant.Theme.Font_14
            btn.tag = 778
            return btn
        }()
        
        fileprivate let occasionButton:UIButton = {
            let btn = UIButton()
            btn.setTitle("场合", for: UIControlState())
            btn.setImage(UIImage(named:"jiantou_down"), for: UIControlState())
            btn.setImage(UIImage(named:"jiantou_down_hight"), for: UIControlState.selected)
            btn.setTitleColor(Constant.Theme.Color17, for: UIControlState())
            btn.titleLabel?.font = Constant.Theme.Font_14
            btn.tag = 779
            return btn
        }()
        
        fileprivate let budgetButton:UIButton = {
            let btn = UIButton()
            btn.setTitle("预算", for: UIControlState())
            btn.setImage(UIImage(named:"jiantou_down"), for: UIControlState())
            btn.setImage(UIImage(named:"jiantou_down_hight"), for: UIControlState.selected)
            btn.setTitleColor(Constant.Theme.Color17, for: UIControlState())
            btn.titleLabel?.font = Constant.Theme.Font_14
            btn.tag = 780
            return btn
        }()
        
        
        fileprivate var linsArr:[UIView] = []
        
        weak var delegate:BaseGiftChoiceGoodsUnitViewDelegate?
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            setupUI()
            NotificationCenter.default.addObserver(self, selector: #selector(HeaderButtonView.heartBtnClick), name: NSNotification.Name(rawValue: "updateButton1"), object: nil)
//            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(HeaderButtonView.heartBtnClick), name: "updateButton2", object: nil)
        }
        
        func heartBtnClick() {
            objectButton.isSelected = false
            styleButton.isSelected = false
            occasionButton.isSelected = false
            budgetButton.isSelected = false
        }
        
        deinit{
            NotificationCenter.default.removeObserver(self)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func buttonOnClick(_ button:UIButton) {
            let index = button.tag - 777
            self.delegate?.baseGiftChoiceGoodsUnitViewTouchdAction(GiftChoiceGoodsActiontype[index], content: button)
        }

        
        func reloadData(_ dataDic:[String:String], data:[GiftChoiceSubModel]){
            
            let imageWidth = objectButton.imageView?.iwidth
            print(imageWidth)
            
            if dataDic["name"] != nil {
                objectButton.setTitle(dataDic["name"], for: UIControlState())
                
                var lblWidth = CalculateHeightOrWidth.getLabOrBtnWidth(dataDic["name"]! as NSString, font: Constant.Theme.Font_14, height: 60)
                if lblWidth > (Constant.ScreenSizeV2.SCREEN_WIDTH - Constant.ScreenSizeV2.MARGIN_60 - 1.5)/4.0{
                    lblWidth = (Constant.ScreenSizeV2.SCREEN_WIDTH - Constant.ScreenSizeV2.MARGIN_60 - 1.5)/4.0
                    objectButton.imageEdgeInsets = UIEdgeInsetsMake(0, lblWidth - 15, 0, 0)
                }
                else{
                
                objectButton.imageEdgeInsets = UIEdgeInsetsMake(0, lblWidth, 0, -lblWidth)
                }
                objectButton.titleEdgeInsets = UIEdgeInsetsMake(0, -imageWidth!, 0, imageWidth!)
            }
            if dataDic["style"] != nil {
                styleButton.setTitle(dataDic["style"], for: UIControlState())
                
                var lblWidth = CalculateHeightOrWidth.getLabOrBtnWidth(dataDic["style"]! as NSString, font: Constant.Theme.Font_14, height: 60)
                
                if lblWidth > (Constant.ScreenSizeV2.SCREEN_WIDTH - Constant.ScreenSizeV2.MARGIN_60 - 1.5)/4.0{
                    lblWidth = (Constant.ScreenSizeV2.SCREEN_WIDTH - Constant.ScreenSizeV2.MARGIN_60 - 1.5)/4.0
                    styleButton.imageEdgeInsets = UIEdgeInsetsMake(0, lblWidth - 15, 0, 0)
                }
                else{
                    
                    styleButton.imageEdgeInsets = UIEdgeInsetsMake(0, lblWidth, 0, -lblWidth)
                }
                styleButton.titleEdgeInsets = UIEdgeInsetsMake(0, -imageWidth!, 0, imageWidth!)
            }else{
                styleButton.setTitle("风格", for: UIControlState())
                let lblWidth = CalculateHeightOrWidth.getLabOrBtnWidth("风格", font: Constant.Theme.Font_14, height: 60)
                
                styleButton.imageEdgeInsets = UIEdgeInsetsMake(0, lblWidth, 0, -lblWidth)
                styleButton.titleEdgeInsets = UIEdgeInsetsMake(0, -imageWidth!, 0, imageWidth!)
            }
            if dataDic["occasion"] != nil {
                occasionButton.setTitle(dataDic["occasion"], for: UIControlState())
                var lblWidth = CalculateHeightOrWidth.getLabOrBtnWidth(dataDic["occasion"]! as NSString, font: Constant.Theme.Font_14, height: 60)
                
                if lblWidth > (Constant.ScreenSizeV2.SCREEN_WIDTH - Constant.ScreenSizeV2.MARGIN_60 - 1.5)/4.0{
                    lblWidth = (Constant.ScreenSizeV2.SCREEN_WIDTH - Constant.ScreenSizeV2.MARGIN_60 - 1.5)/4.0
                    occasionButton.imageEdgeInsets = UIEdgeInsetsMake(0, lblWidth - 15, 0, 0)
                }
                else{
                    
                    occasionButton.imageEdgeInsets = UIEdgeInsetsMake(0, lblWidth, 0, -lblWidth)
                }
                occasionButton.titleEdgeInsets = UIEdgeInsetsMake(0, -imageWidth!, 0, imageWidth!)
  
            }else{
                
                occasionButton.setTitle("场合", for: UIControlState())
                let lblWidth = CalculateHeightOrWidth.getLabOrBtnWidth("场合", font: Constant.Theme.Font_14, height: 60)
                
                occasionButton.imageEdgeInsets = UIEdgeInsetsMake(0, lblWidth, 0, -lblWidth)
                occasionButton.titleEdgeInsets = UIEdgeInsetsMake(0, -imageWidth!, 0, imageWidth!)

            }
        }
        
        
        func setupUI() {
            self.backgroundColor = UIColor.white
            self.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: 100 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
            let buttonArr = [objectButton, styleButton, occasionButton, budgetButton]
            
            
            let lblWidth = CalculateHeightOrWidth.getLabOrBtnWidth("预算", font: Constant.Theme.Font_14, height: 60)
            let imageWidth = objectButton.imageView?.iwidth
            budgetButton.imageEdgeInsets = UIEdgeInsetsMake(0, lblWidth, 0, -lblWidth)
            budgetButton.titleEdgeInsets = UIEdgeInsetsMake(0, -imageWidth! - 10, 0, imageWidth! + 10)
            
            for btn in buttonArr {
                btn.addTarget(self, action: #selector(HeaderButtonView.buttonOnClick(_:)), for: .touchUpInside)
            }
            
            let _ = [objectButton, styleButton, occasionButton, budgetButton].map{self.addSubview($0)}
            
            for _ in 1...3 {
                let line = UIView()
                line.backgroundColor = Constant.Theme.Color17
                self.addSubview(line)
                linsArr.append(line)
            }
            
            let bottomLine = UIView()
            bottomLine.backgroundColor = Constant.Theme.Color9
            bottomLine.frame = CGRect(x: 0, y: Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 100 - 0.5, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: 0.5)
            self.addSubview(bottomLine)
            
            objectButton.snp_makeConstraints { (make) in
                let _ = make.left.equalTo(self).offset(Constant.ScreenSizeV2.MARGIN_30)
                let _ = make.top.equalTo(Constant.ScreenSizeV2.MARGIN_10 / 2)
                let _ = make.height.equalTo(90 * Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE)
                let _ = make.width.equalTo(buttonWidth)
                
            }
            
            linsArr[0].snp_makeConstraints { (make) in
                let _ = make.left.equalTo(objectButton.snp_right)
                let _ = make.top.equalTo(Constant.ScreenSizeV2.MARGIN_30)
                let _ = make.width.equalTo(0.5)
                let _ = make.height.equalTo(Constant.ScreenSizeV2.MARGIN_40)
            }
            
            styleButton.snp_makeConstraints { (make) in
                let _ = make.left.equalTo(linsArr[0].snp_right)
                let _ = make.height.equalTo(90 * Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE)
                let _ = make.top.equalTo(Constant.ScreenSizeV2.MARGIN_10 / 2)
                let _ = make.width.equalTo(buttonWidth)
            }
            
            linsArr[1].snp_makeConstraints { (make) in
                let _ = make.left.equalTo(styleButton.snp_right)
                let _ = make.top.equalTo(Constant.ScreenSizeV2.MARGIN_30)
                let _ = make.width.equalTo(0.5)
                let _ = make.height.equalTo(Constant.ScreenSizeV2.MARGIN_40)
            }
            
            occasionButton.snp_makeConstraints { (make) in
                let _ = make.left.equalTo(linsArr[1].snp_right)
                let _ = make.height.equalTo(90 * Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE)
                let _ = make.top.equalTo(Constant.ScreenSizeV2.MARGIN_10 / 2)
                let _ = make.width.equalTo(buttonWidth)
            }
            
            linsArr[2].snp_makeConstraints { (make) in
                let _ = make.left.equalTo(occasionButton.snp_right)
                let _ = make.top.equalTo(Constant.ScreenSizeV2.MARGIN_30)
                let _ = make.width.equalTo(0.5)
                let _ = make.height.equalTo(Constant.ScreenSizeV2.MARGIN_40)
            }
            
            budgetButton.snp_makeConstraints { (make) in
                let _ = make.left.equalTo(linsArr[2].snp_right)
                let _ = make.top.equalTo(Constant.ScreenSizeV2.MARGIN_10 / 2)
                let _ = make.height.equalTo(90 * Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE)
                let _ = make.width.equalTo(buttonWidth)
            }
            
        
        }
    }
    
    
    class BombBoxView: UIView {
        weak var delegate:BaseGiftChoiceGoodsUnitViewDelegate?
        fileprivate let mainView = UIView()
        let natureResultView:NatureResultView = NatureResultView()
        
        override init(frame: CGRect) {
            super.init(frame: frame)

            self.frame = CGRect(x: 0, y: Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * 100, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: Constant.ScreenSizeV2.SCREEN_HEIGHT - 64 - Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * 100)
            mainView.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: Constant.ScreenSizeV2.SCREEN_HEIGHT - 64 - Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * 100)
            mainView.backgroundColor = UIColor.black
            mainView.alpha = 0.3
            self.addSubview(mainView)
            self.addSubview(natureResultView)
            
//            self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(BombBoxView.onClick)))
            
            
        }
        
//        func onClick(){
//            self.delegate?.baseGiftChoiceMoneyUnitViewTouchdAction(nil)
//        }
        
        override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
            var touch:UITouch?
            for tempTouch in touches {
                   touch = tempTouch
                }
            let point = touch!.location(in: self)
            if point.y > natureResultView.ibottom {
                   self.delegate?.baseGiftChoiceMoneyUnitViewTouchdAction(nil)
              }
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    class NatureResultView: UIView, FFXRangeStepViewDelegate {
        
        fileprivate var viewStyle:GiftChoiceGoodsUnitViewActionType = GiftChoiceGoodsUnitViewActionType.object
        
        fileprivate var dataDic = [String:String]()
        
        fileprivate let dataSource:[String] = []
        fileprivate var linesCount = 1
        fileprivate var lineWidth:CGFloat = 0.0
        fileprivate var numberArr:[Int] = [-1]
        fileprivate var buttonArr:[UIButton] = []
        fileprivate var lineViewArr:[UIView] = []
        fileprivate var widthArr:[CGFloat] = []
        
        fileprivate let cancleBtn = UIButton(frame: CGRect(x: 10,y: 0,width: Constant.ScreenSize.SCREEN_WIDTH/2 - 15,height: 38))//取消按钮
        fileprivate let confirmeBtn = UIButton(frame: CGRect(x: Constant.ScreenSize.SCREEN_WIDTH/2 - 5 + 10,y: 0,width: Constant.ScreenSize.SCREEN_WIDTH/2 - 15,height: 38))//确定按钮

        
        fileprivate let priceScreenView = FFXRangeSlider(frame: CGRect(x: 10,y: 200,width: Constant.ScreenSize.SCREEN_WIDTH - 20,height: 52))//滑块
        fileprivate var moneyString = ["0", "200", "500", "1000", "2000", "2000+"]
        fileprivate let moneyDues = [0,200,500,1000,2000,99990000]
        weak var delegate:BaseGiftChoiceGoodsUnitViewDelegate?
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }

        func reloadDataAndStyle(_ style:GiftChoiceGoodsUnitViewActionType, dataDic:[String:String], data:[String]){

            self.dataDic = dataDic
            
            for subView in self.subviews {
                subView.removeFromSuperview()
            }
            
            linesCount = 1
            lineWidth = 0.0
            numberArr = [-1]
            buttonArr = []
            lineViewArr = []
            widthArr = []
            
            switch style {
            case .object:
                self.viewStyle = .object
                calculateLinesCountAndEveryLineNumber(data)
                break
            case .style:
                self.viewStyle = .style
                if data.count > 0 {
                
                    calculateLinesCountAndEveryLineNumber(data)
                }else{
                    self.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: 0)
                }
                break
            case .occasion:
                self.viewStyle = .occasion
                calculateLinesCountAndEveryLineNumber(data)
                break
            case .budget:
                self.viewStyle = .budget
                budgetView()
                break
//            default:
//                break
            }
  
        }
        
        
        func budgetView(){
            self.backgroundColor = UIColor.white
            self.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: 360 * Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE)
            let priceTitle = CustomLabel(font: Constant.common_F5_font, textColorHex: Constant.common_C7_color, textAlignment: NSTextAlignment.center)//价格
            priceTitle.text = "拖动滑块选择礼物价格区间"
            self.addSubview(priceTitle)
            priceTitle.snp_makeConstraints { (make) in
                let _ = make.top.equalTo(self).offset(Constant.ScreenSizeV2.MARGIN_30)
                let _ = make.centerX.equalTo(self)
            }
            
            priceScreenView.stepView.delegate = self
            priceScreenView.stepView.color = UIColor(rgba: Constant.common_C8_color)
            priceScreenView.stepView.activeColor = UIColor(rgba: Constant.common_C6_color)
            priceScreenView.steps = moneyString as NSArray
            priceScreenView.handleTintColor = UIColor(rgba: "#F9868E")//两个球的初始颜色
            priceScreenView.handleBorderWidth = 2//两个球表边宽度
            priceScreenView.handleBorderColor = UIColor(rgba: "#FFC3C3")//两个球描边
            priceScreenView.trackTintColor = UIColor(rgba: Constant.common_C10_color)//两个球开区间颜色
            priceScreenView.selectedTrackTintColor = UIColor(rgba: Constant.common_C1_color)//两个球闭区间颜色
            self.addSubview(priceScreenView)
            priceScreenView.snp_makeConstraints { (make) in
                let _ = make.top.equalTo(priceTitle.snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_40)
                let _ = make.left.equalTo(self).offset(Constant.ScreenSizeV2.MARGIN_30)
                let _ = make.right.equalTo(self).offset(-Constant.ScreenSizeV2.MARGIN_30)
            }
            
            cancleBtn.setTitle("重置", for: UIControlState())
            cancleBtn.setTitleColor(UIColor(rgba: Constant.common_C6_color), for: UIControlState())
            cancleBtn.titleLabel?.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
            
            cancleBtn.layer.cornerRadius = 3
            cancleBtn.layer.borderWidth = 1
            cancleBtn.layer.borderColor = UIColor(rgba: Constant.common_C7_color).cgColor
            
            
            confirmeBtn.setTitle("确定", for: UIControlState())
            confirmeBtn.setTitleColor(UIColor(rgba: Constant.common_C1_color), for: UIControlState())
            confirmeBtn.titleLabel?.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
            
            confirmeBtn.layer.cornerRadius = 3
            confirmeBtn.layer.borderWidth = 1
            confirmeBtn.layer.borderColor = UIColor(rgba: Constant.common_C1_color).cgColor
            
            cancleBtn.addTarget(self, action: #selector(NatureResultView.chooseBtnAction(_:)), for: UIControlEvents.touchUpInside)
            confirmeBtn.addTarget(self, action: #selector(NatureResultView.chooseBtnAction(_:)), for: UIControlEvents.touchUpInside)

            self.addSubview(cancleBtn)
            self.addSubview(confirmeBtn)
            let buttonWidth = (Constant.ScreenSizeV2.SCREEN_WIDTH - Constant.ScreenSizeV2.MARGIN_60) / 2
            cancleBtn.snp_makeConstraints { (make) in
                let _ = make.top.equalTo(priceScreenView.snp_bottom).offset(Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 90)
                let _ = make.left.equalTo(self).offset(Constant.ScreenSizeV2.MARGIN_20)
                let _ = make.bottom.equalTo(self).offset(-Constant.ScreenSizeV2.MARGIN_20)
                let _ = make.height.equalTo(Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 80)
                let _ = make.width.equalTo(buttonWidth)
            }
            confirmeBtn.snp_makeConstraints { (make) in
                let _ = make.top.equalTo(cancleBtn)
                let _ = make.left.equalTo(cancleBtn.snp_right).offset(Constant.ScreenSizeV2.MARGIN_20)
                let _ = make.bottom.equalTo(self).offset(-Constant.ScreenSizeV2.MARGIN_20)
                let _ = make.height.equalTo(Constant.ScreenSizeV2.SCREEN_HEIGHT_SCALE * 80)
                let _ = make.width.equalTo(buttonWidth)
            }
            
            
            print(priceScreenView.fromIndex,priceScreenView.toIndex)
            if priceScreenView.fromIndex != UInt(NSNotFound) && priceScreenView.toIndex != UInt(NSNotFound) {
                let tempIndexSet = NSMutableIndexSet()
                tempIndexSet.add(Int(priceScreenView.fromIndex))
                tempIndexSet.add(Int(priceScreenView.toIndex))
                self.moneyDuesIndex = tempIndexSet as IndexSet!
                priceScreenView.stepView.activeIndexes = self.moneyDuesIndex
            }
            
        }
        
        var moneyDuesIndex:IndexSet!
        
        func didSelectedIndexSet(_ activeIndexes: IndexSet!) {

            self.moneyDuesIndex = activeIndexes
                

        }
        
        func chooseBtnAction(_ button:UIButton) {
            if button == confirmeBtn
            {
                if let min = self.moneyDuesIndex.first, let max = self.moneyDuesIndex.last {
                    let minMoney:Int = self.moneyDues[min]
                    let maxMoney:Int = self.moneyDues[max]

                    self.delegate?.baseGiftChoiceMoneyUnitViewTouchdAction([minMoney, maxMoney])
                }else{
                    self.delegate?.baseGiftChoiceMoneyUnitViewTouchdAction(nil)
                }
            }else{
                self.priceScreenView.resetHandle()
                self.moneyDuesIndex = IndexSet(integersIn: NSMakeRange(0,(moneyDues.count)).toRange()!)
                self.delegate?.baseGiftChoiceMoneyUnitViewTouchdAction([0, 9900000])
            }

        }
        
        func calculateLinesCountAndEveryLineNumber(_ dataArr:[String]) {
            
            for i in 0..<dataArr.count {
                let button = createBtn(dataArr[i])
                button.tag = 1200 + i
                
                switch self.viewStyle {
                case .object:
                    if dataArr[i] == self.dataDic["name"] {
                        button.isSelected = true
                        button.layer.borderWidth = 0
                        button.backgroundColor = Constant.Theme.Color1
                    }
                    break
                case .style:
                    if dataArr[i] == self.dataDic["style"] {
                        button.isSelected = true
                        button.layer.borderWidth = 0
                        button.backgroundColor = Constant.Theme.Color1
                    }
                    break
                case .occasion:
                    if dataArr[i] == self.dataDic["occasion"] {
                        button.isSelected = true
                        button.layer.borderWidth = 0
                        button.backgroundColor = Constant.Theme.Color1
                    }
                    break
                default:
                    break
                }
                let widthButton = CalculateHeightOrWidth.getLabOrBtnWidth((button.titleLabel?.text)! as NSString, font: Constant.Theme.Font_14, height: Constant.ScreenSizeV2.MARGIN_60) + Constant.ScreenSizeV2.MARGIN_68
                lineWidth = lineWidth + widthButton + Constant.ScreenSizeV2.MARGIN_40
                buttonArr.append(button)
                widthArr.append(widthButton)
                if lineWidth > Constant.ScreenSizeV2.SCREEN_WIDTH - Constant.ScreenSizeV2.MARGIN_40 {
                    linesCount += 1
                    numberArr.append(i - 1)
                    
                    lineWidth = widthButton + Constant.ScreenSizeV2.MARGIN_40
                }
                
            }
            numberArr.append(dataArr.count - 1)
            
            compositionButton()
        }
        
        func compositionButton() {
            
            
            self.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE * (80 + CGFloat(linesCount) * 90))
            self.backgroundColor = UIColor.white
            
            for i in 0...linesCount - 1 {
                let lineView = UIView()
                lineViewArr.append(lineView)
                self.addSubview(lineView)
                for j in numberArr[i] + 1...numberArr[i + 1] {
                    lineView.addSubview(buttonArr[j])

                    if j == numberArr[i] + 1 {
                        
                        buttonArr[j].snp_makeConstraints(closure: { (make) in
                            let _ = make.top.equalTo(lineView)
                            let _ = make.left.equalTo(lineView)
                            let _ = make.width.equalTo(widthArr[j])
                            let _ = make.bottom.equalTo(lineView)
                            let _ = make.height.equalTo(Constant.ScreenSizeV2.MARGIN_60)
                        })
                        
                        
                    }else if j == numberArr[i + 1] {
                        buttonArr[j].snp_makeConstraints(closure: { (make) in
                            let _ = make.top.equalTo(lineView)
                            let _ = make.left.equalTo(buttonArr[j - 1].snp_right).offset(Constant.ScreenSizeV2.MARGIN_40)
                            let _ = make.right.equalTo(lineView)
                            let _ = make.width.equalTo(widthArr[j])
                            let _ = make.bottom.equalTo(lineView)
                            let _ = make.height.equalTo(Constant.ScreenSizeV2.MARGIN_60)
                        })
                    }else {
                        buttonArr[j].snp_makeConstraints(closure: { (make) in
                            let _ = make.top.equalTo(lineView)
                            let _ = make.left.equalTo(buttonArr[j - 1].snp_right).offset(Constant.ScreenSizeV2.MARGIN_40)
                            let _ = make.width.equalTo(widthArr[j])
                            let _ = make.bottom.equalTo(lineView)
                            let _ = make.height.equalTo(Constant.ScreenSizeV2.MARGIN_60)
                            
                        })
                    }
                    
                    
                    
                }
                
                if lineView.subviews.count == 1 {
                    lineView.subviews[0].snp_makeConstraints(closure: { (make) in
                        let _ = make.top.equalTo(lineView)
                        let _ = make.left.equalTo(lineView)
                        let _ = make.bottom.equalTo(lineView)
                        let _ = make.right.equalTo(lineView)
                        let _ = make.height.equalTo(Constant.ScreenSizeV2.MARGIN_60)
                        
                    })
                }
                
                
                if i == 0 {
                    lineView.snp_makeConstraints(closure: { (make) in
                        let _ = make.top.equalTo(self).offset(Constant.ScreenSizeV2.MARGIN_40)
                        let _ = make.centerX.equalTo(self)
                    })
                }else {
                    lineView.snp_makeConstraints(closure: { (make) in
                        let _ = make.top.equalTo(lineViewArr[i - 1].snp_bottom).offset(Constant.ScreenSizeV2.MARGIN_30)
                        let _ = make.centerX.equalTo(self)
                    })
                }
            }
            
        }
        
        func onClick(_ button:UIButton) {
            let index = self.viewStyle.rawValue
            if button.isSelected {
                button.isSelected = false
                button.backgroundColor = UIColor.white
                button.layer.borderWidth = 0.5
                self.delegate?.baseGiftChoiceBombBoxViewUnitViewTouchdAction(GiftChoiceGoodsActiontype[index], content: nil)
            }else{
                for btn in buttonArr{
                    btn.isSelected = false
                    btn.backgroundColor = UIColor.white
                    btn.layer.borderWidth = 0.5
                }
                button.isSelected = true
                button.layer.borderWidth = 0
                button.backgroundColor = Constant.Theme.Color1
                
                let num = button.tag - 1200
                
                
                
                self.delegate?.baseGiftChoiceBombBoxViewUnitViewTouchdAction(GiftChoiceGoodsActiontype[index], content: num)
            }
            
            
        }
        

        
        func createBtn(_ title: String) -> UIButton {
            let btn = UIButton()
            //            btn.backgroundColor = UIColor.whiteColor()
            btn.setTitle(title, for: UIControlState())
            btn.setTitleColor(Constant.Theme.Color17, for: UIControlState())
            btn.setTitleColor(UIColor.white, for: UIControlState.selected)
            btn.titleLabel?.font = Constant.Theme.Font_14
            btn.layer.cornerRadius = Constant.ScreenSizeV2.MARGIN_30
            btn.layer.borderWidth = 0.5
            btn.addTarget(self, action: #selector(NatureResultView.onClick(_:)), for: UIControlEvents.touchUpInside)
            btn.layer.borderColor = Constant.Theme.Color17.cgColor
            btn.contentEdgeInsets.left = 34 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
            btn.contentEdgeInsets.right = 34 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
            
            return btn
        }
        
    }

    
   
}

//MARK:GiftChoiceOptionViewController 点击代理
protocol BaseGiftChoiceGoodsUnitViewDelegate:NSObjectProtocol {
    func baseGiftChoiceGoodsUnitViewTouchdAction(_ actionType:String,content:UIButton)
    func baseGiftChoiceBombBoxViewUnitViewTouchdAction(_ actionType:String,content:Int?)
    func baseGiftChoiceMoneyUnitViewTouchdAction(_ content:[Int]?)
}








