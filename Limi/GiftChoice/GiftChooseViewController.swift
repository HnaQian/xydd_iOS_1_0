//
//  GiftChooseViewController.swift
//  Limi
//
//  Created by ZhiQiangMi on 16/3/31.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//选礼

import UIKit

class GiftChooseViewController: BaseViewController,UICollectionViewDelegate,UICollectionViewDataSource,GiftChooseCollectionViewFooterDelegate {
    
    
    fileprivate var giftModeAry = [GiftChooseRootModel]()
    
    lazy var collectionView:UICollectionView = {
        let cvFrame  = CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: Constant.ScreenSize.SCREEN_HEIGHT - MARGIN_49 + 10)
        var cv = UICollectionView(frame: cvFrame, collectionViewLayout: self.flowLayout)
        cv.delegate = self
        cv.dataSource = self
        cv.bounces = true
        cv.alwaysBounceVertical = true
        cv.autoresizingMask = [UIViewAutoresizing.flexibleHeight, UIViewAutoresizing.flexibleWidth]
        cv.register(GiftChooseCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        
        //注册header
        cv.register(GiftChooseCollectionViewHeader.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "Header")
        
        //注册footer
        cv.register(GiftChooseCollectionViewFooter.self, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "Footer")
        
        //        cv.backgroundColor = UIColor(red: 230.0/255, green: 230.0/255, blue: 230.0/255, alpha: 1)
        cv.backgroundColor = UIColor.white
        return cv
    }()
    
    lazy var flowLayout:UICollectionViewFlowLayout = {
        var flow = UICollectionViewFlowLayout()
        flow.sectionInset = UIEdgeInsetsMake(MARGIN_10, MARGIN_10, MARGIN_10, MARGIN_10)
        flow.headerReferenceSize = CGSize(width: Constant.ScreenSize.SCREEN_WIDTH,height: 30)
        flow.footerReferenceSize = CGSize(width: Constant.ScreenSize.SCREEN_WIDTH,height: 62)
        return flow
    }()
    
    fileprivate var isSuccessLoadData : Bool = false
    //    private var isExpendArr:[Bool] = [Bool]()
    //    private let sectionFixNum = 6
    override func viewDidLoad() {
        super.viewDidLoad()
        self.gesturBackEnable = false
        self.navigationItem.title = "选礼"
        self.navigationItem.leftBarButtonItem = nil
        
        self.view.addSubview(self.collectionView)
        self.collectionView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.view)
            let _ = make.left.equalTo(self.view)
            let _ = make.right.equalTo(self.view)
            let _ = make.bottom.equalTo(self.view).offset(-MARGIN_49 + MARGIN_10)
        }
        
        self.collectionView.addHeaderRefresh { [weak self]() -> Void in
            self?.fetchDataFromNet()
        }
        self.fetchData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        if self.giftModeAry.count == 0 || self.isSuccessLoadData == false
        {
            self.collectionView.beginHeaderRefresh()
        }

        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fetchData() {
        if let homeDB = CoreDataManager.shared.getDBManager(Constant.CoreDataType.homeAAGiftChoiceType),
            let db = homeDB as? HomeAAGiftChoice,
            let dataSource = db.giftChoiceLayout as? [[String:AnyObject]]
        {
            let _ = dataSource.map{self.giftModeAry.append(GiftChooseRootModel($0))}
            
            self.collectionView.reloadData()
        }else {
             self.fetchDataFromNet()
        }
    }
    
    func fetchDataFromNet() {
        let _ = LMRequestManager.requestByDataAnalyses(HTTPMethod.get,context:self.view, URLString: Constant.JTAPI.gift_page,isShowErrorStatuMsg:true, successHandler: {data, status, msg in
            if self.collectionView.isHeaderRefreshing == true{
                self.isSuccessLoadData = true
                self.collectionView.endHeaderRefresh()
            }
            
            if status != 200 {return}
            
            self.giftModeAry.removeAll()
            
            let  jdata = JSON(data as AnyObject)
            
            if let layout = jdata["data"]["layout"].array
            {
                
                print(layout)
                if layout.count > 0
                {
                    
                    let _ = layout.map{self.giftModeAry.append(GiftChooseRootModel($0.dictionaryObject))}
                    
                    //存储数据
                    if let homeDB = CoreDataManager.shared.getDBManager(Constant.CoreDataType.homeAAGiftChoiceType),
                        let db = homeDB as? HomeAAGiftChoice
                    {
                        db.giftChoiceLayout = jdata["data"]["layout"].arrayObject as NSObject?
                        
                        CoreDataManager.shared.save()
                    }
                    
                    self.collectionView.reloadData()
                }
            }
        }){Void in
            
            if self.collectionView.isHeaderRefreshing == true{
                self.collectionView.endHeaderRefresh()
            }
        }
        
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int{
        return self.giftModeAry.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        
        return self.giftModeAry[section].rows
        
        //        if self.isExpendArr[section] == true{
        //            if let childern = self.giftModeAry[section].children{
        //                return childern.count
        //            }
        //        }
        //        else{
        //            if let childern = self.giftModeAry[section].children{
        //                if childern.count > sectionFixNum{
        //                    return sectionFixNum
        //                }
        //                else{
        //                    return childern.count
        //                }
        //            }
        //        }
        //        return 0
    }
    
    
    func spreadAction(_ section:Int){
        self.giftModeAry[section].shouldSpread = !self.giftModeAry[section].shouldSpread
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! GiftChooseCollectionViewCell
        
        cell.reloadData(self.giftModeAry[(indexPath as NSIndexPath).section].children![(indexPath as NSIndexPath).row])
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize{
        //384  260
        let width:CGFloat = (Constant.ScreenSize.SCREEN_WIDTH - MARGIN_10 * 3)/2.0;
        let height:CGFloat = width * 260.0/384;
        
        return CGSize(width: width, height: height)
    }
    
    
    func giftChooseCollectionViewFooterDidSelected(_ section: Int) {
        if let name = self.giftModeAry[section].attributes?.titleAry?[0].name{
        Statistics.count(Statistics.GiftChoice.selectgift_more_click, andAttributes: ["name":name])
        }
        
        self.giftModeAry[section].shouldSpread = !self.giftModeAry[section].shouldSpread
        collectionView.reloadSections(IndexSet(integer: section))
    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView{
        
        precondition(UICollectionElementKindSectionHeader == kind || UICollectionElementKindSectionFooter == kind, "Unexpected element kind")
        
        if UICollectionElementKindSectionHeader == kind{
            let headerView : GiftChooseCollectionViewHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "Header", for: indexPath) as! GiftChooseCollectionViewHeader
            
            if let ary = self.giftModeAry[(indexPath as NSIndexPath).section].attributes!.titleAry{
                if ary.count > 0{
                    headerView.headerLabel.text = self.giftModeAry[(indexPath as NSIndexPath).section].attributes!.titleAry![0].name
                }
            }
            return headerView

        }
        
        let footerView : GiftChooseCollectionViewFooter  = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "Footer", for: indexPath) as! GiftChooseCollectionViewFooter
        footerView.refreshUI((indexPath as NSIndexPath).section, delegate: self, didSpread: giftModeAry[(indexPath as NSIndexPath).section].shouldSpread)
        return footerView
//        switch kind {
//            
//        case UICollectionElementKindSectionHeader:
//            
//            let headerView : GiftChooseCollectionViewHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "Header", for: indexPath) as! GiftChooseCollectionViewHeader
//            
//            if let ary = self.giftModeAry[(indexPath as NSIndexPath).section].attributes!.titleAry{
//                if ary.count > 0{
//                    headerView.headerLabel.text = self.giftModeAry[(indexPath as NSIndexPath).section].attributes!.titleAry![0].name
//                }
//            }
//            
//            return headerView
//            
//        case UICollectionElementKindSectionFooter:
//            let footerView : GiftChooseCollectionViewFooter  = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "Footer", for: indexPath) as! GiftChooseCollectionViewFooter
//            footerView.refreshUI((indexPath as NSIndexPath).section, delegate: self, didSpread: giftModeAry[(indexPath as NSIndexPath).section].shouldSpread)
//            return footerView
//            
//        default:
//            
//            assert(false, "Unexpected element kind")
//        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        
        
        Statistics.count(Statistics.GiftChoice.selectgift_click, andAttributes: ["mode_title": self.giftModeAry[(indexPath as NSIndexPath).section].children![(indexPath as NSIndexPath).row].attributes!.title])

        let giftListVC = GiftChoiceListViewController.newGiftChoiceListViewController(self.giftModeAry[(indexPath as NSIndexPath).section].children![(indexPath as NSIndexPath).row])

        giftListVC.hidesBottomBarWhenPushed = true
        

        self.navigationController?.pushViewController(giftListVC, animated: true)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
