//
//  GiftChoiceOptionViewController.swift
//  Limi
//
//  Created by 千云锋 on 16/8/2.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit


enum GiftChoiceUnitViewActionType:NSInteger {
    case natureView = 0
    case girlOrBoyView
    case agesView
    case constellationView
    case occasionView
    case toolBar
}

let GiftActionKeys = ["NatureView", "GirlOrBoyView", "AgesView", "ConstellationView", "OccasionView", "ToolBar"]

class GiftChoiceOptionViewController: BaseViewController, BaseGiftChoiceUnitViewDelegate {

    fileprivate var dataScrousName:[String] = []
    fileprivate var dataSourceImage:[String] = []
    fileprivate var dataSourcePeople:[GiftChoicePeopleModel] = []
    fileprivate var dataSourceSupplement = GiftChoiceSupplementModel()
    fileprivate var giftChoiceDic = [String:String]()
    fileprivate var _number = 0
    fileprivate var number:Int{return _number}
    class func giftChoiceOption(_ dataSourcePeople:[GiftChoicePeopleModel], dataSourceSupplement:GiftChoiceSupplementModel, number:Int) ->GiftChoiceOptionViewController {
        
        let giftChoiceOption = GiftChoiceOptionViewController()
        
        giftChoiceOption.dataSourcePeople = dataSourcePeople
        for model in dataSourcePeople {
            giftChoiceOption.dataScrousName.append(model.name)

            giftChoiceOption.dataSourceImage.append(model.img)
            

        }
        
        giftChoiceOption.dataSourceSupplement = dataSourceSupplement
        giftChoiceOption._number = number
        return giftChoiceOption
    }
    
    lazy fileprivate var scrollView:UIScrollView = {
        var scroll = UIScrollView()
        scroll.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: Constant.ScreenSizeV2.SCREEN_HEIGHT - 64 - 90 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
        scroll.backgroundColor = UIColor.white
        scroll.showsHorizontalScrollIndicator = false
        scroll.showsVerticalScrollIndicator = false
        
        return scroll
    }()

    //ta的个性
    fileprivate let headerImageView:HeaderImageView = HeaderImageView()
    
    //补充头
    fileprivate let supplementView:SupplementView = SupplementView()
    
    //性别
    fileprivate let girlOrBoyView:GirlOrBoyView = GirlOrBoyView()
    
    //年代
    fileprivate let agesView:AgesView = AgesView()
    
    //星座
    fileprivate let constellationView:ConstellationView = ConstellationView()
    
    //场合
    fileprivate let occasionView:OccasionView = OccasionView()
    
    //toolbar
    fileprivate let toolBar:ToolBar = ToolBar()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.giftChoiceDic.updateValue(self.dataScrousName[number], forKey: "name")
        let model:[GiftGexinModel] = self.dataSourcePeople[number].ge_xin

        if model.count != 0 {
            self.giftChoiceDic.updateValue(model[0].name, forKey: "style")
        }
        
        print(self.giftChoiceDic)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "选礼"
        let imgStr = self.dataSourceImage[self.number]
        let nameStr = self.dataScrousName[self.number]
        let temp = self.dataSourcePeople[self.number]
        let gexinArr = temp.ge_xin
//        print(gexinArr[0])
        var gexinNameArr:[String] = []

        for model:GiftGexinModel in gexinArr {
            gexinNameArr.append(model.name)
        }

        let dataSource = [imgStr, nameStr, gexinNameArr] as [Any]
        self.headerImageView.natureView.delegate = self
        self.headerImageView.reloadData(dataSource as [AnyObject])
        self.headerImageView.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: Constant.ScreenSizeV2.SCREEN_WIDTH)
        self.scrollView.addSubview(headerImageView)
        
        supplementView.frame = CGRect(x: 0, y: headerImageView.frame.size.height, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: 130 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
        self.scrollView.addSubview(supplementView)
        
        self.girlOrBoyView.delegate = self
        self.girlOrBoyView.reloadData(self.dataSourceSupplement.sex)
        self.girlOrBoyView.frame = CGRect(x: 0, y: headerImageView.frame.size.height + 130 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: 144 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
        self.scrollView.addSubview(girlOrBoyView)
        
        self.agesView.delegate = self
        self.agesView.reloadData(self.dataSourceSupplement.age)
        self.agesView.frame = CGRect(x: 0, y: headerImageView.frame.size.height + 274 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: 100 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
        self.scrollView.addSubview(agesView)
        
        self.constellationView.delegate = self
        self.constellationView.reloadData(self.dataSourceSupplement.xin_zhuo)
        self.constellationView.frame = CGRect(x: 0, y: headerImageView.frame.size.height + 374 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: 200 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
        self.scrollView.addSubview(constellationView)
        
        self.occasionView.delegate = self
        self.occasionView.reloadData(self.dataSourceSupplement.chang_he)
        self.occasionView.frame.origin = CGPoint(x: 0, y: Constant.ScreenSizeV2.SCREEN_WIDTH + 574 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
        self.scrollView.addSubview(occasionView)
        
        self.scrollView.contentSize = CGSize(width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: headerImageView.frame.height + supplementView.frame.size.height + girlOrBoyView.frame.size.height + agesView.frame.size.height + constellationView.frame.size.height + occasionView.frame.size.height)
        self.view.addSubview(self.scrollView)
        
        self.view.addSubview(toolBar)
        
        
       toolBar.delegate = self

        // Do any additional setup after loading the view.
    }
    override func backItemAction() {
        
        let _ = self.navigationController?.popViewController(animated: false)
    }
    


    func baseGiftChoiceUnitViewTouchdAction(_ actionType: GiftChoiceUnitViewActionType, content: [String : String]?) {
        let index = actionType.rawValue
        let key:String = GiftActionKeys[index]

        switch actionType {
        case .natureView:
            
            if let stylr =  content{
                self.giftChoiceDic.updateValue(stylr[key]!, forKey: "style")
                Statistics.count(Statistics.GiftChoice.selectgift_label_click, andAttributes: stylr)
            }
            
            
//            self.giftChoiceDic.updateValue(self.dataScrousName[number], forKey: "name")
            
            break
        case .girlOrBoyView:
            if content == nil {
                self.giftChoiceDic.removeValue(forKey: "sex")
            }else{
                if let sex = content {
                    self.giftChoiceDic.updateValue(sex[key]!, forKey: "sex")
                    Statistics.count(Statistics.GiftChoice.selectgift_sex_click, andAttributes: sex)
                }
                
            }
            
            break
        case .agesView:
            if content == nil {
                self.giftChoiceDic.removeValue(forKey: "ages")
            }else{
                if let age = content {
                    self.giftChoiceDic.updateValue(age[key]!, forKey: "ages")
                    Statistics.count(Statistics.GiftChoice.selectgift_old_click, andAttributes: age)
                }
                
            }
            
            break
        case .constellationView:
            if content == nil {
                self.giftChoiceDic.removeValue(forKey: "xinzuo")
            }else{
                if let xinzuo = content {
                    self.giftChoiceDic.updateValue(xinzuo[key]!, forKey: "xinzuo")
                    Statistics.count(Statistics.GiftChoice.selectgift_constellation_click, andAttributes: xinzuo)
                }
                
            }
            
            break
        case .occasionView:
            if content == nil {
                self.giftChoiceDic.removeValue(forKey: "occasion")
            }else{
                if let occasion = content {
                    self.giftChoiceDic.updateValue(occasion[key]!, forKey: "occasion")
                    Statistics.count(Statistics.GiftChoice.selectgift_occasion_click, andAttributes: occasion)
                }
                
            }
            
            break
        case .toolBar:
            Statistics.count(Statistics.GiftChoice.selectgift_finish_click)
            let vc = GiftChoiceGoodsViewController.giftChoiceGoods(self.dataSourcePeople, dataSourceSupplement: self.dataSourceSupplement, dataOption: self.giftChoiceDic, number: self.number)
            self.navigationController?.pushViewController(vc, animated: true)
            break
//        default:
//            break
        }
        print(self.giftChoiceDic)
    }
    
}





