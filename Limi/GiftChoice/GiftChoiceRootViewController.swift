//
//  GiftChoiceRootViewController.swift
//  Limi
//
//  Created by 千云锋 on 16/8/2.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class GiftChoiceRootViewController: BaseViewController{

    
    //存放数据的数组
    fileprivate var dataScrousName:[String] = []
    fileprivate var dataSourceImage:[String] = []
    fileprivate var dataSourcePeople:[GiftChoicePeopleModel] = []
    fileprivate var dataSourceSupplement = GiftChoiceSupplementModel()
    fileprivate var isAnimation:Bool = true
    fileprivate let scrollView:UIScrollView = {
        var scroll = UIScrollView()
        scroll.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: Constant.ScreenSizeV2.SCREEN_HEIGHT - 64 - 49)
        scroll.backgroundColor = UIColor.white
        scroll.showsHorizontalScrollIndicator = false
        scroll.showsVerticalScrollIndicator = false

        return scroll
    }()

    
    fileprivate let titleLabel:UILabel = {
        let lbl = UILabel()
        lbl.text = "送给谁"
        lbl.font = Constant.Theme.Font_15
        lbl.textAlignment = NSTextAlignment.center
        lbl.frame = CGRect(x: 0, y: Constant.ScreenSizeV2.MARGIN_30, width: Constant.ScreenSizeV2.MARGIN_150, height: Constant.ScreenSizeV2.MARGIN_20)
        return lbl
    }()
    //存放所有按钮的数组
    fileprivate var viewArr = [UIButton]()
    //存放所有按钮初始位置的数组
    fileprivate var initFrameArr = [CGRect]()
    //存放所有按钮结束位置的数组
    fileprivate var endFrameArr = [CGRect]()
    
    static let SCREEN = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
    fileprivate let viewW = SCREEN * 216
    fileprivate let viewH = SCREEN * 256
    
    func setupUI(){
        self.view.addSubview(self.scrollView)
        self.titleLabel.center.x = self.view.center.x
        self.scrollView.addSubview(titleLabel)
        var scrollH = dataScrousName.count / 3
        if dataScrousName.count % 3 != 0 {
            scrollH += 1
        }
        
        self.scrollView.contentSize = CGSize(width: Constant.ScreenSizeV2.SCREEN_WIDTH, height: CGFloat(scrollH) * (viewH + Constant.ScreenSizeV2.MARGIN_40) + Constant.ScreenSizeV2.MARGIN_64 + Constant.ScreenSizeV2.MARGIN_15)
        //创建起始位置
        for i in 1...dataScrousName.count {
            let x = (i - 1) % 3
            let frame:CGRect = CGRect(x: CGFloat(x) * viewW, y: Constant.ScreenSizeV2.SCREEN_HEIGHT, width: viewW, height: viewH)
            initFrameArr.append(frame)
        }
        
        //创建结束位置
        for i in 1...dataScrousName.count {
            let x = (i - 1) % 3
            let y = (i - 1) / 3
            let frame:CGRect = CGRect(x: Constant.ScreenSizeV2.MARGIN_30 + CGFloat(x) * (viewW + Constant.ScreenSizeV2.MARGIN_20), y: Constant.ScreenSizeV2.MARGIN_64 + Constant.ScreenSizeV2.MARGIN_15 + CGFloat(y) * (viewH + Constant.ScreenSizeV2.MARGIN_40), width: viewW, height: viewH)
            endFrameArr.append(frame)
        }

        //创建uiview
        for i in 0...dataScrousName.count - 1 {
            
            let btn = UIButton()
            let imageView = UIImageView()
            let imageScale : String = self.dataSourceImage[i] + "?imageView2/0/w/" + "\(Int(viewW * Constant.ScreenSizeV1.SCALE_SCREEN))" + "/h/" + "\(Int(viewW * Constant.ScreenSizeV1.SCALE_SCREEN))"
            
            imageView.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "Default_giftChoice"))
            btn.setTitle(self.dataScrousName[i], for: UIControlState())
            btn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, -220 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE, 0)
            btn.setTitleColor(Constant.Theme.Color6, for: UIControlState())
            btn.titleLabel?.font = Constant.Theme.Font_14
            btn.tag = i + 777
            btn.addTarget(self, action: #selector(GiftChoiceRootViewController.onClick(_:)), for: .touchUpInside)
            

            imageView.frame = CGRect(x: 0, y: 0, width: viewW, height: viewW)
            if isAnimation {
                btn.frame = initFrameArr[i]
                let delayInSeconds:Double = Double(i + 1) * (1 / 7);
                let delay:CFTimeInterval = delayInSeconds + CACurrentMediaTime();
                let hyPop = HyPopMenuView()
                hyPop.springBounce = 6;
                hyPop.springspeed = 15;
                hyPop.startTheAnimation(fromValue: initFrameArr[i], toValue: endFrameArr[i], delay: delay, object: btn, completionBlock: nil, hideDisplay: false)
            
            }else{
                btn.frame = endFrameArr[i]
            }
            self.scrollView.addSubview(btn)
            btn.addSubview(imageView)
        }
        
        self.scrollView.addHeaderRefresh {
            [weak self]() -> Void in

            self?.isAnimation = false
            self?.fetchDataSource()
        }

    }
    
    func onClick(_ button:UIButton){
        
        let number = button.tag - 777
        Statistics.count(Statistics.GiftChoice.selectgift_who_click,andAttributes: ["name":self.dataSourcePeople[number].name])
        
        let vc = GiftChoiceOptionViewController.giftChoiceOption(self.dataSourcePeople, dataSourceSupplement: dataSourceSupplement, number: number)
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.radialPushViewController(vc,duration:0.3,startFrame: button.frame,transitionCompletion: { () -> Void in
   
        })


    
    }
    
    func fetchDataSource(){

        let _ = LMRequestManager.requestByDataAnalyses(context: self.view, URLString: Constant.JTAPI.choosegift_getFilterItems, isNeedUserTokrn: true,isShowErrorStatuMsg:true,isNeedHud : true, successHandler: { (data, status, msg) in
            self.scrollView.endHeaderRefresh()
            
            for view in (self.scrollView.subviews) {
                view.removeFromSuperview()
            }
            self.dataScrousName.removeAll()
            self.dataSourceImage.removeAll()
            
            
            var giftChoicePeopleArr = [GiftChoicePeopleModel]()
            var giftChoiceSupplement = GiftChoiceSupplementModel()
            
            if let tempData:[String:AnyObject] = data["data"] as? [String:AnyObject],
                let list = tempData["shen_fen_to_ge_xin"] as? [[String:AnyObject]],
                let dataList = tempData["bu_chong"] as? [String:AnyObject]
            {
                
                let _ = list.map{giftChoicePeopleArr.append(GiftChoicePeopleModel($0))}
                print(giftChoicePeopleArr)
                self.dataSourcePeople = giftChoicePeopleArr
                giftChoiceSupplement = GiftChoiceSupplementModel(dataList)
            }
            
            for list in giftChoicePeopleArr {
                self.dataScrousName.append(list.name)
                self.dataSourceImage.append(list.img)
            }
            
            self.dataSourceSupplement = giftChoiceSupplement
            self.setupUI()
            
        }){Void in
            
            self.scrollView.endHeaderRefresh()
        }
    }


    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UserDefaults.standard.set(2, forKey: "come_from")
        UserDefaults.standard.synchronize()
        
        if self.dataScrousName.count <= 0 {
            self.scrollView.offsetY = 0
            for view in scrollView.subviews {
                view.removeFromSuperview()
            }
            scrollView.removeFromSuperview()
            fetchDataSource()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.gesturBackEnable = false
        self.navigationItem.title = "选礼"
        self.navigationItem.leftBarButtonItem = nil
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
       
        LMRequestManager.shareRequestManager().terminateNetWorkTask()
    }

}











