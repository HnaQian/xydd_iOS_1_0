//
//  LMRequestManager.swift
//  Limi
//
//  Created by guo chen on 15/11/23.
//  Copyright © 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class LMRequestManager: NSObject
{
    class func shareRequestManager() ->LMRequestManager
    {
        struct Static 
        {
            static var instance : LMRequestManager? = nil
            
            static var once : dispatch_once_t = 0
        }
        
        dispatch_once(&Static.once) {
            
            Static.instance = LMRequestManager() 
        }
        
        return Static.instance!
    }
    
    
    
    class func request(method: Method,_ URLString: URLStringConvertible,parameters: [String: AnyObject]? = nil,encoding: ParameterEncoding = .URL,headers: [String: String]? = nil,completionBlock: (NSURLRequest?, NSHTTPURLResponse?, Result<AnyObject>) -> Void) 
    {
        let  request = Manager.sharedInstance.request(
            method,
            URLString,
            parameters: parameters,
            encoding: encoding,
            headers: headers
        )
        
        request.responseJSON(completionHandler: {request, response, result in
            
            completionBlock(request, response, result)
            
            switch result
            {  
                
            case .Success(let data):
                
                if let token = (data["data"] as? [String: AnyObject])?["token"] as? String
                {
                    Account.updateAccountToken(token)
                }
                
            case .Failure( _, _):

                if Reachability.reachabilityForInternetConnection()?.isReachable() == false
                {
                    ESwiftToast.show("网络连接已断开，请检查网络", duration: ESwiftToast.DEFAULT_DISPLAY_DURATION, position: .center)
                }
                else
                {
                    ESwiftToast.show(Constant.NETWORK_ERR, duration: ESwiftToast.DEFAULT_DISPLAY_DURATION, position: .center)
                }
                
            }
        })   
    }
    
    
}
