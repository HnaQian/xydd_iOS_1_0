//
//  AppDelegate.swift
//  Limi
//
//  Created by ZhiQiangMi on 15/3/23.
//  Copyright  (c) 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit
import CoreData
import AddressBook

var addressBookBool = false

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate,GeTuiSdkDelegate
{
    //text分支
    var window: UIWindow?
    var currentViewController: UIViewController?
        
    var loginType : Int = -1
    
    var isLoginSuccess : Bool = false
    
    fileprivate var adressBook:ABAddressBook? = nil
    fileprivate var hasRegister:Bool = false
    
    fileprivate let loginDialog = LoginDialog()
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        
        UINavigationBar.appearance().barStyle = UIBarStyle.black
        let navigationBarAppearace = UINavigationBar.appearance()
        navigationBarAppearace.tintColor = UIColor(rgba: Constant.titlebar_color)
        navigationBarAppearace.titleTextAttributes = [NSFontAttributeName : UIFont.systemFont(ofSize: 17), NSForegroundColorAttributeName : UIColor(rgba: "#12171F")]
        
        window = UIWindow(frame: Constant.ScreenSize.SCREEN_BOUNDS)
        window?.backgroundColor = UIColor.white
        window?.makeKeyAndVisible()
        
        //设备信息统计
        OISAgent.start(withAppKey: Constant.DEVICE_APPKEY)
        
        if UserDefaults.standard.bool(forKey: "everLaunched") == true{
            OISAgent.setFirstBoot(0)
            window?.rootViewController = MainViewController()
            if AccountManager.shared.isLogin() {
            UserInfoManager.shareUserInfoManager().updateUserInfo(UIApplication.shared.keyWindow!)
            }
        }
        else{
            OISAgent.setFirstBoot(1)
            UserDefaults.standard.set(false, forKey: "everLaunched")
            let date = UserDefaults.standard.object(forKey: "firstLaunchTime")
            if date == nil {
                UserDefaults.standard.set(Date(), forKey: "firstLaunchTime")
            }
            UserDefaults.standard.synchronize()
            window?.rootViewController = UINavigationController(rootViewController: GuideVC())
        }
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        //设置默认支付方式，默认为：微信支付
        let payType: AnyObject? = UserDefaults.standard.object(forKey: "payType") as AnyObject?
        if payType == nil
        {
            UserDefaults.standard.set(1, forKey:"payType")
            UserDefaults.standard.synchronize()
        }
        
        //键盘
        IQKeyboardManager.shared().isEnabled = true
        
        //友盟
        MobClick .start(withAppkey: Constant.UMENG_APPKEY_DIS) //发布环境下务必开启正式key
        MobClick.setAppVersion(Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String)
        //MobClick.setLogEnabled(true) //发布环境下务必关闭
        
        //分享
        let regPlatformConfigList = [[LDSDKConfigAppIdKey : Constant.WECHAT_APPID,LDSDKConfigAppSecretKey : Constant.WECHAT_APPSECRET,LDSDKConfigAppDescriptionKey :
            Bundle.main.object(forInfoDictionaryKey: "CFBundleDisplayName") as! String,LDSDKConfigAppPlatformTypeKey : 2],[LDSDKConfigAppIdKey : Constant.TENCENT_APPID,
                LDSDKConfigAppSecretKey : Constant.TENCENT_APPKEY,
                LDSDKConfigAppPlatformTypeKey : 1],[LDSDKConfigAppSchemeKey : Constant.ALI_APPKEY,
                    LDSDKConfigAppPlatformTypeKey : 4]]
        LDSDKManager.register(withPlatformConfigList: regPlatformConfigList)
        
        
        //魔窗sdk
        MWApi.registerApp(Constant.MW_APPKEY)
        self.registerMlink()
        
        //七鱼初始化
        QYSDK.shared().registerAppId(Constant.QIYU_APPKEY, appName: Constant.QIYU_APPNAME)
        
        OISAgent.setChannel(UIDevice().channelId())
        let uidString = UserDefaults.standard.object(forKey: "UserUid") as? String
        if let tempString = uidString {
            OISAgent.setUserId((tempString as NSString).integerValue)
        }
        OISAgent.setVersionCode((Constant.versionCode as NSString).integerValue)
        OISAgent.onLaunch()
        
        
        //3D Touch
        if #available(iOS 9.0, *) {
                let item1 = UIApplicationShortcutItem(type: "Search", localizedTitle: "搜索礼物", localizedSubtitle: nil, icon: UIApplicationShortcutIcon(templateImageName:  "shortcut_search"), userInfo: nil)
                
                let item2 = UIApplicationShortcutItem(type: "AddReminder", localizedTitle: "添加送礼提醒", localizedSubtitle: nil, icon: UIApplicationShortcutIcon(templateImageName:  "shortcut_remind"), userInfo: nil)
                
                 let item3 = UIApplicationShortcutItem(type: "FlashSale", localizedTitle: "争分夺礼", localizedSubtitle: nil, icon: UIApplicationShortcutIcon(templateImageName:  "shortcut_flash"), userInfo: nil)
                
                 let item4 = UIApplicationShortcutItem(type: "NewTactic", localizedTitle: "最新攻略", localizedSubtitle: nil, icon: UIApplicationShortcutIcon(templateImageName:  "shortcut_tractic"), userInfo: nil)
                if AccountManager.shared.isLogin() {
                application.shortcutItems = [item4, item3, item2,item1]
                }
                else{
                    application.shortcutItems = [item4,item3,item1]
                }
    }
    
        //个推
        GeTuiSdk.start(withAppId: Constant.kGtAppId, appKey: Constant.kGtAppKey, appSecret: Constant.kGtAppSecret, delegate: self);
        self.registerUserNotification(application);
        
        if let notificationDetails = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as? NSDictionary {
            if ((notificationDetails["aps"] as? NSDictionary) != nil) {
                if UserDefaults.standard.bool(forKey: "everLaunched") == true{
                    let userInfo: Dictionary<String,AnyObject>! = [
                        "aps": notificationDetails.object(forKey: "aps")! as AnyObject,
                        "payload": notificationDetails.object(forKey: "payload") as! String as AnyObject,
                        ]
                    NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.notifationMessageReceiveHandle), object: nil, userInfo: userInfo)
                }
            }
        }
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: Constant.show_mainTabBarController), object: nil, queue: OperationQueue.main) { (notification) -> Void in
            self.changeRootViewController(MainViewController())
        }
        
        if #available(iOS 9.0, *) {
            if let shortcutItem = launchOptions?[UIApplicationLaunchOptionsKey.shortcutItem] as? UIApplicationShortcutItem {
                let userInfo: Dictionary<String,AnyObject>! = [
                    "touchType": shortcutItem.type as AnyObject,
                    ]
                NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.notifationTouchReceiveHandle), object: nil, userInfo: userInfo)
                return false
                
            }
        } else {
            // Fallback on earlier versions
        }
        
        self.changeUserAgent()
        return true
    }
    
    func changeUserAgent(){
        let oldAgent = UIWebView().stringByEvaluatingJavaScript(from: "navigator.userAgent")
        let str = (Bundle.main.infoDictionary! as [String: AnyObject])["CFBundleShortVersionString"] as! String
        let newAgent = (oldAgent)! + ";xydd/\(str) chn/0 UDID/\(UIDevice().deviceId() as String)"
        UserDefaults.standard.register(defaults: ["UserAgent":newAgent])
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.APP_LCA.WillResignActive), object: nil)
    }
    
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        //关闭个推
        NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.APP_LCA.DidEnterBackground), object: nil)
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        UIApplication.shared.applicationIconBadgeNumber = 0
        NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.APP_LCA.WillEnterForeground), object: nil)
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        UIApplication.shared.applicationIconBadgeNumber = 0
        //启动个推
        GeTuiSdk.start(withAppId: Constant.kGtAppId, appKey: Constant.kGtAppKey, appSecret: Constant.kGtAppSecret, delegate: self);
        NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.APP_LCA.DidBecomeActive), object: nil)
        
        SystemInfoRequestManager.shareInstance().requestSystemInfo()
//        let authStatus: ABAuthorizationStatus = ABAddressBookGetAuthorizationStatus()
//        if AccountManager.shared.isLogin() {
//            if authStatus == .Authorized {
//                if addressBookBool {
//                addressBookBool = !addressBookBool
//                
//                gcd.async(.Default){
//                    let sysContacts = ABAddressBookCopyArrayOfAllPeople(self.addressBookRef).takeRetainedValue() as NSArray
//                    AddressBookDataSource.sharedInstance.analyzeSysContacts(sysContacts)
//                    }
//                
//                }
//            
//            }
//        }
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        UIApplication.shared.applicationIconBadgeNumber = 0
        CoreDataManager.shared.save()
        NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.APP_LCA.WillTerminate), object: nil)
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        let components = URLComponents(url: url,resolvingAgainstBaseURL: true)
        if components!.host == Constant.JTAPI.PrivateHost || components!.host == Constant.JTAPI.PrivateTestHost{
            MWApi.routeMLink(url)
        }else if url.scheme == "LimiUPPay" {
           let _ = OIUPPayControl.shareControl.handleOpenURL(url: url)
        }else{
            LDSDKManager.handleOpen(url)
        }
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool{
        let components = URLComponents(url: url,resolvingAgainstBaseURL: true)
        if components!.host == Constant.JTAPI.PrivateHost || components!.host == Constant.JTAPI.PrivateTestHost{
            MWApi.routeMLink(url)
        }else if url.scheme == "LimiUPPay" {
            let _ = OIUPPayControl.shareControl.handleOpenURL(url: url)
        }else{
            LDSDKManager.handleOpen(url)
        }
        return true
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool{
      return MWApi.continue(userActivity)
    }
    
    func application(_ application: UIApplication, handleOpen url: URL) -> Bool {
        return LDSDKManager.handleOpen(url)
    }
    
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        application.applicationIconBadgeNumber = 0;        // 标签
        if UserDefaults.standard.bool(forKey: "everLaunched") == true{
            print(userInfo["aps"])
        if let payload = userInfo["payload"] as? String {
            if self.currentViewController == nil{
                
                EventDispatcher.dispatch(payload, onNavigationController: self.window?.rootViewController?.navigationController)
            }
            else{
                EventDispatcher.dispatch(payload, onNavigationController: self.currentViewController!.navigationController)
            }
            
        }
        }
        else{
            if let aps = userInfo["aps"] as? NSDictionary{
                if let alert = aps["alert"] as? NSDictionary{
            let alertView = UIAlertController(title: "通知", message: alert["body"] as? String, preferredStyle: .alert)
            alertView.addAction(UIAlertAction(title: "确定", style: .cancel, handler: nil))
            window?.rootViewController!.present(alertView, animated: true, completion: nil)
                }
            }
            
        }
        
        if let payload = userInfo["payload"] as? String {
            
        if let url : URL = URL(string: payload){
            let eventURL = EventDispatcher.preParserURLWithNotInMap(url)
            let host = eventURL.host
            var id : String = ""
            if let param : NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: url.query) as NSDictionary?{
                if let app_id: AnyObject = param.object(forKey: "id") as AnyObject?{
                    id = String(describing: app_id)
                }
            }
            
            Statistics.count(Statistics.Other.tuisong_ios_daodashu, andAttributes: ["message_id": host! + "    " + id])
        }
        }
    }
    
    
    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void)
    {
        GeTuiSdk.resume()
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        var token = deviceToken.description.trimmingCharacters(in: CharacterSet(charactersIn: "<>"));
        token = token.replacingOccurrences(of: " ", with: "")
        GeTuiSdk.registerDeviceToken(token)
        
        QYSDK.shared().updateApnsToken(deviceToken)
        
    }
    
    //魔窗sdk
    func registerMlink() {
        MWApi.registerMLinkDefaultHandler { (url, params) in
            }
        
        MWApi.registerMLinkHandler(withKey: "goodsitem") { (url, params) in
            var roateurl = url
                if let lastCom = url.path.lastPathComponent {
                    roateurl = URL(string: "local://\(lastCom)/goods_item?\(url.query!)")!
                    if UserDefaults.standard.bool(forKey: "everLaunched") == true {
                    EventDispatcher.dispatch("\(roateurl)", onNavigationController: self.currentViewController?.navigationController)
                    }
                    else{
                        UserDefaults.standard.set(roateurl, forKey: "appRouteMLink")
                        UserDefaults.standard.synchronize()
                    }
                }


        }
        
        MWApi.registerMLinkHandler(withKey: "artilcle_item") { (url, params) in
            var roateurl = url
                if let lastCom = url.path.lastPathComponent {
                    roateurl = URL(string: "local://\(lastCom)/article_item?\(url.query!)")!
                    if UserDefaults.standard.bool(forKey: "everLaunched") == true {
                    EventDispatcher.dispatch("\(roateurl)", onNavigationController: self.currentViewController?.navigationController)
                    }
                    else{
                        UserDefaults.standard.set(roateurl, forKey: "appRouteMLink")
                        UserDefaults.standard.synchronize()
                    }
                }
        }
        
        MWApi.registerMLinkHandler(withKey: "coupon") { (url, params) in
            var roateurl = url
            if let lastCom = url.path.lastPathComponent {
                roateurl = URL(string: "local://\(lastCom)/coupon?\(url.query!)")!
                if UserDefaults.standard.bool(forKey: "everLaunched") == true {
                    if UserInfoManager.didLogin == true{
                    EventDispatcher.dispatch("\(roateurl)", onNavigationController: self.currentViewController?.navigationController)
                    }
                    else{
                      
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.loginType = Constant.InLoginType.couponType.rawValue
                        self.loginDialog.controller = self.currentViewController
                        self.loginDialog.show()
                    }
                }
                else{
                    UserDefaults.standard.set(roateurl, forKey: "appRouteMLink")
                    UserDefaults.standard.synchronize()
                }
            }
        }
        MWApi.registerMLinkHandler(withKey: "package_item") { (url, params) in
            var roateurl = url
            if let lastCom = url.path.lastPathComponent {
                roateurl = URL(string: "local://\(lastCom)/package_item?\(url.query!)")!
                if UserDefaults.standard.bool(forKey: "everLaunched") == true {
                        EventDispatcher.dispatch("\(roateurl)", onNavigationController: self.currentViewController?.navigationController)
                }
                else{
                    UserDefaults.standard.set(roateurl, forKey: "appRouteMLink")
                    UserDefaults.standard.synchronize()
                }
            }
        }
        
        MWApi.registerMLinkHandler(withKey: "package_item_test") { (url, params) in
            var roateurl = url
            if let lastCom = url.path.lastPathComponent {
                roateurl = URL(string: "local://\(lastCom)/package_item?\(url.query!)")!
                if UserDefaults.standard.bool(forKey: "everLaunched") == true {
                    EventDispatcher.dispatch("\(roateurl)", onNavigationController: self.currentViewController?.navigationController)
                }
                else{
                    UserDefaults.standard.set(roateurl, forKey: "appRouteMLink")
                    UserDefaults.standard.synchronize()
                }
            }
        }
        MWApi.registerMLinkHandler(withKey: "package_guess") { (url, params) in
         
            if let param : NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: url.query) as NSDictionary?,
                let id = param["id"],
                let idStr = id as? String
            {
                if UserDefaults.standard.bool(forKey: "everLaunched") == true {
                    let jumpUrl = "http://m.xydd.co/event/guessgift/join?need_token=1&id=" + idStr
                    EventDispatcher.dispatch(jumpUrl, onNavigationController: self.currentViewController?.navigationController)
                }
                else{
                    let jumpUrl = "http://m.xydd.co/event/guessgift/join?need_token=1&id=" + idStr
                    UserDefaults.standard.set(URL(string: jumpUrl), forKey: "appRouteMLink")
                    UserDefaults.standard.synchronize()
                }
            }
            

        }
        MWApi.registerMLinkHandler(withKey: "package_guess_test") { (url, params) in
            if let param : NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: url.query) as NSDictionary?,
                let id = param["id"],
                let idStr = id as? String
            {
                if UserDefaults.standard.bool(forKey: "everLaunched") == true {
                    let jumpUrl = "http://m.xydd.co/event/guessgift?need_token=1&id=" + idStr
                    EventDispatcher.dispatch(jumpUrl, onNavigationController: self.currentViewController?.navigationController)
                }
                else{
                    let jumpUrl = "http://m.xydd.co/event/guessgift?need_token=1&id=" + idStr
                    UserDefaults.standard.set(URL(string: jumpUrl), forKey: "appRouteMLink")
                    UserDefaults.standard.synchronize()
                }
            }
        }
        

    }

    
    //个推>>>>>>>>>>>>>>>>>>>>>>>>>>>
    /** 注册用户通知(推送) */
    func registerUserNotification(_ application: UIApplication) {
        UIApplication.shared.registerForRemoteNotifications()
        let userSettings = UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil)
        UIApplication.shared.registerUserNotificationSettings(userSettings)
    }
    
    
    // MARK: - GeTuiSdkDelegate
    
    /**app被启动后，处理推送 SDK启动成功返回cid */
    func geTuiSdkDidRegisterClient(_ clientId: String!) {
        UserDefaults.standard.set(clientId, forKey: "deviceToken")
        UserDefaults.standard.synchronize()
        print(clientId)
        let _ = LMRequestManager.requestByDataAnalyses(context:UIApplication.shared.keyWindow!, URLString: Constant.JTAPI.commitToken, parameters: ["app_type": 1 as AnyObject], isNeedAppTokrn: true)
    }
    
    /** SDK遇到错误回调 */
    func geTuiSdkDidOccurError(_ error: Error!) {
    }
    
    /** SDK收到sendMessage消息回调 */
    func geTuiSdkDidSendMessage(_ messageId: String!, result: Int32) {
    }
    
    //最终处理结果 远程推送接收入口
    func geTuiSdkDidReceivePayload(_ payloadId : String, andTaskId taskId: String, andMessageId aMsgId : String,andOffLine offLine : Bool, fromApplication appId : String){
            if let content : Data = GeTuiSdk.retrivePayload(byId: payloadId){
                do {
                    print(content)
                    print(NSString(data: content, encoding: String.Encoding.utf8.rawValue))
                    let json = try JSONSerialization.jsonObject(with: content, options: JSONSerialization.ReadingOptions.mutableLeaves)
                    print(json)
                    if let payload = (json as! [String:AnyObject])["url"] as? String{
                        if let url : URL = URL(string: payload){
                            let eventURL = EventDispatcher.preParserURLWithNotInMap(url)
                            let host = eventURL.host
                            var id : String = ""
                            if let param : NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: url.query) as NSDictionary?{
                                if let app_id: AnyObject = param.object(forKey: "id") as AnyObject?{
                                    id = String(describing: app_id)
                                }
                            }
                            
                            Statistics.count(Statistics.Other.tuisong_ios_daodashu, andAttributes: ["message_id": host! + "    " + id])
                        }
                    }
                    if offLine == false{
                        if let title = (json as! [String:AnyObject])["title"] as? String{
                    let alertView = UIAlertController(title: title, message: (json as! [String:AnyObject])["content"] as? String, preferredStyle: .alert)
                    if let payload = (json as! [String:AnyObject])["url"] as? String{
                        alertView.addAction(UIAlertAction(title: "去看看", style: .default, handler: { (alertAction) -> Void in
                            if let payload = (json as! [String:AnyObject])["url"] as? String{
                                if let url : URL = URL(string: payload){
                                    let eventURL = EventDispatcher.preParserURLWithNotInMap(url)
                                    let host = eventURL.host
                                    var id : String = ""
                                    if let param : NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: url.query) as NSDictionary?{
                                        if let app_id: AnyObject = param.object(forKey: "id") as AnyObject?{
                                            id = String(describing: app_id)
                                        }
                                    }
                                    Statistics.count(Statistics.Other.tuisong_ios, andAttributes: ["message_id": host! + "    " + id])
                                }
                            }
                            EventDispatcher.dispatch(payload, onNavigationController: self.currentViewController?.navigationController)
                        }))
                    }
                    alertView.addAction(UIAlertAction(title: "确定", style: .cancel, handler: nil))
                    window?.rootViewController!.present(alertView, animated: true, completion: nil)
                    }
                    }
                    
                    
                } catch let error as NSError {
                    print(error)
                }
            }
            
        }
    

    //个推<<<<<<<<<<<<<<<<<<<<<<<<<<
    
    
    /**
     *  监听shortcut点击事件
     */
    @available(iOS 9.0, *)
    func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
        if UserDefaults.standard.bool(forKey: "everLaunched") == true{
            if shortcutItem.type == "Search"{
                EventDispatcher.dispatch("local://search?app=1", onNavigationController: self.currentViewController!.navigationController)
            }
            else if shortcutItem.type == "AddReminder"{
                if AccountManager.shared.isLogin(){
                    EventDispatcher.dispatch("local://relation_add?app=1", onNavigationController: self.currentViewController!.navigationController)
                    
                }
                else{
                    let alertView = UIAlertController(title: "提示", message: "需要登录账号后才可以添加送礼提醒", preferredStyle: .alert)
                    alertView.addAction(UIAlertAction(title: "确定", style: .cancel, handler: nil))
                    window?.rootViewController!.present(alertView, animated: true, completion: nil)
                }
            }
            else if shortcutItem.type == "FlashSale"{
                EventDispatcher.dispatch("http://wechat.giftyou.me/goods/flashSale", onNavigationController: self.currentViewController!.navigationController)
            }
            else if shortcutItem.type == "NewTactic"{
                EventDispatcher.dispatch("local://wechat.giftyou.me/article_list?app=1&sort=0", onNavigationController: self.currentViewController!.navigationController)
                
            }
        }
    }
    
    
    func changeRootViewController(_ viewController : UIViewController){
        if ((self.window?.rootViewController) == nil){
            self.window!.rootViewController = viewController
            return
        }
        let snapShot : UIView = (self.window?.snapshotView(afterScreenUpdates: true))!
        viewController.view.addSubview(snapShot)
        self.window!.rootViewController = viewController
        UIView.animate(withDuration: 0.3, animations: {
            snapShot.layer.opacity = 0
            snapShot.layer.transform = CATransform3DMakeScale(1.5, 1.5, 1.5)
            }, completion: {
                (value: Bool) in
                snapShot.removeFromSuperview()
        })
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    
}

