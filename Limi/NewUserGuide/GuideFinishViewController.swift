//
//  GuideFinishViewController.swift
//  Limi
//
//  Created by 倪晅 on 16/1/6.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

enum GuideFinishContact {
    case success
    case cancel
}

class GuideFinishViewController: GuideBaseViewController {
    
    fileprivate var _statLabel: UILabel?
    fileprivate var _nextButton: UIButton?
    fileprivate var _scoreView: GuideFinishScoreView?
    
    var statLabel: UILabel {
        get {
            if _statLabel == nil {
                _statLabel = UILabel()
            }
            return _statLabel!
        }
    }
    
    var nextButton: UIButton {
        get {
            if _nextButton == nil {
                _nextButton = UIButton(type: .custom)
                _nextButton!.setTitle("开启我的心意之旅", for: UIControlState())
                _nextButton!.setTitleColor(UIColor.white, for: UIControlState())
                _nextButton!.backgroundColor = Constant.Theme.Color1
                _nextButton!.addTarget(self, action: Selector(("pushMainViewController")), for: .touchUpInside)
                _nextButton!.layer.cornerRadius = 5
                _nextButton!.layer.masksToBounds = true
            }
            return _nextButton!
        }
    }
    
    var scoreView: GuideFinishScoreView {
        get {
            if _scoreView == nil {
                _scoreView = GuideFinishScoreView()
            }
            return _scoreView!
        }
    }
    
    var finishContact: GuideFinishContact
    
    init(finishContact: GuideFinishContact) {
        self.finishContact = finishContact
        
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.addSubview(self.scoreView)
        self.view.addSubview(self.statLabel)
        self.view.addSubview(self.nextButton)
        
        self.scoreView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.view).offset(100)
            let _ = make.centerX.equalTo(self.view)
            let _ = make.width.equalTo(320)
            let _ = make.height.equalTo(100)
        }
        self.statLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.scoreView.snp_bottom).offset(100)
            let _ = make.left.equalTo(self.view).offset(15)
            let _ = make.right.equalTo(self.view).offset(-15)
        }
        self.nextButton.snp_makeConstraints { (make) -> Void in
            let _ = make.centerX.equalTo(self.view)
            let _ = make.width.equalTo(298)
            let _ = make.height.equalTo(44)
            let _ = make.bottom.equalTo(self.view.snp_bottom).offset(-20)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
