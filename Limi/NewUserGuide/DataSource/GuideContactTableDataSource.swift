//
//  GuideContactTableDataSource.swift
//  Limi
//
//  Created by 倪晅 on 16/1/5.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

//拿到通讯录并解析数据成字典

import UIKit

class GuideContactTableDataSource: BaseTableDataSource {
    
    var sysContacts: NSArray = {
        var addressBook = ABAddressBookCreateWithOptions(nil, nil).takeRetainedValue()
        return ABAddressBookCopyArrayOfAllPeople(addressBook).takeRetainedValue() as NSArray
    }()
    
    fileprivate var screenContactsArray = NSMutableArray()//筛选过后的合法联系人
    fileprivate var selectedAry = NSMutableArray()
    
    override init(delegate: BaseTableDataSourceDelegate, viewModelClass: BaseTableCellViewModel.Type?) {
        super.init(delegate: delegate, viewModelClass: viewModelClass)
        self.cellClass = GuideContactTableCell.self
    }
    
    override func fetchRefreshDataWithSuccess(_ success: @escaping TableDataSourceSuccessBlock, failure: @escaping TableDataSourceFailureBlock) {
        if self.sysContacts.count > 0 {
//            success(sysContacts as [AnyObject])
            let list = NSMutableArray()
            for contact in sysContacts {
                let dict = AddressBookManager.analiyseConnectCard(contact as ABRecord!)
                list.add(dict)
            }
            success(list as [AnyObject])

        } else {
            self.screenArray(self.screenContactsArray, andIsFinish:true)
        }
    }
    
    fileprivate func screenArray(_ array:NSMutableArray, andIsFinish isFinish:Bool) {
        for i in 0 ..< array.count {
            let dict = array[i] as! NSMutableDictionary
            
            screenContactsArray.add(dict)
        }
        
        DispatchQueue.main.async(execute: {()->Void in
            self.createSelectedAry(self.screenContactsArray.count, andisfFinish:isFinish)
        });
    }
    
    fileprivate func createSelectedAry(_ count:Int, andisfFinish isFinish:Bool) {
        if isFinish {
        }
        
        if count == 0 {
            return
        }
        
        selectedAry.removeAllObjects()
        for _ in 0 ..< count {
            selectedAry.add(true)
        }
    }

}
