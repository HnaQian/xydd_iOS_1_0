//
//  GuideBirthdayViewController.swift
//  Limi
//
//  Created by 倪晅 on 16/1/6.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class GuideBirthdayViewController: GuideBaseViewController {
    
    fileprivate var _titleLabel: UILabel?
    fileprivate var _subTitleLabel: UILabel?
    fileprivate var _datePicker: DatePickerView?
    fileprivate var _saveButton: UIButton?
    
    var titleLabel: UILabel {
        get {
            if _titleLabel == nil {
                _titleLabel = UILabel()
                _titleLabel!.text = "您的生日？"
                _titleLabel!.textAlignment = .center
                _titleLabel!.font = Constant.Theme.Font_19
                _titleLabel!.textColor = Constant.Theme.Color2
            }
            return _titleLabel!
        }
    }
    
    var subTitleLabel: UILabel {
        get {
            if _subTitleLabel == nil {
                _subTitleLabel = UILabel()
                _subTitleLabel!.text = "填写后我会告诉你生日背后的秘密哦"
                _subTitleLabel!.textAlignment = .center
                _subTitleLabel!.numberOfLines = 0
                _subTitleLabel!.font = Constant.Theme.Font_15
                _subTitleLabel!.textColor = Constant.Theme.Color6
            }
            return _subTitleLabel!
        }
    }
    
    var datePicker: DatePickerView {
        get {
            if _datePicker == nil {
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy"
                let currentDate = NSDate(year: 1980, month: 1, day: 1)
                
                _datePicker = DatePickerView(frame: CGRect(x: 0, y: Constant.ScreenSize.SCREEN_HEIGHT/2 - 50, width: Constant.ScreenSize.SCREEN_WIDTH , height: Constant.ScreenSize.SCREEN_HEIGHT/2 + 50), type: false, display: currentDate as Date!, yearStart: 1905, yearEnd: 2049,toolBarHeight:32,pickerHeight:160,isGuideBirthdayView:true)
                _datePicker!.delegate = self
            }
            return _datePicker!
        }
    }
   
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.addSubview(self.titleLabel)
        self.view.addSubview(self.subTitleLabel)
        self.view.addSubview(self.datePicker)
        
        self.titleLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.topDecorateView.snp_bottom).offset(66 * Constant.ScreenSize.SCREEN_HEIGHT_SCALE)
            let _ = make.left.equalTo(self.view).offset(20)
            let _ = make.right.equalTo(self.view).offset(-20)
        }
        self.subTitleLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.titleLabel.snp_bottom).offset(20 * Constant.ScreenSize.SCREEN_HEIGHT_SCALE)
            let _ = make.left.right.equalTo(self.titleLabel)
        }
        
        self.datePicker.backgroundColor = UIColor.clear
        self.datePicker.hideYearButton.isHidden = true

        
        if Constant.DeviceType.IS_IPHONE_4_OR_LESS || Constant.DeviceType.IS_IPHONE_5{
            self.datePicker.hideYearButton.titleLabel?.font = UIFont.systemFont(ofSize: Constant.common_F3_font)
        }else{
            self.datePicker.hideYearButton.titleLabel?.font = UIFont.systemFont(ofSize: Constant.common_F2_font)
        }
        
        let skipButton = UIButton(type: .custom)
        skipButton.frame = CGRect(x: 0, y: 0, width: 90 + Constant.ScreenSizeV1.MARGIN_44, height: 17)
        skipButton.setTitle("直接进入首页", for: UIControlState())
        skipButton.setTitleColor(Constant.Theme.Color6, for: UIControlState())
        skipButton.titleLabel?.font = Constant.Theme.Font_17
        skipButton.addTarget(self, action: #selector(GuideBirthdayViewController.didTapSkip), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: skipButton)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func didTapSkip() {
        NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.show_mainTabBarController), object: nil)
    }

}

extension GuideBirthdayViewController: DatePickerViewDelegate {
    
    func notifyNewCalendar(_ cal: XYDDCalendar!, type _type: Int32, date: Date!, isYearHidden _isYearHidden: Bool, onlyMonthDay monthDay: String!) {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        var date = formatter.string(from: date)
        date = date + " 00:00:00"
        if _isYearHidden {
            date = "1600-" + monthDay! + " 00:00:00"
        }
        AccountManager.shared.setBirthday(Int64(_type), date: date)
        
        let vc = GuideTagViewController(dateType: NSInteger(_type), date: date)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
