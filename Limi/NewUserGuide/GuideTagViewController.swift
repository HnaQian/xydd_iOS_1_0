//
//  GuideTagViewController.swift
//  Limi
//
//  Created by 倪晅 on 16/1/6.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class GuideTagViewController: GuideBaseViewController {
    
    var gender: NSInteger?
    var dateType: NSInteger?
    var date: String?
    
    fileprivate var _titleLabel: UILabel?
    fileprivate var _subTitleLabel: UILabel?
    fileprivate var _tagCloudView: DBSphereView?
    fileprivate var _firstTagComposeView: GuideTagComposeView?
    fileprivate var _secondTagComposeView: GuideTagComposeView?
    fileprivate var _thirdTagComposeView: GuideTagComposeView?
    fileprivate var _celebrityView: UIView?
    fileprivate var _celebrityTitleLabel: UILabel?
    fileprivate var _celebrityContentLabel: UILabel?
    fileprivate var _nextButton: UIButton?
    
    var titleLabel: UILabel {
        get {
            if _titleLabel == nil {
                _titleLabel = UILabel()
                _titleLabel!.text = " "
                _titleLabel!.numberOfLines = 0
                _titleLabel!.textAlignment = .center
                _titleLabel!.font = UIFont.systemFont(ofSize: 26)
                _titleLabel!.textColor = Constant.Theme.Color2
            }
            return _titleLabel!
        }
    }
    
    var subTitleLabel: UILabel {
        get {
            if _subTitleLabel == nil {
                _subTitleLabel = UILabel()
                _subTitleLabel!.text = " "
                _subTitleLabel!.numberOfLines = 0
                _subTitleLabel!.textAlignment = .center
                _subTitleLabel!.font = Constant.Theme.Font_15
                _subTitleLabel!.textColor = Constant.Theme.Color7
            }
            return _subTitleLabel!
        }
    }
    
    var tagCloudView: DBSphereView {
        get {
            if _tagCloudView == nil {
                _tagCloudView = DBSphereView()
            }
            return _tagCloudView!
        }
    }
    
    var firstTagComposeView: GuideTagComposeView {
        get {
            if _firstTagComposeView == nil {
                _firstTagComposeView = GuideTagComposeView(tipText: " ", subTipText: " ")
                let border = UIView()
                border.backgroundColor = Constant.Theme.Color10
                _firstTagComposeView!.addSubview(border)
                border.snp_makeConstraints(closure: { (make) -> Void in
                    let _ = make.width.equalTo(1)
                    let _ = make.centerY.right.equalTo(_firstTagComposeView!)
                    let _ = make.height.equalTo(40)
                })
            }
            return _firstTagComposeView!
        }
    }
    
    var secondTagComposeView: GuideTagComposeView {
        get {
            if _secondTagComposeView == nil {
                _secondTagComposeView = GuideTagComposeView(tipText: " ", subTipText: " ")
            }
            return _secondTagComposeView!
        }
    }
    
    var thirdTagComposeView: GuideTagComposeView {
        get {
            if _thirdTagComposeView == nil {
                _thirdTagComposeView = GuideTagComposeView(tipText: " ", subTipText: " ")
                let border = UIView()
                border.backgroundColor = Constant.Theme.Color10
                _thirdTagComposeView!.addSubview(border)
                border.snp_makeConstraints(closure: { (make) -> Void in
                    let _ = make.width.equalTo(1)
                    let _ = make.centerY.left.equalTo(_thirdTagComposeView!)
                    let _ = make.height.equalTo(40)
                })
            }
            return _thirdTagComposeView!
        }
    }
    
    var celebrityView: UIView {
        get {
            if _celebrityView == nil {
                _celebrityView = UIView()
                _celebrityView!.backgroundColor = UIColor(rgba: "#F9EEEE")
            }
            return _celebrityView!
        }
    }
    
    var celebrityTitleLabel: UILabel {
        get {
            if _celebrityTitleLabel == nil {
                _celebrityTitleLabel = UILabel()
                _celebrityTitleLabel!.text = " "
                _celebrityTitleLabel!.textAlignment = .center
                _celebrityTitleLabel!.font = Constant.Theme.Font_15
                _celebrityTitleLabel!.textColor = Constant.Theme.Color7
            }
            return _celebrityTitleLabel!
        }
    }
    
    var celebrityContentLabel: UILabel {
        get {
            if _celebrityContentLabel == nil {
                _celebrityContentLabel = UILabel()
                _celebrityContentLabel!.text = " "
                _celebrityContentLabel!.textAlignment = .center
                _celebrityContentLabel!.font = Constant.Theme.Font_13
                _celebrityContentLabel!.textColor = Constant.Theme.Color7
            }
            return _celebrityContentLabel!
        }
    }
    
    var nextButton: UIButton {
        get {
            if _nextButton == nil {
                _nextButton = UIButton(type: .custom)
                _nextButton!.setTitle("开启我的心意之旅", for: UIControlState())
                _nextButton!.setTitleColor(UIColor.white, for: UIControlState())
                _nextButton!.backgroundColor = Constant.Theme.Color1
                _nextButton!.addTarget(self, action: #selector(GuideTagViewController.didTapNext), for: .touchUpInside)
                _nextButton!.layer.cornerRadius = 5
                _nextButton!.layer.masksToBounds = true
            }
            return _nextButton!
        }
    }
    
    init(dateType: NSInteger, date: String) {
        super.init(nibName: nil, bundle: nil)
        
        self.dateType = dateType
        self.date = date
        self.gender = NSInteger(AccountManager.shared.getGender())
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
       // self.view.backgroundColor = UIColor(rgba: "#FEFAFA")
        
        self.view.addSubview(self.titleLabel)
        self.view.addSubview(self.subTitleLabel)
        self.view.addSubview(self.tagCloudView)
        self.view.addSubview(self.firstTagComposeView)
        self.view.addSubview(self.secondTagComposeView)
        self.view.addSubview(self.thirdTagComposeView)
        self.view.addSubview(self.nextButton)
        
        self.titleLabel.snp_makeConstraints { (make) -> Void in
            if Int(Constant.ScreenSize.SCREEN_HEIGHT) < 568{
                let _ = make.top.equalTo(self.topDecorateView.snp_bottom).offset(0 * Constant.ScreenSize.SCREEN_HEIGHT_SCALE)
            }
            else if Int(Constant.ScreenSize.SCREEN_HEIGHT) == 568{
                let _ = make.top.equalTo(self.topDecorateView.snp_bottom).offset(10 * Constant.ScreenSize.SCREEN_HEIGHT_SCALE)
            }
            else
            {
            let _ = make.top.equalTo(self.topDecorateView.snp_bottom).offset(30 * Constant.ScreenSize.SCREEN_HEIGHT_SCALE)
            }
            let _ = make.left.equalTo(self.view).offset(20)
            let _ = make.right.equalTo(self.view).offset(-20)
        }
        self.subTitleLabel.snp_makeConstraints { (make) -> Void in
            if Int(Constant.ScreenSize.SCREEN_HEIGHT) <= 568{
                let _ = make.top.equalTo(self.titleLabel.snp_bottom).offset(15 * Constant.ScreenSize.SCREEN_HEIGHT_SCALE)
            }
            else{
            let _ = make.top.equalTo(self.titleLabel.snp_bottom).offset(20 * Constant.ScreenSize.SCREEN_HEIGHT_SCALE)
            }
            let _ = make.left.equalTo(self.view).offset(20)
            let _ = make.right.equalTo(self.view).offset(-20)
        }
        self.tagCloudView.snp_makeConstraints { (make) -> Void in
            if Int(Constant.ScreenSize.SCREEN_HEIGHT) < 568{
                let _ = make.top.equalTo(self.subTitleLabel.snp_bottom).offset(31 * Constant.ScreenSize.SCREEN_HEIGHT_SCALE)
            }
            else if Int(Constant.ScreenSize.SCREEN_HEIGHT) == 568{
               let _ = make.top.equalTo(self.subTitleLabel.snp_bottom).offset(45 * Constant.ScreenSize.SCREEN_HEIGHT_SCALE)
            }
            else{
            let _ = make.top.equalTo(self.subTitleLabel.snp_bottom).offset(66 * Constant.ScreenSize.SCREEN_HEIGHT_SCALE)
            }
            let _ = make.centerX.equalTo(self.view)
            let _ = make.height.equalTo(187 * Constant.ScreenSize.SCREEN_WIDTH_SCALE)
            let _ = make.width.equalTo(187 * Constant.ScreenSize.SCREEN_WIDTH_SCALE)
        }
        self.firstTagComposeView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.secondTagComposeView)
            let _ = make.left.equalTo(self.view)
            if Int(Constant.ScreenSize.SCREEN_HEIGHT) < 568{
                let _ = make.height.equalTo(30)
            }
            else if Int(Constant.ScreenSize.SCREEN_HEIGHT) == 568{
                let _ = make.height.equalTo(35)
            }else{
            let _ = make.height.equalTo(40)
            }
            let _ = make.width.equalTo(self.view).multipliedBy(0.333333)
        }
        self.secondTagComposeView.snp_makeConstraints { (make) -> Void in
            if Int(Constant.ScreenSize.SCREEN_HEIGHT) < 568{
                let _ = make.top.equalTo(self.tagCloudView.snp_bottom).offset(30 * Constant.ScreenSize.SCREEN_HEIGHT_SCALE)
            }
            else if Int(Constant.ScreenSize.SCREEN_HEIGHT) == 568{
                let _ = make.top.equalTo(self.tagCloudView.snp_bottom).offset(45 * Constant.ScreenSize.SCREEN_HEIGHT_SCALE)
            }
            else{
            let _ = make.top.equalTo(self.tagCloudView.snp_bottom).offset(60 * Constant.ScreenSize.SCREEN_HEIGHT_SCALE)
            }
            if Int(Constant.ScreenSize.SCREEN_HEIGHT) < 568{
               let _ =  make.height.equalTo(30)
            }
            else if Int(Constant.ScreenSize.SCREEN_HEIGHT) == 568{
                let _ = make.height.equalTo(35)
            }
            else{
                let _ = make.height.equalTo(40)
            }
            let _ = make.left.equalTo(self.firstTagComposeView.snp_right)
            let _ = make.right.equalTo(self.thirdTagComposeView.snp_left)
        }
        self.thirdTagComposeView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.secondTagComposeView)
            let _ = make.right.equalTo(self.view)
            if Int(Constant.ScreenSize.SCREEN_HEIGHT) < 568{
                let _ = make.height.equalTo(30)
            }
            else if Int(Constant.ScreenSize.SCREEN_HEIGHT) == 568{
                let _ = make.height.equalTo(35)
            }
            else{
                let _ = make.height.equalTo(40)
            }
            let _ = make.width.equalTo(self.view).multipliedBy(0.333333)
        }
        
        
        self.nextButton.snp_makeConstraints { (make) -> Void in
            let _ = make.centerX.equalTo(self.view)
            let _ = make.width.equalTo(298)
            let _ = make.height.equalTo(44)
            let _ = make.bottom.equalTo(self.view.snp_bottom).offset(-20)
        }

        let params: [String: AnyObject] = [
            "gender": self.gender! as AnyObject,
            "date_type": self.dateType! as AnyObject,
            "date": self.date! as AnyObject
        ]
        
        let _ = LMRequestManager.requestByDataAnalyses(context:self.view,URLString: Constant.JTAPI.birthday_tag_list, parameters: params,isShowErrorStatuMsg: true, isNeedHud: true, hudTitle: "",successHandler: {data, status, msg in
            if status != 200 {
                return
            }
            if let dataDict = data["data"] as? [String: AnyObject] {
                let celebrityContent = dataDict["celebrity_content"] as! String
                let celebrityName = dataDict["celebrity_name"] as! String
                let dayContent = dataDict["day_content"] as! String
                let keywordNames = (dataDict["keyword_names"] as! String).components(separatedBy: ",")
                let keywordValues = (dataDict["keyword_values"] as! String).components(separatedBy: ",")
                let tags = dataDict["tags"] as! String
                let title = dataDict["title"] as! String
                
                self.titleLabel.text = title
                self.subTitleLabel.text = dayContent
                if celebrityName != ""{
                self.celebrityTitleLabel.text = celebrityName
                self.celebrityContentLabel.text = celebrityContent
                    
                self.view.addSubview(self.celebrityView)
                self.celebrityView.addSubview(self.celebrityTitleLabel)
                self.celebrityView.addSubview(self.celebrityContentLabel)
                self.celebrityView.snp_makeConstraints { (make) -> Void in
                    let _ = make.left.right.equalTo(self.view)
                    let _ = make.top.equalTo(self.secondTagComposeView.snp_bottom).offset(20 * Constant.ScreenSize.SCREEN_HEIGHT_SCALE)
                    if Int(Constant.ScreenSize.SCREEN_HEIGHT) < 568{
                        let _ = make.height.equalTo(56)
                    }
                    else if Int(Constant.ScreenSize.SCREEN_HEIGHT) == 568{
                        let _ = make.height.equalTo(66)
                    }
                    else{
                    let _ = make.height.equalTo(76)
                    }
                    }
                self.celebrityTitleLabel.snp_makeConstraints { (make) -> Void in
                    if Int(Constant.ScreenSize.SCREEN_HEIGHT) < 568{
                        let _ = make.top.equalTo(self.celebrityView).offset(5)
                    }
                    else if Int(Constant.ScreenSize.SCREEN_HEIGHT) == 568{
                        let _ = make.top.equalTo(self.celebrityView).offset(10)
                    }
                    else{
                    let _ = make.top.equalTo(self.celebrityView).offset(15)
                    }
                    let _ = make.left.right.equalTo(self.celebrityView)
                    let _ = make.height.equalTo(Constant.Theme.Font_15.lineHeight)
                    }
                self.celebrityContentLabel.snp_makeConstraints { (make) -> Void in
                    let _ = make.left.right.equalTo(self.celebrityView)
                    if Int(Constant.ScreenSize.SCREEN_HEIGHT) < 568{
                        let _ = make.bottom.equalTo(self.celebrityView).offset(-10)
                    }
                    else if Int(Constant.ScreenSize.SCREEN_HEIGHT) == 568{
                        let _ = make.bottom.equalTo(self.celebrityView).offset(-15)
                    }
                    else{
                    let _ = make.bottom.equalTo(self.celebrityView).offset(-20)
                    }
                    }

                }
                
                if keywordNames.count == 3 {
                    self.firstTagComposeView.tipLabel.text = keywordNames[0]
                    self.secondTagComposeView.tipLabel.text = keywordNames[1]
                    self.thirdTagComposeView.tipLabel.text = keywordNames[2]
                }
                if keywordValues.count == 3 {
                    self.firstTagComposeView.subTipLabel.text = keywordValues[0]
                    self.secondTagComposeView.subTipLabel.text = keywordValues[1]
                    self.thirdTagComposeView.subTipLabel.text = keywordValues[2]
                }
                
                self.setupTagCloud(tags.components(separatedBy: ","))
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupTagCloud(_ tagNames: [String]) {
        let colors = [
            UIColor(rgba: "#AD4848"),
            UIColor(rgba: "#479644"),
            UIColor(rgba: "#AB4676"),
            UIColor(rgba: "#4E9ABA"),
            UIColor(rgba: "#794684"),
            UIColor(rgba: "#3C659C"),
            UIColor(rgba: "#AD7F39"),
        ]
        let tags: NSMutableArray = NSMutableArray()
        for i in 0...tagNames.count - 1 {
            let color = colors[i % colors.count]
            let tagButton = UIButton(type: .system)
            tagButton.setTitle(tagNames[i], for: UIControlState())
            tagButton.titleLabel?.font = UIFont.systemFont(ofSize: 24)
            tagButton.setTitleColor(color, for: UIControlState())
            tagButton.frame = CGRect(x: 0, y: 0, width: 160, height: 20)
            tags.add(tagButton)
            self.tagCloudView.addSubview(tagButton)
        }
        self.tagCloudView.setCloudTags(tags as [AnyObject])
    }

    func didTapNext() {
        NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.show_mainTabBarController), object: nil)
    }

}

class GuideTagComposeView: UIView {
    
    fileprivate var _tipLabel: UILabel?
    fileprivate var _subTipLabel: UILabel?
    
    fileprivate var tipText: String
    fileprivate var subTipText: String
    
    var tipLabel: UILabel {
        get {
            if _tipLabel == nil {
                _tipLabel = UILabel()
                _tipLabel!.text = self.tipText
                _tipLabel!.textColor = Constant.Theme.Color7
                _tipLabel!.textAlignment = .center
                _tipLabel!.font = Constant.Theme.Font_13
            }
            return _tipLabel!
        }
    }
    
    var subTipLabel: UILabel {
        get {
            if _subTipLabel == nil {
                _subTipLabel = UILabel()
                _subTipLabel!.text = self.subTipText
                _subTipLabel!.textColor = Constant.Theme.Color6
                _subTipLabel!.textAlignment = .center
                _subTipLabel!.font = Constant.Theme.Font_15
            }
            return _subTipLabel!
        }
    }
    
    init(tipText: String, subTipText: String) {
        self.tipText = tipText
        self.subTipText = subTipText
        super.init(frame: CGRect.zero)
        
        self.addSubview(self.tipLabel)
        self.addSubview(self.subTipLabel)
        
        self.tipLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.top.left.right.equalTo(self)
            let _ = make.height.equalTo(Constant.Theme.Font_13.lineHeight)
        }
        self.subTipLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.bottom.left.right.equalTo(self)
            let _ = make.height.equalTo(Constant.Theme.Font_15.lineHeight)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
