//
//  GuideFinishScoreView.swift
//  Limi
//
//  Created by 倪晅 on 16/1/6.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class GuideFinishScoreView: UIView {
    
    fileprivate var _cornerImage: UIImageView?
    fileprivate var _scoreLabel: UILabel?
    fileprivate var _honorLabel: UILabel?
    
    var cornerImage: UIImageView {
        get {
            if _cornerImage == nil {
                _cornerImage = UIImageView(image: UIImage(named: ""))
            }
            return _cornerImage!
        }
    }
    
    var scoreLabel: UILabel {
        get {
            if _scoreLabel == nil {
                _scoreLabel = UILabel()
                _scoreLabel!.text = "88"
            }
            return _scoreLabel!
        }
    }
    
    var honorLabel: UILabel {
        get {
            if _honorLabel == nil {
                _honorLabel = UILabel()
                _honorLabel!.text = "心意\n达人"
                _honorLabel!.numberOfLines = 2
            }
            return _honorLabel!
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.red
        
        self.addSubview(self.cornerImage)
        self.addSubview(self.scoreLabel)
        self.addSubview(self.honorLabel)
        
        self.cornerImage.snp_makeConstraints { (make) -> Void in
            let _ = make.left.top.equalTo(self)
        }
        self.scoreLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.centerY.equalTo(self)
            let _ = make.left.equalTo(self).offset(20)
        }
        self.honorLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.centerY.equalTo(self)
            let _ = make.right.equalTo(self).offset(-20)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
