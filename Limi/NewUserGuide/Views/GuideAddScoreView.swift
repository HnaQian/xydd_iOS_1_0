//
//  GuideAddScoreView.swift
//  Limi
//
//  Created by 倪晅 on 16/1/6.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class GuideAddScoreView: UIView {
    
    fileprivate var _scoreLabel: UILabel?
    
    var scoreLabel: UILabel {
        get {
            if _scoreLabel == nil {
                _scoreLabel = UILabel()
                _scoreLabel!.text = "心意指数 +1"
                _scoreLabel!.textAlignment = .center
            }
            return _scoreLabel!
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.red
        
        self.addSubview(self.scoreLabel)
        
        self.scoreLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.edges.equalTo(self)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
