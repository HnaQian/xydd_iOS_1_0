//
//  GuideContactControlView.swift
//  Limi
//
//  Created by 倪晅 on 16/1/6.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class GuideContactControlView: UIView {
    
    fileprivate var _allCheckButton: UIButton?
    fileprivate var _importButton: UIButton?
    fileprivate var _skipButton: UIButton?
    
    var allCheckButton: UIButton {
        get {
            if _allCheckButton == nil {
                _allCheckButton = UIButton(type: .custom)
                _allCheckButton!.setTitle("全选", for: UIControlState())
            }
            return _allCheckButton!
        }
    }
    
    var importButton: UIButton {
        get {
            if _importButton == nil {
                _importButton = UIButton(type: .custom)
                _importButton!.setTitle("导入", for: UIControlState())
            }
            return _importButton!
        }
    }
    
    var skipButton: UIButton {
        get {
            if _skipButton == nil {
                _skipButton = UIButton(type: .custom)
                _skipButton!.setTitle("暂不导入", for: UIControlState())
            }
            return _skipButton!
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.red
        
        self.addSubview(self.allCheckButton)
        self.addSubview(self.importButton)
        self.addSubview(self.skipButton)
        
        self.allCheckButton.snp_makeConstraints { (make) -> Void in
            let _ = make.centerY.equalTo(self)
            let _ = make.left.equalTo(self).offset(15)
        }
        self.importButton.snp_makeConstraints { (make) -> Void in
            let _ = make.centerY.equalTo(self)
            let _ = make.left.equalTo(self.allCheckButton.snp_right).offset(10)
            let _ = make.right.equalTo(self.skipButton.snp_left).offset(-10)
        }
        self.skipButton.snp_makeConstraints { (make) -> Void in
            let _ = make.centerY.equalTo(self)
            let _ = make.right.equalTo(self).offset(-15)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
