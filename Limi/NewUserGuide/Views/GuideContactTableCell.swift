//
//  GuideContactTableCell.swift
//  Limi
//
//  Created by 倪晅 on 16/1/5.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class GuideContactTableCell: BaseTableCell {
    
    fileprivate var _radioImage: UIImageView?
    fileprivate var _avatarImage: UIImageView?
    fileprivate var _nameLabel: UILabel?
    fileprivate var _cloudImage: UIImageView?
    fileprivate var _birthdayLabel: UILabel?
    
    var isChecked: Bool = true {
        didSet {
            if isChecked {
                self.radioImage.image = UIImage(named: "mchoose")
            } else {
                self.radioImage.image = UIImage(named: "noChoose")
            }
        }
    }
    
    override var viewModel: BaseTableCellViewModel? {
        didSet {
            let dataDict = viewModel?.model as! NSMutableDictionary
            
            if let name = dataDict["Name"] {
                self.nameLabel.text = name as? String
            }
            let isCloud = dataDict["isCloud"] as! Bool
            self.cloudImage.isHidden = !(isCloud)
            if let birthday = dataDict["Birthday"] {
                self.birthdayLabel.text =  birthday as? String
            }
        }
    }
    
    var radioImage: UIImageView {
        get {
            if _radioImage == nil {
                _radioImage = UIImageView(image: UIImage(named: "mchoose"))
            }
            return _radioImage!
        }
    }
    
    var avatarImage: UIImageView {
        get {
            if _avatarImage == nil {
                _avatarImage = UIImageView()
            }
            return _avatarImage!
        }
    }
    
    var nameLabel: UILabel {
        get {
            if _nameLabel == nil {
                _nameLabel = UILabel()
                _nameLabel!.text = "name"
                _nameLabel!.textColor = UIColor.red
            }
            return _nameLabel!
        }
    }
    
    var cloudImage: UIImageView {
        get {
            if _cloudImage == nil {
                _cloudImage = UIImageView(image: UIImage(named: "newUser_yun"))
            }
            return _cloudImage!
        }
    }
    
    var birthdayLabel: UILabel {
        get {
            if _birthdayLabel == nil {
                _birthdayLabel = UILabel()
                _birthdayLabel!.text = "birthday"
            }
            return _birthdayLabel!
        }
    }

    required init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.contentView.addSubview(self.radioImage)
        self.contentView.addSubview(self.avatarImage)
        self.contentView.addSubview(self.cloudImage)
        self.contentView.addSubview(self.nameLabel)
        self.contentView.addSubview(self.birthdayLabel)
        
        self.radioImage.snp_makeConstraints { (make) -> Void in
            let _ = make.left.equalTo(self.contentView).offset(15)
            let _ = make.width.equalTo(30)
            let _ = make.height.equalTo(30)
            let _ = make.centerY.equalTo(self.contentView)
        }
        self.avatarImage.snp_makeConstraints { (make) -> Void in
            let _ = make.left.equalTo(self.radioImage.snp_right).offset(15)
            let _ = make.height.width.equalTo(40)
            let _ = make.centerY.equalTo(self.contentView)
        }
        self.nameLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.left.equalTo(self.avatarImage.snp_right).offset(10)
            let _ = make.centerY.equalTo(self.contentView)
        }
        self.cloudImage.snp_makeConstraints { (make) -> Void in
            let _ = make.left.equalTo(self.nameLabel.snp_right).offset(10)
            let _ = make.centerY.equalTo(self.contentView)
        }
        self.birthdayLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.right.equalTo(self.contentView).offset(-15)
            let _ = make.centerY.equalTo(self.contentView)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
