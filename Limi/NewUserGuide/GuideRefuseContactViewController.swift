//
//  GuideRefuseContactViewController.swift
//  Limi
//
//  Created by 倪晅 on 16/1/6.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class GuideRefuseContactViewController: GuideBaseViewController {
    
    fileprivate var _titleLabel: UILabel?
    fileprivate var _subTitleLabel: UILabel?
    fileprivate var _nextButton: UIButton?
    
    var titleLabel: UILabel {
        get {
            if _titleLabel == nil {
                _titleLabel = UILabel()
                _titleLabel!.text = "很遗憾小蜜现在不能帮你记生日了"
                _titleLabel!.numberOfLines = 0
                _titleLabel!.textAlignment = .center
            }
            return _titleLabel!
        }
    }
    
    var subTitleLabel: UILabel {
        get {
            if _subTitleLabel == nil {
                _subTitleLabel = UILabel()
                _subTitleLabel!.text = "以后你想发现通讯录中朋友的生日时，请到系统设置中打开给心意点点的通讯录授权哦。"
                _subTitleLabel!.numberOfLines = 0
                _subTitleLabel!.textAlignment = .center
            }
            return _subTitleLabel!
        }
    }
    
    var nextButton: UIButton {
        get {
            if _nextButton == nil {
                _nextButton = UIButton(type: .custom)
                _nextButton!.setTitle("开启我的心意之旅", for: UIControlState())
                _nextButton?.titleLabel?.font = UIFont.systemFont(ofSize: 15)
                _nextButton!.setTitleColor(UIColor.white, for: UIControlState())
                _nextButton!.backgroundColor = Constant.Theme.Color1
                _nextButton!.addTarget(self, action: #selector(GuideRefuseContactViewController.didTapNext), for: .touchUpInside)
                _nextButton!.layer.cornerRadius = 5
                _nextButton!.layer.masksToBounds = true
            }
            return _nextButton!
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(self.titleLabel)
        self.view.addSubview(self.subTitleLabel)
        self.view.addSubview(self.nextButton)
        
        self.titleLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.view).offset(100)
            let _ = make.left.equalTo(self.view).offset(8)
            let _ = make.right.equalTo(self.view).offset(-8)
            let _ = make.centerX.equalTo(self.view)
        }
        self.subTitleLabel
            .snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.titleLabel.snp_bottom).offset(20)
            let _ = make.left.equalTo(self.view).offset(8)
            let _ = make.right.equalTo(self.view).offset(-8)
            let _ = make.centerX.equalTo(self.view)
        }
        self.nextButton.snp_makeConstraints { (make) -> Void in
            let _ = make.centerX.equalTo(self.view)
            let _ = make.width.equalTo(298)
            let _ = make.height.equalTo(44)
            let _ = make.bottom.equalTo(self.view.snp_bottom).offset(-20)
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func didTapNext() {
        let _ = self.navigationController?.popViewController(animated: true)
    }

}
