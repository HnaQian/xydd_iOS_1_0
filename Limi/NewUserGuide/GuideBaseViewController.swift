//
//  GuideBaseViewController.swift
//  Limi
//
//  Created by 倪晅 on 16/1/5.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class GuideBaseViewController: BaseViewController {
    
    fileprivate var _addScoreView: GuideAddScoreView?
    fileprivate var _topDecorateView: UIImageView?
    
    var addScoreView: GuideAddScoreView {
        get {
            if _addScoreView == nil {
                _addScoreView = GuideAddScoreView()
                _addScoreView!.layer.opacity = 0
                _addScoreView!.isHidden = true
            }
            return _addScoreView!
        }
    }
    
    var topDecorateView: UIImageView {
        get {
            if _topDecorateView == nil {
                _topDecorateView = UIImageView(image: UIImage(named: "guide_top_decorate"))
            }
            return _topDecorateView!
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
       // self.view.backgroundColor = UIColor(rgba: "#FEFAFA")
        self.title = ""
        let bgImageView =  UIImageView(frame: self.view.frame)
        bgImageView.image = UIImage(named: "GuideBG")
        self.view.addSubview(bgImageView)
        self.view.addSubview(self.topDecorateView)
        self.view.addSubview(self.addScoreView)

        bgImageView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.left.right.bottom.equalTo(self.view)

        }
        
        
        self.topDecorateView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.left.right.equalTo(self.view)
            let _ = make.height.equalTo(90 * Constant.ScreenSize.SCREEN_WIDTH_SCALE)
        }
        self.addScoreView.snp_makeConstraints { (make) -> Void in
            let _ = make.bottom.equalTo(self.view).offset(-20)
            let _ = make.left.equalTo(self.view).offset(15)
            let _ = make.right.equalTo(self.view).offset(-15)
            let _ = make.height.equalTo(40)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.lt_setBackgroundColor(UIColor.clear)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController?.navigationBar.shadowImage = nil
        self.navigationController?.navigationBar.lt_reset()
        self.navigationController?.navigationBar.lt_setElementsAlpha(1)
        updateNaviBarColor()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func animateAddScore(_ completion: @escaping () -> Void) {
        self.view.setNeedsUpdateConstraints()
        self.view.updateConstraintsIfNeeded()
        self.addScoreView.isHidden = false
        UIView.animate(withDuration: 0.3,
            animations: { () -> Void in
                self.addScoreView.layer.opacity = 1
                self.addScoreView.snp_updateConstraints { (make) -> Void in
                    let _ = make.bottom.equalTo(self.view).offset(-40)
                }
                self.view.layoutIfNeeded()
            }, completion: { (finished) -> Void in
                UIView.animate(withDuration: 0.3,
                    animations: { () -> Void in
                        self.addScoreView.layer.opacity = 0
                        self.addScoreView.snp_updateConstraints { (make) -> Void in
                            let _ = make.bottom.equalTo(self.view).offset(-20)
                        }
                        self.view.layoutIfNeeded()
                    }, completion: { (finished) -> Void in
                })
                completion()
        }) 
    }
    
    func pushMainViewController() {
        NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.show_mainTabBarController), object: nil)
    }
    
}
