//
//  GuidGenderViewController.swift
//  Limi
//
//  Created by 倪晅 on 16/1/4.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class GuideGenderViewController: GuideBaseViewController {
    
    fileprivate var _titleLabel: UILabel?
    fileprivate var _subTitleLabel: UILabel?
    fileprivate var _askTitleLabel: UILabel?
    fileprivate var _nextButton: UIButton?
    fileprivate var _maleButton: UIButton?
    fileprivate var _maleImage: UIImageView?
    fileprivate var _maleLabel: UILabel?
    fileprivate var _femaleButton: UIButton?
    fileprivate var _femaleImage: UIImageView?
    fileprivate var _femaleLabel: UILabel?
    
    var titleLabel: UILabel {
        get {
            if _titleLabel == nil {
                _titleLabel = UILabel()
                _titleLabel!.text = "哈喽，欢迎来到心意点点！";
                _titleLabel!.textColor = Constant.Theme.Color2
                _titleLabel!.textAlignment = .center
                _titleLabel!.font = UIFont.systemFont(ofSize: 22)
            }
            return _titleLabel!
        }
    }
    
    var subTitleLabel: UILabel {
        get {
            if _subTitleLabel == nil {
                _subTitleLabel = UILabel()
                _subTitleLabel!.text = "我是你的贴心礼小蜜，介绍下你自己吧";
                _subTitleLabel!.textColor = Constant.Theme.Color6
                _subTitleLabel!.font = Constant.Theme.Font_15
                _subTitleLabel!.textAlignment = .center
                _subTitleLabel!.numberOfLines = 2
            }
            return _subTitleLabel!
        }
    }
    
    var askTitleLabel: UILabel {
        get {
            if _askTitleLabel == nil {
                _askTitleLabel = UILabel()
                _askTitleLabel!.text = "你是？"
                _askTitleLabel!.textAlignment = .center
                _askTitleLabel!.textColor = Constant.Theme.Color2
                _askTitleLabel!.font = Constant.Theme.Font_19
            }
            return _askTitleLabel!
        }
    }
    
    var maleButton: UIButton {
        get {
            if _maleButton == nil {
                _maleButton = UIButton.init(type: .custom)
                _maleButton!.addTarget(self, action: #selector(GuideGenderViewController.didTapNext(_:)), for: .touchUpInside)
                _maleButton!.tag = AddGiftReminderGender.male.rawValue
            }
            return _maleButton!
        }
    }
    
    var maleImage: UIImageView {
        get {
            if _maleImage == nil {
                _maleImage = UIImageView(image: UIImage(named: "guide_gender_male"))
            }
            return _maleImage!
        }
    }
    
    var maleLabel: UILabel {
        get {
            if _maleLabel == nil {
                _maleLabel = UILabel()
                _maleLabel!.text = "帅哥"
                _maleLabel!.textColor = Constant.Theme.Color6
                _maleLabel!.textAlignment = .center
                _maleLabel!.font = Constant.Theme.Font_15
            }
            return _maleLabel!
        }
    }
    
    var femaleButton: UIButton {
        get {
            if _femaleButton == nil {
                _femaleButton = UIButton.init(type: .custom)
                _femaleButton!.setTitleColor(Constant.Theme.Color6, for: UIControlState())
                _femaleButton!.addTarget(self, action: #selector(GuideGenderViewController.didTapNext(_:)), for: .touchUpInside)
                _femaleButton!.titleLabel?.font = Constant.Theme.Font_15
                _femaleButton!.tag = AddGiftReminderGender.female.rawValue
            }
            return _femaleButton!
        }
    }
    
    var femaleImage: UIImageView {
        get {
            if _femaleImage == nil {
                _femaleImage = UIImageView(image: UIImage(named: "guide_gender_female"))
            }
            return _femaleImage!
        }
    }
    
    var femaleLabel: UILabel {
        get {
            if _femaleLabel == nil {
                _femaleLabel = UILabel()
                _femaleLabel!.text = "美女"
                _femaleLabel!.textColor = Constant.Theme.Color6
                _femaleLabel!.textAlignment = .center
                _femaleLabel!.font = Constant.Theme.Font_15
            }
            return _femaleLabel!
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(self.titleLabel)
        self.view.addSubview(self.subTitleLabel)
        self.view.addSubview(self.askTitleLabel)
        self.view.addSubview(self.maleButton)
        self.view.addSubview(self.maleImage)
        self.view.addSubview(self.maleLabel)
        self.view.addSubview(self.femaleButton)
        self.view.addSubview(self.femaleImage)
        self.view.addSubview(self.femaleLabel)
        
        self.titleLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.topDecorateView.snp_bottom).offset(60 * Constant.ScreenSize.SCREEN_HEIGHT_SCALE)
            let _ = make.left.equalTo(self.view).offset(20)
            let _ = make.right.equalTo(self.view).offset(-20)
            let _ = make.height.equalTo(UIFont.systemFont(ofSize: 22).lineHeight)
        }
        self.subTitleLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.titleLabel.snp_bottom).offset(20 * Constant.ScreenSize.SCREEN_HEIGHT_SCALE)
            let _ = make.left.equalTo(self.view).offset(20)
            let _ = make.right.equalTo(self.view).offset(-20)
            let _ = make.height.equalTo(50)
        }
        self.askTitleLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.subTitleLabel.snp_bottom).offset(70 * Constant.ScreenSize.SCREEN_HEIGHT_SCALE)
            let _ = make.left.equalTo(self.view).offset(20)
            let _ = make.right.equalTo(self.view).offset(-20)
            let _ = make.height.equalTo(Constant.Theme.Font_19.lineHeight)
        }
        self.maleImage.snp_makeConstraints { (make) -> Void in
            let _ = make.right.equalTo(self.view.snp_centerX).offset(-45)
            let _ = make.top.equalTo(self.askTitleLabel.snp_bottom).offset(65 * Constant.ScreenSize.SCREEN_HEIGHT_SCALE)
            let _ = make.width.height.equalTo(74)
        }
        self.maleLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.maleImage.snp_bottom).offset(10)
            let _ = make.left.right.equalTo(self.maleImage)
            let _ = make.height.equalTo(Constant.Theme.Font_15.lineHeight)
        }
        self.maleButton.snp_makeConstraints { (make) -> Void in
            let _ = make.top.left.right.equalTo(self.maleImage)
            let _ = make.bottom.equalTo(self.maleLabel)
        }
        self.femaleImage.snp_makeConstraints { (make) -> Void in
            let _ = make.left.equalTo(self.view.snp_centerX).offset(45)
            let _ = make.top.equalTo(self.askTitleLabel.snp_bottom).offset(65 * Constant.ScreenSize.SCREEN_HEIGHT_SCALE)
            let _ = make.width.height.equalTo(74)
        }
        self.femaleLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.femaleImage.snp_bottom).offset(10)
            let _ = make.left.right.equalTo(self.femaleImage)
            let _ = make.height.equalTo(Constant.Theme.Font_15.lineHeight)
        }
        self.femaleButton.snp_makeConstraints { (make) -> Void in
            let _ = make.top.left.right.equalTo(self.femaleImage)
            let _ = make.bottom.equalTo(self.femaleLabel)
        }
    }
    
    func didTapNext(_ sender: UIButton) {
        let gender = sender.tag
        AccountManager.shared.setGender(Int64(gender), successHandler: { () -> Void in
            let vc = GuideBirthdayViewController()
            self.navigationController?.pushViewController(vc, animated: true)
            }) { () -> Void in
                Utils.showError(context: self.view, errorStr: "保存失败")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
