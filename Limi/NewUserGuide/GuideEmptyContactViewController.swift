//
//  GuideEmptyContactViewController.swift
//  Limi
//
//  Created by 倪晅 on 16/1/6.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class GuideEmptyContactViewController: BaseViewController {
    
    fileprivate var _titleLabel: UILabel?
    fileprivate var _subTitleLabel: UILabel?
    
    fileprivate var _fatherButton: UIButton?
    fileprivate var _motherButton: UIButton?
    fileprivate var _fatherAddedImage: UIImageView?
    fileprivate var _motherAddedImage: UIImageView?
    
    fileprivate var _nextButton: UIButton?
    
    fileprivate let GuideFamilyRelationButtonHeight = 74
    fileprivate let GuideFamilyRelationButtonWidth = 100
    
    var titleLabel: UILabel {
        get {
            if _titleLabel == nil {
                _titleLabel = UILabel()
                _titleLabel!.text = "好遗憾，没有找到有生日的联系人"
                _titleLabel!.textAlignment = .center
                _titleLabel?.numberOfLines = 2
                _titleLabel!.font = Constant.Theme.Font_17
                _titleLabel!.textColor = Constant.Theme.Color6
            }
            return _titleLabel!
        }
    }
    
    var subTitleLabel: UILabel {
        get {
            if _subTitleLabel == nil {
                _subTitleLabel = UILabel()
                _subTitleLabel!.text = "点击下面的图标添加亲人的生日"
                _subTitleLabel!.textAlignment = .center
                _subTitleLabel!.font = Constant.Theme.Font_15
                _subTitleLabel!.textColor = Constant.Theme.Color7
            }
            return _subTitleLabel!
        }
    }
    
    var fatherButton: UIButton {
        get {
            if _fatherButton == nil {
                _fatherButton = self.createFamilyButton()
                _fatherButton?.tag = AddGiftReminderRelation.father.rawValue
            }
            return _fatherButton!
        }
    }
    
    var motherButton: UIButton {
        get {
            if _motherButton == nil {
                _motherButton = self.createFamilyButton()
                _motherButton?.tag = AddGiftReminderRelation.mother.rawValue
            }
            return _motherButton!
        }
    }
    
    var fatherAddedImage: UIImageView {
        get {
            if _fatherAddedImage == nil {
                _fatherAddedImage = UIImageView(image: UIImage(named: "guide_family_added"))
                _fatherAddedImage!.isHidden = true
            }
            return _fatherAddedImage!
        }
    }
    
    var motherAddedImage: UIImageView {
        get {
            if _motherAddedImage == nil {
                _motherAddedImage = UIImageView(image: UIImage(named: "guide_family_added"))
                _motherAddedImage!.isHidden = true
            }
            return _motherAddedImage!
        }
    }
    
    var nextButton: UIButton {
        get {
            if _nextButton == nil {
                _nextButton = UIButton(type: .custom)
                _nextButton!.setTitle("进入送礼提醒", for: UIControlState())
                _nextButton!.setTitleColor(Constant.Theme.Color12, for: UIControlState())
                _nextButton!.backgroundColor = Constant.Theme.Color1
                _nextButton!.layer.cornerRadius = 5
                _nextButton!.layer.masksToBounds = true
                _nextButton!.addTarget(self, action: #selector(GuideEmptyContactViewController.didTapPop), for: .touchUpInside)
            }
            return _nextButton!
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = ""
        self.view.addSubview(self.titleLabel)
        if AccountManager.shared.isMontherAdd() == false || AccountManager.shared.isDadAdd() == false{
            self.view.addSubview(self.subTitleLabel)
            self.view.addSubview(self.fatherButton)
            self.view.addSubview(self.motherButton)
            self.view.addSubview(self.fatherAddedImage)
            self.view.addSubview(self.motherAddedImage)
        }

        self.view.addSubview(self.nextButton)
        
        self.titleLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.view).offset(178 * Constant.ScreenSize.SCREEN_HEIGHT_SCALE)
            let _ = make.left.equalTo(self.view).offset(20)
            let _ = make.right.equalTo(self.view).offset(-20)
        }
        if AccountManager.shared.isDadAdd() == false || AccountManager.shared.isMontherAdd() == false{
        self.subTitleLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.titleLabel.snp_bottom).offset(70 * Constant.ScreenSize.SCREEN_HEIGHT_SCALE)
            let _ = make.left.equalTo(self.view).offset(20)
            let _ = make.right.equalTo(self.view).offset(-20)
        }
        self.fatherButton.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.subTitleLabel.snp_bottom).offset(30 * Constant.ScreenSize.SCREEN_HEIGHT_SCALE)
            let _ = make.right.equalTo(self.view.snp_centerX).offset(-25)
            let _ = make.height.equalTo(GuideFamilyRelationButtonHeight)
            let _ = make.width.equalTo(GuideFamilyRelationButtonWidth)
        }
        self.fatherAddedImage.snp_makeConstraints { (make) -> Void in
            let _ = make.top.centerX.equalTo(self.fatherButton)
            let _ = make.width.height.equalTo(GuideFamilyRelationButtonHeight)
        }
        self.motherButton.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.fatherButton)
            let _ = make.left.equalTo(self.view.snp_centerX).offset(25)
            let _ = make.height.equalTo(GuideFamilyRelationButtonHeight)
            let _ = make.width.equalTo(GuideFamilyRelationButtonWidth)
        }
        self.motherAddedImage.snp_makeConstraints { (make) -> Void in
            let _ = make.top.centerX.equalTo(self.motherButton)
            let _ = make.width.height.equalTo(GuideFamilyRelationButtonHeight)
        }
        }
        self.nextButton.snp_makeConstraints { (make) -> Void in
            let _ = make.centerX.equalTo(self.view)
            let _ = make.width.equalTo(240)
            let _ = make.height.equalTo(44)
            let _ = make.bottom.equalTo(self.view).offset(-50)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.fatherAddedImage.isHidden = AccountManager.shared.isDadAdd() == true ? false : true
        self.motherAddedImage.isHidden = AccountManager.shared.isMontherAdd() == true ? false : true
        
        
//        self.navigationController?.navigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
//        self.navigationController?.navigationBarHidden = false
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        self.fatherButton.set(image: UIImage(named: "guide_family_father_small"), title: "老爸", titlePosition: .bottom, additionalSpacing: 10, state: UIControlState())
        self.motherButton.set(image: UIImage(named: "guide_family_mother_small"), title: "老妈", titlePosition: .bottom, additionalSpacing: 10, state: UIControlState())
    }
    
    func createFamilyButton() -> UIButton {
        let button: UIButton = UIButton(type: .custom)
        button.setTitleColor(UIColor(rgba: Constant.common_C6_color), for: UIControlState())
        button.titleLabel?.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        button.addTarget(self, action: #selector(GuideEmptyContactViewController.didTapFamily(_:)), for: .touchUpInside)
        
        return button
    }
    
    func didTapFamily(_ sender: UIButton) {
        let relation = sender.tag
        if relation == AddGiftReminderRelation.father.rawValue{
            if AccountManager.shared.isDadAdd() == true{
                return
            }
        }
        if relation == AddGiftReminderRelation.mother.rawValue{
            if AccountManager.shared.isMontherAdd() == true{
                return
            }
        }
        let vm = AddGiftReminderViewModel()
        vm.situation <- AddGiftReminderSituation.birthday.rawValue
        vm.relation <- relation
        let vc = AddGiftReminderViewController(withReminderViewModel: vm)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(GuideEmptyContactViewController.changeTitle),
            name: NSNotification.Name(rawValue: "Guide_Empty_Contact_Change_Title"),
            object: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func changeTitle(){
        titleLabel.text = "赞，你已添加了亲人的生日\n生日到来时别忘了送上祝福和礼物哦"
         NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "Guide_Empty_Contact_Change_Title"), object: nil)
    }
    
    func didTapPop() {
        let animation = CATransition()
        animation.duration = 0.3
        animation.type = kCATransitionReveal
        animation.subtype = kCATransitionFromBottom
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        self.navigationController?.view.layer.add(animation, forKey: "")
        let _ = self.navigationController?.popToRootViewController(animated: false)
    }

    override func backItemAction() {
//        super.backItemAction()
//        let vc = RemindViewController()
        let _ = self.navigationController?.popToRootViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
