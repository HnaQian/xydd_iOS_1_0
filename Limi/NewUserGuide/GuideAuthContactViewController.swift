//
//  GuideAuthContactViewController.swift
//  Limi
//
//  Created by 倪晅 on 16/1/6.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class GuideAuthContactViewController: BaseViewController {
    
    fileprivate var _titleLabel: UILabel?
    fileprivate var _heartTipView: GuideAuthContactTipView?
    fileprivate var _alarmTipView: GuideAuthContactTipView?
    fileprivate var _giftTipView: GuideAuthContactTipView?
    fileprivate var _helpLabel: UILabel?
    fileprivate var _importButton: UIButton?
    fileprivate var _closeButton: UIButton?
    var isFirstShow : Bool = false
    
    var titleLabel: UILabel {
        get {
            if _titleLabel == nil {
                _titleLabel = UILabel()
                _titleLabel!.text = "让小蜜帮你发现和记住朋友们的生日吧"
                _titleLabel!.textAlignment = .center
                _titleLabel!.numberOfLines = 0
                _titleLabel!.textColor = Constant.Theme.Color2
                _titleLabel!.font = Constant.Theme.Font_17
            }
            return _titleLabel!
        }
    }
    
    var heartTipView: GuideAuthContactTipView {
        get {
            if _heartTipView == nil {
                _heartTipView = GuideAuthContactTipView(imageName: "gift_reminder_auth_heart", tipText: "用心帮你记住", subTipText: "再也不会忘记重要生日")
            }
            return _heartTipView!
        }
    }
    
    var alarmTipView: GuideAuthContactTipView {
        get {
            if _alarmTipView == nil {
                _alarmTipView = GuideAuthContactTipView(imageName: "gift_reminder_auth_alarm", tipText: "温馨提醒", subTipText: "让你及时送上问候")
             }
            return _alarmTipView!
        }
    }
    
    var giftTipView: GuideAuthContactTipView {
        get {
            if _giftTipView == nil {
                _giftTipView = GuideAuthContactTipView(imageName: "gift_reminder_auth_gift", tipText: "个性化礼物推荐", subTipText: "让你生日送礼最有心意")
            }
            return _giftTipView!
        }
    }
    
    var helpLabel: UILabel {
        get {
            if _helpLabel == nil {
                _helpLabel = UILabel()
                _helpLabel!.textColor = Constant.Theme.Color7
                _helpLabel!.font = Constant.Theme.Font_13
                _helpLabel!.text = "发现生日需要读取您的手机通讯录"
            }
            return _helpLabel!
        }
    }
    
    var importButton: UIButton {
        get {
            if _importButton == nil {
                _importButton = UIButton(type: .custom)
                _importButton!.setTitle("立即发现", for: UIControlState())
                _importButton!.setTitleColor(Constant.Theme.Color12, for: UIControlState())
                _importButton!.backgroundColor = Constant.Theme.Color1
                _importButton!.titleLabel?.font = Constant.Theme.Font_17
                _importButton!.layer.cornerRadius = 5
                _importButton!.layer.masksToBounds = true
                _importButton!.addTarget(self, action: #selector(BaseViewController.didAuthAddressBook), for: .touchUpInside)
            }
            return _importButton!
        }
    }
    
    var closeButton: UIButton {
        get {
            if _closeButton == nil {
                _closeButton = UIButton(type: .custom)
                _closeButton!.setTitle("暂不处理", for: UIControlState())
                _closeButton!.setTitleColor(Constant.Theme.Color6, for: UIControlState())
                _closeButton!.backgroundColor = Constant.Theme.Color12
                _closeButton!.titleLabel?.font = Constant.Theme.Font_17
                _closeButton!.layer.cornerRadius = 5
                _closeButton!.layer.borderWidth = 1
                _closeButton!.layer.borderColor = Constant.Theme.Color7.cgColor
                _closeButton!.layer.masksToBounds = true
                _closeButton!.addTarget(self, action: #selector(GuideAuthContactViewController.didTapClose), for: .touchUpInside)
            }
            return _closeButton!
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = ""
        
        self.view.backgroundColor = UIColor.white
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.rightBarButtonItem = nil

        self.view.addSubview(self.titleLabel)
        self.view.addSubview(self.heartTipView)
        self.view.addSubview(self.alarmTipView)
        self.view.addSubview(self.giftTipView)
        self.view.addSubview(self.helpLabel)
        self.view.addSubview(self.importButton)
        self.view.addSubview(self.closeButton)
        
        self.titleLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.view).offset(70 * Constant.ScreenSize.SCREEN_HEIGHT_SCALE)
            let _ = make.left.equalTo(self.view).offset(20)
            let _ = make.right.equalTo(self.view).offset(-20)
        }
        self.heartTipView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.titleLabel.snp_bottom).offset(70 * Constant.ScreenSize.SCREEN_HEIGHT_SCALE)
            let _ = make.centerX.equalTo(self.view)
            let _ = make.height.equalTo(40)
            let _ = make.width.equalTo(210)
        }
        self.alarmTipView.snp_makeConstraints { (make) -> Void in
            let _ = make.left.equalTo(self.heartTipView)
            let _ = make.top.equalTo(self.heartTipView.snp_bottom).offset(20 * Constant.ScreenSize.SCREEN_HEIGHT_SCALE)
            let _ = make.height.equalTo(40)
        }
        self.giftTipView.snp_makeConstraints { (make) -> Void in
            let _ = make.left.equalTo(self.heartTipView)
            let _ = make.top.equalTo(self.alarmTipView.snp_bottom).offset(20 * Constant.ScreenSize.SCREEN_HEIGHT_SCALE)
            let _ = make.height.equalTo(40)
        }
        self.helpLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.giftTipView.snp_bottom).offset(70 * Constant.ScreenSize.SCREEN_HEIGHT_SCALE)
            let _ = make.centerX.equalTo(self.view)
            let _ = make.height.equalTo(Constant.Theme.Font_13.lineHeight)
        }
        self.importButton.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.helpLabel.snp_bottom).offset(20 * Constant.ScreenSize.SCREEN_HEIGHT_SCALE)
            let _ = make.centerX.equalTo(self.view)
            let _ = make.height.equalTo(44)
            let _ = make.width.equalTo(240)
        }
        self.closeButton.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self.importButton.snp_bottom).offset(20 * Constant.ScreenSize.SCREEN_HEIGHT_SCALE)
            let _ = make.centerX.equalTo(self.view)
            let _ = make.height.equalTo(44)
            let _ = make.width.equalTo(240)
        }
        
    }
    
    override func loadView() {
        super.loadView()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.isFirstShow = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if self.isFirstShow == true{
            var formerVC : UIViewController?
            for temp in self.navigationController!.viewControllers{
                if temp.isKind(of: GuideAuthContactViewController.self){
                    let animation = CATransition()
                    animation.duration = 0.3
                    
                    animation.type = kCATransitionReveal
                    
                    animation.subtype = kCATransitionFromBottom
                    
                    animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                    
                    self.navigationController?.view.layer.add(animation, forKey: "")
                    let _ = self.navigationController?.popToViewController(formerVC!, animated: false)
                }
                formerVC = temp
            }

        }
    }
    
    override func didAuthAddressBook() {
        self.isFirstShow = false
        if self.isAuthingAddressBook == false{
        super.didAuthAddressBook()
        }
    }
    
    
    func didTapClose() {
        self.isFirstShow = false
        var formerVC : UIViewController?
        for temp in self.navigationController!.viewControllers{
            if temp.isKind(of: GuideAuthContactViewController.self){
                let animation = CATransition()
                animation.duration = 0.3
                
                animation.type = kCATransitionReveal
                
                animation.subtype = kCATransitionFromBottom
                
                animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                
                self.navigationController?.view.layer.add(animation, forKey: "")
                let _ = self.navigationController?.popToViewController(formerVC!, animated: false)
            }
            formerVC = temp
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}



class GuideAuthContactTipView: UIView {
    
    fileprivate var _iconImage: UIImageView?
    fileprivate var _tipLabel: UILabel?
    fileprivate var _subTipLabel: UILabel?
    
    fileprivate var imageName: String
    fileprivate var tipText: String
    fileprivate var subTipText: String
    
    var iconImage: UIImageView {
        get {
            if _iconImage == nil {
                _iconImage = UIImageView(image: UIImage(named: self.imageName))
            }
            return _iconImage!
        }
    }
    
    var tipLabel: UILabel {
        get {
            if _tipLabel == nil {
                _tipLabel = UILabel()
                _tipLabel!.text = self.tipText
                _tipLabel!.textColor = Constant.Theme.Color6
                _tipLabel!.font = Constant.Theme.Font_17
            }
            return _tipLabel!
        }
    }
    
    var subTipLabel: UILabel {
        get {
            if _subTipLabel == nil {
                _subTipLabel = UILabel()
                _subTipLabel!.text = self.subTipText
                _subTipLabel!.textColor = Constant.Theme.Color7
                _subTipLabel!.font = Constant.Theme.Font_15
            }
            return _subTipLabel!
        }
    }
    
    init(imageName: String, tipText: String, subTipText: String) {
        self.imageName = imageName
        self.tipText = tipText
        self.subTipText = subTipText
        super.init(frame: CGRect.zero)
        
        self.addSubview(self.iconImage)
        self.addSubview(self.tipLabel)
        self.addSubview(self.subTipLabel)
        
        self.iconImage.snp_makeConstraints { (make) -> Void in
            let _ = make.left.equalTo(self)
            let _ = make.centerY.equalTo(self)
        }
        self.tipLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.left.equalTo(self.iconImage.snp_right).offset(10)
            let _ = make.top.equalTo(self)
        }
        self.subTipLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.left.equalTo(self.tipLabel)
            let _ = make.bottom.equalTo(self)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
