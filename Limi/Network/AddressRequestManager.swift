//
//  AddressRequestManager.swift
//  Limi
//
//  Created by Richie on 16/3/10.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//有关地址的各类请求

import UIKit

public enum AddressRequestType:Int
{
    case loadList   //加载列表
    case update     //编辑
    case plus       //添加
    case delete     //删除
    case setDefault //设置为默认
}

///列表
private let LIST = Constant.JTAPI.address_list

/// 编辑
private let UPDATE = Constant.JTAPI.address_update

///删除
private let DELETE = Constant.JTAPI.address_del

///设置默认
private let DEFAULT = Constant.JTAPI.address_default

///添加
private let PLUS = Constant.JTAPI.address_add

class AddressRequestManager: LMNetWork {

    struct staticStruct
    {
        static var net:AddressRequestManager!
        static var once:Int = 0
    }
    
    private static var __once: () = { () -> Void in
            if AddressRequestManager.staticStruct.net == nil{staticStruct.net = AddressRequestManager()}
        }()

    fileprivate let urlMap = [
        AddressRequestType.loadList:LIST,
        AddressRequestType.update:UPDATE,
        AddressRequestType.delete:DELETE,
        AddressRequestType.setDefault:DEFAULT,
        AddressRequestType.plus:PLUS
    ]
    
    fileprivate var deletedIndex = 10086

    class func shareInstance(_ delegate:AddressRequestManagerDelegate? = nil) ->AddressRequestManager
    {
        _ = AddressRequestManager.__once
        
        staticStruct.net.delegate = delegate
        
        return staticStruct.net
    }
    
    
    class func newInstance(_ delegate:LMNetWorkDelegate? = nil) ->AddressRequestManager
    {
        let manager = AddressRequestManager()
        manager.delegate = delegate
        return manager
    }
    
    fileprivate var _delegate:AddressRequestManagerDelegate?{return (self.delegate as? AddressRequestManagerDelegate)}
    
    ///加载列表
    func addressLoadList(_ context:UIView,isShowErrorStatuMsg:Bool? = false ,isNeedHud : Bool? = false,hudTitle: String? = nil)
    {
        self.requestData(context:context,URLString: LIST, parameters: ["page": 0 as AnyObject, "size": 20 as AnyObject], isNeedUserToken: true,isShowErrorStatuMsg:isShowErrorStatuMsg, isNeedHud: isNeedHud, hudTitle: hudTitle).taskSign(LIST)
    }
    
    ///编辑
    func addressEdit(_ context:UIView,parameters: [String: AnyObject],isShowErrorStatuMsg:Bool? = false ,isNeedHud : Bool? = false,hudTitle: String? = nil)
    {
        self.requestData(context:context,URLString: UPDATE, parameters: parameters, isNeedUserToken: true,isShowErrorStatuMsg:isShowErrorStatuMsg, isNeedHud: isNeedHud, hudTitle: hudTitle).taskSign(UPDATE)
    }
    
    
    ///添加
    func addressPlusNew(_ context:UIView,parameters: [String: AnyObject],isShowErrorStatuMsg:Bool? = false ,isNeedHud : Bool? = false,hudTitle: String? = nil)
    {
        self.requestData(context:context,URLString: PLUS, parameters: parameters, isNeedUserToken: true,isShowErrorStatuMsg:isShowErrorStatuMsg, isNeedHud: isNeedHud, hudTitle: hudTitle).taskSign(PLUS)
    }

    ///添加
    func addressPlusNewWithModel(_ context:UIView,addressModel:AddressMode,isShowErrorStatuMsg:Bool? = false ,isNeedHud : Bool? = false,hudTitle: String? = nil)
    {
        /*
        参数名称	必选	类型及范围	说明
        token	true	string	登录token值
        name	true	string	姓名
        mobile	true	string	手机号码
        province_id	true	int	省id
        city_id	true	int	市id
        area_id	true	int	区id
        address	true	string	地址
        is_default	true	int	设为默认地址 1是 2否
        
        */
        
        
        var params = [String: AnyObject]()
        params["name"] = addressModel.name as AnyObject?
        params["mobile"] = addressModel.mobile as AnyObject?
        params["province_id"] = addressModel.province_id as AnyObject?
        params["city_id"] = addressModel.city_id as AnyObject?
        params["area_id"] = addressModel.area_id as AnyObject?
        params["address"] = addressModel.address as AnyObject?
        params["is_default"] = 2 as AnyObject?
        
        self.requestData(context:context,URLString: PLUS, parameters: params, isNeedUserToken: true,isShowErrorStatuMsg:isShowErrorStatuMsg, isNeedHud: isNeedHud, hudTitle: hudTitle).taskSign(PLUS)
    }
    
    ///删除地址
    func addressDelete(_ context:UIView,addressId: Int,isShowErrorStatuMsg:Bool? = false ,isNeedHud : Bool? = false,hudTitle: String? = nil,index:Int = 10086)
    {
        //["id": info.id]
        self.deletedIndex = index
        self.requestData(context:context,URLString: DELETE, parameters: ["id":addressId as AnyObject], isNeedUserToken: true,isShowErrorStatuMsg:isShowErrorStatuMsg, isNeedHud: isNeedHud, hudTitle: hudTitle).taskSign(DELETE)
    }
    
    ///设置默认
    func addressSetDefault(_ context:UIView,addressId: Int,isShowErrorStatuMsg:Bool? = false ,isNeedHud : Bool? = false,hudTitle: String? = nil)
    {
        self.requestData(context:context,URLString: DEFAULT, parameters: ["id": addressId as AnyObject], isNeedUserToken: true,isShowErrorStatuMsg:isShowErrorStatuMsg, isNeedHud: isNeedHud, hudTitle: hudTitle).taskSign(DEFAULT)
    }
    
    
    override func requestSuccess(_ manager: LMRequestManager, result: LMResultMode)
    {
       self.success(manager,result: result)
    }
    
    
    func success(_ manager: LMRequestManager, result: LMResultMode)
    {
        if manager.taskSign == nil{return}
        
        let sign = manager.taskSign!
        //删除
        if sign == DELETE
        {
            let _ = Delegate_Selector(_delegate, #selector(AddressRequestManagerDelegate.addressRequestManagerDidDeleteSuccess(_:result:index:))){[unowned self] in self._delegate!.addressRequestManagerDidDeleteSuccess!(manager, result: result,index:self.deletedIndex)}
        }//设置默认
        else if sign == DEFAULT
        {
           let _ = Delegate_Selector(_delegate, #selector(AddressRequestManagerDelegate.addressRequestManagerDidDefaultSuccess(_:result:))){[unowned self] in self._delegate!.addressRequestManagerDidDefaultSuccess!(manager, result: result)}
        }//添加
        else if sign == PLUS
        {
           let _ = Delegate_Selector(_delegate, #selector(AddressRequestManagerDelegate.addressRequestManagerDidPlusSuccess(_:result:))){[unowned self] in self._delegate!.addressRequestManagerDidPlusSuccess!(manager, result: result)}
        }//更新
        else if sign == UPDATE
        {
           let _ = Delegate_Selector(_delegate, #selector(AddressRequestManagerDelegate.addressRequestManagerDidEditSuccess(_:result:))){[unowned self] in self._delegate!.addressRequestManagerDidEditSuccess!(manager, result: result)}
        }//列表
        else if sign == LIST
        {
            var addressList = [AddressMode]()

            if let list = result.data?["list"] as? [[String: AnyObject]]{
                if list.count > 0{
                let _  = list.map{addressList.append(AddressMode($0))}
                }
            }
           let _ = Delegate_Selector(_delegate, #selector(AddressRequestManagerDelegate.addressRequestManagerDidLoadListFinish(_:result:addressList:))){[unowned self] in self._delegate!.addressRequestManagerDidLoadListFinish!(manager, result: result,addressList:addressList)}
        }
    }
}


@objc protocol AddressRequestManagerDelegate:LMNetWorkDelegate
{
    @objc optional func addressRequestManagerDidDeleteSuccess(_ requestManager:LMRequestManager,result:LMResultMode,index:Int)
    
    @objc optional func addressRequestManagerDidEditSuccess(_ requestManager:LMRequestManager,result:LMResultMode)
    
    @objc optional func addressRequestManagerDidPlusSuccess(_ requestManager:LMRequestManager,result:LMResultMode)
    
    @objc optional func addressRequestManagerDidDefaultSuccess(_ requestManager:LMRequestManager,result:LMResultMode)
    
    @objc optional func addressRequestManagerDidLoadListFinish(_ requestManager:LMRequestManager,result:LMResultMode,addressList:[AddressMode])
}

