//
//  SystemInfoRequestManager.swift
//  Limi
//
//  Created by Richie on 16/3/21.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class SystemInfoRequestManager: LMNetWork {
    
    struct staticStruct{
        static var systemInfo:SystemInfoRequestManager!
        static var once:Int = 0
    }
    
    private static var __once: () = { () -> Void in
            SystemInfoRequestManager.staticStruct.systemInfo = SystemInfoRequestManager()
        }()
    
    
    
    /*
     "service_phone": 021-68411255,
     "version_url": ,
     "user_protocol": http: //test.wechat.giftyou.me/issue_serviceRule.html,
     "best_introduction": http: //test.wechat.giftyou.me/issue_goodsBest.html,
     "apple_pay": 1,
     "birthday_wiki": http: //test.wechat.giftyou.me/app/birthdayWiki,
     "gift_holiday": http: //test.wechat.giftyou.me/giftremind/holiday?id=,
     "gift_scene": http: //test.wechat.giftyou.me/giftremind/scene?id=,
     "gift_service": http: //test.wechat.giftyou.me/giftserver/list?need_token=1&ids=0,
     0,
     0,
     "my_birthday_wiki": http: //test.wechat.giftyou.me/app/myBirthdayWiki,
     "share_logo": http: //up.xydd.co/share_logo.png
     
     
     "auto_update": 1,
     "version_size": 发生发的是,
     "version_force": 2,
     "version_number": 1309,
     "version_info": 放大师傅师傅范德萨发撒旦,
     
     auto_update	true是否执行自动更新，false不执行
     version_force	强制更新 1是，2否
     version_info	更新说明
     version_number	版本号
     version_size	版本大小
     */
    
    
    //	存在并且为1时显示apple支付
    fileprivate let APPLE_PAY       = "apple_pay"
    //  share_logo
    fileprivate let SHARE_LOGO      = "share_logo"
    //  user_protocol
    fileprivate let USER_PROTOCOL   = "user_protocol"
    //  service_phone
    fileprivate let SERVICE_PHONE   = "service_phone"
    //  是否自动更新
    fileprivate let AUTO_UPDATE     = "auto_update"
    //  版本信息
    fileprivate let VERSION_INFO    = "version_info"
    //  版本尺寸
    fileprivate let VERSION_SIZE    = "version_size"
    //  是否强制更新
    fileprivate let VERSION_FORCE   = "version_force"
    //  版本号
    fileprivate let VERSION_NUMBER  = "version_number"
    //	心意服务地址
    fileprivate let WISHSEND_SERVICE    = "gift_service"
    //  我的生日的百科地址
    fileprivate let BIRTHDAY_WIKI_OWEN  = "my_birthday_wiki"
    //  最新推荐
    fileprivate let BEST_INSTRODUCTION  = "best_introduction"
    //	联系人的生日百科地址
    fileprivate let BIRTHDAY_WIKI_OTHER = "birthday_wiki"
    //	联系人的送礼详情地址
    fileprivate let GIFTREMIND_DETAIL_SENCE     = "gift_scene"
    //	节日的送礼详情地址
    fileprivate let GIFTREMIND_DETAIL_HOLIDAY   = "gift_holiday"
    
    //礼品包装服务地址
    fileprivate let SERVICE_LIST_INTRODUCTION = "gift_introduction"
    
    
    var didLoadSuccess :Bool{return _didLoadSuccess}
    
    fileprivate var _didLoadSuccess = false
    
    
    //要不要更新
    var auto_update: Bool{
        if let num = self.getIntNumberWithKey(AUTO_UPDATE){
            if num == 1{
                return true
            }
            else{
               return false
            }
        }
        return false
    }
    
    var version_info: String{
        if let info = self.getContentWithKey(VERSION_INFO){
            return info;
        }
        return "更新了"
    }
    
    var version_size: String{return self.getContentWithKey(VERSION_SIZE) ?? "12.2M"}
    //是否强制更新
    var version_force: Bool{
        
        if let num = self.getIntNumberWithKey(VERSION_FORCE){
            if num == 1{
                return true
            }
            else{
                return false
            }
        }
        return false
        
    }
    
    
    var version_number: String{
        var numStr = "0.0.00"
        if let num = self.getIntNumberWithKey(VERSION_NUMBER){
            numStr = "\(num)"
            let str_0 = NSString(string: numStr).substring(to: 1)
            let str_1 = NSString(string: NSString(string: numStr).substring(to: 2)).substring(from: 1)
            let str_rest = NSString(string:NSString(string: numStr).substring(from: 2)).integerValue
            numStr = str_0 + "." + str_1 + "." + "\(str_rest)"
        }
        
        return numStr
    }
    
    var birthday_wiki_owen: String?{return self.getContentWithKey(BIRTHDAY_WIKI_OWEN)}
    
    var birthday_wiki_other: String{return self.getContentWithKey(BIRTHDAY_WIKI_OTHER) != nil ? self.getContentWithKey(BIRTHDAY_WIKI_OTHER)! : "http://wechat.giftyou.me/app/birthdayWiki"}
    
    var giftRemind_detail_sence: String?{return self.getContentWithKey(GIFTREMIND_DETAIL_SENCE)}
    
    var giftRemind_detail_holid: String?{return self.getContentWithKey(GIFTREMIND_DETAIL_HOLIDAY)}
    
    var wishsend_service: String?{
        return self.preParserURLWithNotInMap(self.getContentWithKey(WISHSEND_SERVICE)!)
    }
    
    var service_list_introduction:String?{return self.getContentWithKey(SERVICE_LIST_INTRODUCTION)}
    
    var best_instroduction:String?{return self.getContentWithKey(BEST_INSTRODUCTION)}
    
    // "http://wechat.giftyou.me/issue_1.html"
    var user_protocol:String{return self.getContentWithKey(USER_PROTOCOL) != nil ? self.getContentWithKey(USER_PROTOCOL)! : "http://wechat.giftyou.me/issue_1.html"}
    
    //客服号码
    var service_number:String!{
        var mobile = "021-68411255"
        if let mob = self.getContentWithKey(SERVICE_PHONE){
            mobile = mob
        }
        return mobile}
    
    //分享时需要传的logo
    var share_logo:String?{return self.getContentWithKey(SHARE_LOGO)}
    
    //是否打开applepay
    var apple_pay:Int{
        var applePay:Int = 0
        if dataDict == nil{
            self.requestSystemInfo()
        }else{
            if let payVal = dataDict![APPLE_PAY],
                let pay = payVal as? String{
                applePay = pay.intValue
            }
        }
        return applePay
    }
    
    
    class func shareInstance()->SystemInfoRequestManager
    {
        
        _ = SystemInfoRequestManager.__once
        
        return staticStruct.systemInfo
    }
    
    
    
    //数据
    fileprivate var dataDict:[String:AnyObject]?
    {
        if let data = UserDefaults.standard.value(forKey: "systemSettingInfo"),
            let dic = data as? [String:AnyObject]{
            return dic
        }else{
            requestSystemInfo()
        }
        return nil
    }
    
    
    
    func getContentWithKey(_ key:String) ->String?
    {
        var content:String?
        
        if dataDict == nil{
            self.requestSystemInfo()
        }else{
            content = dataDict![key] as? String
        }
        return content
    }
    
    func getIntNumberWithKey(_ key:String) ->Int?
    {
        var unmber:Int?
        if dataDict == nil{
            self.requestSystemInfo()
        }else{
            unmber = dataDict![key] as? Int
        }
        return unmber
    }
    
    func getFloatNumberWithKey(_ key:String) ->Float?
    {
        var unmber:Float?
        if dataDict == nil{
            self.requestSystemInfo()
        }else{
            unmber = dataDict![key] as? Float
        }
        return unmber
    }
    
    
    func getBoolWithKey(_ key:String) ->Bool?
    {
        var unmber:Bool?
        if dataDict == nil{
            self.requestSystemInfo()
        }else{
            unmber = dataDict![key] as? Bool
        }
        return unmber
    }
    
    
    func preParserURLWithNotInMap(_ urlStr: String) ->String {
        
        var url: URL!
        print(urlStr)
        
        if let tempUrl = URL(string: urlStr){
            url = tempUrl
            
        }else{ return ""}
        
        let newUrl:String = url.scheme! + "://" + url.host! + url.path + "?"
        
        var paramt:[String:AnyObject]!
        
        if let param : NSDictionary = CMDQueryStringSerialization.dictionary(withQueryString: url.query) as NSDictionary?{
            paramt = param as! [String : AnyObject]
            if let app: AnyObject = param.object(forKey: "need_token") as AnyObject?{
                if "\(app)" == "1" {
                    
                }
            }
        }
        
        
        var paramString = ""
        
        for (key, value) in paramt{
            if key != "ids"{
                paramString = key + "=\(value)&"
            }
        }
        
        if let value = paramt["need_uid"] as? Int{
            if value == 1{
                //放入token
                let uid : Int = UserDefaults.standard.integer(forKey: "UserUid")
                paramString = paramString + "uid" + "=\(uid)&"
            }
        }
        
        
        if let value = paramt["need_token"] as? String {
            if value.intValue == 1{
                //放入token
                if let token : String = UserDefaults.standard.object(forKey: "UserToken") as? String{
                    paramString = paramString + "token" + "=\(token)&"
                }
            }
        }
        
        paramString = paramString + "ids="
        print(newUrl + paramString)
        return newUrl + paramString
    }
    
    
    ///请求更新配置信息
    func requestSystemInfo(){
       let _ = self.requestData(HTTPMethod.get,context: UIApplication.shared.keyWindow!, URLString: Constant.JTAPI.setting_info)
    }
    
    override func requestSuccess(_ manager: LMRequestManager, result: LMResultMode) {
        
        if result.status == 200{
            _didLoadSuccess = true
            UserDefaults.standard.setValue(result.data!, forKey: "systemSettingInfo")
            UserDefaults.standard.synchronize()
            print(result.data)
            if let versionNum = self.getIntNumberWithKey(VERSION_NUMBER){
                
                if versionNum > Int(Constant.versionCode){
                    
                    let _ = AppUpdateView.checkUpdateShow()
                }
                
            }
        }
    }
}
