//
//  LMRequestManager.swift
//  Limi
//
//  Created by guo chen on 15/11/23.
//  Copyright © 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//



import UIKit
import CoreData

enum FavouriteType: Int {
    case goods = 1
    case article = 2
    case leaderboard = 3
    case packageItem = 4
}


class LMRequestManager: NSObject,NSURLConnectionDataDelegate
{
    private static var __once1: () = {

            let configuration = URLSessionConfiguration.default
            
            let acceptEncoding: String = "gzip;q=1.0,compress;q=0.5"
            
            let acceptLanguage: String = {
                var components: [String] = []
                for (index, languageCode) in (Locale.preferredLanguages as [String]).enumerated() {
                    let q = 1.0 - (Double(index) * 0.1)
                    components.append("\(languageCode);q=\(q)")
                    if q <= 0.5 {
                        break
                    }
                }
                
                return components.joined(separator: ",")
            }()
            
            // User-Agent Header; see https://tools.ietf.org/html/rfc7231#section-5.5.3
            let infoDict = Bundle.main.infoDictionary
            var appBuild = ""
            if let info = infoDict {    // app版本号
                appBuild = info["CFBundleShortVersionString"] as! String!
            }
        //旧的UA规则
//            let userAgent: String = "iOS" + ";" + UIDevice.current.model + ";" + UIDevice.current.systemVersion + ";" + UIDevice.current.systemName + ";"  + appBuild
        //新的UA规则
        let userAgent: String = "XYDD" + " " + appBuild + " (\(UIDevice.current.model); \(UIDevice.current.systemName) \(UIDevice.current.systemVersion); \(NSLocale.current.identifier))"
        var originToken = UserDefaults.standard.string(forKey: "UserToken")
        var token = ""
        if let temp = originToken {
                token = temp
            }else {
                token = ""
        }
        configuration.httpAdditionalHeaders = [
                "Accept-Encoding": acceptEncoding,
                "Accept-Language": acceptLanguage,
                "User-Agent": userAgent,
                //五个参数
                "OI-APPKEY":Constant.DEVICE_APPKEY,
                "OI-AUTH":token,
                "OI-UDID":UIDevice().deviceId(),
                "OI-APIVER":"24",
                "OI-CHN":UIDevice().channelId()
            ]
          LMRequestManager.Static.instance = SessionManager(configuration: configuration)
        }()
    private static var __once: () = {ShareStatic.instance = LMRequestManager()}()
    var hud : ProgressHUD?
    let reachability = Reachability()
    struct ShareStatic
    {
        static var instance : LMRequestManager? = nil
        static var once : Int = 0
    }

    class func shareRequestManager() ->LMRequestManager
    {
        _ = LMRequestManager.__once
        
        return ShareStatic.instance!
    }
    
    
    struct Static
    {
        static var instance : SessionManager? = nil
        static var once : Int = 0
    }
    //MARK: shareAlamofireManager
    func shareAlamofireManager() ->SessionManager
    {
        _ = LMRequestManager.__once1
        
        return Static.instance!
    }
    
    
    class func newManager() ->LMRequestManager
    {
        let newManager = LMRequestManager()

        return newManager
    }
    
    var request:DataRequest!
    
    var requestMode = LMRequestMode()
    
    var taskID:Int{return requestMode.taskID}
    
    var resultMode:LMResultMode!
    
    var taskSign:String?
    
    ///*数据初步分析 data:为原始数据 从data中解析出：statue,msg*/
    class func requestByDataAnalyses(_ method: HTTPMethod = .post, context:UIView,URLString: URLConvertible,parameters: [String: AnyObject]? = nil,isNeedUserTokrn : Bool? = false,isNeedAppTokrn : Bool? = false,isShowErrorStatuMsg:Bool? = false ,isShowSuccessStatuMsg:Bool? = false ,isNeedHud : Bool? = false,hudTitle: String? = nil ,encoding: URLEncoding = URLEncoding.default ,headers: [String: String]? = nil,successHandler: ((_ data:[String: AnyObject],_ status:Int,_ msg:String) ->Void)? = nil,failureHandler:(()->Void)? = nil) ->LMRequestManager
    {
      let _ = self.shareRequestManager().request(method,context:context, URLString: URLString, parameters: parameters, isNeedUserTokrn: isNeedUserTokrn, isNeedAppTokrn: isNeedAppTokrn,isShowErrorStatuMsg:isShowErrorStatuMsg,isShowSuccessStatuMsg:isShowSuccessStatuMsg,isNeedHud: isNeedHud, hudTitle: hudTitle, encoding: encoding, headers: headers)
        
        
       let _ = self.shareRequestManager().analyseData(successHandler,failureHandler:failureHandler)
        
        return self.shareRequestManager()
    }
    
    //请求和处理数据
    class func requestWithModeByDataAnalyse(_ context:UIView,requestMode:LMRequestMode) ->LMRequestManager
    {
        return self.requestByDataAnalyses(requestMode.method,context:context, URLString: requestMode.URLString, parameters: requestMode.parameters, isNeedUserTokrn: requestMode.isNeedUserToken, isNeedAppTokrn: requestMode.isNeedAppToken, isShowErrorStatuMsg: requestMode.isShowErrorStatuMsg, isNeedHud: requestMode.isNeedHud, hudTitle: requestMode.hudTitle, encoding: requestMode.encoding, headers: requestMode.headers, successHandler: requestMode.successHandler, failureHandler: requestMode.failureHandler)
    }
    

    //MARK:创建请求
    func request(_ method: HTTPMethod = .post,context:UIView,URLString: URLConvertible,parameters: [String: AnyObject]? = nil,isNeedUserTokrn : Bool? = false,isNeedAppTokrn : Bool? = false,isShowErrorStatuMsg:Bool? = false ,isShowSuccessStatuMsg:Bool? = false ,isNeedHud : Bool? = false,hudTitle: String? = nil ,encoding: URLEncoding = URLEncoding.default,headers: [String: String]? = nil) ->LMRequestManager
    {
        self.requestMode.method = method
        self.requestMode.context = context
        self.requestMode.URLString = URLString
        self.requestMode.parameters = parameters
        self.requestMode.isNeedUserToken = isNeedUserTokrn
        self.requestMode.isNeedAppToken = isNeedAppTokrn
        self.requestMode.isShowErrorStatuMsg = isShowErrorStatuMsg
        self.requestMode.isShowSuccessStatuMsg = isShowSuccessStatuMsg
        self.requestMode.isNeedHud = isNeedHud
        self.requestMode.hudTitle = hudTitle
        self.requestMode.encoding = encoding
        self.requestMode.headers = headers
        
        return self.requestWithMode(self.requestMode)
    }
    
    //创建请求
    func requestWithMode(_ requestMode:LMRequestMode) ->LMRequestManager
    {
        if self.requestMode.isNeedUserToken!//如果需要token ，及时parameters没有传也会加上
        {
            if UserInfoManager.didLogin == true{
                if self.requestMode.parameters != nil
                {
                    self.requestMode.parameters!["token"] = UserDefaults.standard.string(forKey: "UserToken")! as AnyObject?
                }else
                {
                    self.requestMode.parameters = [String:AnyObject]()
                    self.requestMode.parameters!["token"] = UserDefaults.standard.string(forKey: "UserToken")! as AnyObject?
                }
            }
        }
        
        
        
        if  self.requestMode.isNeedAppToken!//如果需要ParamNeedAPPToken ，及时parameters没有传也会加上
        {
            if self.requestMode.parameters != nil
            {
                if let token :String = UserDefaults.standard.string(forKey: "deviceToken")
                {
                    self.requestMode.parameters!["app_token"] = token as AnyObject?
                }
            }else
            {
                self.requestMode.parameters = [String:AnyObject]()
                
                if let token :String = UserDefaults.standard.string(forKey: "deviceToken")
                {
                    self.requestMode.parameters!["app_token"] = token as AnyObject?
                }
            }
        }
        
        if self.requestMode.isNeedHud! {
            hud = ProgressHUD(view: self.requestMode.context!)
            self.requestMode.context!.addSubview(hud!)
            hud!.show(true)
        }
        
        self.request = self.shareAlamofireManager().request(self.requestMode.URLString, method:self.requestMode.method,parameters: self.requestMode.parameters,encoding: self.requestMode.encoding,headers: self.requestMode.headers)
        self.requestMode.taskID = (self.request.task?.taskIdentifier)!
        
        return self
    }
    
    
    //MARK: 处理请求结果
    func analyseData(_ successHandler: ((_ data:[String: AnyObject],_ status:Int,_ msg:String) ->Void)? = nil,failureHandler:(()->Void)? = nil) ->LMRequestManager
    {
        if self.request == nil{fatalError("还没有创建请求")}
        
        self.requestMode.successHandler = successHandler
        self.requestMode.failureHandler = failureHandler
        self.analyseResponse{ manager,response  in
            switch response.result
            {
            case .success(let data):
                
                self.resultMode = LMResultMode(rootData: data as AnyObject)
                
                if let subData = (data as AnyObject)["data"] as? NSDictionary
                {
                    if let token = subData["token"]{
                            UserDefaults.standard.set(token, forKey: "UserToken")
                    }
                    
                    if let uid = subData["uid"] as? Int{
                        UserDefaults.standard.set(uid, forKey: "UserUid")
                    }
                    UserDefaults.standard.synchronize()
                }
                
                var status = 0
                if let sta = (data as AnyObject)["status"] as? Int
                {
                    status = sta
                }
                
                var status_msg = ""
                if let msg = (data as AnyObject)["msg"] as? String
                {
                    if self.requestMode.isShowErrorStatuMsg!
                    {
                        if status != 200 && status != 400 && self.requestMode.context != nil
                        {
                            Utils.showError(context: self.requestMode.context!, errorStr: msg)
                        }
                    }
                    
                    if self.requestMode.isShowSuccessStatuMsg!
                    {
                        if status == 200 && self.requestMode.context != nil
                        {
                            Utils.showError(context: self.requestMode.context!, errorStr: msg)
                        }
                    }
                    status_msg = msg
                }
                
                
                
                if successHandler != nil
                {
                    successHandler!((data as? [String: AnyObject])!,status,status_msg)
                }
                
                
            case .failure(let error):
                print(error._code)
                if error._code == NSURLErrorCancelled {
                    return
                }
                if failureHandler != nil
                {
                    failureHandler!()
                }

                if (self.reachability?.isReachable)! {
                    Utils.showError(context: UIApplication.shared.keyWindow!, errorStr:Constant.NETWORK_DATA_ERR)
                }else {
                  //无网络
                    Utils.showError(context: UIApplication.shared.keyWindow!, errorStr:Constant.NETWORK_NO)
                }
            }
        }
        
        return self
    }
    
    
    func analyseResponse(_ responseBlock:@escaping (LMRequestManager,DataResponse<Any>) ->Void)
    {
        if self.requestMode.isNeedHud! {
            
            DispatchQueue.main.async(execute: {
                self.hud!.hide(true)
            })
        }
 
     let _ = self.request.responseJSON{response in responseBlock(self,response)}
    }
    
    
}


extension LMRequestManager
{
    //添加标记
    func taskSign(_ sign:String)
    {
        self.taskSign = sign
    }
    
    ///刷新数据
    func refreshData()
    {
        self.request.resume()
    }
    
    //终止数据请求
    func terminateNetWorkTask()
    {
        self.request.cancel()
    }
    
    //暂停数据请求
    func interruptionNetWorkTask()
    {
        self.request.suspend()
    }
    
    
    //恢复请求
    func resumeNetWorkTask()
    {
        self.request.resume()
    }
}



class OriginalDownLoad:NSObject,NSURLConnectionDataDelegate
{
    let receiveData = NSMutableData()
    
    var urlString:String!
    
    var myConnection:NSURLConnection!
    
    var completionHandle:((_ data:Data?)->Void)?
    
    var hud : ProgressHUD?
    
    func downloadWithUrlString(context:UIView,shouldShowHud:Bool = true,urlString:String,completionHandle:@escaping ((_ data:Data?)->Void))
    {
            hud = ProgressHUD(view: context)
        if shouldShowHud {
            context.addSubview(hud!)
            hud!.show(true)
        }
      
        //设置下载url的字符串
        self.urlString = urlString
        
        self.completionHandle = completionHandle
        
        let url = URL(string: urlString)
        
        let request =  Foundation.URLRequest(url: url!, cachePolicy: .returnCacheDataElseLoad, timeoutInterval: 5)
        
        myConnection =  NSURLConnection(request: request, delegate: self)
    }
    
    func connection(_ connection: NSURLConnection, didReceive response: URLResponse)
    {
        receiveData.length = 0
    }
    
    
    func connection(_ connection: NSURLConnection, didReceive data: Data)
    {
        receiveData.append(data)
    }
    
    
    
    func connectionDidFinishLoading(_ connection: NSURLConnection)
    {
        if self.completionHandle != nil
        {
            hud!.hide(true)
            self.completionHandle!(self.receiveData as Data)
        }
    }
    
    internal func connection(_ connection: NSURLConnection, didFailWithError error: Error)
    {
        if self.completionHandle != nil
        {
            hud!.hide(true)
            self.completionHandle!(nil)
        }
    }
    
    func connection(_ connection: NSURLConnection, didCancel challenge: URLAuthenticationChallenge)
    {
        if self.completionHandle != nil
        {
            hud!.hide(true)
            self.completionHandle!(nil)
        }
    }
    
    deinit
    {
        if self.completionHandle != nil
        {
            self.completionHandle!(nil)
        }
        
        myConnection.cancel()
        
        myConnection = nil
    }
    
}

