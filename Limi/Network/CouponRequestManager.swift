//
//  CouponRequestManager.swift
//  Limi
//
//  Created by Richie on 16/3/10.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//有关红包的请求

import UIKit

class CouponRequestManager: LMNetWork {
    
    struct staticStruct
    {
        static var net:CouponRequestManager!
        static var once:Int = 0
    }
    
    private static var __once: () = { () -> Void in
            if CouponRequestManager.staticStruct.net == nil{CouponRequestManager.staticStruct.net = CouponRequestManager()}
        }()
    
    //红包列表
    let COUPON_LIST = Constant.JTAPI.user_coupon_list
    
    //红包详情
    let COUPON_DETAIL = Constant.JTAPI.one_user_coupon
    
    //可用红包列表
    let COUPON_USEABLE = Constant.JTAPI.order_coupon_list

    //可用红包列表
    let CHARGE_COUPON_USEABLE = Constant.JTAPI.recharge_coupon_list
    
    fileprivate var _delegate:CouponRequestManagerDelegate?{return self.delegate as? CouponRequestManagerDelegate}
    
    class func shareInstance(_ delegate:CouponRequestManagerDelegate? = nil) ->CouponRequestManager
    {
        
        _ = CouponRequestManager.__once
        
        staticStruct.net.delegate = delegate
        
        return staticStruct.net
    }
    
    
    //红包列表
    func couponLoadList(_ context:UIView,type:Int,isShowErrorStatuMsg:Bool = true) ->CouponRequestManager
    {
        self.requestData(context:context,URLString: COUPON_LIST, parameters: ["status":type as AnyObject,"size":100 as AnyObject], isNeedUserToken: true,  isShowErrorStatuMsg: isShowErrorStatuMsg).taskSign("\(type)")
        return self
    }
    

    //红包列表
    func couponLoadMore(_ context:UIView,type:Int,lastId:Int,isShowErrorStatuMsg:Bool = true) ->CouponRequestManager
    {
        self.requestData(context:context,URLString: COUPON_LIST, parameters: ["status":type as AnyObject,"size":100 as AnyObject,"last_id":lastId as AnyObject], isNeedUserToken: true,  isShowErrorStatuMsg: isShowErrorStatuMsg).taskSign("\(type)")
        return self
    }
    
    
    //红包详情
    func couponDetail(_ context:UIView,couponId:Int,isShowErrorStatuMsg: Bool = true, isNeedHud: Bool = true) ->CouponRequestManager
    {
        self.requestData(context:context,URLString: COUPON_DETAIL, parameters: ["coupon_id":couponId as AnyObject], isNeedUserToken: true,  isShowErrorStatuMsg: isShowErrorStatuMsg,isNeedHud:isNeedHud).taskSign(COUPON_DETAIL)
        return self
    }
    
    
    /**
     充值可用红包
     
     token	true	string	登录token值
     mobile	true	string	手机号
     id	true	int	产品ID
     */
    func chargeCouponAvailableOfOrder(_ context:UIView,mobile:String,chargeId:Int,isShowErrorStatuMsg: Bool = true, isNeedHud: Bool = true) ->CouponRequestManager
    {
        
        self.requestData(context:context,URLString: CHARGE_COUPON_USEABLE, parameters:["mobile":mobile as AnyObject,"id":chargeId as AnyObject], isNeedUserToken: true,  isShowErrorStatuMsg: isShowErrorStatuMsg,isNeedHud:isNeedHud).taskSign(CHARGE_COUPON_USEABLE)
        return self
    }
    /**
     根据订单得出可用红包
     
     - token	true	string	登录token值
     - status	true	int	1未使用的优惠券 2快过期的优惠券
     - price	true	int	订单的实际价格
     - goods_id	true	[]int	商品ID集合，数组格式
     */
    
    func couponAvailableOfOrder(_ context:UIView,goods_id:[Int],goods_sub_id:[Int],goods_num:[Int],status:Int = 1,isShowErrorStatuMsg: Bool = true, isNeedHud: Bool = true) ->CouponRequestManager
    {
        /*
         token	true	string	登录token值
         status	true	int	1未使用的优惠券 2快过期的优惠券
         goods_id	true	[]int	商品ID集合，数组格式
         goods_sub_id	true	[]int	子商品ID集合，数组格式，没有传0
         goods_num	true	[]int	商品数量集合，数组格式
         */
        
        var para = [String:AnyObject]()
        
        para["goods_id"] = goods_id as AnyObject?
        para["goods_num"] = goods_num as AnyObject?
        para["status"] = status as AnyObject?

        if goods_sub_id.count == 0{
            var sunids = [Int]()
            for _ in 0..<goods_id.count{
                sunids.append(0)
            }
            para["goods_sub_id"] = sunids as AnyObject?
            
        }else{
            para["goods_sub_id"] = goods_sub_id as AnyObject?
        }
        
        print(para)
        
        self.requestData(context:context,URLString: COUPON_USEABLE, parameters:para, isNeedUserToken: true,  isShowErrorStatuMsg: isShowErrorStatuMsg,isNeedHud:isNeedHud).taskSign(COUPON_USEABLE)
        
        return self
    }
    
    override func requestSuccess(_ manager: LMRequestManager, result: LMResultMode) {
        if manager.taskSign == nil{return}
        let sign = manager.taskSign!
        
        if sign == COUPON_DETAIL
        {
           let _ = Delegate_Selector(_delegate, #selector(CouponRequestManagerDelegate.couponRequestManagerDidLoadDetailFinish(_:result:))){[unowned self] in self._delegate!.couponRequestManagerDidLoadDetailFinish!(manager, result: result)}
            
        }else if sign == COUPON_USEABLE || sign == CHARGE_COUPON_USEABLE
        {
            print(result.rootData)
            print(result.data)
            
            var couponList = [[String:AnyObject]]()
            
            if let data = result.data,
                let list = data["list"] as? [[String:AnyObject]]
            {
                couponList = list
            }
            
           let _ = Delegate_Selector(_delegate, #selector(CouponRequestManagerDelegate.couponRequestManagerDidLoadUseableListFinish(_:result:couponList:))){[unowned self] in self._delegate!.couponRequestManagerDidLoadUseableListFinish!(manager, result: result, couponList: couponList)}
        }
        else
        {
            var couponList = [[String:AnyObject]]()
            
            if let data = result.data,
                let list = data["list"] as? [[String:AnyObject]]
            {
                couponList = list
            }
            
           let _ = Delegate_Selector(_delegate, #selector(CouponRequestManagerDelegate.couponRequestManagerDidLoadListFinish(_:result:couponList:))){[unowned self] in self._delegate!.couponRequestManagerDidLoadListFinish!(manager, result: result, couponList: couponList)}
        }
    }
    
}


@objc protocol CouponRequestManagerDelegate:LMNetWorkDelegate
{
    @objc optional func couponRequestManagerDidLoadListFinish(_ requestManager:LMRequestManager,result:LMResultMode,couponList:[[String:AnyObject]])
    @objc optional func couponRequestManagerDidLoadUseableListFinish(_ requestManager:LMRequestManager,result:LMResultMode,couponList:[[String:AnyObject]])
    @objc optional func couponRequestManagerDidLoadDetailFinish(_ requestManager:LMRequestManager,result:LMResultMode)
}

