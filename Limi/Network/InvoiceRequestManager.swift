//
//  InvoiceRequestManager.swift
//  Limi
//
//  Created by Richie on 16/3/17.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//有关发票的请求

import UIKit

class InvoiceRequestManager: LMNetWork {
    
    //发票列表
    let INVOICE_LIST = Constant.JTAPI.invoice_list
    //新增发票
    let INVOICE_ADD = Constant.JTAPI.invoice_add
    //删除发票
    let INVOICE_DEL = Constant.JTAPI.invoice_del
    //开发票
    let INVOICE_MAKE = Constant.JTAPI.invoice_commit
    
    fileprivate var _delegate:InvoiceRequestManagerDelegate?{return (self.delegate as? InvoiceRequestManagerDelegate)}
    
    fileprivate var _isNeedHud = false
    
    //发票列表 progress
    func invoiceList(_ context:UIView,showError: Bool = true)
    {
        self.requestData(context:context,URLString: INVOICE_LIST, isNeedUserToken: true, isShowErrorStatuMsg: showError).taskSign(INVOICE_LIST)
    }
    
    
    //新增发票
    func invoiceAddnew(_ context:UIView,title:String,showFail:Bool = true,indicator:String? = nil)
    {
        _isNeedHud = indicator != nil
        
        self.requestData(context:context,URLString: INVOICE_ADD, parameters:["invoice_head":title as AnyObject],isNeedUserToken: true, isShowErrorStatuMsg: showFail,isNeedHud : _isNeedHud,hudTitle:indicator).taskSign(INVOICE_ADD)
    }
    
    
    //删除
    func invoiceDelete(_ context:UIView,id:String,showFail:Bool = true,indicator:String? = nil)
    {
        _isNeedHud = (indicator != nil)
        
        self.requestData(context:context,URLString: INVOICE_DEL, parameters:["id":id as AnyObject],isNeedUserToken: true, isShowErrorStatuMsg: showFail,isNeedHud : _isNeedHud,hudTitle:indicator).taskSign(INVOICE_DEL)
    }
    
    
    //开发票 invoice_text	true	int	发票内容：1明细 2礼品 3办公用品
    func invoiceMake(_ context:UIView,oid:Int,invoiceModel:InvoiceModel,addressId:Int,showFail:Bool = true,isNeedHud : Bool = true)
    {
        var parameters = [String:AnyObject]()
        /*
        
        token	true	string	登录的token值，必须传
        oid	true	int	订单ID
        address_id	true	int	地址ID
        invoice_head	true	string	发票抬头
        invoice_type	true	int	发票内容：1明细 2礼品 3办公用品
        */
        
        parameters["oid"] = oid as AnyObject?
        parameters["address_id"] = addressId as AnyObject?
        parameters["invoice_head"] = invoiceModel.invoice as AnyObject?
        parameters["invoice_type"] = invoiceModel.invoice_type as AnyObject?
        
        self.requestData(context:context,URLString: INVOICE_MAKE, parameters:parameters,isNeedUserToken: true, isShowErrorStatuMsg: showFail,isShowSuccessStatuMsg: true,isNeedHud : _isNeedHud).taskSign(INVOICE_MAKE)
    }
    
    
    override func requestSuccess(_ manager: LMRequestManager, result: LMResultMode) {
        if manager.taskSign == nil{return}
        let sign = manager.taskSign!
        
        if sign == INVOICE_LIST
        {
            var dataList = [InvoiceModel]()
            
            if let data = result.data,
                let list = data["list"] as? [[String:AnyObject]]{
                    let _ = list.map{dataList.append(InvoiceModel($0))}
            }
            
            let _ = Delegate_Selector(_delegate, #selector(InvoiceRequestManagerDelegate.invoiceRequestManagerDidLoadListFinish(_:result:list:))){Void in self._delegate!.invoiceRequestManagerDidLoadListFinish!(manager, result: result, list: dataList)}
        }
        else if sign == INVOICE_ADD
        {
            let _ = Delegate_Selector(_delegate, #selector(InvoiceRequestManagerDelegate.invoiceRequestManagerDidAddnewFinish(_:result:))){Void in self._delegate!.invoiceRequestManagerDidAddnewFinish!(manager, result: result)}
        }
        else if sign == INVOICE_DEL
        {
            let _ = Delegate_Selector(_delegate, #selector(InvoiceRequestManagerDelegate.invoiceRequestManagerDidDeleteFinish(_:result:))){Void in self._delegate!.invoiceRequestManagerDidDeleteFinish!(manager, result: result)}
        }
        else if sign == INVOICE_MAKE
        {
            let _ = Delegate_Selector(_delegate, #selector(InvoiceRequestManagerDelegate.invoiceRequestManagerDidMakeFinish(_:result:))){Void in self._delegate!.invoiceRequestManagerDidMakeFinish!(manager, result: result)}
        }
    }
    
    
    
}


@objc protocol InvoiceRequestManagerDelegate:LMNetWorkDelegate{
    
    @objc optional func invoiceRequestManagerDidLoadListFinish(_ manager:LMRequestManager,result:LMResultMode,list:[InvoiceModel])
    
    @objc optional func invoiceRequestManagerDidAddnewFinish(_ manager:LMRequestManager,result:LMResultMode)
    
    @objc optional func invoiceRequestManagerDidDeleteFinish(_ manager:LMRequestManager,result:LMResultMode)
    
    @objc optional func invoiceRequestManagerDidMakeFinish(_ manager:LMRequestManager,result:LMResultMode)
}
