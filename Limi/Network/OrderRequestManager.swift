//
//  OrderRequestManager.swift
//  Limi
//
//  Created by Richie on 16/3/14.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//有关订单的各类请求

import UIKit

class OrderRequestManager: LMNetWork {
    struct staticStruct
    {
        static var net:OrderRequestManager!
        static var once:Int = 0
    }
    
    private static var __once: () = { () -> Void in
            if OrderRequestManager.staticStruct.net == nil{OrderRequestManager.staticStruct.net = OrderRequestManager()}
        }()
    
    //提交订单，从商品中
    let  COMMIT_GOODS = Constant.JTAPI.order_commit
    
    //提交订单，从礼物篮
    let  COMMIT_BASKT = Constant.JTAPI.order_commitV2
    
    //提交订单，从礼物篮
    let  EXPRESS_TIPE = Constant.JTAPI.order_ExpressTipe
    
    //订单列表
    let  LIST = Constant.JTAPI.order_listV1
    
    //更多订单列表标记
    let  MORE = "more_order_list"
    
    //订单数量
    let  COUNT = Constant.JTAPI.order_num
    
    //提交售后信息
    let  SOLD = Constant.JTAPI.order_sold
    
    //支付接口
    let  PAY = Constant.JTAPI.order_prepay
    
    //删除订单
    let  DELETE = Constant.JTAPI.order_cancel
    
    //订单详情
    let  DETAIL = Constant.JTAPI.order_item
    
    //订单详情
    let  VOICE_ADD = Constant.JTAPI.voice_add
    
    
    //心意服务数据
    let  GIFTSERVICE = Constant.JTAPI.giftservice
    
    var page = 0
    
    class func shareInstance(_ delegate:OrderRequestManagerDelegate? = nil) ->OrderRequestManager
    {
        
        _ = OrderRequestManager.__once
        
        staticStruct.net.delegate = delegate
        
        return staticStruct.net
    }
    
    fileprivate var _isNeed = false
    
    fileprivate var _delegate:OrderRequestManagerDelegate?{return (self.delegate as? OrderRequestManagerDelegate)}
    
    
    //提交订单，从订单详情
    func orderCommitFromGoodsDetail(_ context:UIView,dataManager:OrderDataManager,isShowErrorStatuMsg:Bool,isNeedHud:Bool)
    {
        /*
         token	true	string	登录token值
         address_id	true	int	收货地址ID
         goods_id	true	int	商品ID
         goods_sub_id	false	int	用户选择了属性组合，必须传属性组合的ID
         goods_num	true	int	商品数量
         order_remark	false	string	订单备注
         coupon_id	false	int	优惠券ID
         source	false	int	订单来源：1 iOS app,2 android app,3 微信,4 pc web,5 mobile web
         */
        
        var para = [String:AnyObject]()
        
        para["goods_num"] = dataManager.goodsList[0].selectedGoodsSubModelCount as AnyObject?
        
        para["goods_id"] = dataManager.goodsList[0].goodsId as AnyObject?
        
        if let subModel = dataManager.goodsList![0].selelctedGoodsSubModel,
            let sub  = subModel{
            para["goods_sub_id"] = sub.id as AnyObject?
        }
        
        para["address_id"] = dataManager.address?.id as AnyObject?
        
        para["order_remark"] = dataManager.remark as AnyObject?? ?? "" as AnyObject?
        
        if let coupon = dataManager.couponModel{
            para["coupon_id"] = coupon.id as AnyObject?
        }
        
        if let ids = dataManager.serviceIds{
            para["service_ids"] = "\(ids[0]),\(ids[1]),\(ids[2])" as AnyObject?
        }
        
        
        para["source"] = 1 as AnyObject?
        
        print(para)
        
        self.requestData(context:context,URLString: COMMIT_GOODS, parameters: para, isNeedUserToken: true, isShowErrorStatuMsg: isShowErrorStatuMsg, isNeedHud: isNeedHud).taskSign(COMMIT_GOODS)
    }
    
    
    func orderGetExpressTipe(_ context:UIView,dataManager:OrderDataManager){
        /*
         token	true	string	登录token值
         goods_id	true	[]int	商品ID，数组格式
         service_ids	true	string	心意服务，例如：0,0,0
         
         */
        
        var para = [String:AnyObject]()
        var goods_idAry = [Int]()
        
        for goods in dataManager.goodsList
        {
            goods_idAry.append(goods.goodsId!)
        }
        
        if let ids = dataManager.serviceIds{
            para["service_ids"] = "\(ids[0]),\(ids[1]),\(ids[2])" as AnyObject?
        }
        
        para["goods_id"] = goods_idAry as AnyObject?
        
        self.requestData(context:context,URLString: EXPRESS_TIPE, parameters: para, isNeedUserToken: true).taskSign(EXPRESS_TIPE)
        
    }
    
    
    //提交订单从礼物蓝
    func orderCommitFromBasket(_ context:UIView,dataManager:OrderDataManager,sourceType:Int,isShowErrorStatuMsg:Bool,isNeedHud:Bool)
    {
        /*
         token	true	string	登录token值
         address_id	true	int	收货地址ID
         goods_id	true	[]int	商品ID，数组格式
         goods_sub_id	true	[]int	用户选择了属性组合，传属性组合的ID,没有传0，数组格式
         goods_num	true	[]int	商品数量，数组格式
         order_remark	false	string	订单备注
         coupon_id	false	int	优惠券ID
         source	false	int	订单来源：1 iOS app,2 android app,3 微信,4 pc web,5 mobile web
         
         
         1.3.8+
         package_id
         discount_price
         event
         */
        var para = [String:AnyObject]()
        var goods_sub_idAry = [Int]()
        var goods_numAry = [Int]()
        var goods_idAry = [Int]()
        
        for goods in dataManager.goodsList
        {
            
            if sourceType == 3{
                if let id = goods.goodsSubId{
                goods_sub_idAry.append(id)
                }
                else{
                goods_sub_idAry.append(0)
                }
            }
            else{
            if  let model = goods.selelctedGoodsSubModel,
                let m = model{
                goods_sub_idAry.append(m.id)
            }
            else{
                goods_sub_idAry.append(0)
            }
            }
            
            goods_numAry.append(goods.selectedGoodsSubModelCount)
            goods_idAry.append(goods.goodsId!)
        }
        
        
        if let ids = dataManager.serviceIds{
            para["service_ids"] = "\(ids[0]),\(ids[1]),\(ids[2])" as AnyObject?
        }
        
        para["source"]      = 1 as AnyObject?
        para["goods_id"]    = goods_idAry as AnyObject?
        para["goods_num"]   = goods_numAry as AnyObject?
        para["goods_sub_id"] = goods_sub_idAry as AnyObject?
        
        
        para["address_id"]      = dataManager.address?.id as AnyObject?? ?? "" as AnyObject?
        para["order_remark"]    = dataManager.remark as AnyObject?? ?? "" as AnyObject?
        
        if let coupon = dataManager.couponModel
        {
            para["coupon_id"] = coupon.id as AnyObject?
        }
        
        
        //1.3.8+
        if let event = dataManager.callback_event{
            para["event"] = event as AnyObject?
        }
        if let packageID = dataManager.packageID{
            para["package_id"] = packageID as AnyObject?
        }
        if let discount_price = dataManager.disCountOfPackage{
            para["discount_price"] = discount_price as AnyObject?
        }
        
        self.requestData(context:context,URLString: COMMIT_BASKT, parameters: para, isNeedUserToken: true, isShowErrorStatuMsg: isShowErrorStatuMsg, isNeedHud: isNeedHud).taskSign(COMMIT_BASKT)
    }
    
    /**
     获取订单列表,如果想要请求更多数据请使用[ moreOrderList]
     
     - parameter size:      int	不传默认为20
     - parameter status:    int 订单状态：0全部订单 1未付款订单 2待收货订单 3已完成订单
     - parameter showError: 是否显示异常信息
     */
    func orderList(_ context:UIView,size:Int = 10,status:Int,showError:Bool = true)
    {
        self.page = 0
        let para = ["page":0,"size":size,"status":status]
        
        self.requestData(context:context,URLString: LIST, parameters: para as [String : AnyObject]?, isNeedUserToken: true, isShowErrorStatuMsg: showError).taskSign(LIST)
    }
    
    //加载更多订单列表
    func moreOrderList(_ context:UIView,lastId:Int,size:Int = 10,status:Int,showError:Bool = true)
    {
        self.page += 1
        let para = ["page":self.page,"size":size,"status":status]

        self.requestData(context:context,URLString: LIST, parameters: para as [String : AnyObject]?, isNeedUserToken: true, isShowErrorStatuMsg: showError).taskSign(MORE)
    }
    
    //删除订单
    func orderdelete(_ context:UIView,oid:Int)
    {
        self.requestData(context:context,URLString: DELETE, parameters: ["oid":oid as AnyObject], isNeedUserToken: true, isShowErrorStatuMsg: true, isNeedHud: true, hudTitle: "正在取消").taskSign(DELETE)
        
    }
    
    
    //添加语音
    func orderUpLoadVoice(_ context:UIView,audio:String,second:Int){
        
        /*
         audio	true	string	音频文件名
         second	true	int	录音时长
         */
        
        var parameters = [String:AnyObject]()
        
        parameters["audio"] = audio as AnyObject?
        parameters["second"] = second as AnyObject?
        
        self.requestData(context:context,URLString: VOICE_ADD, parameters: parameters, isNeedUserToken: true,isShowErrorStatuMsg: true, isNeedHud: true, hudTitle: "正在上传" ).taskSign(VOICE_ADD)
    }
    
    
    //订单数量
    func orderListCount(){}
    
    
    //订单提交售后
    func orderCommitSlod(_ context:UIView,params:[String:AnyObject]){
        /*
         token	true	string	登录token值
         oid	true	int	订单ID
         goods_oid	true	int	子订单ID
         type	true	int	售后类型: 1换货 2退货 3维修 4退款
         reason	true	string	售后原因
         detail	false	string	问题描述
         sold_img	false	[]string	售后图片，有的话必须传，格式为数组
         
         */
        self.requestData(context:context,URLString: SOLD, parameters: params, isNeedUserToken: true, isShowErrorStatuMsg: true, isNeedHud: true, hudTitle: "正在提交").taskSign(SOLD)
    }
    
    //支付
    func orderPrepay(){}
    
    //订单详情
    func orderDetail(_ context:UIView,oid:Int,showError:Bool = true,hudContent:String? = nil)
    {
        let para = ["oid":oid]
        
        _isNeed = hudContent != nil
        
        self.requestData(context:context,URLString: DETAIL, parameters: para as [String : AnyObject]?, isNeedUserToken: true, isShowErrorStatuMsg: showError, isNeedHud: _isNeed, hudTitle: hudContent).taskSign(DETAIL)
    }
    
    
    /**
     ids	true	string	选择的心意服务的id字符串，如：“0,0,0”
     package_id	false	int	如果是自选礼盒订单页面，则必须传该礼盒id
     */
    ///心意服务
    func orderRequestGiftServiceList(_ context:UIView,ids:String,package_id:Int?,showError:Bool = true,hudContent:String? = nil){
        var parameters = [String:AnyObject]()
        
        parameters["ids"] = ids as AnyObject?
        
        if let id = package_id{
            parameters["package_id"] = id as AnyObject?
        }
        
        self.requestData(context:context,URLString: GIFTSERVICE, parameters: parameters, isNeedUserToken: true,isShowErrorStatuMsg: showError, isNeedHud: hudContent != nil, hudTitle: hudContent).taskSign(GIFTSERVICE)
    }
    
    override func requestSuccess(_ manager: LMRequestManager, result: LMResultMode) {
        
        if manager.taskSign == COMMIT_GOODS
        {
            var oid:Int = 0
            
            if let data = result.data,
                let id = data["id"] as? Int{
                oid = id
            }
            
            let _ = Delegate_Selector(_delegate, #selector(OrderRequestManagerDelegate.orderRequestManagerDidCommitOfFormGoodsDetail(_:result:oid:))){Void in self._delegate?.orderRequestManagerDidCommitOfFormGoodsDetail!(manager, result: result,oid:oid)}
        }else if manager.taskSign == COMMIT_BASKT
        {
            var oid:Int = 0
            
            if let data = result.data,
                let id = data["id"] as? Int{
                oid = id
            }
            
            let _ = Delegate_Selector(_delegate, #selector(OrderRequestManagerDelegate.orderRequestManagerDidCommitOfFormGoodsBasket(_:result:oid:))){Void in self._delegate?.orderRequestManagerDidCommitOfFormGoodsBasket!(manager, result: result,oid:oid)}
        }else if manager.taskSign == LIST
        {
            var models = [OrderModel]()
            
            if let data = result.data,
                let list = data["list"] as? [[String:AnyObject]]{
                
                let _ = list.map{models.append(OrderModel($0))}
            }
            
            let _ = Delegate_Selector(_delegate, #selector(OrderRequestManagerDelegate.orderRequestManagerDidLoadListFinish(_:result:list:))){Void in self._delegate?.orderRequestManagerDidLoadListFinish!(manager, result: result,list: models)}
            
        }else if manager.taskSign == MORE
        {
            var models = [OrderModel]()
            
            if let data = result.data,
                let list = data["list"] as? [[String:AnyObject]]{
                
                let _ = list.map{models.append(OrderModel($0))}
            }
            
            let _ = Delegate_Selector(_delegate, #selector(OrderRequestManagerDelegate.orderRequestManagerDidLoadMoreListFinish(_:result:list:))){Void in self._delegate?.orderRequestManagerDidLoadMoreListFinish!(manager, result: result,list: models)}
        }
        else if manager.taskSign == COUNT
        {
            //            Delegate_Selector(_delegate, "orderRequestManagerDidLoadGoodsCount:result:"){Void in self._delegate?.orderRequestManagerDidLoadGoodsCount(manager, result: result)}
        }
        else if manager.taskSign == SOLD
        {
            let _ = Delegate_Selector(_delegate, #selector(OrderRequestManagerDelegate.orderRequestManagerDidCommitOfSoldFinish(_:result:))){Void in self._delegate?.orderRequestManagerDidCommitOfSoldFinish!(manager, result: result)}
        }
        else if manager.taskSign == PAY
        {
            let _ = Delegate_Selector(_delegate, #selector(OrderRequestManagerDelegate.orderRequestManagerDidCommitPrepayFinish(_:result:))){Void in self._delegate?.orderRequestManagerDidCommitPrepayFinish!(manager, result: result)}
        }
        else if manager.taskSign == DELETE
        {
            
            let _ = Delegate_Selector(_delegate, #selector(OrderRequestManagerDelegate.orderRequestManagerDidDeleteFinish(_:result:))){Void in self._delegate?.orderRequestManagerDidDeleteFinish!(manager, result: result)}
        }
        else if manager.taskSign == DETAIL
        {
            var detail:OrderDetailModel!
            
            if let data = result.data,
                let order = data["order"] as? [String:AnyObject]{
                
                print(data)
                
                detail = OrderDetailModel(order)
            }
            
            let _ = Delegate_Selector(_delegate, #selector(OrderRequestManagerDelegate.orderRequestManagerDidLoadDetailFinish(_:result:detail:))){Void in self._delegate?.orderRequestManagerDidLoadDetailFinish!(manager, result: result, detail: detail)}
            
        }else if manager.taskSign == VOICE_ADD{
            
            let _ = Delegate_Selector(_delegate, #selector(OrderRequestManagerDelegate.orderRequestManagerDidVoiceAdd(_:result:))){Void in self._delegate?.orderRequestManagerDidVoiceAdd!(manager, result: result)}
        }else if manager.taskSign == EXPRESS_TIPE{
            
            let _ = Delegate_Selector(_delegate, #selector(OrderRequestManagerDelegate.orderRequestManagerDidGetExpressTipe(_:result:))){Void in self._delegate?.orderRequestManagerDidGetExpressTipe!(manager, result: result)}
        }else if manager.taskSign == GIFTSERVICE{
           
            /*
             
             audio = "<null>";
             box =         {
             attr = "\U683c\U8bf4\U660e";
             "choose_price" = 22;
             "choose_service_id" = 40;
             "choose_title" = "\U793c\U76d2\U670d\U52a1\U670d\U52a1\U526f\U6807\U9898";
             image = "http://up.xydd.co/14606238626245.jpg";
             };
             card = "<null>";
             */
            var giftServiceAry = [GiftServiceModel]()
            
            if let data = result.data{
                let keyAry = ["box","card","audio"]
                for index in 0..<keyAry.count{
                    let key = keyAry[index]
                    if let dic = data[key] as? [String:AnyObject]{
                        let model = GiftServiceModel(data:dic,type:index + 1)
                        giftServiceAry.append(model)
                    }
                }
            }
            
            let _ = Delegate_Selector(_delegate, #selector(OrderRequestManagerDelegate.orderRequestManagerDidLoadGiftServiceList(_:result:giftServiceList:))){Void in
                self._delegate?.orderRequestManagerDidLoadGiftServiceList!(manager, result: result,giftServiceList:giftServiceAry)
            }
        }
    }
}



@objc protocol OrderRequestManagerDelegate:LMNetWorkDelegate{
    
    //从商品详情提交订单
    @objc optional func orderRequestManagerDidCommitOfFormGoodsDetail(_ manager:LMRequestManager,result:LMResultMode,oid:Int)
    
    //从礼物蓝提交订单
    @objc optional func orderRequestManagerDidCommitOfFormGoodsBasket(_ manager:LMRequestManager,result:LMResultMode,oid:Int)
    
    //订单列表
    @objc optional func orderRequestManagerDidLoadListFinish(_ manager:LMRequestManager,result:LMResultMode,list:[OrderModel])
    
    //更多订单
    @objc optional func orderRequestManagerDidLoadMoreListFinish(_ manager:LMRequestManager,result:LMResultMode,list:[OrderModel])
    
    //订单详情
    @objc optional func orderRequestManagerDidLoadDetailFinish(_ manager:LMRequestManager,result:LMResultMode,detail:OrderDetailModel?)
    
    //订单数量
    @objc optional func orderRequestManagerDidLoadGoodsCount(_ manager:LMRequestManager,result:LMResultMode,count:Int)
    
    //获取售后
    @objc optional func orderRequestManagerDidCommitOfSoldFinish(_ manager:LMRequestManager,result:LMResultMode)
    
    //生成订单
    @objc optional func orderRequestManagerDidCommitPrepayFinish(_ manager:LMRequestManager,result:LMResultMode)
    
    //取消删除订单
    @objc optional func orderRequestManagerDidDeleteFinish(_ manager:LMRequestManager,result:LMResultMode)
    
    //语音祝福
    @objc optional func orderRequestManagerDidVoiceAdd(_ manager:LMRequestManager,result:LMResultMode)
    
    //心意服务列表
    @objc optional func orderRequestManagerDidLoadGiftServiceList(_ manager:LMRequestManager,result:LMResultMode,giftServiceList:[GiftServiceModel])
    
    //物流提醒
    @objc optional func orderRequestManagerDidGetExpressTipe(_ manager:LMRequestManager,result:LMResultMode)
}
