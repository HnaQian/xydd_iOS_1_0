//
//  GoodsRequestManager.swift
//  Limi
//
//  Created by Richie on 16/3/22.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class GoodsRequestManager: LMNetWork {
    
    
    let RECOMMEND_LIST = Constant.JTAPI.goodsOfrecommend_list
    
    let DETAIL = Constant.JTAPI.goods
    
    let RECOMMEND_MORE = "more"
    
    let GOODS_RELATION = Constant.JTAPI.goods_relation
    
    let GOODS_OTHER = Constant.JTAPI.goods_other
    
    let GOODS_SPCE = Constant.JTAPI.goods_spec
    
    
    //请求商品详情
    func goodsLoadDetail(_ hudView:UIView,goodsId:Int){
        
        let params = ["goods_id": goodsId]
        
        self.requestData(context: hudView, URLString: DETAIL, parameters: params as [String : AnyObject]?, isNeedUserToken: true,  isShowErrorStatuMsg: true, isNeedHud: true).taskSign(DETAIL)
    }
    
    //请求商品参数
    func goodsLoadSpceInfo(_ hudView:UIView,goodsId:Int){
        
        let params = ["goods_id": goodsId]
        
        self.requestData(context: hudView, URLString: GOODS_SPCE, parameters: params as [String : AnyObject]?, isNeedUserToken: true,  isShowErrorStatuMsg: true, isNeedHud: true).taskSign(GOODS_SPCE)
    }
    
    fileprivate var _delegate:GoodsRequestManagerDelegate?{return (self.delegate as? GoodsRequestManagerDelegate)}
    
    //请求相关的商品
    func goodsLoadRelationList(_ hudView:UIView, goodsId:Int){
        let params = ["goods_id": goodsId]
        
        self.requestData(context: hudView, URLString: GOODS_RELATION, parameters: params as [String : AnyObject]?, isShowErrorStatuMsg: true, isNeedHud: true).taskSign(GOODS_RELATION)
        
        
    }
    
    //请求推荐商品
    func goodsLoadRecommendList(_ context:UIView,code:String,size:Int = 20,sort:Int = 0,showError:Bool = true){
        
        var parameters = [String:AnyObject]()
        
        parameters["code"] = code as AnyObject?
        parameters["size"] = size as AnyObject?
        parameters["sort"] = sort as AnyObject?
        
        self.requestData(context:context,URLString: RECOMMEND_LIST, parameters: parameters, isNeedUserToken: true, isShowErrorStatuMsg: true).taskSign(RECOMMEND_LIST)
    }
    //其他活动商品列表
    func goodsLoadOtherList(_ hudView:UIView, goodsId:Int, eventId:Int){
//        let params = ["event_id":eventId, "goods_id":goodsId]
        
        var params = [String:AnyObject]()
        
        params["event_id"] = eventId as AnyObject?
        params["goods_id"] = goodsId as AnyObject?
        
        self.requestData(context: hudView, URLString: GOODS_OTHER, parameters: params,isNeedUserToken: true, isShowErrorStatuMsg: true, isNeedHud: true).taskSign(GOODS_OTHER)
    }
    
    
    override func requestSuccess(_ manager: LMRequestManager, result: LMResultMode) {
        print(result)
        if manager.taskSign == nil{return}
        let taskSign = manager.taskSign!
        
        if taskSign == RECOMMEND_LIST{
            
            /*
             
             [
             "Goods_subtitle": 一柱香一杯茶,
             "Goods_image": http: //up.xinyidiandian.com/14370429791322.jpg,
             "Market_price": 1520,
             "Collect_status": 0,
             "Gid": 731,
             "Price": 1267.2,
             "Collect_num": 3990,
             "Has_feature": 0,
             "Goods_feature": <null>,
             "Goods_name": 香台+茶杯,
             "Goods_outer": 1
             ]
             
             */
            
            var goods = [GoodsModel]()
            
            if let data = result.data,
                let list = data["list"] as? [[String:AnyObject]]
            {
                let _ = list.map{goods.append(GoodsModel($0))}
            }
            
            let _ = Delegate_Selector(_delegate, #selector(GoodsRequestManagerDelegate.goodsRequestDidLoadGoodsOfRecommendList(_:result:goods:))){Void in self._delegate!.goodsRequestDidLoadGoodsOfRecommendList!(manager, result: result, goods: goods)}
            
        }else if taskSign == RECOMMEND_MORE{
            
            var goods = [GoodsModel]()
            
            if let data = result.data,
                let list = data["list"] as? [[String:AnyObject]]
            {
                let _ = list.map{goods.append(GoodsModel($0))}
            }
            
            let _ = Delegate_Selector(_delegate, #selector(GoodsRequestManagerDelegate.goodsRequestDidLoadMoreGoodsOfRecommendList(_:result:goods:))){Void in self._delegate!.goodsRequestDidLoadMoreGoodsOfRecommendList!(manager, result: result, goods: goods)}
            
        }else if taskSign == DETAIL{
            
            var goodsModel:GoodsModel?
            if let mdata = result.data,
                let goods = mdata["goods"] as? [String:AnyObject]{
                
                goodsModel = GoodsModel(goods)
            }
            
            let _ = Delegate_Selector(_delegate, #selector(GoodsRequestManagerDelegate.goodsRequestDidLoadGoodsDetail(_:result:detailModel:))){Void in self._delegate!.goodsRequestDidLoadGoodsDetail!(manager, result: result, detailModel: goodsModel)}
            
        }else if taskSign == GOODS_RELATION{
            
            var goodsModelArr = [GoodsRelationModel]()
            
            print(result.rootData)
            if let list = result.rootData["data"] as? [AnyObject]{
                
                
                if list.count > 0 {
                    
                    //                    let _ = list.map{goodsModelArr.append(GoodsModel($0 as! [String:AnyObject]))}
                    
                    for js in list{
                        goodsModelArr.append(GoodsRelationModel(js as! [String:AnyObject]))
                    }
                    
                }
            }
            
            
            let _ = Delegate_Selector(_delegate, #selector(GoodsRequestManagerDelegate.goodsRequestDidLoadRelationGoodsList(_:result:goodsList:))){Void in self._delegate!.goodsRequestDidLoadRelationGoodsList!(manager, result: result, goodsList: goodsModelArr)}
        }else if taskSign == GOODS_OTHER{
            
            var goodsModelArr = [GoodsModel]()
            
            if let list = result.rootData["data"] as? [AnyObject]{
                if list.count > 0 {
                    for js in list{
                        goodsModelArr.append(GoodsModel(js as! [String:AnyObject]))
                    }
                    
                }
            }
            
            let _ = Delegate_Selector(_delegate, #selector(GoodsRequestManagerDelegate.goodsRequestDidLoadOtherEventGoodsList(_:result:goodsList:))){Void in self._delegate!.goodsRequestDidLoadOtherEventGoodsList!(manager, result: result, goodsList: goodsModelArr)}
        }else if taskSign == GOODS_SPCE{
            
            var goodsSpceInfoModel = [GoodsSpceInfoModel]()
            
            if let list = result.rootData["data"] as? [AnyObject] {
                if list.count > 0 {
                    for js in list {
                        goodsSpceInfoModel.append(GoodsSpceInfoModel(js as! [String:AnyObject]))
                    }
                }
            }
            
            let _ = Delegate_Selector(_delegate, #selector(GoodsRequestManagerDelegate.goodsRequestDidLoadGoodsSpceInfo(_:result:detailModel:))){Void in self._delegate!.goodsRequestDidLoadGoodsSpceInfo!(manager, result: result, detailModel: goodsSpceInfoModel)}
        }
    }
}



@objc protocol GoodsRequestManagerDelegate:LMNetWorkDelegate{
    
    @objc optional func goodsRequestDidLoadGoodsDetail(_ manager:LMRequestManager,result:LMResultMode,detailModel:GoodsModel?)
    
    @objc optional func goodsRequestDidLoadGoodsOfRecommendList(_ manager:LMRequestManager,result:LMResultMode,goods:[GoodsModel])
    
    @objc optional func goodsRequestDidLoadMoreGoodsOfRecommendList(_ manager:LMRequestManager,result:LMResultMode,goods:[GoodsModel])
    
    //获取相关商品
    @objc optional func goodsRequestDidLoadRelationGoodsList(_ manager:LMRequestManager,result:LMResultMode,goodsList:[GoodsRelationModel])
    
    //其它活动商品
    @objc optional func goodsRequestDidLoadOtherEventGoodsList(_ manager:LMRequestManager,result:LMResultMode,goodsList:[GoodsModel])
    //商品参数
    @objc optional func goodsRequestDidLoadGoodsSpceInfo(_ manager:LMRequestManager,result:LMResultMode,detailModel:[GoodsSpceInfoModel])
}
