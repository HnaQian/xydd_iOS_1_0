//
//  ImageUploader.swift
//  Limi
//
//  Created by 程巍巍 on 5/27/15.
//  Copyright (c) 2015 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import Foundation
import CoreData

struct ImageUploader {
    
   fileprivate static func urlRequestWithComponents(_ urlString:String, parameters:Dictionary<String, String>, imageData:Data) -> (Foundation.URLRequest, Data) {
        
        // create url request to send
        let mutableURLRequest = NSMutableURLRequest(url: URL(string: urlString)!)
        mutableURLRequest.httpMethod = HTTPMethod.post.rawValue
        let boundaryConstant = "myRandomBoundary12345";
        let contentType = "multipart/form-data;boundary="+boundaryConstant
        mutableURLRequest.setValue(contentType, forHTTPHeaderField: "Content-Type")
    
        // create upload data to send
        let uploadData = NSMutableData()
        
        // add image
        uploadData.append("\r\n--\(boundaryConstant)\r\n".data(using: String.Encoding.utf8)!)
        uploadData.append("Content-Disposition: form-data; name=\"image_file\"; filename=\"file.png\"\r\n".data(using: String.Encoding.utf8)!)
        uploadData.append("Content-Type: image/jpg\r\n\r\n".data(using: String.Encoding.utf8)!)
        uploadData.append(imageData)
        
        // add parameters
        for (key, value) in parameters {
            uploadData.append("\r\n--\(boundaryConstant)\r\n".data(using: String.Encoding.utf8)!)
            uploadData.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n\(value)".data(using: String.Encoding.utf8)!)
        }
    
        uploadData.append("\r\n--\(boundaryConstant)--\r\n".data(using: String.Encoding.utf8)!)
        
        // return URLRequestConvertible and NSData
//        do{
//            let encodedURLRequest = try URLEncoding.queryString.encode(mutableURLRequest as! URLRequestConvertible, parameters: nil)
//            return (encodedURLRequest, uploadData as Data)
//        }catch{
//        
//        }
        return (mutableURLRequest as URLRequest, uploadData as Data)
    }
    
    
    static func uploadimage(context:UIView,data: Data, handler: @escaping (_ id: String?,_ domain: String?)->Void)
    {
           if UserInfoManager.didLogin == true{
                let token = UserDefaults.standard.string(forKey: "UserToken")! 
                    let parameters = [
                        "token": "51438df0bc1e8178d17228192290c6a4"
                        ]
                    let imageData = data
                    
                    let hud : ProgressHUD = ProgressHUD(view: context)
                    context.addSubview(hud)
                    hud.show(true)
                    let urlCe:String = "http://api.herogym.cn/v1/upload/img"
                    let urlRequest =
                        urlRequestWithComponents(urlCe, parameters: parameters, imageData: imageData)
                    upload(urlRequest.1, with: urlRequest.0).responseJSON{ response in
                    hud.hide(true)

//                    upload(urlRequest.0, data: urlRequest.1).responseJSON{ response in
//                    hud.hide(true)
                        print(response)
                        switch response.result
                        {
                        case .success(let data):
                            if let json = data as? [String: AnyObject]
                            {
                                
                                if let status = json["status"] as? Int
                                {
                                    
                                    if status == 200
                                    {
                                        
                                        if let name = (json["data"] as? [String: AnyObject])?["name"] as? String
                                        {
                                            
                                            if let domain = (json["data"] as? [String: AnyObject])?["domain"] as? String
                                            {
                                                
                                                handler(name,domain)
                                                
                                                return
                                            }
                                        }
                                    }
                                }
                            }
                            
                            handler(nil,nil)
                            Utils.showError(context: context, errorStr: "图片上传失败")
                        case .failure(let error):
//                            print(error)
                            print(error)
                            Utils.showError(context: context,errorStr: error._domain)
                            handler(nil,nil)
                            
                        }
                        }.resume()
                    
            }else
                {
                    handler(nil,nil)
                }


    }
    
    
    
    
    fileprivate static func fileRequestWithComponents(_ urlString:String, parameters:Dictionary<String, AnyObject>, file:Data) -> (Foundation.URLRequest, Data) {
        
        // create url request to send
        var mutableURLRequest = URLRequest(url: URL(string: urlString)!)
        mutableURLRequest.httpMethod = HTTPMethod.post.rawValue
        let boundaryConstant = "myRandomBoundary12345";
        let contentType = "multipart/form-data;boundary="+boundaryConstant
        mutableURLRequest.setValue(contentType, forHTTPHeaderField: "Content-Type")
        
        // create upload data to send
        let uploadData = NSMutableData()
        
        // add image
        uploadData.append("\r\n--\(boundaryConstant)\r\n".data(using: String.Encoding.utf8)!)
        let docDir: AnyObject = NSSearchPathForDirectoriesInDomains(.documentDirectory,
                                                                    .userDomainMask, true)[0] as AnyObject
        //组合录音文件路径
        let  aacPath = (docDir as! String) + "/record.aac"
        
        uploadData.append("Content-Disposition: form-data; name=\"audio\"; filename=\"\(aacPath)\"\r\n".data(using: String.Encoding.utf8)!)
        uploadData.append("Content-Type: audio/MP3\r\n\r\n".data(using: String.Encoding.utf8)!)
        uploadData.append(file)
        
        // add parameters
        for (key, value) in parameters {
            uploadData.append("\r\n--\(boundaryConstant)\r\n".data(using: String.Encoding.utf8)!)
            uploadData.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n\(value)".data(using: String.Encoding.utf8)!)
        }
        
        uploadData.append("\r\n--\(boundaryConstant)--\r\n".data(using: String.Encoding.utf8)!)
        
        // return URLRequestConvertible and NSData
        
        do{
            let encodedURLRequest = try URLEncoding.queryString.encode(mutableURLRequest as URLRequestConvertible, parameters: nil)
            return (encodedURLRequest, uploadData as Data)
        }catch{
            
        }
        return (mutableURLRequest, uploadData as Data)
    }
    
    
    
    static func uploadFile(context:UIView,url:String,data: Data,handler: @escaping (_ id: String?,_ domain: String?)->Void)
    {
        // init paramters Dictionary
        
        
        if UserInfoManager.didLogin == true{
            let token = UserDefaults.standard.string(forKey: "UserToken")!
            let parameters = [
                "token": token
            ]
            
            let imageData = data
            
            // CREATE AND SEND REQUEST ----------
            let urlRequest = fileRequestWithComponents(url, parameters: parameters as Dictionary<String, AnyObject>, file: imageData)
            upload(urlRequest.1, with: urlRequest.0).responseJSON{ response in
                switch response.result
                {
                case .success(let data):
                    print(data)
                    if let json = data as? [String: AnyObject]
                    {
                        if let status = json["status"] as? Int
                        {
                            if status == 200
                            {
                                if let name = (json["data"] as? [String: AnyObject])?["name"] as? String
                                {
                                    if let domain = (json["data"] as? [String: AnyObject])?["domain"] as? String
                                    {
                                        handler(name,domain)
                                        
                                        return
                                    }
                                }
                            }else
                            {
                            Utils.showError(context: context,errorStr: "上传失败")
                            }
                        }
                    }
                    
                    handler(nil,nil)
                    
                    
                case .failure(let error):
                    Utils.showError(context: context,errorStr: error._domain)
                    handler(nil,nil)
                    
                }
                }.resume()
            
        }
        else
        {
            handler(nil,nil)
        }
        
        
    }
    
}
