//
//  LMNetWork.swift
//  Limi
//
//  Created by Richie on 16/3/9.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//


import UIKit

class LMNetWork: NSObject {
    
    override init() {
        super.init()
    }
    
    init(delegate:LMNetWorkDelegate) {
        super.init()
        self.delegate = delegate
    }
    
    //最后一次的请求
    var recentlyRequest:LMRequestManager?{return _recentlyRequest}
    
    var requestManagerAry:[LMRequestManager]{return _requestManagerAry}
    
    var taskSignAry:[String]{return _taskSignAry}
    
    var didLoadMore = false
    
    weak var delegate:LMNetWorkDelegate?
    //数据列表的key
    var dataKey:String = ""
    
    fileprivate var _recentlyRequest:LMRequestManager?
    
    fileprivate var _requestMap = [String:LMRequestManager]()
    
    fileprivate var _taskSignAry = [String]()
    
    fileprivate var _requestManagerAry = [LMRequestManager]()
    
    let reachability = Reachability()
    //刷新数据
    func requestData(_ method: HTTPMethod = .post,context:UIView,URLString: URLConvertible,parameters: [String: AnyObject]? = nil,isNeedUserToken : Bool? = false,isNeedAppToken : Bool? = false,isShowErrorStatuMsg:Bool? = false ,isShowSuccessStatuMsg:Bool? = false ,isNeedHud : Bool? = false,hudTitle: String? = nil ,encoding: URLEncoding = URLEncoding.default,headers: [String: String]? = nil) -> LMNetWork
    {
        self._recentlyRequest = LMRequestManager.newManager()
        _requestManagerAry.append(self._recentlyRequest!)
        
        self._recentlyRequest?.request(method, context: context, URLString: URLString, parameters: parameters, isNeedUserTokrn: isNeedUserToken, isNeedAppTokrn: isNeedAppToken, isShowErrorStatuMsg: isShowErrorStatuMsg, isShowSuccessStatuMsg: isShowSuccessStatuMsg, isNeedHud: isNeedHud, hudTitle: hudTitle, encoding: encoding, headers: headers).analyseResponse { (manager,response) -> Void in
            self.analyiseData(context,manager: manager,isShowErrorStatuMsg:isShowErrorStatuMsg! ,isShowSuccessStatuMsg:isShowSuccessStatuMsg!,response: response )
        }
        return self
    }
    
    
    //网络任务ID
    func taskID()->Int{
        if let request = recentlyRequest{
            return request.taskID
        }
        return 0
    }
    
    func requestDataWithMode(_ context:UIView,requestMode:LMRequestMode) ->LMNetWork
    {
        return  self.requestData(requestMode.method,context: context, URLString: requestMode.URLString, parameters: requestMode.parameters, isNeedUserToken: requestMode.isNeedUserToken, isNeedAppToken: requestMode.isNeedAppToken, isShowErrorStatuMsg: requestMode.isShowErrorStatuMsg, isNeedHud: requestMode.isNeedHud, hudTitle: requestMode.hudTitle, encoding: requestMode.encoding, headers: requestMode.headers)
    }
    
    
    //在添加新的网络任务时，添加标记
    func taskSign(_ sign:String)
    {
        if let curRequest = self._recentlyRequest
        {
            _taskSignAry.append(sign)
            
            curRequest.taskSign = sign
            
            self._requestMap[sign] = curRequest
        }
    }
    
    //如果添加过标记可以通过这个获取
    func getRequestByTasksign(_ sign:String) ->LMNetWork?
    {
        if let request = self._requestMap[sign]
        {
            self._recentlyRequest = request
            return self
        }
        
        return nil
    }
    
    //解析数据
    func analyiseData(_ context:UIView,manager:LMRequestManager,isShowErrorStatuMsg:Bool,isShowSuccessStatuMsg:Bool ,response:(DataResponse<Any>))
    {
        
        switch response.result
        {
        case .success(let data):
            if let status = (data as AnyObject)["status"] as? Int
            {
                if let msg = (data as AnyObject)["msg"] as? String
                {
                    if isShowErrorStatuMsg
                    {
                        if status != 200 && status != 201 && status != 400
                        {
                            Utils.showError(context: context, errorStr: msg)
                        }
                    }
                    
                    if isShowSuccessStatuMsg
                    {
                        if status == 200
                        {
                            Utils.showError(context: context, errorStr: msg)
                        }
                    }
                }
            }
            print(data)
            self.requestSuccess(manager, result: LMResultMode(rootData: data as AnyObject))
            self.requestFinish(manager, result:LMResultMode(rootData: data as AnyObject) , error: nil)
        case .failure(let error):
            self.requestFail(context,manager: manager,error:error as NSError)
            self.requestFinish(manager, result:nil , error: error as NSError?)
        }
    }
    
    /**
     请求结束
     
     - parameter manager: 请求元
     - parameter result:  数据，如果请求失败，数据为空
     - parameter error:   错误，如果请求成功，错误为空
     */
    func requestFinish(_ manager:LMRequestManager,result:LMResultMode?,error:NSError?){}
    
    func requestSuccess(_ manager:LMRequestManager,result:LMResultMode)
    {
        let _ = Delegate_Selector(delegate, #selector(LMNetWorkDelegate.networkTaskDidSuccess(_:result:))){Void in self.delegate!.networkTaskDidSuccess!(manager, result: result)}
        
        if result.status == 200 || result.status == 10086
        {
            self.responseStateEq200(manager, result:result)
        }else
        {
            self.responseStateEq200(manager, result:result)
        }
    }
    
    func responseStateEq200(_ manager:LMRequestManager,result:LMResultMode){}
    
    func responseStateUnEq200(_ manager:LMRequestManager,result:LMResultMode){}
    
    func requestFail(_ context:UIView,manager:LMRequestManager,error:NSError)
    {
        //error:NSError?,msg
        let _ = Delegate_Selector(self.delegate, #selector(LMNetWorkDelegate.networkTaskDidFail(_:error:msg:))){Void in self.delegate!.networkTaskDidFail!(manager,error:error,msg:"")}
        
        if (self.reachability?.isReachable)! {
            Utils.showError(context: UIApplication.shared.keyWindow!, errorStr:Constant.NETWORK_DATA_ERR)
        }else {
            //无网络
            Utils.showError(context: UIApplication.shared.keyWindow!, errorStr:Constant.NETWORK_NO)
        }

    }
    
    
    
    ///刷新数据
    func refreshData()
    {
        didLoadMore = false
        self._recentlyRequest!.refreshData()
    }
    
    //终止数据请求
    func terminateNetWorkTask()
    {
        _recentlyRequest?.terminateNetWorkTask()
       let _ = Delegate_Selector(self.delegate, #selector(LMNetWorkDelegate.networkTaskDidTerminate(_:))){Void in self.delegate!.networkTaskDidTerminate!(self._recentlyRequest!)}
    }
    
    //暂停数据请求
    func interruptionNetWorkTask()
    {
        _recentlyRequest?.interruptionNetWorkTask()
        let _ = Delegate_Selector(self.delegate, #selector(LMNetWorkDelegate.networkTaskDidInterruption(_:))){Void in self.delegate!.networkTaskDidInterruption!(self._recentlyRequest!)}
    }
    
    
    
    //恢复请求
    func resumeNetWorkTask()
    {
        _recentlyRequest?.resumeNetWorkTask()
        let _ = Delegate_Selector(self.delegate, #selector(LMNetWorkDelegate.networkTaskDidStart(_:))){Void in self.delegate!.networkTaskDidStart!(self._recentlyRequest!)}
    }
}





@objc protocol LMNetWorkDelegate:NSObjectProtocol
{
    ///开始网络请求
    @objc optional func networkTaskDidStart(_ networkTask:LMRequestManager)
    
    ///暂停网络请求
    @objc optional func networkTaskDidInterruption(_ networkTask:LMRequestManager)
    
    ///终止网络请求
    @objc optional func networkTaskDidTerminate(_ networkTask:LMRequestManager)
    
    ///网络请求完成
    @objc optional func networkTaskDidSuccess(_ networkTask:LMRequestManager,result:LMResultMode)
    
    ///网络请求失败
    @objc optional func networkTaskDidFail(_ networkTask:LMRequestManager,error:NSError?,msg:String?)
}
