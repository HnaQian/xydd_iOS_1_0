//
//  GiftBasketRequestManager.swift
//  Limi
//
//  Created by Richie on 16/3/27.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//礼物蓝数据请求

import UIKit


class GiftBasketRequestManager: LMNetWork {
    
    let BASKET_LIST = Constant.JTAPI.gift_cart_list
    
    let BASKET_ADD = Constant.JTAPI.gift_cart_add
    
    let BASKET_DEL = Constant.JTAPI.gift_cart_del
    
    let BASKET_UPDATE = Constant.JTAPI.gift_cart_update
    
    let BASKET_UPDATE_NUM = Constant.JTAPI.gift_cart_update_num
    
    let BASKET_NUM = Constant.JTAPI.gift_cart_num
    
    // 收藏
    let COLLECT_ADD = Constant.JTAPI.collect_add
    let COLLECT_DEL = Constant.JTAPI.collect_del
    let COLLECT_LIST = Constant.JTAPI.collect_list
    
    /*
    static var collect_add: String {return HOST + "v1/collect/add"}
    static var collect_del: String {return HOST + "v1/collect/del"}
    static var collect_list: String {return HOST + "v1/collect/list"}
    */
    
    
    fileprivate var _delegate:GiftBasketRequestManagerDelegate?{return (self.delegate as? GiftBasketRequestManagerDelegate)}
    
    /**
     请求礼物蓝数据
     
     - parameter showError: 是否显示请求异常信息
     */
    func basketList(_ context:UIView,showError: Bool = true){
        self.requestData(context:context,URLString: BASKET_LIST,isNeedUserToken: true, isShowErrorStatuMsg: showError).taskSign(BASKET_LIST)
    }
    
    
    
    /**
     添加新数据到礼物蓝
     
     - parameter gids:       商品的ID
     - parameter subIds:     商品选择的规格的ID
     - parameter num:        商品的数量
     - parameter showError:  是否显示请求异常信息
     */
    func basketAdd(_ context:UIView,gids:[Int],subIds:[Int],num:[Int],showError:Bool = true,isNeedHud : Bool? = false){
        var parameters = [String:AnyObject]()
        parameters["goods_id"] = gids as AnyObject?
        parameters["goods_sub_id"] = subIds as AnyObject?
        parameters["num"] = num as AnyObject?
        
        self.requestData(context:context,URLString: BASKET_ADD, parameters: parameters, isNeedUserToken: true,isShowErrorStatuMsg: showError,isNeedHud : isNeedHud).taskSign(BASKET_ADD)
    }
    
    /**
     删除礼物蓝的商品
     
     - parameter giftBasktId: 选中的礼物的ID
     - parameter showError:   是否显示请求异常信息
     */
    func basketDelete(_ context:UIView,giftBasktId:[Int],showError:Bool = true){
        self.requestData(context:context,URLString: BASKET_DEL, parameters: ["id":giftBasktId as AnyObject], isNeedUserToken: showError, isShowErrorStatuMsg: true).taskSign(BASKET_DEL)
    }
    
    
    func basketUpdate(_ context:UIView,gids:[Int],subIds:[Int],num:[Int],showError:Bool = true){
        
        var parameters = [String:AnyObject]()
        parameters["goods_id"] = gids as AnyObject?
        parameters["goods_sub_id"] = subIds as AnyObject?
        parameters["num"] = num as AnyObject?
        
        self.requestData(context:context,URLString: BASKET_UPDATE, parameters: parameters, isNeedUserToken: true,isShowErrorStatuMsg: showError).taskSign(BASKET_UPDATE)
    }
    
    
    func baskeUpdateNum(_ context:UIView,giftBasketId:Int,num:Int,showError:Bool = true){
        
        var params = [String: AnyObject]()
        params["id"] = giftBasketId as AnyObject?
        params["num"] = num as AnyObject?
        
        self.requestData(context:context,URLString: BASKET_UPDATE_NUM,parameters: params,isNeedUserToken: true, isShowErrorStatuMsg: showError).taskSign(BASKET_UPDATE_NUM)
    }
    
    func basketNUm(_ context:UIView,showError:Bool = false){
        self.requestData(context:context,URLString: BASKET_NUM,isNeedUserToken: true, isShowErrorStatuMsg: showError).taskSign(BASKET_NUM)
    }
    
    //添加收藏
    func basketCollectAdd(_ context:UIView,ids:[Int],collectType:Int = 1,showError:Bool = true,showWaitting:Bool = true){
        
        var params = [String:AnyObject]()
        
        /*
        params["collect_type"] = 1
        params["id"] = cartItemGid
        */
        
        params["id"] = ids as AnyObject?
        params["collect_type"] = collectType as AnyObject?
        
        self.requestData(context:context,URLString: COLLECT_ADD, parameters: params, isNeedUserToken: true, isShowErrorStatuMsg: showError,isNeedHud : showWaitting).taskSign(COLLECT_ADD)
    }
    
    //取消收藏
    func basketCollectDelete(){}
    
    //加载收藏列表
    func basketCollectLoadList(){}
    
    override func requestSuccess(_ manager: LMRequestManager, result: LMResultMode) {
        if manager.taskSign == nil{return}
        let taskSign = manager.taskSign!
        
        if taskSign == BASKET_LIST{
            
            var ary = [GoodsOfBasketModel]()
            
            if let data = result.data,
                let list = JSON(data as AnyObject)["gift_basket_list"].arrayObject{
                    if list.count > 0{
                        let _ = list.map{ary.append(GoodsOfBasketModel($0 as! [String : AnyObject]))}
                    }
            }
           let _ = Delegate_Selector(_delegate, #selector(GiftBasketRequestManagerDelegate.giftBasketRequestManagerDidLoadList(_:result:list:))){Void in self._delegate!.giftBasketRequestManagerDidLoadList!(manager, result: result, list: ary)}
            
            
        }else if taskSign == BASKET_ADD{
           let _ = Delegate_Selector(_delegate, #selector(GiftBasketRequestManagerDelegate.giftBasketRequestManagerDidAdd(_:result:))){Void in self._delegate!.giftBasketRequestManagerDidAdd!(manager, result: result)}
            
            
        }else if taskSign == BASKET_DEL{
          let _ = Delegate_Selector(_delegate, #selector(GiftBasketRequestManagerDelegate.giftBasketRequestManagerDidDelete(_:result:))){Void in self._delegate!.giftBasketRequestManagerDidDelete!(manager, result: result)}
            
            
        }else if taskSign == BASKET_UPDATE{
           let _ = Delegate_Selector(_delegate, #selector(GiftBasketRequestManagerDelegate.giftBasketRequestManagerDidUpdate(_:result:))){Void in self._delegate!.giftBasketRequestManagerDidUpdate!(manager, result: result)}
            
            
        }else if taskSign == BASKET_UPDATE_NUM{
           let _ = Delegate_Selector(_delegate, #selector(GiftBasketRequestManagerDelegate.giftBasketRequestManagerDidUpdateNum(_:result:))){Void in self._delegate!.giftBasketRequestManagerDidUpdateNum!(manager, result: result)}
            
            
        }else if taskSign == BASKET_NUM{
            
           let _ = Delegate_Selector(_delegate, #selector(GiftBasketRequestManagerDelegate.giftBasketRequestManagerDidNum(_:result:))){Void in self._delegate!.giftBasketRequestManagerDidNum!(manager, result: result)}
            
            
        }else if taskSign == COLLECT_ADD{
           let _ = Delegate_Selector(_delegate, #selector(GiftBasketRequestManagerDelegate.giftBasketRequestManagerCollectDidAddSuccess(_:result:))){Void in self._delegate!.giftBasketRequestManagerCollectDidAddSuccess!(manager, result: result)}
            
            
        }
        else if taskSign == COLLECT_DEL{
            let _ = Delegate_Selector(_delegate, #selector(GiftBasketRequestManagerDelegate.giftBasketRequestManagerCollectDidDeleteSuccess(_:result:))){Void in self._delegate!.giftBasketRequestManagerCollectDidDeleteSuccess!(manager, result: result)}
            
            
        }
        else if taskSign == COLLECT_LIST{
             let _ = Delegate_Selector(_delegate, #selector(GiftBasketRequestManagerDelegate.giftBasketRequestManagerCollectDidLoadList(_:result:))){Void in self._delegate!.giftBasketRequestManagerCollectDidLoadList!(manager, result: result)}
            
        }
    }
}



@objc protocol GiftBasketRequestManagerDelegate:LMNetWorkDelegate{
    
    @objc optional func giftBasketRequestManagerDidLoadList(_ manager:LMRequestManager,result:LMResultMode,list:[GoodsOfBasketModel])
    
    @objc optional func giftBasketRequestManagerDidAdd(_ manager:LMRequestManager,result:LMResultMode)
    
    @objc optional func giftBasketRequestManagerDidDelete(_ manager:LMRequestManager,result:LMResultMode)
    
    @objc optional func giftBasketRequestManagerDidUpdate(_ manager:LMRequestManager,result:LMResultMode)
    
    @objc optional func giftBasketRequestManagerDidUpdateNum(_ manager:LMRequestManager,result:LMResultMode)
    
    @objc optional func giftBasketRequestManagerDidNum(_ manager:LMRequestManager,result:LMResultMode)
    

    @objc optional func giftBasketRequestManagerCollectDidAddSuccess(_ manager:LMRequestManager,result:LMResultMode)
    
    @objc optional func giftBasketRequestManagerCollectDidDeleteSuccess(_ manager:LMRequestManager,result:LMResultMode)
    
    @objc optional func giftBasketRequestManagerCollectDidLoadList(_ manager:LMRequestManager,result:LMResultMode)
}
