//
//  LMRequestMode.swift
//  Limi
//
//  Created by Richie on 16/3/10.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class LMRequestMode: BaseDataMode {
    
    var method: HTTPMethod = .post
    
    var context: UIView? = nil
    
    var URLString: URLConvertible = ""
    
    var parameters: [String: AnyObject]? = nil
    
    var isNeedUserToken : Bool? = false
    
    var isNeedAppToken : Bool? = false
    
    var isShowSuccessStatuMsg:Bool? = false
    
    var isShowErrorStatuMsg:Bool? = false
    
    var isNeedHud : Bool? = false
    
    var hudTitle: String? = nil
    
    var encoding: URLEncoding = URLEncoding.default
    
    var headers: [String: String]? = nil
    
    var successHandler: ((_ data:[String: AnyObject],_ status:Int,_ msg:String) ->Void)? = nil
    
    var failureHandler:(()->Void)? = nil
    
    var taskID = 0
}

class LMResultMode: BaseDataMode {
    
    init(rootData: AnyObject)
    {
        super.init()
        
        let tempData = rootData as! [String : AnyObject]
        
        self.rootData = tempData
        print(tempData["data"] as? [String : AnyObject])
        self.data = tempData["data"] as? [String : AnyObject]
        
        if let mdata = self.data
        {
            if let mtoken  = JSON(mdata as AnyObject)["token"].string
            {
                self.token = mtoken
            }
        }
        
        if let m = tempData["msg"] as? String
        {
            self.msg = m
        }

        if let stauts = tempData["status"] as? Int
        {
            self.status = stauts
        }
    }
    
    var rootData:[String:AnyObject]!
    
    var data:[String:AnyObject]?{didSet{
        if let mdata = self.data
        {
            if let token : String = JSON(mdata as AnyObject)["token"].string{
                if token != ""{
                    UserDefaults.standard.set(token, forKey: "UserToken")
                }
            }
            
            if let uid : Int = JSON(mdata as AnyObject)["uid"].int{
                UserDefaults.standard.set(uid, forKey: "UserUid")
            }
            
            UserDefaults.standard.synchronize()
        }
        }}
    
    var token:String?
    
    var msg:String = ""
    
    var status:Int = 10086
    
    var error:NSError?
}
