//
//  LDSDKAliPayServiceImpl.m
//  LDSDKManager
//
//  Created by ss on 15/8/21.
//  Copyright (c) 2015年 张海洋. All rights reserved.
//

#import "LDSDKAliPayServiceImpl.h"
#import <AlipaySDK/AlipaySDK.h>
#import "Order.h"
#import "DataSigner.h"

@interface LDSDKAliPayServiceImpl ()

@property (strong, nonatomic) NSString *aliPayScheme;

@end

@implementation LDSDKAliPayServiceImpl

+ (instancetype)sharedService
{
    static LDSDKAliPayServiceImpl *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}


#pragma mark -
#pragma mark - 配置部分

- (BOOL)isPlatformAppInstalled
{
    return YES;
}


- (void)registerWithPlatformConfig:(NSDictionary *)config
{
    if (config == nil || config.allKeys.count == 0) return;

    NSString *appScheme = config[LDSDKConfigAppSchemeKey];
    if (appScheme && [appScheme length]) {
        self.aliPayScheme = appScheme;
    }
}

- (BOOL)isRegistered
{
    return (self.aliPayScheme && [self.aliPayScheme length]);
}

- (BOOL)handleResultUrl:(NSURL *)url
{
    return [self payProcessOrderWithPaymentResult:url standbyCallback:NULL];
}


#pragma mark -
#pragma mark -  支付部分

- (void)payOrder:(NSDictionary *)orderDictionary callback:(LDSDKPayCallback)callback
{ 
    
    [self alipayOrder:[orderDictionary objectForKey:@"param"] callback:callback];
//    Order *order = [[Order alloc] init];
//    order.partner = [orderDictionary objectForKey:@"partner"];
//    order.seller = [orderDictionary objectForKey:@"seller_id"];
//    order.tradeNO = [orderDictionary objectForKey:@"out_trade_no"]; //订单ID（由商家自行制定）
//    order.productName = [orderDictionary objectForKey:@"subject"]; //商品标题
//    order.productDescription = [orderDictionary objectForKey:@"body"]; //商品描述
//    order.amount = [orderDictionary objectForKey:@"total_fee"];//商品价格
//    order.notifyURL =  [orderDictionary objectForKey:@"notify_url"]; //回调URL
//    
//    order.service = [orderDictionary objectForKey:@"service"];
//    order.paymentType = [orderDictionary objectForKey:@"payment_type"];
//    order.inputCharset = [orderDictionary objectForKey:@"_input_charset"];
//    order.itBPay = [orderDictionary objectForKey:@"it_b_pay"];
//    order.showUrl = @"m.alipay.com";
//    
//    //应用注册scheme,在AlixPayDemo-Info.plist定义URL types
//    NSString *appScheme = @"alisdk";
//    
//    //将商品信息拼接成字符串
//    NSString *orderSpec = [order description];
//    NSLog(@"orderSpec = %@",orderSpec);
//    
//    //获取私钥并将商户信息签名,外部商户可以根据情况存放私钥和签名,只需要遵循RSA签名规范,并将签名字符串base64编码和UrlEncode
//    id<DataSigner> signer = CreateRSADataSigner([orderDictionary objectForKey:@"rsa_private"]);
//    NSString *signedString = [signer signString:orderSpec];
//    
//    //将签名成功字符串格式化为订单字符串,请严格按照该格式
//    NSString *orderString = nil;
//    if (signedString != nil) {
//        orderString = [NSString stringWithFormat:@"%@&sign=\"%@\"&sign_type=\"%@\"",
//                       orderSpec, signedString, @"RSA"];
//        [self alipayOrder:orderString callback:callback];
//        
//        [[AlipaySDK defaultService] payOrder:orderString fromScheme:appScheme callback:^(NSDictionary *resultDic) {
//            NSLog(@"reslut = %@",resultDic);
//        }];
//    }
    
    
    
}

- (BOOL)payProcessOrderWithPaymentResult:(NSURL *)url
                         standbyCallback:(void (^)(NSDictionary *))callback
{
    if ([url.scheme isEqualToString:@"Limi"]) {
        [self aliPayProcessOrderWithPaymentResult:url standbyCallback:callback];
        return YES;
    } else {
        return NO;
    }
}


#pragma mark - alipay
- (void)alipayOrder:(NSString *)orderString callback:(LDSDKPayCallback)callback
{
    [[AlipaySDK defaultService]
          payOrder:orderString
        fromScheme:@"Limi"
          callback:^(NSDictionary *resultDic) {
              NSString *signString = [resultDic objectForKey:@"result"];
              NSString *memo = [resultDic objectForKey:@"memo"];
              NSInteger resultStatus = [[resultDic objectForKey:@"resultStatus"] integerValue];
              NSLog(@"%@   %@   %ld",signString,memo,(long)resultStatus);
              if (callback) {
                  dispatch_async(dispatch_get_main_queue(), ^{
                      if (resultStatus == 9000) {
                          callback(signString, nil);
                      } else {
                          NSString *errorStr = memo;
                          if (resultStatus == 8000){
                              errorStr = @"支付失败";
                          }
                          else if (resultStatus == 6001){
                              errorStr = @"支付取消";
                          }
                          NSError *error = [NSError
                              errorWithDomain:@"AliPay"
                                         code:0
                                     userInfo:[NSDictionary
                                                  dictionaryWithObjectsAndKeys:
                                                      errorStr, @"NSLocalizedDescription", nil]];
                          callback(signString, error);
                      }

                  });
              }
          }];
}

- (void)aliPayProcessOrderWithPaymentResult:(NSURL *)url
                            standbyCallback:(void (^)(NSDictionary *resultDic))callback
{
    [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:callback];
}

@end
