//
//  OIUPPayControl.swift
//  Limi
//
//  Created by maohs on 16/11/3.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class OIUPPayControl: NSObject {
   
    static let shareControl = OIUPPayControl()
    fileprivate override init() {}
    
    var OIUPPaymentResultBlock: ((_ code:String?,_ dict:[AnyHashable:Any]?)->Void)?
    
    func handleOpenURL(url:URL) ->Bool {
        UPPaymentControl.default().handlePaymentResult(url) { (code, data) in
            if (self.OIUPPaymentResultBlock != nil) {
                self.OIUPPaymentResultBlock!(code,data)
            }
        }
        return true
    }
    
    func startPay(_ tn:String,fromScheme:String,mode:String,viewController:UIViewController) ->Bool {
        let temp = UPPaymentControl.default().startPay(tn, fromScheme: fromScheme, mode: mode, viewController: viewController)
        return temp
    }
}
