//
//  LazyPageScrollView.swift
//  Limi
//
//  Created by maohs on 16/5/23.
//  Copyright (c) 2015 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

public enum TabItemType: Int {
    case systemSegment
    case customView
}

@objc protocol LazyPageScrollViewDelegate:NSObjectProtocol {
    @objc optional func lazyPageScrollView(_ pageScrollView:LazyPageScrollView,changeIndex index :Int)
    @objc optional func lazyPageScrollViewEdgeSwipeLeft(_ isLeft:Bool)
}

class LazyPageScrollView: UIView,UIScrollViewDelegate,UIGestureRecognizerDelegate {

    var tabItem = UIView()
    
    var currentTabItemType = TabItemType.systemSegment
    weak var delegate:LazyPageScrollViewDelegate?
    
    
    fileprivate var _viewTop = UIView()
    fileprivate let _viewScrollMain = UIScrollView()
    
    fileprivate var _selectIndex:Int = -1
    fileprivate var _distance:CGFloat = 0
    
    fileprivate var _leftRec:UISwipeGestureRecognizer?
    fileprivate var _rightRec:UISwipeGestureRecognizer?
    
    fileprivate var _titleArray = [String]()
    fileprivate var _viewArray = [UIView]()
    
    
    
    init(verticalDistance:CGFloat,tabItemType:TabItemType) {
        
        super.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 0))
        
        _distance = verticalDistance
        currentTabItemType = tabItemType
        setUpUI()
    }
    
    
    func segment(_ titles:[String],views:[UIView],frame:CGRect) {
        
        _titleArray = titles
        _viewArray = views
        
        self.frame = frame
       
        generateUI()
    }

    
    func updateGapHeight(_ gapHeight:CGFloat){
        
        _viewScrollMain.itop = _viewScrollMain.itop - gapHeight
        _viewScrollMain.iheight = self.iheight  - _viewScrollMain.itop
        let _ = _viewArray.map{$0.iheight = _viewScrollMain.iheight}
        _viewScrollMain.contentSize = CGSize(width: _viewScrollMain.iwidth * CGFloat(_viewArray.count), height: _viewScrollMain.iheight)
    }
    
    
    //MARK: 加载头部item
    fileprivate func generateUI() {
        _viewTop.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: 80 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
        _viewScrollMain.frame = CGRect(x: 0, y: _viewTop.ibottom + _distance, width: self.frame.width, height: self.iheight - _viewTop.ibottom - _distance)
        
        if currentTabItemType == .systemSegment {
            self.tabItem = UISegmentedControl(items: _titleArray)
            (self.tabItem as! UISegmentedControl).selectedSegmentIndex = 0
            _viewTop.addSubview(self.tabItem)
            
            (self.tabItem as! UISegmentedControl).addTarget(self, action: #selector(LazyPageScrollView.segmentValueChanged(_:)), for: .valueChanged)
        }else {
            self.tabItem = LMSegmentedControl(frame: CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: 80 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE), selectedHandle: { [weak self](index) -> Void in
                self?.selectedIndex(index)
            })
            (self.tabItem as! LMSegmentedControl).titles = _titleArray
            _viewTop.addSubview(self.tabItem)
        }

        for index in 0 ..< _viewArray.count {
            let tempView = _viewArray[index]
            
            tempView.tag = index
            
            tempView.frame = CGRect(x: _viewScrollMain.iwidth * CGFloat(index), y: 0, width: _viewScrollMain.iwidth,height: _viewScrollMain.iheight)
            
            _viewScrollMain.addSubview(tempView)
        }
        
        _viewScrollMain.bounces = false
        _viewScrollMain.addGestureRecognizer(_rightRec!)
        if _viewArray.count == 0 {
            _viewScrollMain.addGestureRecognizer(_leftRec!)
        }
        
        _viewScrollMain.contentSize = CGSize(width: _viewScrollMain.frame.size.width * CGFloat(_viewArray.count), height: _viewScrollMain.frame.size.height)
        
    }
    
    
    //MARK: - 处理页面滑动、点击
    @objc fileprivate func segmentValueChanged(_ sender:AnyObject) {
        self.selectedIndex((sender as! UISegmentedControl).selectedSegmentIndex)
    }
    
    
    fileprivate func selectedIndex(_ index:Int) {
        if _selectIndex != index{
            _selectIndex = index
            _viewScrollMain.setContentOffset(CGPoint(x: CGFloat(index) * _viewScrollMain.bounds.size.width, y: 0), animated: false)
            delegate?.lazyPageScrollView!(self, changeIndex: index)
        }
    }
    
    fileprivate func pageChange(_ index:Int) {
        if(index == 0)
        {
            _viewScrollMain.addGestureRecognizer(_rightRec!)
        }
        else if (index == _viewArray.count-1)
        {
            _viewScrollMain.addGestureRecognizer(_leftRec!)
        }
        else
        {
            _viewScrollMain.removeGestureRecognizer(_leftRec!)
            _viewScrollMain.removeGestureRecognizer(_rightRec!)
        }
        
        UIView.animate(withDuration: 0.2, animations: { 
            self.layoutIfNeeded()
        }) 
        delegate?.lazyPageScrollView!(self, changeIndex: index)

    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let offset = scrollView.contentOffset;
        
        if offset.x.truncatingRemainder(dividingBy: scrollView.frame.size.width) == 0{
            
            let indexNew = Int(offset.x / scrollView.frame.size.width)
            
            if(_selectIndex == indexNew)
            {
                return;
            }
            
            _selectIndex = indexNew;
            if (self.currentTabItemType == .systemSegment) {
                (self.tabItem as! UISegmentedControl).selectedSegmentIndex = indexNew
            }else {
                
                (self.tabItem as! LMSegmentedControl).currentIndex = indexNew
            }
            self.pageChange(indexNew)
        }
    }
    
    func enableScroll(_ canScroll:Bool) {
        _viewScrollMain.isScrollEnabled = canScroll
    }
    
    //MARK: - 页面滑动的方向
   @objc fileprivate func onLeftRect() {
        delegate?.lazyPageScrollViewEdgeSwipeLeft!(true)
    }
    
   @objc fileprivate func onRightRec() {
        delegate?.lazyPageScrollViewEdgeSwipeLeft!(false)
    }
    
    deinit {
        _titleArray.removeAll()
        _viewArray.removeAll()
    }
    
    
    
    fileprivate func setUpUI() {
        
        _leftRec = UISwipeGestureRecognizer(target: self, action: #selector(LazyPageScrollView.onLeftRect))
        _leftRec?.delegate = self
        _leftRec?.direction = UISwipeGestureRecognizerDirection.left
        
        _rightRec = UISwipeGestureRecognizer(target: self, action: #selector(LazyPageScrollView.onRightRec))
        _rightRec?.delegate = self
        _rightRec?.direction = UISwipeGestureRecognizerDirection.right
        
        
        self.addSubview(_viewTop)
        self.addSubview(_viewScrollMain)
        _viewScrollMain.showsHorizontalScrollIndicator = false
        _viewScrollMain.showsVerticalScrollIndicator = false
        _viewScrollMain.isPagingEnabled = true
        _viewScrollMain.isDirectionalLockEnabled = true
        _viewScrollMain.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
