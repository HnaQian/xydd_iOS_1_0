//
//  CheShiViewController.swift
//  测试UIActionSheet
//
//  Created by meimao on 15/10/15.
//  Copyright © 2015年 meimao. All rights reserved.
//

import UIKit

class PopTotastView: UIView{
    
    var margon: CGFloat = 50
    var spaceH: CGFloat = 5
    
    // 屏幕宽、高
    let screenWidth = Constant.ScreenSize.SCREEN_WIDTH
    let screenHeight = Constant.ScreenSize.SCREEN_HEIGHT
    var table = UITableView()
    var arr = [[String: AnyObject]]()
    var cancelBtn = UIButton()
    var contentView = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        table.delegate = self
        table.dataSource = self
        table.showsHorizontalScrollIndicator = false
        table.showsVerticalScrollIndicator = false
        self.addSubview(contentView)
        contentView.addSubview(table)
        contentView.addSubview(cancelBtn)
        
        contentView.backgroundColor = UIColor(rgba: Constant.common_C5_color)
        
        table.register(tableCell.self, forCellReuseIdentifier: "cell")
        table.layer.borderWidth = 0.5
        table.backgroundColor = UIColor.white
        table.layer.borderColor = UIColor(rgba: Constant.common_separatLine_color).cgColor
        table.separatorStyle = UITableViewCellSeparatorStyle.none
        self.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        
        // 取消按钮设置
        cancelBtn.backgroundColor = UIColor.white
        cancelBtn.setTitle("取消", for: UIControlState())
        cancelBtn.setTitleColor(UIColor(rgba: Constant.common_C2_color), for: UIControlState())
        cancelBtn.titleLabel?.font = UIFont.systemFont(ofSize: Constant.common_F2_font)
        cancelBtn.layer.borderColor = UIColor(rgba: Constant.common_separatLine_color).cgColor
        cancelBtn.layer.borderWidth = 0.5
        cancelBtn.addTarget(self, action: #selector(PopTotastView.cancelAction), for: .touchUpInside)
        
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var dismissHandler: ((_ dic: [String: AnyObject])->Void)?
    
    class tableCell: UITableViewCell {
        var label = UILabel()
        var separaLine = UIView()
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)
            self.contentView.addSubview(label)
            self.contentView.addSubview(separaLine)
            label.addAttribute(font: Constant.common_F2_font, textAlignment: NSTextAlignment.center, textColor: Constant.common_C2_color, text: nil)
            
            separaLine.backgroundColor = UIColor(rgba: Constant.common_separatLine_color)
            label.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(self)
                let _ = make.center.equalTo(self)
            }
            separaLine.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(self)
                let _ = make.right.equalTo(self)
               let _ =  make.bottom.equalTo(self)
                let _ = make.height.equalTo(0.5)
            }
            
            self.backgroundColor = UIColor.white
            
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    
    func addCell(_ array: [[String: AnyObject]], handler: @escaping ((_ dic: [String: AnyObject])->Void)) {
        arr = array
        dismissHandler = handler
    }
    
    func showTableView() {
        
        let height = CGFloat(arr.count) * margon
        
        table.reloadData()
        
        if height <= Constant.ScreenSize.SCREEN_HEIGHT/2 - (margon + spaceH) {
            
            contentView.frame = CGRect(x: 0, y: Constant.ScreenSize.SCREEN_HEIGHT, width: Constant.ScreenSize.SCREEN_WIDTH, height: height + spaceH + margon)
            table.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH, height: height)
        }
        else
        {
            contentView.frame = CGRect(x: 0, y: Constant.ScreenSize.SCREEN_HEIGHT, width: Constant.ScreenSize.SCREEN_WIDTH, height: Constant.ScreenSize.SCREEN_HEIGHT/2)
            table.frame = CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH,height: Constant.ScreenSize.SCREEN_HEIGHT/2 - (margon + spaceH))
        }
        cancelBtn.frame = CGRect(x: 0, y: table.frame.maxY + spaceH, width: Constant.ScreenSize.SCREEN_WIDTH, height: margon)
        
        UIView.animate(withDuration: 0.25, animations: { () -> Void in
            if height <= Constant.ScreenSize.SCREEN_HEIGHT/2 - (self.margon + self.spaceH) {
                
                self.contentView.iy = Constant.ScreenSize.SCREEN_HEIGHT - height - (self.margon + self.spaceH)
            }
            else
            {
                self.contentView.iy = Constant.ScreenSize.SCREEN_HEIGHT/2
            }
            
        }) 
        
    }
    
    
    func cancelAction() {
        UIView.animate(withDuration: 0.25, animations: { () -> Void in
            
            self.contentView.iy = Constant.ScreenSize.SCREEN_HEIGHT
            
            }, completion: { (value: Bool) -> Void in
                self.alpha = 0
                self.isHidden = true
        }) 
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            
            self.contentView.iy = Constant.ScreenSize.SCREEN_HEIGHT
            
            }, completion: { (value: Bool) -> Void in
                self.alpha = 0
                self.isHidden = true
        }) 
    }
    
}


extension PopTotastView: UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return margon
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? tableCell
        let oneDic = arr[(indexPath as NSIndexPath).row] as [String: AnyObject]
        cell!.label.text = oneDic.values.first as? String
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let selectDic = arr[(indexPath as NSIndexPath).row] as [String: AnyObject]
        
        self.dismissHandler?(selectDic)
    
        
        UIView.animate(withDuration: 0.35, animations: { () -> Void in
            
            self.contentView.iy = Constant.ScreenSize.SCREEN_HEIGHT
            
            }, completion: { (value: Bool) -> Void in
                self.alpha = 0
                self.isHidden = true
        }) 
        
    }
    
}
