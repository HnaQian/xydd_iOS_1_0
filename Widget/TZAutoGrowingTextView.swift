//
//  TZAutoGrowingTextView.swift
//  TZAutoGrowingTextView
//
//  Created by Thomas Zhao on 12/28/14.
//  Copyright (c) 2014 thomasrzhao. All rights reserved.
//

import UIKit

open class TZAutoGrowingTextView: UITextView {

    fileprivate let contentSizeKeyPath = "contentSize";
    fileprivate var kvoContext = UInt8();
    
    public override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer);
        commonInit();
    }
    
    public required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!;
        commonInit();
    }
    
    fileprivate func commonInit() {
        super.addObserver(self, forKeyPath: contentSizeKeyPath, options: [.initial , .new], context: &kvoContext);
    }
    
    deinit {
        super.removeObserver(self, forKeyPath: contentSizeKeyPath, context: &kvoContext);
    }
    
    open override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        switch(keyPath, context) {
        case(contentSizeKeyPath?, _):
            invalidateIntrinsicContentSize();
            layoutIfNeeded();
            if(isFirstResponder) { scrollRangeToVisible(selectedRange); }
        default: super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context);
        }
    }
    
    open override var intrinsicContentSize : CGSize {
        return contentSize;
    }
    
}
