//
//  LMButton.swift
//  Limi
//
//  Created by Richie on 16/3/23.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

enum LMButtonType:Int{
    
    case none
    
    //红字、红边、圆角：3、白底
    case title_bord_red
    
    //白字、红底、圆角：3
    case titleWhite_bgRed
    
    //灰字、灰边、白底、圆角：3
    case title_bord_gray

    //黑字、黑边、白底、圆角：3
    case title_bord_black
    
    //红字、白底
    case titleRed_bgWhite

    //黑字、黑边、白底、圆角：3
    case title_gray
    
    //有选中功能的button的未选中状态:发票内容选项按钮
    case selectedAviable_title_bord_gray
    
    //有选中功能的button的选中状态：发票内容选项按钮
    case selectedAviable_title_bord_red
}


class LMButton: UIControl {
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
    // Drawing code
    }
    */
    
    var title:String!{didSet{_label.text = title}}
    
    var indetifer = ""
    
    var type:LMButtonType{return _type}
    
    fileprivate var _label = UILabel()
    fileprivate var _bgImageView:UIImageView!
    
    fileprivate var bgImageView:UIImageView!{
        
        if _bgImageView == nil
        {
            _bgImageView = UIImageView()
            _bgImageView.contentMode = UIViewContentMode.scaleToFill
            
            self.addSubview(_bgImageView)
            
            _bgImageView.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(self)
                let _ = make.left.equalTo(self)
                let _ = make.right.equalTo(self)
                let _ = make.bottom.equalTo(self)
            }
            
            self.bringSubview(toFront: _label)
        }
        
        return _bgImageView
    }
    
    fileprivate var _type:LMButtonType = .none
    
    init(type:LMButtonType){
        
        super.init(frame:CGRect.zero)
        
        self.refreshButtonType(type)
    }
    
    
    
    func refreshButtonType(_ type:LMButtonType)
    {
        _type = type
        
        if self.subviews.contains(_label){
            _label.removeFromSuperview()
            _label = UILabel()
        }
        
        self.addSubview(_label)
        
        _label.text = title
        _label.textAlignment = NSTextAlignment.center
        
        _label.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self)
            let _ = make.left.equalTo(self)
            let _ = make.right.equalTo(self)
            let _ = make.bottom.equalTo(self)
        }
        
        self.backgroundColor = UIColor.white
        self.layer.cornerRadius = 3
        self.clipsToBounds = true
        self.backgroundColor = UIColor.white
        
        
        _label.layer.cornerRadius = 3
        _label.clipsToBounds = true
        _label.font = UIFont.systemFont(ofSize: 15)
        
        self.setProperty(type)
    }
    
    
    fileprivate func setProperty(_ type:LMButtonType){
        
        var titleColor = UIColor.black
        var bgColor = UIColor.clear
        var bordColor:CGColor?
        var bgImage:UIImage?
        
        switch type{
            
        case .none:print("none")

        case .title_bord_gray:
            titleColor = UIColor(rgba: Constant.common_C8_color)
            bordColor = UIColor(rgba: Constant.common_C8_color).cgColor
            
        case .title_bord_black:
            titleColor = UIColor(rgba: Constant.common_C2_color)
            bordColor = UIColor(rgba: Constant.common_C2_color).cgColor
            
            
        case .title_bord_red:
            titleColor = UIColor(rgba: Constant.common_red_color)
            bordColor = UIColor(rgba: Constant.common_red_color).cgColor
            
        case .titleWhite_bgRed:
            titleColor = UIColor.white
            bgColor = UIColor(rgba: Constant.common_red_color)
            
        case .selectedAviable_title_bord_gray:
            bgImage = UIImage(named: "goods_order_info_attribute_button")
            titleColor = UIColor(rgba: Constant.common_C8_color)
            
        case .selectedAviable_title_bord_red:
            bgImage = UIImage(named: "goods_order_info_attribute_button_selected")
            titleColor = UIColor(rgba: Constant.common_red_color)
            
        case .title_gray:
            bgImage = nil
            titleColor = UIColor(rgba: Constant.common_C8_color)
            
        case .titleRed_bgWhite:
            titleColor = UIColor(rgba: Constant.common_red_color)
        }

        _label.backgroundColor = bgColor

        _label.textColor = titleColor
        
        if bordColor != nil{
            _label.layer.borderColor = bordColor
            _label.layer.borderWidth = 0.5
        }
        
        if bgImage != nil{bgImageView.image = bgImage}
        
        print(self.subviews.count)
        print(self.subviews.description)
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
}
