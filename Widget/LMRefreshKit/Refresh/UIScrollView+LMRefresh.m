//
//  UIScrollView+LMRefresh.m
//  RQRefresh
//
//  Created by Richie on 16/7/11.
//  Copyright © 2016年 Richie. All rights reserved.
//

#import <objc/runtime.h>

#import "LMRefreshFooter.h"
#import "LMRefreshHeader.h"



#import "UIScrollView+LMRefresh.h"

@implementation NSObject (LMRefresh)

//偷天换日
+ (void)exchangeInstanceMethod1:(SEL)method1 method2:(SEL)method2
{
    method_exchangeImplementations(class_getInstanceMethod(self, method1), class_getInstanceMethod(self, method2));
}

+ (void)exchangeClassMethod1:(SEL)method1 method2:(SEL)method2
{
    method_exchangeImplementations(class_getClassMethod(self, method1), class_getClassMethod(self, method2));
}

@end

@implementation UIScrollView (LMRefresh)

#pragma mark - header
static const char RefreshHeaderKey = '\0';
- (void)setRefreshHeader:(RefreshHeader *)refreshHeader
{
    if (refreshHeader != self.refreshHeader) {
        // 删除旧的，添加新的
        [self.refreshFooter removeFromSuperview];
        [self insertSubview:refreshHeader atIndex:0];
        
        // 存储新的
        [self willChangeValueForKey:@"refreshHeader"]; // KVO
        objc_setAssociatedObject(self, &RefreshHeaderKey,
                                 refreshHeader, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        [self didChangeValueForKey:@"refreshHeader"]; // KVO
    }
}

- (RefreshHeader *)refreshHeader
{
    return objc_getAssociatedObject(self, &RefreshHeaderKey);
}

#pragma mark - footer
static const char RefreshFooterKey = '\0';
- (void)setRefreshFooter:(RefreshFooter *)refreshFooter
{
    if (refreshFooter != self.refreshFooter) {
        // 删除旧的，添加新的
        [self.refreshFooter removeFromSuperview];
        [self addSubview:refreshFooter];
        
        // 存储新的
        [self willChangeValueForKey:@"refreshFooter"]; // KVO
        objc_setAssociatedObject(self, &RefreshFooterKey,
                                 refreshFooter, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        [self didChangeValueForKey:@"refreshFooter"]; // KVO
    }
}


- (RefreshFooter *)refreshFooter
{
    return objc_getAssociatedObject(self, &RefreshFooterKey);
}

#pragma mark - other 计算tableview一共有多少row
- (NSInteger)totalDataCount
{
    NSInteger totalCount = 0;
    if ([self isKindOfClass:[UITableView class]]) {
        UITableView *tableView = (UITableView *)self;
        
        for (NSInteger section = 0; section<tableView.numberOfSections; section++) {
            totalCount += [tableView numberOfRowsInSection:section];
        }
    } else if ([self isKindOfClass:[UICollectionView class]]) {
        UICollectionView *collectionView = (UICollectionView *)self;
        
        for (NSInteger section = 0; section<collectionView.numberOfSections; section++) {
            totalCount += [collectionView numberOfItemsInSection:section];
        }
    }
    return totalCount;
}



//给刷新block赋值
static const char RefreshReloadDataBlockKey = '\0';
- (void)setReloadDataBlock:(void (^)(NSInteger))reloadDataBlock
{
    reloadDataBlock(0);
    [self willChangeValueForKey:@"reloadDataBlock"]; // KVO
    objc_setAssociatedObject(self, &RefreshReloadDataBlockKey, reloadDataBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
    [self didChangeValueForKey:@"reloadDataBlock"]; // KVO
}

- (void (^)(NSInteger))reloadDataBlock
{
    return objc_getAssociatedObject(self, &RefreshReloadDataBlockKey);
}

//tableView刷新时调用此函数 此函数的作用是如果自动隐藏刷新的话，在此函数里面隐藏刷新控件
- (void)executeReloadDataBlock
{
    //如果footer不为空，当数据为0时，隐藏footer
    if (self.refreshFooter != NULL) {
        if (self.totalDataCount == 0) {
            self.refreshFooter.hidden = true;
        }
    }
    
    if (self.reloadDataBlock == NULL){
        NSLog(@"It's null!!!");
    }else{
        self.reloadDataBlock(self.totalDataCount);
    }
}






- (void)setInsetT:(CGFloat)insetT
{
    UIEdgeInsets inset = self.contentInset;
    inset.top = insetT;
    self.contentInset = inset;
}

- (CGFloat)insetT
{
    return self.contentInset.top;
}

- (void)setInsetB:(CGFloat)insetB
{
    UIEdgeInsets inset = self.contentInset;
    inset.bottom = insetB;
    self.contentInset = inset;
}

- (CGFloat)insetB
{
    return self.contentInset.bottom;
}

- (void)setInsetL:(CGFloat)insetL
{
    UIEdgeInsets inset = self.contentInset;
    inset.left = insetL;
    self.contentInset = inset;
}

- (CGFloat)insetL
{
    return self.contentInset.left;
}

- (void)setInsetR:(CGFloat)insetR
{
    UIEdgeInsets inset = self.contentInset;
    inset.right = insetR;
    self.contentInset = inset;
}

- (CGFloat)insetR
{
    return self.contentInset.right;
}

- (void)setOffsetX:(CGFloat)offsetX
{
    CGPoint offset = self.contentOffset;
    offset.x = offsetX;
    self.contentOffset = offset;
}

- (CGFloat)offsetX
{
    return self.contentOffset.x;
}

- (void)setOffsetY:(CGFloat)offsetY
{
    CGPoint offset = self.contentOffset;
    offset.y = offsetY;
    self.contentOffset = offset;
}

- (CGFloat)offsetY
{
    return self.contentOffset.y;
}

- (void)setContentW:(CGFloat)contentW
{
    CGSize size = self.contentSize;
    size.width = contentW;
    self.contentSize = size;
}

- (CGFloat)contentW
{
    return self.contentSize.width;
}

- (void)setContentH:(CGFloat)contentH
{
    CGSize size = self.contentSize;
    size.height = contentH;
    self.contentSize = size;
}

- (CGFloat)contentH
{
    return self.contentSize.height;
}



-(TipesManager *)manager
{
    return [TipesManager newManager:self];
}

-(void)setTipeImage:(NSString *)tipeImage andTipeContent:(NSString *)tipeContent andForTipeType:(UIScrollTipeMode)tipeMode{}


-(TipesManager *)showTipe:(UIScrollTipeMode)tipeMode{
    [self closeAllRefresh];
    return [self.manager showTipeWithTipeMode:tipeMode];
}

-(TipesManager *)closeTipe
{
    return [self.manager closeTipe];
}


-(void)addHeaderRefresh:(RefreshComponentRefreshingBlock) freshBlock{
    if (self.refreshHeader != NULL) {
        [self.refreshHeader removeFromSuperview];
    }
    self.refreshHeader = [LMRefreshHeader headerWithRefreshingBlock:freshBlock];
}


-(void)addFooterRefresh:(RefreshComponentRefreshingBlock) freshBlock
{
    if (self.refreshFooter != NULL) {
        [self.refreshFooter removeFromSuperview];
    }
    
    self.refreshFooter = [LMRefreshFooter footerWithRefreshingBlock:freshBlock];
    
    self.refreshFooter.hidden = true;
}



-(BOOL)isHeaderRefreshing
{
    if (self.refreshHeader != NULL)
    {
        return [self.refreshHeader isRefreshing];
    }
    
    return false;
}


-(BOOL)isFooterRefreshing
{
    if (self.refreshFooter != NULL)
    {
        return [self.refreshFooter isRefreshing];
    }
    
    return false;
}



//start load refresh operation
-(void)beginHeaderRefresh
{
    if ( self.refreshHeader != NULL && !self.isFooterRefreshing)
    {
        if (!self.isHeaderRefreshing) {
            
            [self.refreshHeader beginRefreshing];
        }
    }
}


//end load more operation
-(void)endHeaderRefresh
{
    if ( self.refreshHeader != NULL)
    {
        [self.refreshHeader endRefreshing];
    }
}



///结束“上拉加载”操作
-(void)endFooterRefreshWithNoMoreData{
    [self.refreshFooter endRefreshingWithNoMoreData];
}


//start load more operation
-(void)beginFooterRefresh
{
    if ( self.refreshFooter != NULL && !self.isHeaderRefreshing)
    {
        if (!self.isFooterRefreshing) {
            
            [self.refreshFooter beginRefreshing];
        }
    }
}


//end load more operation
-(void)endFooterRefresh
{
    if ( self.refreshFooter != NULL)
    {
        [self.refreshFooter endRefreshing];
    }
}


- (void)removeHeaderRefresh
{
    if (self.refreshHeader != NULL)
    {
        [self.refreshHeader removeFromSuperview];
        self.refreshHeader = NULL;
    }
    
}


- (void)removeFooterRefresh
{
    if (self.refreshFooter != NULL)
    {
        self.refreshFooter.iheight = 0;
        
        [self.refreshFooter removeFromSuperview];
        
        self.refreshFooter = NULL;
    }
}



- (void)showFooterRefresh
{
    if (self.refreshFooter != NULL)
    {
        [self.refreshFooter resetNoMoreData];
        
        self.refreshFooter.alpha = 1;
    }
}


-(void)closeAllRefresh
{
    if ( self.refreshHeader != NULL)
    {
        [self.refreshHeader endRefreshing];
    }
    
    if ( self.refreshFooter != NULL)
    {
        [self.refreshFooter endRefreshing];
    }
}

@end

@implementation UITableView (LMRefresh)

+ (void)load
{
    [self exchangeInstanceMethod1:@selector(reloadData) method2:@selector(lm_reloadData)];
}

- (void)lm_reloadData
{
    [self lm_reloadData];
    
    [self executeReloadDataBlock];
}


- (void)executeReloadDataBlock
{
    //如果footer不为空，当数据为0时，隐藏footer
    if (self.refreshFooter != NULL) {
        if (self.totalDataCount == 0) {
            self.refreshFooter.hidden = true;
        }else{
            self.refreshFooter.hidden = false;
        }
    }
    
    if (self.reloadDataBlock == NULL){
        NSLog(@"It's null!!!");
    }else{
        self.reloadDataBlock(self.totalDataCount);
    }
}


@end

@implementation UICollectionView (LMRefresh)

+ (void)load
{
    [self exchangeInstanceMethod1:@selector(reloadData) method2:@selector(lm_reloadData)];
}

- (void)lm_reloadData
{
    [self lm_reloadData];
    
    [self executeReloadDataBlock];
}

- (void)executeReloadDataBlock
{
    //如果footer不为空，当数据为0时，隐藏footer
    if (self.refreshFooter != NULL) {
        if (self.totalDataCount == 0) {
            self.refreshFooter.hidden = true;
        }else{
            self.refreshFooter.hidden = false;
        }
    }
    
    if (self.reloadDataBlock == NULL){
        NSLog(@"It's null!!!");
    }else{
        self.reloadDataBlock(self.totalDataCount);
    }
}


@end