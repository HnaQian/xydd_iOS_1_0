//
//  RefreshFooter.m
//  RQRefresh
//
//  Created by Richie on 16/7/11.
//  Copyright © 2016年 Richie. All rights reserved.
//

#import "RefreshFooter.h"

@implementation RefreshFooter

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


+ (instancetype)footerWithRefreshingBlock:(RefreshComponentRefreshingBlock)refreshingBlock
{
    RefreshFooter *cmp = [[self alloc] init];
    cmp.refreshingBlock = refreshingBlock;
    return cmp;
}
+ (instancetype)footerWithRefreshingTarget:(id)target refreshingAction:(SEL)action
{
    RefreshFooter *cmp = [[self alloc] init];
    [cmp setRefreshingTarget:target refreshingAction:action];
    return cmp;
}


- (void)prepare
{
    [super prepare];
    
    // 设置自己的高度
    self.iheight = RefreshFooterHeight;
    
    // 默认是自动隐藏
    self.automaticallyHidden = YES;
}


- (void)willMoveToSuperview:(UIView *)newSuperview
{
    [super willMoveToSuperview:newSuperview];
    
    if (newSuperview) {
        // 监听scrollView数据的变化
        if ([self.scrollView isKindOfClass:[UITableView class]] || [self.scrollView isKindOfClass:[UICollectionView class]]) {
            [self.scrollView setReloadDataBlock:^(NSInteger totalDataCount) {
                //如果user设置了自动隐藏和显示功能，那么在数据为0是显示，如果没有数据不显示
//                if (self.isAutomaticallyHidden) {
//                    self.hidden = (totalDataCount == 0);
//                }
            }];
        }
    }
}


- (void)endRefreshingWithNoMoreData
{
    self.state = RefreshStateNoMoreData;
}

- (void)resetNoMoreData
{
    self.state = RefreshStateIdle;
}



@end
