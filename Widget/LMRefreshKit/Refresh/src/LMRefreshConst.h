//
//  LMRefreshConst.h
//  RQRefresh
//
//  Created by Richie on 16/7/11.
//  Copyright © 2016年 Richie. All rights reserved.
//

/**
 刷新控件的一些常量
 */

#import <Foundation/Foundation.h>
#import <objc/message.h>
#import <UIKit/UIKit.h>


typedef NS_ENUM(NSInteger, UIScrollTipeMode) {
    UIScrollTipeModeNullData = 0,          // null data
    UIScrollTipeModeLostConnect,         // lost network connect
    UIScrollTipeModeNoMuchMore
};



/** 刷新控件的状态 */
typedef NS_ENUM(NSInteger, RefreshState) {
    /** 普通闲置状态 */
    RefreshStateIdle = 1,
    /** 松开就可以进行刷新的状态 */
    RefreshStatePulling,
    /** 正在刷新中的状态 */
    RefreshStateRefreshing,
    /** 即将刷新的状态 */
    RefreshStateWillRefresh,
    /** 所有数据加载完毕，没有更多的数据了 */
    RefreshStateNoMoreData
};


/** 进入刷新状态的回调 */
typedef void (^RefreshComponentRefreshingBlock)();

// 运行时objc_msgSend
#define RefreshMsgSend(...) ((void (*)(void *, SEL, UIView *))objc_msgSend)(__VA_ARGS__)
#define RefreshMsgTarget(target) (__bridge void *)(target)

// RGB颜色
#define RefreshColor(r, g, b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1.0]
// 文字颜色
#define RefreshLabelTextColor RefreshColor(90, 90, 90)
// 字体大小
#define RefreshLabelFont [UIFont boldSystemFontOfSize:14]

// 图片  下拉刷新箭头图片
#define RefreshSrcName @"arrow"


// 图片  没有内容的时候的提示图片：
#define NullDataTipeIconStyle_0 @"iconStyle0"
#define NullDataTipeIconStyle_1 @"iconStyle1"

// 常量
UIKIT_EXTERN const CGFloat RefreshHeaderHeight;
UIKIT_EXTERN const CGFloat RefreshFooterHeight;
UIKIT_EXTERN const CGFloat RefreshFastAnimationDuration;
UIKIT_EXTERN const CGFloat RefreshSlowAnimationDuration;

UIKIT_EXTERN NSString *const RefreshKeyPathContentOffset;
UIKIT_EXTERN NSString *const RefreshKeyPathContentSize;
UIKIT_EXTERN NSString *const RefreshKeyPathContentInset;
UIKIT_EXTERN NSString *const RefreshKeyPathPanState;

UIKIT_EXTERN NSString *const RefreshHeaderLastUpdatedTimeKey;

UIKIT_EXTERN NSString *const RefreshHeaderIdleText;
UIKIT_EXTERN NSString *const RefreshHeaderPullingText;
UIKIT_EXTERN NSString *const RefreshHeaderRefreshingText;

// 底部刷新控件闲置时的提示语(等待刷新)
UIKIT_EXTERN NSString *const RefreshFooterIdleText;
UIKIT_EXTERN NSString *const RefreshFooterRefreshingText;
UIKIT_EXTERN NSString *const RefreshFooterNoMoreDataText;


// tableview上的显示
UIKIT_EXTERN NSString *const RefreshNoDataText;
UIKIT_EXTERN NSString *const RefreshNoNetWorkText;


UIKIT_EXTERN NSString *const RefreshBackFooterIdleText;
UIKIT_EXTERN NSString *const RefreshBackFooterPullingText;
UIKIT_EXTERN NSString *const RefreshBackFooterRefreshingText;
UIKIT_EXTERN NSString *const RefreshBackFooterNoMoreDataText;
