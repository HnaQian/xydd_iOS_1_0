//
//  LMRefreshConst.m
//  RQRefresh
//
//  Created by Richie on 16/7/11.
//  Copyright © 2016年 Richie. All rights reserved.
//

#import "LMRefreshConst.h"

//下拉刷新控件的高度
const CGFloat RefreshHeaderHeight = 54.0;
//底加载控件的高度
const CGFloat RefreshFooterHeight = 44.0;
//正常动画时间
const CGFloat RefreshFastAnimationDuration = 0.25;
//长动画时间
const CGFloat RefreshSlowAnimationDuration = 0.4;

//各种Key
NSString *const RefreshKeyPathContentOffset = @"contentOffset";
NSString *const RefreshKeyPathContentInset = @"contentInset";
NSString *const RefreshKeyPathContentSize = @"contentSize";
NSString *const RefreshKeyPathPanState = @"state";

//获取上传刷新时间的Key
NSString *const RefreshHeaderLastUpdatedTimeKey = @"RefreshHeaderLastUpdatedTimeKey";


//各种提示语
//刷新头部的提示语
NSString *const RefreshHeaderIdleText = @"下拉可以刷新";
NSString *const RefreshHeaderPullingText = @"松开立即刷新";
NSString *const RefreshHeaderRefreshingText = @"正在刷新数据中...";

//刷新尾部的提示语
NSString *const RefreshFooterIdleText = @"点击或上拉加载更多";
NSString *const RefreshFooterRefreshingText = @"正在加载更多的数据...";
NSString *const RefreshFooterNoMoreDataText = @" 已经加载完毕 ";


//无数据时的显示：tableview上的显示
NSString *const RefreshNoDataText = @"没有数据了";
NSString *const RefreshNoNetWorkText = @"没有网络了";


NSString *const RefreshBackFooterIdleText = @"";
NSString *const RefreshBackFooterPullingText = @"";
NSString *const RefreshBackFooterRefreshingText = @"";
NSString *const RefreshBackFooterNoMoreDataText = @"";

