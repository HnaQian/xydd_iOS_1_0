//
//  LMRefreshFooter.m
//  RQRefresh
//
//  Created by Richie on 16/7/11.
//  Copyright © 2016年 Richie. All rights reserved.
//

#import "LMRefreshFooter.h"
@interface LMRefreshFooter()
{
    ///显示刷新状态的label
    __unsafe_unretained UILabel *_mstateLabel;
}

//[状态：文字]
@property (strong, nonatomic) NSMutableDictionary *stateTitles;
@property (strong, nonatomic) UIActivityIndicatorView *loadingView;

@end


@implementation LMRefreshFooter

//刷新状提示
- (NSMutableDictionary *)stateTitles
{
    if (!_stateTitles) {
        self.stateTitles = [NSMutableDictionary dictionary];
    }
    return _stateTitles;
}

//非刷新状态提示
- (UILabel *)stateLabel
{
    if (!_mstateLabel) {
        [self addSubview:_mstateLabel = [UILabel label]];
    }
    return _mstateLabel;
}


//存储各种状态所对应的提示语
- (void)setTitle:(NSString *)title forState:(RefreshState)state
{
    if (title == nil) return;
    self.stateTitles[@(state)] = title;
    self.stateLabel.text = self.stateTitles[@(self.state)];
}


//点击加载提示语的动作
- (void)stateLabelClick
{
    if (self.state == RefreshStateIdle) {
        [self beginRefreshing];
    }
}


- (UIActivityIndicatorView *)loadingView
{
    if (!_loadingView) {
        UIActivityIndicatorView *loadingView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:self.activityIndicatorViewStyle];
        loadingView.hidesWhenStopped = YES;
        [self addSubview:_loadingView = loadingView];
    }
    return _loadingView;
}



//
- (void)willMoveToSuperview:(UIView *)newSuperview
{
    [super willMoveToSuperview:newSuperview];
    
    if (newSuperview) { // 新的父控件
        if (self.hidden == NO) {
            //为刷新控件腾出空间
            self.scrollView.insetB += self.iheight;
        }
        
        // 设置位置
        self.iy = self.scrollView.contentH + (self.scrollView.insetB - self.iheight);
    } else { // 被移除了
        if (self.hidden == NO) {
            //还原父控件的 inset
            self.scrollView.insetB -= self.iheight;
        }
    }
}



- (void)scrollViewContentSizeDidChange:(NSDictionary *)change{
    
    [super scrollViewContentSizeDidChange:change];
    // 设置位置
    self.iy = self.scrollView.contentH + (self.scrollView.insetB - self.iheight);
}

- (void)scrollViewContentOffsetDidChange:(NSDictionary *)change
{
    [super scrollViewContentOffsetDidChange:change];
    
    if (self.state != RefreshStateIdle || !self.automaticallyRefresh || self.iy == 0) return;
    
    // 当底部刷新控件完全出现时，才刷新
    if (self.scrollView.insetT + self.scrollView.contentH >= self.scrollView.iheight) { // 内容超过一个屏幕
        if (self.scrollView.offsetY >= self.scrollView.contentH - self.scrollView.iheight + self.iheight * self.triggerAutomaticallyRefreshPercent + self.scrollView.insetB - self.iheight - 2) {
            // 防止手松开时连续调用
            CGPoint old = [change[@"old"] CGPointValue];
            CGPoint new = [change[@"new"] CGPointValue];
            if (new.y <= old.y) return;
            
            [self beginRefreshing];
        }
    }
}

- (void)scrollViewPanStateDidChange:(NSDictionary *)change
{
    [super scrollViewPanStateDidChange:change];
    
    if (self.state != RefreshStateIdle) return;
    
    if (self.scrollView.panGestureRecognizer.state == UIGestureRecognizerStateEnded) {// 手松开
        if (self.scrollView.insetT + self.scrollView.contentH <= self.scrollView.iheight) {  // 不够一个屏幕
            if (self.scrollView.offsetY >= - self.scrollView.insetT) { // 向上拽
                [self beginRefreshing];
            }
        } else { // 超出一个屏幕
            if (self.scrollView.offsetY >= self.scrollView.contentH + self.scrollView.insetB - self.scrollView.iheight) {
                [self beginRefreshing];
            }
        }
    }
}


- (void)setHidden:(BOOL)hidden
{
    BOOL lastHidden = self.isHidden;
    
    [super setHidden:hidden];
    
    if (!lastHidden && hidden) {
        self.state = RefreshStateIdle;
        
        self.scrollView.insetB -= self.iheight;
    } else if (lastHidden && !hidden) {
        self.scrollView.insetB += self.iheight;
        
        // 设置位置
        self.iy = self.scrollView.contentH + (self.scrollView.insetB - self.iheight);
    }
}


- (void)setActivityIndicatorViewStyle:(UIActivityIndicatorViewStyle)activityIndicatorViewStyle
{
    _activityIndicatorViewStyle = activityIndicatorViewStyle;
    
    self.loadingView = nil;
    [self setNeedsLayout];
}


- (void)prepare
{
    [super prepare];

    // 默认底部控件100%出现时才会自动刷新
    self.triggerAutomaticallyRefreshPercent = 1.0;
    
    // 设置为默认状态
    self.automaticallyRefresh = YES;

    
    // 初始化文字
    [self setTitle:RefreshFooterIdleText forState:RefreshStateIdle];
    [self setTitle:RefreshFooterRefreshingText forState:RefreshStateRefreshing];
    [self setTitle:RefreshFooterNoMoreDataText forState:RefreshStateNoMoreData];
    
    // 监听label
    self.stateLabel.userInteractionEnabled = YES;
    [self.stateLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(stateLabelClick)]];
    
    self.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
}



- (void)placeSubviews
{
    [super placeSubviews];
    
    if (self.stateLabel.constraints.count) return;
    
    // 状态标签
    self.stateLabel.frame = self.bounds;
    
    if (self.loadingView.constraints.count) return;
    
    // 圈圈
    CGFloat loadingCenterX = self.iwidth * 0.5;
    if (!self.isRefreshingTitleHidden) {
        loadingCenterX -= 100;
    }
    CGFloat loadingCenterY = self.iheight * 0.5;
    self.loadingView.center = CGPointMake(loadingCenterX, loadingCenterY);
}

- (void)setState:(RefreshState)state
{
    RefreshState oldState = self.state;
    if (state == oldState) return;
    [super setState:state];
    
    if (state == RefreshStateRefreshing) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self executeRefreshingCallback];
        });
    }
    
    
    // 根据状态做事情
    if (state == RefreshStateNoMoreData || state == RefreshStateIdle) {
        [self.loadingView stopAnimating];
    } else if (state == RefreshStateRefreshing) {
        [self.loadingView startAnimating];
    }
    
    self.stateLabel.text = self.stateTitles[@(self.state)];
}


@end
