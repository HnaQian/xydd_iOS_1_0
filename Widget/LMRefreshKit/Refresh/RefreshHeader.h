//
//  RefreshHeader.h
//  RQRefresh
//
//  Created by Richie on 16/7/11.
//  Copyright © 2016年 Richie. All rights reserved.
//

/**
 头部刷新的基类
 */
#import "BaseRefreshView.h"

@interface RefreshHeader : BaseRefreshView

/** 创建header */
+ (instancetype)headerWithRefreshingBlock:(RefreshComponentRefreshingBlock)refreshingBlock;
/** 创建header */
+ (instancetype)headerWithRefreshingTarget:(id)target refreshingAction:(SEL)action;

/** 这个key用来存储上一次下拉刷新成功的时间 */
@property (copy, nonatomic) NSString *lastUpdatedTimeKey;
/** 上一次下拉刷新成功的时间 */
@property (strong, nonatomic, readonly) NSDate *lastUpdatedTime;

/** 忽略多少scrollView的contentInset的top */
@property (assign, nonatomic) CGFloat ignoredScrollViewContentInsetTop;

@end
