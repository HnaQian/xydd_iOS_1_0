//
//  UIScrollView+LMRefresh.h
//  RQRefresh
//
//  Created by Richie on 16/7/11.
//  Copyright © 2016年 Richie. All rights reserved.
//

/**
 扩展
 */

#import <UIKit/UIKit.h>
#import "TipesManager.h"
#import "LMRefreshConst.h"

@class RefreshHeader, RefreshFooter;

/** 进入刷新状态的回调 */
typedef void (^RefreshBlock)();

@interface UIScrollView (LMRefresh)

#pragma mark - other
- (NSInteger)totalDataCount;


@property (assign, nonatomic) CGFloat insetT;
@property (assign, nonatomic) CGFloat insetB;
@property (assign, nonatomic) CGFloat insetL;
@property (assign, nonatomic) CGFloat insetR;

@property (assign, nonatomic) CGFloat offsetX;
@property (assign, nonatomic) CGFloat offsetY;

@property (assign, nonatomic) CGFloat contentW;
@property (assign, nonatomic) CGFloat contentH;

@property (nonatomic,assign,readonly)TipesManager *manager;



#pragma mark- 外部访问属性和函数 --------->
/** 下拉刷新控件 */
@property (strong, nonatomic) RefreshHeader *refreshHeader;

/** 上拉刷新控件 */
@property (strong, nonatomic) RefreshFooter *refreshFooter;

@property (nonatomic,readonly)BOOL isHeaderRefreshing;

@property (nonatomic,readonly)BOOL isFooterRefreshing;

/**监听tableView的刷新动作，totalDataCount是tableview此次刷新之后的数据总量*/
@property (copy, nonatomic) void (^reloadDataBlock)(NSInteger totalDataCount);
//
/////* set tipeView's tipe content,tipe icon for specified tipe mode */
-(void)setTipeImage:(NSString *)tipeImage andTipeContent:(NSString *)tipeContent andForTipeType:(UIScrollTipeMode)tipeMode;

///显示提示语
/**
 *tipeMode      :选择显示的提示模式
 */
///注：当 tipeMode = UIScrollTipeMode.Custom 时 tipeContent和tipeIcon都需要设置
-(TipesManager *)showTipe:(UIScrollTipeMode)tipeMode;

///隐藏提示语
/**
 *tipeMode      :选择隐藏的提示模式
 */
-(TipesManager *)closeTipe;


///添加heander 下拉刷新功能
/**
 *freshBlock     :刷新动作回调
 */

-(void)addHeaderRefresh:(RefreshComponentRefreshingBlock) freshBlock;


///添加footer 上拉加载功能
/**
 *freshBlock     :加载动作回调
 */
-(void)addFooterRefresh:(RefreshComponentRefreshingBlock) freshBlock;

///启动“下拉刷新”操作
-(void)beginHeaderRefresh;

///启动“上拉加载更多”操作
-(void)beginFooterRefresh;

///结束“下拉刷新”操作
-(void)endHeaderRefresh;

///结束“上拉加载”操作
-(void)endFooterRefresh;

///结束“上拉加载”操作
-(void)endFooterRefreshWithNoMoreData;

///结束所有刷新动作
-(void)closeAllRefresh;

///移除“下拉刷新”
- (void)removeHeaderRefresh;

///移除“上拉加载”
- (void)removeFooterRefresh;

- (void)executeReloadDataBlock;

@end
