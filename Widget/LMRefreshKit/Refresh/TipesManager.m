//
//  TipesManager.m
//  Limi
//
//  Created by guo chen on 16/1/6.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

#import "TipesManager.h"
#import "UIView+RQLayout.h"
#import "LMRefreshFooter.h"
#import "LMRefreshHeader.h"



#define TIPE_HEIGHT ([UIScreen mainScreen].bounds.size.height * 0.3)

#define TIPE_WIDTH ([UIScreen mainScreen].bounds.size.width)

//@class LMTipeView;



@interface TipesManager()
{
    LMTipeView *_lmTipeView;
}


///已经加载完所有数据的tipe  tag:9110
@property (nonatomic,retain)UIView *nomuchMoreDataTipeView;

//tipView
@property (nonatomic,readonly)LMTipeView *tipeView;

@property (nonatomic,strong)UIScrollView *scrollView;

@end


@implementation TipesManager


#pragma mark- get tipeView

+(TipesManager *)newManager:(UIScrollView *)scrollView{
    
    TipesManager *manager = [TipesManager new];
    manager.scrollView = scrollView;
    return manager;
}


- (LMTipeView *)tipeView{
    if (_lmTipeView == NULL) {
        _lmTipeView = [[LMTipeView alloc] initWithFrame:CGRectMake(0, 0, self.scrollView.iwidth, self.scrollView.iheight)];
    }
    return _lmTipeView;
}


#pragma mark - 关闭提示内容根据：UIScrollTipeMode
-(TipesManager *)closeTipe{
    
    /**如果当前你没有显示tipeView，不做任何操作*/
    //    if (self.tipeView.superview == NULL) {return self;}
    if (![self.scrollView isKindOfClass:[UITableView class]] && ![self.scrollView isKindOfClass:[UICollectionView class]]) {
        return self;
    }
    
    [UIView animateWithDuration:0.25 animations:^{
        self.tipeView.alpha = 0;
    } completion:^(BOOL finished) {
        
        self.tipeView.alpha = 1;
        self.tipeView.tipes = @"";
        self.tipeView.iconName = NullDataTipeIconStyle_0;
        
        if ([self.scrollView isKindOfClass:[UITableView class]]) {
            ((UITableView *)self.scrollView).backgroundView = NULL;
        }else{
            ((UICollectionView *)self.scrollView).backgroundView = NULL;
        }
    }];
    
    return self;
}



#pragma mark - 显示提示内容根据：UIScrollTipeMode
-(TipesManager *)showTipeWithTipeMode:(UIScrollTipeMode)currentMode{
    
    switch (currentMode) {
            
        case UIScrollTipeModeNullData:
            
            self.tipeView.iconName = NullDataTipeIconStyle_0;
            self.tipeView.tipes = RefreshNoDataText;
            break;
        case UIScrollTipeModeLostConnect:
            
            self.tipeView.iconName = NullDataTipeIconStyle_1;
            self.tipeView.tipes = RefreshNoDataText;
            break;
        case  UIScrollTipeModeNoMuchMore:
            [self.scrollView endFooterRefreshWithNoMoreData];
            break;
        default:
            break;
    }
    
    if (self.scrollView != NULL) {
        
        if ([self.scrollView isKindOfClass:[UITableView class]]) {
            
            ((UITableView *)(self.scrollView)).backgroundView = self.tipeView;
            
        }else if ([self.scrollView isKindOfClass:[UICollectionView class]]) {
            ((UICollectionView *)(self.scrollView)).backgroundView = self.tipeView;
        }
    }
    
    return self;
}



#pragma mark - 设置tipe的提示文字
-(TipesManager *)tipeContent:(NSString *)content{
    self.tipeView.tipes = content;
    return self;
}





#pragma mark - 设置tipe的提示图标
-(TipesManager *)tipeIcon:(NSString *)iconName{
    
    self.tipeView.iconName = iconName;
    return self;
}

@end






#pragma mark - LMTipeView
@implementation LMTipeView


-(void)setIconName:(NSString *)iconName{
    _iconName = iconName;
    if (iconName != NULL) {
        if (iconName.length > 0) {
            self.iconImg.image = [UIImage imageNamed:iconName];
        }
    }
}


- (void)setTipes:(NSString *)tipes{
    _tipes = tipes;
    self.tipesLbl.text = tipes;
}


- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupUI];
    }
    return self;
}

/**
 
 
 */

- (void)setupUI{
    
    /**提示头像*/
    self.iconImg = [UIImageView new];
    
    /**提示文字*/
    self.tipesLbl = [UILabel new];
    self.tipesLbl.textAlignment = NSTextAlignmentCenter;
    
    [self addSubview:self.iconImg];
    [self addSubview:self.tipesLbl];
    
    //icon距离tableView顶部的距离
    CGFloat topGap = 300 * [[UIScreen mainScreen] bounds].size.width/750;
    
    
    /**布局默认的：icon横向居中显示,tipelabel显示在icon的正下方*/
    
    //---使用frame---
    self.iconImg.frame = CGRectMake(0, 0, 40, 40);
    self.tipesLbl.frame = CGRectMake(0, 0, self.iwidth - 16, 30);
    
    self.iconImg.center = CGPointMake(self.iwidth/2, topGap + self.iconImg.iheight/2);
    self.tipesLbl.center = CGPointMake(self.iwidth/2, self.iconImg.ibottom + self.tipesLbl.iheight/2);
    
    
    //---使用Auto Layout约束---
    [self.iconImg setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.tipesLbl setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    //Icon
    //距离顶部距离
    NSLayoutConstraint *contraint1 = [NSLayoutConstraint constraintWithItem:self.iconImg attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1.0 constant:topGap];
    
    //横向居中
    NSLayoutConstraint *contraint12 = [NSLayoutConstraint constraintWithItem:self.iconImg attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0];
    
    NSLayoutConstraint *contraint13 = [NSLayoutConstraint constraintWithItem:self.iconImg attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:60];
    
    NSLayoutConstraint *contraint14 = [NSLayoutConstraint constraintWithItem:self.iconImg attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:60];
    
    
    //Label 的约束
    NSLayoutConstraint *contraint21 = [NSLayoutConstraint constraintWithItem:self.tipesLbl attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1.0 constant:8];
    
    NSLayoutConstraint *contraint22 = [NSLayoutConstraint constraintWithItem:self.tipesLbl attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1.0 constant:-8];
    
    NSLayoutConstraint *contraint23 = [NSLayoutConstraint constraintWithItem:self.tipesLbl attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:30];
    
    NSLayoutConstraint *contraint24 = [NSLayoutConstraint constraintWithItem:self.tipesLbl attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.iconImg attribute:NSLayoutAttributeBottom multiplier:1.0 constant:8];
    
    
    //把约束添加到父视图上
    NSArray *array = [NSArray arrayWithObjects:contraint1, contraint12,contraint13,contraint14, contraint21, contraint22, contraint23, contraint24, NULL];
    
    [self addConstraints:array];
}

@end




