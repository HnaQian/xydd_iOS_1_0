//
//  UIView+RQLayout.m
//  RQcatorgy
//
//  Created by Get-CC on 15/8/7.
//  Copyright (c) 2015年 GET-CC. All rights reserved.
//

#import "UIView+RQLayout.h"

@implementation UIView (RQLayout)

- (CGFloat)iwidth
{
    return self.frame.size.width;
}

- (CGFloat)iheight
{
    return self.frame.size.height;
}

- (CGFloat)ix
{
    return self.frame.origin.x;
}

- (CGFloat)iy
{
    return self.frame.origin.y;
}

/*
 @property (nonatomic,assign)CGFloat icenterX;
 
 @property (nonatomic,assign)CGFloat icenterY;
 */

- (CGFloat)icenterX
{
    return self.center.x;
}

- (CGFloat)icenterY
{
    return self.center.y;
}



- (void )setIwidth:(CGFloat)iwidth
{
    self.frame = CGRectMake(self.ix, self.iy, iwidth, self.iheight);
}

- (void)setIheight:(CGFloat)iheight
{
    self.frame = CGRectMake(self.ix, self.iy, self.iwidth, iheight);
}

- (void)setIx:(CGFloat)ix
{
    self.frame = CGRectMake(ix, self.iy, self.iwidth, self.iheight);
}

- (void)setIy:(CGFloat)iy
{
    self.frame = CGRectMake(self.ix, iy, self.iwidth, self.iheight);
}


- (void)setIcenterX:(CGFloat)icenterX
{
    self.frame = CGRectMake(icenterX - self.iwidth/2, self.iy, self.iwidth, self.iheight);
}


-(void)setIcenterY:(CGFloat)icenterY
{
    self.frame = CGRectMake(self.ix, icenterY - self.iheight/2, self.iwidth, self.iheight);
}



-(CGFloat)itop
{
    return self.frame.origin.y;
}

-(CGFloat)ibottom
{
    return  self.frame.origin.y + self.frame.size.height;
}

-(CGFloat)ileft
{
    return  self.frame.origin.x;
}


-(CGFloat)iright
{
    return  self.frame.origin.x + self.frame.size.width;
}



- (void)setItop:(CGFloat)itop
{
    self.frame = CGRectMake(self.ix, itop, self.iwidth, self.iheight);
}

- (void)setIbottom:(CGFloat)ibottom
{
    self.frame = CGRectMake(self.ix, ibottom - self.iheight, self.iwidth, self.iheight);
}

- (void)setIleft:(CGFloat)ileft
{
    self.frame = CGRectMake(ileft, self.iy, self.iwidth, self.iheight);
}

- (void)setIright:(CGFloat)iright
{
    self.frame = CGRectMake(iright - self.iwidth, self.iy, self.iwidth, self.iheight);
}



-(UIView *)itoLeftOf:(UIView *)view
{
    self.frame = CGRectMake(view.ileft - self.iwidth, self.iy, self.iwidth, self.iheight);
    
    return self;
}


-(UIView *)itoRightOf:(UIView *)view
{
    self.frame = CGRectMake(view.iright, self.iy, self.iwidth, self.iheight);
    
    return self;
}


-(UIView *)ibelow:(UIView *)view
{
    self.frame = CGRectMake(self.ix, view.ibottom, self.iwidth, self.iheight);
    
    return self;
}


-(UIView *)iabove:(UIView *)view
{
    self.frame = CGRectMake(self.ix, view.iy - self.iheight, self.iwidth, self.iheight);
    
    return self;
}


- (CGSize)isize{return self.frame.size;}

- (void)setIsize:(CGSize)isize{
    CGRect frame = self.frame;
    frame.size = isize;
    self.frame = frame;

}

-(void)layoutSubviews
{
    
    
}

@end
