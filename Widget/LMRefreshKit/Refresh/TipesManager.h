//
//  TipesManager.h
//  Limi
//
//  Created by guo chen on 16/1/6.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "LMRefreshConst.h"


//[UIScreen mainScreen].bounds.size.width
@interface TipesManager : NSObject

+(TipesManager *)newManager:(UIScrollView *)scrollView;

///根据传进来的tipeMode显示相应的模式
///注：当模式为：Custom ，需要设置提示的内容和图标，默认两者都是为nil
-(TipesManager *)showTipeWithTipeMode:(UIScrollTipeMode)currentMode;

-(TipesManager *)closeTipe;

///设置提示文字
-(TipesManager *)tipeContent:(NSString *)content;

///设置提示图标
-(TipesManager *)tipeIcon:(NSString *)iconName;


@end



//提示视图
@interface LMTipeView : UIView

//图标
@property (nonatomic,strong)NSString *iconName;
//提示语
@property (nonatomic,strong)NSString *tipes;

//图标
@property (nonatomic,retain)UIImageView *iconImg;
//提示语
@property (nonatomic,retain)UILabel *tipesLbl;

@end
