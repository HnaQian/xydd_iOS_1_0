//
//  LMRefreshBackStateFooter.h
//  Limi
//
//  Created by Richie on 16/7/13.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

#import "RefreshFooter.h"

@interface LMRefreshBackStateFooter : RefreshFooter

/** 显示刷新状态的label */
@property (strong, nonatomic, readonly) UILabel *stateLabel;

/** 设置state状态下的文字 */
- (void)setTitle:(NSString *)title forState:(RefreshState)state;

/** 获取state状态下的title */
- (NSString *)titleForState:(RefreshState)state;





@property (strong, nonatomic, readonly) UIImageView *gifView;

/** 设置state状态下的动画图片images 动画持续时间duration*/
- (void)setImages:(NSArray *)images duration:(NSTimeInterval)duration forState:(RefreshState)state;
- (void)setImages:(NSArray *)images forState:(RefreshState)state;


@end
