//
//  LMRefresh.h
//  RQRefresh
//
//  Created by Richie on 16/7/11.
//  Copyright © 2016年 Richie. All rights reserved.
//

#ifndef LMRefresh_h
#define LMRefresh_h

#import "LMRefreshConst.h"
#import "LMRefreshFooter.h"
#import "LMRefreshHeader.h"
#import "UIScrollView+LMRefresh.h"

#import "LMRefreshBackStateFooter.h"
#import "LMRefreshStateHeader.h"

#endif /* LMRefresh_h */



#if NO

#pragma mark- 供 UITableView 使用
///一下函数的位置在  /Refresh/UIScrollView+LMRefresh.h

/** 下拉刷新控件 */
@property (strong, nonatomic) LMRefreshHeader *refreshHeader;
/** 上拉刷新控件 */
@property (strong, nonatomic) LMRefreshFooter *refreshFooter;
/**是否正在下拉刷新*/
@property (nonatomic,readonly)BOOL isHeaderRefreshing;
/**是否正在上拉加载*/
@property (nonatomic,readonly)BOOL isFooterRefreshing;


/**
 监听tableView的刷新动作，totalDataCount是tableview此次刷新之后的数据总量
 
 当数据为 == 0 时：
 建议设置refreshFooter.hidden = true/YES，保证此时不会出发“上拉加载动作”,
 如果有必要，也可以让tableView显示无数据的提示语，请调用 [showTipe:]
 
 当数据 >0 时：
 建议设置refreshFooter.hidden = false/NO，保证此时“上拉加载”功能是开启的,
 关闭tableView显示无数据的提示语，请调用 [closeTipe]
 */
@property (copy, nonatomic) void (^reloadDataBlock)(NSInteger totalDataCount);


/////* set tipeView's tipe content,tipe icon for specified tipe mode */
-(void)setTipeImage:(NSString *)tipeImage andTipeContent:(NSString *)tipeContent andForTipeType:(UIScrollTipeMode)tipeMode;


///显示提示语
/**
 *tipeMode      :选择显示的提示模式
 */
-(TipesManager *)showTipe:(UIScrollTipeMode)tipeMode;

/**隐藏提示语*/
-(TipesManager *)closeTipe;


///添加heander 下拉刷新功能
/**
 *freshBlock     :刷新动作回调
 */

-(void)addHeaderRefresh:(RefreshComponentRefreshingBlock) freshBlock;


///添加footer 上拉加载功能
/**
 *freshBlock     :加载动作回调
 */
-(void)addFooterRefresh:(RefreshComponentRefreshingBlock) freshBlock;

///启动“下拉刷新”操作
-(void)beginHeaderRefresh;

///启动“上拉加载更多”操作
-(void)beginFooterRefresh;

///结束“下拉刷新”操作
-(void)endHeaderRefresh;

///结束“上拉加载”操作
-(void)endFooterRefresh;

///结束“上拉加载”操作
-(void)endFooterRefreshWithNoMoreData;

///结束所有刷新动作
-(void)closeAllRefresh;

///移除“下拉刷新”
- (void)removeHeaderRefresh;

///移除“上拉加载”
- (void)removeFooterRefresh;

#endif