//
//  HyPopMenuView.m
//  HyPopMenuView
//
//  Created by  H y on 15/9/8.
//  Copyright (c) 2015年 ouy.Aberi. All rights reserved.
//

#import "MenuLabel.h"
#import <pop/POP.h>
#import "HyPopMenuView.h"
#import "UIColor+ImageGetColor.h"
#import <objc/runtime.h>

static const char kAssocKey_Window;
#define Duration 0.2
#define KeyPath @"transform.scale"
#define CancelStrImgaeName @"tabbar_compose_background_icon_add"
#define kTitleRatio 0.4
#define kW [UIScreen mainScreen].bounds.size.width
#define kH [UIScreen mainScreen].bounds.size.height
#define HowMucHline 4
#define ButtonWidth kW/HowMucHline
#define ButtonHigh ButtonWidth
#define ButtonTag 200
#define Interval (0.195 / _ItmesArr.count)

@interface HyPopMenuView ()
{
    UIWindow *window;
}
@property (nonatomic,retain) UIView *BlurView;
@property (nonatomic,retain) NSArray *ItmesArr;
@property (nonatomic,strong) SelectdCompletionBlock block;
@property (nonatomic,assign) BOOL is;
@end

@implementation HyPopMenuView

+(void)CreatingPopMenuObjectItmes:(NSArray<MenuLabel *> *)Items
                        isGeneral:(BOOL)isGeneral
                          TopView:(UIView *)topView
       OpenOrCloseAudioDictionary:(NSDictionary *)openOrCloseAudioDictionary
           SelectdCompletionBlock:(SelectdCompletionBlock)block{

    HyPopMenuView *menu = [[HyPopMenuView alloc] initWithItmes:Items isGeneral:isGeneral];
    [menu SelectdCompletionBlock:block];
}

+(void)CreatingPopMenuObjectItmes:(NSArray<MenuLabel *> *)Items
                        isGeneral:(BOOL)isGeneral
                          TopView:(UIView *)topView
           SelectdCompletionBlock:(SelectdCompletionBlock)block{

    HyPopMenuView *menu = [[HyPopMenuView alloc] initWithItmes:Items isGeneral:isGeneral];
    [menu SelectdCompletionBlock:block];
}

+(void)CreatingPopMenuObjectItmes:(NSArray<MenuLabel *> *)Items
                        isGeneral:(BOOL)isGeneral
           SelectdCompletionBlock:(SelectdCompletionBlock)block{
    HyPopMenuView *menu = [[HyPopMenuView alloc] initWithItmes:Items isGeneral:isGeneral];
    [menu SelectdCompletionBlock:block];
}

-(instancetype) initWithItmes:(NSArray<MenuLabel *> *)Itmes isGeneral:(BOOL)isGeneral
{
    self = [super init];
    if (self) {
        _is = true;
        _ItmesArr = [Itmes copy];
        [self setFrame:CGRectMake(0, 0, kW, kH)];
        if (isGeneral) {
            [self initUI];
        }else {
            [self specialCirculatingItmes];
        }
        
        [self show];
        
    }
    return self;
}

-(void)initUI
{
    _springBounce = 10;
    _springspeed = 20;
    UIView *BlurView = [[UIView alloc] initWithFrame:self.bounds];
    UIImageView *blurImage = [[UIImageView alloc] initWithFrame:BlurView.frame];
    blurImage.image = [UIImage imageNamed:@"blurBg"];
    [BlurView addSubview:blurImage];
    
    BlurView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleBottomMargin;
    _BlurView = BlurView;
    [self addSubview:_BlurView];
    self.alpha = 0.0f;
    
    [self CirculatingItmes];
}

-(void)CirculatingItmes
{
    typeof(self) weak = self;
    [UIView animateWithDuration:Duration animations:^{
        [weak setAlpha:1];
    }];
    NSInteger index = 0;
    for (MenuLabel *Obj in _ItmesArr) {
        CGFloat buttonX,buttonY;
        buttonX = (index % HowMucHline) * ButtonWidth;
        
        buttonY = [UIScreen mainScreen].bounds.size.height - 30 - ButtonHigh;
        CGRect fromValue = CGRectMake(buttonX, [UIScreen mainScreen].bounds.size.height, ButtonWidth, ButtonHigh);
        CGRect toValue = CGRectMake(buttonX, buttonY, ButtonWidth, ButtonHigh);
        CustomButton *button = [self AllockButtonIndex:index];
        button.MenuData = Obj;
        [button setFrame:fromValue];
        double delayInSeconds = index * Interval;
        CFTimeInterval delay = delayInSeconds + CACurrentMediaTime();
        
        [self StartTheAnimationFromValue:fromValue ToValue:toValue Delay:delay Object:button CompletionBlock:^(BOOL CompletionBlock) {
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                        action:@selector(dismiss)];
            [_BlurView addGestureRecognizer:tap];
        } HideDisplay:false];
        index ++;
    }
}

-(void)specialCirculatingItmes
{
    _springBounce = 10;
    _springspeed = 10;
    UIView *BlurView = [[UIView alloc] initWithFrame:self.bounds];
    UIImageView *blurImage = [[UIImageView alloc] initWithFrame:BlurView.frame];
    blurImage.image = [UIImage imageNamed:@"blurBg_snapShot"];
    [BlurView addSubview:blurImage];
    _BlurView = BlurView;
    [self addSubview:_BlurView];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismiss)];
    [_BlurView addGestureRecognizer:tap];
    
    NSInteger index = 0;
    CGFloat space = 240 * kW / 750;
    CGFloat buttonWidth = 160 * kW / 750;
    CGFloat buttonHeight = 130 * kW /750 + 15;
    NSInteger arrayCount = _ItmesArr.count;
    for (MenuLabel *Obj in _ItmesArr) {
        CGFloat buttonX,buttonY;
        buttonX = (index % 3) * space + 55 * kW/750;
        
        buttonY = [UIScreen mainScreen].bounds.size.height - (60*kW/750+buttonHeight)*((arrayCount -1)/3 - index/3 +1);
        CGRect fromValue = CGRectMake(buttonX, [UIScreen mainScreen].bounds.size.height, buttonWidth, buttonHeight);
        CGRect toValue = CGRectMake(buttonX, buttonY, buttonWidth, buttonHeight);
        CustomButton *button = [self AllockButtonIndex:index];
        [button refreshMenuData:Obj contentSize:CGSizeMake(buttonWidth, buttonHeight)];
        [button setFrame:fromValue];
        double delayInSeconds = index * Interval;
        CFTimeInterval delay = delayInSeconds + CACurrentMediaTime();
        
        [self StartTheAnimationFromValue:fromValue ToValue:toValue Delay:delay Object:button CompletionBlock:^(BOOL CompletionBlock) {
           
        } HideDisplay:false];
        index ++;
    }
}

-(CustomButton *)AllockButtonIndex:(NSInteger)index
{
    CustomButton *button = [[CustomButton alloc] init];
    [button setTag:(index + 1) + ButtonTag];
    [button setAlpha:0.0f];
    [button setTitleColor:[UIColor colorWithWhite:0.38 alpha:1] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(selectd:) forControlEvents:UIControlEventTouchUpInside];
    [_BlurView addSubview:button];
    
    return button;
}

-(void)StartTheAnimationFromValue:(CGRect)fromValue
                          ToValue:(CGRect)toValue
                            Delay:(CFTimeInterval)delay
                           Object:(id/*<UIView *>*/)obj
                  CompletionBlock:(void(^) (BOOL CompletionBlock))completionBlock HideDisplay:(BOOL)HideDisplay{
    
    POPSpringAnimation *springAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewFrame];
    springAnimation.removedOnCompletion = YES;
    springAnimation.beginTime = delay;
    CGFloat springBounciness = _springBounce;
    springAnimation.springBounciness = springBounciness;    // value between 0-20
   
    CGFloat springSpeed = _springspeed;
    springAnimation.springSpeed = springSpeed;     // value between 0-20
    springAnimation.toValue = [NSValue valueWithCGRect:toValue];
    springAnimation.fromValue = [NSValue valueWithCGRect:fromValue];
    
    POPSpringAnimation *SpringAnimationAlpha = [POPSpringAnimation animationWithPropertyNamed:kPOPViewAlpha];
    SpringAnimationAlpha.removedOnCompletion = YES;
    SpringAnimationAlpha.beginTime = delay;
    SpringAnimationAlpha.springBounciness = springBounciness;    // value between 0-20
    
    CGFloat toV,fromV;
    if (HideDisplay) {
        fromV = 1.0f;
        toV = 0.0f;
    }else{
        fromV = 0.0f;
        toV = 1.0f;
    }
    
    SpringAnimationAlpha.springSpeed = springSpeed;     // value between 0-20
    SpringAnimationAlpha.toValue = @(toV);
    SpringAnimationAlpha.fromValue = @(fromV);
    
    [obj pop_addAnimation:SpringAnimationAlpha forKey:SpringAnimationAlpha.name];
    [obj pop_addAnimation:springAnimation forKey:springAnimation.name];
    [springAnimation setCompletionBlock:^(POPAnimation *spring, BOOL Completion) {
        if (!completionBlock) {
            return ;
        }
        completionBlock(Completion);
    }];
    
}

-(void)DismissCompletionBlock:(void(^) (BOOL CompletionBlock)) completionBlock{

    NSInteger index = 0;

    for (MenuLabel *label in _ItmesArr) {
        CustomButton *button = (CustomButton *)[self viewWithTag:(index + 1) + ButtonTag];
        button.MenuData = label;
        CGFloat buttonX,buttonY;
        buttonX = (index % HowMucHline) * ButtonWidth;
        buttonY = [UIScreen mainScreen].bounds.size.height - 30 - ButtonHigh;
        
        CGRect toValue = CGRectMake(buttonX, kH, ButtonWidth, ButtonHigh);
        CGRect fromValue = CGRectMake(buttonX, buttonY, ButtonWidth, ButtonHigh);
        double delayInSeconds = (_ItmesArr.count - index) * Interval;
        CFTimeInterval delay = delayInSeconds + CACurrentMediaTime();
        
        [self StartTheAnimationFromValue:fromValue ToValue:toValue Delay:delay Object:button CompletionBlock:^(BOOL CompletionBlock) {
        } HideDisplay:true];
        index ++;
    }
    window = objc_getAssociatedObject([UIApplication sharedApplication], &kAssocKey_Window);
    
    [UIView transitionWithView:window
                      duration:.3
                       options:UIViewAnimationOptionTransitionCrossDissolve|UIViewAnimationOptionCurveEaseInOut
                    animations:^{
                        UIView *view = window.rootViewController.view;
                        
                        for (UIView *v in view.subviews) {
                            v.transform = CGAffineTransformMakeScale(.8, .8);
                        }
                        
                        window.alpha = 0;
                    }
                    completion:^(BOOL finished) {
                        
                        [window.rootViewController.view removeFromSuperview];
                        window.rootViewController = nil;
                        
                        // 上乗せしたウィンドウを破棄
                        objc_setAssociatedObject([UIApplication sharedApplication], &kAssocKey_Window, nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
                        
                        // メインウィンドウをキーウィンドウにする
                        UIWindow *nextWindow = [[UIApplication sharedApplication].delegate window];
                        [nextWindow makeKeyAndVisible];
                    }];

}


-(void)selectd:(CustomButton *)button
{
    NSInteger tag = button.tag - (ButtonTag + 1);
    typeof(self) weak = self;
    for (MenuLabel *label in _ItmesArr) {
        NSInteger index = [_ItmesArr indexOfObject:label];
        CustomButton *buttons = (CustomButton *)[self viewWithTag:(index + 1) + ButtonTag];
        if (index == tag) {
            [button SelectdAnimation];
        }else{
            [buttons CancelAnimation];
        }
    }
    
    [self HidDelay:0.3f CompletionBlock:^(BOOL completion) {
        if (!weak.block) {
            return ;
        }
        weak.block(tag);
    }];
    
    window = objc_getAssociatedObject([UIApplication sharedApplication], &kAssocKey_Window);
    
    [UIView transitionWithView:window
                      duration:.3
                       options:UIViewAnimationOptionTransitionCrossDissolve|UIViewAnimationOptionCurveEaseInOut
                    animations:^{
                        UIView *view = window.rootViewController.view;
                        
                        for (UIView *v in view.subviews) {
                            v.transform = CGAffineTransformMakeScale(.8, .8);
                        }
                        
                        window.alpha = 0;
                    }
                    completion:^(BOOL finished) {
                        
                        [window.rootViewController.view removeFromSuperview];
                        window.rootViewController = nil;
                        
                        // 上乗せしたウィンドウを破棄
                        objc_setAssociatedObject([UIApplication sharedApplication], &kAssocKey_Window, nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
                        
                        // メインウィンドウをキーウィンドウにする
                        UIWindow *nextWindow = [[UIApplication sharedApplication].delegate window];
                        [nextWindow makeKeyAndVisible];
                    }];
}

-(void)HidDelay:(NSTimeInterval)delay
CompletionBlock:(void(^)(BOOL completion))blcok
{
    [self setUserInteractionEnabled:false];
    typeof(self) weak = self;
    [UIView animateKeyframesWithDuration:Duration delay:delay options:UIViewKeyframeAnimationOptionLayoutSubviews animations:^{
        [weak setAlpha:0.0f];
        
    } completion:^(BOOL finished) {
        [weak removeFromSuperview];
        if (!blcok) {
            return ;
        }
        blcok(finished);
    }];
    
    
}

-(void)SelectdCompletionBlock:(SelectdCompletionBlock) block{

    _block = [block copy];
}

-(void)dismiss{
    
    UIView *button = [self viewWithTag:10];
    [button setUserInteractionEnabled:false];
    [self setUserInteractionEnabled:false];
    typeof(self) weak = self;
    [self DismissCompletionBlock:^(BOOL CompletionBlock) {
        [weak removeFromSuperview];
    }];
  
}


-(void)removeFromSuperview{
    [super removeFromSuperview];
}


-(void)show{
    window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    window.windowLevel = UIWindowLevelNormal + 5;
    window.backgroundColor = [UIColor clearColor];
    [window makeKeyAndVisible];
    window.alpha = 0;
    window.transform = CGAffineTransformMakeScale(1.1, 1.1);
    [window makeKeyAndVisible];
    [window addSubview:self];
    
    // ウィンドウのオーナーとしてアプリ自身に括りつけとく
    objc_setAssociatedObject([UIApplication sharedApplication], &kAssocKey_Window, window, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
    [UIView transitionWithView:window duration:.2 options:UIViewAnimationOptionTransitionCrossDissolve|UIViewAnimationOptionCurveEaseInOut animations:^{
        window.alpha = 1.;
        window.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        
    }];
}

@end

@interface CustomButton ()
@end

@implementation CustomButton

-(instancetype) initWithFrame:(CGRect)frame{

    self = [super initWithFrame:frame];
    if (self) {
        
        //1.文字居中
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        //2.文字大小
        self.titleLabel.font = [UIFont systemFontOfSize:14];
        //3.图片的内容模式
        self.imageView.contentMode = UIViewContentModeScaleAspectFit;
        
        [self addTarget:self action:@selector(scaleToSmall)
       forControlEvents:UIControlEventTouchDown | UIControlEventTouchDragEnter];
        [self addTarget:self action:@selector(scaleToDefault)
       forControlEvents:UIControlEventTouchDragExit];
    }
    return self;
}

-(void)setMenuData:(MenuLabel *)MenuData {

    _MenuData = MenuData;
    UIImage *image = [UIImage imageNamed:MenuData.iconName];
    [self setImage:image forState:UIControlStateNormal];
    [self setTitle:MenuData.title forState:UIControlStateNormal];
    UIColor *color = [UIColor getPixelColorAtLocation:CGPointMake(50, 20) inImage:image];
    [self setTitleColor:color forState:UIControlStateNormal];
    
}

- (void)refreshMenuData:(MenuLabel *)MenuData contentSize:(CGSize)contentSize{
    _MenuData = MenuData;
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(25*kW/750, 0, contentSize.width - 50*kW/750, contentSize.width- 50*kW/750)];
    imageView.image = [UIImage imageNamed:MenuData.iconName];
    [self addSubview:imageView];
    
    UILabel *textLabel = [[UILabel alloc] init];
    textLabel.frame = CGRectMake(0, contentSize.height - 15, contentSize.width, 15);
    textLabel.textAlignment = NSTextAlignmentCenter;
    textLabel.font = [UIFont systemFontOfSize:13.0f];
    textLabel.textColor = [UIColor whiteColor];
    textLabel.text = MenuData.title;
    [self addSubview:textLabel];
}

- (void)scaleToSmall
{
    CABasicAnimation *theAnimation;
    theAnimation=[CABasicAnimation animationWithKeyPath:KeyPath];
    theAnimation.delegate = self;
    theAnimation.duration = 0.1;
    theAnimation.repeatCount = 0;
    theAnimation.removedOnCompletion = FALSE;
    theAnimation.fillMode = kCAFillModeForwards;
    theAnimation.autoreverses = NO;
    theAnimation.fromValue = [NSNumber numberWithFloat:1];
    theAnimation.toValue = [NSNumber numberWithFloat:1.1f];
    [self.imageView.layer addAnimation:theAnimation forKey:theAnimation.keyPath];
}

- (void)scaleToDefault
{
    CABasicAnimation *theAnimation;
    theAnimation=[CABasicAnimation animationWithKeyPath:KeyPath];
    theAnimation.delegate = self;
    theAnimation.duration = 0.1;
    theAnimation.repeatCount = 0;
    theAnimation.removedOnCompletion = FALSE;
    theAnimation.fillMode = kCAFillModeForwards;
    theAnimation.autoreverses = NO;
    theAnimation.fromValue = [NSNumber numberWithFloat:1.1f];
    theAnimation.toValue = [NSNumber numberWithFloat:1];
    [self.imageView.layer addAnimation:theAnimation forKey:theAnimation.keyPath];
}

-(void)SelectdAnimation{

    CABasicAnimation *scaleAnimation = [CABasicAnimation animationWithKeyPath:KeyPath];
    scaleAnimation.delegate = self;
    scaleAnimation.duration = 0.2;
    scaleAnimation.repeatCount = 0;
    scaleAnimation.removedOnCompletion = FALSE;
    scaleAnimation.fillMode = kCAFillModeForwards;
    scaleAnimation.autoreverses = NO;
    scaleAnimation.fromValue = @1;
    scaleAnimation.toValue = @1.3;
    
    CABasicAnimation *opacityAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    opacityAnimation.delegate = self;
    opacityAnimation.duration = 0.2;
    opacityAnimation.repeatCount = 0;
    opacityAnimation.removedOnCompletion = FALSE;
    opacityAnimation.fillMode = kCAFillModeForwards;
    opacityAnimation.autoreverses = NO;
    opacityAnimation.fromValue = @1;
    opacityAnimation.toValue = @0;
    
    [self.layer addAnimation:scaleAnimation forKey:scaleAnimation.keyPath];
    [self.layer addAnimation:opacityAnimation forKey:opacityAnimation.keyPath];
}

-(void)CancelAnimation{

    CABasicAnimation *scaleAnimation = [CABasicAnimation animationWithKeyPath:KeyPath];
    scaleAnimation.delegate = self;
    scaleAnimation.duration = 0.2;
    scaleAnimation.repeatCount = 0;
    scaleAnimation.removedOnCompletion = FALSE;
    scaleAnimation.fillMode = kCAFillModeForwards;
    scaleAnimation.autoreverses = NO;
    scaleAnimation.fromValue = @1;
    scaleAnimation.toValue = @0.3;
    
    CABasicAnimation *opacityAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    opacityAnimation.delegate = self;
    opacityAnimation.duration = 0.2;
    opacityAnimation.repeatCount = 0;
    opacityAnimation.removedOnCompletion = FALSE;
    opacityAnimation.fillMode = kCAFillModeForwards;
    opacityAnimation.autoreverses = NO;
    opacityAnimation.fromValue = @1;
    opacityAnimation.toValue = @0;
    
    [self.layer addAnimation:scaleAnimation forKey:scaleAnimation.keyPath];
    [self.layer addAnimation:opacityAnimation forKey:opacityAnimation.keyPath];
    
}

#pragma mark 调整内部ImageView的frame
- (CGRect)imageRectForContentRect:(CGRect)contentRect
{
    CGFloat imageWidth = contentRect.size.width/1.7;
    CGFloat imageX = CGRectGetWidth(contentRect)/2 - imageWidth/2;
    CGFloat imageHeight = imageWidth;
    CGFloat imageY = CGRectGetHeight(self.bounds) - (imageHeight + 30);
    return CGRectMake(imageX, imageY, imageWidth, imageHeight);
}

#pragma mark 调整内部UILabel的frame
- (CGRect)titleRectForContentRect:(CGRect)contentRect
{
    CGFloat titleX = 0;
    CGFloat titleHeight = 20;
    CGFloat titleY = contentRect.size.height - titleHeight;
    CGFloat titleWidth = contentRect.size.width;
    return CGRectMake(titleX,titleY, titleWidth, titleHeight);
}

@end

@implementation ImageView

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event{
    UIView *view = [super hitTest:point withEvent:event];
    
    if (view) {
        return view.superview.superview;//view.superview; or view.superview.superview; or view.superview.superview.superview;... if has
    }else
    return nil;
}

@end
