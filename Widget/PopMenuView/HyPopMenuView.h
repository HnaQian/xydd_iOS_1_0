//
//  HyPopMenuView.h
//  HyPopMenuView
//
//  Created by  H y on 15/9/8.
//  Copyright (c) 2015年 ouy.Aberi. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MenuLabel;

typedef void(^SelectdCompletionBlock)(NSInteger index);


@interface HyPopMenuView : UIView
@property(nonatomic,assign) CGFloat springBounce;
@property(nonatomic,assign) CGFloat springspeed;
+(void)CreatingPopMenuObjectItmes:(NSArray<MenuLabel *> *)Items
                        isGeneral:(BOOL)isGeneral
                          TopView:(UIView *)topView
           SelectdCompletionBlock:(SelectdCompletionBlock)block;

+(void)CreatingPopMenuObjectItmes:(NSArray<MenuLabel *> *)Items
                        isGeneral:(BOOL)isGeneral
           SelectdCompletionBlock:(SelectdCompletionBlock)block;

-(void)SelectdCompletionBlock:(SelectdCompletionBlock)block;

-(void)StartTheAnimationFromValue:(CGRect)fromValue
                          ToValue:(CGRect)toValue
                            Delay:(CFTimeInterval)delay
                           Object:(id/*<UIView *>*/)obj
                  CompletionBlock:(void(^) (BOOL CompletionBlock))completionBlock HideDisplay:(BOOL)HideDisplay;

-(instancetype) initWithItmes:(NSArray<MenuLabel *> *)Itmes isGeneral:(BOOL)isGeneral;

@end

@interface CustomButton : UIButton
@property (nonatomic, retain)MenuLabel *MenuData;

- (void)refreshMenuData:(MenuLabel *)MenuData contentSize:(CGSize)contentSize;
-(void)SelectdAnimation;
-(void)CancelAnimation;
@end

@interface  ImageView: UIImageView
@end