//
//  ReceiveAddressView.swift
//  Limi
//
//  Created by Richie on 16/3/11.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class ReceiveAddressView: BaseUnitView {
    
    var address_delegate:OperationableReceiveAddressViewDelegate?{return (self.delegate as? OperationableReceiveAddressViewDelegate)}
    
    //private
    let addressIcon = UIImageView(image: UIImage(named: "icon_address"))
    let nameLbl = UILabel()
    let mobileLbl = UILabel()
    let addressLbl = UILabel()
    let detailIcon =  UIImageView(image: UIImage(named: "cellInfo"))
    
    //收货人
    var tipeTitleLbl:UILabel{return _tipeTitleLbl}
    //请选择收货地址
    var tipeContentLbl:UILabel{return _tipeContentLbl}

    //收货人
    fileprivate let _tipeTitleLbl = UILabel()
    //请选择收货地址
    fileprivate let _tipeContentLbl = UILabel()
    
    
    //Public
    var addressModel:AddressMode?{return _addressModel}
    fileprivate var _addressModel:AddressMode?
    
    func reloadData(_ data:AddressMode?)
    {
        self._addressModel = data
        if self._addressModel != nil {
            let _ = Delegate_Selector(address_delegate, #selector(OperationableReceiveAddressViewDelegate.operationableReceiveAddressViewDidSelectedAddress(_:))){Void in self.address_delegate!.operationableReceiveAddressViewDidSelectedAddress!(self._addressModel!)}
            
            if let mode = data
            {
                nameLbl.text = mode.name
                mobileLbl.text = mode.mobile
                addressLbl.text = mode.province + " " + mode.city + " " + mode.area + " " + mode.address
            }
            
            let _ = [nameLbl,mobileLbl,addressLbl].map{$0.isHidden = (data == nil)}
            let _ = [_tipeTitleLbl,_tipeContentLbl].map{$0.isHidden = !(data == nil)}
            
        }else {
            
            let _ = [nameLbl,mobileLbl,addressLbl].map{$0.isHidden = (data == nil)}
            let _ = [_tipeTitleLbl,_tipeContentLbl].map{$0.isHidden = !(data == nil)}
            
        }
        
    }
    
    
    /**
     创建地址栏
     
     - parameter operationable: 是否可操作
     
     - returns: object
     */
    init()
    {
        super.init(frame: CGRect(x: 0, y: 0, width: Constant.ScreenSize.SCREEN_WIDTH,height: 80))
        
        self.isUserInteractionEnabled = false
        
        detailIcon.isHidden = true
        
        self.setupUI()
    }
    
    let colorEdgBottom = UIImageView(image: UIImage(named: "colorEdg"))
    let colorEdgTop = UIImageView(image: UIImage(named: "colorEdg"))
    
    override func setupUI()
    {
        self.backgroundColor = UIColor.white
        
        let _ = [colorEdgTop,colorEdgBottom,addressIcon,addressLbl,nameLbl,mobileLbl,detailIcon,_tipeTitleLbl,_tipeContentLbl].map{self.addSubview($0)}
        
        _tipeContentLbl.textColor = UIColor(rgba: Constant.common_C8_color)
        addressLbl.textColor = UIColor(rgba: Constant.common_C8_color)
        addressLbl.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        addressLbl.numberOfLines = 2
        addressLbl.lineBreakMode = NSLineBreakMode.byCharWrapping
        
        self.addressIcon.isHidden = false
        _tipeTitleLbl.text = ""
        _tipeContentLbl.text = ""
        
        addressIcon.snp_makeConstraints { (make) -> Void in
            let _ = make.left.equalTo(self).offset(MARGIN_8)
            let _ = make.centerY.equalTo(self)
            let _ = make.height.equalTo(30)
            let _ = make.width.equalTo(30)
        }
        
        nameLbl.snp_makeConstraints { (make) -> Void in
            let _ = make.bottom.equalTo(addressIcon.snp_centerY)
            let _ = make.left.equalTo(addressIcon.snp_right).offset(MARGIN_8)
            let _ = make.height.equalTo(20)
        }
        
        mobileLbl.snp_makeConstraints { (make) -> Void in
            let _ = make.left.equalTo(nameLbl.snp_right).offset(MARGIN_8)
            let _ = make.top.equalTo(nameLbl)
            let _ = make.height.equalTo(20)
        }
        
        addressLbl.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(addressIcon.snp_centerY).offset(-MARGIN_4)
            let _ = make.left.equalTo(addressIcon.snp_right).offset(MARGIN_8)
            let _ = make.right.equalTo(detailIcon.snp_left)
            let _ = make.height.equalTo(40)
        }
        
        detailIcon.snp_makeConstraints { (make) -> Void in
            let _ = make.centerY.equalTo(self)
            let _ = make.right.equalTo(self).offset(-MARGIN_8)
            let _ = make.height.equalTo(20)
            let _ = make.width.equalTo(20)
        }
        
        _tipeTitleLbl.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self).offset(MARGIN_10)
            let _ = make.left.equalTo(self).offset(MARGIN_8)
            let _ = make.height.equalTo(20)
        }
        
        _tipeContentLbl.snp_makeConstraints { (make) -> Void in
            let _ = make.bottom.equalTo(self).offset(-MARGIN_10)
            let _ = make.left.equalTo(self).offset(MARGIN_8)
            let _ = make.height.equalTo(20)
        }
        
        colorEdgTop.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self)
            let _ = make.left.equalTo(self)
            let _ = make.right.equalTo(self)
            let _ = make.height.equalTo(8 * Constant.ScreenSize.SCREEN_WIDTH/820)
        }
        
        colorEdgBottom.snp_makeConstraints { (make) -> Void in
            let _ = make.left.equalTo(self)
            let _ = make.right.equalTo(self)
            let _ = make.bottom.equalTo(self)
            let _ = make.height.equalTo(8 * Constant.ScreenSize.SCREEN_WIDTH/820)
        }
        
        if let mode = self._addressModel{self.reloadData(mode)}
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


//MARK: Order Address
class OperationableReceiveAddressView:ReceiveAddressView,AddressListDelegate,EditAddressDelegate,AddressRequestManagerDelegate
{
    fileprivate var index = 0
    fileprivate var haveAddress:Bool{return addressListVC.addressList.count > 0}
    
    fileprivate var _addressListVC:AddressListViewController!
    fileprivate  var addressListVC:AddressListViewController!{
        if _addressListVC == nil{
            _addressListVC = AddressListViewController(nibName: "AddressListViewController", bundle: nil)
            _addressListVC.delegate = self
        }
        return _addressListVC
    }
    
    
    fileprivate var _addressEditVC:DeliveryAddressEditViewController!
    fileprivate var addressEditVC:DeliveryAddressEditViewController!{
        if _addressEditVC == nil{
            _addressEditVC = DeliveryAddressEditViewController()
            _addressEditVC.delegate = self
        }
        return _addressEditVC
    }
    
    fileprivate var didInit = false
    
    init(setDefaultAddress:Bool = true){
        
        super.init()
        
        self.isUserInteractionEnabled = true
        
        detailIcon.isHidden = false
        
        if setDefaultAddress == true{
            
            if self.isUserInteractionEnabled == true{
                //如果已经登录，添加监听
                if UserInfoManager.didLogin == true{
                    //                    if UserInfoManager.didLogin{
                    addressListVC.loadData(){[weak self] in
                        if let sel = self
                        {
                            if sel.didInit{return}
                            sel.didInit = true
                            
                            if sel.addressListVC.addressList.count > 0{
                                for item in sel.addressListVC.addressList{
                                    if item.is_default == 1{
                                        sel.reloadData(item)
                                    }
                                }
                            }
                        }
                    }
                    //                    }
                }else{
                    //如果已经登录，添加监听
                    
                    NotificationCenter.default.addObserver(self, selector: #selector(OperationableReceiveAddressView.loginSuccess), name: NSNotification.Name(rawValue: Constant.notification_login_success), object: nil)
                }
                
            }
        }
    }
    
    
    
    override func setupUI() {
        super.setupUI()
        tipeTitleLbl.snp_remakeConstraints { (make) -> Void in
            let _ = make.height.equalTo(20)
            let _ = make.left.equalTo(addressIcon.snp_right).offset(MARGIN_8)
            let _ = make.centerY.equalTo(addressIcon)
        }
        
        tipeTitleLbl.text = "请选择收货地址"
        
        tipeContentLbl.text = ""
    }
    
    override func reloadData(_ data: AddressMode?) {
        super.reloadData(data)
        if data == nil{
            _tipeTitleLbl.text = "请选择收货地址"
            _tipeContentLbl.text = ""
        }
        
    }
    
    //地址delegate
    func reloadAddressCellFromList(_ info: AddressMode?,index : Int){
        self.reloadData(info)
        self.index = index
    }
    
    //新增地址delegate
    func reloadAddressToOrder(_ info: AddressMode?)
    {
        if UserInfoManager.didLogin{
            addressListVC.loadData()
        }
        self.reloadData(info)
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if UserInfoManager.didLogin
        {
            if haveAddress{
                addressListVC.selectIndex = self.index
                self.viewController?.navigationController?.pushViewController(addressListVC, animated: true)
            }
            else{
                let VC = DeliveryAddressEditViewController()
                VC.delegate = self
                self.viewController?.navigationController?.pushViewController(VC, animated: true)
            }
        }else
        {
            let VC = DeliveryAddressEditViewController()
            VC.delegate = self
            VC.addressInfo = self._addressModel
            self.viewController?.navigationController?.pushViewController(VC, animated: true)
        }
        
        let _ = Delegate_Selector(delegate, #selector(OrderUnitViewDelegate.orderReceiveAddressViewTapAction(_:))){Void in self.delegate!.orderReceiveAddressViewTapAction!(self)}
    }
    
    
    
    @objc func loginSuccess()
    {
        NotificationCenter.default.removeObserver(self, name:NSNotification.Name(rawValue: Constant.notification_login_success), object: nil)
        self.uploadAddress()
    }
    
    
    
    /**
     上传地址
     */
    func uploadAddress(){
        if let model = self._addressModel
        {
            AddressRequestManager(delegate:self).addressPlusNewWithModel(self,addressModel: model)
        }
    }
    
    
    
    func addressRequestManagerDidPlusSuccess(_ requestManager: LMRequestManager, result: LMResultMode) {
        
        if result.status == 200{
            if let data = result.data,
                let id = data["id"] as? Int,
                let model = addressModel
            {
                addressListVC.loadData()
                model.id = id
            }
        }
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


//MARK: Invlice Address
class OperationableReceiveAddressViewOfInvoice:ReceiveAddressView,AddressListDelegate,EditAddressDelegate,AddressRequestManagerDelegate{
    
    let titleLabel = UILabel()
    
    fileprivate var haveAddress:Bool{return addressListVC.addressList.count > 0}
    fileprivate var _addressListVC:AddressListViewController!
    fileprivate var addressListVC:AddressListViewController!{
        if _addressListVC == nil{
            _addressListVC = AddressListViewController(nibName: "AddressListViewController", bundle: nil)
            _addressListVC.delegate = self
        }
        return _addressListVC
    }
    
    
    fileprivate var _addressEditVC:DeliveryAddressEditViewController!
    fileprivate var addressEditVC:DeliveryAddressEditViewController!{
        if _addressEditVC == nil{
            _addressEditVC = DeliveryAddressEditViewController()
            _addressEditVC.delegate = self
        }
        return _addressEditVC
    }
    
    fileprivate  var setDefaultAddress = false
    
    init(setDefaultAddress:Bool = true){
        
        super.init()
        self.setDefaultAddress = setDefaultAddress
        
        self.isUserInteractionEnabled = true
        
        detailIcon.isHidden = false
        
        
        addressListVC.loadData()
    }
    
    
    override func setupUI() {
        
        super.setupUI()
        self.addressIcon.isHidden = false
        self.colorEdgBottom.isHidden = true
        self.colorEdgTop.isHidden = true
        
        self.addressIcon.snp_updateConstraints { (make) -> Void in
            let _ = make.width.equalTo(0)
        }
        
        self.addSubview(titleLabel)
        
        titleLabel.font = UIFont.systemFont(ofSize: Constant.common_F2_font)
        titleLabel.text = "发票寄送地址"
        tipeTitleLbl.text = ""
        tipeContentLbl.font = UIFont.systemFont(ofSize: Constant.common_F2_font)
        tipeContentLbl.text = "请选择寄送地址"
        titleLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self).offset(MARGIN_8)
            let _ = make.left.equalTo(self).offset(MARGIN_8)
            let _ = make.height.equalTo(20)
        }

        tipeContentLbl.snp_remakeConstraints { (make) -> Void in
            let _ = make.top.equalTo(titleLabel.snp_bottom).offset(MARGIN_8)
            let _ = make.left.equalTo(titleLabel)
            let _ = make.height.equalTo(20)
        }
        nameLbl.snp_remakeConstraints { (make) -> Void in
            let _ = make.top.equalTo(titleLabel.snp_bottom).offset(MARGIN_13)
            let _ = make.left.equalTo(titleLabel)
            let _ = make.height.equalTo(20)
        }
        
        addressLbl.snp_remakeConstraints { (make) -> Void in
            let _ = make.top.equalTo(nameLbl.snp_bottom).offset(MARGIN_8)
            let _ = make.left.equalTo(titleLabel)
            let _ = make.right.equalTo(detailIcon.snp_left)
            let _ = make.height.equalTo(20)
        }
        
        nameLbl.textColor = UIColor(rgba: Constant.common_C8_color)
        mobileLbl.textColor = UIColor(rgba: Constant.common_C8_color)
        
        addressListVC.navTitle = "选择地址"
        addressListVC.addBtnTitle = "新增地址"
        
        addressEditVC.navTitle = "新增地址"
    }
    
    //地址delegate
    func reloadAddressCellFromList(_ info: AddressMode?,index : Int){
        self.reloadData(info)
        self.snp_updateConstraints { (make) in
            if info == nil {
                let _ = make.height.equalTo(60)
            }else {
                let _ = make.height.equalTo(100)
            }
            
        }
        viewController?.view.layoutIfNeeded()
    }
    
    //新增地址delegate
    func reloadAddressToOrder(_ info: AddressMode?)
    {
        addressListVC.loadData()
        self.reloadData(info)
        self.snp_updateConstraints { (make) in
            if info == nil {
                let _ = make.height.equalTo(60)
            }else {
                let _ = make.height.equalTo(100)
            }
            
        }
        viewController?.view.layoutIfNeeded()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if UserInfoManager.didLogin
        {
            if haveAddress{
                self.viewController?.navigationController?.pushViewController(addressListVC, animated: true)
            }
            else{
                let VC = DeliveryAddressEditViewController()
                VC.delegate = self
                self.viewController?.navigationController?.pushViewController(VC, animated: true)
            }
        }
        let _ = Delegate_Selector(delegate, #selector(OrderUnitViewDelegate.orderReceiveAddressViewTapAction(_:))){Void in self.delegate!.orderReceiveAddressViewTapAction!(self)}
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class TariffeAddressView: BaseUnitView {
    
    let _rechargeLbl = UILabel()
    let _belongLbl = UILabel()
    
    var tariffeAddressMode: TariffeAddressMode?{return _tariffeAddressMode}
    fileprivate var _tariffeAddressMode:TariffeAddressMode?
    
    
    init() {
        super.init(frame: CGRect.zero)
        self.isUserInteractionEnabled = false
        
        self.setupUI()
    }
    
    override func setupUI() {
        
        self.iheight = 60
        self.backgroundColor = UIColor.white
        
        let _ = [_rechargeLbl,_belongLbl].map{self.addSubview($0)}
        
        _rechargeLbl.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        _belongLbl.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        
        _rechargeLbl.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(self).offset(MARGIN_10)
            let _ = make.left.equalTo(self).offset(MARGIN_8)
            let _ = make.height.equalTo(20)
        }
        
        _belongLbl.snp_makeConstraints { (make) in
            let _ = make.top.equalTo(_rechargeLbl.snp_bottom).offset(MARGIN_4)
            let _ = make.left.equalTo(self).offset(MARGIN_8)
            let _ = make.height.equalTo(20)
        }
        
    }
    
    func reloadData(_ data:TariffeAddressMode) {
        self._tariffeAddressMode = data
        if self.tariffeAddressMode != nil {
            _rechargeLbl.text = "充值号码：" + (self._tariffeAddressMode?.rechargeNumber)!
            _belongLbl.text = "归属地：   " + (self._tariffeAddressMode?.belongAddress)!
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
}

class TariffeAddressMode: BaseDataMode {
    
    var rechargeNumber = NULL_STRING
    var belongAddress = NULL_STRING
    
}





@objc protocol OperationableReceiveAddressViewDelegate:OrderUnitViewDelegate{
    @objc optional func operationableReceiveAddressViewDidSelectedAddress(_ address:AddressMode)
}
