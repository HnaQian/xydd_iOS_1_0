//
//  BaseUnitView.swift
//  Limi
//
//  Created by Richie on 16/3/11.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit


enum EdgLineMode : Int
{
    case topLongLine
    case bottomLongLine
    case topShortLine
    case bottomShortLine
}


class BaseUnitView: UIView {

    weak var delegate:OrderUnitViewDelegate?
    weak var viewController:UIViewController?
    
    var edgLineModes = [EdgLineMode](){didSet
    {
        if edgLineModes.count == 0{return}
        
        for mode in edgLineModes
        {
            let line = Line()
            self.addSubview(line)
            
            line.snp_makeConstraints(closure: { (make) -> Void in
                let _ = make.height.equalTo(0.5)
                
                switch (mode)
                {
                case .topLongLine:
                    let _ = make.top.equalTo(self)
                   let _ =  make.right.equalTo(self)
                    let _ = make.left.equalTo(self)
                    
                case .topShortLine:
                    let _ = make.top.equalTo(self)
                    let _ = make.right.equalTo(self).offset(-MARGIN_8)
                   let _ =  make.left.equalTo(self).offset(MARGIN_8)
                    
                case .bottomLongLine:
                    let _ = make.bottom.equalTo(self)
                    let _ = make.right.equalTo(self)
                    let _ = make.left.equalTo(self)
                    
                case .bottomShortLine:
                    let _ = make.bottom.equalTo(self)
                    let _ = make.right.equalTo(self).offset(-MARGIN_8)
                    let _ = make.left.equalTo(self).offset(MARGIN_8)
                }
            })
        }
        }}
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.clipsToBounds = true
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    class Line: UIView {
        convenience init()
        {
            self.init(frame:CGRect.zero)
            self.backgroundColor = UIColor(rgba: Constant.common_separatLine_color)
        }
    }
    
    func setupUI(){}
    
    func customInit(){}
}
