//
//  RecommendGoodsBanner.swift
//  Limi
//
//  Created by Richie on 16/3/7.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//推荐商品

import UIKit

//banner的高度
private let HEIGHT:CGFloat = 181

//每个item的宽度
private let ITEM_WIDTH:CGFloat = HEIGHT * 0.65

class RecommendGoodsBanner: UIView,GoodsRequestManagerDelegate {
    
    weak var delegate:RecommendGoodsBannerDelegate?
    
    var recommendTipe = ""{didSet{moreTipeLbl.text = "  " + recommendTipe + "  "}}
    
    fileprivate let cell_height:CGFloat = HEIGHT
    fileprivate let cell_width:CGFloat = ITEM_WIDTH + MARGIN_8
    fileprivate var horTableView:LMHorizontalTableView!
    
    fileprivate let moreTipeLbl = UILabel()
    fileprivate var moreTipeLine:UIView = {return UIView()}()
    
    fileprivate var goodsItemViewAry = [GoodsItemView]()
    fileprivate var style:Int{return _style}
    fileprivate var _style = 0
    
    fileprivate var didLoadDataSuccess = false
    
    //Data
    var goodsList: [GoodsModel]{return _goodsList}
    
    fileprivate var _goodsList = [GoodsModel]()
    
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        
        self.setupUI()
    }
    
    init(style:Int,frame: CGRect) {
        super.init(frame:frame)
        _style = style
        self.setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate var shouldDispaly:Bool{return _shouldDispaly}
    fileprivate var _shouldDispaly = true
    
    func disPlay(_ shouldDisplay:Bool){
        
        _shouldDispaly = shouldDisplay
        
        if shouldDisplay && _goodsList.count == 0{
            return
        }
        
        UIView.animate(withDuration: 0.25, animations: {
            self.alpha = shouldDisplay ? 1 : 0
        }) 
    }
    
    
    func reloadDataIfNull(){
        self.loadRecommendGoodsList(self.code, size: self.size, sort: self.sort)
    }
    
    
    fileprivate func setupUI()
    {
        self.isUserInteractionEnabled  = true
        self.iheight = HEIGHT + 40
        self.alpha = 0
        self.backgroundColor = UIColor.white
        
        
        horTableView = LMHorizontalTableView(frame: CGRect(x: 0,y: self.iheight - cell_height,width: Constant.ScreenSizeV2.SCREEN_WIDTH,height: cell_height))
        horTableView.dataSource = self
        horTableView.delegate = self
        
        moreTipeLbl.backgroundColor = UIColor.white
        moreTipeLbl.textAlignment = .center
        moreTipeLbl.font = UIFont.systemFont(ofSize: 15)
        moreTipeLbl.textColor = UIColor.black
        
        if style == 0{
            for view in [horTableView,moreTipeLine,moreTipeLbl]{
                self.addSubview(view)
            }
            
            moreTipeLine.backgroundColor = UIColor(rgba: Constant.common_separatLine_color)
            self.backgroundColor = UIColor(rgba: Constant.common_background_color)
            self.moreTipeLbl.backgroundColor = UIColor(rgba: Constant.common_background_color)
            
            moreTipeLine.snp_makeConstraints { (make) -> Void in
                let _ = make.centerY.equalTo(moreTipeLbl)
                let _ = make.left.equalTo(self).offset(MARGIN_8)
                let _ = make.right.equalTo(self).offset(-MARGIN_8)
                let _ = make.height.equalTo(0.5)
            }
        }else{
            for view in [horTableView,moreTipeLbl] as [Any]{
                self.addSubview(view as! UIView)
            }
        }
        
        moreTipeLbl.snp_makeConstraints { (make) -> Void in
            let _ = make.bottom.equalTo(horTableView.snp_top)
            let _ = make.centerX.equalTo(self)
            let _ = make.height.equalTo(40)
        }

        horTableView.snp_makeConstraints { (make) -> Void in
            let _ = make.bottom.equalTo(self)
            let _ = make.left.equalTo(self)
            let _ = make.width.equalTo(self)
            let _ = make.height.equalTo(cell_height)
        }

    }
    
    fileprivate var code = ""
    fileprivate var size = 20
    fileprivate var sort = 0
    
    
    func loadRecommendGoodsList(_ code: String, size: Int = 20, sort: Int = 0)
    {
        self.size = size
        self.code = code
        self.sort = sort
        
        GoodsRequestManager(delegate:self).goodsLoadRecommendList(self,code: code, size: size, sort: sort, showError: true)
    }
    
    
    func goodsRequestDidLoadGoodsOfRecommendList(_ manager: LMRequestManager, result: LMResultMode, goods: [GoodsModel]) {
        
        if goods.count > 0{
            
            UIView.animate(withDuration: 0.25, animations: {
                self.alpha = self.shouldDispaly ? 1 : 0
            })
            
            _goodsList = goods
            didLoadDataSuccess = true
            reloadData()
        }else{
            UIView.animate(withDuration: 0.25, animations: {
                self.alpha = 0
            })
        }
    }
    
    
    func reloadData(){
        if _goodsList.count == 0{return}
        self.horTableView.reloadData()
    }
}



extension RecommendGoodsBanner:LMHorizontalTableViewDataSource,LMHorizontalTableViewDelegate{
    
    func numberOfRows(_ horizontalTableView: LMHorizontalTableView) -> Int {
        return goodsList.count
    }
    
    
    func horizontalTableView(_ horizontalTableView: LMHorizontalTableView, cellForRow row: Int ,reuseCell:((_ reuseInfentifer:String) ->LMHorizontalTableViewCell?)) -> LMHorizontalTableViewCell {
        
        var cell:SubCell? = reuseCell("indetifer") as? SubCell
        
        if cell == nil{
            cell = SubCell(reuseIdentifier:"indetifer")
        }
        
        cell!.reloadData(goodsList[row])
        
        return cell!
    }
    
    
    func horizontalTableView(_ horizontalTableView: LMHorizontalTableView, heightForRow row: Int) -> CGFloat{
        return cell_height
    }
    
    func horizontalTableView(_ horizontalTableView: LMHorizontalTableView, widthForRow row: Int) -> CGFloat{
        return cell_width
    }
    
    
    func horizontalTableView(_ horizontalTableView: LMHorizontalTableView, didSelectRow row: Int) {
        let _ = Delegate_Selector(delegate, Selector(("recommendGoodsBannerdidSelectedItem:"))){Void in self.delegate?.recommendGoodsBannerdidSelectedItem(self.goodsList[row])}
    }
}



extension RecommendGoodsBanner
{
    
    fileprivate class SubCell:LMHorizontalTableViewCell{
        
        fileprivate var itemView:GoodsItemView!
        
        override init(reuseIdentifier: String) {
            super.init(reuseIdentifier: reuseIdentifier)
            setupUI()
        }
        
        
        func reloadData(_ dataModel:GoodsModel){
            itemView.reloadData(dataModel)
        }
        
        fileprivate func setupUI(){
            itemView = GoodsItemView(frame: CGRect(x: MARGIN_4,y: 0,width: ITEM_WIDTH,height: HEIGHT))
            self.addSubview(itemView)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    
    class GoodsItemView: UIView {
        fileprivate let goodsImg = UIImageView()
        fileprivate let titleLbl = UILabel()
        fileprivate let currentPrice = UILabel()
        fileprivate let oldPrice = StrikeThroughLabel()
        fileprivate let favBtn = UIButton()
        fileprivate let favCount = UILabel()
        fileprivate var goodsModel:GoodsModel!
        
        
        override init(frame:CGRect) {
            super.init(frame: frame)
            self.setupUI()
        }
        
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func reloadData(_ data:GoodsModel)
        {
            self.goodsModel  = data
            
            self.goodsImg.image = UIImage(named: "loadingDefault")
            
            if URL(string: data.goods_image) != nil{
                let tempWidth = Int(self.goodsImg.iwidth * Constant.ScreenSizeV1.SCALE_SCREEN)
                let tempHeight = Int(self.goodsImg.iheight * Constant.ScreenSizeV1.SCALE_SCREEN)
                let urlSuffix = "?imageView2/0/w/" + String(tempWidth) + "/h/" + String(tempHeight)
                let imageScale : String = String(data.goods_image) + urlSuffix
                self.goodsImg.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "loadingDefault"))
            }
            
            titleLbl.text = data.goods_name
            
            currentPrice.text = Utils.checkDecimal(data.price)
            if data.market_price == 0 {
                oldPrice.removeFromSuperview()
            }
            oldPrice.text = Utils.checkDecimal(data.market_price)
            
            favCount.text = "\(data.collect_num)"
        }
        
        
        fileprivate func setupUI()
        {
            self.isUserInteractionEnabled = true
            
            self.layer.borderWidth = 0.5
            self.layer.borderColor = UIColor(rgba: Constant.common_separatLine_color).cgColor
            
            let _ = [goodsImg,titleLbl,currentPrice,oldPrice,favBtn,favCount].map{self.addSubview($0)}
            
            titleLbl.textColor = UIColor(rgba: Constant.common_C6_color)
            titleLbl.font = UIFont.systemFont(ofSize: Constant.common_F6_font)
            if Constant.DeviceType.IS_IPHONE_4_OR_LESS{
                titleLbl.numberOfLines = 1
            }else{
                titleLbl.numberOfLines = 2
            }
            
            currentPrice.textColor = UIColor(rgba: Constant.common_red_color)
            currentPrice.font = UIFont.systemFont(ofSize: Constant.common_F6_font)
            
            oldPrice.textColor = UIColor(rgba: Constant.common_C8_color)
            oldPrice.font = UIFont.systemFont(ofSize: Constant.common_F6_font)
            oldPrice.strikeThroughEnabled = true
            
            favCount.font = UIFont.systemFont(ofSize: Constant.common_F6_font)
            favCount.textColor = UIColor(rgba: Constant.common_C8_color)
            favCount.textAlignment = NSTextAlignment.right
            
            favBtn.setImage(UIImage(named: "goods_general_toolbar_heart"), for: UIControlState())
            favBtn.imageView?.contentMode = UIViewContentMode.scaleAspectFill
            favBtn.isHidden = true
            favCount.isHidden = true
            
            goodsImg.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(self)
                let _ = make.left.equalTo(self)
                let _ = make.right.equalTo(self)
                let _ = make.height.equalTo(goodsImg.snp_width)
            }
            
            
            titleLbl.snp_makeConstraints { (make) -> Void in
                let _ = make.top.equalTo(goodsImg.snp_bottom).offset(MARGIN_4)
                let _ = make.left.equalTo(goodsImg).offset(MARGIN_4)
                let _ = make.right.equalTo(goodsImg).offset(-MARGIN_4)
            }
            
            currentPrice.snp_makeConstraints { (make) -> Void in
                let _ = make.bottom.equalTo(self)
                let _ = make.left.equalTo(titleLbl)
                let _ = make.height.equalTo(20)
            }
            
            oldPrice.snp_makeConstraints { (make) -> Void in
                let _ = make.left.equalTo(currentPrice.snp_right).offset(MARGIN_4)
                let _ = make.top.equalTo(currentPrice)
                let _ = make.height.equalTo(currentPrice)
            }
            
            
            favCount.snp_makeConstraints { (make) -> Void in
                let _ = make.bottom.equalTo(currentPrice)
                let _ = make.right.equalTo(self).offset(-MARGIN_4)
                let _ = make.height.equalTo(20)
            }
            
            favBtn.snp_makeConstraints { (make) -> Void in
                let _ = make.centerY.equalTo(currentPrice)
                let _ = make.right.equalTo(favCount.snp_left)
                let _ = make.height.equalTo(12)
                let _ = make.width.equalTo(12)
            }
        }
    }
}



protocol RecommendGoodsBannerDelegate:NSObjectProtocol
{
    func recommendGoodsBannerdidSelectedItem(_ data:GoodsModel)
}
