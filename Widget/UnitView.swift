//
//  UnitView.swift
//  Limi
//
//  Created by Richie on 16/3/11.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

@objc protocol UnitViewDelegate:NSObjectProtocol {
    @objc optional func addBill()
}
//图标、标题、内容、单元视图
class UnitView: BaseUnitView {
    
    //Private
    fileprivate let _titleIcon = UIImageView()
    fileprivate let _titleLbl = UILabel()
    fileprivate let _contentLbl = UILabel()
    fileprivate let _detailIcon = UIImageView(image: UIImage(named: "cellInfo"))
    fileprivate let _contentIcon = UIImageView()
    fileprivate let _billBtn = UIButton(type: UIButtonType.custom)
    
    var contentIcon:UIImageView{return _contentIcon}
    var titleIcon:UIImageView{return _titleIcon}
    var titleLbl:UILabel{return _titleLbl}
    var contentLbl:UILabel{return _contentLbl}
    var detailIcon:UIImageView{return _detailIcon}
    var billBtn:UIButton {return _billBtn}
    
    weak var originDelegate:UnitViewDelegate?
    
    //Public
    var title:String?{didSet
    {
        self._titleLbl.text = title
        }}
    
    var content:String?{didSet{self._contentLbl.text = content}}
    
    
    var iconImg:String?{didSet
    {
        if let img = iconImg
        {
            self._titleIcon.image = UIImage(named: img)
            
            self._titleIcon.snp_updateConstraints(closure: { (make) -> Void in
                let _ = make.height.equalTo(30)
                let _ = make.width.equalTo(30)
            })
        }
        
        }}
    
    /**
     设置title的内容、字号、颜色
     
     - parameter title: 内容
     - parameter font:  字号，默认为15，如果为0，则使用上一次的传值或者默认值
     - parameter color: 颜色，默认为C2
     */
    func setTitleLblAttrubutes(_ title:String? = nil,font:CGFloat = 0,color:UIColor! = nil,isBolderFont:Bool = false)
    {
        if title != nil{self.title = title}
        if font != 0{ self._titleLbl.font = UIFont.systemFont(ofSize: font)}
        if color != nil{self._titleLbl.textColor = color}
        if isBolderFont {
            self._titleLbl.font = UIFont.boldSystemFont(ofSize: Constant.common_F4_font)
        }
    }
    
    /**
     设置content的内容、字号、颜色
     
     - parameter content: 内容
     - parameter font:    字号，默认为15，如果为0，则使用上一次的传值或者默认值
     - parameter color:   颜色，默认为C8
     */
    func setContentLblAttrubutes(_ content:String? = nil,font:CGFloat = 0,color:UIColor! = nil,textAlignment:NSTextAlignment = NSTextAlignment.right,isBolderFont:Bool = false)
    {
        self._contentLbl.textAlignment = textAlignment
        if content != nil{ self.content = content}
        if font != 0{
            if isBolderFont {
                self._contentLbl.font = UIFont.boldSystemFont(ofSize: font)
            }else {
                self._contentLbl.font = UIFont.systemFont(ofSize: font)
            }
        }
        if color != nil{self._contentLbl.textColor = color}
        if _contentLbl.textAlignment.rawValue == 0 {
            _contentLbl.snp_updateConstraints(closure: { (make) in
                let _ = make.left.equalTo(50)
            })
        }
    }
    

    /**
     如果此unitview可以点击，传：true,默认false.
     
     - parameter userInteractionEnabled: 是否可点击,如果传true，会显示箭头。false则不显示
     
     - returns: [UnitView instance]
     */
    init(_ userInteractionEnabled:Bool! = false,title:String = "")
    {
        super.init(frame:CGRect.zero)
        
        self.isUserInteractionEnabled = userInteractionEnabled
        
        self.setupUI()
        
        self._titleLbl.text = title
        
        self.customInit()
    }
    
    
    override func setupUI()
    {
        self.clipsToBounds = true
        
        self.iheight = 46
        self.backgroundColor = UIColor.white
        _detailIcon.contentMode = UIViewContentMode.scaleAspectFill
        
        _titleLbl.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        _titleLbl.textColor = UIColor(rgba: Constant.common_C2_color)
        _titleLbl.textAlignment = NSTextAlignment.left
        
        _billBtn.isHidden = true
        _billBtn.titleLabel?.font = UIFont.systemFont(ofSize: Constant.common_F5_font)
        _billBtn.setTitleColor(UIColor(rgba:Constant.common_red_color), for: UIControlState())
        _billBtn.layer.borderColor = UIColor(rgba: Constant.common_red_color).cgColor
        _billBtn.layer.borderWidth = 1
        _billBtn.layer.cornerRadius = 5.0
        _billBtn.layer.masksToBounds = true
        _billBtn.addTarget(self, action: #selector(UnitView.billBtnOnClick), for: .touchUpInside)
        
        _contentLbl.textAlignment = NSTextAlignment.right
        _contentLbl.numberOfLines = 0
        
        _contentLbl.font = UIFont.systemFont(ofSize: Constant.common_F4_font)
        _contentLbl.textColor = UIColor(rgba: Constant.common_C8_color)
        
        let _ = [_titleIcon,_titleLbl,_billBtn,_contentLbl,_contentIcon].map{self.addSubview($0)}
        _titleIcon.snp_makeConstraints { (make) -> Void in
            let _ = make.left.equalTo(self).offset(MARGIN_8)
            let _ = make.width.equalTo(0)
            let _ = make.centerY.equalTo(self)
        }

        _titleLbl.snp_makeConstraints { (make) -> Void in
            let _ = make.centerY.equalTo(self)
            let _ = make.left.equalTo(_titleIcon.snp_right)
            let _ = make.right.equalTo(_billBtn.snp_left).offset(-MARGIN_8)
            let _ = make.height.equalTo(20)
        }
        
        _billBtn.snp_makeConstraints { (make) in
            let _ = make.centerY.equalTo(self)
            let _ = make.left.equalTo(_titleLbl.snp_right)
            let _ = make.width.equalTo(50)
            let _ = make.height.equalTo(25)
        }
        
        _contentIcon.snp_makeConstraints { (make) -> Void in
            let _ = make.right.equalTo(_contentLbl.snp_left).offset(-MARGIN_8)
            let _ = make.centerY.equalTo(self)
            let _ = make.height.equalTo(20)
            let _ = make.width.equalTo(20)
        }

        
        if self.isUserInteractionEnabled == true
        {
            self.addSubview(_detailIcon)
            _detailIcon.snp_makeConstraints(closure: { (make) -> Void in
                let _ = make.centerY.equalTo(self)
                let _ = make.right.equalTo(self).offset(-MARGIN_8)
                let _ = make.height.equalTo(20)
                let _ = make.width.equalTo(20)
            })
            
            _contentLbl.snp_makeConstraints(closure: { (make) -> Void in
                let _ = make.left.equalTo(_contentIcon.snp_right).offset(MARGIN_8)
                let _ = make.right.equalTo(_detailIcon.snp_left)
//                make.height.equalTo(40)
                let _ = make.centerY.equalTo(self)
            })
        }else
        {
            _contentLbl.snp_makeConstraints(closure: { (make) -> Void in
                let _ = make.left.equalTo(_contentIcon.snp_right).offset(MARGIN_8)
                let _ = make.right.equalTo(self).offset(-MARGIN_8)
//                make.height.equalTo(40)
                let _ = make.centerY.equalTo(self)
            })
        }
        
        
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let _ = Delegate_Selector(delegate, #selector(OrderUnitViewDelegate.orderUnitViewTapAction(_:))){Void in self.delegate!.orderUnitViewTapAction!(self)}
    }
    
    @objc func billBtnOnClick() {
        let _ = Delegate_Selector(originDelegate,  #selector(UnitViewDelegate.addBill)) {
            Void in self.originDelegate?.addBill!()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private let COST_KEY = "serviceCost"
private let VOICE_KEY = "voiceFile"
private let IDS_KEY = "serviceids"
private let TEMP_IDS_KEY = "tempServiceids"

class WishSendUnitView:UnitView {
    
    fileprivate var _delegate:WishSendUnitViewDelegate?{return (self.delegate as? WishSendUnitViewDelegate)}
    
    static var serviceCostAry:[Double]{return UserDefaults.standard.object(forKey: COST_KEY) as! [Double]}
    static var serviceidAry:[Int]{return UserDefaults.standard.object(forKey: IDS_KEY) as! [Int]}
    static var tempServiceidAry:[Int]{return UserDefaults.standard.object(forKey: TEMP_IDS_KEY) as! [Int]}
    static var voiceId:Int{return UserDefaults.standard.object(forKey: VOICE_KEY) as! Int}
    static var serviceIdsEffected = false
    
    var serviceCost:[Double]{return WishSendUnitView.serviceCostAry}
    var serviceIds:[Int]{return WishSendUnitView.serviceidAry}
    var packageID:Int?
    
    fileprivate let wishData = [["leftWishIcon","¥5.00起","礼物包装"],
                                ["middleWishIcon","¥0.00起","手写贺卡"],
                                ["rightWishIcon","¥0.00起","语音祝福"]]
    
    fileprivate let titleLabel:UILabel = {
       let lbl = UILabel()
        lbl.font = Constant.Theme.Font_14
        lbl.textColor = UIColor(rgba: Constant.common_C2_color)
        lbl.textAlignment = .left
        return lbl
    }()
    
    fileprivate let contentLabel:UILabel = {
        let lbl = UILabel()
        lbl.font = Constant.Theme.Font_14
        lbl.textColor = UIColor(rgba: Constant.common_red_color)
        lbl.layer.borderColor = UIColor(rgba: Constant.common_red_color).cgColor
        lbl.layer.borderWidth = 1.5
        lbl.layer.cornerRadius = 5 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        lbl.textAlignment = .center
        
        return lbl
    }()
    
    fileprivate let leftWishView = singalWishView()
    fileprivate let middleWishView = singalWishView()
    fileprivate let rightWishView = singalWishView()
    
    override func customInit() {
        self.addSubview(titleLabel)
        self.addSubview(contentLabel)
        self.addSubview(leftWishView)
        self.addSubview(middleWishView)
        self.addSubview(rightWishView)
        
        let scale = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        titleLabel.frame = CGRect(x: MARGIN_8, y: 20 * scale, width: 80, height: 25)
        contentLabel.frame = CGRect(x: Constant.ScreenSizeV2.SCREEN_WIDTH - 230 * scale, y: 20 * scale, width: 200 * scale, height: 54 * scale)
        contentLabel.iwidth = CalculateHeightOrWidth.getLabOrBtnWidth("使用心意服务" as NSString, font: contentLabel.font, height: 54 * scale) + 16 * scale
        leftWishView.itop = contentLabel.ibottom + 24 * scale
        leftWishView.ileft = 25 * scale
        
        middleWishView.itop = leftWishView.itop
        middleWishView.ileft = leftWishView.iright
        
        rightWishView.itop = middleWishView.itop
        rightWishView.ileft = middleWishView.iright
        self.iheight = 50 * scale + rightWishView.iheight + 25
        self.titleLabel.text = "心意服务"
        self.contentLabel.text = "使用心意服务"
        self.detailIcon.isHidden = true
        //初始化
        WishSendUnitView.updateServiceids([0,0,0])
        WishSendUnitView.updateServiceCost([0.0,0.0,0.0])
        WishSendUnitView.saveTempServiceids([0,0,0])
        WishSendUnitView.updateVoiceId(0)
        
        leftWishView.reloadData(data: wishData[0])
        middleWishView.reloadData(data: wishData[1])
        rightWishView.reloadData(data: wishData[2])
    }
    
    func reloadData()
    {
        var serviceCount = 0
        
        for serviceID in WishSendUnitView.serviceidAry{
            if serviceID != 0{serviceCount += 1}
        }
        
        let _ = Delegate_Selector(delegate, #selector(WishSendUnitViewDelegate.wishSendChooseOver(_:serviceids:prices:))){Void in self._delegate!.wishSendChooseOver!(self,serviceids: self.serviceIds, prices: self.serviceCost)}
    }
    
    
    //获取录音服务地址
    static var wishSendUrl:String{
        return self.contanctWishSendUrl(serviceidAry)
    }
    
    
    //获取临时的录音服务地址
    static var tempWishSendUrl:String{
        return self.contanctWishSendUrl(tempServiceidAry)
    }
    
    
    //获取心意服务地址
    class func contanctWishSendUrl(_ ids:[Int])->String
    {
        if let wishSend = SystemInfoRequestManager.shareInstance().wishsend_service{

            var urlStr = wishSend

            for id in ids{
                urlStr = urlStr + "\(id),"
            }
            

            urlStr = NSString(string: urlStr).substring(to: urlStr.characters.count - 1)
            print(urlStr)
            return urlStr

        }
        
        return ""
    }
   
    
    /**
     更新心意服务地址
     
     - parameter newPath: 新的录音文件
     */
    class func updateGiftServiceUrl(_ urlStr:String) {
        
    }
    
    
    /**
     更新录音文件
     
     - parameter newPath: 新的录音文件
     */
    class func updateVoiceId(_ voiceId:Int) {
        UserDefaults.standard.setValue(voiceId, forKey: VOICE_KEY)
        UserDefaults.standard.synchronize()
        
        self.saveTempServiceids([self.tempServiceidAry[0],self.tempServiceidAry[1],self.voiceId])
    }
    
    /**
     更新service_ids
     
     - parameter newServiceids: 新的service_ids
     */
    class func updateServiceids(_ newServiceids:[Int]){
        if newServiceids.count > 0{

            if newServiceids.count > 0{
                if newServiceids[0] != 0{
                    Statistics.count(Statistics.Order.order_service_box_click, andAttributes: ["osid":"\(newServiceids[0])"])
                }
            }
            
            if newServiceids.count > 1{
                if newServiceids[1] != 0{
                    Statistics.count(Statistics.Order.order_service_card_click, andAttributes: ["osid":"\(newServiceids[1])"])
                }
            }
            
            UserDefaults.standard.setValue(newServiceids, forKey: IDS_KEY)
            UserDefaults.standard.synchronize()
        }
    }
    
    
    /**
     更新service_ids
     
     - parameter newServiceids: 新的service_ids
     */
    class func saveTempServiceids(_ newServiceids:[Int]){
        if newServiceids.count > 0{
            UserDefaults.standard.setValue(newServiceids, forKey: TEMP_IDS_KEY)
            UserDefaults.standard.synchronize()
        }
    }
    
    
    /**
     更新心意服务价格
     
     - parameter cost: 价格数组
     */
    class func updateServiceCost(_ cost:[Double]) {
        UserDefaults.standard.setValue(cost, forKey: COST_KEY)
        UserDefaults.standard.synchronize()
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {

        Statistics.count(Statistics.Order.order_service_click)
        
        super.touchesBegan(touches, with: event)
        
        if let VC = self.viewController{
            
            var urlStr = WishSendUnitView.wishSendUrl
            if let id = self.packageID{
                urlStr = WishSendUnitView.wishSendUrl + "&package_id=\(id)"
            }
            print(urlStr)
            if let url = URL(string: urlStr){
                VC.navigationController?.pushViewController(WebViewController(URL: url), animated: true)
            }else{
                print("心意服务地址不正确!---\(#file)---\(#line)---")
            }
        }
    }
}

class singalWishView: UIView {

    fileprivate let titleImageView:UIImageView = {
        let imageView = UIImageView()
        imageView.layer.borderColor = UIColor(rgba: Constant.common_borderColor_color).cgColor
        imageView.layer.borderWidth = 1.0
        imageView.layer.cornerRadius = 2.0
        return imageView
    }()
    
    fileprivate let priceLabel:UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: Constant.common_F5_font)
        lbl.textColor = UIColor(rgba: Constant.common_C2_color)
        lbl.textAlignment = .center
        return lbl
    }()
    
    fileprivate let contentLabel:UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: Constant.common_F5_font)
        lbl.textColor = Constant.Theme.Color17
        lbl.textAlignment = .center
        return lbl
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }
    
    func setupUI() {
        self.addSubview(titleImageView)
        self.addSubview(priceLabel)
        self.addSubview(contentLabel)
        
        let scale = Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE
        let width = (Constant.ScreenSizeV2.SCREEN_WIDTH - 45 * scale) / 3
        self.frame = CGRect(x: 0, y: 0, width: width, height: width + 30 + 10 * scale)
        
        titleImageView.frame = CGRect(x: 7.5 * scale, y: 0, width: width - 15 * scale, height: width - 15 * scale)
        priceLabel.frame = CGRect(x: titleImageView.ileft, y: titleImageView.ibottom  + 5 * scale, width: titleImageView.iwidth, height: 15)
        contentLabel.frame = CGRect(x: titleImageView.ileft, y: priceLabel.ibottom + 4 * scale, width: titleImageView.iwidth, height: 15)
    }
    
    func reloadData(data:[String]) {
        if data.count >= 3 {
            self.titleImageView.image = UIImage(named: data[0])
            self.priceLabel.text = data[1]
            self.contentLabel.text = data[2]
        }
      
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}

@objc protocol WishSendUnitViewDelegate:OrderUnitViewDelegate
{
    @objc optional func wishSendChooseOver(_ context:UIView,serviceids:[Int],prices:[Double])
}

