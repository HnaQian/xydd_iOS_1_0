//
//  ProgressView.swift
//  Limi
//
//  Created by ZhiQiangMi on 16/4/19.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class ProgressView: UIView {
    internal dynamic var progress : Float = 0.0
    internal dynamic var progressTintColor = UIColor(white:0.5, alpha:1.0)
    internal dynamic var backgroundTintColor = UIColor(white:1.0, alpha:1.0)
    internal dynamic var annular : Bool = false
    
    convenience init() {
        self.init(frame:CGRect(x: 0, y: 0, width: 37, height: 37))
    }
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        self.backgroundColor = UIColor.clear
        self.isOpaque = false
        _sf_registerForKVO()
    }
    
    required internal init?(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)
    }
    
    override internal func draw(_ rect: CGRect) {
        let allRect = self.bounds
        let circleRect = allRect.insetBy(dx: 2.0, dy: 2.0)
        let context = UIGraphicsGetCurrentContext()
        
        let pi = Float(M_PI)
        if (annular) {
            // Draw background
            let  lineWidth: CGFloat = 2.0
            let processBackgroundPath = UIBezierPath()
            processBackgroundPath.lineWidth = lineWidth
            processBackgroundPath.lineCapStyle = .butt
            let center = CGPoint(x: self.bounds.size.width/2, y: self.bounds.size.height/2)
            let radius = (self.bounds.size.width - lineWidth)/2
            let startAngle = -(pi / 2) // 90 degrees
            var endAngle =  2 * pi + startAngle
            processBackgroundPath.addArc(withCenter: center, radius:radius, startAngle:CGFloat(startAngle), endAngle:CGFloat(endAngle), clockwise: true)
            backgroundTintColor.set()
            processBackgroundPath.stroke()
            // Draw progress
            let processPath = UIBezierPath()
            processPath.lineCapStyle = .square
            processPath.lineWidth = lineWidth
            endAngle = progress * 2 * pi + startAngle
            processPath.addArc(withCenter: center, radius:radius, startAngle:CGFloat(startAngle), endAngle:CGFloat(endAngle), clockwise: true)
            progressTintColor.set()
            processPath.stroke()
        } else {
            // Draw background
            progressTintColor.setStroke()
            backgroundTintColor.setFill()
            context?.setLineWidth(2.0)
            context?.fillEllipse(in: circleRect)
            context?.strokeEllipse(in: circleRect)
            // Draw progress
            let center = CGPoint(x: allRect.size.width / 2, y: allRect.size.height / 2)
            let radius = (allRect.size.width - 4) / 2
            let startAngle = -(pi / 2) // 90 degrees
            let endAngle = progress * 2 * pi + startAngle
            progressTintColor.setFill()
            context?.move(to: CGPoint(x: center.x, y: center.y))
            context?.addArc(center:CGPoint(x: center.x, y: center.y), radius: radius, startAngle: CGFloat(startAngle), endAngle: CGFloat(endAngle), clockwise: false)
//            CGContextAddArc(context, center.x, center.y, radius, CGFloat(startAngle), CGFloat(endAngle), 0)
            context?.closePath()
            context?.fillPath()
        }
    }
    
    deinit {
        _sf_unregisterFromKVO()
    }
    
    // MARK: KVO
    func _sf_registerForKVO() {
        for keyPath in self._sf_observableKeypaths() {
            self.addObserver(self, forKeyPath:keyPath, options:.new, context:nil)
        }
    }
    
    func _sf_unregisterFromKVO() {
        for keyPath in self._sf_observableKeypaths() {
            self.removeObserver(self, forKeyPath:keyPath)
        }
    }
    
    func _sf_observableKeypaths() -> [String] {
        return ["progressTintColor",
                "backgroundTintColor",
                "progress",
                "annular"]
    }
    
    override internal func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        self.setNeedsDisplay()
    }
}

open class SFEffectView : UIVisualEffectView {
    init() {
        super.init(effect: UIBlurEffect(style: .light))
        commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    fileprivate func commonInit() {
        backgroundColor = UIColor(white: 0, alpha: 0.8)
        layer.cornerRadius = 9.0
        layer.masksToBounds = true
        
        contentView.addSubview(self.content)
        
        let offset = 20.0
        
        let motionEffectsX = UIInterpolatingMotionEffect(keyPath: "center.x", type: .tiltAlongHorizontalAxis)
        motionEffectsX.maximumRelativeValue = offset
        motionEffectsX.minimumRelativeValue = -offset
        
        let motionEffectsY = UIInterpolatingMotionEffect(keyPath: "center.y", type: .tiltAlongVerticalAxis)
        motionEffectsY.maximumRelativeValue = offset
        motionEffectsY.minimumRelativeValue = -offset
        
        let group = UIMotionEffectGroup()
        group.motionEffects = [motionEffectsX, motionEffectsY]
        
        addMotionEffect(group)
    }
    
    fileprivate var _content = UIView()
    internal var content: UIView {
        get {
            return _content
        }
        set {
            _content.removeFromSuperview()
            _content = newValue
            _content.alpha = 0.8
            _content.clipsToBounds = true
            _content.contentMode = .center
            frame.size = _content.bounds.size
            addSubview(_content)
        }
    }
}



/**
 * Displays a simple HUD window containing a progress indicator and two optional labels for short messages.
 *
 * This is a simple drop-in class for displaying a progress HUD view similar to Apple's private UIProgressHUD class.
 * The ProgressHUD window spans over the entire space given to it by the initWithFrame constructor and catches all
 * user input on this region, thereby preventing the user operations on components below the view. The HUD itself is
 * drawn centered as a rounded semi-transparent view which resizes depending on the user specified content.
 *
 * This view supports four modes of operation:
 *  - ProgressHUDModeIndeterminate - shows a UIActivityIndicatorView
 *  - ProgressHUDModeDeterminate - shows a custom round progress indicator
 *  - ProgressHUDModeAnnularDeterminate - shows a custom annular progress indicator
 *  - ProgressHUDModeCustomView - shows an arbitrary, user specified view (see `customView`)
 *
 * All three modes can have optional labels assigned:
 *  - If the labelText property is set and non-empty then a label containing the provided content is placed below the
 *    indicator view.
 *  - If also the detailsLabelText property is set then another label is placed below the first label.
 */

let kPadding : CGFloat = 4.0

open class ProgressHUD : UIView {
    
    open dynamic var mode : SFProgressHUDMode = .indeterminate
    open dynamic var customView : UIView?
    open dynamic var indicator : UIView?
    open dynamic var progress : Float = 0.0
    
    open var color : UIColor?
    open var dimBackground : Bool = false
    
    var effectView = SFEffectView()
    var opacity : CGFloat = 0.9
    var xOffset : CGFloat = 0.0
    var yOffset : CGFloat = 0.0
    var margin : CGFloat = 20.0
    var cornerRadius : CGFloat = 10.0
    var graceTime : Float = 0.0
    var minShowTime : TimeInterval = 0.0
    var minSize : CGSize = CGSize.zero
    var size : CGSize = CGSize.zero
    var square : Bool = false
    
    
    //    var hudWasHidden : (() -> ())?
    //    var hudCompletion : (() -> ())?
    
    fileprivate var isFinished : Bool = false
    fileprivate var useAnimation : Bool = false
    fileprivate var taskInProgress : Bool = false
    
    fileprivate var removeFromSuperViewOnHide : Bool = false
    fileprivate var rotationTransform = CGAffineTransform.identity
    fileprivate var showStarted : Date?
    fileprivate var graceTimer : Timer?
    fileprivate var minShowTimer : Timer?
    
    
    static let labelFontSize : CGFloat = 16
    static let detailsLabelFontSize : CGFloat = 12
    
    
    /// MARK: class Method
    
    open class func showHUD(_ view: UIView, animated: Bool) -> ProgressHUD {
        let hud: ProgressHUD = ProgressHUD(view:view)
        hud.removeFromSuperViewOnHide = true
        view.addSubview(hud)
        hud.show(animated)
        return hud
    }
    
    open class func hideHUD(_ view: UIView, animated: Bool) -> Bool {
        if let hud = ProgressHUD.HUD(view) {
            hud.removeFromSuperViewOnHide = true
            hud.hide(animated)
            return true
        }
        return false
    }
    
    open class func hideAllHUDs(_ onView: UIView, animated: Bool) -> Bool {
        var result = false
        if let huds = self.allHUDs(onView) {
            for hud in huds {
                hud.removeFromSuperViewOnHide = true
                hud.hide(animated)
            }
            result = true
        }
        return result
    }
    
    open class func HUD(_ onView: UIView) -> ProgressHUD? {
        for case let hud as ProgressHUD in onView.subviews {
            return hud
        }
        return nil
    }
    
    open class func allHUDs(_ onView: UIView) -> [ProgressHUD]? {
        var huds = [ProgressHUD]()
        for case let hud as ProgressHUD in onView.subviews {
            huds.append(hud)
        }
        return huds.count > 0 ? huds : nil
    }
    
    
    /// show && hide
    
    open func show(_ animated: Bool) {
        assert(Thread.isMainThread, "ProgressHUD needs to be accessed on the main thread.")
        useAnimation = animated
        if (self.graceTime > 0.0) {
            let timer = Timer(timeInterval: 1, target: self, selector: #selector(ProgressHUD.handleGraceTimer(_:)),                       userInfo: nil, repeats: false)
            RunLoop.current.add(timer, forMode:RunLoopMode.commonModes)
        } else {
            showUsingAnimation(useAnimation)
        }
    }
    
    open func hide(_ animated: Bool) {
        assert(Thread.isMainThread, "ProgressHUD needs to be accessed on the main thread.")
        useAnimation = animated;
        // If the minShow time is set, calculate how long the hud was shown,
        // and pospone the hiding operation if necessary
        if (minShowTime > 0.0 && showStarted != nil) {
            let interv = Date().timeIntervalSince(showStarted!)
            if (interv < minShowTime) {
                minShowTimer = Timer.scheduledTimer(timeInterval: minShowTime - interv, target:self, selector:#selector(ProgressHUD.handleMinShowTimer(_:)), userInfo:nil, repeats:false)
                return
                
            }
        }
        // ... otherwise hide the HUD immediately
        hideUseAnimation(useAnimation)
    }
    
    open func hide(_ animated: Bool, afterDelay: TimeInterval) {
        self.perform(#selector(ProgressHUD.hideDelayed(_:)), with:NSNumber(value: animated as Bool), afterDelay:afterDelay)
    }
    
    open func hideDelayed(_ animated: NSNumber) {
        hide(animated.boolValue)
    }
    
    // Timer CallBack
    @objc fileprivate func handleGraceTimer(_ timer: Timer) {
        if taskInProgress {
            showUsingAnimation(useAnimation)
        }
    }
    
    @objc fileprivate func handleMinShowTimer(_ timer: Timer) {
        hideUseAnimation(useAnimation)
    }
    
    override open func didMoveToSuperview() {
        super.didMoveToSuperview()
        _sf_updateForCurrentOrientationAnimated(false)
    }
    
    
    fileprivate func showUsingAnimation(_ animated: Bool) {
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        setNeedsDisplay()
        
        self.showStarted = Date()
        if animated {
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                self.alpha = 1.0
            })
        } else {
            self.alpha = 1.0
        }
    }
    
    fileprivate func hideUseAnimation(_ animated: Bool) {
        if (animated && showStarted != nil) {
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                self.alpha = 0.02
                }, completion: { (finished) -> Void in
                    self.done()
            })
        } else {
            self.done()
        }
        self.showStarted = nil
    }
    
    fileprivate func done() {
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        isFinished = true
        self.alpha = 0.0
        if removeFromSuperViewOnHide {
            removeFromSuperview()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.alpha = 0.0
        self.isOpaque = false
        self.backgroundColor = UIColor.clear
        self.contentMode = .center
        self.taskInProgress = false
        self.rotationTransform = CGAffineTransform.identity
        
        addSubview(effectView)
        addSubview(label)
        addSubview(detailsLabel)
        
        _sf_updateIndicator()
        _sf_registeNotifications()
        _sf_registerForKVO()
    }
    
    deinit {
        _sf_unregisterFromKVO()
        _sf_unregisteNotifications()
    }
    
    //  show on view
    public convenience init(view: UIView) {
        self.init(frame:view.bounds)
    }
    
    public convenience init(window: UIView) {
        self.init(view:window)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        // Entirely cover the parent view
        if let parent = self.superview {
            self.frame = parent.bounds
        }
        let bounds = self.bounds
        
        // Determine the total widt and height needed
        let maxWidth = bounds.size.width - 4 * margin
        var totalSize = CGSize.zero
        
        var indicatorF = CGRect.zero
        if let indicator = indicator {
            indicatorF = indicator.bounds
            indicatorF.size.width = min(indicatorF.size.width, maxWidth)
            totalSize.width = max(totalSize.width, indicatorF.size.width)
            totalSize.height += indicatorF.size.height
        }
        
        var labelSize = _sf_textSize(label.text, font:label.font)
        labelSize.width = min(labelSize.width, maxWidth)
        totalSize.width = max(totalSize.width, labelSize.width)
        totalSize.height += labelSize.height
        if (labelSize.height > 0.0 && indicatorF.size.height > 0.0) {
            totalSize.height += kPadding
        }
        
        let remainingHeight = bounds.size.height - totalSize.height - kPadding - 4 * margin
        let maxSize = CGSize(width: maxWidth, height: remainingHeight)
        let detailsLabelSize = _sf_mutilLineTextSize(detailsLabel.text, font:detailsLabel.font, maxSize:maxSize)
        totalSize.width = max(totalSize.width, detailsLabelSize.width)
        totalSize.height += detailsLabelSize.height
        if (detailsLabelSize.height > 0.0 && (indicatorF.size.height > 0.0 || labelSize.height > 0.0)) {
            totalSize.height += kPadding
        }
        
        totalSize.width += 2 * margin
        totalSize.height += 2 * margin
        
        // Position elements
        var yPos = round(((bounds.size.height - totalSize.height) / 2)) + margin + yOffset
        let xPos = xOffset
        indicatorF.origin.y = yPos
        indicatorF.origin.x = round((bounds.size.width - indicatorF.size.width) / 2) + xPos
        if let indicator = indicator {
            indicator.frame = indicatorF
        }
        yPos += indicatorF.size.height
        
        if (labelSize.height > 0.0 && indicatorF.size.height > 0.0) {
            yPos += kPadding
        }
        var labelF = CGRect.zero
        labelF.origin.y = yPos
        labelF.origin.x = round((bounds.size.width - labelSize.width) / 2) + xPos
        labelF.size = labelSize
        label.frame = labelF
        yPos += labelF.size.height
        
        if (detailsLabelSize.height > 0.0 && (indicatorF.size.height > 0.0 || labelSize.height > 0.0)) {
            yPos += kPadding
        }
        var detailsLabelF = CGRect.zero
        detailsLabelF.origin.y = yPos
        detailsLabelF.origin.x = round((bounds.size.width - detailsLabelSize.width) / 2) + xPos
        detailsLabelF.size = detailsLabelSize
        detailsLabel.frame = detailsLabelF
        
        // Enforce minsize and quare rules
        if (square) {
            let maxValue = max(totalSize.width, totalSize.height)
            if (maxValue <= bounds.size.width - 2 * margin) {
                totalSize.width = maxValue
            }
            if (maxValue <= bounds.size.height - 2 * margin) {
                totalSize.height = maxValue
            }
        }
        if (totalSize.width < minSize.width) {
            totalSize.width = minSize.width
        }
        if (totalSize.height < minSize.height) {
            totalSize.height = minSize.height
        }
        
        size = totalSize
    }
    
    open override func draw(_ rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()
        UIGraphicsPushContext(context!)
        
        if dimBackground {
            //Gradient colours
            let gradLocationsNum: size_t = 2
            let gradLocations: [CGFloat] = [0.0, 1.0]
            let gradColors: [CGFloat] = [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.75]
            let colorSpace = CGColorSpaceCreateDeviceRGB()
            let gradient = CGGradient(colorSpace: colorSpace, colorComponents: gradColors, locations: gradLocations, count: gradLocationsNum);
            
            //Gradient center
            let gradCenter = CGPoint(x: self.bounds.size.width/2, y: self.bounds.size.height/2);
            //Gradient radius
            let gradRadius = min(self.bounds.size.width , self.bounds.size.height)
            //Gradient draw
            context?.drawRadialGradient(gradient!, startCenter: gradCenter, startRadius: 0, endCenter: gradCenter, endRadius: gradRadius, options: .drawsAfterEndLocation)
        }
        
        // Set background rect color
        if let color = color {
            context?.setFillColor(color.cgColor)
        } else {
            context?.setFillColor(gray: 0.0, alpha: opacity)
        }
        
        // Center HUD
        let allRect = self.bounds
        // Draw rounded HUD backgroud rect
        var boxRect = CGRect(x: round((allRect.size.width - size.width) / 2) + self.xOffset,
                                 y: round((allRect.size.height - size.height) / 2) + self.yOffset, width: size.width, height: size.height)
        boxRect = boxRect.integral
        let radius = self.cornerRadius
        
        effectView.bounds = boxRect
        effectView.center = self.center
//        effectView.layer.cornerRadius = radius
        
        context?.beginPath()
        context?.move(to: CGPoint(x: boxRect.minX + radius, y: boxRect.minY))
//         context?.addArc(center:CGPoint(x:boxRect.maxX - radius, y: boxRect.minY + radius), radius: radius, startAngle: CGFloat(3 * M_PI / 2), endAngle: 0, clockwise: false)
//        context?.addArc(center:CGPoint(x:boxRect.maxX - radius, y: boxRect.maxY - radius), radius: radius, startAngle: 0, endAngle:  CGFloat(M_PI / 2), clockwise: false)
//        context?.addArc(center:CGPoint(x:boxRect.maxX - radius, y: boxRect.maxY - radius), radius: radius, startAngle: CGFloat(M_PI / 2), endAngle:  CGFloat(M_PI), clockwise: false)
//        context?.addArc(center:CGPoint(x:boxRect.minX + radius, y: boxRect.minY + radius), radius: radius, startAngle: CGFloat(M_PI), endAngle:  CGFloat(3 * M_PI / 2), clockwise: false)
//        CGContextAddArc(context, boxRect.maxX - radius, boxRect.minY + radius, radius, CGFloat(3 * M_PI / 2), 0, 0)
//        CGContextAddArc(context, boxRect.maxX - radius, boxRect.maxY - radius, radius, 0, CGFloat(M_PI / 2), 0)
//        CGContextAddArc(context, boxRect.minX + radius, boxRect.maxY - radius, radius, CGFloat(M_PI / 2), CGFloat(M_PI), 0)
//        CGContextAddArc(context, boxRect.minX + radius, boxRect.minY + radius, radius, CGFloat(M_PI),
//                        CGFloat(3 * M_PI / 2), 0)
        context?.closePath()
        context?.fillPath()
        
        UIGraphicsPopContext()
    }
    
    func _sf_registerForKVO() {
        for keyPath in self._sf_observableKeypaths() {
            self.addObserver(self, forKeyPath:keyPath, options:.new, context:nil)
        }
    }
    
    func _sf_unregisterFromKVO() {
        for keyPath in self._sf_observableKeypaths() {
            self.removeObserver(self, forKeyPath:keyPath)
        }
    }
    
    func _sf_observableKeypaths() -> [String] {
        return ["mode",
                "customView",
                "progress",
                "activityIndicatorColor"]
    }
    
    open override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if !Thread.isMainThread {
            self.performSelector(onMainThread: #selector(ProgressHUD._sf_updateUIForKeypath(_:)), with:keyPath, waitUntilDone: false)
        } else {
            _sf_updateUIForKeypath(keyPath!)
        }
    }
    
    func _sf_updateUIForKeypath(_ keyPath: String) {
        if keyPath == "mode" || keyPath == "customView" || keyPath == "activityIndicatorColor" {
            self._sf_updateIndicator()
        } else if keyPath == "progress" {
            if let indicator = indicator as? ProgressView {
                indicator.progress = progress
            }
            return
        }
        self.setNeedsLayout()
        self.setNeedsDisplay()
    }
    
    // MARK: Notifications
    
    func _sf_registeNotifications() {
        NotificationCenter.default.addObserver(self, selector:#selector(ProgressHUD.statusBarOrientationDidChange(_:)), name:NSNotification.Name.UIApplicationDidChangeStatusBarOrientation, object:nil)
    }
    
    func _sf_unregisteNotifications() {
        NotificationCenter.default.removeObserver(self, name:NSNotification.Name.UIApplicationDidChangeStatusBarOrientation, object:nil)
    }
    
    func statusBarOrientationDidChange(_ notification: Notification) {
        if self.superview != nil {
            return
        } else {
            _sf_updateForCurrentOrientationAnimated(true)
        }
    }
    
    func _sf_updateForCurrentOrientationAnimated(_ animated: Bool) {
        // Stay in sync with the superview in any case
        if let superview = self.superview {
            bounds = superview.bounds
            setNeedsDisplay()
        }
    }
    
    func _sf_updateIndicator() {
        let isActivityIndicator : Bool = indicator is UIActivityIndicatorView
        let isRoundIndicator : Bool = indicator is ProgressView
        
        if (mode == .indeterminate) {
            if (!isActivityIndicator) {
                // Update to indeterminate indicator
                if indicator == nil {
                    indicator = UIActivityIndicatorView(activityIndicatorStyle:.whiteLarge)
                } else {
                    indicator!.removeFromSuperview()
                }
                (indicator as! UIActivityIndicatorView).startAnimating()
                addSubview(indicator!)
            }
        } else if (mode == .determinate || mode == .annularDeterminate) {
            if (!isRoundIndicator) {
                // Update to determinante indicator
                indicator!.removeFromSuperview()
                indicator = ProgressView()
                addSubview(indicator!)
            }
            if (mode == .annularDeterminate) {
                let progressView = indicator as! ProgressView
                progressView.annular = true
            }
        }
        else if (mode == .customView && customView != indicator) {
            // Update custom view indicator
            if let customView = customView {
                indicator!.removeFromSuperview()
                indicator = customView
                addSubview(indicator!)
            }
        } else if (mode == .text) {
            indicator!.removeFromSuperview()
            indicator = nil
        }
    }
    
    func _sf_textSize(_ text: String?, font: UIFont) -> CGSize {
        if let text = text , text.characters.count > 0 {
            return text.size(attributes: [NSFontAttributeName : font])
        } else {
            return CGSize.zero
        }
    }
    
    func _sf_mutilLineTextSize(_ text: String?, font: UIFont, maxSize: CGSize) -> CGSize {
        if let text = text , text.characters.count > 0 {
            return text.boundingRect(with: maxSize, options:.usesLineFragmentOrigin, attributes:[NSFontAttributeName : font], context:nil).size
        } else {
            return CGSize.zero
        }
    }
    
    lazy open var label : UILabel = {
        let label = UILabel(frame: self.bounds)
        label.adjustsFontSizeToFitWidth = false
        label.textAlignment = .center
        label.isOpaque = false
        label.backgroundColor = UIColor.clear
        label.font = UIFont.boldSystemFont(ofSize: labelFontSize)
        label.textColor = UIColor.white
        return label
    }()
    
    lazy open var detailsLabel : UILabel = {
        let detailsLabel = UILabel(frame: self.bounds)
        detailsLabel.adjustsFontSizeToFitWidth = false
        detailsLabel.textAlignment = .center
        detailsLabel.isOpaque = false
        detailsLabel.backgroundColor = UIColor.clear
        detailsLabel.font = UIFont.boldSystemFont(ofSize: detailsLabelFontSize)
        detailsLabel.textColor = UIColor.white
        return detailsLabel
    }()
    
    @objc public enum SFProgressHUDMode : NSInteger {
        /** Progress is shown using an UIActivityIndicatorView. This is the default. */
        case indeterminate
        /** Progress is shown using a round, pie-chart like, progress view. */
        case determinate
        /** Progress is shown using a ring-shaped progress view. */
        case annularDeterminate
        /** Shows a custom view */
        case customView
        /** Shows only labels */
        case text
    }
}
