//
//  LMHorizontalTableViewCell.swift
//  Limi
//
//  Created by Richie on 16/5/13.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class LMHorizontalTableViewCell: UIView {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    var reuseIdentifier:String{return _reuseIdentifier}

    
    fileprivate var _reuseIdentifier = ""
    
    init(reuseIdentifier: String) {
        super.init(frame: CGRect.zero)
        self.backgroundColor = UIColor.clear
        
        _reuseIdentifier = reuseIdentifier
    }

    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


class LMHorizontalTableViewExtensionSystemCell: UITableViewCell {

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = UIColor.clear
        self.contentView.backgroundColor = UIColor.clear
        
        self.selectionStyle = UITableViewCellSelectionStyle.none
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func setContent(_ content:LMHorizontalTableViewCell){
        content.isUserInteractionEnabled = false
        
        self.addSubview(content)
        content.tag = 91101

        content.center = CGPoint(x: content.frame.height/2,y: content.frame.width/2)
        content.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI/180.0) * 90)
    }
    
    
    func getHorizontalTableViewCell() ->LMHorizontalTableViewCell?{
        return self.viewWithTag(91101) as? LMHorizontalTableViewCell
    }
}

