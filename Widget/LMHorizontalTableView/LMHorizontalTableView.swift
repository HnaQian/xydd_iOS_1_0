//
//  LMHorizontalTableView.swift
//  Limi
//
//  Created by Richie on 16/5/13.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//


/**
 注：暂不支持使用autoLayout！！！！！
 */

import UIKit

//MARK: dataSource
protocol LMHorizontalTableViewDataSource:NSObjectProtocol {
    
    func numberOfRows(_ horizontalTableView: LMHorizontalTableView) -> Int
    
    func horizontalTableView(_ horizontalTableView: LMHorizontalTableView, cellForRow row: Int ,reuseCell:((_ reuseInfentifer:String) ->LMHorizontalTableViewCell?)) -> LMHorizontalTableViewCell
}

//MARK: delegate
@objc protocol LMHorizontalTableViewDelegate:NSObjectProtocol {

    func horizontalTableView(_ horizontalTableView: LMHorizontalTableView, heightForRow row: Int) -> CGFloat
    
    func horizontalTableView(_ horizontalTableView: LMHorizontalTableView, widthForRow row: Int) -> CGFloat
    
    @objc optional func horizontalTableView(_ horizontalTableView: LMHorizontalTableView, didSelectRow row: Int)
    
    @objc optional func horizontalTableViewDidScroll(_ horizontalTableView: LMHorizontalTableView)
}



class LMHorizontalTableView: UIView {
    
    //public:
    var pagingEnabled = false{didSet{horizontalTableView.isPagingEnabled = pagingEnabled}}
    
    var showsHorizontalScrollIndicator = false{didSet{horizontalTableView.showsVerticalScrollIndicator = showsHorizontalScrollIndicator}}
    
    var showsVerticalScrollIndicator = false{didSet{horizontalTableView.showsHorizontalScrollIndicator = showsVerticalScrollIndicator}}
    
    var contentOffset:CGPoint{return CGPoint(x: horizontalTableView.contentOffset.y, y: horizontalTableView.contentOffset.x)}

    weak var delegate:LMHorizontalTableViewDelegate?

    weak var dataSource:LMHorizontalTableViewDataSource?{didSet{horizontalTableView.reloadData()}}

    //private:
    fileprivate let PI:CGFloat = CGFloat(M_PI/180.0)
    fileprivate let rotationValue:CGFloat = -90
    
    func reloadData(){
        self.horizontalTableView.reloadData()
    }
    
    
    func registerClass(_ cellClass: AnyClass?, forCellReuseIdentifier identifier: String){
        self.horizontalTableView.register(cellClass, forCellReuseIdentifier: identifier);
    }
    
    
    func scrollRectToVisible(_ rect: CGRect, animated: Bool){
        horizontalTableView.scrollRectToVisible(CGRect(x: rect.origin.y, y: rect.origin.x, width: rect.size.height, height: rect.size.width), animated: animated)
    }
    
    /**
     滚动到指定的row
     注：此方法仅适用所有cell的width都相同的tableview
     */
    func scrollToDesignateRow(_ row:Int,animated: Bool){
        if row < horizontalTableView.numberOfRows(inSection: 0){
            
            var width:CGFloat = UIScreen.main.bounds.size.width
            
            if let wid = self.delegate?.horizontalTableView(self, widthForRow: row){
                width = wid
            }
            
            self.scrollRectToVisible(CGRect(x: CGFloat(row) * width, y: 0, width: 1, height: 1), animated: animated)
        }
    }
    
    fileprivate var horizontalTableView:UITableView = {
        let tab = UITableView()

        tab.separatorStyle = UITableViewCellSeparatorStyle.none
        tab.showsHorizontalScrollIndicator = false
        tab.showsVerticalScrollIndicator = false
        
        return tab
    }()
    
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupUI()
    }
    
    
    fileprivate func setupUI(){
        self.backgroundColor = UIColor.clear
        horizontalTableView.backgroundColor = UIColor.clear
        
        self.addSubview(horizontalTableView);
        horizontalTableView.delegate = self
        horizontalTableView.dataSource = self
        
        horizontalTableView.frame = CGRect(x: 0, y: 0, width: self.frame.height, height: self.frame.width)
        horizontalTableView.center = CGPoint( x: self.frame.width/2,y: self.frame.height/2)
        self.horizontalTableView.transform = CGAffineTransform(rotationAngle: self.PI * rotationValue)
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


extension LMHorizontalTableView:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.dataSource != nil{
            return self.dataSource!.numberOfRows(self)
        }
        
        return 0
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if self.delegate != nil{
            return self.delegate!.horizontalTableView(self, widthForRow: (indexPath as NSIndexPath).row)
        }
        
        return UIScreen.main.bounds.size.width
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        if self.dataSource != nil{

            var cell:LMHorizontalTableViewExtensionSystemCell?

            let customeCell = self.dataSource!.horizontalTableView(self, cellForRow: (indexPath as NSIndexPath).row,reuseCell: {reuseIndentifer in
                
                cell = tableView.dequeueReusableCell(withIdentifier: reuseIndentifer) as? LMHorizontalTableViewExtensionSystemCell
                return cell?.getHorizontalTableViewCell()
            })
            
            
            if cell == nil{

                cell = LMHorizontalTableViewExtensionSystemCell(style: UITableViewCellStyle.default, reuseIdentifier: customeCell.reuseIdentifier)
                
                if self.delegate != nil{
                    customeCell.frame = CGRect(x: 0, y: 0 ,width: self.delegate!.horizontalTableView(self, widthForRow: (indexPath as NSIndexPath).row),height: self.delegate!.horizontalTableView(self, heightForRow: (indexPath as NSIndexPath).row))
                }
                
                cell?.setContent(customeCell)
            }
            
            return cell!
        }
        
        return UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "")
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.horizontalTableView?(self, didSelectRow: (indexPath as NSIndexPath).row)

//        self.scrollRectToVisible(CGRect(x: self.frame.width * 3,y: 0, width: self.frame.width,height: self.frame.height), animated: true)
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.delegate?.horizontalTableViewDidScroll?(self)
    }
}

