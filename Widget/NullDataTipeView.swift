//
//  NullDataTipeView.swift
//  Limi
//
//  Created by guo chen on 15/12/31.
//  Copyright © 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class NullDataTipeView: UIView {
    
    class func newNullDataTipeView(_ imageName: String = "search_biaoQing", title: String = "报告大王，攻略被妖精抓走了!") -> NullDataTipeView
    {
        let nullView = NullDataTipeView()
        
        nullView.imageName = imageName
        nullView.title = title
        
        return nullView
    }
    
    var imageName: String? {
        didSet{
            imageView.image = UIImage(named: imageName!)
        }
    }
    var title: String? {
        didSet{
            titleLabel.text = title!
        }
    }
    
    var imageView = UIImageView()
    var titleLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        imageView.clipsToBounds = true
        imageView.contentMode = UIViewContentMode.scaleAspectFill
        
        titleLabel.font = UIFont.systemFont(ofSize: Constant.common_F5_font)
        titleLabel.textColor = UIColor(rgba: Constant.common_C6_color)
        titleLabel.textAlignment = NSTextAlignment.center
        
        self.addSubview(imageView)
        self.addSubview(titleLabel)
        imageView.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(self).offset(60)
            let _ = make.centerX.equalTo(self)
            let _ = make.width.equalTo(100)
            let _ = make.height.equalTo(100)
        }

        
        titleLabel.snp_makeConstraints { (make) -> Void in
            let _ = make.top.equalTo(imageView.snp_bottom)
            let _ = make.centerX.equalTo(self)
            let _ = make.width.equalTo(self)
            let _ = make.height.equalTo(40)
        }
        
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
