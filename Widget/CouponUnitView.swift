//
//  CouponUnitView.swift
//  Limi
//
//  Created by Richie on 16/3/11.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class CouponUnitView: UnitView,CouponRequestManagerDelegate,ChooseCouponViewControllerDelegate {
    
    fileprivate var _couponVC:ChooseCouponViewController!//优惠券
    
    fileprivate var couponunitView_delegate:CouponUnitViewDelegate?{return (self.delegate as? CouponUnitViewDelegate)}
    
    fileprivate var taskId = 0
    
    fileprivate var numberLbl = UILabel()
    
    fileprivate var couponList = [[String:AnyObject]]()
    
    fileprivate var selectedIndex = 10086
    
    fileprivate var setselectedCouponModel:CouponModel?
    
    fileprivate let couponLabel:UILabel = {
        let lbl = UILabel()
        lbl.font = Constant.Theme.Font_16
        lbl.textColor = UIColor(rgba: Constant.common_C1_color)
        lbl.textAlignment = .center
        lbl.isHidden = true
        return lbl
    }()
    
    fileprivate let couponImage:UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "orderCoupon")
        image.isHidden = true
        return image
    }()
    
    var couponModel:CouponModel?{return _couponModel}
    
    fileprivate var goodsData = [GoodsGenericModel]()
    
    fileprivate var _couponModel:CouponModel?{
        didSet{
            
            if let model = _couponModel{
                numberLbl.isHidden = true
                self.couponImage.isHidden = false
                self.couponLabel.isHidden = false
                self.couponLabel.text = Utils.checkDecimal(model.par_value)
//                self.content =  model.name + " " + Utils.checkDecimal(model.par_value) + "元"
                
               let _ = Delegate_Selector(couponunitView_delegate, #selector(CouponUnitViewDelegate.couponUnitViewDidSelectedCoupon(_:))){Void in self.couponunitView_delegate!.couponUnitViewDidSelectedCoupon!(self._couponModel!)}
            }
        }
    }
    
    
    fileprivate var couponVC:ChooseCouponViewController!{
        if _couponVC == nil{
            _couponVC = ChooseCouponViewController()
            _couponVC.delegate  = self
        }
        return _couponVC
    }
    
    
    override init(_ userInteractionEnabled: Bool!, title: String) {
        super.init(userInteractionEnabled, title: title)
    }
    
    
    func getUseableCoupon(_ goods_ids:[Int],totalMoney:Double,status:Int){}
    
    
    func prepareData(_ goodsDataList:[GoodsGenericModel])
    {
        self.goodsData = goodsDataList
        if UserInfoManager.didLogin == false{
            self.setTipeWithCouponCount(0)
            return
        }
        
        if self.isUserInteractionEnabled{
            var ids = [Int]()
            var goods_sub_ids = [Int]()
            var goods_num = [Int]()
            
            for goods in goodsDataList{
                
                ids.append(goods.goodsId!)
                
                if let subModel = goods.selelctedGoodsSubModel,
                    let model = subModel{
                    goods_sub_ids.append(model.id)
                    
                }else{
                    goods_sub_ids.append(0)
                }
                
                goods_num.append(goods.selectedGoodsSubModelCount)
            }
            
            self.loadCouponData(ids, goods_sub_id: goods_sub_ids, goods_num: goods_num, status: 1)
        }
    }
    
    
    func setTipeWithCouponCount(_ count:Int){
        
        self.couponImage.isHidden = true
        self.couponLabel.isHidden = true
        if count == 0
        {
            self.content = "无可用红包"
            self.contentLbl.textColor = UIColor(rgba: Constant.common_C8_color)
            numberLbl.isHidden = true
            self.detailIcon.isHidden = true
            self.couponList = [[String:AnyObject]]()
        }else{
            self.detailIcon.isHidden = false
            numberLbl.text = "\(count)"
            self.contentLbl.textColor = UIColor(rgba: Constant.common_C2_color)
            numberLbl.isHidden = false
            self.content = "可用红包"

            //todo 注释掉帮用户选择红包的逻辑
//            //如果有默认的选择默认的
//            if setselectedCouponModel != nil{
//                self.couponVC.setSelectedCoupon(setselectedCouponModel!)
//            }else{
//                //如果没有默认的筛选默认的
//                findDefaultCoupon()
//            }
        }
        self.couponVC.rereLoadData(couponList,goodsData:self.goodsData)
    }
    
    //筛选默认的红包
    fileprivate  func findDefaultCoupon(){
        
        if couponList.count == 0{return}
        
        //找出ID最小的
        var minModelID:CouponModel!
        //找出最大的金额
        var maxModelDisCount:CouponModel!
        //找出即将过期的
        var minModelInterval:CouponModel!
        
        //所有的折扣价是否相同，默认相同
        var allDisCountSame = true
        //所有的过期时间是否相同，默认相同
        var allDisDateSame = true
        
        var modelAry = [CouponModel]()
        let _ = couponList.map{modelAry.append(CouponModel($0))}
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        for index in 0..<modelAry.count{
            let model = modelAry[index]
            model.rowIndex = index
            
            //如果优惠价格不等于0 切不等于当前model的优惠价格，那么所有的红包中的优惠价格肯定不相同
            if maxModelDisCount != nil{
                if maxModelDisCount.discount != model.discount{
                    allDisCountSame = false
                    
                    if maxModelDisCount.discount < model.discount{
                        maxModelDisCount = model
                    }
                }
            }
            
            
            //找出即将过期的
            if minModelInterval != nil{
                if minModelInterval.end_time != model.end_time{
                    allDisDateSame = false
                    
                    if let curData = dateFormatter.date(from: model.end_time),
                        let lastData = dateFormatter.date(from: minModelInterval.end_time){
                        //如果当前的红包过期时间小于上次红包的过期时间
                        if curData.compare(lastData) == .orderedAscending{
                            minModelInterval = model
                        }
                    }
                }
            }
            
            //找出最小ID
            if minModelID != nil{
                if minModelID.id < model.id{
                    minModelID = model
                }
            }
            
            
            //初始化
            if index == 0{
                maxModelDisCount = model
                minModelInterval = model
                minModelID       = model
            }
        }
        
        
        if allDisCountSame{
            if allDisDateSame{
                chooseCouponDidSelected(minModelInterval.rowIndex)
            }else{
                chooseCouponDidSelected(minModelID.rowIndex)
            }
        }else{
            chooseCouponDidSelected(maxModelDisCount.rowIndex)
        }
    }
    
    //设置选择的红包
    func setSelectedCoupon(_ coupon:CouponModel?){
        setselectedCouponModel = coupon
    }
    
    func couponRequestManagerDidLoadUseableListFinish(_ requestManager: LMRequestManager, result: LMResultMode, couponList: [[String : AnyObject]])
    {
        /*
         //如果没有用taskID，那么taskID为0，如果用了taskID，那么taskID一定要等于最后一次的请求
         这里面请求充值优惠券的时候用了taskID，而且要求使用最后一次的数据为准
         */
        if taskId == 0 || taskId == requestManager.taskID{
            self.couponList = couponList
            self.setTipeWithCouponCount(couponList.count)
        }
    }
    
    //心意充值优惠券
    func loadChargeCouponData(_ mobile:String,chargeId:Int){
        
        taskId = CouponRequestManager.shareInstance(self).chargeCouponAvailableOfOrder(self, mobile: mobile, chargeId: chargeId, isShowErrorStatuMsg: false, isNeedHud: false).taskID()
    }
    
    
    //商品优惠券
    func loadCouponData(_ goods_id: [Int], goods_sub_id: [Int] = [], goods_num: [Int], status: Int){
        
       let _ = CouponRequestManager.shareInstance(self).couponAvailableOfOrder(self,goods_id: goods_id, goods_sub_id: goods_sub_id, goods_num: goods_num, status: status, isShowErrorStatuMsg: false, isNeedHud: false)
    }
    
    
    
    func chooseCouponDidSelected(_ index: Int) {
        selectedIndex = index
        self.setContentLblAttrubutes(color:UIColor(rgba: Constant.common_C2_color))
        _couponModel = CouponModel(self.couponList[index])
    }
    
    func chooseCouponCancel() {
        self.couponImage.isHidden = true
        self.couponLabel.isHidden = true
        selectedIndex = 10086
        if self.couponList.count == 0
        {
            self.content = "无可用红包"
            self.contentLbl.textColor = UIColor(rgba: Constant.common_C8_color)
            numberLbl.isHidden = true
            self.detailIcon.isHidden = true
            self.couponList = [[String:AnyObject]]()
        }else{
            self.detailIcon.isHidden = false
            numberLbl.text = "\(self.couponList.count)"
            self.contentLbl.textColor = UIColor(rgba: Constant.common_C2_color)
            numberLbl.isHidden = false
            self.content = "可用红包"
        }
    }
    
    func reloadCouponData(data:[[String:AnyObject]]) {
        self.couponList = data
        self.setTipeWithCouponCount(couponList.count)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        super.touchesBegan(touches, with: event)
        
        self.viewController?.navigationController?.pushViewController(couponVC, animated: true)
//        if self.couponList.count > 0{
//            self.viewController?.navigationController?.pushViewController(couponVC, animated: true)}
    }
    
    
    
    override func setupUI() {
        super.setupUI()
        
        self.addSubview(numberLbl)
        self.addSubview(couponImage)
        self.addSubview(couponLabel)
        numberLbl.isHidden = true
        numberLbl.backgroundColor = UIColor(rgba: Constant.common_red_color)
        numberLbl.font = UIFont.systemFont(ofSize: Constant.common_F5_font)
        numberLbl.textColor = UIColor(rgba: Constant.common_C12_color)
        numberLbl.textAlignment  = NSTextAlignment.center
        numberLbl.layer.cornerRadius = 10
        
        numberLbl.clipsToBounds = true
        
        numberLbl.snp_makeConstraints { (make) -> Void in
            let _ = make.centerY.equalTo(self)
            let _ = make.right.equalTo(self.contentLbl.snp_right).offset(-MARGIN_49 - MARGIN_13 * 2 + 5)
            let _ = make.height.equalTo(20)
            let _ = make.width.equalTo(20)
        }

        couponImage.snp_makeConstraints { (make) in
            let _ = make.centerY.equalTo(self)
            let _ = make.right.equalTo(self.contentLbl.snp_right)
            let _ = make.width.equalTo(160 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
            let _ = make.height.equalTo(60 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
        }
        
        couponLabel.snp_makeConstraints { (make) in
            let _ = make.centerY.equalTo(self)
            let _ = make.right.equalTo(self.contentLbl.snp_right)
            let _ = make.width.equalTo(160 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
            let _ = make.height.equalTo(60 * Constant.ScreenSizeV2.SCREEN_WIDTH_SCALE)
        }
    }
    
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}



@objc protocol CouponUnitViewDelegate:OrderUnitViewDelegate{
    
    @objc optional func couponUnitViewDidSelectedCoupon(_ coupon:CouponModel)
    
}
