//
//  MenuItemView.swift
//  PagingMenuController
//
//  Created by Yusuke Kita on 5/9/15.
//  Copyright (c) 2015 kitasuke. All rights reserved.
//

import UIKit

open class MenuItemView: UIView {
    
    open let titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.textAlignment = .center
        label.isUserInteractionEnabled = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    fileprivate var options: PagingMenuOptions!
    fileprivate var widthLabelConstraint: NSLayoutConstraint!
    
    // MARK: - Lifecycle
    
    internal init(title: String, options: PagingMenuOptions) {
        super.init(frame: .zero)
        
        self.options = options
        
        setupView()
        setupLabel(title: title)
        layoutLabel()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    // MARK: - Cleanup
    
    internal func cleanup() {
        titleLabel.removeFromSuperview()
    }
    
    // MARK: - Constraints manager
    
    internal func updateLabelConstraints(size: CGSize) {
        // set width manually to support ratotaion
        if case .segmentedControl = options.menuDisplayMode {
            let labelSize = calculateLableSize(size: size)
            widthLabelConstraint.constant = labelSize.width
        }
    }
    
    // MARK: - Label changer
    
    internal func focusLabel(selected: Bool) {
        if case .roundRect = options.menuItemMode {
            backgroundColor = UIColor.clear
        } else {
            backgroundColor = selected ? options.selectedBackgroundColor : options.backgroundColor
        }
        titleLabel.textColor = selected ? options.selectedTextColor : options.textColor
        titleLabel.font = selected ? options.selectedFont : options.font

        // adjust label width if needed
        let labelSize = calculateLableSize()
        widthLabelConstraint.constant = labelSize.width
    }
    
    // MARK: - Constructor
    
    fileprivate func setupView() {
        if case .roundRect = options.menuItemMode {
            backgroundColor = UIColor.clear
        } else {
            backgroundColor = options.backgroundColor
        }
        translatesAutoresizingMaskIntoConstraints = false
    }
    
    fileprivate func setupLabel(title: String) {
        titleLabel.text = title
        titleLabel.textColor = options.textColor
        titleLabel.font = options.font
        addSubview(titleLabel)
    }
    
    fileprivate func layoutLabel() {
        let viewsDictionary = ["label": titleLabel]
        
        let labelSize = calculateLableSize()

        let horizontalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[label]|", options: [], metrics: nil, views: viewsDictionary)
        let verticalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|[label]|", options: [], metrics: nil, views: viewsDictionary)
        
        NSLayoutConstraint.activate(horizontalConstraints + verticalConstraints)
        
        widthLabelConstraint = NSLayoutConstraint(item: titleLabel, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1.0, constant: labelSize.width)
        widthLabelConstraint.isActive = true
    }
    
    // MARK: - Size calculator
    
    fileprivate func calculateLableSize(size: CGSize = UIApplication.shared.keyWindow!.bounds.size) -> CGSize {
        guard let text = titleLabel.text else { return .zero }
        
        let labelSize = NSString(string: text).boundingRect(with: CGSize(width: CGFloat.greatestFiniteMagnitude, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: titleLabel.font], context: nil).size

        let itemWidth: CGFloat
        switch options.menuDisplayMode {
        case let .standard(widthMode, _, _):
            itemWidth = labelWidth(size: labelSize, widthMode: widthMode)
        case .segmentedControl:
            itemWidth = size.width / CGFloat(options.menuItemCount)
        case let .infinite(widthMode):
            itemWidth = labelWidth(size: labelSize, widthMode: widthMode)
        }
        
        let itemHeight = floor(labelSize.height)
        return CGSize(width: itemWidth + calculateHorizontalMargin() * 2, height: itemHeight)
    }
    
    fileprivate func labelWidth(size: CGSize, widthMode: PagingMenuOptions.MenuItemWidthMode) -> CGFloat {
        switch widthMode {
        case .flexible: return ceil(size.width)
        case let .fixed(width): return width
        }
    }
    
    fileprivate func calculateHorizontalMargin() -> CGFloat {
        if case .segmentedControl = options.menuDisplayMode {
            return 0.0
        }
        return options.menuItemMargin
    }
}
