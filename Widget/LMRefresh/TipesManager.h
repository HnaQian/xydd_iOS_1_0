//
//  TipesManager.h
//  Limi
//
//  Created by guo chen on 16/1/6.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

#import "UIScrollView+LMloadtipe.h"

#define LOADALL_TIPE @"已显示全部内容"

#define LOADALL_TIPE_ICON @"nullDataTipeIcon"

#define LOSTNETWORK_TIPE @"数据木有加载粗来!"

#define LOSTNETWORK_TIPE_ICON @"tipeIcon_hard"

#define NODTATA_TIPE @"暂时还没有数据!"

#define NODTATA_TIPE_ICON @"nullDataTipeIcon"

#define TIPE_HEIGHT ([UIScreen mainScreen].bounds.size.height * 0.3)

#define TIPE_WIDTH ([UIScreen mainScreen].bounds.size.width)

//[UIScreen mainScreen].bounds.size.width
@interface TipesManager : NSObject

+(TipesManager *)newManager:(UIScrollView *)scrollView;

///根据传进来的tipeMode显示相应的模式
///注：当模式为：Custom ，需要设置提示的内容和图标，默认两者都是为nil
-(TipesManager *)showTipeWithTipeMode:(UIScrollTipeMode)currentMode;

-(TipesManager *)closeTipeWithTipeMode:(UIScrollTipeMode)currentMode;

///设置提示文字
-(TipesManager *)tipeContent:(NSString *)content;

///设置提示图标
-(TipesManager *)tipeIcon:(NSString *)iconName;


@end
