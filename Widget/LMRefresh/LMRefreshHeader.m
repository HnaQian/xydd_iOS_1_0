//
//  LMRefreshHeader.m
//  Limi
//
//  Created by guo chen on 16/1/5.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

#import "LMRefreshHeader.h"

#import "UIScrollView+LMloadtipe.h"

@interface LMRefreshHeader()

@property (nonatomic,readonly)CGFloat orgHeight;

@end


@implementation LMRefreshHeader


-(CGFloat)orgHeight
{
    return self.bounds.size.height;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */
+(instancetype)headerWithRefreshingBlock:(MJRefreshComponentRefreshingBlock)refreshingBlock
{
    LMRefreshHeader *refresh = [super headerWithRefreshingBlock:refreshingBlock];

    return refresh;
}


-(void)beginRefreshing
{
    [super beginRefreshing];
    
    UIView *tempView = [self.scrollView.subviews lastObject];
    
    if (tempView.tag == 9111 || tempView.tag == 9112 || tempView.tag == 9113)
    {
        [UIView animateWithDuration:0.25 animations:^{
            
            tempView.alpha = 0;
        }];
    }
}



-(void)endRefreshing
{
    if (self.isRefreshing)
    {
        [super endRefreshing];
        
        if (self.scrollView.mj_footer != NULL)
        {
            UIView *item = [self.scrollView.mj_footer.subviews lastObject];
            
            //9110的视图是“已加载全部内容”的视图
            if (item.tag == 9110)
            {
                item.alpha = 0;
            }
            
            self.scrollView.mj_footer.alpha = 1;
            
            [self.scrollView.mj_footer resetNoMoreData];
        }
    }
    
}




@end
