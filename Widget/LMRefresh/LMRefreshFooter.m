//
//  LMRefreshFooter.m
//  Limi
//
//  Created by guo chen on 16/1/5.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

#import "LMRefreshFooter.h"

@interface LMRefreshFooter()

@property (nonatomic,readonly)CGFloat orgHeight;

@end

@implementation LMRefreshFooter

-(CGFloat)orgHeight
{
    return self.bounds.size.height;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/



@end
