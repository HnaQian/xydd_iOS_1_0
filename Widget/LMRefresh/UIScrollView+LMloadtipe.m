//
//  UIScrollView+LMloadtipe.m
//  Limi
//
//  Created by guo chen on 16/1/5.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

#import "UIScrollView+LMloadtipe.h"

#import "TipesManager.h"

#import "UIView+RQLayout.h"

@implementation UIScrollView (LMloadtipe)


-(LMRefreshHeader *)lm_header
{
    return (LMRefreshHeader *)self.mj_header;
}


-(LMRefreshFooter *)lm_footer
{
    return (LMRefreshFooter *)self.mj_footer;
}

-(TipesManager *)manager
{
    return [TipesManager newManager:self];
}

-(void)setTipeImage:(NSString *)tipeImage andTipeContent:(NSString *)tipeContent andForTipeType:(UIScrollTipeMode)tipeMode{}


-(TipesManager *)showTipe:(UIScrollTipeMode)tipeMode{
    
    return [self.manager showTipeWithTipeMode:tipeMode];
}

-(TipesManager *)closeTipe:(UIScrollTipeMode)tipeMode
{
    return [self.manager closeTipeWithTipeMode:tipeMode];
}



-(void)addHeaderRefresh:(MJRefreshComponentRefreshingBlock) freshBlock{
    
    self.mj_header = [LMRefreshHeader headerWithRefreshingBlock:freshBlock];
    
}


-(void)addFooterRefresh:(MJRefreshComponentRefreshingBlock) freshBlock
{
    self.mj_footer = [LMRefreshFooter footerWithRefreshingBlock:freshBlock];

    self.mj_footer.hidden = true;
}



-(BOOL)isHeaderRefreshing
{
    if (self.mj_header != NULL)
    {
        return self.mj_header.isRefreshing;
    }
    
    return false;
}


-(BOOL)isFooterRefreshing
{
    if (self.mj_footer != NULL)
    {
        return self.mj_footer.isRefreshing;
    }
    
    return false;
}



//start load refresh operation
-(void)beginHeaderRefresh
{
    if ( self.mj_header != NULL && !self.isFooterRefreshing)
    {
        if (!self.isHeaderRefreshing) {
            
            [self.mj_header beginRefreshing];
        }
    }
}


//end load more operation
-(void)endHeaderRefresh
{
    if ( self.mj_header != NULL)
    {
        [self.mj_header endRefreshing];
    }
}



//start load more operation
-(void)beginFooterRefresh
{
    if ( self.mj_footer != NULL && !self.isHeaderRefreshing)
    {
        if (!self.isFooterRefreshing) {
            
            [self.mj_footer beginRefreshing];
        }
    }
}


//end load more operation
-(void)endFooterRefresh
{
    if ( self.mj_footer != NULL)
    {
        [self.mj_footer endRefreshing];
    }
}


- (void)removeHeaderRefresh
{
    if (self.mj_header != NULL)
    {
        self.mj_header = NULL;
    }

}


- (void)removeFooterRefresh
{
    if (self.mj_footer != NULL)
    {
        self.mj_footer.iheight = 0;
        
        [self.mj_footer removeFromSuperview];
        
        self.mj_footer = NULL;
    }
}



- (void)showFooterRefresh
{
    if (self.mj_footer != NULL)
    {
        [self.mj_footer resetNoMoreData];
        
        self.mj_footer.alpha = 1;
    }
}


-(void)closeAllRefresh
{
    if ( self.mj_header != NULL)
    {
        [self.mj_header endRefreshing];
    }
    
    if ( self.mj_footer != NULL)
    {
        [self.mj_footer endRefreshing];
    }
}


@end
