//
//  UIScrollView+LMloadtipe.h
//  Limi
//
//  Created by guo chen on 16/1/5.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "MJRefreshConst.h"

#import "LMRefreshFooter.h"

#import "LMRefreshHeader.h"

@class TipesManager;

typedef NS_ENUM(NSInteger, UIScrollTipeMode) {
    UIScrollTipeModeNullData,          // null data
    UIScrollTipeModeNoMuchMore,          // no much more data
    UIScrollTipeModeLostConnect,         // lost network connect
    UIScrollTipeModeCustom         // lost network connect
};

@interface UIScrollView (LMloadtipe)

@property (nonatomic,readonly)BOOL isHeaderRefreshing;

@property (nonatomic,readonly)BOOL isFooterRefreshing;

@property (nonatomic,retain,readonly)TipesManager *manager;

@property (nonatomic,readonly)LMRefreshFooter *lm_footer;

@property (nonatomic,readonly)LMRefreshHeader *lm_header;

///* set tipeView's tipe content,tipe icon for specified tipe mode */
-(void)setTipeImage:(NSString *)tipeImage andTipeContent:(NSString *)tipeContent andForTipeType:(UIScrollTipeMode)tipeMode;

///显示提示语
/**
 *tipeMode      :选择显示的提示模式
 */
///注：当 tipeMode = UIScrollTipeMode.Custom 时 tipeContent和tipeIcon都需要设置
-(TipesManager *)showTipe:(UIScrollTipeMode)tipeMode;

///隐藏提示语
/**
 *tipeMode      :选择隐藏的提示模式
 */
-(TipesManager *)closeTipe:(UIScrollTipeMode)tipeMode;


///添加heander 下拉刷新功能
/**
 *freshBlock     :刷新动作回调
 */
-(void)addHeaderRefresh:(MJRefreshComponentRefreshingBlock) freshBlock;


///添加footer 上拉加载功能
/**
 *freshBlock     :加载动作回调
 */
-(void)addFooterRefresh:(MJRefreshComponentRefreshingBlock) freshBlock;

///启动“下拉刷新”操作
-(void)beginHeaderRefresh;

///启动“上拉加载更多”操作
-(void)beginFooterRefresh;

///结束“下拉刷新”操作
-(void)endHeaderRefresh;

///结束“上拉加载”操作
-(void)endFooterRefresh;

///结束所有刷新动作
-(void)closeAllRefresh;

///移除“下拉刷新”
- (void)removeHeaderRefresh;

///移除“上拉加载”
- (void)removeFooterRefresh;

@end
