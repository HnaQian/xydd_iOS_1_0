//
//  TipesManager.m
//  Limi
//
//  Created by guo chen on 16/1/6.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

#import "TipesManager.h"

#import "UIView+RQLayout.h"

#import "LMRefreshFooter.h"

#import "LMRefreshHeader.h"


@interface TipesManager()

///已经加载完所有数据的tipe  tag:9110
@property (nonatomic,retain)UIView *nomuchMoreDataTipeView;

///自定义tipe  tag:9113
@property (nonatomic,retain)UIView *customView;

///没有数据的提示 tag:9111
@property (nonatomic,retain)UIView *nullDataTipeView;

///没有网络提示 tag:9112
@property (nonatomic,retain)UIView *lostNetworkConnectTipeView;

///当前选择的模式
@property (nonatomic,assign)UIScrollTipeMode currentMode;

@property (nonatomic,strong)UIScrollView *scrollView;

@end


@implementation TipesManager

static TipesManager *manager;


+(TipesManager *)newManager:(UIScrollView *)scrollView
{
    if (manager == NULL)
    {
        manager = [TipesManager new];
    }
    
    if (manager.scrollView != NULL) {
        
        if ([manager.scrollView isEqual:scrollView])
        {
            return manager;
        }else
        {
            manager = [TipesManager new];
            
            manager.scrollView = scrollView;
            
            return manager;
        }
        
    }else
    {
        manager.scrollView = scrollView;
        
        return manager;
    }
}


-(TipesManager *)closeTipeWithTipeMode:(UIScrollTipeMode)currentMode
{
    self.currentMode = currentMode;
    
    switch (currentMode)
    {
        case UIScrollTipeModeNoMuchMore:

            
            if (self.nomuchMoreDataTipeView != NULL) {
                
                self.nomuchMoreDataTipeView.alpha = 0;
            }
            
            break;
            
            
            
        case UIScrollTipeModeNullData:
            
            [self removeSubView:9111];
//            
//            if (self.nullDataTipeView != NULL) {
//                
//
//                
//                self.nullDataTipeView.alpha = 0;
//            }
            
            break;
            
            
            
        case UIScrollTipeModeLostConnect:
            
            if (self.lostNetworkConnectTipeView != NULL) {
                
                self.lostNetworkConnectTipeView.alpha = 0;
            }

            
            break;
            
        case UIScrollTipeModeCustom:
            
            
            if (self.customView != NULL) {
                
                self.customView.alpha = 0;
            }
            
            break;
            
        default:
            break;
    }
    
    return self;
}


- (void)removeSubView:(int)tag{
    
    NSMutableArray *views = [NSMutableArray new];
    
    for (UIView * view in self.scrollView.subviews) {
        if (view.tag == tag){
            [views addObject:view];
        }
    }
    
    if (views.count > 0){
        for (UIView *view in views){
            [view removeFromSuperview];
        }
    }
}



#pragma mark - 显示提示内容根据：UIScrollTipeMode
-(TipesManager *)showTipeWithTipeMode:(UIScrollTipeMode)currentMode{
    
    self.currentMode = currentMode;
    
    switch (currentMode)
    {
        case UIScrollTipeModeNoMuchMore:
            
            [self setupUIOfNomuchMoreDataTipeView];
            
            break;
            
            
            
        case UIScrollTipeModeNullData:
            
            [self setupUIOfNullDataTipeView];
            
            break;
            
            
            
        case UIScrollTipeModeLostConnect:
            
            [self setupUIOfLostConnectTipeView];
            
            break;
            
        case UIScrollTipeModeCustom:

            [self setupUIOfCustomView];

            break;
            
        default:
            break;
    }
    
    return self;
}





#pragma mark - 设置tipe的提示文字
-(TipesManager *)tipeContent:(NSString *)content{
    
    UILabel *contentLabel;
    
    switch (self.currentMode)
    {
        case UIScrollTipeModeNoMuchMore:
            
            contentLabel = [self.nomuchMoreDataTipeView viewWithTag:101];
            
            if (contentLabel != NULL) {
                
                CGSize size = [content sizeWithAttributes:@{ NSFontAttributeName : contentLabel.font }];
                
                contentLabel.iwidth = size.width + 8;
                
                contentLabel.icenterX = self.nomuchMoreDataTipeView.iwidth/2;
            }
            
            
            break;
            
            
        case UIScrollTipeModeNullData:
            
            contentLabel = [self.nullDataTipeView viewWithTag:101];

            break;
            
            
        case UIScrollTipeModeLostConnect:
            
            contentLabel = [self.lostNetworkConnectTipeView viewWithTag:101];
            
            break;
            
        case UIScrollTipeModeCustom:
            
            contentLabel = [self.customView viewWithTag:101];
            
            break;
            
        default:
            break;
    }
    
    if (contentLabel != NULL) {
        
        contentLabel.text = content;

    }
    
    return self;
}





#pragma mark - 设置tipe的提示图标
-(TipesManager *)tipeIcon:(NSString *)iconName{
    
    UIImageView *iconImage;
    
    switch (self.currentMode)
    {
        case UIScrollTipeModeNoMuchMore:
            
            break;
            
        case UIScrollTipeModeNullData:
            
            iconImage = [self.nullDataTipeView viewWithTag:102];
            
            break;
            
            
        case UIScrollTipeModeLostConnect:
            
            iconImage = [self.lostNetworkConnectTipeView viewWithTag:102];
            
            break;

        case UIScrollTipeModeCustom:
            
            iconImage = [self.customView viewWithTag:102];
            
            break;
            
        default:
            break;
    }
    
    
    if (iconImage != NULL)
    {
        if (iconName == NULL || iconName.length == 0)
        {
            iconImage.image = NULL;
        }else
        {
            iconImage.image = [UIImage imageNamed:iconName];
        }
    }
    
    
    return self;
}





#pragma mark - 无数据tipe
-(void)setupUIOfNullDataTipeView
{
    if (self.nullDataTipeView == NULL)
    {
        self.nullDataTipeView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, TIPE_WIDTH, TIPE_HEIGHT)];
        
        self.nullDataTipeView.alpha = 0;
        
        self.nullDataTipeView.iwidth = [UIScreen mainScreen].bounds.size.width;
        
        self.nullDataTipeView.tag = 9111;
        
        UIImageView *icon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 70, 70)];
        
        icon.contentMode = UIViewContentModeScaleAspectFill;
        
        icon.tag = 102;
        
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, self.nullDataTipeView.iheight - 30, self.nullDataTipeView.iwidth, 30)];
        
        icon.ibottom = label.itop;
        
        icon.icenterX = self.nullDataTipeView.iwidth/2;
        
        label.tag = 101;
        
        label.textAlignment = NSTextAlignmentCenter;
        
        label.textColor = [UIColor lightGrayColor];
        
        label.font = [UIFont systemFontOfSize:15];
        
        label.text = NODTATA_TIPE;
        
        icon.image = [UIImage imageNamed:NODTATA_TIPE_ICON];
        
        [self.nullDataTipeView addSubview:label];
        
        [self.nullDataTipeView addSubview:icon];
    }
    
    
    //如果当前的scrollView的子视图里面有self.nullDataTipeView
    if ([self.scrollView.subviews containsObject:self.nullDataTipeView]) {

        //如果self.nullDataTipeView不在当前scrollview的最上层
        if (![[self.scrollView.subviews lastObject] isEqual:self.nullDataTipeView])
        {
            [self.scrollView bringSubviewToFront:self.nullDataTipeView];
        }
    }else
    {
        //如果当前的scrollView的子视图里面没有有self.nullDataTipeView，把self.nullDataTipeView放在scrollview的最上层
        [self.scrollView insertSubview:self.nullDataTipeView aboveSubview:[self.scrollView.subviews lastObject]];
    }
    
    [UIView animateWithDuration:0.25 animations:^{
        
        self.nullDataTipeView.alpha = 1;
    }];
}






#pragma mark - 无网络tipe
-(void)setupUIOfLostConnectTipeView{
    
    if (self.lostNetworkConnectTipeView == NULL)
    {
        self.lostNetworkConnectTipeView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, TIPE_WIDTH, TIPE_HEIGHT)];
        
        self.lostNetworkConnectTipeView.alpha = 0;
        
        self.lostNetworkConnectTipeView.iwidth = [UIScreen mainScreen].bounds.size.width;
        
        self.lostNetworkConnectTipeView.tag = 9112;
        
        UIImageView *icon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
        
        icon.contentMode = UIViewContentModeScaleAspectFill;
        
        icon.tag = 102;

        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, self.nullDataTipeView.iheight - 30, self.nullDataTipeView.iwidth, 30)];
        
        icon.ibottom = label.itop;
        
        icon.icenterX = self.nullDataTipeView.iwidth/2;
        
        label.tag = 101;
        
        label.textAlignment = NSTextAlignmentCenter;
        
        label.textColor = [UIColor lightGrayColor];
        
        label.font = [UIFont systemFontOfSize:15];
        
        label.text = LOSTNETWORK_TIPE;
        
        icon.image = [UIImage imageNamed:LOSTNETWORK_TIPE_ICON];
        
        [self.lostNetworkConnectTipeView addSubview:label];
        
        [self.lostNetworkConnectTipeView addSubview:icon];
    }
    
    
    //    //如果当前的scrollView的子视图里面有self.nullDataTipeView
    if ([self.scrollView.subviews containsObject:self.lostNetworkConnectTipeView]) {
        
        //如果self.nullDataTipeView不在当前scrollview的最上层
        if (![[self.scrollView.subviews lastObject] isEqual:self.lostNetworkConnectTipeView])
        {
            [self.scrollView bringSubviewToFront:self.lostNetworkConnectTipeView];
        }
        
    }else
    {
        //如果当前的scrollView的子视图里面没有有self.nullDataTipeView，把self.nullDataTipeView放在scrollview的最上层
        [self.scrollView insertSubview:self.lostNetworkConnectTipeView aboveSubview:[self.scrollView.subviews lastObject]];
    }
    
    [UIView animateWithDuration:0.25 animations:^{
        
        self.lostNetworkConnectTipeView.alpha = 1;
    }];
    
}





#pragma mark - 自定义tipe 注：自定义的tipe的提示内容和图标默认为nil
-(void)setupUIOfCustomView
{
    if (self.customView == NULL)
    {
        self.customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, TIPE_WIDTH, TIPE_HEIGHT)];
        
        self.customView.alpha = 0;
        
        self.customView.iwidth = [UIScreen mainScreen].bounds.size.width;
        
        self.customView.tag = 9113;
        
        UIImageView *icon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
        
        icon.contentMode = UIViewContentModeScaleAspectFill;
        
        icon.tag = 102;
        
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, self.nullDataTipeView.iheight - 30, self.nullDataTipeView.iwidth, 30)];
        
        icon.ibottom = label.itop;
        
        icon.icenterX = self.nullDataTipeView.iwidth/2;
        
        label.tag = 101;
        
        label.textAlignment = NSTextAlignmentCenter;
        
        [self.customView addSubview:label];
        
        [self.customView addSubview:icon];
    }
    
    //    //如果当前的scrollView的子视图里面有self.nullDataTipeView
    if ([self.scrollView.subviews containsObject:self.customView]) {
        
        //如果self.nullDataTipeView不在当前scrollview的最上层
        if (![[self.scrollView.subviews lastObject] isEqual:self.customView])
        {
            [self.scrollView bringSubviewToFront:self.customView];
        }

    }else
    {
        //如果当前的scrollView的子视图里面没有有self.nullDataTipeView，把self.nullDataTipeView放在scrollview的最上层
        [self.scrollView insertSubview:self.customView aboveSubview:[self.scrollView.subviews lastObject]];
    }
    
    [UIView animateWithDuration:0.25 animations:^{
        
        self.customView.alpha = 1;
    }];
}





#pragma mark - 所有数据加载完毕tipe
-(void)setupUIOfNomuchMoreDataTipeView
{
    if (self.nomuchMoreDataTipeView == NULL)
    {
        self.nomuchMoreDataTipeView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 30)];
        
        self.nomuchMoreDataTipeView.tag = 9110;
        
        CGFloat color = 0.95703;
        
        self.nomuchMoreDataTipeView.backgroundColor = [UIColor colorWithRed:color green:color blue:color alpha:1];
        
        UILabel *label = [UILabel new];
        
        UIView *line = [UIView new];
        
        [self.nomuchMoreDataTipeView addSubview:line];
        
        [self.nomuchMoreDataTipeView addSubview:label];
        
        label.font = [UIFont systemFontOfSize:13];
        
        label.backgroundColor = self.nomuchMoreDataTipeView.backgroundColor;
        
        label.textColor = [UIColor lightGrayColor];
        
        label.textAlignment = NSTextAlignmentCenter;
        

        label.tag = 101;
        
        label.text = LOADALL_TIPE;
        
        color = 0.9176;
        
        line.backgroundColor = [UIColor colorWithRed:color green:color blue:color alpha:1];
        
        line.frame = CGRectMake(0,0,self.nomuchMoreDataTipeView.iwidth,0.5);
        
        label.frame = CGRectMake(0,0,100,20);
        
        CGPoint center = CGPointMake(self.nomuchMoreDataTipeView.iwidth/2, self.nomuchMoreDataTipeView.iheight/2);
        
        label.center = center;
        
        line.center = center;
    }
    
    
    if (self.scrollView.mj_footer != NULL)
    {
        //如果footer上已经有了self.nomuchMoreDataTipeView
        if ([self.scrollView.mj_footer.subviews containsObject:self.nomuchMoreDataTipeView]) {
            
            //如果当前的footer的最上层不是 self.nomuchMoreDataTipeView 把self.nomuchMoreDataTipeView放在最上层
            if (![[self.scrollView.mj_footer.subviews lastObject] isEqual:self.nomuchMoreDataTipeView])
            {
                [self.scrollView.mj_footer bringSubviewToFront:self.nomuchMoreDataTipeView];
            }
            
        }else
        {
            //如果当前的footer上没有self.nomuchMoreDataTipeView，添加一个
            [self.scrollView.mj_footer addSubview:self.nomuchMoreDataTipeView];
        }
     
        [self.scrollView.mj_footer endRefreshingWithNoMoreData];
        
        self.nomuchMoreDataTipeView.alpha = 1;
    }
}



@end
