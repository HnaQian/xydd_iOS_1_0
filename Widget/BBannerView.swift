//
//  BBannerView.swift
//  Limi
//
//  Created by Richie on 16/3/7.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit


@objc protocol BBannerViewDelegate {
    @objc optional func didSelectItem(_ index: Int)
}

@objc protocol BBannerViewDataSource {
    func viewForItem(_ bannerView: BBannerView, index: Int) -> UIView
    func numberOfItems() -> Int
}


class BBannerView: UIView,UIScrollViewDelegate {
    
    fileprivate var scrollView: UIScrollView!
    
    var dataSource: BBannerViewDataSource!
    var delegate: BBannerViewDelegate?
    fileprivate var timer: Timer?
    var pageControl: UIPageControl?
    var isShowPageCotroller : Bool = true
    
    var currentPageIndicatorTintColor:UIColor?
    var pageIndicatorTintColor:UIColor?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        self.scrollView = UIScrollView(frame: self.bounds)
        scrollView.scrollsToTop = false

        
        self.scrollView.isPagingEnabled = true
        self.scrollView.showsHorizontalScrollIndicator = false
        self.scrollView.showsVerticalScrollIndicator = false
        self.scrollView.delegate = self
        self.addSubview(self.scrollView)
    }
    
    func reloadData() {
        let itemCount = dataSource.numberOfItems()
        
        let subviews = self.scrollView.subviews
        for view in subviews {
            view.removeFromSuperview()
        }
        
        scrollView.contentSize = CGSize(width: scrollView.frame.size.width * CGFloat(dataSource.numberOfItems()), height: scrollView.frame.size.height)
        if dataSource.numberOfItems() > 1 {
            scrollView.contentSize = CGSize(width: scrollView.frame.size.width * CGFloat(dataSource.numberOfItems() + 2), height: scrollView.frame.size.height)
        }
        
        if dataSource.numberOfItems() > 1 {
            let itemBefore = dataSource.viewForItem(self, index: dataSource.numberOfItems() - 1)
            itemBefore.frame.origin.x = 0
            itemBefore.tag = dataSource.numberOfItems() - 1 + 500
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(BBannerView.tapItem(_:)))
            itemBefore.isUserInteractionEnabled = true
            itemBefore.addGestureRecognizer(tap)
            scrollView.addSubview(itemBefore)
        }
        
        for i in 0 ..< itemCount {
            let item = dataSource.viewForItem(self, index: i)
            item.tag = i + 500
            
            if itemCount > 1 {
                item.frame.origin.x = scrollView.frame.size.width * CGFloat(i + 1)
            } else {
                item.frame.origin.x = scrollView.frame.size.width * CGFloat(i)
            }
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(BBannerView.tapItem(_:)))
            item.isUserInteractionEnabled = true
            item.addGestureRecognizer(tap)
            
            scrollView.addSubview(item)
        }
        
        if dataSource.numberOfItems() > 1 {
            let item = dataSource.viewForItem(self, index: 0)
            item.frame.origin.x = scrollView.frame.size.width * CGFloat(dataSource.numberOfItems() + 1)
            scrollView.addSubview(item)
        }
        
        if dataSource.numberOfItems() > 1 {
            scrollView.scrollRectToVisible(CGRect(x: scrollView.frame.size.width, y: 0, width: scrollView.frame.size.width, height: scrollView.frame.size.height), animated: false)
        }
        if isShowPageCotroller == true{
            if itemCount > 1 {
                if pageControl == nil
                {
                    pageControl = UIPageControl()
                    
                    if let _ = currentPageIndicatorTintColor
                    {
                        pageControl?.currentPageIndicatorTintColor = currentPageIndicatorTintColor
                    }
                    
                    if let _  = pageIndicatorTintColor
                    {
                        pageControl?.pageIndicatorTintColor = pageIndicatorTintColor
                    }
                    
                    addSubview(pageControl!)
                    
                    pageControl?.snp_makeConstraints(closure: { (make) -> Void in
                        let _ = make.centerX.equalTo(self)
                        let _ = make.bottom.equalTo(self).offset(-5)
                        let _ = make.width.equalTo(150)
                        let _ = make.height.equalTo(10)
                    })
                }
                
                pageControl?.isHidden = false
                pageControl?.numberOfPages = itemCount
                pageControl?.currentPage = 0
            } else {
                pageControl?.isHidden = true
            }
        }
    }
    
    func tapItem(_ tap: UITapGestureRecognizer) {
        let index = tap.view!.tag - 500
        if self.delegate != nil {
            self.delegate?.didSelectItem!(index)
        }
    }
    
    func startAutoScroll(_ timeIntrval: Int) {
        self.stopAutoScroll()
        if timer == nil {
            timer = Timer(timeInterval: Double(timeIntrval), target: self, selector: #selector(BBannerView.next as (BBannerView) -> () -> ()), userInfo: nil, repeats: true)
        }
        
        RunLoop.current.add(timer!, forMode: RunLoopMode.commonModes)
        if self.dataSource.numberOfItems() > 1 {
            timer?.fireDate = (Date() as NSDate).addingSeconds(timeIntrval)
        } else {
            timer?.fireDate = Date.distantFuture
        }
    }
    
    func stopAutoScroll() {
        if timer != nil {
            timer?.fireDate = Date.distantFuture
        }
    }
    
    // scroll to next page
    func next() {
        let page = Int(scrollView.contentOffset.x / scrollView.frame.size.width) + 1
        scrollView.scrollRectToVisible(CGRect(x: CGFloat(page) * scrollView.frame.size.width, y: 0, width: scrollView.frame.size.width, height: scrollView.frame.size.height), animated: true)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool){
        
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView){
        
    }
    // when scroll to first or last page, update scrollView's contentoffset
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if dataSource.numberOfItems() > 1 {
            if scrollView.contentOffset.x.truncatingRemainder(dividingBy: scrollView.frame.size.width) == 0 {
                let page = Int(scrollView.contentOffset.x / scrollView.frame.size.width) + 1
                if page == dataSource.numberOfItems() + 2 {
                    scrollView.scrollRectToVisible(CGRect(x: scrollView.frame.size.width, y: 0, width: scrollView.frame.size.width, height: scrollView.frame.size.height), animated: false)
                } else if page == 1 {
                    scrollView.scrollRectToVisible(CGRect(x: scrollView.frame.size.width * CGFloat(dataSource.numberOfItems()), y: 0, width: scrollView.frame.size.width, height: scrollView.frame.size.height), animated: false)
                }
            }
            if isShowPageCotroller == true{
                var page = Int(scrollView.contentOffset.x / scrollView.frame.size.width) - 1
                if page == -1 {
                    page = dataSource.numberOfItems() - 1
                }
                
                pageControl?.currentPage = page
               
            }
        }
    }
    
    // MARK: - UIScrollViewDelegate
    
    //    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
    //
    //        if dataSource.numberOfItems() > 1 {
    //            var page = Int(scrollView.contentOffset.x / scrollView.frame.size.width) + 1
    //            if page == dataSource.numberOfItems() + 2 {
    //                scrollView.scrollRectToVisible(CGRectMake(scrollView.frame.size.width, 0, scrollView.frame.size.width, scrollView.frame.size.height), animated: false)
    //            } else if page == 0 {
    //                scrollView.scrollRectToVisible(CGRectMake(scrollView.frame.size.width * CGFloat(dataSource.numberOfItems()), 0, scrollView.frame.size.width, scrollView.frame.size.height), animated: false)
    //            }
    //        }
    //    }

}
