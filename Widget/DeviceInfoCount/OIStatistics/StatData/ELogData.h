//
//  ELogData.h
//  BirthdayReminder
//
//  Created by YuXiao on 15/7/3.
//
//

#import "TaskData.h"

@interface ELogData : TaskData<NSCoding>
{
    NSString *event_id;//事件标识
    NSString *category;//事件分类
    NSDictionary *params;//时间参数
    NSDate *event_time;//事件发生时间
}

@property (nonatomic,strong)NSString *event_id;//事件标识
@property (nonatomic,strong)NSString *category;//事件分类
@property (nonatomic,strong)NSDictionary *params;//时间参数
@property (nonatomic,strong)NSDate *event_time;//事件发生时间
@end
