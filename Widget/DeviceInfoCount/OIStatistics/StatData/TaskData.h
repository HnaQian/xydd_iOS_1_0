//
//  TaskData.h
//  BirthdayReminder
//
//  Created by YuXiao on 13-4-23.
//
//

#import <Foundation/Foundation.h>

typedef enum {
    TYPE_LAUNCH = 1,//启动
    TYPE_SESSION = 2,//Session
    TYPE_EVENT = 3,//事件
    TYPE_ERROR = 4,//错误日志
    TYPE_ACTIVITY = 5,//页面跟踪
    TYPE_ELOG = 6,//事件日志
} TaskType;

@interface TaskData : NSObject
{
    int type;
}

@property (nonatomic)int type;
@end
