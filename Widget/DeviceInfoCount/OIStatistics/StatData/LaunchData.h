//
//  LanuchData.h
//  BirthdayReminder
//
//  Created by YuXiao on 13-4-23.
//
//

#import "TaskData.h"

@interface LaunchData : TaskData<NSCoding>
{
    NSDate *client_time;//启动时间
}

@property (nonatomic,strong)NSDate *client_time;

@end
