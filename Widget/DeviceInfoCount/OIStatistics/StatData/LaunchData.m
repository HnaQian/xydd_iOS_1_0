//
//  LanuchData.m
//  BirthdayReminder
//
//  Created by YuXiao on 13-4-23.
//
//

#import "LaunchData.h"

#define kLDTimeKey @"client_time"

@implementation LaunchData
@synthesize client_time;


-(id)init
{
    if(self=[super init])
    {
        self.type = TYPE_LAUNCH;
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    if(self = [super init])
    {
        self.type = TYPE_LAUNCH;
        self.client_time = [aDecoder decodeObjectForKey:kLDTimeKey];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:client_time forKey:kLDTimeKey];
}

@end
