//
//  ELogData.m
//  BirthdayReminder
//
//  Created by YuXiao on 15/7/3.
//
//

#import "ELogData.h"

#define kELIdKey @"event_id"
#define kELTimeKey @"event_time"
#define kELCategoryKey @"category"
#define kELParamsKey @"params"

@implementation ELogData
@synthesize event_id;
@synthesize event_time;
@synthesize category;
@synthesize params;

-(id)init
{
    if(self=[super init])
    {
        self.type = TYPE_ELOG;
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    if(self = [super init])
    {
        self.type = TYPE_ELOG;
        self.event_id = [aDecoder decodeObjectForKey:kELIdKey];
        self.event_time = [aDecoder decodeObjectForKey:kELTimeKey];
        self.category = [aDecoder decodeObjectForKey:kELCategoryKey];
        self.params = [aDecoder decodeObjectForKey:kELParamsKey];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:event_id forKey:kELIdKey];
    [aCoder encodeObject:event_time forKey:kELTimeKey];
    [aCoder encodeObject:category forKey:kELCategoryKey];
    [aCoder encodeObject:params forKey:kELParamsKey];
}
@end
