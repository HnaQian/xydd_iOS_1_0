//
//  ClientData.h
//  BirthdayReminder
//
//  Created by YuXiao on 13-4-23.
//
//

#import <Foundation/Foundation.h>

@interface ClientData : NSObject
{
    NSString *deviceid;
    NSString *product_key;
    NSString *channel_id;
    NSInteger version_code;
    NSString *version_name;
    NSString *os_version;
    NSString *language;
    NSString *resolution;
    NSString *device_name;
    NSString *model_name;
    NSString *imei;
    NSString *imsi;
    NSInteger have_gps;
    NSInteger have_wifi;
    NSString *wifi_mac;
    NSString *latitude;
    NSString *longitude;
    NSString *mccmnc;
    NSString *network;
    NSString *device_token;
    NSInteger isroot;
    
    NSInteger birth_year;
    NSInteger birth_month;
    NSInteger birth_day;
    NSInteger birth_islunar;
    NSInteger gender;
    NSInteger userid;
    NSInteger birthcnt;
    NSInteger cityId;
    NSInteger first_boot;
}

@property (nonatomic,strong) NSString *deviceid;
@property (nonatomic,strong) NSString *product_key;
@property (nonatomic,strong) NSString *channel_id;
@property (nonatomic) NSInteger version_code;
@property (nonatomic,strong) NSString *version_name;
@property (nonatomic,strong) NSString *os_version;
@property (nonatomic,strong) NSString *language;
@property (nonatomic,strong) NSString *resolution;
@property (nonatomic,strong) NSString *device_name;
@property (nonatomic,strong) NSString *model_name;
@property (nonatomic,strong) NSString *imei;
@property (nonatomic,strong) NSString *imsi;
@property (nonatomic) NSInteger have_gps;
@property (nonatomic) NSInteger have_wifi;
@property (nonatomic,strong) NSString *wifi_mac;
@property (nonatomic,strong) NSString *latitude;
@property (nonatomic,strong) NSString *longitude;
@property (nonatomic,strong) NSString *mccmnc;
@property (nonatomic,strong) NSString *network;
@property (nonatomic,strong) NSString *device_token;
@property (nonatomic) NSInteger isroot;

@property (nonatomic) NSInteger birth_year;
@property (nonatomic) NSInteger birth_month;
@property (nonatomic) NSInteger birth_day;
@property (nonatomic) NSInteger birth_islunar;
@property (nonatomic) NSInteger gender;
@property (nonatomic) NSInteger userid;
@property (nonatomic) NSInteger birthcnt;
@property (nonatomic) NSInteger cityId;
@property (nonatomic) NSInteger first_boot;

@end
