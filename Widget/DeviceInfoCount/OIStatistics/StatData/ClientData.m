//
//  ClientData.m
//  BirthdayReminder
//
//  Created by YuXiao on 13-4-23.
//
//

#import "ClientData.h"

@implementation ClientData
@synthesize deviceid;
@synthesize product_key;
@synthesize channel_id;
@synthesize version_code;
@synthesize version_name;
@synthesize os_version;
@synthesize language;
@synthesize resolution;
@synthesize device_name;
@synthesize model_name;
@synthesize imei;
@synthesize imsi;
@synthesize have_gps;
@synthesize have_wifi;
@synthesize wifi_mac;
@synthesize latitude;
@synthesize longitude;
@synthesize mccmnc;
@synthesize network;
@synthesize device_token;
@synthesize isroot;
@synthesize birth_year;
@synthesize birth_month;
@synthesize birth_day;
@synthesize birth_islunar;
@synthesize gender;
@synthesize userid;
@synthesize birthcnt;
@synthesize cityId;
@synthesize first_boot;
@end
