//
//  OISAgent.m
//  BirthdayReminder
//
//  Created by YuXiao on 13-4-23.
//
//

#import "OISAgent.h"
#import "UIDevice-Hardware.h"
//#import "OIJSONKit.h"
#import "ClientData.h"
#import "LaunchData.h"
#import "ELogData.h"

#import "asl.h"
#import <SystemConfiguration/SystemConfiguration.h>
#import <arpa/inet.h> // For AF_INET, etc.
#import <ifaddrs.h> // For getifaddrs()
#import <net/if.h> // For IFF_LOOPBACK
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTCall.h>
#import <CoreTelephony/CTCallCenter.h>
#import <sys/utsname.h>
#import <zlib.h>
#import "ASIDataCompressor.h"

#define kOIMaxErrorCnt 100
#define kOIMaxLogCnt 1000

#define kOISPersistFile @"OIStatPersist.dat"

#define kOISessionIdKey @"sessionId"
#define kOISessionStartDataKey @"sessionStartDate"
#define kOISessionStopDateKey @"sessionStopDate"

#define kOISServerUrl @"http://stat.octinn.com/service/report"

@interface OISAgent()
{
    NSString *appKey;
    
    ClientData *clientData;//本地信息
    
    NSMutableArray *logArray;
    NSMutableArray *tmpLogArray;
    NSLock *postLock;
}

@property (nonatomic,strong) NSString *appKey;
@property (nonatomic,strong) ClientData *clientData;

@property (nonatomic,strong) NSMutableArray *logArray;
@property (nonatomic,strong) NSMutableArray *tmpLogArray;
@property (nonatomic,strong) NSLock *postLock;
@end

@implementation OISAgent
@synthesize appKey;
@synthesize clientData;
@synthesize logArray;
@synthesize tmpLogArray;
@synthesize postLock;


+(OISAgent*)getInstance
{
    static OISAgent *instance = nil;
    if(instance == nil)
    {
        instance = [[[self class] alloc] init];
    }
    return instance;
}

+(void)startWithAppKey:(NSString*)appKey
{
    [[OISAgent getInstance] initWithAppKey:appKey];
}

-(void)initWithAppKey:(NSString*)applicationKey
{
    self.appKey = applicationKey;
    
    self.clientData = [self getClientInfo];
    
    postLock = [[NSLock alloc]init];
    
    NSNotificationCenter *notifCenter = [NSNotificationCenter defaultCenter];
    [notifCenter addObserver:self selector:@selector(resignActive:) name:UIApplicationWillResignActiveNotification object:nil];
    [notifCenter addObserver:self selector:@selector(becomeActive:) name:UIApplicationWillEnterForegroundNotification object:nil];
    
    self.tmpLogArray = [NSMutableArray arrayWithCapacity:2];
    
    NSString *filePath = [[NSHomeDirectory()stringByAppendingPathComponent:@"Documents"]stringByAppendingPathComponent:kOISPersistFile];
    NSData *logData = [NSData dataWithContentsOfFile:filePath];
    if(logData!=nil)
    {
        @try {
            self.logArray = [NSKeyedUnarchiver unarchiveObjectWithData:logData];
        }
        @catch (NSException *exception) {
            self.logArray = [NSMutableArray arrayWithCapacity:2];
        }
        @finally {
        }
    }
    else
    {
        self.logArray = [NSMutableArray arrayWithCapacity:2];
    }
}

-(void)saveLog
{
    //保存之前清理不用的数据
    if(self.logArray.count>kOIMaxLogCnt)
    {
        [self.logArray removeObjectsInRange:NSMakeRange(0, self.logArray.count - kOIMaxLogCnt)];
    }
    
    NSString *filePath = [[NSHomeDirectory()stringByAppendingPathComponent:@"Documents"]stringByAppendingPathComponent:kOISPersistFile];
    NSData *newLogData = [NSKeyedArchiver archivedDataWithRootObject:self.logArray];
    [newLogData writeToFile:filePath atomically:YES];
}

- (void)resignActive:(NSNotification *)notification
{
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:kOISessionIdKey];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:kOISessionStartDataKey];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:kOISessionStopDateKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self saveLog];
    [self performSelectorInBackground:@selector(reportLocalLogs) withObject:nil];
}

- (void)becomeActive:(NSNotification *)notification
{
    [self performSelectorInBackground:@selector(reportLocalLogs) withObject:nil];
}

-(void)addTask:(NSObject*)taskObject
{
    if([postLock tryLock])
    {
        [self.logArray addObject:taskObject];
        if(self.tmpLogArray.count>0)
        {
            [self.logArray addObjectsFromArray:self.tmpLogArray];
            [self.tmpLogArray removeAllObjects];
        }
        if(self.logArray.count%20==10)//每隔20个日志，写一次文件
        {
            [self saveLog];
        }
        [postLock unlock];
    }
    else
    {
        [self.tmpLogArray addObject:taskObject];
    }
}

-(void) reportLocalLogs
{
    @autoreleasepool {
        [postLock lock];
        if([self.logArray count]>0)
        {
            NSMutableArray *uploadLogArray = [[NSMutableArray alloc]init];
            for(TaskData *oneTask in self.logArray)
            {
                NSMutableDictionary *oneLog = [[NSMutableDictionary alloc]init];
                [oneLog setObject:[NSNumber numberWithInt:oneTask.type] forKey:@"type"];
                NSMutableDictionary *data = [[NSMutableDictionary alloc]init];
                [data setObject:self.clientData.deviceid forKey:@"udid"];
                [data setObject:self.clientData.product_key forKey:@"product_key"];
                [data setObject:self.clientData.channel_id forKey:@"channel_id"];
                [data setObject:self.clientData.os_version forKey:@"os_version"];
                [data setObject:[NSNumber numberWithInteger:self.clientData.version_code] forKey:@"version_code"];
                [data setObject:self.clientData.version_name forKey:@"version_name"];
                switch (oneTask.type) {
                    case TYPE_LAUNCH:
                    {
                        LaunchData *oneLaunch = (LaunchData*)oneTask;
                        [data setObject:[self getDateStr:oneLaunch.client_time] forKey:@"client_time"];
                        [data setObject:self.clientData.language forKey:@"language"];
                        [data setObject:self.clientData.resolution forKey:@"resolution"];
                        [data setObject:self.clientData.device_name forKey:@"device_name"];
                        [data setObject:self.clientData.model_name forKey:@"model_name"];
                        [data setObject:self.clientData.imei forKey:@"imei"];
                        [data setObject:self.clientData.imsi forKey:@"imsi"];
                        [data setObject:[NSNumber numberWithInteger:self.clientData.have_gps] forKey:@"have_gps"];
                        [data setObject:[NSNumber numberWithInteger:self.clientData.have_wifi] forKey:@"have_wifi"];
                        [data setObject:self.clientData.wifi_mac forKey:@"wifi_mac"];
                        [data setObject:self.clientData.latitude forKey:@"latitude"];
                        [data setObject:self.clientData.longitude forKey:@"longitude"];
                        [data setObject:self.clientData.mccmnc forKey:@"mccmnc"];
                        [data setObject:self.clientData.network forKey:@"network"];
                        [data setObject:self.clientData.device_token forKey:@"device_token"];
                        [data setObject:[NSNumber numberWithInteger:self.clientData.cityId] forKey:@"city_id"];
                        [data setObject:[NSNumber numberWithInteger:self.clientData.isroot] forKey:@"is_root"];
                        [data setObject:[NSNumber numberWithInteger:self.clientData.birth_year] forKey:@"birth_year"];
                        [data setObject:[NSNumber numberWithInteger:self.clientData.birth_month] forKey:@"birth_month"];
                        [data setObject:[NSNumber numberWithInteger:self.clientData.birth_day] forKey:@"birth_day"];
                        [data setObject:[NSNumber numberWithInteger:self.clientData.birth_islunar] forKey:@"birth_islunar"];
                        [data setObject:[NSNumber numberWithInteger:self.clientData.gender] forKey:@"gender"];
                        [data setObject:[NSNumber numberWithInteger:self.clientData.userid] forKey:@"userid"];
                        [data setObject:[NSNumber numberWithInteger:self.clientData.birthcnt] forKey:@"birthcnt"];
                        [data setObject:[NSNumber numberWithInteger:self.clientData.first_boot] forKey:@"first_boot"];
                    }
                        break;
                    case TYPE_ELOG:
                    {
                        ELogData *oneELog = (ELogData*)oneTask;
                        if(oneELog.event_id)
                        {
                            [data setObject:oneELog.event_id forKey:@"name"];
                        }
                        if(oneELog.category)
                        {
                            [data setObject:oneELog.category forKey:@"category"];
                        }
                        if(oneELog.params)
                        {
                            [data setObject:oneELog.params forKey:@"params"];
                        }
                        [data setObject:[self getDateStr:oneELog.event_time] forKey:@"time"];
                    }
                        break;
                    default:
                        break;
                }
                [oneLog setObject:data forKey:@"data"];
                [uploadLogArray addObject:oneLog];
            }
            
            NSData *allLogData = [NSJSONSerialization dataWithJSONObject:uploadLogArray options:0 error:nil];
//            NSData *allLogData = [uploadLogArray toJSONData];
            NSData *postData = [ASIDataCompressor compressData:allLogData error:nil];

            NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:kOISServerUrl]];
            [request addValue:@"application/octet-stream" forHTTPHeaderField:@"Content-Type"];
            [request setValue:[NSString stringWithFormat:@"XYDD %@ (%@; %@ %@; %@)", self.clientData.version_name, self.clientData.model_name, [[UIDevice currentDevice]systemName], self.clientData.os_version, [[NSLocale currentLocale] localeIdentifier]] forHTTPHeaderField:@"User-Agent"];
            [request setHTTPMethod:@"POST"];
            [request setHTTPBody:postData];
            
            NSError        *error = nil;
            NSURLResponse  *response = nil;
            
            /*NSData *returnData =*/ [NSURLConnection sendSynchronousRequest:request returningResponse:&response error: &error];
//            if(response==nil)
//            {
//                
//            }
//            else
//            {
                NSInteger statusCode = [(NSHTTPURLResponse*)response statusCode];
                if(statusCode==200)
                {
                    [logArray removeAllObjects];
                }
//            }
        }
        if(self.tmpLogArray.count>0)
        {
            [self.logArray addObjectsFromArray:self.tmpLogArray];
            [self.tmpLogArray removeAllObjects];
        }
        [postLock unlock];
        [self saveLog];
    }
}

+(void)onLaunch
{    
    LaunchData *launchData = [[LaunchData alloc]init];
    launchData.client_time = [NSDate date];
    [[OISAgent getInstance]addTask:launchData];
    
    //此处上传日志
    
    [[OISAgent getInstance]performSelectorInBackground:@selector(reportLocalLogs) withObject:nil];
}

+(void)logEvent:(NSString *)eventName category:(NSString *)category params:(NSDictionary *)params
{
    ELogData *event = [[ELogData alloc]init];
    event.event_id = eventName;
    event.event_time = [NSDate date];
    event.category = category;
    event.params = params;
    [[OISAgent getInstance]addTask:event];

}

+(void)setUserId:(NSInteger)_userId
{
    [[OISAgent getInstance].clientData setUserid:_userId];
}

+(void)setBirthYear:(NSInteger)_birthYear
{
    [[OISAgent getInstance].clientData setBirth_year:_birthYear];
}

+(void)setBirthMonth:(NSInteger)_birthMonth
{
    [[OISAgent getInstance].clientData setBirth_month:_birthMonth];
}

+(void)setBirthDay:(NSInteger)_birthDay
{
    [[OISAgent getInstance].clientData setBirth_day:_birthDay];
}

+(void)setBirthIslunar:(NSInteger)_birthIslunar
{
    [[OISAgent getInstance].clientData setBirth_islunar:_birthIslunar];
}

+(void)setGender:(NSInteger)_gender
{
    [[OISAgent getInstance].clientData setGender:_gender];
}

+(void)setBirthCnt:(NSInteger)_cnt
{
    [[OISAgent getInstance].clientData setBirthcnt:_cnt];
}

+(void)setDevToken:(NSString *)_devToken
{
    [[OISAgent getInstance].clientData setDevice_token:_devToken];
}

+(void)setVersionCode:(NSInteger)_version_code
{
    [[OISAgent getInstance].clientData setVersion_code:_version_code];
}

+(void)setChannel:(NSString *)_channel
{
    [[OISAgent getInstance].clientData setChannel_id:_channel];
}

+(void)setLatitude:(NSString *)_latitude
{
    [[OISAgent getInstance].clientData setLatitude:_latitude];
}

+(void)setLongitude:(NSString *)_longitude
{
    [[OISAgent getInstance].clientData setLongitude:_longitude];
}

+(void)setCityId:(NSInteger)cityId
{
    [[OISAgent getInstance].clientData setCityId:cityId];
}
+(void)setFirstBoot:(NSInteger)_firstBoot
{
    [[OISAgent getInstance].clientData setFirst_boot:_firstBoot];
}

-(ClientData *)getClientInfo
{
    NSString *mac = [[UIDevice currentDevice]macaddress];
    if(mac==nil)
    {
        mac = @"mac";
    }
    
    ClientData  *info = [[ClientData alloc] init];
    info.deviceid = [[UIDevice currentDevice]deviceId];
    info.product_key = self.appKey;
    info.channel_id = @"";
    info.version_code = 0;
    info.version_name = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    info.os_version = [[UIDevice currentDevice] systemVersion];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *languages = [defaults objectForKey:@"AppleLanguages"];
    info.language = [languages objectAtIndex:0];
    
    CGRect rect = [[UIScreen mainScreen] bounds];
    CGFloat scale = [[UIScreen mainScreen] scale];
    info.resolution = [[NSString alloc] initWithFormat:@"%.fx%.f",rect.size.width*scale,rect.size.height*scale];
    
    info.device_name = [self machineName];
    
    info.device_token = @"";
    info.model_name = [[UIDevice currentDevice] model];
    info.imei = @"";
//    info.imsi = @"";
    info.imsi = [[UIDevice currentDevice]idfaString];
    info.have_gps = 1;
    info.have_wifi = 1;
    info.wifi_mac = mac;
    
    info.latitude = @"";
    info.longitude = @"";
    
    CTTelephonyNetworkInfo*netInfo =[[CTTelephonyNetworkInfo alloc] init];
    CTCarrier*carrier =[netInfo subscriberCellularProvider];
    NSString*mcc =[carrier mobileCountryCode];
    NSString*mnc =[carrier mobileNetworkCode];
    NSString *mccmnc = [mcc stringByAppendingString:mnc];
    if(mccmnc!=nil)
        info.mccmnc = mccmnc;
    else
        info.mccmnc = @"";
    
    info.network = @"";
    if ([[NSFileManager defaultManager] fileExistsAtPath:@"/private/var/lib/apt/"])
    {
        info.isroot = 1;
    }
    else
    {
        info.isroot = 0;
    }
    info.birth_year = 0;
    info.birth_month = 0;
    info.birth_day = 0;
    info.birth_islunar = 0;
    info.gender = -1;
    info.userid = 0;
    info.birthcnt = 0;
    
    return info;
}

-(NSString *)getCurrentTime
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"ABC"];
    [dateFormatter setTimeZone:gmt];
    NSString *timeStamp = [dateFormatter stringFromDate:[NSDate date]];
    if(timeStamp==nil)
    {
        timeStamp = @"2014-10-06 13:46:00";
    }
    return timeStamp;
}

-(NSString *)getDateStr:(NSDate *)inputDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"ABC"];
    [dateFormatter setTimeZone:gmt];
    NSString *timeStamp = [dateFormatter stringFromDate:inputDate];
    if(timeStamp==nil)
    {
        timeStamp = [self getCurrentTime];
    }
    return timeStamp;
}

-(NSString*) machineName
{
    struct utsname systemInfo;
    uname(&systemInfo);
    return  [NSString stringWithCString:systemInfo.machine
                               encoding:NSUTF8StringEncoding];
}

@end
