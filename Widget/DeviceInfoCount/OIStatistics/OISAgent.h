//
//  OISAgent.h
//  BirthdayReminder
//
//  Created by YuXiao on 13-4-23.
//
//

#import <Foundation/Foundation.h>

@interface OISAgent : NSObject

+(void)startWithAppKey:(NSString*)appKey;

+(void)onLaunch;

+(void)logEvent:(NSString*)eventName category:(NSString*)category params:(NSDictionary*)params;

+(void)setUserId:(NSInteger)_userId;
+(void)setGender:(NSInteger)_gender;
+(void)setBirthYear:(NSInteger)_birthYear;
+(void)setBirthMonth:(NSInteger)_birthMonth;
+(void)setBirthDay:(NSInteger)_birthDay;
+(void)setBirthIslunar:(NSInteger)_birthIslunar;
+(void)setBirthCnt:(NSInteger)_cnt;
+(void)setDevToken:(NSString*)_devToken;
+(void)setVersionCode:(NSInteger)_version_code;
+(void)setChannel:(NSString*)_channel;
+(void)setLatitude:(NSString*)_latitude;
+(void)setLongitude:(NSString*)_longitude;
+(void)setCityId:(NSInteger)cityId;
+(void)setFirstBoot:(NSInteger)_firstBoot;
@end
