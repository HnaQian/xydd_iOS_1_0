//
//  PCDropdownMenuView.swift
//  DropdownMenuDemo
//
//  Created by Artpower on 15/11/12.
//  Copyright © 2015年 http://www.devpc.cn. All rights reserved.
//

import UIKit

private let DROPDOWN_MENU_ANIMATE_DURATION: TimeInterval = 0.5


// MARK: -

public protocol PCDropdownMenuViewDataSource: NSObjectProtocol {
    
    // 菜单项总个数
    func numberOfRowInDropdownMenuView(_ view: PCDropdownMenuView) -> Int
    // 根据索引返回对应的菜单项
    func dropdownMenuView(_ view: PCDropdownMenuView, itemForRowAtIndexPath indexPath: IndexPath) -> PCDropdownMenuItem
    // 下拉菜单的箭头图标
    func dropdownMenuViewArrowImage(_ view: PCDropdownMenuView) -> UIImage?
    // 下拉菜单内容区域的frame,高度自适应
    func dropdownMenuViewContentFrame(_ view: PCDropdownMenuView) -> CGRect
    // 箭头图标的水平偏移值,调节箭头靠左还是靠右(忽略垂直偏移)
    func dropdownMenuViewArrowImageOffset(_ view: PCDropdownMenuView) -> UIOffset
    
}


// MARK: -

@objc public protocol PCDropdownMenuViewDelegate: NSObjectProtocol {
    // 菜单项被点击后的回调
    @objc optional func dropdownMenuViewDidSelectedItem(_ view: PCDropdownMenuView, inIndex index: Int)
}


// MARK: -

open class PCDropdownMenuView: UIView, UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate {

    weak open var dataSource: PCDropdownMenuViewDataSource?
    weak open var delegate: PCDropdownMenuViewDelegate?
    fileprivate var isNeedHint : Bool = false
    fileprivate var selectedIndex : Int = 0
    fileprivate var contentView = UIView()
    //private var arrowImageView = UIImageView()
    fileprivate var tableView = UITableView(frame: CGRect.zero, style: UITableViewStyle.plain)
    
    // MARK: - Override Property
    
    override open var frame: CGRect {
        set {
            super.frame = Constant.ScreenSize.SCREEN_BOUNDS
        }
        get {
            return super.frame
        }
    }
    
    override open var bounds: CGRect {
        set {
            super.bounds = Constant.ScreenSize.SCREEN_BOUNDS
        }
        get {
            return super.bounds
        }
    }
    
    
    //MARK: - Lifecycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(PCDropdownMenuView.tapGestureAction(_:)))
        tapGesture.delegate = self
        self.addGestureRecognizer(tapGesture)
        self.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.2)
        
        self.addSubview(contentView)
        //contentView.addSubview(arrowImageView)
        contentView.addSubview(tableView)
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = 44.0
        tableView.layer.cornerRadius = 5.0
        tableView.layer.masksToBounds = true
        tableView.isScrollEnabled = false
        tableView.separatorInset = UIEdgeInsetsMake(0, 10, 0, 10)
    }

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    func inittableView(){
        if let dataSource = self.dataSource {
            let image = dataSource.dropdownMenuViewArrowImage(self)
            let imageSize = image?.size ?? CGSize.zero
            
            let count = dataSource.numberOfRowInDropdownMenuView(self)
            let tableHeight = CGFloat(count) * tableView.rowHeight
            var frame = dataSource.dropdownMenuViewContentFrame(self)
            frame.size.height = imageSize.height + tableHeight
            contentView.frame = frame
            
            tableView.frame = CGRect(x: 0.0, y: 5, width: frame.size.width, height: tableHeight)
        }

    }
    // MARK: - Public
    
    open func showWithAnimate(_ animate: Bool,isNeedHint: Bool? = false,selectedIndex: Int? = 0) {
        UIApplication.shared.keyWindow?.addSubview(self)
        if let hint : Bool = isNeedHint{
            self.isNeedHint = hint
        }
        if let index : Int = selectedIndex{
            self.selectedIndex = index
        }
        if !animate {
            contentView.alpha = 1.0
            return
        }
        self.inittableView()
        self.tableView.reloadData()
        contentView.alpha = 0.0
        UIView.animate(withDuration: DROPDOWN_MENU_ANIMATE_DURATION, animations: { () -> Void in
            self.contentView.alpha = 1.0
            
        }) 
    }
    
    open func hiddenWithAnimate(_ animate: Bool) {
        if !animate {
            self.removeFromSuperview()
            return
        }
        UIView.animate(withDuration: DROPDOWN_MENU_ANIMATE_DURATION, animations: { () -> Void in
            self.contentView.alpha = 0.0
            }, completion: { (_) -> Void in
                self.removeFromSuperview()
        }) 
    }
    
    open func tapGestureAction(_ recognizer: UITapGestureRecognizer) {
        self.hiddenWithAnimate(true)
    }
    
    // MARK: - UITableViewDataSource
    
    open func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let dataSource = self.dataSource {
            return dataSource.numberOfRowInDropdownMenuView(self)
        }
        return 0
    }
    
    open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "Cell"
        var cell = tableView.dequeueReusableCell(withIdentifier: identifier)
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: identifier)
            cell?.textLabel?.font = UIFont.systemFont(ofSize: 15)
            cell?.textLabel?.textAlignment = .center
        }
        
        if let dataSource = self.dataSource {
            let item = dataSource.dropdownMenuView(self, itemForRowAtIndexPath: indexPath)
            cell?.textLabel?.text = item.name
            if self.isNeedHint{
                cell?.textLabel?.textColor = (indexPath as NSIndexPath).row == self.selectedIndex ? UIColor(rgba: Constant.common_C1_color) : UIColor.black
                }
            else{
                cell?.textLabel?.textColor = UIColor.black
            }
        }
        
        return cell!
    }
    
    
    // MARK: - UITableViewDelegate
    
    open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        self.delegate?.dropdownMenuViewDidSelectedItem?(self, inIndex: (indexPath as NSIndexPath).row)
    }
    
    
    // MARK: - UIGestureRecognizerDelegate
    
    open func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return (touch.view == self)
    }

}
