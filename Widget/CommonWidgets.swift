//
//  CommonWidgets.swift
//  Limi
//
//  Created by Richie on 16/3/5.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//常用小控件集合

import UIKit

class CommonWidgets: NSObject {

    
    ///导航栏右上角的红字按钮
    class NavigationRightBtn: UIBarButtonItem {
        
        var intendifer:String{return _intendifer}
        fileprivate var _intendifer = ""
        /**
         导航栏右上角的红字按钮，也可以用在左上角
         此button.type = .Custom,字体居中
         - parameter title:     title  默认为"确定"
         - parameter bgImgName: 背景图片 默认""
         - parameter target:    *
         - parameter action:    *
         - parameter color:     字体颜色 默认红色:"#FF3A48"
         - parameter font:      字体尺寸 默认15
         
         - returns: 返回一个button
         */
        
        class func newRightBtn(_ title:String! = "",bgImgName:String! = "",imgName:String! = "",target: AnyObject?,action: Selector,color:String! = "#12171F",font:CGFloat! = 15) -> NavigationRightBtn
        {
            let confirmBtn = UIButton(type: .custom)
            //title
            confirmBtn.setTitle(title, for: UIControlState())
            //bgimage
            confirmBtn.setBackgroundImage(UIImage(named: bgImgName), for: UIControlState())
            confirmBtn.setImage(UIImage(named:imgName), for: UIControlState())
            confirmBtn.imageView?.contentMode = UIViewContentMode.scaleAspectFill
            //action
            confirmBtn.addTarget(target, action: action, for: UIControlEvents.touchUpInside)
            //title color
            confirmBtn.setTitleColor(UIColor(rgba: color), for: UIControlState())
            //font
            confirmBtn.titleLabel?.font = UIFont.systemFont(ofSize: font)
            //frame
            if title.characters.count == 0{
                confirmBtn.frame = CGRect(x: 0,y: 0,width: 25,height: 25)
            }else{
                confirmBtn.frame = CGRect(x: 0,y: 0,width: CGFloat(title.characters.count) * 16,height: 40)
            }
            let navItem = NavigationRightBtn(customView: confirmBtn)
            navItem._intendifer = title
            return navItem
        }
    }
    
    
    /// 放在VC底部的红色button
    class ConfirmButton:UIButton
    {
        var title:String = ""{didSet{self.setTitle(title, for: UIControlState())}}
        
        /**
         放在VC底部的红色按钮
         ,红底白字，红底颜色值:"#FF3A48",字号为17，居中
         - parameter title:  按钮名称 ,
         - parameter target: *
         - parameter action: *
         
         - returns: button
         */
        class func newButton(_ title:String,target: AnyObject?,action: Selector) ->ConfirmButton
        {
            let confirmBtn = ConfirmButton()
            confirmBtn.layer.cornerRadius = 3
            confirmBtn.clipsToBounds = true
            confirmBtn.setTitle(title, for: UIControlState())
            confirmBtn.backgroundColor =  UIColor(rgba: "#FF3A48")
            confirmBtn.addTarget(target, action: action, for: UIControlEvents.touchUpInside)
            
            return confirmBtn
        }
    }
}
