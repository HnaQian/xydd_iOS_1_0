//
//  Waver.swift
//  Swift-Waver
//
//  Created by nangezao on 14/12/21.
//  Copyright (c) 2014年 dreamer.nange. All rights reserved.
//

import UIKit

open class Wave: UIView {
    
    open var frequency:CGFloat     = 1.2
    open var amplitude:CGFloat     = 1.0
    open var idleAmplitude:CGFloat = 0.01

    open var phaseShift:CGFloat    = -0.25
    open var density:CGFloat       = 1.0
    open var numOfWavers           = 5
    
    open var mainWaveWidth         = 2.0
    open var decorativeWavesWidth  = 2.0

    open var waveColor             = UIColor.white
    
    fileprivate var waverHeight:CGFloat  = 0
    fileprivate var waverWidth:CGFloat   = 0
    fileprivate var phase:CGFloat        = 0.0
    fileprivate var wavers:Array<CAShapeLayer> = []
    


    fileprivate var waverCallBack:() ->() = {return}
    
    fileprivate var waveMid:CGFloat{
        get{
            return waverWidth/2.0
        }
    }
    fileprivate var maxAmplitude:CGFloat{
        get{
            return waverHeight - 4.0
        }
    }
    
    open var level:CGFloat{
        set{
            phase = phase + phaseShift; // Move the wave
            amplitude = max( newValue,idleAmplitude);
            updateMeters();
        }
        get{
            return 0.1;
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        waverWidth  = 0.98*frame.width
        waverHeight = frame.height
    }

    required public init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    
    func setWaverLevelCallback(_ callBack:@escaping ()->()){
        waverCallBack = callBack
        
        for i in 0 ..< self.numOfWavers {
            let waveline = CAShapeLayer();
            waveline.lineCap       = kCALineCapButt;
            waveline.lineJoin      = kCALineJoinRound;
            waveline.strokeColor   = UIColor.clear.cgColor
            waveline.fillColor     = UIColor.clear.cgColor
            let lineWidth =   (i == 0) ? mainWaveWidth : decorativeWavesWidth;
            waveline.lineWidth = CGFloat(lineWidth)
            let progress = 1.0 - CGFloat(i)/CGFloat(numOfWavers);
            let param1 : CGFloat = progress / 3.0
            let param2 : CGFloat = param1 * 2.0
            let multiplier = min(CGFloat(1.0), CGFloat(param2 + (1.0 / 3.0)));
            waveline.strokeColor   = UIColor(white: 1.0, alpha:( (i == 0) ? 1.0 : 1.0 * multiplier * 0.4)).cgColor;
            self.layer.addSublayer(waveline);
            wavers.append(waveline)
        }

        let displaylink = CADisplayLink(target:self, selector:#selector(Wave.warverExecute))
        displaylink.add(to: RunLoop.current ,forMode:RunLoopMode.commonModes)
    }
    
    func warverExecute(){
        waverCallBack()
    }
    
    
    func updateMeters(){
        UIGraphicsBeginImageContext(self.frame.size)
        
        for i in 0...numOfWavers - 1{
            let wavelinePath = UIBezierPath()
            let progress = 1.0 - CGFloat(i)/CGFloat( self.numOfWavers);
            let normedAmplitude:CGFloat = (1.5 * progress - 0.5) * CGFloat(self.amplitude);
            //代替下边的for循环
            var x:CGFloat = 0.0 
            while x < self.waverWidth + self.density {
                let scaling = -pow(x / self.waveMid  - 1, 2) + 1;                             // make center bigger
                
                let sinValue = sin(( CGFloat(M_PI*2.0)*(x / self.waverWidth) * self.frequency + self.phase))
                
                let y = scaling * self.maxAmplitude * normedAmplitude * sinValue + self.waverHeight;
                
                if x == 0 {
                    wavelinePath.move(to: CGPoint(x: x, y: y))
                }
                else {
                    wavelinePath.addLine(to: CGPoint(x: x, y: y))
                }

                x = x + self.density
            }
            
            
//            for  x:CGFloat = 0.0; x < self.waverWidth + self.density;x += self.density{
//                
//                let scaling = -pow(x / self.waveMid  - 1, 2) + 1;                             // make center bigger
//                
//                let sinValue = sin(( CGFloat(M_PI*2.0)*(x / self.waverWidth) * self.frequency + self.phase))
//                
//                let y = scaling * self.maxAmplitude * normedAmplitude * sinValue + self.waverHeight;
//                
//                if x == 0 {
//                    wavelinePath.move(to: CGPoint(x: x, y: y))
//                }
//                else {
//                    wavelinePath.addLine(to: CGPoint(x: x, y: y))
//                }
//            }
            let waveline = self.wavers[i];
            waveline.path = wavelinePath.cgPath;
        }
        
        UIGraphicsEndImageContext()
    }

}
