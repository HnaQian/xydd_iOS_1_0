//
//  BottomToolsBar.swift
//  Limi
//
//  Created by Richie on 16/3/11.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

/**
 底部工具栏，包括四个组件
 
 - iconImgView  :  图标，可点击，点击事件为：ActionType.iconTouch
 - titleLbl     :  标题
 - contentLbl   : 内容
 - actionBtn    : 操作btn，点击事件为：ActionType.actionBtnTouch
 */
 /// 注：点击工具条本身的事件为：ActionType.selfTouch
class BottomToolsBar: BaseUnitView {
    
    //icon-title-content-button
    
    enum ActionType : Int
    {
        case selfTouch = 0
        case iconTouch
        case actionBtnTouch
    }
    
    //Private
    fileprivate let iconImgView = UIImageView()
    fileprivate let titleLbl = UILabel()
    fileprivate let contentLbl = UILabel()
    fileprivate let actionBtn = UIButton()
    fileprivate var _actionType:ActionType!
    
    //Public
    var icomImg:String?{didSet{if let icon = icomImg{ self.iconImgView.image = UIImage(named: icon)}}}
    var title:String?{didSet{self.titleLbl.text = title}}
    var content:String?{didSet{self.contentLbl.text = content}}
    var actionBtnTitle:String?{didSet{self.actionBtn.setTitle(actionBtnTitle, for: UIControlState())}}
    /// 按钮的样式：0:红底白字 1:红边红字 2:白字黑底
    var actionBtnStyle = 0{didSet{
        if actionBtnStyle == 0{
            actionBtn.isEnabled = true
            actionBtn.backgroundColor = UIColor(rgba: Constant.common_red_color)
            actionBtn.layer.borderWidth = 0
            actionBtn.setTitleColor(UIColor.white, for: UIControlState())
            
        }else if actionBtnStyle == 1{
            actionBtn.isEnabled = true
            actionBtn.backgroundColor = UIColor.white
            actionBtn.layer.borderWidth = 1
            actionBtn.layer.borderColor = UIColor(rgba: Constant.common_C1_color).cgColor
            actionBtn.setTitleColor(UIColor(rgba: Constant.common_red_color), for: UIControlState())
            
        }else if actionBtnStyle == 2{
            actionBtn.isEnabled = true
            actionBtn.backgroundColor = UIColor(rgba: Constant.common_C8_color)
            actionBtn.layer.borderWidth = 1
            actionBtn.layer.borderColor = UIColor(rgba: Constant.common_separatLine_color).cgColor
            actionBtn.setTitleColor(UIColor(rgba: Constant.common_C12_color), for: UIControlState())
            
        }else if actionBtnStyle == 3{
            actionBtn.layer.borderWidth = 0
            actionBtn.isEnabled = false
            actionBtn.backgroundColor = UIColor.white
            actionBtn.setTitle("", for: UIControlState())
        }
        }}
    
    var contentTextColor:UIColor?{didSet{if contentTextColor != nil{self.contentLbl.textColor = contentTextColor!}}}

    
    
    /// readonly
    var actionType:ActionType{return _actionType}
    
    
    convenience init()
    {
        self.init(frame:CGRect.zero)
        
        self.setupUI()
    }
    
    
    func setContent(_ textColor:UIColor? = nil,textFont:CGFloat = 0,isBolderFont:Bool = false)
    {
        self.contentLbl.textColor = textColor
        if isBolderFont {
            self.contentLbl.font = UIFont.boldSystemFont(ofSize: Constant.Theme.FontSize_15)
        }
        if textFont != 0{
            self.contentLbl.font = UIFont.systemFont(ofSize: textFont)
        }
    }
    
    func setTitle(_ textColor:UIColor? = nil,textFont:CGFloat = 0,isBolderFont:Bool = false)
    {
        self.titleLbl.textColor = textColor
        if isBolderFont {
            self.titleLbl.font = UIFont.boldSystemFont(ofSize: Constant.Theme.FontSize_15)
        }
        if textFont != 0{
            self.titleLbl.font = UIFont.systemFont(ofSize: textFont)
        }
    }
    
    override func setupUI() {
        self.backgroundColor = UIColor.white
        self.isUserInteractionEnabled = true
        self.iconImgView.isUserInteractionEnabled = true
        
        titleLbl.font  = Constant.Theme.Font_15
        
        contentLbl.font  = Constant.Theme.Font_15
        
        let _ = [iconImgView,titleLbl,contentLbl,actionBtn].map{self.addSubview($0)}
        
        self.iconImgView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(BottomToolsBar.actionClick(_:))))
        self.actionBtn.addTarget(self, action: #selector(BottomToolsBar.actionClick(_:)), for: UIControlEvents.touchUpInside)
        actionBtn.backgroundColor = UIColor(rgba: Constant.common_red_color)
        actionBtn.layer.cornerRadius = 5
        actionBtn.titleLabel?.font = Constant.Theme.Font_17
        iconImgView.snp_makeConstraints { (make) -> Void in
           let _ =  make.centerY.equalTo(self)
            let _ = make.left.equalTo(self).offset(MARGIN_8)
            let _ = make.height.equalTo(30)
            let _ = make.width.equalTo(0)
        }
        
        titleLbl.snp_makeConstraints { (make) -> Void in
            let _ = make.left.equalTo(iconImgView.snp_right)
            let _ = make.centerY.equalTo(self)
            let _ = make.height.equalTo(20)
        }
        
        contentLbl.snp_makeConstraints { (make) -> Void in
            let _ = make.left.equalTo(titleLbl.snp_right)
            let _ = make.width.greaterThanOrEqualTo(100)
            let _ = make.centerY.equalTo(self)
            let _ = make.height.equalTo(20)
        }
        
        actionBtn.snp_makeConstraints { (make) -> Void in
            let _ = make.right.equalTo(self).offset(-MARGIN_8)
            let _ = make.top.equalTo(self).offset(MARGIN_10 - MARGIN_4)
            let _ = make.bottom.equalTo(self).offset(-(MARGIN_10 - MARGIN_4))
            let _ = make.width.equalTo(100)
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.actionClick(self)
    }
    
    func actionClick(_ sender:UIView)
    {
        let _ = Delegate_Selector(self.delegate, #selector(OrderUnitViewDelegate.orderBottomToolBarAction(_:))){Void in
            
            if sender == self
            {
                self._actionType = ActionType.selfTouch
            }else if sender == self.iconImgView
            {
                self._actionType = ActionType.iconTouch
            }else if sender == self.actionBtn
            {
                self._actionType = ActionType.actionBtnTouch
            }
            
            self.delegate!.orderBottomToolBarAction!(self)
        }
    }
}
