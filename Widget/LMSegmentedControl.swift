//
//  LMSegmentedControl.swift
//  Limi
//
//  Created by guo chen on 16/1/11.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

class LMSegmentedControl: UIControl,UIScrollViewDelegate {
    
    fileprivate let bottomEdgLine = UIView()
    
    fileprivate let topEdgLine = UIView()
    
    fileprivate let scrollView = UIScrollView()

    var showTopEdg:Bool!{
        didSet{
            self.topEdgLine.isHidden = !showTopEdg
        }}
    
    var selectedHandle:((_ index:Int)->Void)?
    
    fileprivate var indicator = UIView()
    
    var titles:[String]!//标题集合数组
        {
        didSet
        {
            if let mtitles = titles
            {
                if mtitles.count > 0
                {
                    self.createTabs()
                }
            }
        }
    }
    
    fileprivate var tabs = [CustomLabel]()//标签数组
    
    var currentIndex = 10086//当前选择的index
        {
        didSet
        {
            self.selectedTabRefreshUI(currentIndex)
        }
        
    }
    
    fileprivate var selected_tab:CustomLabel?//当前选择的tab
    
    var extra_width:CGFloat = 10//以最长标签的字数长度 + extra_width作为所有标签的显示宽度
    
    init(frame:CGRect,selectedHandle:@escaping ((_ index:Int)->Void)) {
        
        super.init(frame: frame)
        
        self.selectedHandle = selectedHandle
        
        self.setupUI()
    }
    
    
    
    fileprivate func setupUI()
    {
        self.addSubview(scrollView)
        
        scrollView.addSubview(indicator)
        self.addSubview(bottomEdgLine)
        self.addSubview(topEdgLine)
        self.backgroundColor = UIColor.white
        
        bottomEdgLine.frame = CGRect(x: 0, y: self.iheight - 0.5, width: self.iwidth, height: 0.5)
        topEdgLine.frame = CGRect(x: 0, y: 0, width: self.iwidth, height: 0.5)
        bottomEdgLine.backgroundColor = UIColor(rgba: Constant.common_C9_color)
        topEdgLine.backgroundColor = UIColor(rgba: Constant.common_C9_color)
        topEdgLine.isHidden = true
        bottomEdgLine.isHidden = true
        
        scrollView.frame = CGRect(x: 0, y: 0, width: self.iwidth, height: self.iheight)
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        scrollView.delegate = self
        scrollView.scrollsToTop = false
        scrollView.isScrollEnabled = false
        
        indicator.backgroundColor = UIColor(rgba: Constant.common_red_color)
        indicator.iheight = 2
        self.indicator.ibottom = scrollView.iheight
        }
    
    
    
    //MArk:创建标签
    fileprivate func createTabs()
    {
        //移除旧视图
        if self.tabs.count > 0
        {
            for tab in self.tabs
            {
                tab.removeFromSuperview()
            }
        }
        
        self.tabs = [CustomLabel]()
        
        
        var index = 0

        var allWidth:CGFloat = 0
        
        for title in self.titles
        {
            let tab = CustomLabel(font: Constant.common_F3_font, textColorHex: Constant.common_C2_color, textAlignment: NSTextAlignment.center)
            
            tab.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(LMSegmentedControl.tapAction(_:))))
            
            tab.isUserInteractionEnabled = true
            
            self.scrollView.addSubview(tab)
            
            tab.text = title
            
            tab.tag = 300 + index
            
            self.tabs.append(tab)
            
            allWidth += tab.sizeThatFits(CGSize(width: CGFloat(MAXFLOAT),height: self.iheight - indicator.iheight)).width + extra_width
            
            index += 1
        }
        
        allWidth += extra_width
        
        var avergeMarginWidth:CGFloat = 0
        
        if allWidth <= self.iwidth
        {
            avergeMarginWidth = (self.iwidth - allWidth)/CGFloat(self.titles.count + 1)
        }
        
        //tab布局
        var tempTab:CustomLabel!
        
        for tab in self.tabs
        {
            tab.iwidth = tab.sizeThatFits(CGSize(width: CGFloat(MAXFLOAT),height: tab.iheight)).width + extra_width + avergeMarginWidth
            
            tab.iheight = self.iheight - indicator.iheight
            
            if tempTab == nil
            {
                tab.ileft = (avergeMarginWidth + extra_width)/2
            }else
            {
                tab.ileft = tempTab.iright
            }
            
            tempTab = tab
        }
        
        if allWidth <= self.iwidth{
            
            self.scrollView.contentSize = CGSize(width: self.iwidth + 1, height: self.iheight)
        }else
        {
            self.scrollView.contentSize = CGSize(width: allWidth + extra_width + avergeMarginWidth, height: self.iheight)
        }
        
        if currentIndex == 10086
        {
            self.didselectedTab(tabs[0])
        }else
        {
            currentIndex = currentIndex < titles.count - 1 ? currentIndex : titles.count - 1
            
            self.didselectedTab(tabs[currentIndex])
        }
    }
    
    
    
    @objc func tapAction(_ ges:UITapGestureRecognizer)
    {
        if let tab = ges.view as? CustomLabel
        {
            self.didselectedTab(tab)
        }
    }
    
    
    //执行回调
    fileprivate func didselectedTab(_ tab:CustomLabel)
    {
        self.currentIndex = tab.tag - 300
        
        self.selectedTabRefreshUI(currentIndex)
        
        
        if self.selectedHandle != nil
        {
            self.selectedHandle!(tab.tag - 300)
        }
    }
    
    
    //更新视图
    fileprivate func selectedTabRefreshUI(_ index:Int)
    {
        let tab = self.tabs[index]
        
        for mtab in self.tabs
        {
            if mtab == tab
            {
                mtab.textColor = UIColor(rgba: Constant.common_red_color)
            }
            else
            {
                mtab.textColor = UIColor(rgba: Constant.common_C8_color)
            }
        }
        
        var endOffsetX:CGFloat?
        
        if tab.ileft < scrollView.contentOffset.x
        {
            endOffsetX = tab.ileft
        }else if tab.iright > scrollView.contentOffset.x + scrollView.iwidth
        {
            endOffsetX = tab.iright - scrollView.iwidth
        }
        
        if let offsetX = endOffsetX
        {
            UIView.animate(withDuration: 0.25, animations: { () -> Void in
                
                self.scrollView.contentOffset = CGPoint(x: offsetX,y: 0)
                
            })
        }
        
        UIView.animate(withDuration: 0.25, animations: { () -> Void in
            
            self.indicator.iwidth = tab.sizeThatFits(CGSize(width: CGFloat(MAXFLOAT),height: tab.iheight)).width * 1.1
            
            self.indicator.icenterX = tab.icenterX
            
        })
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

