//
//  Created by 刘超 on 15/4/26.
//  Copyright (c) 2015年 Leo. All rights reserved.
//
//  Email:  leoios@sina.com
//  GitHub: http://github.com/LeoiOS
//  如有问题或建议请给我发 Email, 或在该项目的 GitHub 主页 Issues 我, 谢谢:)
//
//  V 1.1.1

#import "LCActionSheet.h"

// 间隔高度
#define SPACE_H 5.0f
// 按钮高度
#define BUTTON_H 50.0f
// 屏幕尺寸
#define SCREEN_SIZE [UIScreen mainScreen].bounds.size
// 颜色
#define LCColor(r, g, b) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:1.0f]

#define LC_ACTION_SHEET_TITLE_FONT  [UIFont systemFontOfSize:17.0f]

@interface LCActionSheet ()

/** 所有按钮 */
@property (nonatomic, strong) NSArray *buttonTitles;

/** 暗黑色的view */
@property (nonatomic, strong) UIView *darkView;

/** 所有按钮的底部view */
@property (nonatomic, strong) UIView *bottomView;

/** 代理 */
@property (nonatomic, retain) id<LCActionSheetDelegate> delegate;

@property (nonatomic, strong) UIWindow *backWindow;

@property (nonatomic, copy) LCActionSheetBlock clickedBlock;

@property (nonatomic, copy) NSString *title;

@property (nonatomic, assign) NSInteger redButtonIndex;

@property (nonatomic,copy) NSString *cancelTitle;



@end

@implementation LCActionSheet

+ (instancetype)sheetWithTitle:(NSString *)title buttonTitles:(NSArray *)buttonTitles redButtonIndex:(NSInteger)redButtonIndex delegate:(id<LCActionSheetDelegate>)delegate {
    
    return [[self alloc] initWithTitle:title buttonTitles:buttonTitles redButtonIndex:redButtonIndex delegate:delegate];
}

+ (instancetype)sheetWithTitle:(NSString *)title buttonTitles:(NSArray *)buttonTitles redButtonIndex:(NSInteger)redButtonIndex clicked:(LCActionSheetBlock)clicked {
    
    return [[self alloc] initWithTitle:title buttonTitles:buttonTitles redButtonIndex:redButtonIndex clicked:clicked];
}

+ (instancetype)sheetWithTitle:(NSString *)title buttonTitles:(NSArray *)buttonTitles cancelTitle:(NSString *)cancelTitle redButtonIndex:(NSInteger)redButtonIndex delegate:(id<LCActionSheetDelegate>)delegate {
    return [[self alloc] initWithTitle:title buttonTitles:buttonTitles cancelTitle:cancelTitle redButtonIndex:redButtonIndex delegate:delegate];
}

+ (instancetype)sheetWithTitle:(NSString *)title buttonTitles:(NSArray *)buttonTitles cancelTitle:(NSString *)cancelTitle redButtonIndex:(NSInteger)redButtonIndex clicked:(LCActionSheetBlock)clicked {
    return [[self alloc] initWithTitle:title buttonTitles:buttonTitles cancelTitle:cancelTitle redButtonIndex:redButtonIndex clicked:clicked];
}

- (instancetype)initWithTitle:(NSString *)title buttonTitles:(NSArray *)buttonTitles cancelTitle:(NSString *)cancelTitle redButtonIndex:(NSInteger)redButtonIndex delegate:(id<LCActionSheetDelegate>)delegate
{
    if (self = [super init]) {
        
        self.title = title;
        self.buttonTitles = buttonTitles;
        self.redButtonIndex = redButtonIndex;
        self.delegate = delegate;
        self.cancelTitle = cancelTitle;
        
        [self setupMainView];
    }
    return  self;
}

- (instancetype)initWithTitle:(NSString *)title buttonTitles:(NSArray *)buttonTitles cancelTitle:(NSString *)cancelTitle redButtonIndex:(NSInteger)redButtonIndex clicked:(LCActionSheetBlock)clicked {
    
    if (self = [super init]) {
        
        self.title = title;
        self.buttonTitles = buttonTitles;
        self.redButtonIndex = redButtonIndex;
        self.clickedBlock = clicked;
        self.cancelTitle = cancelTitle;
        [self setupMainView];
    }
    
    return self;
}

- (instancetype)initWithTitle:(NSString *)title
                 buttonTitles:(NSArray *)buttonTitles
               redButtonIndex:(NSInteger)redButtonIndex
                     delegate:(id<LCActionSheetDelegate>)delegate {
    
    if (self = [super init]) {
        
        self.title = title;
        self.buttonTitles = buttonTitles;
        self.redButtonIndex = redButtonIndex;
        self.delegate = delegate;
        self.cancelTitle = @"取消";
        
        [self setupMainView];
    }
    
    return self;
}

- (instancetype)initWithTitle:(NSString *)title
                 buttonTitles:(NSArray *)buttonTitles
               redButtonIndex:(NSInteger)redButtonIndex
                      clicked:(LCActionSheetBlock)clicked {
    
    if (self = [super init]) {
        
        self.title = title;
        self.buttonTitles = buttonTitles;
        self.redButtonIndex = redButtonIndex;
        self.clickedBlock = clicked;
        self.cancelTitle = @"取消";
        
        [self setupMainView];
    }
    
    return self;
}

- (void)setupMainView {
    
    // 暗黑色的view
    UIView *darkView = [[UIView alloc] init];
    [darkView setAlpha:0.4];
    [darkView setUserInteractionEnabled:NO];
    [darkView setFrame:(CGRect){0, 0, SCREEN_SIZE}];
    [darkView setBackgroundColor:LCColor(0, 0, 0)];
    [self addSubview:darkView];
    _darkView = darkView;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss:)];
    [darkView addGestureRecognizer:tap];
    
    // 所有按钮的底部view
    UIView *bottomView = [[UIView alloc] init];
    [bottomView setBackgroundColor:LCColor(242, 243, 245)];
    _bottomView = bottomView;
    
    UIView *titleBgView = [[UIView alloc] init];
    if (self.title) {
        
        CGFloat vSpace = 0;
        CGSize titleSize = [self.title sizeWithAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:15.0f]}];
        if (titleSize.width > SCREEN_SIZE.width - 30.0f) {
            vSpace = titleSize.height - 17.0f;
        }
        
        titleBgView.backgroundColor = [UIColor whiteColor];
        titleBgView.frame = CGRectMake(0, 0, SCREEN_SIZE.width, BUTTON_H + vSpace);
        [bottomView addSubview:titleBgView];
        
        // 标题
        UILabel *label = [[UILabel alloc] init];
        [label setText:self.title];
        [label setNumberOfLines:0];
        [label setTextColor:LCColor(153, 153, 153)];
        [label setTextAlignment:NSTextAlignmentCenter];
        [label setFont:[UIFont systemFontOfSize:15.0f]];
        [label setBackgroundColor:[UIColor whiteColor]];
        [label setFrame:CGRectMake(15.0f, 0, SCREEN_SIZE.width - 30.0f, titleBgView.frame.size.height)];
        
        // 标题上线条
        UIImageView *line = [[UIImageView alloc] init];
        [line setBackgroundColor:LCColor(234, 234, 234)];
        [line setFrame:CGRectMake(0, 0, SCREEN_SIZE.width, 0.5f)];
        [label addSubview:line];
        
        [titleBgView addSubview:label];
    }
    
    if (self.buttonTitles.count) {
        
        for (int i = 0; i < self.buttonTitles.count; i++) {
            
            // 所有按钮
            UIButton *btn = [[UIButton alloc] init];
            [btn setTag:(i + 1)];
            [btn setBackgroundColor:[UIColor whiteColor]];
            [btn setTitle:self.buttonTitles[i] forState:UIControlStateNormal];
            [[btn titleLabel] setFont:LC_ACTION_SHEET_TITLE_FONT];
            UIColor *titleColor = nil;
            if (i == self.redButtonIndex - 1) {
                
                titleColor = LCColor(255, 10, 10);
                
            } else {
                
                titleColor = LCColor(18, 23, 31) ;
            }
            [btn setTitleColor:titleColor forState:UIControlStateNormal];
            
            [btn addTarget:self action:@selector(didClickBtn:) forControlEvents:UIControlEventTouchUpInside];
            
            CGFloat btnY = BUTTON_H * i + (self.title ? CGRectGetMaxY(titleBgView.frame) : 0);
            [btn setFrame:CGRectMake(0, btnY, SCREEN_SIZE.width, BUTTON_H)];
            [bottomView addSubview:btn];
            
            // 所有线条
            UIImageView *line = [[UIImageView alloc] init];
            [line setBackgroundColor:LCColor(234, 234, 234)];
            [line setFrame:CGRectMake(0, 0, SCREEN_SIZE.width, 0.5f)];
            [btn addSubview:line];
            
        }
        
    }
    
    // 取消按钮
    UIButton *cancelBtn = [[UIButton alloc] init];
    [cancelBtn setTag:0];
    [cancelBtn setBackgroundColor:[UIColor whiteColor]];
    [cancelBtn setTitle:self.cancelTitle forState:UIControlStateNormal];
    [[cancelBtn titleLabel] setFont:LC_ACTION_SHEET_TITLE_FONT];
    
    if (self.redButtonIndex == 0) {
        
        [cancelBtn setTitleColor:LCColor(255, 10, 10) forState:UIControlStateNormal];

        
    } else {
        
        [cancelBtn setTitleColor:LCColor(18, 23, 31) forState:UIControlStateNormal];

    }
    [cancelBtn addTarget:self action:@selector(didClickCancelBtn:) forControlEvents:UIControlEventTouchUpInside];
    
    CGFloat btnY = BUTTON_H * self.buttonTitles.count + SPACE_H + (self.title ? CGRectGetMaxY(titleBgView.frame) : 0);
    
    [cancelBtn setFrame:CGRectMake(0, btnY, SCREEN_SIZE.width, BUTTON_H)];
    [bottomView addSubview:cancelBtn];
    
    CGFloat bottomH = (self.title ? CGRectGetMaxY(titleBgView.frame) : 0) + BUTTON_H * self.buttonTitles.count + BUTTON_H + SPACE_H;
    [bottomView setFrame:CGRectMake(0, SCREEN_SIZE.height, SCREEN_SIZE.width, bottomH)];
    
    [self setFrame:(CGRect){0, 0, SCREEN_SIZE}];
}

- (UIWindow *)backWindow {
    
    if (_backWindow == nil) {
        
        _backWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _backWindow.windowLevel       = UIWindowLevelStatusBar;
        _backWindow.backgroundColor   = [UIColor clearColor];
        _backWindow.hidden = NO;
    }
    
    return _backWindow;
}


- (void)didClickBtn:(UIButton *)btn {
    
    [self dismiss:nil];
    
    if ([_delegate respondsToSelector:@selector(actionSheet:didClickedButtonAtIndex:)]) {
        
        [_delegate actionSheet:self didClickedButtonAtIndex:btn.tag];
    }
    
    if (self.clickedBlock) {
        
        self.clickedBlock(btn.tag);
    }
}

- (void)dismiss:(UITapGestureRecognizer *)tap {
    
    [UIView animateWithDuration:0.25f delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        
        [_darkView setAlpha:0];
        [_darkView setUserInteractionEnabled:NO];
        
        CGRect frame = _bottomView.frame;
        frame.origin.y += frame.size.height;
        [_bottomView setFrame:frame];
        
    } completion:^(BOOL finished) {
        
        [self removeFromSuperview];
        
        self.backWindow.hidden = YES;
    }];
}

- (void)didClickCancelBtn:(UIButton *)btn {
    
    [UIView animateWithDuration:0.25f delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        
        [_darkView setAlpha:0];
        [_darkView setUserInteractionEnabled:NO];
        
        CGRect frame = _bottomView.frame;
        frame.origin.y += frame.size.height;
        [_bottomView setFrame:frame];
        
    } completion:^(BOOL finished) {
        
        if ([_delegate respondsToSelector:@selector(actionSheet:didClickedButtonAtIndex:)]) {
            
            [_delegate actionSheet:self didClickedButtonAtIndex:btn.tag];
        }
        
        if (self.clickedBlock) {
            
            self.clickedBlock(btn.tag);
        }
        
        [self removeFromSuperview];
        
        self.backWindow.hidden = YES;
    }];
}

- (void)show {
    
    self.backWindow.hidden = NO;
    
    [self addSubview:self.bottomView];
    [self.backWindow addSubview:self];
    
    [UIView animateWithDuration:0.25f delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        
        [_darkView setAlpha:0.4f];
        [_darkView setUserInteractionEnabled:YES];
        
        CGRect frame = _bottomView.frame;
        frame.origin.y -= frame.size.height;
        [_bottomView setFrame:frame];
        
    } completion:nil];
}

@end
