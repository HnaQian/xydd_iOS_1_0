//
//  ImageGalleryViewController.swift
//  Limi
//
//  Created by 程巍巍 on 5/13/15.
//  Copyright (c) 2015 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

import UIKit

typealias TapActionBlock = (_ shouldDisplay:Bool)->Void

class ImageGalleryViewController: UIViewController, UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var countLabel = UILabel()
    var pageControl = UIPageControl()
    var collectionView: UICollectionView!
    var imageSource = [String]()
    var tapActionBlock:TapActionBlock?
    
    fileprivate var animateLocation: CGRect?
    fileprivate var currentIndex: Int = 0
    
    func show(from: UIView?, currentIndex: Int = 0){
        if from?.superview != nil {
            animateLocation = from!.superview!.convert(from!.frame, to: UIApplication.shared.keyWindow!)
        }
        self.currentIndex = currentIndex
        self.modalPresentationStyle = .custom
        UIApplication.shared.keyWindow?.rootViewController?.present(self, animated: false, completion: nil)
    }

    let backView = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backView.frame = self.view.bounds
        backView.backgroundColor = UIColor.black
        self.view.addSubview(backView)
        
        
        self.view.iheight = self.view.iheight + 300
        self.view.layer.contents = nil
        self.view.alpha = 0.0
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = UICollectionViewScrollDirection.horizontal
        self.collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.isPagingEnabled = true
        self.collectionView.showsHorizontalScrollIndicator = false
        self.collectionView.showsVerticalScrollIndicator = false
        self.collectionView.register(ImageCell.self, forCellWithReuseIdentifier: "ImageCell")
        
        pageControl.numberOfPages = imageSource.count
        pageControl.isHidden = true
        
        countLabel.font = UIFont.systemFont(ofSize: 13)
        countLabel.textColor = UIColor.white
        
        self.view.addSubview(collectionView)
        self.view.addSubview(pageControl)
        self.view.addSubview(countLabel)
        
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        pageControl.translatesAutoresizingMaskIntoConstraints = false
        countLabel.translatesAutoresizingMaskIntoConstraints = false
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "|-0-[cv]-0-|", options:[], metrics: nil, views: ["cv": collectionView]))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[cv]-0-|", options:[], metrics: nil, views: ["cv": collectionView]))
        
        self.view.addConstraint(NSLayoutConstraint(item: pageControl, attribute: NSLayoutAttribute.centerX, relatedBy: NSLayoutRelation.equal, toItem: self.view, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: pageControl, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: self.view, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: -66))
        
        self.view.addConstraint(NSLayoutConstraint(item: countLabel, attribute: NSLayoutAttribute.centerX, relatedBy: NSLayoutRelation.equal, toItem: self.view, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: countLabel, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: self.view, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: -66))
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ImageGalleryViewController.hide)))
    }

    override func viewDidAppear(_ animated: Bool) 
    {   
        self.collectionView.backgroundColor = UIColor.clear
        self.collectionView.scrollToItem(at: IndexPath(item: currentIndex, section: 0), at: UICollectionViewScrollPosition.centeredHorizontally, animated: false)
        let center = self.view.center
        if animateLocation != nil {
            let transform = CGAffineTransform(scaleX: animateLocation!.width/self.view.frame.width, y: animateLocation!.height/self.view.frame.height)
            self.view.transform = transform
            self.view.center = CGPoint(x: animateLocation!.origin.x + animateLocation!.width/2, y: animateLocation!.origin.y + animateLocation!.height/2)
            
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            
            UIView.animate(withDuration: 0.25, animations: { () -> Void in
                self.view.center = center
                self.view.alpha = 1.0
            })
        }
        
        countLabel.text = "\(pageControl.currentPage+1)/\(imageSource.count)"
        countLabel.updateConstraints()
    }
    
    func hide(){
        if animateLocation != nil 
        {
            self.tapActionBlock?(false)
            
            UIView.animate(withDuration: 0.25, animations: { () -> Void in
                self.view.alpha = 0.0
            }, completion: { (stop) -> Void in
                self.dismiss(animated: false, completion: nil)
            })
        }else{
            self.dismiss(animated: false, completion: nil)
        }
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return imageSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath) as! ImageCell
        cell.reloadData(imageSource[(indexPath as NSIndexPath).item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        return collectionView.frame.size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetX = scrollView.contentOffset.x
        pageControl.currentPage = Int(offsetX/self.view.frame.width + 0.5)
        countLabel.text = "\(pageControl.currentPage+1)/\(imageSource.count)"
        countLabel.updateConstraints()
    }
}

extension ImageGalleryViewController {
    class ImageCell: UICollectionViewCell, UIScrollViewDelegate {
        var imageView = UIImageView()
        var scrollView = UIScrollView()
        
        func reloadData(_ url: String) {
            let imageScale : String = url + "?imageView2/0/w/" + "\(Int(Constant.ScreenSize.SCREEN_WIDTH*Constant.ScreenSize.SCALE_SCREEN))" + "/h/" + "\(Int(Constant.ScreenSize.SCREEN_WIDTH*Constant.ScreenSize.SCALE_SCREEN))"
            
            imageView.af_setImageWithURL(URL(string: imageScale)!, placeholderImage: UIImage(named: "loadingDefault"))
            
            scrollView.zoomScale = 1
        }
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            self.contentView.addSubview(scrollView)
            self.scrollView.addSubview(imageView)
            scrollView.maximumZoomScale = 2.0
            scrollView.minimumZoomScale = 1.0
            scrollView.delegate = self
            scrollView.showsHorizontalScrollIndicator = false
            scrollView.showsVerticalScrollIndicator = false
            imageView.contentMode = UIViewContentMode.scaleAspectFit
        }

        required init(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            scrollView.frame = self.bounds
            imageView.frame = self.bounds
        }
        
        func viewForZooming(in scrollView: UIScrollView) -> UIView?{
            return imageView
        }
    }
}
