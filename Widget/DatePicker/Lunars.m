//
//  Lunars.m
//  Limi
//
//  Created by ZhiQiangMi on 16/1/14.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

#import "Lunars.h"

@implementation Lunars
+ (instancetype)modelWithDict:(NSDictionary *)dict
{
    Lunars *lunars = [[self alloc]init];
    [lunars setValuesForKeysWithDictionary:dict];
    return lunars;
}
@end
