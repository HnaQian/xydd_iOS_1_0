//
//  RQDateSwitch.h
//  LuarTools
//
//  Created by guo chen on 15/10/26.
//  Copyright © 2015年 Richie. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RQDateSwitch : NSObject

/**
 *  历法转换
 *
 *  @param sourceDate    要转换的日期 如果是公历： 传入NSDate  农历传入：NSDictionary
 *  @param calendarType 要转换的类型，0：公历转农历 1：农历转公历
 *
 *  @return 返回转换后的日期
 */
+ (NSDictionary *)calendarSwitch:(NSObject *)sourceDate andTargetCalendarType:(int)calendarType;

@end
