//
//  gregorians.m
//  Limi
//
//  Created by ZhiQiangMi on 16/1/14.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

#import "gregorians.h"

@implementation Gregorians
+ (instancetype)modelWithDict:(NSDictionary *)dict
{
    Gregorians *gregorians = [[self alloc]init];
    [gregorians setValuesForKeysWithDictionary:dict];
    return gregorians;
}
@end
