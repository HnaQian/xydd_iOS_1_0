//
//  DatePickerView.h
//  Limi
//
//  Created by ZhiQiangMi on 15/9/17.
//  Copyright (c) 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChineseCalendar.h"

// 手动设置循环的宏-倍数 规则是年月日row数量一致
#define YEAR_LOOP 1
#define MONTH_LOOP 1
#define DAY_LOOP 1

// 联动的row基数，避免会挑到row 0
#define CHANG_BASE_ROWS 0

// toolBar
#define SEGMENT_WIDTH 80

#define SEGMENT_X 5
#define SEGMENT_Y 6


@protocol DatePickerViewDelegate;

//日历显示的类型
enum calendarType {
    Gregorian1=1,
    Chinese1
};

@interface DatePickerView : UIView<UIPickerViewDataSource,UIPickerViewDelegate>{
    int type;
    NSInteger startYear;
    NSInteger endYear;
    NSMutableArray *years;//第一列的数据容器
    NSMutableArray *months;//第二列的数据容器
    NSMutableArray *days;//第三列的数据容器
    
    NSMutableArray *months_hidden;
    NSMutableArray *days_hidden;
    
    XYDDCalendar *cal;//日期类
    UIPickerView *picker;
    id<DatePickerViewDelegate> delegate;
}
@property (nonatomic, weak) id<DatePickerViewDelegate> delegate;
@property (nonatomic ,strong) NSDate *showDate;
@property (nonatomic, strong)UIButton *hideYearButton;
//最小年份不能小于1901 最大年份不能大于2049
- (id)initWithFrame:(CGRect)frame type:(BOOL)_type displayDate:(NSDate *)displayDete yearStart:(NSInteger)_yearStart yearEnd:(NSInteger)_yearEnd toolBarHeight:(CGFloat)_toolBarHeight pickerHeight:(CGFloat)_pickerHeight isGuideBirthdayView:(BOOL)_isGuideBirthdayView;
@end

@protocol DatePickerViewDelegate <NSObject>
//通知使用这个控件的类，用户选取的日期
- (void)notifyNewCalendar:(XYDDCalendar *)cal type:(int)_type date:(NSDate *)date isYearHidden : (BOOL)_isYearHidden onlyMonthDay:(NSString *)monthDay;
@end
