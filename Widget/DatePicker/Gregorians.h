//
//  Gregorians.h
//  Limi
//
//  Created by ZhiQiangMi on 16/1/14.
//  Copyright © 2016年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Gregorians : NSObject
@property(nonatomic,copy) NSString *name;
@property(nonatomic,strong) NSArray *days;
+ (instancetype)modelWithDict:(NSDictionary *)dict;
@end
