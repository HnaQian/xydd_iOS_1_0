//
//  CalendarUtil.mm
//  Limi
//
//  Created by ZhiQiangMi on 15/9/17.
//  Copyright (c) 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

#import "CalendarUtil.h"
#import "SolarDate.h"
#import "ChineseDate.h"
#import "RQDateSwitch.h"

#define CH_Years @[@"甲子", @"乙丑", @"丙寅",	@"丁卯",	@"戊辰",	@"己巳",	@"庚午",	@"辛未",	@"壬申",	@"癸酉",@"甲戌",	@"乙亥",	@"丙子",	@"丁丑", @"戊寅",@"己卯",	@"庚辰",	@"辛己",	@"壬午",	@"癸未",@"甲申",	@"乙酉",	@"丙戌",	@"丁亥",	@"戊子",	@"己丑",	@"庚寅",	@"辛卯",	@"壬辰",	@"癸巳",@"甲午",	@"乙未",	@"丙申",	@"丁酉",	@"戊戌",	@"己亥",	@"庚子",	@"辛丑",	@"壬寅",	@"癸丑",@"甲辰",	@"乙巳",	@"丙午",	@"丁未",	@"戊申",	@"己酉",	@"庚戌",	@"辛亥",	@"壬子",	@"癸丑",@"甲寅",	@"乙卯",	@"丙辰",	@"丁巳",	@"戊午",	@"己未",	@"庚申",	@"辛酉",	@"壬戌",	@"癸亥"]

#define CH_Month @[@"正月", @"二月", @"三月", @"四月", @"五月", @"六月", @"七月", @"八月",@"九月", @"十月", @"冬月", @"腊月"]

#define CH_Days @[@"初一", @"初二", @"初三", @"初四", @"初五", @"初六", @"初七", @"初八", @"初九", @"初十",@"十一", @"十二", @"十三", @"十四", @"十五", @"十六", @"十七", @"十八", @"十九", @"二十",@"廿一", @"廿二", @"廿三", @"廿四", @"廿五", @"廿六", @"廿七", @"廿八", @"廿九", @"三十"]

@implementation CalendarUtil
+ (NSDateComponents *)toSolarDateWithYear:(NSUInteger)_year month:(NSString *)_month day:(NSUInteger)_day {
    ChineseDate chineseDate=ChineseDate((int)_year, (int)[CalendarUtil monthFromMineToCppWithYear:_year month:_month], (int)_day);
    SolarDate solarDate=chineseDate.ToSolarDate();
    NSDateComponents *dc=[[NSDateComponents alloc]init];
    dc.year=solarDate.GetYear();
    dc.month=solarDate.GetMonth();
    dc.day=solarDate.GetDay();
    return [dc autorelease];
}

+ (NSDateComponents *)toChineseDateWithYear:(NSUInteger)_year month:(NSString *)_month day:(NSUInteger)_day {
    SolarDate solarDate=SolarDate((int)_year, [_month intValue], (int)_day);
    ChineseDate chineseDate;
    solarDate.ToChineseDate(chineseDate);
    NSDateComponents *dc=[[NSDateComponents alloc]init];
    dc.year=chineseDate.GetYear();
    dc.month=chineseDate.GetMonth();
    dc.day=chineseDate.GetDay();
    return [dc autorelease];
}

+ (int)weekDayWithSolarYear:(NSUInteger)_year month:(NSString *)_month day:(NSUInteger)_day {
    SolarDate solarDate=SolarDate((int)_year, [_month intValue], (int)_day);
    return solarDate.ToWeek();
}

+ (int)weekDayWithChineseYear:(NSUInteger)_year month:(NSString *)_month day:(NSUInteger)_day {
    NSDateComponents *dc=[CalendarUtil toSolarDateWithYear:_year month:_month day:_day];
    return [CalendarUtil weekDayWithSolarYear:dc.year month:[NSString stringWithFormat:@"%ld", (long)dc.month] day:dc.day];
}

+ (NSUInteger)monthFromMineToCppWithYear:(NSUInteger)_year month:(NSString *)_month {
    int runMonth=ChineseCalendarDB::GetLeapMonth((int)_year);
    NSArray *array=[_month componentsSeparatedByString:@"-"];
    int month_=0;
    if ([[array objectAtIndex:0]isEqualToString:CHINESE_MONTH_LUNAR]) {
        month_=[[array objectAtIndex:1]intValue]+1;
    } else {
        if (runMonth!=0&&[[array objectAtIndex:1]intValue]>runMonth)
            month_=[[array objectAtIndex:1]intValue]+1;
        else
            month_=[[array objectAtIndex:1]intValue];
    }
    return month_;
}

+ (NSString *)monthFromCppToMineWithYear:(NSUInteger)_year month:(NSUInteger)_month {
    int run=ChineseCalendarDB::GetLeapMonth((int)_year);
    if (run==0) {
        return [NSString stringWithFormat:@"%@-%d", CHINESE_MONTH_NORMAL, (int)_month];
    } else {
        if (_month==run+1) {
            return [NSString stringWithFormat:@"%@-%u", CHINESE_MONTH_LUNAR, (int)_month-1];
        } else if(_month<=run) {
            return [NSString stringWithFormat:@"%@-%lu", CHINESE_MONTH_NORMAL, (unsigned long)_month];
        } else {
            return [NSString stringWithFormat:@"%@-%d", CHINESE_MONTH_NORMAL, (int)_month-1];
        }
    }
}

+ (NSString *)animalWithJiazi:(NSUInteger)jiazi {
    NSString *animal=nil;
    switch (jiazi%12) {
        case 1:{
            animal=@"鼠";
            break;
        }
        case 2:{
            animal=@"牛";
            break;
        }
        case 3:{
            animal=@"虎";
            break;
        }
        case 4:{
            animal=@"兔";
            break;
        }
        case 5:{
            animal=@"龙";
            break;
        }
        case 6:{
            animal=@"蛇";
            break;
        }
        case 7:{
            animal=@"马";
            break;
        }
        case 8:{
            animal=@"羊";
            break;
        }
        case 9:{
            animal=@"猴";
            break;
        }
        case 10:{
            animal=@"鸡";
            break;
        }
        case 11:{
            animal=@"狗";
            break;
        }
        case 0:{
            animal=@"猪";
            break;
        }
        default:
            break;
    }
    return animal;
}

+ (void)jiaziWithYear:(NSUInteger)_year outEra:(int *)era outJiazi:(int *)jiazi {
    std::pair<int, int> p=ChineseCalendarDB::GetEraAndYearOfLunar((int)_year);
    *era=p.first;
    *jiazi=p.second;
}

+ (NSMutableDictionary *)jieqiWithYear:(NSUInteger)_year {
    NSArray *arrayJieqi=[NSArray arrayWithObjects:@"小寒", @"大寒", @"立春", @"雨水", @"惊蛰", @"春分", @"清明", @"谷雨", @"立夏", @"小满", @"芒种", @"夏至", @"小暑", @"大暑", @"立秋", @"处暑", @"白露", @"秋分", @"寒露", @"霜降", @"立冬", @"小雪", @"大雪", @"冬至", nil];
    NSMutableDictionary * dic=[[NSMutableDictionary alloc]initWithCapacity:24];
    for (int i=1; i<=24; i++) {
        int day=ChineseCalendarDB::GetSolarTerm((int)_year, i);
        div_t dt=div(i, 2);
        NSDateComponents *dc=[CalendarUtil toChineseDateWithYear:_year month:[NSString stringWithFormat:@"%d", dt.rem+dt.quot] day:day];
        NSString *month=[CalendarUtil monthFromCppToMineWithYear:dc.year month:dc.month];
        [dic setObject:[arrayJieqi objectAtIndex:i-1] forKey:[NSString stringWithFormat:@"%@-%d", month, (int)dc.day]];
    }
    return [dic autorelease];
}

+ (NSDate *)stringToSolarDate:(NSString *)dateStr{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSLog(@"%@",dateStr);
    NSDate *date = [dateFormat dateFromString:[dateStr substringToIndex:10]];
    NSTimeZone *zone = [NSTimeZone systemTimeZone];
    NSInteger interval = [zone secondsFromGMTForDate: date];
    return [date  dateByAddingTimeInterval: interval];
}


+(BOOL) isleapMonth:(NSString *)dateStr
{
    return [self test:dateStr];
    return NO;
}


+(BOOL)test:(NSString *)dateStr
{
    if (dateStr.length < 10){return NO;}
    
    NSString *mdateStr = [dateStr substringToIndex:10];
    
    NSDateFormatter *dateFormat = [NSDateFormatter new];
    
    dateFormat.dateFormat = @"yyyy-MM-dd";
    
    NSDate *targetDate = [dateFormat dateFromString:mdateStr];
    
    targetDate = [targetDate dateByAddingTimeInterval:60 * 60 * 20];
    NSInteger unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitWeekday;
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDateComponents *targetComp = [calendar components:unitFlags fromDate:targetDate];

    NSDate *forntDate;
    
    
    if (targetComp.month == 1)
    {
        NSString *forntStr = [mdateStr substringToIndex:4];
        
        forntStr = [NSString stringWithFormat:@"%d-12-%2d",(int)targetComp.year - 1,(int)targetComp.day];
        
        forntDate = [[dateFormat dateFromString:forntStr] dateByAddingTimeInterval:60 * 60 * 20];
        
    }else
    {
        NSString *forntStr = [mdateStr substringToIndex:4];

        forntStr = [NSString stringWithFormat:@"%@-%2d-%2d",forntStr,(int)targetComp.month - 1,(int)targetComp.day];
        
        forntDate = [[dateFormat dateFromString:forntStr] dateByAddingTimeInterval:60 * 60 * 20];
    }
    
    
    NSLog(@"targetDate == >%@",targetDate.description);
    NSLog(@"forntDate == >%@",forntDate.description);
    
    
    NSTimeInterval oneDay = 60 * 60 * 24;
    
    
    NSDictionary *targetDic = [RQDateSwitch calendarSwitch:targetDate andTargetCalendarType:0];

    NSNumber *month = targetDic[@"month"];

    int target = month.intValue;
    
    NSNumber *sday = targetDic[@"day"];
    
    int day = sday.intValue;
    
    
    for (int i = day + 1; i < 40 + day; i++)
    {
        forntDate = [targetDate dateByAddingTimeInterval:(- i) * oneDay];
        
        NSLog(@"targetDate == >%@",targetDate.description);
        NSLog(@"forntDate == >%@",forntDate.description);
        
        NSDictionary *forntDic = [RQDateSwitch calendarSwitch:forntDate andTargetCalendarType:0];

        NSNumber *frontMonth = forntDic[@"month"];

        //    [targetComp release];
        
        int fornt = frontMonth.intValue;

        if (fornt == target)
        {
            return YES;
        }
   
    }

    return NO;
}

+ (NSString *)alTolunarYear:(NSString *)timeStr//返回农历 年、月、日
{
    NSDictionary *dateDict = [RQDateSwitch calendarSwitch:[self stringToSolarDate:timeStr] andTargetCalendarType:0];
    
    int year = ((NSNumber *)dateDict[@"year"]).intValue;
    
    int month = ((NSNumber *)dateDict[@"month"]).intValue;
    
    int day = ((NSNumber *)dateDict[@"day"]).intValue;
    
    NSInteger yu = (year)%CH_Years.count == 0 ? CH_Years.count - 1 - 3 : (year)%CH_Years.count - 1 - 3;
    
    if (yu < 0)
    {
        yu  = CH_Years.count + yu;
    }
    
    NSString *yearStr = CH_Years[yu];
    
    NSString *monthStr = CH_Month[month - 1];
    
    NSString *dayStr = CH_Days[day - 1];
    
    monthStr = [monthStr stringByAppendingString:dayStr];
    
    yearStr = [yearStr stringByAppendingString:@"年"];
    
    yearStr = [yearStr stringByAppendingString:[NSString stringWithFormat:@"(%d)",year]];
    
    yearStr = [yearStr stringByAppendingString:monthStr];
    
    return  [NSString stringWithString:yearStr];
}

+ (NSString *)alTolunar:(NSString *)timeStr//返回农历 月、日
{
    NSDictionary *dateDict = [RQDateSwitch calendarSwitch:[self stringToSolarDate:timeStr] andTargetCalendarType:0];
    
    NSString *monthStr = CH_Month[((NSNumber *)dateDict[@"month"]).intValue - 1];
    
    NSString *dayStr = CH_Days[((NSNumber *)dateDict[@"day"]).intValue - 1];
    
    return [monthStr stringByAppendingString:dayStr];
}



//服务器传回时间解析
+ (NSDictionary *)calendarChange:(NSString *)timeStr andWithType:(int)type
{
    NSDate *targetDate = [self stringToSolarDate:timeStr];
    
    NSString *targetTime = [targetDate.description substringToIndex:10];//取得 年-月-日
    
    NSDictionary *dateDict;
    
    if (type == 1)//公历转农历
    {
        targetTime = [self dateToNow:targetTime];//当前公历转为今年公历
        
        dateDict = [RQDateSwitch calendarSwitch:[self stringToSolarDate:targetTime] andTargetCalendarType:0];//今年的农历
        
    }else//农历转公历
    {
        dateDict = [RQDateSwitch calendarSwitch:[self stringToSolarDate:targetTime] andTargetCalendarType:0];//  当年的公历转为当年的农历
        
        NSString *todayStr = [[[NSDate new] description] substringToIndex:10];
        
        NSDateComponents *todayComps = [self newCompsWithdate:[NSDate new]];
        
        NSLog(@"weekday--->%ld",(long)todayComps.weekday);
        
        NSDictionary *todayDict = [RQDateSwitch calendarSwitch:[self stringToSolarDate:todayStr] andTargetCalendarType:0];//今天的农历日期
        
        NSLog(@"周 %ld",(long)todayComps.weekday);
        
        long year = todayComps.year;
        
        if (((NSNumber *)todayDict[@"month"]).intValue < ((NSNumber *)dateDict[@"month"]).intValue)
        {//节日还未到
            
        }else if (((NSNumber *)todayDict[@"month"]).intValue == ((NSNumber *)dateDict[@"month"]).intValue)
        {
            if (((NSNumber *)todayDict[@"day"]).intValue <= ((NSNumber *)dateDict[@"day"]).intValue)
            {//节日还未到
                
            }else
            {//节日已过，下个节日就是明年
                
                year = todayComps.year + 1;
            }
        }else
        {//节日已过，下个节日就是明年
            year = todayComps.year + 1;
        }
        
        /*
         当年的农历的月份与今天的农历月份对比
         
         当年的农历的日与今天的日对比，
         
         综上：如果生日已过，那就是明年
         
         没有过，那就是今年
         */
        
        dateDict = [RQDateSwitch calendarSwitch:@{@"year":@(year),@"month":dateDict[@"month"],@"day":dateDict[@"day"]} andTargetCalendarType:1];//今年的农历转为今年的公历
    }
    
    return dateDict;
}

+ (NSString *)dateToNow:(NSString *)dateString
{
    NSDate *currentDate = [self stringToSolarDate:dateString];
    
    NSString *curDateStr = dateString;
    
    if ([dateString isEqualToString:@"2016-01-01"])
    {
       // NSLog(@"dateString--> %@",dateString);
    }
    
    NSDate *date = [NSDate new];
    
    NSTimeInterval oneHour = 60 * 60;//一小时
    
    date = [date dateByAddingTimeInterval:8 * oneHour];
    
    if ([date compare:currentDate] == NSOrderedDescending) {//如果当前年份大于节日时间的年份
        
        curDateStr = [dateString substringFromIndex:4];
        
        NSDateComponents *comps = [self newCompsWithdate:date];
        
        curDateStr = [NSString stringWithFormat:@"%ld%@",(long)comps.year,curDateStr];
    }
    
    return [NSString stringWithString:curDateStr];
}

+ (NSDateComponents *)newCompsWithdate:(NSDate *)date
{
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    
    NSInteger unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitWeekday;

    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    comps = [calendar components:unitFlags fromDate:date];
    
    return comps;
}


+ (NSDictionary *)newDateDictionary:(NSDate *)date
{
    NSDateComponents *comps = [self newCompsWithdate:date];

    return  [[NSDictionary alloc] initWithDictionary:@{@"year":@(comps.year),@"month":@(comps.month),@"day":@(comps.day),@"weekday":@(comps.weekday)}];
}


+ (NSString *)subString:(NSString *)string withRang:(NSRange)rang
{
    return [string substringWithRange:rang];
}

+ (NSString *)subString:(NSString *)string withFromIndex:(NSInteger)index
{
    if (index < 0 || index > string.length - 1)
    {
        return @"无";
    }
    
    return [string substringFromIndex:index];
}

+ (NSString *)subString:(NSString *)string withToIndex:(NSInteger)index
{
    return [string substringToIndex:index];
}

//dateDict：时间字典，targetType：0：拼接成公历  1：拼接成农历
+(NSString *)dateDictToString:(NSDictionary *)dateDict andTargetDateType:(int)targetType
{
    int year = ((NSNumber *)dateDict[@"year"]).intValue;
    
    int month = ((NSNumber *)dateDict[@"month"]).intValue;
    
    int day = ((NSNumber *)dateDict[@"day"]).intValue;
    
    if (targetType == 0)
    {//公历
        if ((year%4 == 0 && year%100 != 0) && month > 2)//如果是闰年
        {
            NSDate *date = [self stringToSolarDate:[NSString stringWithFormat:@"%4d-%02d-%02d",year,month,day]];
            
            NSTimeInterval oneHour = 60 * 60;//一小时
            
            date = [date dateByAddingTimeInterval:-24 * oneHour];
            
            return [[date description] substringToIndex:10];
        }
        
        return  [NSString stringWithFormat:@"%4d-%02d-%02d",year,month,day];
        
    }else//农历
    {
        NSInteger yu = (year)%CH_Years.count == 0 ? CH_Years.count - 1 - 3 : (year)%CH_Years.count - 1 - 3;
        
        if (yu < 0)
        {
            yu  = CH_Years.count + yu;
        }
        
        NSString *yearStr = CH_Years[yu];
        
        
        NSString *monthStr = CH_Month[month-1];
        
        NSString *dayStr = CH_Days[day-1];
        
        monthStr = [monthStr stringByAppendingString:dayStr];
        
        yearStr = [yearStr stringByAppendingString:@"年"];
        
        yearStr = [yearStr stringByAppendingString:monthStr];
        
        return  [NSString stringWithString:yearStr];
    }
}




@end

