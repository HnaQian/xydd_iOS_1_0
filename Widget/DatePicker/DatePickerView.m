//
//  DatePickerView.m
//  Limi
//
//  Created by ZhiQiangMi on 15/9/17.
//  Copyright (c) 2015年 ZhiZhuo (Shanghai) Network Technology Co., Ltd. All rights reserved.
//

#import "DatePickerView.h"
#import "CalendarUtil.h"
#import "Gregorians.h"
#import "Lunars.h"
#import "UIView+RQLayout.h"




@interface LuaAAGreButton : UIButton

@property (nonatomic,assign)BOOL shouldDisplay;
@property (nonatomic,strong)UIImageView *siginImage;
@property (nonatomic,strong)NSString *mtitle;
@end
@implementation LuaAAGreButton

-(void)setShouldDisplay:(BOOL)shouldDisplay
{
    if (shouldDisplay) {
        
        self.siginImage.image = [UIImage imageNamed:@"datePicker_selecte"];
    }else
    {
        self.siginImage.image = [UIImage imageNamed:@"datePicker_unselecte"];
    }
}



- (instancetype)initWithFrame:(CGRect)frame andTitle:(NSString *)title
{
    self = [super initWithFrame:frame];
    if (self) {
        self.mtitle = title;
        
        [self setupUI];
    }
    return self;
}

-(void)setupUI
{
    self.siginImage.image = [UIImage imageNamed:@"noChoose"];
    self.siginImage.backgroundColor = [UIColor clearColor];
    self.shouldDisplay = false;
    
    self.userInteractionEnabled = YES;
    
    CGFloat width = 20;
    
    self.siginImage = [[UIImageView alloc] initWithFrame:CGRectMake((self.iheight - width)/2, (self.iheight - width)/2, width, width)];
    
//    self.siginImage.contentMode = UIViewContentModeCenter;
    
    [self addSubview:self.siginImage];
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(self.siginImage.iright + 4, 0, self.iwidth - width - (self.iheight - width)/2, self.iheight)];
    title.backgroundColor = [UIColor clearColor];
    title.font  = [UIFont systemFontOfSize:13];
    
    title.text = self.mtitle;
    
    [self addSubview:title];
}


@end



@interface DatePickerView ()

@property (nonatomic, assign)BOOL isYearHidden;

@property (nonatomic, assign)int orgMonthIndex;//传来的month
@property (nonatomic, assign)int orgDayIndex;//传来的天

@property (nonatomic, assign)NSInteger curGreMonthIndex;//当前公历月数的row
@property (nonatomic, assign)NSInteger curGreDayIndex;//当前公历天数的row

@property (nonatomic, assign)NSInteger curLuaMonthIndex;//当前农月数的row
@property (nonatomic, assign)NSInteger curLuaDayIndex;//当前农天数的row

@property (nonatomic ,strong,readonly) NSArray *gregorianData;
@property (nonatomic ,strong,readonly) NSArray *lunardata;

@property (nonatomic,assign)NSArray *gregorianAry;//公历
@property (nonatomic,assign)NSArray *luarAry;//农历
@property (nonatomic,assign)LuaAAGreButton *luarBtn;
@property (nonatomic,assign)LuaAAGreButton *gregorianBtn;
- (void)_setYears;
- (void)_setMonthsInYear:(NSUInteger)_year;
- (void)_setDaysInMonth:(NSString *)_month year:(NSUInteger)_year;
- (void)changeMonths;
- (void)changeDays;
@end


@implementation DatePickerView
@synthesize delegate;


-(NSArray *)gregorianAry
{
    static NSMutableArray *gorary;
    
    if (gorary == NULL)
    {
        gorary = [NSMutableArray new];
        
        NSString *path = [[NSBundle mainBundle] pathForResource:@"GregorianMonth" ofType:@"plist"];
        
        NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:path];
        
        for (int i = 0; i < 12; i ++)
        {
            NSString *key = [NSString stringWithFormat:@"Item%d",i];
            
            [gorary addObject:dict[key]];
        }
    }
    
    return gorary;
}

//-(void)setRow_height:(NSInteger)row_height
//{
//    _row_height = row_height;
//
//    [picker reloadAllComponents];
//}

-(NSArray *)luarAry
{
    static NSMutableArray *luarary;
    
    if (luarary == NULL)
    {
        luarary = [NSMutableArray new];
        
        NSString *path = [[NSBundle mainBundle] pathForResource:@"ChineseMonth" ofType:@"plist"];
        
        NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:path];
        
        for (int i = 0; i < 12; i ++)
        {
            NSString *key = [NSString stringWithFormat:@"Item%d",i];
            
            [luarary addObject:dict[key]];
        }
    }
    
    return luarary;
}


#pragma mark - 初始化函数
- (id)initWithFrame:(CGRect)frame type:(BOOL)_type displayDate:(NSDate *)displayDete yearStart:(NSInteger)_yearStart yearEnd:(NSInteger)_yearEnd toolBarHeight:(CGFloat)_toolBarHeight pickerHeight:(CGFloat)_pickerHeight isGuideBirthdayView:(BOOL)_isGuideBirthdayView
{
    self = [super initWithFrame:frame];
    if (self) {
        // pickerview 和 toolBar 放到一个view上面
        UIView *view = [[UIView alloc] initWithFrame:self.bounds]; // 外部设置frame
        self.backgroundColor = [UIColor whiteColor];
        type=_type == NO ? 1 : 2;
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd"];
        NSString *stringFromDate = [formatter stringFromDate:displayDete];
        NSLog(@"%@",[stringFromDate substringToIndex:4]);
        NSLog(@"%@",displayDete);
        startYear = _yearStart;
        endYear = _yearEnd;
        self.backgroundColor = [UIColor whiteColor];

        if ([[stringFromDate substringToIndex:4] isEqualToString:@"1600"])
        {
            
            NSDate *date = [CalendarUtil stringToSolarDate:@"1984-01-01"];
            NSString *stringFromDate = [formatter stringFromDate:date];
            NSTimeZone *zone = [NSTimeZone systemTimeZone];
            NSInteger interval = [zone secondsFromGMTForDate: [formatter dateFromString:stringFromDate]];
            _showDate = [[[formatter dateFromString:stringFromDate]  dateByAddingTimeInterval: interval] copy];
            //初始化
            NSLog(@"%@",_showDate);
            if (_gregorianData == nil) {
                NSString *path = [[NSBundle mainBundle] pathForResource:@"GregorianMonth.plist" ofType:nil];
                NSArray *temDays = [NSArray arrayWithContentsOfFile:path];
                NSMutableArray *tem = [NSMutableArray array];
                for (NSDictionary *dict in temDays) {
                    Gregorians *gregorians = [Gregorians modelWithDict:dict];
                    [tem addObject:gregorians];
                }
                _gregorianData = tem;
            }
            
            if (_lunardata == nil) {
                NSString *path = [[NSBundle mainBundle] pathForResource:@"ChineseMonth.plist" ofType:nil];
                NSArray *temDays = [NSArray arrayWithContentsOfFile:path];
                NSMutableArray *tem = [NSMutableArray array];
                for (NSDictionary *dict in temDays) {
                    Lunars *lunars = [Lunars modelWithDict:dict];
                    [tem addObject:lunars];
                }
                _lunardata = tem;
            }
            
            self.isYearHidden = true;
            [self.hideYearButton setTitle:@"显示年份" forState:UIControlStateNormal];
            NSCalendar *myCal = [[NSCalendar alloc]initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
            unsigned units  = NSCalendarUnitMonth|NSCalendarUnitDay;
            NSDateComponents *comp1 = [myCal components:units fromDate:displayDete];
            NSInteger month = [comp1 month];
            NSInteger day = [comp1 day];
            self.orgMonthIndex = (int)month - 1;
            
            if (type == Chinese1) {
                
                self.curLuaMonthIndex = month - 1;
                self.curLuaDayIndex = day - 1;
                
                self.curGreMonthIndex = 0;
                self.curGreDayIndex = 0;
                
            }else
            {
                self.curLuaMonthIndex = 0;
                self.curLuaDayIndex = 0;
                
                self.curGreMonthIndex = month - 1;
                self.curGreDayIndex = day - 1;
            }
            
            [self refreshMonthAADayWhenYearHidden:(int)month - 1];
            
            self.orgDayIndex = (int)day - 1;
            NSLog(@"%ld,%ld",(long)month,(long)day);
        }
        else{
            NSTimeZone *zone = [NSTimeZone systemTimeZone];
            NSInteger interval = [zone secondsFromGMTForDate: [formatter dateFromString:stringFromDate]];
    
            _showDate = [[[formatter dateFromString:stringFromDate]  dateByAddingTimeInterval: interval] copy];
            self.isYearHidden = false;
        }
        
        if (type==Gregorian1) {
            cal=[[XYDDCalendar alloc]initWithYearStart:startYear end:endYear displayDate:_showDate];
        } else {
            cal=[[ChineseCalendar alloc]initWithYearStart:startYear end:endYear displayDate:_showDate];
        }
        [self _setYears];
        [self _setMonthsInYear:[cal.year intValue]];
        [self _setDaysInMonth:cal.month year:[cal.year intValue]];
        [formatter release];
        
        // 定义toolBar位置大小背景色
        UIToolbar *toolBar =[[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, _toolBarHeight)];
        toolBar.barTintColor = [UIColor whiteColor];
        toolBar.tintColor = [UIColor colorWithRed:(float)225/255 green:(float)54/255 blue:(float)54/255 alpha:1];
        [toolBar setShadowImage:[[UIImage alloc]init] forToolbarPosition:UIBarPositionAny];
        [toolBar setBackgroundImage:[[UIImage alloc]init] forToolbarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault];
        
        self.gregorianBtn = [[LuaAAGreButton alloc] initWithFrame:CGRectMake(5, 0, 60, toolBar.iheight) andTitle:@"公历"];
        
        self.luarBtn = [[LuaAAGreButton alloc] initWithFrame:CGRectMake(self.gregorianBtn.iright + 5, 0, 60, toolBar.iheight) andTitle:@"农历"];
        
        if (type == Chinese1) {
            self.luarBtn.shouldDisplay = YES;
            self.gregorianBtn.shouldDisplay = NO;
        }else
        {
            self.luarBtn.shouldDisplay = NO;
            self.gregorianBtn.shouldDisplay = YES;
        }
        
        [self.gregorianBtn addTarget:self action:@selector(chinese_Gregorian1:) forControlEvents:UIControlEventTouchUpInside];
        [self.luarBtn addTarget:self action:@selector(chinese_Gregorian1:) forControlEvents:UIControlEventTouchUpInside];//
     
        UIBarButtonItem *fixedButton  = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target: nil action: nil];
        NSArray *array = [[NSArray alloc] initWithObjects:fixedButton, nil];
        [toolBar setItems:array];
        [toolBar addSubview:self.luarBtn];
        [toolBar addSubview:self.gregorianBtn];
        
        
        self.hideYearButton.frame = CGRectMake(self.iwidth - SEGMENT_WIDTH - 5, SEGMENT_Y, SEGMENT_WIDTH, _toolBarHeight- 2*SEGMENT_Y);
        [toolBar addSubview:self.hideYearButton];
        
        // view
        picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0,_toolBarHeight - 10, view.bounds.size.width, _pickerHeight)];
        //        picker.backgroundColor = [UIColor colorWithWhite:0.965 alpha:1.000];
        picker.backgroundColor = [UIColor clearColor];
        
        // 代理
        picker.delegate = self;
        picker.dataSource = self;
        
        if (self.isYearHidden)
        {
            if (type==Gregorian1) {
                
                [picker selectRow:self.curGreMonthIndex inComponent:0 animated:YES];
                [picker selectRow:self.curGreDayIndex inComponent:1 animated:YES];
                
            } else if (type==Chinese1) {
                
                [picker selectRow:self.curLuaMonthIndex inComponent:0 animated:YES];
                [picker selectRow:self.curLuaDayIndex inComponent:1 animated:YES];
            }
        }
        else{
            
            if (type==Gregorian1) {
                
                [picker selectRow:[years indexOfObject:cal.year] inComponent:0 animated:YES];
                [picker selectRow:[months indexOfObject:cal.month] inComponent:1 animated:YES];
                [picker selectRow:[days indexOfObject:cal.day] inComponent:2 animated:YES];
                
            } else if (type==Chinese1) {
                
                [picker selectRow:[years indexOfObject:[NSString stringWithFormat:@"%@-%@-%@", cal.era, ((ChineseCalendar *)cal).jiazi, cal.year]] inComponent:0 animated:YES];
                [picker selectRow:[months indexOfObject:cal.month] inComponent:1 animated:YES];
                [picker selectRow:[days indexOfObject:cal.day] inComponent:2 animated:YES];
            }
        }
        
        
        [view addSubview:picker];
        [view addSubview:toolBar];
        
        
        UIButton *sureButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        if (_isGuideBirthdayView)
        {
            sureButton.frame = CGRectMake((self.bounds.size.width - 298)/2, view.iheight - 64, 298, 44);
            sureButton.backgroundColor = [UIColor colorWithRed:1 green:0.22745 blue:0.28235 alpha:1];
            [sureButton setTitle:@"保存" forState:UIControlStateNormal];
            sureButton.iwidth = 298;
            sureButton.iheight = 44;
            sureButton.itop = view.iheight - 64;
            sureButton.icenterX = view.iwidth/2;
        }
        else{
            
            sureButton.frame = CGRectMake(10, view.iheight - 44, view.iwidth - 20, 36);
            sureButton.backgroundColor = [UIColor colorWithRed:255.0 / 255.0 green:58.0 / 255.0 blue:72.0 / 255.0 alpha:1.0];
            
            [sureButton setTitle:@"确定" forState:UIControlStateNormal];
        }
        
        sureButton.layer.cornerRadius = 5;
        
        sureButton.layer.masksToBounds = true;
        
        
        [sureButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [sureButton addTarget:self action:@selector(sureAction:) forControlEvents:UIControlEventTouchUpInside];
        
        [view addSubview:sureButton];
        
        // 添加所有
        [self addSubview:view];
        
    }
    return self;
}



- (void)sureAction:(id)sender
{
    
    NSInteger month_index = 0;
    NSInteger day_index = 0;
    
    if (type == Chinese1)
    {
        month_index = self.curLuaMonthIndex;
        day_index = self.curLuaDayIndex;
    }else
    {
        month_index = self.curGreMonthIndex;
        day_index = self.curGreDayIndex;
    }
    // 获取值 确保不选择也显示值
    [delegate notifyNewCalendar:cal type:type date:_showDate isYearHidden:self.isYearHidden onlyMonthDay:[NSString stringWithFormat:@"%02d-%02d",(int)month_index + 1,(int)day_index + 1]];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"datepicker_doneBtnClick" object:self userInfo:NULL];
    
}

-(int)compareDate:(NSDate*)date01 withDate:(NSDate*)date02{
    int ci;
    NSComparisonResult result = [date01 compare:date02];
    switch (result)
    {
            //date02比date01大
        case NSOrderedAscending: ci=1; break;
            //date02比date01小
        case NSOrderedDescending: ci=-1; break;
            //date02=date01
        case NSOrderedSame: ci=0; break;
        default: NSLog(@"erorr dates %@, %@", date01, date02); break;
    }
    return ci;
}


#pragma mark - 公农历转换
- (void)chinese_Gregorian1:(id)sender
{
    if (!self.isYearHidden)
    {
        
        // 获取值 确保不选择也显示值
        NSString *minDateStr = @"1901-01-01";
        NSString *maxDateStr = @"2050-01-01";
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
        NSDate *minDate = [dateFormat dateFromString:minDateStr];
        NSDate *maxDate = [dateFormat dateFromString:maxDateStr];
        NSTimeZone *zone = [NSTimeZone systemTimeZone];
        NSInteger interval = [zone secondsFromGMTForDate: minDate];
        NSDate *minDateLocation = [minDate  dateByAddingTimeInterval: interval];
        NSDate *maxDateLocation = [maxDate  dateByAddingTimeInterval: interval];
        
        int minCi = [self compareDate:minDateLocation withDate:_showDate];
        int maxCi = [self compareDate:maxDateLocation withDate:_showDate];
        
        if (minCi == -1 || maxCi == 1) {
            NSLog(@"转换数据超出范围");
            return;
        }
        
        [dateFormat release];
    }
    
    // 切换公历农历
    LuaAAGreButton *btn = (LuaAAGreButton *)sender;
    
    
    if ([btn isEqual:self.gregorianBtn]) { // 公历
    
        btn.shouldDisplay = YES;
        
        self.luarBtn.shouldDisplay = NO;
        
        if (type == Gregorian1) {return;}
        
        type = Gregorian1;
        
        cal=[[XYDDCalendar alloc]initWithYearStart:startYear end:endYear displayDate:_showDate];
        
    }else {
        
        btn.shouldDisplay = YES;
        
        self.gregorianBtn.shouldDisplay = NO;
        
        if (type == Chinese1) {return;}
        
        type = Chinese1;
        
        cal=[[ChineseCalendar alloc]initWithYearStart:startYear end:endYear displayDate:_showDate];
    }
    
    
    
    if (self.isYearHidden)
    {
        if (type==Chinese1) {
            
            [self refreshMonthAADayWhenYearHidden:self.curLuaMonthIndex];
            [picker reloadAllComponents];
            
            [picker selectRow:self.curLuaMonthIndex inComponent:0 animated:YES];
            [picker selectRow:self.curLuaDayIndex inComponent:1 animated:YES];
            
        } else{
            [self refreshMonthAADayWhenYearHidden:self.curGreMonthIndex];
            [picker reloadAllComponents];
            
            [picker selectRow:self.curGreMonthIndex inComponent:0 animated:YES];
            [picker selectRow:self.curGreDayIndex inComponent:1 animated:YES];
        }
        
    }else{
        
        [self _setYears];
        [self _setMonthsInYear:[cal.year intValue]];
        [self _setDaysInMonth:cal.month year:[cal.year intValue]];
        
        [picker reloadAllComponents];
        
        if (type==Gregorian1) {
            
            [picker selectRow:[years indexOfObject:cal.year] inComponent:0 animated:YES];
            [picker selectRow:[months indexOfObject:cal.month] inComponent:1 animated:YES];
            [picker selectRow:[days indexOfObject:cal.day] inComponent:2 animated:YES];
        } else if (type==Chinese1) {
            
            [picker selectRow:[years indexOfObject:[NSString stringWithFormat:@"%@-%@-%@", cal.era, ((ChineseCalendar *)cal).jiazi, cal.year]] inComponent:0 animated:YES];
            [picker selectRow:[months indexOfObject:cal.month] inComponent:1 animated:YES];
            [picker selectRow:[days indexOfObject:cal.day] inComponent:2 animated:YES];
        }
    }
    
    // 刷新
}

#pragma mark - pickerView delegate method

// pickerview 的列数
// UIPickerViewDataSource中定义的方法，该方法返回值决定该控件包含多少列
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    // 是否忽略年份
    if (self.isYearHidden) {
        return 2;
    }
    return 3;
}

#pragma mark - 返回每列行数
// 每列的数量 区分是否去掉年份
// UIPickerViewDataSource中定义的方法，该方法返回值决定该控件指定列包含多少个列表项
// 为了实现类似循环滚动的效果 都乘以10
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (self.isYearHidden)
    {
        if (component == 0) {
            return months_hidden.count;
        }else
        {
            return days_hidden.count;
        }
        
    }else
    {
        
        NSInteger dynamicComponent = [self datePickerComponent:component];
        if (3 == dynamicComponent) {
            return years.count * YEAR_LOOP; // 为了循环
        }else if (2 == dynamicComponent) {
            return months.count * MONTH_LOOP;
        }else if (1 == dynamicComponent) {
            return days.count * DAY_LOOP;
        }else {
            return 0;
        }
    }
}



// 设置每个cell的宽度
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    NSInteger dynamicComponent = [self datePickerComponent:component];
    if (type == Chinese1) {
        switch (dynamicComponent) {
            case 3:
                return self.bounds.size.width * 3/8;
                break;
            case 2:
                return [self monthComponentWidth];
                break;
            case 1:
                return [self dayComponentWidth];
                break;
            default:
                return 0;
                break;
        }
    }else {
        switch (dynamicComponent) {
            case 3:
                return self.bounds.size.width * 3/8;
                break;
            case 2:
                return [self monthComponentWidth];
                break;
            case 1:
                return [self dayComponentWidth];
                break;
            default:
                return 0;
                break;
        }
    }
    
}



-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 30;
}

//为指定滚轮上的指定位置的Cell设置内容
// cell上得显示 区分公农历，区分是否去掉
// UIPickerViewDelegate中定义的方法，该方法返回的NSString将作为UIPickerView
// 中指定列、指定列表项的标题文本
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    // 设置个string，用来返回
    NSString *dateAlvin = [[NSString alloc]init];
    NSInteger dynamicComponent = [self datePickerComponent:component];
    
    switch (dynamicComponent) {
        case 3:{//年份
            NSString *str=[years objectAtIndex:row];
            if (type==Chinese1) {
                NSArray *array=[str componentsSeparatedByString:@"-"];
                str=[NSString stringWithFormat:@"%@(%@年)", [array objectAtIndex:2],[((ChineseCalendar *)cal).chineseYears objectAtIndex:[[array objectAtIndex:1]intValue]-1]];
                dateAlvin = str;
            }else{
                dateAlvin = [NSString stringWithFormat:@"%@%@", str,@"年"]; // 滚轮1最终显示
            }
            return dateAlvin;
            break;
        }
        case 2:{//月份
            
            if (self.isYearHidden) {
                
                return months_hidden[row];
            }
            
            NSString *str=[NSString stringWithFormat:@"%@", [months objectAtIndex:row]];
            if (type==Chinese1) {
                NSArray *array=[str componentsSeparatedByString:@"-"];
                if ([[array objectAtIndex:0]isEqualToString:@"a"]) {
                    dateAlvin=[((ChineseCalendar *)cal).chineseMonths objectAtIndex:[[array objectAtIndex:1]intValue]-1];
                } else {
                    dateAlvin=[NSString stringWithFormat:@"%@%@", @"闰", [((ChineseCalendar *)cal).chineseMonths objectAtIndex:[[array objectAtIndex:1]intValue]-1]];
                }
            } else {
                dateAlvin=[NSString stringWithFormat:@"%@%@", str, @"月"];
            }
            NSLog(@"%@",dateAlvin);
            return dateAlvin;
            break;
        }
        case 1:{//月份
            
            if (self.isYearHidden)
            {
                return days_hidden[row];
            }
            
            if (type==Gregorian1) {
                int day=[[days objectAtIndex:row]intValue];
                int weekday=[CalendarUtil weekDayWithSolarYear:[cal.year intValue] month:cal.month day:day];
                if (self.isYearHidden){
                    dateAlvin=[NSString stringWithFormat:@"%d", day];
                }
                else{
                    dateAlvin=[NSString stringWithFormat:@"%d  %@", day, [cal.weekdays objectAtIndex:weekday]];
                }
                
            } else {
                NSString *jieqi=[[CalendarUtil jieqiWithYear:[cal.year intValue]]objectForKey:[NSString stringWithFormat:@"%@-%d", cal.month, [[days objectAtIndex:row]intValue]]];
                if (!jieqi) {
                    dateAlvin=[NSString stringWithFormat:@"%@", [((ChineseCalendar *)cal).chineseDays objectAtIndex:[[days objectAtIndex:row]intValue]-1]];
                } else {
                    dateAlvin=[NSString stringWithFormat:@"%@(%@)", [((ChineseCalendar *)cal).chineseDays objectAtIndex:[[days objectAtIndex:row]intValue]-1],jieqi];
                }
            }
            NSLog(@"%@",dateAlvin);
            return dateAlvin;
            break;
        }
        default:
            break;
    }
    return dateAlvin;
}

//设置选中条的位置
- (NSUInteger)selectionPosition {
    return 1;
}



#pragma mark - pickview滚停之后的操作
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSInteger dynamicComponent = [self datePickerComponent:component];
    switch (dynamicComponent) {
        case 3:{
            NSString *str=[years objectAtIndex:row];
            if (type==Chinese1) {
                NSArray *array=[str componentsSeparatedByString:@"-"];
                str=[array objectAtIndex:2];
                NSString *pYear=[cal.year copy];
                cal.era=[array objectAtIndex:0];
                ((ChineseCalendar *)cal).jiazi=[array objectAtIndex:1];
                cal.year=str;
                //因为用户可能从2011年滚动，最后放手的时候，滚回了2011年，所以需要判断与上一次选中的年份是否不同，再联动月份的滚轮
                if (![pYear isEqualToString:cal.year]) {
                    [self changeMonths];
                }
            } else {
                cal.year=str;
                //因为公历的每年都是12个月，所以当年份变化的时候，只需要后面的天数联动
                [self changeDays];
            }
            break;
        }
        case 2:{
            
            if (self.isYearHidden)
            {
                [self refreshMonthAADayWhenYearHidden:row];
                
                [picker reloadComponent:1];
                
                if (type == Chinese1)
                {
                    self.curLuaMonthIndex = row;
                }else
                {
                    self.curGreMonthIndex = row;
                }
                
            }else{
                
                
                NSString *pMonth=[cal.month copy];
                NSString *str=[months objectAtIndex:row];
                cal.month=str;
                if (![pMonth isEqualToString:cal.month]) {
                    //联动天数的滚轮
                    [self changeDays];
                }
            }
            break;
        }
        case 1:{
            
            if (self.isYearHidden)
            {
                if (type == Chinese1)
                {
                    self.curLuaDayIndex = row;
                }else
                {
                    self.curGreDayIndex = row;
                }
                
            }else{
                cal.day=[days objectAtIndex:row];
            }
            
            break;
        }
        default:
            break;
    }
    
    if (!self.isYearHidden)
    {
        
        if (type==Gregorian1) {
            cal.weekday=[NSString stringWithFormat:@"%d", [CalendarUtil weekDayWithSolarYear:[cal.year intValue] month:cal.month day:[cal.day intValue]]];
        } else {
            cal.weekday=[NSString stringWithFormat:@"%d", [CalendarUtil weekDayWithChineseYear:[cal.year intValue] month:cal.month day:[cal.day intValue]]];
            ((ChineseCalendar *)cal).animal=[CalendarUtil animalWithJiazi:[((ChineseCalendar *)cal).jiazi intValue]];
        }
        
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
        NSInteger year,mouth,day;
        
        // 切换数据的时候保存，保证公农历切换的时候时间是一致的
        if ([cal isMemberOfClass:[XYDDCalendar class]]) {
            year = [cal.year integerValue];
            mouth = [cal.month integerValue];
            day = [cal.day integerValue];
        } else if ([cal isMemberOfClass:[ChineseCalendar class]]) {
            NSDateComponents *dateCom = [CalendarUtil toSolarDateWithYear:[cal.year intValue] month:cal.month day:[cal.day intValue]];
            
            NSLog(@"cal.month === > %@",cal.month);
            year = dateCom.year;
            mouth = dateCom.month;
            day = dateCom.day;
        }
        
        NSString *mouthStr = mouth > 9? [NSString stringWithFormat:@"%ld",(long)mouth] : [NSString stringWithFormat:@"0%ld",(long)mouth];
        NSString *dayStr = day > 9? [NSString stringWithFormat:@"%ld",(long)day] : [NSString stringWithFormat:@"0%ld",(long)day];
        NSString *CalendarDateStr = [NSString stringWithFormat:@"%ld-%@-%@",(long)year,mouthStr,dayStr];
        
        NSTimeZone *zone = [NSTimeZone systemTimeZone];
        NSInteger interval = [zone secondsFromGMTForDate: [dateFormat dateFromString:CalendarDateStr]];
        _showDate = [[[dateFormat dateFromString:CalendarDateStr]  dateByAddingTimeInterval: interval] copy];
        
        [dateFormat release];
    }
}

// 修改cell字体
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    //创建属性字典
    NSDictionary *attrDict = @{ NSFontAttributeName:[UIFont systemFontOfSize:13],
                                NSForegroundColorAttributeName: [UIColor blackColor] };
    
    NSAttributedString *att = [[NSAttributedString alloc] initWithString:[self pickerView:pickerView titleForRow:row forComponent:component] attributes:attrDict];
    
    UILabel *label = [[UILabel alloc] init];
    label.attributedText = att;
    label.textAlignment = 1;
    return label;
}

#pragma mark -Calendar Data Handle-
//动态改变农历月份列表，因为公历的月份只有12个月，不需要跟随年份滚轮联动
- (void)changeMonths{
    if (type==Chinese1) {
        [self _setMonthsInYear:[cal.year intValue]];
        [picker reloadComponent:[self monthComponent]];
        long int cell=[months indexOfObject:cal.month];
        if (cell==NSNotFound) {
            cell=0;
            cal.month=[months objectAtIndex:0];
        }
        [picker selectRow:cell inComponent:[self monthComponent] animated:YES];
        //月份改变之后，天数进行联动
        [self changeDays];
    }
}

//动态改变日期列表
- (void)changeDays{
    [self _setDaysInMonth:cal.month year:[cal.year intValue]];
    [picker reloadComponent:[self dayComponent]];
    long int cell=[days indexOfObject:cal.day];
    //假如用户上次选择的是1月31日，当月份变为2月的时候，第三列的滚轮不可能再选中31日，我们设置默认的值为第一个。
    if (cell==NSNotFound) {
        cell=0;
        cal.day=[days objectAtIndex:0];
    }
    [picker selectRow:cell inComponent:[self dayComponent] animated:YES];
}

#pragma mark -Fill init Data-
//填充年份
- (void)_setYears {
    [years release];
    years=[[cal yearsInRange]retain];
}

//填充月份
- (void)_setMonthsInYear:(NSUInteger)_year {
    [months release];
    months=[[cal monthsInYear:_year]retain];
}

//填充天数
- (void)_setDaysInMonth:(NSString *)_month year:(NSUInteger)_year {
    [days release];
    days=[[cal daysInMonth:_month year:_year]retain];
}


#pragma mark - 隐藏年份
- (void)toggleYearHidden:(UIButton *)sender {
    self.isYearHidden = !self.isYearHidden;
    
    if (self.isYearHidden) {
        
        [_hideYearButton setTitle:@"显示年份" forState:UIControlStateNormal];
        
        NSInteger month_index = 0;
        NSInteger day_index = 0;
        
        if (type == Chinese1)
        {
            month_index = self.curLuaMonthIndex;
            day_index = self.curLuaDayIndex;
        }else
        {
            month_index = self.curGreMonthIndex;
            day_index = self.curGreDayIndex;
        }
        
        [self refreshMonthAADayWhenYearHidden:month_index];
        
        [picker reloadAllComponents];
        
        
        [picker selectRow:month_index inComponent:0 animated:YES];
        [picker selectRow:day_index inComponent:1 animated:YES];
        
    } else
    {
        [_hideYearButton setTitle:@"隐藏年份" forState:UIControlStateNormal];
        
        // 获取值 确保不选择也显示值
        NSString *minDateStr = @"1901-01-01";
        NSString *maxDateStr = @"2050-01-01";
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
        NSDate *minDate = [dateFormat dateFromString:minDateStr];
        NSDate *maxDate = [dateFormat dateFromString:maxDateStr];
        NSTimeZone *zone = [NSTimeZone systemTimeZone];
        NSInteger interval = [zone secondsFromGMTForDate: minDate];
        NSDate *minDateLocation = [minDate  dateByAddingTimeInterval: interval];
        NSDate *maxDateLocation = [maxDate  dateByAddingTimeInterval: interval];
        NSLog(@"%@",_showDate);
        int minCi = [self compareDate:minDateLocation withDate:_showDate];
        int maxCi = [self compareDate:maxDateLocation withDate:_showDate];
        
        if (minCi == -1 || maxCi == 1) {
            NSLog(@"转换数据超出范围");
            return;
        }
        
        [dateFormat release];
        
        self.gregorianBtn.shouldDisplay = YES;
        
        self.luarBtn.shouldDisplay = NO;
        
        type = Gregorian1;
        
        cal=[[XYDDCalendar alloc]initWithYearStart:startYear end:endYear displayDate:_showDate];

        
        
        NSLog(@"%@",cal.day);
        [self _setYears];
        [self _setMonthsInYear:[cal.year intValue]];
        [self _setDaysInMonth:cal.month year:[cal.year intValue]];
        
        
        [picker reloadAllComponents];
        
        if (type==Gregorian1) {
            [picker selectRow:[years indexOfObject:cal.year] inComponent:0 animated:YES];
            [picker selectRow:[months indexOfObject:cal.month] inComponent:1 animated:YES];
            [picker selectRow:[days indexOfObject:cal.day] inComponent:2 animated:YES];
            
        } else if (type==Chinese1) {
            [picker selectRow:[years indexOfObject:[NSString stringWithFormat:@"%@-%@-%@", cal.era, ((ChineseCalendar *)cal).jiazi, cal.year]] inComponent:0 animated:YES];
            [picker selectRow:[months indexOfObject:cal.month] inComponent:1 animated:YES];
            [picker selectRow:[days indexOfObject:cal.day] inComponent:2 animated:YES];
        }
    }
}

- (NSInteger)datePickerComponent:(NSInteger)component {
    NSInteger componentCount = self.isYearHidden ? 2 : 3;
    return componentCount - component;
}

- (NSInteger)dayComponent {
    return self.isYearHidden ? 1 : 2;
}

- (NSInteger)monthComponent {
    return self.isYearHidden ? 0 : 1;
}

- (CGFloat)dayComponentWidth {
    return self.isYearHidden ? self.bounds.size.width / 2 : self.bounds.size.width * 3 / 8;
}

- (CGFloat)monthComponentWidth {
    return self.isYearHidden ? self.bounds.size.width / 2 : self.bounds.size.width * 2 / 8;
}

#pragma mark -dealloc-
- (void)dealloc{
    [years release];
    [months release];
    [days release];
    [cal release];
    [picker release];
    
    [_gregorianAry release];
    [_luarAry release];
    [_luarBtn release];
    [_gregorianBtn release];
    
    
    
    
//    [self.gregorianData release];
//    
//    [self.luarAry release];
    
    [super dealloc];
}

#pragma mark - 初始化隐藏年份的月日数组
-(void)refreshMonthAADayWhenYearHidden:(NSInteger)month
{
    if (month > 11 || month < 0) {
        month = 0;
    }
    
    if (months_hidden == NULL || days_hidden == NULL)
    {
        months_hidden = [NSMutableArray new];
        days_hidden = [NSMutableArray new];
    }else
    {
        [months_hidden removeAllObjects];
        [days_hidden removeAllObjects];
    }
    
    
    
    if (type == Chinese1) {
        
        for (NSDictionary *dic in self.luarAry) {
            [months_hidden addObject:dic[@"name"]];
        }
        
        for (NSString *day in self.luarAry[month][@"days"]) {
            [days_hidden addObject:day];
        }
        
        
        if (self.curLuaDayIndex > days_hidden.count - 1) {
            self.curLuaDayIndex = days_hidden.count - 1;
        }
        
    }else
    {
        for (NSDictionary *dic in self.gregorianAry) {
            [months_hidden addObject:dic[@"name"]];
        }
        
        for (NSString *day in self.gregorianAry[month][@"days"]) {
            [days_hidden addObject:day];
        }
        
        if (self.curGreDayIndex > days_hidden.count - 1) {
            self.curGreDayIndex = days_hidden.count - 1;
        }
    }
}


#pragma mark - getter
- (UIButton *)hideYearButton {
    if (!_hideYearButton) {
        _hideYearButton = [UIButton buttonWithType:UIButtonTypeCustom];

        [_hideYearButton setTitleColor:[UIColor colorWithRed:(float)225/255 green:(float)54/255 blue:(float)54/255 alpha:1] forState:UIControlStateNormal];
        
        _hideYearButton.titleLabel.font = [UIFont systemFontOfSize:15];
        
        [_hideYearButton setTitle:@"隐藏年份" forState:UIControlStateNormal];
        [_hideYearButton addTarget:self action:@selector(toggleYearHidden:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _hideYearButton;
}

@end
